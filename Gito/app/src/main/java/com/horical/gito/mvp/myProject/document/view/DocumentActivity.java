package com.horical.gito.mvp.myProject.document.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.mvp.allProject.list.view.AllProjectActivity;
import com.horical.gito.mvp.myProject.document.adapter.DocumentAdapter;
import com.horical.gito.mvp.myProject.document.adapter.DocumentItemBody;
import com.horical.gito.mvp.myProject.document.adapter.PathItemBody;
import com.horical.gito.mvp.myProject.document.adapter.TopHorizontalAdapter;
import com.horical.gito.mvp.myProject.document.injection.component.DaggerDocumentComponent;
import com.horical.gito.mvp.myProject.document.injection.component.DocumentComponent;
import com.horical.gito.mvp.myProject.document.injection.module.DocumentModule;
import com.horical.gito.mvp.myProject.document.presenter.DocumentPresenter;
import com.horical.gito.widget.recycleview.VerticalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DocumentActivity extends BaseActivity implements IDocumentView, View.OnClickListener {

    @Bind(R.id.document_switch)
    ImageView btnSwitch;

    @Bind(R.id.document_back_ll)
    LinearLayout llBack;

    @Bind(R.id.rcv_path)
    RecyclerView pathList;

    @Bind(R.id.rcv_content)
    RecyclerView containList;

    @Bind(R.id.rf_layout)
    SwipeRefreshLayout mRfLayout;

    List<IRecyclerItem> mPathItems;
    TopHorizontalAdapter mPathAdapter;

    DocumentAdapter mContainerAdapter;


    RecyclerView.LayoutManager listLayoutManager;
    RecyclerView.LayoutManager gridLayoutManager;
    int listStyle = AppConstants.LIST_STYLE;

    @Inject
    DocumentPresenter mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);
        ButterKnife.bind(this);

        DocumentComponent component = DaggerDocumentComponent.builder()
                .documentModule(new DocumentModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {

        mPathItems = new ArrayList<>();
        mPathItems.add(new PathItemBody(mPresenter.getPreferManager().getProjectId(), "Home"));

        mPresenter.getAllDocuments(mPresenter.getPreferManager().getProjectId());

        listStyle = AppConstants.LIST_STYLE;
        listLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        gridLayoutManager = new GridLayoutManager(this, 3);

        setupPath();
        setupContainer();
    }

    private void setupContainer() {
        if (listStyle == AppConstants.LIST_STYLE) {
            mContainerAdapter = new DocumentAdapter(this, mPresenter.getAllItems(), listStyle);
            containList.setLayoutManager(listLayoutManager);
            containList.setAdapter(mContainerAdapter);
            containList.addItemDecoration(new VerticalDividerItemDecoration(getApplicationContext(),true));
        }
        if (listStyle == AppConstants.GRID_STYLE) {
            mContainerAdapter = new DocumentAdapter(this, mPresenter.getAllItems(), listStyle);
            containList.setLayoutManager(gridLayoutManager);
            containList.setAdapter(mContainerAdapter);
        }
    }

    private void setupPath() {
        mPathAdapter = new TopHorizontalAdapter(this, mPathItems);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        pathList.setLayoutManager(layoutManager);
        pathList.setAdapter(mPathAdapter);
    }

    protected void initListener() {

        mContainerAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                DocumentItemBody body = (DocumentItemBody) mPresenter.getAllItems().get(position);
                //set path
                mPathItems.add(new PathItemBody(body.document.getLinkFile(), body.document.getName()));
                mPathAdapter.setCurrentPosition(mPathItems.size() - 1);
                mPathAdapter.notifyDataSetChanged();
                pathList.smoothScrollToPosition(mPathItems.size() - 1);

                //set new document list
                mPresenter.getAllDocuments(body.document.getLinkFile());

            }
        });

        mPathAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (position != mPathItems.size() - 1) {
                    //set path
                    int length = mPathItems.size();
                    for (int i = position + 1; i < length; i++) {
                        mPathItems.remove(position + 1);
                    }
                    mPathAdapter.setCurrentPosition(position);
                    mPathAdapter.notifyDataSetChanged();

                    //set new document list
                    PathItemBody body = (PathItemBody) mPathItems.get(position);
                    mPresenter.getAllDocuments(body.linkFile);
                }
            }
        });

        mRfLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
            }
        });

        btnSwitch.setOnClickListener(this);
        llBack .setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.document_back_ll:
                Intent intent = new Intent(DocumentActivity.this, AllProjectActivity.class);
                startActivity(intent);
                this.finish();
                break;
            case R.id.document_switch:
                setBtnSwitch();
                break;
        }
    }

    private void setBtnSwitch() {
        if (listStyle == AppConstants.GRID_STYLE) {
            btnSwitch.setImageDrawable(getResources().getDrawable(R.drawable.ic_grid));
            listStyle = AppConstants.LIST_STYLE;
            setupContainer();
        } else if (listStyle == AppConstants.LIST_STYLE) {
            btnSwitch.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu));
            listStyle = AppConstants.GRID_STYLE;
            setupContainer();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllDocumentSuccess() {
        mContainerAdapter.notifyDataSetChanged();
    }

    @Override
    public void getAllDocumentFailure(String err) {

    }
}
