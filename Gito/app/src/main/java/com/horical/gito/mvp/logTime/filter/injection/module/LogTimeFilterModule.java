package com.horical.gito.mvp.logTime.filter.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.logTime.filter.presenter.LogTimeFilterPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class LogTimeFilterModule {

    @PerActivity
    @Provides
    LogTimeFilterPresenter providerLogTimeFilterPresenter() {
        return new LogTimeFilterPresenter();
    }
}
