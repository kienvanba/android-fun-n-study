package com.horical.gito.mvp.myProject.task.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.myProject.task.list.presenter.TaskPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TaskModule {

    @Provides
    @PerActivity
    TaskPresenter providerTaskPresenter () {
        return new TaskPresenter();
    }
}
