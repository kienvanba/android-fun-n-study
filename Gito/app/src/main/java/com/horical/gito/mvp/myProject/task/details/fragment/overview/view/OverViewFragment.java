package com.horical.gito.mvp.myProject.task.details.fragment.overview.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.model.Task;
import com.horical.gito.model.User;
import com.horical.gito.model.wrapperModel.WrapperListUser;
import com.horical.gito.mvp.dialog.dialogTask.TaskDateTimePickerDialog;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.adapter.UserAdapter;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.injection.component.DaggerOverViewComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.injection.component.OverViewComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.injection.module.OverViewModule;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.presenter.OverViewPresenter;
import com.horical.gito.mvp.myProject.task.details.view.TaskDetailsActivity;
import com.horical.gito.mvp.popup.popupTask.PopupTask;

import com.horical.gito.mvp.user.list.view.UserActivity;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.utils.HDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class OverViewFragment extends BaseFragment implements IOverView, View.OnClickListener{

    public final static int REQUEST_CODE_USER = 11;
    public static final String TAG = makeLogTag(OverViewFragment.class);

    @Bind(R.id.btn_pick_time)
    ImageView btnPickTime;

    @Bind(R.id.edt_title)
    EditText edtTitle;

    @Bind(R.id.edt_description)
    EditText edtDescription;

    @Bind(R.id.tv_type_overview)
    TextView tvType;

    @Bind(R.id.tv_status_overview)
    TextView tvStatus;

    @Bind(R.id.tv_priority_overview)
    TextView tvPriority;

    @Bind(R.id.btn_add_assignee)
    ImageView btnAddAssignee;

    @Bind(R.id.rcv_assignee)
    RecyclerView rcvAssignee;

    @Bind(R.id.tv_date_from)
    TextView tvDateFrom;

    @Bind(R.id.tv_date_to)
    TextView tvDateTo;

    private UserAdapter assignAdapter;
    private List<User> assigns;

    Task task;

    public Task initTask(){
        Task task = ((TaskDetailsActivity)getActivity()).getTask();
        task.setTitle(edtTitle.getText().toString());
        task.setAssigns(assigns);
        task.setDesc(edtDescription.getText().toString());
        task.setTaskType(PopupTask.getOrderInList(tvType.getText().toString()));
        task.setStatus(PopupTask.getOrderInList(tvStatus.getText().toString()));
        task.setPriority(PopupTask.getOrderInList(tvPriority.getText().toString()));
        return task;
    }

    private PopupTask popupTask;
    private String keyType;
    private String keyStatus;
    private String keyPriority;
    private TaskDateTimePickerDialog taskPickTime;

    private WrapperListUser wrapperListUser;

    @Inject
    OverViewPresenter mPresenter;

    public static OverViewFragment newInstance() {
        Bundle args = new Bundle();
        OverViewFragment fragment = new OverViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_task_details_overview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        OverViewComponent component = DaggerOverViewComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .overViewModule(new OverViewModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }


    protected void initData() {
        if (getActivity().getIntent().getIntExtra("type",1) == TaskDetailsActivity.OPEN_FOR_CREATING){
            keyType = "Task";
            tvType.setText("Task");
            keyStatus = "Open";
            tvStatus.setText("Open");
            keyPriority = "Immediate";
            tvPriority.setText("Immediate");

            assigns = new ArrayList<User>();
        }

        if (getActivity().getIntent().getIntExtra("type",0) == TaskDetailsActivity.OPEN_FOR_UPDATING){
            task = ((TaskDetailsActivity)getActivity()).getTask();
            edtTitle.setText(task.getTitle());
            edtDescription.setText(task.getDesc());

            keyType = PopupTask.getTypeByOrder(task.getTaskType());
            tvType.setText(keyType);
            keyStatus = PopupTask.getStatusByOrder(task.getStatus());
            tvStatus.setText(keyStatus);
            keyPriority = PopupTask.getPriorityByOrder(task.getPriority());
            tvPriority.setText(keyPriority);

            assigns = task.getAssigns();

            tvDateFrom.setText(DateUtils.formatDate(task.getStartDate(), "dd/MM/yyyy HH:mm"));
            tvDateTo.setText(DateUtils.formatDate(task.getEndDate(), "dd/MM/yyyy HH:mm"));
        }

        assignAdapter = new UserAdapter(getContext(), assigns);

        LinearLayoutManager llUser = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rcvAssignee.setLayoutManager(llUser);
        rcvAssignee.setHasFixedSize(false);
        rcvAssignee.setAdapter(assignAdapter);
    }

    protected void initListener() {
        tvType.setOnClickListener(this);
        tvStatus.setOnClickListener(this);
        tvPriority.setOnClickListener(this);
        btnPickTime.setOnClickListener(this);
        btnAddAssignee.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_type_overview:
                popupTask = new PopupTask(getContext(), new PopupTask.OnItemSelectedListener() {
                    @Override
                    public void onClickSelected(String codeFilter) {
                        tvType.setText(codeFilter);
                        keyType = codeFilter;
                    }
                });
                popupTask.showTaskType(tvType, keyType);
                break;
            case R.id.tv_status_overview:
                popupTask = new PopupTask(getContext(), new PopupTask.OnItemSelectedListener() {
                    @Override
                    public void onClickSelected(String codeFilter) {
                        tvStatus.setText(codeFilter);
                        keyStatus = codeFilter;
                    }
                });
                popupTask.showStatus(tvStatus, keyStatus);
                break;
            case R.id.tv_priority_overview:
                popupTask = new PopupTask(getContext(), new PopupTask.OnItemSelectedListener() {
                    @Override
                    public void onClickSelected(String codeFilter) {
                        tvPriority.setText(codeFilter);
                        keyPriority  = codeFilter;
                    }
                });
                popupTask.showPriority(tvPriority, keyPriority);
                break;
            case R.id.btn_pick_time: {

                taskPickTime = new TaskDateTimePickerDialog(getActivity(), new TaskDateTimePickerDialog.DateTimeDialogCallback() {
                    @Override
                    public void onDoneClick(HDate DateTimeFrom, HDate DateTimeTo) {
                        Calendar calendar = Calendar.getInstance();

                        calendar.set(DateTimeFrom.year, DateTimeFrom.month, DateTimeFrom.day, DateTimeFrom.hour, DateTimeFrom.minute);
                        Date dateFrom = new Date(calendar.getTimeInMillis());

                        calendar.set(DateTimeTo.year, DateTimeTo.month, DateTimeTo.day, DateTimeTo.hour, DateTimeTo.minute);
                        Date dateTo = new Date(calendar.getTimeInMillis());

                        ((TaskDetailsActivity)getActivity()).getTask().setStartDate(dateFrom);
                        ((TaskDetailsActivity)getActivity()).getTask().setEndDate(dateTo);

                        taskPickTime.setFrom(DateTimeFrom);
                        taskPickTime.setTo(DateTimeTo);

                        tvDateFrom.setText(DateUtils.formatDate(dateFrom, "dd/MM/yyyy HH:mm"));
                        tvDateTo.setText(DateUtils.formatDate(dateTo, "dd/MM/yyyy HH:mm"));
                    }

                    @Override
                    public void onCancelClick() {
                        taskPickTime.dismiss();
                    }
                });
                taskPickTime.setFrom(new HDate());
                taskPickTime.setTo(new HDate());
                taskPickTime.show();
                break;
            }
            case R.id.btn_add_assignee:
                Intent intent = new Intent(getContext(), UserActivity.class);
                intent.putExtra(
                        UserActivity.KEY_FROM,
                        UserActivity.FROM_NEW_SURVEY
                );

                startActivityForResult(intent, REQUEST_CODE_USER);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_USER) {
            if (resultCode == Activity.RESULT_OK) {
                wrapperListUser = (WrapperListUser) data.getSerializableExtra(UserActivity.KEY_INTENT_USER_LIST);

                for (int i=0; i<wrapperListUser.getMyUsers().size(); i++){
                    assigns.add(wrapperListUser.getMyUsers().get(i));
                }
                assignAdapter.notifyDataSetChanged();
            }
        }
    }
}
