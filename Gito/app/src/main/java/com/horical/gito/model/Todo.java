package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Todo implements Serializable{

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("status")
    @Expose
    private Boolean status;

    public Todo(String desc, Boolean status) {
        this.desc = desc;
        this.status = status;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
