package com.horical.gito.mvp.salary.detail.emailForm.view;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.mvp.salary.detail.emailForm.injection.component.DaggerIDetailEmailFormComponent;
import com.horical.gito.mvp.salary.detail.emailForm.injection.component.IDetailEmailFormComponent;
import com.horical.gito.mvp.salary.detail.emailForm.injection.module.DetailEmailFormModule;
import com.horical.gito.mvp.salary.detail.emailForm.presenter.DetailEmailFormPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailEmailFormActivity extends BaseActivity implements View.OnClickListener, IDetailEmailFormView {

    @Bind(R.id.ln_back)
    LinearLayout lnBack;

    @Bind(R.id.edt_title)
    EditText edtTitle;

    @Bind(R.id.imv_edit_title)
    ImageView imvEditTitle;

    @Bind(R.id.edt_content)
    EditText edtContent;

    @Bind(R.id.imv_edit_content)
    ImageView imvEditContent;

    @Inject
    DetailEmailFormPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_email_form);
        ButterKnife.bind(this);

        IDetailEmailFormComponent component = DaggerIDetailEmailFormComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .detailEmailFormModule(new DetailEmailFormModule())
                .build();

        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {

    }

    protected void initListener() {
        lnBack.setOnClickListener(this);
        imvEditTitle.setOnClickListener(this);
        imvEditContent.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}