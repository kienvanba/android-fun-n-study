package com.horical.gito.mvp.dialog.dialogTodoNote;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.base.BaseDialog;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Luong on 28-Mar-17.
 */

public class FilterTodoNoteDialog extends BaseDialog implements View.OnClickListener {

    @Bind(R.id.tv_todo_filter_cancel)
    GitOTextView tvTodoFilterCancel;

    @Bind(R.id.tv_todo_filter_done)
    GitOTextView tvTodoFilterDone;

    @Bind(R.id.cb_todo_filter_all)
    CheckBox cbFilterAll;

    @Bind(R.id.cb_todo_filter_metting)
    CheckBox cbFilterMetting;

    @Bind(R.id.tv_todo_filter_from)
    GitOTextView tvTodoFilterFrom;

    @Bind(R.id.tv_todo_filter_to)
    GitOTextView tvTodoFilterTo;

    @Bind(R.id.imv_todo_filter_date_picker)
    ImageView ivDatePicker;

    private Context mContext;
    private TodoDatePickerDialog pickTime;

    public FilterTodoNoteDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_todo_note_filter;
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void initListener() {
        tvTodoFilterCancel.setOnClickListener(this);
        tvTodoFilterDone.setOnClickListener(this);

        cbFilterAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });

        cbFilterMetting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
        tvTodoFilterFrom.setOnClickListener(this);
        tvTodoFilterTo.setOnClickListener(this);
        ivDatePicker.setOnClickListener(this);

    }

    public void initDataPicker() {
        pickTime = new TodoDatePickerDialog(getContext(), new TodoDatePickerDialog.DateDialogCallback() {
            @Override
            public void onDoneClick(Date DateFrom, Date DateTo) {
                tvTodoFilterFrom.setText(DateFrom.getDay() + "/" + DateFrom.getMonth() + "/" + DateFrom.getYear());
                tvTodoFilterTo.setText(DateTo.getDay() + "/" + DateTo.getMonth() + "/" + DateTo.getYear());
            }

            @Override
            public void onCancelClick() {
                pickTime.dismiss();
            }
        });
        pickTime.setFrom(new Date());
        pickTime.setTo(new Date());
        pickTime.setCanceledOnTouchOutside(true);
        pickTime.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_todo_filter_cancel:
                cancel();
                break;
            case R.id.imv_todo_filter_date_picker:
                initDataPicker();
                break;
        }
    }
}