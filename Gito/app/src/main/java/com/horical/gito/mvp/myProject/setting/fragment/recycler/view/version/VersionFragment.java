package com.horical.gito.mvp.myProject.setting.fragment.recycler.view.version;

import android.content.Intent;

import com.horical.gito.AppConstants;
import com.horical.gito.mvp.myProject.setting.adapter.VersionItemBody;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.view.BaseRecyclerFragment;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.view.NewVersionActivity;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.view.VersionDetailActivity;

public class VersionFragment extends BaseRecyclerFragment {
    @Override
    protected void requestData() {
        mPresenter.getAllVersions();
        dividerVisibility = false;
    }

    @Override
    protected void setOnItemClickListener(int position) {
        Intent intent = new Intent(getActivity(), VersionDetailActivity.class);
        intent.putExtra("version_id",((VersionItemBody)mItems.get(position)).version.get_id());
        startActivity(intent);
    }

    @Override
    protected void setOnDeleteItemClickListener(int position) {
        mPresenter.deleteVersion(position);
    }

    @Override
    public void setOnAddItemClickListener() {
        Intent intent = new Intent(getActivity(), NewVersionActivity.class);
        startActivity(intent);
    }
}
