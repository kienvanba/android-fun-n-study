package com.horical.gito.mvp.myProject.task.details.fragment.subtask.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.task.details.fragment.subtask.presenter.SubTaskPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SubTaskModule {
    @PerFragment
    @Provides
    SubTaskPresenter providerSubTaskPresenter() {
        return new SubTaskPresenter();
    }
}
