package com.horical.gito.mvp.myProject.gantt.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.myProject.gantt.presenter.GanttPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class GanttModule {
    @PerActivity
    @Provides
    GanttPresenter provideGanttPresenter() {
        return new GanttPresenter();
    }

}
