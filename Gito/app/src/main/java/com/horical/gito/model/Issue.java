package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nxon on 3/20/17
 */

public class Issue {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("resolve")
    @Expose
    private int resolve;

    @SerializedName("assigns")
    @Expose
    private User assigns;

    @SerializedName("projectId")
    @Expose
    private String projectId;

    @SerializedName("totalTask")
    @Expose
    private int totalTask;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getResolve() {
        return resolve;
    }

    public void setResolve(int resolve) {
        this.resolve = resolve;
    }

    public User getAssigns() {
        return assigns;
    }

    public void setAssigns(User assigns) {
        this.assigns = assigns;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public int getTotalTask() {
        return totalTask;
    }

    public void setTotalTask(int totalTask) {
        this.totalTask = totalTask;
    }

    public int getRemain() {
        return totalTask - resolve;
    }
}
