package com.horical.gito.mvp.myProject.sourceCode.list.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.dialog.dialogInputText.InputTextDialog;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.myProject.sourceCode.detail.view.SourceCodeDetailActivity;
import com.horical.gito.mvp.myProject.sourceCode.list.adapter.SourceCodeAdapter;
import com.horical.gito.mvp.myProject.sourceCode.list.injection.component.DaggerSourceCodeComponent;
import com.horical.gito.mvp.myProject.sourceCode.list.injection.component.SourceCodeComponent;
import com.horical.gito.mvp.myProject.sourceCode.list.injection.module.SourceCodeModule;
import com.horical.gito.mvp.myProject.sourceCode.list.presenter.SourceCodePresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SourceCodeActivity extends DrawerActivity implements ISourceCodeView, View.OnClickListener {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.refresh_code)
    SwipeRefreshLayout mRefresh;

    @Bind(R.id.rcv_code)
    RecyclerView mRcvCode;

    private SourceCodeAdapter mCodeAdapter;

    @Inject
    SourceCodePresenter mPresenter;

    protected int getLayoutId() {
        return R.layout.activity_source_code;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_SOURCE_CODE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        SourceCodeComponent component = DaggerSourceCodeComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .sourceCodeModule(new SourceCodeModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle("Code");
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);

        mCodeAdapter = new SourceCodeAdapter(this, mPresenter.getListCode());
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRcvCode.setLayoutManager(llm);
        mRcvCode.setAdapter(mCodeAdapter);

        showLoading();
        mPresenter.getAllCode(30, 1);
    }

    protected void initListener() {
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {

            @Override
            public void onLeftClicked() {
                openDrawer();
            }

            @Override
            public void onRightButtonOneClicked() {
                InputTextDialog dialog = new InputTextDialog(SourceCodeActivity.this, 0, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        showLoading();
                        mPresenter.createCode(text);
                    }
                });
                dialog.show();
            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });

        mCodeAdapter.setOnItemClickListener(new SourceCodeAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                Intent intent = new Intent(SourceCodeActivity.this, SourceCodeDetailActivity.class);
                startActivity(intent);
            }
        });

        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                mPresenter.getAllCode(30, 1);
            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllCodeSuccess() {
        mRefresh.setRefreshing(false);
        hideLoading();
        showToast("Success");
        mCodeAdapter.notifyDataSetChanged();
    }

    @Override
    public void getAllCodeFailure(String err) {
        hideLoading();
        mRefresh.setRefreshing(false);
        showToast("Failure");
    }

    @Override
    public void createCodeSuccess() {
        hideLoading();
        showToast("Success");
        mCodeAdapter.notifyDataSetChanged();
    }

    @Override
    public void createCodeFailure(String err) {
        hideLoading();
        showToast("Failure");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}
