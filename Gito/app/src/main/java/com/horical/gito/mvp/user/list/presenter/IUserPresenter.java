package com.horical.gito.mvp.user.list.presenter;

import com.horical.gito.model.User;

import java.util.List;

/**
 * Created by Tin on 15-Dec-16.
 */

public interface IUserPresenter {

    void getAllUser();

    List<User> getListSearchUser(String keyword);

}
