package com.horical.gito.mvp.todolist.note.list.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.note.GetAllNoteResponse;
import com.horical.gito.model.Note;
import com.horical.gito.mvp.todolist.note.list.view.INoteView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Luong on 21-Mar-17.
 */

public class NotePresenter extends BasePresenter implements INotePresenter {

    public List<Note> mList;

    private static final String TAG = makeLogTag(NotePresenter.class);

    public void attachView(INoteView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public INoteView getView() {
        return (INoteView) getIView();
    }

    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Note> getListNote() {
        return mList;
    }

    public void setListNote(List<Note> note) {
        this.mList = note;
    }

    @Override
    public void getAllNote() {
        getApiManager().getAllNote(new ApiCallback<GetAllNoteResponse>() {
            @Override
            public void success(GetAllNoteResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.notes);
                getView().getAllNoteSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getAllNoteFailure(error.message);
            }
        });
    }

    public void deleteNote(String noteId, final int position) {
        getApiManager().deleteNote(noteId, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteNoteSuccess(position);
            }

            @Override
            public void failure(RestError error) {
                getView().deleteNoteFailure();
            }
        });

    }
}