package com.horical.gito.interactor.api.request.version;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Request implements Serializable{
    @SerializedName("desc")
    @Expose
    public String desc;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("versionId")
    @Expose
    public String versionId;

    @SerializedName("releaseNote")
    @Expose
    public String releaseNote;

    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("endDate")
    @Expose
    public Date endDate;

    @SerializedName("status")
    @Expose
    public int status;
}
