package com.horical.gito.interactor.api.response.meeting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Meeting;


public class GetUpdateMeetingResponse extends BaseResponse{

    @SerializedName("results")
    @Expose
    public Meeting meeting;
}
