package com.horical.gito.mvp.companySetting.list.fragment.members.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.members.adapter.MembersAdapter;
import com.horical.gito.mvp.companySetting.list.fragment.members.injection.component.DaggerMembersComponent;
import com.horical.gito.mvp.companySetting.list.fragment.members.injection.component.MembersComponent;
import com.horical.gito.mvp.companySetting.list.fragment.members.injection.module.MembersModule;
import com.horical.gito.mvp.companySetting.list.fragment.members.presenter.MembersPresenter;
import com.horical.gito.mvp.user.detail.view.UserDetailActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class MembersFragment extends BaseCompanyFragment implements IMembersView, View.OnClickListener {
    public static final String TAG = makeLogTag(MembersFragment.class);
    public final static int REQUEST_CODE_ADD_MEMBERS = 1;
    @Bind(R.id.refresh)
    SwipeRefreshLayout refresh;

    @Bind(R.id.rcv_members)
    RecyclerView rcvMembers;

    private MembersAdapter adapter;

    @Inject
    MembersPresenter mPresenter;

    public static MembersFragment newInstance() {
        Bundle args = new Bundle();
        MembersFragment fragment = new MembersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_company_setting_members;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        MembersComponent component = DaggerMembersComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .membersModule(new MembersModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        adapter = new MembersAdapter(getContext(), mPresenter.getListMembers());
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvMembers.setLayoutManager(llm);
        rcvMembers.setAdapter(adapter);

        showLoading();
        mPresenter.getAllMember();
    }

    @Override
    protected void initListener() {
        adapter.setOnItemClickListener(new MembersAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(int position) {
                Intent intent = new Intent(getContext(), UserDetailActivity.class);
                intent.putExtra(UserDetailActivity.ARG_MEMBER, mPresenter.getListMembers().get(position));
                getActivity().startActivityForResult(intent, REQUEST_CODE_ADD_MEMBERS);
            }

            @Override
            public void onChangeLevel(int position, String id) {
                mPresenter.updateMember(mPresenter.getListMembers().get(position), id, "");
            }

            @Override
            public void onChangeDepartment(int position, String id) {
                mPresenter.updateMember(mPresenter.getListMembers().get(position), "", id);
            }

            @Override
            public void onDelete(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.are_you_sure_for_delete_this, "member"));
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mPresenter.deleteMember(mPresenter.getListMembers().get(position).get_id(), position);
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getAllMember();
            }
        });
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllMemberSuccess() {
        hideLoading();
        refresh.setRefreshing(false);
        Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void getAllMemberFailure(String err) {
        hideLoading();
        refresh.setRefreshing(false);
        Toast.makeText(getContext(), "failure", Toast.LENGTH_SHORT).show();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void deleteMemberSuccess(int position) {
        Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
        mPresenter.getListMembers().remove(position);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void deleteMemberFailure(String err) {
        Toast.makeText(getContext(), "failure", Toast.LENGTH_SHORT).show();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void updateMemberSuccess() {
        Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
        mPresenter.getAllMember();
    }

    @Override
    public void updateMemberFailure(String err) {
        Toast.makeText(getContext(), "failure", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateData() {
        mPresenter.getAllMember();
    }
}
