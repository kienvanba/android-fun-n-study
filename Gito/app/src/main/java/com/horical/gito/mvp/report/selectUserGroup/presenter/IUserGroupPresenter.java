package com.horical.gito.mvp.report.selectUserGroup.presenter;

import java.util.List;



public interface IUserGroupPresenter {

    void getListReportToUser();

    void getListInformToUser();

    void getListGroup();


    List<Object> getListSearchUser(String keyword);

}
