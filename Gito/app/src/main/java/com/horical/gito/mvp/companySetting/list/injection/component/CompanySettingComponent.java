package com.horical.gito.mvp.companySetting.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.injection.module.CompanySettingModule;
import com.horical.gito.mvp.companySetting.list.view.CompanySettingActivity;

import dagger.Component;

/**
 * Created by thanhle on 3/13/17.
 */


@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = CompanySettingModule.class)
public interface CompanySettingComponent {
    void inject(CompanySettingActivity activity);
}