package com.horical.gito.mvp.myProject.setting.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ComponentHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.edit_name)
    public TextView mTvName;
    @Bind(R.id.btn_delete)
    public ImageButton mBtnDelete;


    public ComponentHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
