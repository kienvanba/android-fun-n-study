package com.horical.gito.interactor.api.response.meeting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Meeting;

import java.util.List;

public class GetAllMeetingResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Meeting> meeting;
}
