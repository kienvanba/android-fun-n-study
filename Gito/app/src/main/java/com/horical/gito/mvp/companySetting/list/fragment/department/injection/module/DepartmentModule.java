package com.horical.gito.mvp.companySetting.list.fragment.department.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.list.fragment.department.presenter.DepartmentPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class DepartmentModule {
    @PerFragment
    @Provides
    DepartmentPresenter provideDepartmentPresenter(){
        return new DepartmentPresenter();
    }
}
