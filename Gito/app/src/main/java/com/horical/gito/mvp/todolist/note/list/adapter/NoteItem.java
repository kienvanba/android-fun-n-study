package com.horical.gito.mvp.todolist.note.list.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.Note;

/**
 * Created by Luong on 28-Mar-17.
 */

public class NoteItem implements IRecyclerItem {
    public Note mNote;

    public NoteItem(Note note) {
        this.mNote = note;
    }

    public Note getNote() {
        return mNote;
    }

    public void setNote(Note note) {
        mNote = note;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.NOTE_ITEM;
    }
}