package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Penguin on 23-Dec-16.
 */

public class CheckIn implements Serializable {

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("userId")
    @Expose
    public String userId;

    @SerializedName("companyId")
    @Expose
    public String companyId;

    @SerializedName("registerInHour")
    @Expose
    public String registerInHour;

    @SerializedName("registerOutHour")
    @Expose
    public String registerOutHour;

    @SerializedName("checkInHour")
    @Expose
    public String checkInHour;

    @SerializedName("checkOutHour")
    @Expose
    public String checkOutHour;

    @SerializedName("latitude")
    @Expose
    public String latitude;

    @SerializedName("longitude")
    @Expose
    public String longitude;

    @SerializedName("totalCheckin")
    @Expose
    public Float totalCheckin;

    @SerializedName("totalRegister")
    @Expose
    public Float totalRegister;

    @SerializedName("createdDate")
    @Expose
    public String createdDate;
}
