package com.horical.gito.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.horical.gito.R;

public class DialogOk extends Dialog {

    private String mTitle, mMessage;
    private IOkDialogListener mListener;

    public DialogOk(Context context, String title, String message, IOkDialogListener listener) {
        super(context, R.style.FullscreenDialog);
        mTitle = title;
        mMessage = message;
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        setContentView(R.layout.dialog_ok);

        TextView mTvTitle = (TextView) findViewById(R.id.dialog_title);
        TextView mTvMessage = (TextView) findViewById(R.id.dialog_message);
        Button mBtnOk = (Button) findViewById(R.id.dialog_btn_ok);

        mTvTitle.setText(mTitle);
        mTvMessage.setText(mMessage);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onIOkDialogAnswerOk(DialogOk.this);
            }
        });
    }

    public interface IOkDialogListener {
        void onIOkDialogAnswerOk(DialogOk dialog);
    }

}
