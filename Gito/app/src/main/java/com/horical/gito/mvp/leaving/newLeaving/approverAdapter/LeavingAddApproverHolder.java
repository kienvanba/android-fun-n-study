package com.horical.gito.mvp.leaving.newLeaving.approverAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 12/12/2016.
 */

public class LeavingAddApproverHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.leaving_iv_add_approver_check)
    ImageView mCheck;
    @Bind(R.id.leaving_tv_add_approver_name)
    GitOTextView mAddName;

    public LeavingAddApproverHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        mCheck.setImageResource(R.drawable.ic_blue_checked);
    }

    public void setCheck() {
        mCheck.setImageResource(R.drawable.ic_blue_checked);
    }

    public void setUncheck() {
        mCheck.setImageResource(R.drawable.ic_blue_check);
    }
}
