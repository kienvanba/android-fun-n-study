package com.horical.gito.interactor.api.request.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginRequest {

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("password")
    @Expose
    public String password;

    @SerializedName("deviceName")
    @Expose
    public String deviceName;

    @SerializedName("deviceDescription")
    @Expose
    public String deviceDescription;

    @SerializedName("deviceOS")
    @Expose
    public String deviceOS;

    @SerializedName("deviceOSVersion")
    @Expose
    public String deviceOSVersion;

    @SerializedName("deviceAPNSToken")
    @Expose
    public String deviceAPNSToken;

    @SerializedName("deviceFcmToken")
    @Expose
    public String deviceFcmToken;

}
