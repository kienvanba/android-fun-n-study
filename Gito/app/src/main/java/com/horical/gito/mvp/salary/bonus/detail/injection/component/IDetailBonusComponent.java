package com.horical.gito.mvp.salary.bonus.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.bonus.detail.injection.module.DetailBonusModule;
import com.horical.gito.mvp.salary.bonus.detail.view.DetailBonusActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = DetailBonusModule.class)
public interface IDetailBonusComponent {
    void inject(DetailBonusActivity activity);
}
