package com.horical.gito.mvp.myProject.task.details.presenter;

import com.horical.gito.model.Task;

public interface ITaskDetailsPresenter {
    void createTask(Task task);
    void updateOverViewTask(Task task);
}
