package com.horical.gito.mvp.companySetting.detail.newLevel.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.companySetting.CreateLevelRequest;
import com.horical.gito.interactor.api.response.companySetting.CreateLevelResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateLevelResponse;
import com.horical.gito.model.Level;
import com.horical.gito.mvp.companySetting.detail.newLevel.view.INewLevelView;

/**
 * Created by Tin on 23-Nov-16.
 */

public class NewLevelPresenter extends BasePresenter implements INewLevelPresenter {

    public void attachView(INewLevelView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public INewLevelView getView() {
        return (INewLevelView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void updateLevel(final Level level) {
        getApiManager().updateLevel(level.getId(), level, new ApiCallback<GetUpdateLevelResponse>() {
            @Override
            public void success(GetUpdateLevelResponse res) {
                getView().updateLevelSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().updateLevelFailure(error.message);
            }
        });
    }

    @Override
    public void createLevel(Level level) {
        CreateLevelRequest createLevelRequest = new CreateLevelRequest();
        createLevelRequest.name = level.getName();
        createLevelRequest.desc = level.getDesc();
        getApiManager().createLevel(createLevelRequest, new ApiCallback<CreateLevelResponse>() {
            @Override
            public void success(CreateLevelResponse res) {
                getView().createLevelSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().createLevelFailure(error.message);
            }
        });
    }
}
