package com.horical.gito.interactor.api.request.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterRequest {

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("companyId")
    @Expose
    public String companyId;

    @SerializedName("password")
    @Expose
    public String password;

    public RegisterRequest(String email, String companyId, String password) {
        this.email = email;
        this.companyId = companyId;
        this.password = password;
    }
}
