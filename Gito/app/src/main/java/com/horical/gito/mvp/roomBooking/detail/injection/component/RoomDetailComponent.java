package com.horical.gito.mvp.roomBooking.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.roomBooking.detail.injection.module.RoomDetailModule;
import com.horical.gito.mvp.roomBooking.detail.view.RoomDetailActivity;

import dagger.Component;

/**
 * Created by Lemon on 3/21/2017.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = RoomDetailModule.class)
public interface RoomDetailComponent {
    void inject(RoomDetailActivity activity);
}
