package com.horical.gito.mvp.salary.list.fragments.subEmail.presenter;

import com.horical.gito.mvp.salary.dto.emailForm.EmailFormDto;

import java.util.List;

public interface IEmailFormFragmentPresenter {
    List<EmailFormDto> getEmailForms();

}
