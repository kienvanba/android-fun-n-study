package com.horical.gito.mvp.roomBooking.list.view.dto;

/**
 * Created by QuangHai on 09/11/2016.
 */

public class Room {
    public String name;

    public String description;

    public Room(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
