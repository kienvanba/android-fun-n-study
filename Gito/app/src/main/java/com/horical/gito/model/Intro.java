package com.horical.gito.model;

import java.io.Serializable;

public class Intro implements Serializable {

    public String title;
    public String content;
    public String image;

    public Intro(String title, String content, String image) {
        this.title = title;
        this.content = content;
        this.image = image;
    }
}
