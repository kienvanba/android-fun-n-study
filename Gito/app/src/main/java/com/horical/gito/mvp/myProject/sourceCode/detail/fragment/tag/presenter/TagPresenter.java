package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.view.ITagView;

import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class TagPresenter extends BasePresenter implements ITagPresenter {
    private List<String> mList;

    public void attachView(ITagView view) {
        super.attachView(view);
    }

    public ITagView getView() {
        return (ITagView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<String> getListTag() {
        return mList;
    }

    public void setListTag(List<String> mList) {
        this.mList = mList;
    }
}
