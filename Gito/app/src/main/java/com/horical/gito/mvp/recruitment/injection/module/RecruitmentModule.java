package com.horical.gito.mvp.recruitment.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.recruitment.presenter.RecruitmentPresenter;

import dagger.Module;
import dagger.Provides;


@Module
public class RecruitmentModule {
    @PerActivity
    @Provides
    RecruitmentPresenter provideRecruitmentPresenter() {
        return new RecruitmentPresenter();
    }
}

