package com.horical.gito.mvp.drawer.dto;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nxon on 3/15/17.
 */

public class BodyHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.view_container)
    public LinearLayout mContainer;

    @Bind(R.id.menu_iv_icon)
    public ImageView mIvIcon;

    @Bind(R.id.menu_tv_title)
    public GitOTextView mTvTitle;

    public BodyHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
