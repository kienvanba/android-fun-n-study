package com.horical.gito.mvp.meeting.list.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.meeting.GetAllMeetingResponse;
import com.horical.gito.model.Meeting;
import com.horical.gito.mvp.meeting.list.view.IMeetingView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class MeetingPresenter extends BasePresenter implements IMeetingPresenter {

    private List<Meeting> mList;

    private int from;

    private static final String TAG = makeLogTag(MeetingPresenter.class);

    public IMeetingView getView() {
        return (IMeetingView) getIView();
    }

    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Meeting> getListMeeting() {
        return mList;
    }

    public void setListMeeting(List<Meeting> mList) {
        this.mList = mList;
    }

    @Override
    public void getAllMeeting() {
        getApiManager().getAllMeeting(new ApiCallback<GetAllMeetingResponse>() {
            @Override
            public void success(GetAllMeetingResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.meeting);
                getView().getAllMeetingSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getAllMeetingFailure(error.message);
            }
        });
    }

    @Override
    public void deleteMeeting(String meetingId, final int position) {
        getApiManager().deleteMeeting(meetingId, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteMeetingSuccess(position);
            }

            @Override
            public void failure(RestError error) {
                getView().deleteMeetingFailure();
            }
        });
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }
}
