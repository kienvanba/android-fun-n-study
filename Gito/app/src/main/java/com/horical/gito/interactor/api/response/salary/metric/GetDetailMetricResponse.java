package com.horical.gito.interactor.api.response.salary.metric;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Metric;

public class GetDetailMetricResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    private Metric metric;

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }
}
