package com.horical.gito.interactor.api.request.TodoNote.Note;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Luong on 10-Apr-17.
 */

public class NoteRequest {

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("desc")
    @Expose
    public String desc;

    @SerializedName("createdDate")
    @Expose
    public Date createdDate;

    @SerializedName("attachFiles")
    @Expose
    public List<String> attachFiles = new ArrayList<>();
}
