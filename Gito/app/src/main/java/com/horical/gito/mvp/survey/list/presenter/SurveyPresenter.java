package com.horical.gito.mvp.survey.list.presenter;



import android.util.Log;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyApprovedResponse;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyPendingResponse;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyResponse;
import com.horical.gito.model.Survey;
import com.horical.gito.mvp.survey.list.view.ISurveyView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by thanhle on 11/9/16.
 */

public class SurveyPresenter extends BasePresenter implements ISurveyPresenter {
    private static final String TAG = makeLogTag(SurveyPresenter.class);

    private List<Survey> mList;

    public void attachView(ISurveyView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public ISurveyView getView() {
        return (ISurveyView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Survey> getListSurvey() {
        return mList;
    }

    public void setListSurvey(List<Survey> surveys) {
        this.mList = surveys;
    }

    @Override
    public void getAllSurvey() {
        getApiManager().getAllSurvey(new ApiCallback<GetAllSurveyResponse>() {
            @Override
            public void success(GetAllSurveyResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.surveyList);
                Log.i("TAG",mList.size()+"");
                getView().getAllSurveySuccess();
            }

            @Override
            public void failure(RestError error) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                getView().getAllSurveyFailure(error.message);
            }
        });
    }

    @Override
    public void getAllSurveyApproved() {
        getApiManager().getAllSurveyApproved(new ApiCallback<GetAllSurveyApprovedResponse>() {
            @Override
            public void success(GetAllSurveyApprovedResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.surveyList);
                getView().getAllSurveySuccess();
            }

            @Override
            public void failure(RestError error) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                getView().getAllSurveyFailure(error.message);
            }
        });
    }

    @Override
    public void getAllSurveyPending() {
        getApiManager().getAllSurveyPending(new ApiCallback<GetAllSurveyPendingResponse>() {
            @Override
            public void success(GetAllSurveyPendingResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.surveyList);
                getView().getAllSurveySuccess();
            }

            @Override
            public void failure(RestError error) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                getView().getAllSurveyFailure(error.message);
            }
        });
    }
}