package com.horical.gito.mvp.survey.statistical.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.survey.statistical.adapter.StatisticalAdapter;
import com.horical.gito.mvp.survey.statistical.presenter.StatisticalPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nhattruong251295 on 4/7/2017.
 */

public class StatisticalActivity extends BaseActivity implements IStatisticalView, View.OnClickListener {
    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.rcv_result_statistical)
    RecyclerView mRcvResultStatistical;

    StatisticalAdapter adapterStatistical;

    List<String> mList;

    @Inject
    StatisticalPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_result_statistical_survey);
        ButterKnife.bind(this);

        initData();
        initListener();
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

            }
        });
    }

    protected void initData() {
        topBar.setTextTitle("Result Statistical");
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);

        mList = new ArrayList<>();
        mList.add("Jay");
        mList.add("Jeremy");
        mList.add("Jack");

        adapterStatistical = new StatisticalAdapter(mList, this);
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRcvResultStatistical.setLayoutManager(llm);
        mRcvResultStatistical.setHasFixedSize(false);
        mRcvResultStatistical.setAdapter(adapterStatistical);

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
