package com.horical.gito.mvp.salary.bonus.list.fragments.mine.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.mvp.salary.bonus.detail.view.DetailBonusActivity;
import com.horical.gito.mvp.salary.bonus.list.fragments.BaseListBonusFragment;
import com.horical.gito.mvp.salary.bonus.list.fragments.mine.adapter.BonusMineAdapter;
import com.horical.gito.mvp.salary.bonus.list.fragments.mine.injection.component.DaggerIListMineFragmentComponent;
import com.horical.gito.mvp.salary.bonus.list.fragments.mine.injection.component.IListMineFragmentComponent;
import com.horical.gito.mvp.salary.bonus.list.fragments.mine.injection.module.ListMineFragmentModule;
import com.horical.gito.mvp.salary.bonus.list.fragments.mine.presenter.ListMineFragmentPresenter;

import javax.inject.Inject;

import butterknife.Bind;

public class ListMineFragment extends BaseListBonusFragment implements View.OnClickListener,
        IListMineFragmentView {

    @Bind(R.id.rcv_bonus)
    RecyclerView rcvBonus;

    @Inject
    ListMineFragmentPresenter mPresenter;

    private BonusMineAdapter mAdapter;

    public static ListMineFragment newInstance() {
        Bundle args = new Bundle();
        ListMineFragment fragment = new ListMineFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list_bonus;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IListMineFragmentComponent component = DaggerIListMineFragmentComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .listMineFragmentModule(new ListMineFragmentModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        showLoading();
        mPresenter.requestGetMineInputAddon();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        mAdapter = new BonusMineAdapter(getActivity(), mPresenter.getListInputAddon(), new BonusMineAdapter.MineAdapterListener() {
            @Override
            public void onSelected(int position) {
                Intent detailBonusIntent = new Intent(getActivity(), DetailBonusActivity.class);
                detailBonusIntent.putExtra(AppConstants.BONUS_ID_KEY, mPresenter.getListInputAddon().get(position).get_id());
                detailBonusIntent.putExtra(AppConstants.BONUS_MODE_KEY, getString(R.string.bonus_detail));
                startActivity(detailBonusIntent);
            }

            @Override
            public void onDelete(int position) {
                Toast.makeText(getActivity(), "delete at position:" + position, Toast.LENGTH_SHORT).show();
            }
        });
        rcvBonus.setAdapter(mAdapter);
        rcvBonus.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void requestGetMineInputAddonSuccess() {
        hideLoading();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void requestGetMineInputAddonError(RestError error) {
        hideLoading();
        showErrorDialog(error.message, null);
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }
}