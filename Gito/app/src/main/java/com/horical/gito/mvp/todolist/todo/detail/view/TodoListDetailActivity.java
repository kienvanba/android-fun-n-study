package com.horical.gito.mvp.todolist.todo.detail.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.caches.files.UserCaches;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.model.TodoList;
import com.horical.gito.model.wrapperModel.WrapperListUser;
import com.horical.gito.mvp.dialog.dialogMeeting.MeetingDateTimePickerDialog;
import com.horical.gito.mvp.dialog.dialogMeeting.UploadAttachFileDialog;
import com.horical.gito.mvp.todolist.todo.detail.adapter.AttachFileTodoAdapter;
import com.horical.gito.mvp.todolist.todo.detail.injection.component.DaggerTodoListDetailComponent;
import com.horical.gito.mvp.todolist.todo.detail.injection.component.TodoListDetailComponent;
import com.horical.gito.mvp.todolist.todo.detail.injection.module.TodoListDetailModule;
import com.horical.gito.mvp.todolist.todo.detail.presenter.TodoListDetailPresenter;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.utils.FileUtils;
import com.horical.gito.utils.NetworkUtils;
import com.horical.gito.widget.edittext.GitOEditText;
import com.horical.gito.widget.textview.GitOTextView;

import java.io.File;
import java.util.Date;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;

import static com.horical.gito.mvp.meeting.detail.view.MeetingDetailActivity.REQUEST_CODE_USERS;
import static com.horical.gito.mvp.meeting.detail.view.MeetingDetailActivity.REQUEST_PERMISSION_READ_LIBRARY;
import static com.horical.gito.mvp.meeting.detail.view.MeetingDetailActivity.REQUEST_READ_LIBRARY;

public class TodoListDetailActivity extends BaseActivity implements View.OnClickListener, ITodoListDetailView
        , AttachFileTodoAdapter.OnItemClickListener {

    public static final String ARG_TODOLIST = "TodoList";
    private WrapperListUser wrapperListUser;
    private MeetingDateTimePickerDialog mTodoPickTime;

    public static final int MODE_ADD = 1;
    public static final int MODE_DETAIL = 2;
    public static final int MODE_EDIT = 3;
    public int currentMode = MODE_DETAIL;

    private TodoList todoList;
    private File mUploadFile;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.imv_todo_detail_title)
    ImageView ivTodoTitle;
    @Bind(R.id.edt_todo_detail_title)
    GitOEditText edtTodoTitle;

    @Bind(R.id.tv_todo_detail_important)
    GitOTextView tvTodoImportant;
    @Bind(R.id.cb_todo_detail_important)
    CheckBox cbTodoImportant;

    @Bind(R.id.tv_todo_detail_start_date)
    GitOTextView tvTodoStartDate;
    @Bind(R.id.tv_todo_detail_Start_time)
    GitOTextView tvTodoStartTime;
    @Bind(R.id.imv_todo_detail_calendar)
    ImageView ivTodoCalendar;

    @Bind(R.id.tv_todo_detail_item)
    GitOTextView tvTodoItem;
    @Bind(R.id.imv_todo_detail_add)
    ImageView ivTodoAdd;
    @Bind(R.id.rcv_todo_detail_content)
    RecyclerView rcvTodoContent;

    @Inject
    TodoListDetailPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todolist_detail);
        ButterKnife.bind(this);
        TodoListDetailComponent component = DaggerTodoListDetailComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .todoListDetailModule(new TodoListDetailModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        todoList = (TodoList) getIntent().getSerializableExtra(ARG_TODOLIST);
        if (todoList != null) {
            currentMode = MODE_DETAIL;
            mPresenter.setStartDate(todoList.getStartDate());
            if (todoList.getAttachFiles() != null && todoList.getAttachFiles().size() > 0) {
                mPresenter.getAttachFiles().addAll(todoList.getAttachFiles());
            }
        } else {
            currentMode = MODE_ADD;
        }

        initData();
        initListener();
        updateViewMode();
        updateViewHeader();
    }

    protected void initData() {
        LinearLayoutManager fileAttachLlm = new LinearLayoutManager(this);
        fileAttachLlm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvTodoContent.setLayoutManager(fileAttachLlm);
        rcvTodoContent.setAdapter(new AttachFileTodoAdapter(this, mPresenter.getAttachFiles(), this));

        if (todoList != null) {
            tvTodoStartDate.setText(DateUtils.formatDate(mPresenter.getStartDate(), "dd/MM/yyyy"));
            tvTodoStartTime.setText(DateUtils.formatDate(mPresenter.getStartDate(), "hh:mm a"));
            edtTodoTitle.setText(todoList.getTitle());
            cbTodoImportant.setEnabled(todoList.getImportant());
            if (todoList.getImportant()) {
                tvTodoImportant.setText(R.string.todo_important);
            } else {
                tvTodoImportant.setText(R.string.todo_unimportant);
            }
        }
    }


    protected void initListener() {
        ivTodoTitle.setOnClickListener(this);
        cbTodoImportant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvTodoImportant.setText(R.string.todo_important);
                } else {
                    tvTodoImportant.setText(R.string.todo_unimportant);
                }
            }
        });
        ivTodoCalendar.setOnClickListener(this);
        ivTodoAdd.setOnClickListener(this);

        if (currentMode == MODE_DETAIL) {
            topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {

                @Override
                public void onLeftClicked() {
                    onBackPressed();
                }

                @Override
                public void onRightButtonOneClicked() {
                    currentMode = MODE_EDIT;
                    updateViewMode();
                }

                @Override
                public void onRightButtonTwoClicked() {

                }

                @Override
                public void onRightButtonThreeClicked() {

                }
            });
        } else {
            topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
                @Override
                public void onLeftClicked() {
                    if (currentMode == MODE_ADD) {
                        onBackPressed();
                    } else {
                        if (currentMode == MODE_EDIT) {
                            currentMode = MODE_DETAIL;
                            updateViewMode();
                        }
                    }
                }

                @Override
                public void onRightClicked() {
                    if (currentMode == MODE_ADD) {
                        mPresenter.createTodo(edtTodoTitle.getText().toString());
                    } else if (currentMode == MODE_EDIT) {
                        mPresenter.updateTodo(todoList.getId(), edtTodoTitle.getText().toString());
                    }
                }
            });
        }
    }

    private void updateViewMode() {
        switch (currentMode) {
            case MODE_DETAIL:
                topBar.setTextTitle(getString(R.string.todo_detail));
                topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);

                edtTodoTitle.setEnabled(false);
                cbTodoImportant.setEnabled(false);
                ivTodoCalendar.setEnabled(false);
                ivTodoAdd.setEnabled(false);

                break;
            case MODE_ADD:
                topBar.setTextTitle("New Todo");
                topBar.setTextViewLeft("Cancel");
                topBar.setTextViewRight("Save");

                edtTodoTitle.setEnabled(true);
                cbTodoImportant.setEnabled(true);
                ivTodoCalendar.setEnabled(true);
                ivTodoAdd.setEnabled(true);
                break;

            case MODE_EDIT:
                topBar.setTextTitle("Todo Edit");
                topBar.setTextViewLeft("Cancel");
                topBar.setTextViewRight("Save");

                edtTodoTitle.setEnabled(true);
                cbTodoImportant.setEnabled(true);
                ivTodoCalendar.setEnabled(true);
                ivTodoAdd.setEnabled(true);
                break;
        }
    }

    private void updateViewHeader() {
        tvTodoItem.setText(getResources().getString(R.string.todo_content_attach, mPresenter.getAttachFiles().size()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_todo_detail_title:
                edtTodoTitle.setEnabled(true);
                break;
            case R.id.imv_todo_detail_add: {
                final UploadAttachFileDialog uploadAttachFileDialog = new UploadAttachFileDialog(this);
                uploadAttachFileDialog.setListener(new UploadAttachFileDialog.OnDialogEditFileClickListener() {
                    @Override
                    public void albumGallery() {
                        handleOpenAlBumGallery();
                        uploadAttachFileDialog.dismiss();
                    }

                    @Override
                    public void documentGallery() {
                        handleOpenDocumentGallery();
                        uploadAttachFileDialog.dismiss();
                    }
                });

                uploadAttachFileDialog.show();
                break;
            }
            case R.id.imv_todo_detail_calendar:
                initDataTimePicker();
                break;
        }
    }

    public void initDataTimePicker() {
        mTodoPickTime = new MeetingDateTimePickerDialog(this, new MeetingDateTimePickerDialog.DateTimeDialogCallback() {
            @Override
            public void onDoneClick(Date DateTimeFrom, Date DateTimeTo) {
                // TODO use here
                mPresenter.setStartDate(DateTimeFrom);
                tvTodoStartDate.setText(DateUtils.formatDate(mPresenter.getStartDate(), "dd/MM/yyyy"));
                tvTodoStartTime.setText(DateUtils.formatDate(mPresenter.getStartDate(), "hh:mm a"));
            }

            @Override
            public void onCancelClick() {
                mTodoPickTime.dismiss();
            }
        });
        mTodoPickTime.setFrom(new Date());
        mTodoPickTime.setTo(new Date());
        mTodoPickTime.show();
    }

    private void handleOpenAlBumGallery() {
        String s[] = {android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(s)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, REQUEST_READ_LIBRARY);

        } else {
            ActivityCompat.requestPermissions(this, s, REQUEST_PERMISSION_READ_LIBRARY);
        }
    }

    private void handleOpenDocumentGallery() {
        String s[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(s)) {
            Intent intent = new Intent();
            intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, REQUEST_READ_LIBRARY);
        } else {
            ActivityCompat.requestPermissions(this, s, REQUEST_PERMISSION_READ_LIBRARY);
        }

    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void validateFailedTitleEmpty() {
        showErrorDialog(getString(R.string.todo_title_empty));
    }

    @Override
    public void validateFailedTime() {
        showErrorDialog(getString(R.string.todo_time_empty));
    }

    @Override
    public void createTodoSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void createTodoFailure(RestError error) {
        showRestErrorDialog(error);
    }

    @Override
    public void updateTodoSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void updateTodoFailure(RestError error) {
        showRestErrorDialog(error);
    }

    @Override
    public void uploadSuccess(String url) {
        UserCaches.readCaches(this);
    }

    @Override
    public void uploadFailure(String error) {
        showErrorDialog(error);
    }

    @Override
    public void onAttachFileTodoDelete(int position) {
        mPresenter.getAttachFiles().remove(position);
        rcvTodoContent.getAdapter().notifyItemRemoved(position);
        rcvTodoContent.getAdapter().notifyItemRangeChanged(position, mPresenter.getAttachFiles().size());
    }

    @Override
    public void onAttachFileTodoClick(int position) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_READ_LIBRARY) {
                System.out.println("File from library " + data.getData());
                if (!NetworkUtils.isConnected(this)) {
                    showNoNetworkErrorDialog();
                } else {
                    mUploadFile = FileUtils.convertUriToFile(this, data.getData());
                    mPresenter.uploadFile(
                            MediaType.parse(getContentResolver().getType(data.getData())),
                            mUploadFile);
                }
            }
        }
        if (requestCode == REQUEST_CODE_USERS) {
            wrapperListUser = (WrapperListUser) data.getSerializableExtra("LIST_USER");
        }
    }
}