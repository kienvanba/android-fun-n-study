package com.horical.gito.mvp.companySetting.list.fragment.devices.presenter;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IDevicesPresenter {
    void getAllDevices();
    void deleteDevice(String idDevice, int position);
}
