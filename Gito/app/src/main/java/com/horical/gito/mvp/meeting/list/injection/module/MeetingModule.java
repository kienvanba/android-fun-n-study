package com.horical.gito.mvp.meeting.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.meeting.list.presenter.MeetingPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lemon on 11/21/2016.
 */

@Module
public class MeetingModule {
    @Provides
    @PerActivity
    MeetingPresenter provideMeetingPresenter(){
        return new MeetingPresenter();
    }
}
