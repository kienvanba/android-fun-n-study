package com.horical.gito.mvp.calendar.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.calendar.injection.module.CalendarModule;
import com.horical.gito.mvp.calendar.view.CalendarActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = CalendarModule.class)
public interface CalendarComponent {
    void inject(CalendarActivity activity);
}
