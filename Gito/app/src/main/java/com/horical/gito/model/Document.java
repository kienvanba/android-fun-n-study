package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Document {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("folderId")
    @Expose
    private String folderId;
    @SerializedName("linkFile")
    @Expose
    private String linkFile;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("projectId")
    @Expose
    private String projectId;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("shareList")
    @Expose
    private List<Object> shareList;
    @SerializedName("quanlity")
    @Expose
    private Integer quanlity;
    @SerializedName("isPrivate")
    @Expose
    private Boolean isPrivate;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("userId")
    @Expose
    private String userId;

    public void setId(String id) {
        this.id = id;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public void setLinkFile(String linkFile) {
        this.linkFile = linkFile;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public void setShareList(List<Object> shareList) {
        this.shareList = shareList;
    }

    public void setQuanlity(Integer quanlity) {
        this.quanlity = quanlity;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public String getFolderId() {
        return folderId;
    }

    public String getLinkFile() {
        return linkFile;
    }

    public String getLastname() {
        return lastname;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getV() {
        return v;
    }

    public List<Object> getShareList() {
        return shareList;
    }

    public Integer getQuanlity() {
        return quanlity;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public Integer getSize() {
        return size;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public String getType() {
        return type;
    }

    public String getUserId() {
        return userId;
    }
}
