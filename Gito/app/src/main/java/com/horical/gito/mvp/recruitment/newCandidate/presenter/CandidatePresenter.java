package com.horical.gito.mvp.recruitment.newCandidate.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.recruitment.newCandidate.view.ICandidateView;

public class CandidatePresenter extends BasePresenter implements ICandidatePresenter {
    public void attachView(ICandidateView view) {
        super.attachView(view);
    }

    public ICandidateView getView() {
        return (ICandidateView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }


}
