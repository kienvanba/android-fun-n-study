package com.horical.gito.mvp.companySetting.list.fragment.level.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.fragment.level.injection.module.LevelModule;
import com.horical.gito.mvp.companySetting.list.fragment.level.view.LevelFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = LevelModule.class)
public interface LevelComponent {
    void inject(LevelFragment fragment);
}
