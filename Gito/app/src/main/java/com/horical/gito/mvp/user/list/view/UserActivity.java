package com.horical.gito.mvp.user.list.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.User;
import com.horical.gito.model.wrapperModel.WrapperListUser;
import com.horical.gito.mvp.user.list.adapter.UserAdapter;
import com.horical.gito.mvp.user.list.injection.component.DaggerUserComponent;
import com.horical.gito.mvp.user.list.injection.component.UserComponent;
import com.horical.gito.mvp.user.list.injection.module.UserModule;
import com.horical.gito.mvp.user.list.presenter.UserPresenter;
import com.horical.gito.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UserActivity extends BaseActivity implements IUserView, View.OnClickListener {

    public static final String TAG = LogUtils.makeLogTag(UserActivity.class);

    public final static String KEY_FROM = "KEY_FROM";
    public static final String KEY_INTENT_USER_LIST = "USER_LIST";
    public final static int FROM_NEW_SURVEY = 0;
    public final static int FROM_FILTER_LEAVING = 1;
    public final static int FROM_LOG_TIME = 2;
    public final static int FROM_NEW_MEETING = 3;
    public final static int FROM_TASK = 4;
    public final static int FROM_LEAVING_APPROVER =5;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.edt_search_user)
    EditText edtSearch;

    @Bind(R.id.imv_clear_search)
    ImageView imvClearSearch;

    @Bind(R.id.ll_check_all)
    LinearLayout llCheckAll;

    @Bind(R.id.chk_item_user)
    CheckBox chkCheckAll;

    @Bind(R.id.refresh_user)
    SwipeRefreshLayout refreshUser;

    @Bind(R.id.rcv_user)
    RecyclerView rcvUser;

    private UserAdapter mUserAdapter;

    //nhung dong nay tu man hinh khac, neu làm xin vui long dung xoa
    private int keyFrom;

    @Inject
    UserPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);

        UserComponent component = DaggerUserComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .userModule(new UserModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        //nhung dong nay tu man hinh khac, neu làm xin vui long dung xoa
        keyFrom = getIntent().getIntExtra(KEY_FROM, 2);

        initData();
        initListener();
    }

    protected void initData() {
        mUserAdapter = new UserAdapter(this, mPresenter.getSearchUser());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcvUser.setLayoutManager(layoutManager);
        rcvUser.setHasFixedSize(false);
        rcvUser.setAdapter(mUserAdapter);

        switch (keyFrom) {
            case FROM_NEW_SURVEY: {
                topBar.setTextViewLeft(getString(R.string.cancel));
                topBar.setTextViewRight(getString(R.string.save));

                mPresenter.getAllUser();
                break;
            }
            case FROM_FILTER_LEAVING: {
                topBar.setTextTitle(getString(R.string.filter_leaving));
                topBar.setTextViewLeft(getString(R.string.cancel));
                topBar.setTextViewRight(getString(R.string.save));

                mPresenter.getAllUser();
                break;
            }
            case FROM_LOG_TIME: {
                refreshUser.setEnabled(false);
                topBar.setTextTitle(getString(R.string.user_picker));
                topBar.setTextViewLeft(getString(R.string.cancel));
                topBar.setTextViewRight(getString(R.string.done));

                WrapperListUser wrapperListUser = (WrapperListUser) getIntent().getSerializableExtra(KEY_INTENT_USER_LIST);
                mPresenter.setListUser(wrapperListUser.getMyUsers());
                edtSearch.setText("");
                mUserAdapter.notifyDataSetChanged();
                chkCheckAll.setChecked(mUserAdapter.isAllUserChecked());
                break;
            }

            case FROM_NEW_MEETING: {
                topBar.setTextTitle(getString(R.string.user_picker));
                topBar.setTextViewLeft(getString(R.string.cancel));
                topBar.setTextViewRight(getString(R.string.done));

                mPresenter.getAllUser();
                break;
            }

            case FROM_LEAVING_APPROVER: {
                topBar.setTextViewLeft(getString(R.string.cancel));
                topBar.setTextViewRight(getString(R.string.done));
                topBar.setTextTitle(getString(R.string.approver));

                mPresenter.getAllUser();
                break;
            }

            default: {
                topBar.setTextViewRight(getString(R.string.done));
                topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
                break;
            }
        }
    }

    protected void initListener() {
        llCheckAll.setOnClickListener(this);
        chkCheckAll.setOnClickListener(this);
        imvClearSearch.setOnClickListener(this);

        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {
                boolean hasUserChecked = false;
                List<User> myUser = new ArrayList<>();
                for (int i = 0; i < mPresenter.getSearchUser().size(); i++) {
                    User user = mPresenter.getSearchUser().get(i);
                    if (user.isSelected()) {
                        hasUserChecked = true;
                        myUser.add(user);
                    }
                }

                if (hasUserChecked) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(KEY_INTENT_USER_LIST, new WrapperListUser(myUser));
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    showErrorDialog(getString(R.string.error_user_picker));
                }
            }
        });

        refreshUser.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                mPresenter.getAllUser();
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateAdapterFromAllUser();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) imvClearSearch.setVisibility(View.VISIBLE);
                else imvClearSearch.setVisibility(View.GONE);
            }
        });

        mUserAdapter.setOnItemClickListener(new UserAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                isCheckAll = mUserAdapter.isAllUserChecked();
                chkCheckAll.setChecked(mUserAdapter.isAllUserChecked());
            }
        });
    }

    private void updateAdapterFromAllUser() {
        mPresenter.getListSearchUser(edtSearch.getText().toString());
        mUserAdapter.notifyDataSetChanged();
    }

    private boolean isCheckAll;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_check_all:
            case R.id.chk_item_user:
                isCheckAll = !isCheckAll;
                chkCheckAll.setChecked(isCheckAll);

                mPresenter.setCheckAll(isCheckAll);
                mUserAdapter.notifyDataSetChanged();
                break;

            case R.id.imv_clear_search:
                edtSearch.setText("");
                break;
        }
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllUserSuccess() {
        hideLoading();
        refreshUser.setRefreshing(false);
        updateAdapterFromAllUser();
    }

    @Override
    public void getAllUserFailure(String error) {
        hideLoading();
        refreshUser.setRefreshing(false);
        showToast(error);
        updateAdapterFromAllUser();
    }
}
