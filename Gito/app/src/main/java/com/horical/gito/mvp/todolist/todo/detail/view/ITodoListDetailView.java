package com.horical.gito.mvp.todolist.todo.detail.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

/**
 * Created by Luong on 16-Mar-17.
 */

public interface ITodoListDetailView extends IView {
    void showLoading();

    void hideLoading();

    void validateFailedTitleEmpty();

    void validateFailedTime();

    void createTodoSuccess();

    void createTodoFailure(RestError error);

    void updateTodoSuccess();

    void updateTodoFailure(RestError error);

    void uploadSuccess(String url);

    void uploadFailure(String error);
}
