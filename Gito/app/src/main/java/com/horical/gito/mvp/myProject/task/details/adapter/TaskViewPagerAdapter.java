package com.horical.gito.mvp.myProject.task.details.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.horical.gito.mvp.myProject.task.details.fragment.comment.view.CommentFragment;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.view.OverViewFragment;
import com.horical.gito.mvp.myProject.task.details.fragment.subtask.view.SubTaskFragment;
import com.horical.gito.mvp.myProject.task.details.fragment.logtime.view.LogTimeFragment;

public class TaskViewPagerAdapter extends FragmentPagerAdapter {

    public TaskViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return OverViewFragment.newInstance();
            case 1:
                return SubTaskFragment.newInstance();
            case 2:
                return LogTimeFragment.newInstance();
            case 3:
                return CommentFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
