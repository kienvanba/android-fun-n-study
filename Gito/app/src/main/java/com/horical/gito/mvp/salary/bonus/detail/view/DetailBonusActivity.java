package com.horical.gito.mvp.salary.bonus.detail.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.model.InputAddon;
import com.horical.gito.model.InputAddonUser;
import com.horical.gito.model.User;
import com.horical.gito.mvp.dialog.dialogInputEdit.InputEditDialog;
import com.horical.gito.mvp.salary.bonus.detail.adapter.BonusDetailUserAdapter;
import com.horical.gito.mvp.salary.bonus.detail.injection.component.DaggerIDetailBonusComponent;
import com.horical.gito.mvp.salary.bonus.detail.injection.component.IDetailBonusComponent;
import com.horical.gito.mvp.salary.bonus.detail.injection.module.DetailBonusModule;
import com.horical.gito.mvp.salary.bonus.detail.presenter.DetailBonusPresenter;
import com.horical.gito.mvp.salary.dialog.DialogFragmentAddStaff;
import com.horical.gito.utils.DateUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailBonusActivity extends BaseActivity implements View.OnClickListener,
        IDetailBonusView {

    @Bind(R.id.ln_back)
    LinearLayout lnBack;

    @Bind(R.id.tv_title)
    TextView tvTitle;

    @Bind(R.id.tv_save)
    TextView tvSave;

    @Bind(R.id.imv_edit_title)
    ImageView imvEditTitle;

    @Bind(R.id.edt_title)
    EditText edtTitle;

    @Bind(R.id.imv_edit_acronym)
    ImageView imvEditAcronym;

    @Bind(R.id.edt_acronym)
    EditText edtAcronym;

    @Bind(R.id.ln_status)
    LinearLayout lnStatus;

    @Bind(R.id.tv_date_from)
    TextView tvDateFrom;

    @Bind(R.id.tv_date_to)
    TextView tvDateTo;

    @Bind(R.id.imv_edit_time)
    ImageView imvEditTime;

    @Bind(R.id.imv_add_user)
    ImageView imvAddUser;

    @Bind(R.id.rcv_user)
    RecyclerView rcvUser;

    @Bind(R.id.ln_footer)
    LinearLayout lnFooter;

    @Bind(R.id.tv_submit)
    TextView tvSubmit;

    @Bind(R.id.tv_reject)
    TextView tvReject;

    @Bind(R.id.tv_approve)
    TextView tvApprove;

    @Inject
    DetailBonusPresenter mPresenter;

    private InputEditDialog inputEditDialog;
    private BonusDetailUserAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_bonus);
        ButterKnife.bind(this);
        IDetailBonusComponent component = DaggerIDetailBonusComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .detailBonusModule(new DetailBonusModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        String inputAddonId = getIntent().getStringExtra(AppConstants.BONUS_ID_KEY);
        if (!TextUtils.isEmpty(inputAddonId)) {
            mPresenter.requestGetDetailInputAddon(inputAddonId);
        }
        initData();
        initListener();
    }

    protected void initData() {
        tvTitle.setText(getIntent().getStringExtra(AppConstants.BONUS_MODE_KEY));

        Collections.sort(mPresenter.getInputAddon().getListUser(), inputAddonUserComparator);
        mAdapter = new BonusDetailUserAdapter(this, mPresenter.getInputAddon().getListUser(), new BonusDetailUserAdapter.UserAdapterListener() {
            @Override
            public void onRemoveUser(int position) {
                mPresenter.getInputAddon().getListUser().remove(position);
                mAdapter.notifyDataSetChanged();
            }
        });
        mAdapter.setEditable(mPresenter.isEditable());
        rcvUser.setAdapter(mAdapter);
        rcvUser.setLayoutManager(new LinearLayoutManager(this));
        setupCurrentMode();
    }

    private void setupCurrentMode() {
        if (mPresenter.isEditable()) {
            tvSave.setVisibility(View.VISIBLE);
            imvEditTitle.setVisibility(View.VISIBLE);
            imvEditAcronym.setVisibility(View.VISIBLE);
            imvEditTime.setVisibility(View.VISIBLE);
            imvAddUser.setVisibility(View.VISIBLE);
            lnFooter.setVisibility(View.VISIBLE);

            //footer
            switch (mPresenter.getInputAddon().getStatus()) {
                case DetailBonusPresenter.STATUS_NEW:
                    lnStatus.setVisibility(View.GONE);
                    tvSubmit.setVisibility(View.VISIBLE);
                    tvReject.setVisibility(View.GONE);
                    tvApprove.setVisibility(View.GONE);
                    break;

                case DetailBonusPresenter.STATUS_SUBMIT:
                    lnStatus.setVisibility(View.GONE);
                    tvSubmit.setVisibility(View.GONE);
                    tvReject.setVisibility(View.VISIBLE);
                    tvApprove.setVisibility(View.VISIBLE);
                    break;

                case DetailBonusPresenter.STATUS_REJECT:
                case DetailBonusPresenter.STATUS_APPROVE:
                    lnStatus.setVisibility(View.VISIBLE);
                    break;

                default:
                    break;
            }
        } else {
            tvSave.setVisibility(View.GONE);
            imvEditTitle.setVisibility(View.GONE);
            imvEditAcronym.setVisibility(View.GONE);
            imvEditTime.setVisibility(View.GONE);
            imvAddUser.setVisibility(View.GONE);
            lnStatus.setVisibility(View.VISIBLE);
            lnFooter.setVisibility(View.GONE);
        }
    }

    private void updateData() {
        setupCurrentMode();
        InputAddon inputAddon = mPresenter.getInputAddon();
        edtTitle.setText(inputAddon.getName());
        edtAcronym.setText(inputAddon.getAcronym());
        tvDateFrom.setText(DateUtils.formatDate(inputAddon.getApplyFrom()));
        tvDateTo.setText(DateUtils.formatDate(inputAddon.getApplyTo()));

        mAdapter.setDataChanged(inputAddon.getListUser());
        mAdapter.setEditable(mPresenter.isEditable());
        mAdapter.notifyDataSetChanged();
    }

    protected void initListener() {
        lnBack.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        imvEditTitle.setOnClickListener(this);
        imvEditAcronym.setOnClickListener(this);
        imvEditTime.setOnClickListener(this);
        imvAddUser.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        tvReject.setOnClickListener(this);
        tvApprove.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ln_back:
                finish();
                break;

            case R.id.tv_save:
                if (TextUtils.isEmpty(mPresenter.getInputAddon().get_id())) {
                    showLoading();
                    mPresenter.getInputAddon().setStatus(DetailBonusPresenter.STATUS_NEW);
                    mPresenter.requestCreateInputAddon();
                } else {
                    if (!mPresenter.getInputAddon().equals(mPresenter.getInputAddonDefault())) {
                        showLoading();
                        mPresenter.requestUpdateInputAddon();
                    }
                }
                break;

            case R.id.imv_edit_title:
                if (inputEditDialog == null || !inputEditDialog.isShowing()) {
                    inputEditDialog = new InputEditDialog(DetailBonusActivity.this,
                            edtTitle.getText().toString(),
                            new InputEditDialog.OnDoneListener() {
                                @Override
                                public void onDone(String text) {
                                    mPresenter.getInputAddon().setName(text);
                                    edtTitle.setText(mPresenter.getInputAddon().getName());
                                    inputEditDialog.dismiss();
                                }
                            });
                    inputEditDialog.show();
                }
                break;

            case R.id.imv_edit_acronym:
                if (inputEditDialog == null || !inputEditDialog.isShowing()) {
                    inputEditDialog = new InputEditDialog(DetailBonusActivity.this,
                            edtAcronym.getText().toString(),
                            new InputEditDialog.OnDoneListener() {
                                @Override
                                public void onDone(String text) {
                                    mPresenter.getInputAddon().setAcronym(text);
                                    edtAcronym.setText(mPresenter.getInputAddon().getAcronym());
                                    inputEditDialog.dismiss();
                                }
                            });
                    inputEditDialog.show();
                }

                break;

            case R.id.imv_edit_time:
                break;

            case R.id.imv_add_user:
                showLoading();
                mPresenter.requestGetAllUser();
                break;

            case R.id.tv_submit:
                mPresenter.getInputAddon().setStatus(DetailBonusPresenter.STATUS_SUBMIT);
                if (TextUtils.isEmpty(mPresenter.getInputAddon().get_id())) {
                    showLoading();
                    mPresenter.requestCreateInputAddon();
                } else {
                    if (!mPresenter.getInputAddon().equals(mPresenter.getInputAddonDefault())) {
                        showLoading();
                        mPresenter.requestUpdateInputAddon();
                    }
                }
                break;

            case R.id.tv_reject:
                mPresenter.getInputAddon().setStatus(DetailBonusPresenter.STATUS_REJECT);
                mPresenter.requestUpdateInputAddon();
                break;

            case R.id.tv_approve:
                mPresenter.getInputAddon().setStatus(DetailBonusPresenter.STATUS_APPROVE);
                mPresenter.requestUpdateInputAddon();
                break;

            default:
                break;
        }
    }

    @Override
    public void requestGetDetailInputAddonSuccess() {
        hideLoading();
        updateData();
    }

    @Override
    public void requestGetDetailInputAddonError(RestError error) {
        hideLoading();
        showErrorDialog(error.message);
    }

    @Override
    public void requestCreateInputAddonSuccess() {
        hideLoading();
        updateData();
    }

    @Override
    public void requestCreateInputAddonError(RestError error) {
        hideLoading();
        showErrorDialog(error.message);
    }

    @Override
    public void requestUpdateInputAddonSuccess() {
        hideLoading();

    }

    @Override
    public void requestUpdateInputAddonError(RestError error) {
        hideLoading();
        showErrorDialog(error.message);
    }

    @Override
    public void requestGetAllUserSuccess(List<User> users) {
        hideLoading();
        DialogFragmentAddStaff dialogAddStaff = DialogFragmentAddStaff.newInstance(getString(R.string.user));
        dialogAddStaff.setUsers(users);
        dialogAddStaff.setCallback(new DialogFragmentAddStaff.IUserAdapterListener() {
            @Override
            public void onDone(List<User> selectedUsers) {
                HashMap<String, InputAddonUser> userHashMap = new HashMap<>();
                for (InputAddonUser inputAddonUser : mPresenter.getInputAddon().getListUser()) {
                    userHashMap.put(inputAddonUser.getUserId().getUserId(), inputAddonUser);
                }
                List<InputAddonUser> inputAddonUsers = mPresenter.getInputAddonUsersFromUsers(selectedUsers);
                for (InputAddonUser inputAddonUser : inputAddonUsers) {
                    userHashMap.put(inputAddonUser.getUserId().getUserId(), inputAddonUser);
                }
                List<InputAddonUser> inputAddonUser = new ArrayList<>(userHashMap.values());
                Collections.sort(inputAddonUser, inputAddonUserComparator);
                mPresenter.getInputAddon().setListUser(inputAddonUser);
                mAdapter.notifyDataSetChanged();
            }
        });
        dialogAddStaff.show(getSupportFragmentManager(), "DialogFragmentAddStaff");
    }

    @Override
    public void requestGetAllUserError(RestError error) {
        hideLoading();
        showErrorDialog(error.message);
    }

    private Comparator<InputAddonUser> inputAddonUserComparator = new Comparator<InputAddonUser>() {
        @Override
        public int compare(InputAddonUser o1, InputAddonUser o2) {
            return o1.getUserId().getDisplayName().compareToIgnoreCase(o2.getUserId().getDisplayName());
        }
    };

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }
}