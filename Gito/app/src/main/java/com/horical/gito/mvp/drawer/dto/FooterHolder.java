package com.horical.gito.mvp.drawer.dto;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nxon on 3/15/17.
 */

public class FooterHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.menu_iv_avatar_user)
    public CircleImageView mIvAvatarUser;

    @Bind(R.id.menu_tv_user_name)
    public GitOTextView mTvName;

    @Bind(R.id.menu_tv_email)
    public GitOTextView mTvEmail;

    @Bind(R.id.menu_tv_logout)
    public GitOTextView mTvLogout;

    @Bind(R.id.menu_pgr_avt_loading)
    public ProgressBar mProgressBar;


    public FooterHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
