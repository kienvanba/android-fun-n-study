package com.horical.gito.mvp.companySetting.detail.newCompanyRole.view;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.RoleCompany;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.adapter.NewCompanyRoleAdapter;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.dto.ContentNewCompanyRole;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.injection.component.DaggerNewCompanyRoleComponent;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.injection.component.NewCompanyRoleComponent;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.injection.module.NewCompanyRoleModule;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.presenter.NewCompanyRolePresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class NewCompanyRoleActivity extends BaseActivity implements INewCompanyRoleView, View.OnClickListener {
    public static final String TAG = makeLogTag(NewCompanyRoleActivity.class);
    public static final String COMPANY_ROLE = "CompanyRole";

    public static final int MODE_DETAIL = 1;
    public static final int MODE_ADD = 2;
    public int currentMode = MODE_DETAIL;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.edt_name)
    EditText edtName;

    @Bind(R.id.edt_id)
    EditText edtId;

    @Bind(R.id.lv_new_company_role)
    ExpandableListView lvCompanyRole;

    private NewCompanyRoleAdapter adapter;
    private RoleCompany roleCompany;

    private List<String> listDataHeader;
    private HashMap<String, List<ContentNewCompanyRole>> listDataChild;

    @Inject
    NewCompanyRolePresenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_setting_new_company_role);
        ButterKnife.bind(this);

        NewCompanyRoleComponent component = DaggerNewCompanyRoleComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .newCompanyRoleModule(new NewCompanyRoleModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        roleCompany = (RoleCompany) getIntent().getSerializableExtra(COMPANY_ROLE);

        if (roleCompany != null) {
            currentMode = MODE_DETAIL;
        } else {
            currentMode = MODE_ADD;
        }

        initData();
        updateViewMode();
        initListener();
    }

    protected void initData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<ContentNewCompanyRole>>();

        if (roleCompany != null) {
            edtName.setText(roleCompany.getRoleName());
            edtId.setText(roleCompany.getRoleCompanyId());

            setDataRoleCompany(roleCompany);

            adapter = new NewCompanyRoleAdapter(this, listDataHeader, listDataChild);
            lvCompanyRole.setAdapter(adapter);

            for (int i = 0; i < adapter.getGroupCount(); i++) {
                lvCompanyRole.expandGroup(i);
            }

        } else {
            roleCompany = new RoleCompany();
            edtName.setText("");
            edtId.setText("");

            setDataRoleCompany(roleCompany);

            adapter = new NewCompanyRoleAdapter(this, listDataHeader, listDataChild);
            lvCompanyRole.setAdapter(adapter);

            for (int i = 0; i < adapter.getGroupCount(); i++) {
                lvCompanyRole.expandGroup(i);
            }
        }
    }

    public void setDataRoleCompany(RoleCompany roleCompany) {
        listDataHeader.add("Overtime");
        listDataHeader.add("Calendar");
        listDataHeader.add("Chat");
        listDataHeader.add("Project");
        listDataHeader.add("Salary");
        listDataHeader.add("Accounting");
        listDataHeader.add("Survey");
        listDataHeader.add("Leaving");
        listDataHeader.add("Checkin");
        listDataHeader.add("Announcement");
        listDataHeader.add("Recruitment");
        listDataHeader.add("CaseStudy");
        listDataHeader.add("Estate");
        listDataHeader.add("Repayment");
        listDataHeader.add("Report");
        listDataHeader.add("Meeting");
        listDataHeader.add("Room Booking");
        listDataHeader.add("Department");
        listDataHeader.add("Level");
        listDataHeader.add("Device");
        listDataHeader.add("Company Role");
        listDataHeader.add("Project Role");
        listDataHeader.add("Payment");
        listDataHeader.add("Quota");
        listDataHeader.add("Working & Holiday");
        listDataHeader.add("Member");
        listDataHeader.add("Information");
        listDataHeader.add("Feature");

        List<ContentNewCompanyRole> overtime = new ArrayList<ContentNewCompanyRole>();
        overtime.add(new ContentNewCompanyRole("Approve", roleCompany.getOverTimeRole().getApprove()));
        overtime.add(new ContentNewCompanyRole("Add", roleCompany.getOverTimeRole().getCreate()));
        overtime.add(new ContentNewCompanyRole("Delete", roleCompany.getOverTimeRole().getDel()));
        overtime.add(new ContentNewCompanyRole("Modify", roleCompany.getOverTimeRole().getModify()));
        overtime.add(new ContentNewCompanyRole("View", roleCompany.getOverTimeRole().getView()));

        List<ContentNewCompanyRole> calendar = new ArrayList<ContentNewCompanyRole>();
        calendar.add(new ContentNewCompanyRole("View", roleCompany.getCalendarRole().isView()));

        List<ContentNewCompanyRole> chat = new ArrayList<ContentNewCompanyRole>();
        chat.add(new ContentNewCompanyRole("Add free room", roleCompany.getChatRole().isCreateFreeRoom()));
        chat.add(new ContentNewCompanyRole("Create company room", roleCompany.getChatRole().isCreateCompanyRoom()));
        chat.add(new ContentNewCompanyRole("Leave room", roleCompany.getChatRole().isLeaveRoom()));
        chat.add(new ContentNewCompanyRole("Eject member", roleCompany.getChatRole().isEjectMember()));
        chat.add(new ContentNewCompanyRole("Add member", roleCompany.getChatRole().isAddMember()));
        chat.add(new ContentNewCompanyRole("View", roleCompany.getChatRole().isView()));

        List<ContentNewCompanyRole> project = new ArrayList<ContentNewCompanyRole>();
        project.add(new ContentNewCompanyRole("Delete", roleCompany.getProject().isDel()));
        project.add(new ContentNewCompanyRole("Lock", roleCompany.getProject().isLock()));
        project.add(new ContentNewCompanyRole("Modify", roleCompany.getProject().isModify()));
        project.add(new ContentNewCompanyRole("Add", roleCompany.getProject().isCreate()));
        project.add(new ContentNewCompanyRole("View", roleCompany.getProject().isView()));

        List<ContentNewCompanyRole> salary = new ArrayList<ContentNewCompanyRole>();
        salary.add(new ContentNewCompanyRole("Approve", roleCompany.getSalaryRole().isApprove()));
        salary.add(new ContentNewCompanyRole("Delete", roleCompany.getSalaryRole().isDel()));
        salary.add(new ContentNewCompanyRole("Modify", roleCompany.getSalaryRole().isModify()));
        salary.add(new ContentNewCompanyRole("Add", roleCompany.getSalaryRole().isCreate()));
        salary.add(new ContentNewCompanyRole("View", roleCompany.getSalaryRole().isView()));


        List<ContentNewCompanyRole> accounting = new ArrayList<ContentNewCompanyRole>();
        accounting.add(new ContentNewCompanyRole("Approve", roleCompany.getAccountingRole().isApprove()));
        accounting.add(new ContentNewCompanyRole("Delete", roleCompany.getAccountingRole().isDel()));
        accounting.add(new ContentNewCompanyRole("Modify", roleCompany.getAccountingRole().isModify()));
        accounting.add(new ContentNewCompanyRole("Add", roleCompany.getAccountingRole().isCreate()));
        accounting.add(new ContentNewCompanyRole("View", roleCompany.getAccountingRole().isView()));

        List<ContentNewCompanyRole> survey = new ArrayList<ContentNewCompanyRole>();
        survey.add(new ContentNewCompanyRole("Delete", roleCompany.getSurveyRole().isDel()));
        survey.add(new ContentNewCompanyRole("Modify", roleCompany.getSurveyRole().isModify()));
        survey.add(new ContentNewCompanyRole("Add", roleCompany.getSurveyRole().isCreate()));
        survey.add(new ContentNewCompanyRole("View", roleCompany.getSurveyRole().isView()));

        List<ContentNewCompanyRole> leaving = new ArrayList<ContentNewCompanyRole>();
        leaving.add(new ContentNewCompanyRole("Approve", roleCompany.getLeavingRole().isApprove()));
        leaving.add(new ContentNewCompanyRole("Delete", roleCompany.getLeavingRole().isDel()));
        leaving.add(new ContentNewCompanyRole("Modify", roleCompany.getLeavingRole().isModify()));
        leaving.add(new ContentNewCompanyRole("Add", roleCompany.getLeavingRole().isCreate()));
        leaving.add(new ContentNewCompanyRole("View", roleCompany.getLeavingRole().isView()));

        List<ContentNewCompanyRole> checkin = new ArrayList<ContentNewCompanyRole>();
        checkin.add(new ContentNewCompanyRole("Approve", roleCompany.getCheckinRole().isApprove()));
        checkin.add(new ContentNewCompanyRole("Delete", roleCompany.getCheckinRole().isDel()));
        checkin.add(new ContentNewCompanyRole("Modify", roleCompany.getCheckinRole().isModify()));
        checkin.add(new ContentNewCompanyRole("Add", roleCompany.getCheckinRole().isCreate()));
        checkin.add(new ContentNewCompanyRole("View", roleCompany.getCheckinRole().isView()));

        List<ContentNewCompanyRole> announcement = new ArrayList<ContentNewCompanyRole>();
        announcement.add(new ContentNewCompanyRole("Approve", roleCompany.getAnnouncementRole().isApprove()));
        announcement.add(new ContentNewCompanyRole("Delete", roleCompany.getAnnouncementRole().isDel()));
        announcement.add(new ContentNewCompanyRole("Modify", roleCompany.getAnnouncementRole().isModify()));
        announcement.add(new ContentNewCompanyRole("Add", roleCompany.getAnnouncementRole().isCreate()));
        announcement.add(new ContentNewCompanyRole("View", roleCompany.getAnnouncementRole().isView()));

        List<ContentNewCompanyRole> recruitment = new ArrayList<ContentNewCompanyRole>();
        recruitment.add(new ContentNewCompanyRole("Interview", roleCompany.getRecruitmentRole().isInterview()));
        recruitment.add(new ContentNewCompanyRole("Delete", roleCompany.getRecruitmentRole().isDel()));
        recruitment.add(new ContentNewCompanyRole("Modify", roleCompany.getRecruitmentRole().isModify()));
        recruitment.add(new ContentNewCompanyRole("Add", roleCompany.getRecruitmentRole().isCreate()));
        recruitment.add(new ContentNewCompanyRole("View", roleCompany.getRecruitmentRole().isView()));

        List<ContentNewCompanyRole> casestudy = new ArrayList<ContentNewCompanyRole>();
        casestudy.add(new ContentNewCompanyRole("Approve", roleCompany.getCaseStudyRole().isApprove()));
        casestudy.add(new ContentNewCompanyRole("Delete", roleCompany.getCaseStudyRole().isDel()));
        casestudy.add(new ContentNewCompanyRole("Modify", roleCompany.getCaseStudyRole().isModify()));
        casestudy.add(new ContentNewCompanyRole("Add", roleCompany.getCaseStudyRole().isCreate()));
        casestudy.add(new ContentNewCompanyRole("View", roleCompany.getCaseStudyRole().isView()));

        List<ContentNewCompanyRole> estate = new ArrayList<ContentNewCompanyRole>();
        estate.add(new ContentNewCompanyRole("Delete Estate item", roleCompany.getEstateRole().isDelEstateItem()));
        estate.add(new ContentNewCompanyRole("Modify Estate item", roleCompany.getEstateRole().isModifyEstateItem()));
        estate.add(new ContentNewCompanyRole("Add Estate item", roleCompany.getEstateRole().isAddEstateItem()));
        estate.add(new ContentNewCompanyRole("Delete", roleCompany.getEstateRole().isDel()));
        estate.add(new ContentNewCompanyRole("Modify", roleCompany.getEstateRole().isModify()));
        estate.add(new ContentNewCompanyRole("Add", roleCompany.getEstateRole().isCreate()));
        estate.add(new ContentNewCompanyRole("View", roleCompany.getEstateRole().isView()));

        List<ContentNewCompanyRole> repayment = new ArrayList<ContentNewCompanyRole>();
        repayment.add(new ContentNewCompanyRole("Approve", false));
        repayment.add(new ContentNewCompanyRole("Delete", false));
        repayment.add(new ContentNewCompanyRole("Modify", false));
        repayment.add(new ContentNewCompanyRole("Add", false));
        repayment.add(new ContentNewCompanyRole("View", false));

        List<ContentNewCompanyRole> report = new ArrayList<ContentNewCompanyRole>();
        report.add(new ContentNewCompanyRole("Approve", roleCompany.getReportRole().isApprove()));
        report.add(new ContentNewCompanyRole("Delete", roleCompany.getReportRole().isDel()));
        report.add(new ContentNewCompanyRole("Modify", roleCompany.getReportRole().isModify()));
        report.add(new ContentNewCompanyRole("Add", roleCompany.getReportRole().isCreate()));
        report.add(new ContentNewCompanyRole("View", roleCompany.getReportRole().isView()));

        List<ContentNewCompanyRole> meeting = new ArrayList<ContentNewCompanyRole>();
        meeting.add(new ContentNewCompanyRole("Delete", roleCompany.getMeetingRole().isDel()));
        meeting.add(new ContentNewCompanyRole("Modify", roleCompany.getMeetingRole().isModify()));
        meeting.add(new ContentNewCompanyRole("Add", roleCompany.getMeetingRole().isCreate()));
        meeting.add(new ContentNewCompanyRole("View", roleCompany.getMeetingRole().isView()));

        List<ContentNewCompanyRole> roombooking = new ArrayList<ContentNewCompanyRole>();
        roombooking.add(new ContentNewCompanyRole("Delete", roleCompany.getRoomRole().isDel()));
        roombooking.add(new ContentNewCompanyRole("Modify", roleCompany.getRoomRole().isModify()));
        roombooking.add(new ContentNewCompanyRole("Add", roleCompany.getRoomRole().isCreate()));
        roombooking.add(new ContentNewCompanyRole("View", roleCompany.getRoomRole().isView()));

        List<ContentNewCompanyRole> department = new ArrayList<ContentNewCompanyRole>();
        department.add(new ContentNewCompanyRole("Add", roleCompany.getDepartmentRole().isCreate()));
        department.add(new ContentNewCompanyRole("Delete", roleCompany.getDepartmentRole().isDel()));
        department.add(new ContentNewCompanyRole("Modify", roleCompany.getDepartmentRole().isModify()));
        department.add(new ContentNewCompanyRole("View", roleCompany.getDepartmentRole().isView()));

        List<ContentNewCompanyRole> level = new ArrayList<ContentNewCompanyRole>();
        level.add(new ContentNewCompanyRole("Add", roleCompany.getLevelRole().isCreate()));
        level.add(new ContentNewCompanyRole("Delete", roleCompany.getLevelRole().isDel()));
        level.add(new ContentNewCompanyRole("Modify", roleCompany.getLevelRole().isModify()));
        level.add(new ContentNewCompanyRole("View", roleCompany.getLevelRole().isView()));

        List<ContentNewCompanyRole> device = new ArrayList<ContentNewCompanyRole>();
        device.add(new ContentNewCompanyRole("Add", roleCompany.getDeviceRole().isCreate()));
        device.add(new ContentNewCompanyRole("Delete", roleCompany.getDeviceRole().isDel()));
        device.add(new ContentNewCompanyRole("Modify", roleCompany.getDeviceRole().isModify()));
        device.add(new ContentNewCompanyRole("View", roleCompany.getDeviceRole().isView()));

        List<ContentNewCompanyRole> companyRole = new ArrayList<ContentNewCompanyRole>();
        companyRole.add(new ContentNewCompanyRole("Add", roleCompany.getCompanyRole().isCreate()));
        companyRole.add(new ContentNewCompanyRole("Delete", roleCompany.getCompanyRole().isDel()));
        companyRole.add(new ContentNewCompanyRole("Modify", roleCompany.getCompanyRole().isModify()));
        companyRole.add(new ContentNewCompanyRole("View", roleCompany.getCompanyRole().isView()));

        List<ContentNewCompanyRole> projectRole = new ArrayList<ContentNewCompanyRole>();
        projectRole.add(new ContentNewCompanyRole("Add", roleCompany.getProjectRole().isCreate()));
        projectRole.add(new ContentNewCompanyRole("Delete", roleCompany.getProjectRole().isDel()));
        projectRole.add(new ContentNewCompanyRole("Modify", roleCompany.getProjectRole().isModify()));
        projectRole.add(new ContentNewCompanyRole("View", roleCompany.getProjectRole().isView()));

        List<ContentNewCompanyRole> payment = new ArrayList<ContentNewCompanyRole>();
        payment.add(new ContentNewCompanyRole("Delete", roleCompany.getPaymentRole().isDel()));
        payment.add(new ContentNewCompanyRole("Modify", roleCompany.getPaymentRole().isModify()));
        payment.add(new ContentNewCompanyRole("Add", roleCompany.getPaymentRole().isCreate()));
        payment.add(new ContentNewCompanyRole("View", roleCompany.getPaymentRole().isView()));

        List<ContentNewCompanyRole> quota = new ArrayList<ContentNewCompanyRole>();
        quota.add(new ContentNewCompanyRole("Modify", roleCompany.getQuotaRole().isModify()));
        quota.add(new ContentNewCompanyRole("View", roleCompany.getQuotaRole().isView()));

        List<ContentNewCompanyRole> workingAndHoliday = new ArrayList<ContentNewCompanyRole>();
        workingAndHoliday.add(new ContentNewCompanyRole("Add", roleCompany.getWorkingRole().isCreate()));
        workingAndHoliday.add(new ContentNewCompanyRole("Delete", roleCompany.getWorkingRole().isDel()));
        workingAndHoliday.add(new ContentNewCompanyRole("Modify", roleCompany.getWorkingRole().isModify()));
        workingAndHoliday.add(new ContentNewCompanyRole("View", roleCompany.getWorkingRole().isView()));

        List<ContentNewCompanyRole> member = new ArrayList<ContentNewCompanyRole>();
        member.add(new ContentNewCompanyRole("View User", roleCompany.getMemberRole().isViewUser()));
        member.add(new ContentNewCompanyRole("Delete", roleCompany.getMemberRole().isDel()));
        member.add(new ContentNewCompanyRole("Modify", roleCompany.getMemberRole().isModify()));
        member.add(new ContentNewCompanyRole("Add", roleCompany.getMemberRole().isAdd()));
        member.add(new ContentNewCompanyRole("View", roleCompany.getMemberRole().isView()));

        List<ContentNewCompanyRole> information = new ArrayList<ContentNewCompanyRole>();
        information.add(new ContentNewCompanyRole("Modify", roleCompany.getInfoRole().isModify()));
        information.add(new ContentNewCompanyRole("View", roleCompany.getInfoRole().isView()));

        List<ContentNewCompanyRole> feature = new ArrayList<ContentNewCompanyRole>();
        feature.add(new ContentNewCompanyRole("Modify", roleCompany.getFeatureRole().isModify()));
        feature.add(new ContentNewCompanyRole("View", roleCompany.getFeatureRole().isView()));

        listDataChild.put(listDataHeader.get(0), overtime);
        listDataChild.put(listDataHeader.get(1), calendar);
        listDataChild.put(listDataHeader.get(2), chat);
        listDataChild.put(listDataHeader.get(3), project);
        listDataChild.put(listDataHeader.get(4), salary);
        listDataChild.put(listDataHeader.get(5), accounting);
        listDataChild.put(listDataHeader.get(6), survey);
        listDataChild.put(listDataHeader.get(7), leaving);
        listDataChild.put(listDataHeader.get(8), checkin);
        listDataChild.put(listDataHeader.get(9), announcement);
        listDataChild.put(listDataHeader.get(10), recruitment);
        listDataChild.put(listDataHeader.get(11), casestudy);
        listDataChild.put(listDataHeader.get(12), estate);
        listDataChild.put(listDataHeader.get(13), repayment);
        listDataChild.put(listDataHeader.get(14), report);
        listDataChild.put(listDataHeader.get(15), meeting);
        listDataChild.put(listDataHeader.get(16), roombooking);
        listDataChild.put(listDataHeader.get(17), department);
        listDataChild.put(listDataHeader.get(18), level);
        listDataChild.put(listDataHeader.get(19), device);
        listDataChild.put(listDataHeader.get(20), companyRole);
        listDataChild.put(listDataHeader.get(21), projectRole);
        listDataChild.put(listDataHeader.get(22), payment);
        listDataChild.put(listDataHeader.get(23), quota);
        listDataChild.put(listDataHeader.get(24), workingAndHoliday);
        listDataChild.put(listDataHeader.get(25), member);
        listDataChild.put(listDataHeader.get(26), information);
        listDataChild.put(listDataHeader.get(27), feature);
    }

    private void updateViewMode() {
        switch (currentMode) {
            case MODE_DETAIL:
                topBar.setTextTitle(roleCompany.getRoleName());
                topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
                break;
            case MODE_ADD:
                topBar.setTextTitle(getString(R.string.new_company_role));
                topBar.setTextViewRight(getString(R.string.cancel));
                topBar.setTextViewLeft(getString(R.string.save));
                break;
        }
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

            }

        });

        adapter.setOnItemClickListener(new NewCompanyRoleAdapter.OnItemClickListener() {
            @Override
            public void onItemHeaderSelected(ImageView img) {
                if (img.getRotation() == 0) {
                    img.setRotation(180);

                } else {
                    img.setRotation(0);
                }
            }

            @Override
            public void onItemContentSelected() {
                Log.d("TAG", roleCompany.getChatRole().isAddMember().toString());
                Log.d("TAG", "moi bam vao item");
            }
        });
    }

    @Override
    public void onClick(View view) {

    }
}
