package com.horical.gito.mvp.signUp.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.signUp.injection.module.SignUpModule;
import com.horical.gito.mvp.signUp.view.SignUpActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = SignUpModule.class)
public interface SignUpComponent {
    void inject(SignUpActivity activity);
}
