package com.horical.gito.mvp.report.presenter;

import com.horical.gito.model.Reports;

import java.util.List;

public interface IReportPresenter {

    void getDataMember();

    void getMyReport();

    void getDataByDate(List<Reports> reports);

    void getDateByContent(List<Reports> reports);

    void setOnChooseMember(int position);

    void setDataDoingForByDate(int position, String text);

    void setDataProblemForByDate(int position, String text);

    void setDataFinishForByDate(int position, String text);

    void setDataRejectForByDate(int position, String text);

    void setStatusSubmittedReportByDate(int position);

    void setStatusApproveReportByDate(int position);

    void upDateReport(int position);

    void upDateReportStatus(int position);
}
