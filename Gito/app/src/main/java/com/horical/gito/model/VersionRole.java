package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nxon on 3/17/17
 */

public class VersionRole implements Serializable {

    @SerializedName("approve")
    @Expose
    private boolean approve;

    @SerializedName("canReleased")
    @Expose
    private boolean canReleased;

    @SerializedName("viewReleasedFile")
    @Expose
    private boolean viewReleasedFile;

    @SerializedName("add")
    private boolean add;

    @SerializedName("del")
    private boolean del;

    @SerializedName("view")
    private boolean view;

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isApprove() {
        return approve;
    }

    public void setApprove(boolean approve) {
        this.approve = approve;
    }

    public boolean isDel() {
        return del;
    }

    public void setDel(boolean del) {
        this.del = del;
    }

    public boolean isCanReleased() {
        return canReleased;
    }

    public void setCanReleased(boolean canReleased) {
        this.canReleased = canReleased;
    }

    public boolean isViewReleasedFile() {
        return viewReleasedFile;
    }

    public void setViewReleasedFile(boolean viewReleasedFile) {
        this.viewReleasedFile = viewReleasedFile;
    }

    public boolean isView() {
        return view;
    }

    public void setView(boolean view) {
        this.view = view;
    }
}
