package com.horical.gito.mvp.myProject.gantt.dto;

import com.horical.gito.base.IRecyclerItem;

public class GanttDayDto implements IRecyclerItem {

    public int month;
    public int startDay;
    public int endDay;

    @Override
    public int getItemViewType() {
        return 0;
    }
}
