package com.horical.gito.mvp.checkin.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.checkin.detail.view.Checkin2Activity;
import com.horical.gito.mvp.checkin.detail.injection.module.Checkin2Module;

import dagger.Component;

/**
 * Created by Penguin on 28-Dec-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = Checkin2Module.class)
public interface Checkin2Component  {
    void inject(Checkin2Activity activity);
}
