package com.horical.gito.mvp.salary.bonus.list.fragments.mine.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

public interface IListMineFragmentView extends IView {
    void requestGetMineInputAddonSuccess();

    void requestGetMineInputAddonError(RestError error);

    void showLoading();

    void hideLoading();
}
