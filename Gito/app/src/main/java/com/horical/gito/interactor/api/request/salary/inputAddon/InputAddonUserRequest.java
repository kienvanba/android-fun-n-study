package com.horical.gito.interactor.api.request.salary.inputAddon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InputAddonUserRequest implements Serializable {
    @SerializedName("userId")
    @Expose
    private String userId;

    @SerializedName("amount")
    @Expose
    private int amount;

    public InputAddonUserRequest(String userId, int amount) {
        this.userId = userId;
        this.amount = amount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
