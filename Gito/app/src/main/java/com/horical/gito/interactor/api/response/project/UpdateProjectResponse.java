package com.horical.gito.interactor.api.response.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Project;

public class UpdateProjectResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public Project project;
}
