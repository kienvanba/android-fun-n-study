package com.horical.gito.widget.button;

/**
 * Created by NXON on 10/28/16.
 */

public interface IButtonStyle {

    // === Blue button == //
    void setBlueStyle();

    // == White button == //
    void setWhiteStyle();

    // == Grey button == //
    void setGreyStyle(boolean corner);
}
