package com.horical.gito.mvp.companySetting.list.fragment.feature.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IFeatureView extends IView {

    void getFeatureSuccess();

    void getFeatureFailure(String err);

    void updateFeatureSuccess();

    void updateFeatureFailure(String err);
}
