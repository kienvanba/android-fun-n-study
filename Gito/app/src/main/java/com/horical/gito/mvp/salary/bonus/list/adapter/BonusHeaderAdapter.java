package com.horical.gito.mvp.salary.bonus.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.salary.dto.SalaryHeaderDto;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BonusHeaderAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<SalaryHeaderDto> items;
    private HeaderAdapterListener callback;
    private int mCurrentSelected = 0;

    public BonusHeaderAdapter(Context context, List<SalaryHeaderDto> items, HeaderAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    public void setSelectedTab(int position) {
        this.mCurrentSelected = position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_salary_list_header, parent, false);
        return new HeaderHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SalaryHeaderDto headerDto = items.get(position);
        HeaderHolder headerHolder = (HeaderHolder) holder;
        headerHolder.tvHeaderName.setSelected(mCurrentSelected == position);
        headerHolder.tvHeaderName.setText(headerDto.getName());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class HeaderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.tv_header_name)
        TextView tvHeaderName;

        HeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvHeaderName.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            callback.onSelected(getAdapterPosition());
        }
    }

    public interface HeaderAdapterListener {
        void onSelected(int position);
    }
}