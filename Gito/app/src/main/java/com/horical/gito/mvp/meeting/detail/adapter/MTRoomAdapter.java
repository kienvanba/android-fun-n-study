package com.horical.gito.mvp.meeting.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.model.Room;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MTRoomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Room> mListRoomMeeting;
    private OnItemClickListener callback;

    public MTRoomAdapter(Context context, List<Room> mListRoomMeeting, OnItemClickListener callback) {
        this.mListRoomMeeting = mListRoomMeeting;
        this.context = context;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_meeting_room, parent, false);
        return new RoomMeetingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Room room = mListRoomMeeting.get(position);
        RoomMeetingViewHolder viewHolder = (RoomMeetingViewHolder) holder;
        if (room != null) {
            viewHolder.mTvMeetingRoom.setText(room.getName());
        } else {
            viewHolder.itemView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mListRoomMeeting != null ? mListRoomMeeting.size() : 0;
    }

    public class RoomMeetingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.tv_add_meeting_detail_room)
        GitOTextView mTvMeetingRoom;

        @Bind(R.id.ic_delete_room)
        ImageView mBtnDeleteRoom;

        public RoomMeetingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == mBtnDeleteRoom.getId()) {
                callback.onMTRoomDelete(getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onMTRoomDelete(int position);
    }

}
