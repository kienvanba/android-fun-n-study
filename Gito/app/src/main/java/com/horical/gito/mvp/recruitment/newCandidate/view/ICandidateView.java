package com.horical.gito.mvp.recruitment.newCandidate.view;

import com.horical.gito.base.IView;


public interface ICandidateView extends IView {

    void showLoading();

    void dismissLoading();
}
