package com.horical.gito.mvp.myProject.setting.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.widget.chartview.line.TimeLineView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VersionHolder extends RecyclerView.ViewHolder{
    @Bind(R.id.edit_name)
    public TextView mTvName;
    @Bind(R.id.tv_status)
    public TextView mTvStatus;
    @Bind(R.id.btn_delete)
    public ImageButton mBtnDelete;
    @Bind(R.id.btn_edit_desc)
    public ImageButton mBtnEdit;
    @Bind(R.id.tv_description)
    public TextView mTvDescription;
    @Bind(R.id.time_line)
    public TimeLineView mTimeLine;

    public VersionHolder(View itemView){
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
