package com.horical.gito.mvp.salary.detail.metric.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.detail.metric.presenter.DetailMetricPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailMetricModule {
    @PerActivity
    @Provides
    DetailMetricPresenter provideReportPresenter(){
        return new DetailMetricPresenter();
    }
}
