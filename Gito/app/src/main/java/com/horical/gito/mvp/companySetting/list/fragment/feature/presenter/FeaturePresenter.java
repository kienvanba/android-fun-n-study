package com.horical.gito.mvp.companySetting.list.fragment.feature.presenter;

import com.horical.gito.R;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.companySetting.GetFeatureResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateFeatureResponse;
import com.horical.gito.model.Feature;
import com.horical.gito.mvp.companySetting.list.fragment.feature.dto.MenuFeature;
import com.horical.gito.mvp.companySetting.list.fragment.feature.view.IFeatureView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class FeaturePresenter extends BasePresenter implements IFeaturePresenter {
    private List<MenuFeature> menuFeatures = new ArrayList<>();
    private Feature feature = new Feature();
    public void attachView(IFeatureView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IFeatureView getView() {
        return (IFeatureView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<MenuFeature> getMenuFeatures() {
        return menuFeatures;
    }

    public void setMenuFeatures(List<MenuFeature> menuFeatures) {
        this.menuFeatures = menuFeatures;
    }

    public void getCurrentDataFeature(){
        feature.hfCalendar = menuFeatures.get(0).isSelected();
        feature.hfTodo = menuFeatures.get(1).isSelected();
        feature.hfMeeting = menuFeatures.get(2).isSelected();
        feature.hfReport = menuFeatures.get(3).isSelected();
        feature.hfChat = menuFeatures.get(4).isSelected();
        feature.hfEstate = menuFeatures.get(5).isSelected();
        feature.hfCaseStudy = menuFeatures.get(6).isSelected();
        feature.hfRecruitment = menuFeatures.get(7).isSelected();
        feature.hfNotification = menuFeatures.get(8).isSelected();
        feature.hfCheckIn = menuFeatures.get(9).isSelected();
        feature.hfLeavingRegister = menuFeatures.get(10).isSelected();
        feature.hfSurvey = menuFeatures.get(11).isSelected();
        feature.hfAccounting = menuFeatures.get(12).isSelected();
        feature.hfSalary = menuFeatures.get(13).isSelected();
    }
    public void initData(Feature feature) {
        menuFeatures.add(new MenuFeature(
                "Calendar",
                R.drawable.ic_calendar,
                feature.hfCalendar));
        menuFeatures.add(new MenuFeature(
                "Todo",
                R.drawable.ic_todo_list,
                feature.hfTodo));
        menuFeatures.add(new MenuFeature(
                "Metting",
                R.drawable.ic_meeting_room,
                feature.hfMeeting));
        menuFeatures.add(new MenuFeature(
                "Report",
                R.drawable.ic_report,
                feature.hfReport));
        menuFeatures.add(new MenuFeature(
                "Chat",
                R.drawable.ic_chat,
                feature.hfChat));
        menuFeatures.add(new MenuFeature(
                "Estate",
                R.drawable.ic_estate,
                feature.hfEstate));
        menuFeatures.add(new MenuFeature(
                "Case Study",
                R.drawable.ic_case_study,
                feature.hfCaseStudy));
        menuFeatures.add(new MenuFeature(
                "Recruitment",
                R.drawable.ic_recruitment,
                feature.hfRecruitment));
        menuFeatures.add(new MenuFeature(
                "Announcement",
                R.drawable.ic_notification,
                feature.hfAnnouncement));
        menuFeatures.add(new MenuFeature(
                "Check in",
                R.drawable.ic_check_in,
                feature.hfCheckIn));
        menuFeatures.add(new MenuFeature(
                "Leaving Register",
                R.drawable.ic_leaving_register,
                feature.hfLeavingRegister));
        menuFeatures.add(new MenuFeature(
                "Survey",
                R.drawable.ic_survey,
                feature.hfSurvey));
        menuFeatures.add(new MenuFeature(
                "Accounting",
                R.drawable.ic_accounting,
                feature.hfAccounting));
        menuFeatures.add(new MenuFeature(
                "Salary",
                R.drawable.ic_salary,
                feature.hfSalary));
    }


    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public Feature getFeature() {
        return feature;
    }

    @Override
    public void getDataFeature() {
        getApiManager().getFeature(new ApiCallback<GetFeatureResponse>() {

            @Override
            public void success(GetFeatureResponse res) {
                initData(res.feature);
                setFeature(res.feature);
                getView().getFeatureSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getFeatureFailure(error.message);
            }
        });
    }

    @Override
    public void updateFeature(final Feature feature){
        getApiManager().updateFeature(feature, new ApiCallback<GetUpdateFeatureResponse>() {

            @Override
            public void success(GetUpdateFeatureResponse res) {
                initData(res.feature);
                setFeature(res.feature);
                getView().updateFeatureSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().updateFeatureFailure(error.message);
            }
        });
    }
}

