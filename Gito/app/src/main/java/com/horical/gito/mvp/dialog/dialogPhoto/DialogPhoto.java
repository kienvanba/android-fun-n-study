package com.horical.gito.mvp.dialog.dialogPhoto;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DialogPhoto extends Dialog implements View.OnClickListener {

//    @Bind(R.id.btn_view_photo)
//    TextView btnViewPhoto;
//
//    @Bind(R.id.btn_save_photo)
//    TextView btnSavePhoto;

    @Bind(R.id.btn_album_gallery)
    TextView btnAlbumGallery;

    @Bind(R.id.btn_take_photo)
    TextView btnTakePhoto;

    @Bind(R.id.btn_cancel)
    TextView btnCancel;

    private OnDialogEditPhotoClickListener mListener;

    public void setListener(OnDialogEditPhotoClickListener mListener) {
        this.mListener = mListener;
    }

    public DialogPhoto(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_edit_photo);
        init();

    }

    private void init() {
        ButterKnife.bind(this);

        if (this.getWindow() != null) {
            this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }

        setCanceledOnTouchOutside(true);

//        btnViewPhoto.setOnClickListener(this);
//        btnSavePhoto.setOnClickListener(this);
        btnAlbumGallery.setOnClickListener(this);
        btnTakePhoto.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.btn_view_photo:
//                mListener.viewPhoto();
//                break;

//            case R.id.btn_save_photo:
//                mListener.savePhoto();
//                break;
//
            case R.id.btn_album_gallery:
                mListener.albumGallery();
                break;

            case R.id.btn_take_photo:
                mListener.takePhoto();
                break;

            case R.id.btn_cancel:
                this.dismiss();
                break;

        }
    }

    public interface OnDialogEditPhotoClickListener {
//        void viewPhoto();
//
//        void savePhoto();

        void albumGallery();

        void takePhoto();
    }
}
