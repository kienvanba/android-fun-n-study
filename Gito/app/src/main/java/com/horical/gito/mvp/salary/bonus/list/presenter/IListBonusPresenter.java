package com.horical.gito.mvp.salary.bonus.list.presenter;

import com.horical.gito.mvp.salary.dto.SalaryHeaderDto;

import java.util.List;

public interface IListBonusPresenter {
    List<SalaryHeaderDto> getHeaders();
}
