package com.horical.gito.mvp.dialog.dialogTask;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.ItemFilter;
import com.horical.gito.mvp.dialog.dialogTask.adapter.FilterAdapter;
import com.horical.gito.mvp.popup.popupTask.PopupTask;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 3/7/17.
 */

public class DialogFilterTask extends Dialog implements View.OnClickListener {
    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.lv_assignee)
    ListView lvAssignee;

    @Bind(R.id.lv_type)
    ListView lvType;

    @Bind(R.id.lv_priority)
    ListView lvPriority;

    @Bind(R.id.lv_status)
    ListView lvStatus;

    @Bind(R.id.lv_version)
    ListView lvVersion;

    @Bind(R.id.lv_component)
    ListView lvComponent;

    @Bind(R.id.tv_filter_sortby)
    TextView tvSortBy;

    private String keyFilterSortBy;
    private FilterAdapter adapterType;
    private FilterAdapter adapterPriotity;
    private FilterAdapter adapterStatus;
    private FilterAdapter adapterVersion;
    private FilterAdapter adapterComponent;

    public DialogFilterTask(Context context) {
        super(context, R.style.FullscreenDialog);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_filter_task);
        ButterKnife.bind(this);

        initData();
        initListener();
    }

    private void initData() {
        topBar.setTextTitle("Task Filter");
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        topBar.setTextViewRight("Done");

        keyFilterSortBy = "Date";
        tvSortBy.setText("Date");

        adapterType = new FilterAdapter(getContext(), 0);
        adapterPriotity = new FilterAdapter(getContext(), 0);
        adapterStatus = new FilterAdapter(getContext(), 0);
        adapterVersion = new FilterAdapter(getContext(), 0);
        adapterComponent = new FilterAdapter(getContext(), 0);

        lvType.setAdapter(adapterType);
        lvPriority.setAdapter(adapterPriotity);
        lvStatus.setAdapter(adapterStatus);
        lvVersion.setAdapter(adapterVersion);
        lvComponent.setAdapter(adapterComponent);

        adapterType.add(new ItemFilter("Task", false));
        adapterType.add(new ItemFilter("Bug", false));
        adapterType.add(new ItemFilter("FeedBack", false));
        adapterType.add(new ItemFilter("Question", false));
        adapterType.add(new ItemFilter("Inform", false));
        adapterType.add(new ItemFilter("Request", false));
        setListViewHeight(lvType, adapterType);

        adapterPriotity.add(new ItemFilter("Immediate", false));
        adapterPriotity.add(new ItemFilter("Urgent", false));
        adapterPriotity.add(new ItemFilter("High", false));
        adapterPriotity.add(new ItemFilter("Normal", false));
        adapterPriotity.add(new ItemFilter("Low", false));
        adapterPriotity.add(new ItemFilter("Next Phase", false));
        setListViewHeight(lvPriority, adapterPriotity);

        adapterStatus.add(new ItemFilter("Open", false));
        adapterStatus.add(new ItemFilter("Doing", false));
        adapterStatus.add(new ItemFilter("Done", false));
        adapterStatus.add(new ItemFilter("Closed", false));
        adapterStatus.add(new ItemFilter("Pending", false));
        adapterStatus.add(new ItemFilter("Rejected", false));
        setListViewHeight(lvStatus, adapterStatus);

        adapterVersion.add(new ItemFilter("Version 1", false));
        adapterVersion.add(new ItemFilter("Version 2", false));
        adapterVersion.add(new ItemFilter("Version 3", false));
        setListViewHeight(lvVersion, adapterVersion);

        adapterComponent.add(new ItemFilter("Component 1", false));
        adapterComponent.add(new ItemFilter("Component 2", false));
        adapterComponent.add(new ItemFilter("Component 3", false));
        setListViewHeight(lvComponent, adapterComponent);
    }

    private void initListener() {
        tvSortBy.setOnClickListener(this);


        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {

            @Override
            public void onLeftClicked() {

            }

            @Override
            public void onRightClicked() {

            }
        });

        lvType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("TAG", position + "");
            }
        });
        lvPriority.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getContext(), position + "", Toast.LENGTH_LONG).show();
            }
        });
        lvStatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getContext(), position + "", Toast.LENGTH_LONG).show();
            }
        });
        lvVersion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getContext(), position + "", Toast.LENGTH_LONG).show();
            }
        });
        lvComponent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getContext(), position + "", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_filter_sortby:
                PopupTask popupTask = new PopupTask(getContext(), new PopupTask.OnItemSelectedListener() {
                    @Override
                    public void onClickSelected(String codeFilter) {
                        tvSortBy.setText(codeFilter);
                        keyFilterSortBy = codeFilter;
                    }
                });
                popupTask.showSortBy(tvSortBy, keyFilterSortBy);
                break;
        }
    }

    public void setListViewHeight(ListView myListView, FilterAdapter adapter) {
        int totalHeight = 0;
        for (int size = 0; size < adapter.getCount(); size++) {
            View listItem = adapter.getView(size, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight + (myListView.getDividerHeight() * (adapter.getCount() - 1));
        myListView.setLayoutParams(params);
    }
}
