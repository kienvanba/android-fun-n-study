package com.horical.gito.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProjectResult implements Serializable{

    @SerializedName("projectDetail")
    @Expose
    private Project project;

    @SerializedName("roleProject")
    @Expose
    private ProjectRole roleProject;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public ProjectRole getRoleProject() {
        return roleProject;
    }

    public void setRoleProject(ProjectRole roleProject) {
        this.roleProject = roleProject;
    }
}
