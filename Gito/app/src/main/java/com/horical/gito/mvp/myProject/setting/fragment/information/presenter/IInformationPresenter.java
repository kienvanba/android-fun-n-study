package com.horical.gito.mvp.myProject.setting.fragment.information.presenter;

import com.horical.gito.model.Project;

public interface IInformationPresenter {

    Project getProject();
}
