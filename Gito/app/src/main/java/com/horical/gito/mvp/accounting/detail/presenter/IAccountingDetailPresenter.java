package com.horical.gito.mvp.accounting.detail.presenter;


public interface IAccountingDetailPresenter {
    void getAllAccountingDetail();
}
