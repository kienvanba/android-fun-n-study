package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.view.IUserView;

import java.util.List;

/**
 * Created by Tin on 21-Dec-16.
 */

public class UserPresenter extends BasePresenter implements IUserPresenter {

    private List<String> mList;
    public void attachView(IUserView view) {
        super.attachView(view);
    }

    public IUserView getView() {
        return (IUserView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<String> getListUser() {
        return mList;
    }

    public void setListUser(List<String> mList) {
        this.mList = mList;
    }
}
