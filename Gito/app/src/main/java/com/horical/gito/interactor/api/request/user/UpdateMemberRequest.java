package com.horical.gito.interactor.api.request.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kut3b on 11/04/2017.
 */

public class UpdateMemberRequest {
    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("phone")
    @Expose
    public String phone;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("displayName")
    @Expose
    public String displayName;

    @SerializedName("userId")
    @Expose
    public String userId;

    @SerializedName("level")
    @Expose
    public String level;

    @SerializedName("isAdmin")
    @Expose
    public  boolean isAdmin;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("roleCompany")
    @Expose
    public String roleCompany;

    @SerializedName("department")
    @Expose
    public String department;
}
