package com.horical.gito.mvp.leaving.main.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.utils.HDate;

import java.util.List;

/**
 * Created by Dragonoid on 12/6/2016.
 */

public class LeavingMonthDetailItem implements IRecyclerItem {
    @Override
    public int getItemViewType()  {
        return 0;
    }
    public String mStatus;
    public String mReason;
    public HDate mDate;
    public HDate mFromDate;
    public HDate mToDate;
    public List<IRecyclerItem> mApproverList;
    public LeavingMonthDetailItem (String Status,String Reason, HDate Date){
        mStatus = Status;
        mReason = Reason;
        mDate = Date;
    }
    public String getStringDateLeaving(){
        return mDate.day.toString() + "/" + mDate.month.toString() + "/" + mDate.year.toString();
    }
    public LeavingMonthDetailItem(){

    }
}
