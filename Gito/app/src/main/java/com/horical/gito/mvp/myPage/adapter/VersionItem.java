package com.horical.gito.mvp.myPage.adapter;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.Version;

public class VersionItem implements IRecyclerItem {

    private Version version;

    public VersionItem(Version version) {
        this.version = version;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    @Override
    public int getItemViewType() {
        return 0;
    }
}
