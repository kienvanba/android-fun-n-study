package com.horical.gito.mvp.companySetting.list.fragment.feature.presenter;

import com.horical.gito.model.Feature;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IFeaturePresenter {
    void getDataFeature();

    void updateFeature(Feature feature);
}
