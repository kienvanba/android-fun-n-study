package com.horical.gito.interactor.api.response.mypage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.MyPage;

public class GetMyPageResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public MyPage myPage;
}
