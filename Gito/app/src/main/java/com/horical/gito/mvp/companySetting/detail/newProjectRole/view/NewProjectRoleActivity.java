package com.horical.gito.mvp.companySetting.detail.newProjectRole.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.adapter.NewProjectRoleAdapter;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.dto.ContentNewCompanyRole;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.injection.component.DaggerNewProjectRoleComponent;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.injection.component.NewProjectRoleComponent;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.injection.module.NewProjectRoleModule;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.presenter.NewProjectRolePresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class NewProjectRoleActivity extends BaseActivity implements INewProjectRoleView, View.OnClickListener {
    public static final String TAG = makeLogTag(NewProjectRoleActivity.class);

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.lv_new_company_role)
    ExpandableListView lvCompanyRole;

    private NewProjectRoleAdapter adapter;
    private List<String> listDataHeader;
    private HashMap<String, List<ContentNewCompanyRole>> listDataChild;


    @Inject
    NewProjectRolePresenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_setting_new_project_role);
        ButterKnife.bind(this);

        NewProjectRoleComponent component = DaggerNewProjectRoleComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .newProjectRoleModule(new NewProjectRoleModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle("New Project Role");
        topBar.setTextViewLeft("Cancel");
        topBar.setTextViewRight("Save");

        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<String, List<ContentNewCompanyRole>>();

        // Adding child data
        listDataHeader.add("Calendar");
        listDataHeader.add("Chat");
        listDataHeader.add("Project");
        listDataHeader.add("Salary");
        // Adding child data
        List<ContentNewCompanyRole> calendar = new ArrayList<ContentNewCompanyRole>();
        calendar.add(new ContentNewCompanyRole(R.drawable.ic_view, "View", false));

        List<ContentNewCompanyRole> chat = new ArrayList<ContentNewCompanyRole>();
        chat.add(new ContentNewCompanyRole(R.drawable.ic_add_blue, "Add free room", false));
        chat.add(new ContentNewCompanyRole(R.drawable.ic_add_blue, "Create company room", false));
        chat.add(new ContentNewCompanyRole(R.drawable.ic_leave_room, "Leave room", false));
        chat.add(new ContentNewCompanyRole(R.drawable.ic_eject, "Eject member", false));
        chat.add(new ContentNewCompanyRole(R.drawable.ic_add_blue, "Add member", false));
        chat.add(new ContentNewCompanyRole(R.drawable.ic_view, "View", false));

        List<ContentNewCompanyRole> project = new ArrayList<ContentNewCompanyRole>();
        project.add(new ContentNewCompanyRole(R.drawable.ic_delete, "Delete", false));
        project.add(new ContentNewCompanyRole(R.drawable.ic_lock, "Lock", false));
        project.add(new ContentNewCompanyRole(R.drawable.ic_edit, "Modify", false));
        project.add(new ContentNewCompanyRole(R.drawable.ic_add_blue, "Add", false));
        project.add(new ContentNewCompanyRole(R.drawable.ic_view, "View", false));

        List<ContentNewCompanyRole> salary = new ArrayList<ContentNewCompanyRole>();
        salary.add(new ContentNewCompanyRole(R.drawable.ic_approve, "Approve", false));
        salary.add(new ContentNewCompanyRole(R.drawable.ic_delete, "Delete", false));
        salary.add(new ContentNewCompanyRole(R.drawable.ic_edit, "Modify", false));
        salary.add(new ContentNewCompanyRole(R.drawable.ic_add_blue, "Add", false));
        salary.add(new ContentNewCompanyRole(R.drawable.ic_view, "View", false));

        listDataChild.put(listDataHeader.get(0), calendar);
        listDataChild.put(listDataHeader.get(1), chat);
        listDataChild.put(listDataHeader.get(2), project);
        listDataChild.put(listDataHeader.get(3), salary);

        adapter = new NewProjectRoleAdapter(NewProjectRoleActivity.this, listDataHeader, listDataChild);
        lvCompanyRole.setAdapter(adapter);
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

            }
        });
        adapter = new NewProjectRoleAdapter(this, listDataHeader, listDataChild);
        lvCompanyRole.setAdapter(adapter);

        // Listview Group expanded listener
        lvCompanyRole.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                //Nhan lan 1
            }
        });

        // Listview Group collasped listener
        lvCompanyRole.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                //Nhan lan 2
            }
        });

        lvCompanyRole.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Toast.makeText(NewProjectRoleActivity.this, childPosition + "", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {

    }
}
