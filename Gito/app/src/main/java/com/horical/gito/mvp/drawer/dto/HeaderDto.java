package com.horical.gito.mvp.drawer.dto;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.TokenFile;

/**
 * Created by nxon on 3/15/17.
 */

public class HeaderDto implements IRecyclerItem {

    public String companyName;
    public TokenFile avatar;

    public HeaderDto(String companyName, TokenFile avatar) {
        this.companyName = companyName;
        this.avatar = avatar;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.MENU_HEADER;
    }
}
