package com.horical.gito.mvp.todolist.note.detail.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.caches.files.UserCaches;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.model.Note;
import com.horical.gito.model.wrapperModel.WrapperListUser;
import com.horical.gito.mvp.dialog.dialogMeeting.UploadAttachFileDialog;
import com.horical.gito.mvp.todolist.note.detail.adapter.AttachFileNoteAdapter;
import com.horical.gito.mvp.todolist.note.detail.injection.component.DaggerNoteDetailComponent;
import com.horical.gito.mvp.todolist.note.detail.injection.component.NoteDetailComponent;
import com.horical.gito.mvp.todolist.note.detail.injection.module.NoteDetailModule;
import com.horical.gito.mvp.todolist.note.detail.presenter.NoteDetailPresenter;
import com.horical.gito.utils.FileUtils;
import com.horical.gito.utils.NetworkUtils;
import com.horical.gito.widget.edittext.GitOEditText;
import com.horical.gito.widget.textview.GitOTextView;

import java.io.File;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;

import static com.horical.gito.mvp.meeting.detail.view.MeetingDetailActivity.REQUEST_CODE_USERS;
import static com.horical.gito.mvp.meeting.detail.view.MeetingDetailActivity.REQUEST_PERMISSION_READ_LIBRARY;
import static com.horical.gito.mvp.meeting.detail.view.MeetingDetailActivity.REQUEST_READ_LIBRARY;

/**
 * Created by Luong on 28-Mar-17.
 */

public class NoteDetailActivity extends BaseActivity implements View.OnClickListener, INoteDetailView, AttachFileNoteAdapter.OnItemClickListener {

    public static final String ARG_NOTE = "note";
    private WrapperListUser wrapperListUser;
    public static final int MODE_ADD = 1;
    public static final int MODE_DETAIL = 2;
    public static final int MODE_EDIT = 3;
    public int currentMode = MODE_DETAIL;

    private Note note;
    private File mUploadFile;

    AttachFileNoteAdapter mAttachFileNoteAdapter;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.imv_note_edit_title)
    ImageView ivTitle;
    @Bind(R.id.edt_note_title_detail)
    GitOEditText edtTitle;

    @Bind(R.id.imv_note_edit_content)
    ImageView ivContent;
    @Bind(R.id.edt_note_content_detail)
    GitOEditText edtContent;

    @Bind(R.id.tv_note_number_file)
    GitOTextView tvCountFile;
    @Bind(R.id.imv_note_detail_add)
    ImageView ivAdd;
    @Bind(R.id.rcv_note_detail_attach_file)
    RecyclerView rcvAttachFile;


    @Inject
    NoteDetailPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);
        ButterKnife.bind(this);
        NoteDetailComponent component = DaggerNoteDetailComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .noteDetailModule(new NoteDetailModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        note = (Note) getIntent().getSerializableExtra(ARG_NOTE);
        if (note != null) {
            currentMode = MODE_DETAIL;
            if (note.getAttachFiles() != null && note.getAttachFiles().size() > 0) {
                mPresenter.getAttachFiles().addAll(note.getAttachFiles());
            }
        } else {
            currentMode = MODE_ADD;
        }

        initData();
        initListener();
        updateViewMode();
        updateViewHeader();
    }

    protected void initData() {
        LinearLayoutManager fileAttachLlm = new LinearLayoutManager(this);
        fileAttachLlm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvAttachFile.setLayoutManager(fileAttachLlm);
        rcvAttachFile.setAdapter(new AttachFileNoteAdapter(this, mPresenter.getAttachFiles(), this));

        if (note != null) {
            edtTitle.setText(note.getTitle());
            edtContent.setText(note.getDesc());
            rcvAttachFile.setAdapter(mAttachFileNoteAdapter);
        }
    }


    protected void initListener() {
        ivTitle.setOnClickListener(this);
        ivAdd.setOnClickListener(this);
        ivContent.setOnClickListener(this);

        if (currentMode == MODE_DETAIL) {
            topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {

                @Override
                public void onLeftClicked() {
                    onBackPressed();
                }

                @Override
                public void onRightButtonOneClicked() {
                    currentMode = MODE_EDIT;
                    updateViewMode();
                }

                @Override
                public void onRightButtonTwoClicked() {

                }

                @Override
                public void onRightButtonThreeClicked() {

                }
            });
        } else {
            topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
                @Override
                public void onLeftClicked() {
                    if (currentMode == MODE_ADD) {
                        onBackPressed();
                    } else {
                        if (currentMode == MODE_EDIT) {
                            currentMode = MODE_DETAIL;
                            updateViewMode();
                        }
                    }
                }

                @Override
                public void onRightClicked() {
                    if (currentMode == MODE_ADD) {
                        mPresenter.createNote(edtTitle.getText().toString(), edtContent.getText().toString());
                    } else if (currentMode == MODE_EDIT) {
                        mPresenter.updateNote(note.getId(), edtTitle.getText().toString(), edtContent.getText().toString());
                    }
                }
            });
        }
    }

    private void updateViewMode() {
        switch (currentMode) {
            case MODE_DETAIL:
                topBar.setTextTitle(getString(R.string.note_detail));
                topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);

                edtTitle.setEnabled(false);
                edtContent.setEnabled(false);
                ivAdd.setEnabled(false);
                break;
            case MODE_ADD:
                topBar.setTextTitle("New Note");
                topBar.setTextViewLeft("Cancel");
                topBar.setTextViewRight("Save");

                edtTitle.setEnabled(true);
                edtContent.setEnabled(true);
                ivAdd.setEnabled(true);
                break;

            case MODE_EDIT:
                topBar.setTextTitle("Note Edit");
                topBar.setTextViewLeft("Cancel");
                topBar.setTextViewRight("Save");

                edtTitle.setEnabled(true);
                edtContent.setEnabled(true);
                ivAdd.setEnabled(true);
                break;
        }
    }

    private void updateViewHeader() {
        tvCountFile.setText(getResources().getString(R.string.note_file_attach, mPresenter.getAttachFiles().size()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_note_edit_title:
                edtTitle.setEnabled(true);
                break;
            case R.id.imv_note_edit_content:
                edtContent.setEnabled(true);
                break;
            case R.id.imv_note_detail_add: {
                final UploadAttachFileDialog uploadAttachFileDialog = new UploadAttachFileDialog(this);
                uploadAttachFileDialog.setListener(new UploadAttachFileDialog.OnDialogEditFileClickListener() {
                    @Override
                    public void albumGallery() {
                        handleOpenAlBumGallery();
                        uploadAttachFileDialog.dismiss();
                    }

                    @Override
                    public void documentGallery() {
                        handleOpenDocumentGallery();
                        uploadAttachFileDialog.dismiss();
                    }
                });

                uploadAttachFileDialog.show();
                break;
            }
        }
    }

    private void handleOpenAlBumGallery() {
        String s[] = {android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(s)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, REQUEST_READ_LIBRARY);

        } else {
            ActivityCompat.requestPermissions(this, s, REQUEST_PERMISSION_READ_LIBRARY);
        }
    }

    private void handleOpenDocumentGallery() {
        String s[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(s)) {
            Intent intent = new Intent();
            intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, REQUEST_READ_LIBRARY);
        } else {
            ActivityCompat.requestPermissions(this, s, REQUEST_PERMISSION_READ_LIBRARY);
        }

    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void validateFailedTitleEmpty() {
        showErrorDialog(getString(R.string.note_title_empty));
    }

    @Override
    public void validateFailedContentEmpty() {
        showErrorDialog(getString(R.string.note_content_empty));
    }

    @Override
    public void createNoteSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void createNoteFailure(RestError error) {
        showRestErrorDialog(error);
    }

    @Override
    public void updateNoteSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void updateNoteFailure(RestError error) {
        showRestErrorDialog(error);
    }

    @Override
    public void uploadSuccess(String url) {
        UserCaches.readCaches(this);
    }

    @Override
    public void uploadFailure(String error) {
        showErrorDialog(error);
    }

    @Override
    public void onAttachFileNoteDelete(int position) {
        mPresenter.getAttachFiles().remove(position);
        rcvAttachFile.getAdapter().notifyItemRemoved(position);
        rcvAttachFile.getAdapter().notifyItemRangeChanged(position, mPresenter.getAttachFiles().size());
    }

    @Override
    public void onAttachFileNoteClick(int position) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_READ_LIBRARY) {
                System.out.println("File from library " + data.getData());
                if (!NetworkUtils.isConnected(this)) {
                    showNoNetworkErrorDialog();
                } else {
                    mUploadFile = FileUtils.convertUriToFile(this, data.getData());
                    mPresenter.uploadFile(
                            MediaType.parse(getContentResolver().getType(data.getData())),
                            mUploadFile);
                }
            }
        }
        if (requestCode == REQUEST_CODE_USERS) {
            wrapperListUser = (WrapperListUser) data.getSerializableExtra("LIST_USER");
        }
    }
}

