package com.horical.gito.mvp.myProject.task.details.fragment.logtime.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Huy on 4/14/2017.
 */

public class LogtimeAdapter extends RecyclerView.Adapter<LogtimeAdapter.LogtimeHolder>{
    private Context mContext;
    private List<String> mList;
    private OnItemClickListener mOnItemClickListener;

    public LogtimeAdapter(Context context, List<String> list) {
        mContext = context;
        mList = list;
    }

    public List<String> getList() {
        return mList;
    }

    public void setList(List<String> mList) {
        this.mList = mList;
    }

    @Override
    public LogtimeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_task_details_logtime, parent, false);
        return new LogtimeHolder(view);
    }

    @Override
    public void onBindViewHolder(LogtimeHolder holder, final int position) {
        //holder.tvName.setText(mList.get(position));

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

        }});
    }

@Override
public int getItemCount() {
        return mList != null ? mList.size() : 0;
        }

public class LogtimeHolder extends RecyclerView.ViewHolder {
    //@Bind(R.id.tv_shortname_user)
    //TextView tvShortName;
    View view;

    public LogtimeHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        view = itemView;
    }
}

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

public interface OnItemClickListener {
    void onItemClickListener(int position);
}
}
