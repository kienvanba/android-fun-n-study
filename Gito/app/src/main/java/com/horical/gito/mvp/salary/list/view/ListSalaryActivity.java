package com.horical.gito.mvp.salary.list.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.salary.bonus.list.view.ListBonusActivity;
import com.horical.gito.mvp.salary.list.adapter.SalaryHeaderAdapter;
import com.horical.gito.mvp.salary.list.adapter.ListSalaryPagerAdapter;
import com.horical.gito.mvp.salary.list.fragments.BaseListSalaryFragment;
import com.horical.gito.mvp.salary.list.fragments.subEmail.view.ListEmailFormFragment;
import com.horical.gito.mvp.salary.list.fragments.subMetricGroup.view.ListMetricGroupFragment;
import com.horical.gito.mvp.salary.list.fragments.subSalary.view.ListSalaryFragment;
import com.horical.gito.mvp.salary.list.fragments.subStaff.view.ListStaffFragment;
import com.horical.gito.mvp.salary.list.injection.component.DaggerIListSalaryComponent;
import com.horical.gito.mvp.salary.list.injection.component.IListSalaryComponent;
import com.horical.gito.mvp.salary.list.injection.module.ListSalaryModule;
import com.horical.gito.mvp.salary.list.presenter.ListSalaryPresenter;
import com.horical.gito.mvp.salary.mySalary.list.view.ListMySalaryActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class ListSalaryActivity extends DrawerActivity implements View.OnClickListener, IListSalaryView {

    @Bind(R.id.ln_menu)
    LinearLayout lnMenu;

    @Bind(R.id.tv_title)
    TextView tvTitle;

    @Bind(R.id.imv_bonus)
    ImageView imvBonus;

    @Bind(R.id.imv_my_salary)
    ImageView imvMySalary;

    @Bind(R.id.imv_new_salary)
    ImageView imvNewSalary;

    @Bind(R.id.rcv_header)
    RecyclerView rcvHeader;

    @Bind(R.id.vp_salary)
    ViewPager vpSalary;

    @Bind(R.id.indicator_salary)
    CircleIndicator indicator;

    @Inject
    ListSalaryPresenter mPresenter;

    private SalaryHeaderAdapter mHeaderAdapter;
    private ListSalaryPagerAdapter mPagerAdapter;
    private List<BaseListSalaryFragment> mFragments = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_list_salary;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_SALARY;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        IListSalaryComponent component = DaggerIListSalaryComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .listSalaryModule(new ListSalaryModule())
                .build();

        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    private void setFragments() {
        /* Salary */
        ListSalaryFragment salaryFragment = ListSalaryFragment.newInstance();

        /* Staff */
        ListStaffFragment staffFragment = ListStaffFragment.newInstance();

        /* Metric */
        ListMetricGroupFragment metricFragment = ListMetricGroupFragment.newInstance();

        /* Email Form */
        ListEmailFormFragment emailFormFragment = ListEmailFormFragment.newInstance();

        mFragments.add(salaryFragment);
        mFragments.add(staffFragment);
        mFragments.add(metricFragment);
        mFragments.add(emailFormFragment);
    }

    protected void initData() {
        //header
        setFragments();
        mHeaderAdapter = new SalaryHeaderAdapter(this, mPresenter.getHeaders(), new SalaryHeaderAdapter.HeaderAdapterListener() {
            @Override
            public void onSelected(int position) {
                mHeaderAdapter.setSelectedTab(position);
                mHeaderAdapter.notifyDataSetChanged();
                vpSalary.setCurrentItem(position);

            }
        });
        rcvHeader.setAdapter(mHeaderAdapter);
        rcvHeader.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        //pager
        mPagerAdapter = new ListSalaryPagerAdapter(getSupportFragmentManager(), mFragments);
        vpSalary.setAdapter(mPagerAdapter);
        indicator.setViewPager(vpSalary);
    }

    protected void initListener() {
        vpSalary.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mHeaderAdapter.setSelectedTab(position);
                mHeaderAdapter.notifyDataSetChanged();
                rcvHeader.smoothScrollToPosition(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        imvBonus.setOnClickListener(this);
        imvMySalary.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_bonus:
                Intent bonusIntent = new Intent(ListSalaryActivity.this, ListBonusActivity.class);
                startActivity(bonusIntent);
                break;

            case R.id.imv_my_salary:
                Intent mySalaryIntent = new Intent(ListSalaryActivity.this, ListMySalaryActivity.class);
                startActivity(mySalaryIntent);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}