package com.horical.gito.mvp.myProject.sourceCode.list.injection.module;

import com.horical.gito.mvp.myProject.sourceCode.list.presenter.SourceCodePresenter;
import com.horical.gito.injection.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 22-Nov-16.
 */

@Module
public class SourceCodeModule {
    @PerActivity
    @Provides
    SourceCodePresenter codePresenter() {
        return new SourceCodePresenter();
    }
}

