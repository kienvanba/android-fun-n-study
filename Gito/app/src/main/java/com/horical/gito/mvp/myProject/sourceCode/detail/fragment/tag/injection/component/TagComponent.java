package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.injection.moduel.TagModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.view.TagFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class , modules = TagModule.class)
public interface TagComponent {
    void inject(TagFragment fragment);
}
