package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by thanhle on 3/23/17.
 */

public class Holiday implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("createdDate")
    @Expose
    private Date createdDate;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("name")
    @Expose
    private String name;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
