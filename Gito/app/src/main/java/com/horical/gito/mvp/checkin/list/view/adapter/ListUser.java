package com.horical.gito.mvp.checkin.list.view.adapter;

import com.horical.gito.base.IRecyclerItem;

/**
 * Created by Penguin on 16-Dec-16.
 */

public class ListUser implements IRecyclerItem {
    public int Avatar;
    public String UserName;
    public String Major;
    public int Delete;
    
    public ListUser(int avatar, String username, String major, int delete)
    {
        this.Avatar = avatar;
        this.UserName = username;
        this.Major = major;
        this.Delete = delete;
    }
    @Override
    public int getItemViewType() {
        return 0;
    }
}
