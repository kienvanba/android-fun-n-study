package com.horical.gito.mvp.salary.bonus.detail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.salary.inputAddon.CreateInputAddonRequest;
import com.horical.gito.interactor.api.response.salary.bonus.GetCreateInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetDetailInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetUpdateInputAddonResponse;
import com.horical.gito.interactor.api.response.user.GetAllUserResponse;
import com.horical.gito.model.InputAddon;
import com.horical.gito.model.InputAddonUser;
import com.horical.gito.model.RoleCompany;
import com.horical.gito.model.User;
import com.horical.gito.mvp.salary.bonus.detail.view.IDetailBonusView;

import java.util.ArrayList;
import java.util.List;

public class DetailBonusPresenter extends BasePresenter implements IDetailBonusPresenter {

    public static final int STATUS_NEW = 0;
    public static final int STATUS_APPROVE = 1;
    public static final int STATUS_REJECT = 2;
    public static final int STATUS_SUBMIT = 3;

    public void attachView(IDetailBonusView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IDetailBonusView getView() {
        return (IDetailBonusView) getIView();
    }

    private InputAddon inputAddon;
    private InputAddon inputAddonDefault;

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public InputAddon getInputAddon() {
        if (inputAddon == null) {
            inputAddon = new InputAddon();
        }
        return inputAddon;
    }
    @Override
    public InputAddon getInputAddonDefault() {
        if (inputAddonDefault == null) {
            inputAddonDefault = new InputAddon();
        }
        return inputAddonDefault;
    }

    public void setInputAddonDefault(InputAddon inputAddonDefault) {
        this.inputAddonDefault = inputAddonDefault;
    }

    @Override
    public boolean isEditable() {
        return getInputAddon().getStatus() != STATUS_APPROVE;
    }

    private void setInputAddon(InputAddon inputAddon) {
        this.inputAddon = inputAddon;
    }

    @Override
    public void requestGetDetailInputAddon(String inputAddonId) {
        getApiManager().getInputAddonById(inputAddonId, new ApiCallback<GetDetailInputAddonResponse>() {
            @Override
            public void success(GetDetailInputAddonResponse res) {
                setInputAddon(res.getInputAddon());
                getView().requestGetDetailInputAddonSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().requestGetDetailInputAddonError(error);
            }
        });
    }

    @Override
    public void requestCreateInputAddon() {
        getApiManager().createInputAddon(new CreateInputAddonRequest(getInputAddon()), new ApiCallback<GetCreateInputAddonResponse>() {
            @Override
            public void success(GetCreateInputAddonResponse res) {
                setInputAddon(res.getInputAddon());
                getView().requestCreateInputAddonSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().requestCreateInputAddonError(error);
            }
        });
    }

    @Override
    public void requestUpdateInputAddon() {
        getApiManager().updateInputAddon(getInputAddon().get_id(), getInputAddon(), new ApiCallback<GetUpdateInputAddonResponse>() {
            @Override
            public void success(GetUpdateInputAddonResponse res) {
                getView().requestUpdateInputAddonSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().requestUpdateInputAddonError(error);
            }
        });
    }

    @Override
    public void requestGetAllUser() {
        getApiManager().getAllUser(new ApiCallback<GetAllUserResponse>() {
            @Override
            public void success(GetAllUserResponse res) {
                getView().requestGetAllUserSuccess(res.getUsers());
            }

            @Override
            public void failure(RestError error) {
                getView().requestGetAllUserError(error);
            }
        });
    }

    @Override
    public List<InputAddonUser> getInputAddonUsersFromUsers(List<User> selectedUsers) {
        List<InputAddonUser> addonUsers = new ArrayList<>();
        for (User selectedUser : selectedUsers) {
            addonUsers.add(getInputAddonUserFromUser(selectedUser));
        }
        return addonUsers;
    }

    public InputAddonUser getInputAddonUserFromUser(User user) {
        User userId = new User(user.get_id(), user.getDisplayName(), new RoleCompany(user.getRoleCompany().getRoleName()));
        return new InputAddonUser(userId, user.getAmount());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
