package com.horical.gito.interactor.api.request.caseStudy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by thanhle on 4/10/17.
 */

public class UpdateAttachFileRequest {

    @SerializedName("attachFiles")
    @Expose
    public List<String> attachFiles;
}
