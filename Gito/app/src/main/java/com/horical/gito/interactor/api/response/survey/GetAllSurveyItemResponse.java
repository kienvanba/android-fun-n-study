package com.horical.gito.interactor.api.response.survey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Question;

import java.util.List;

/**
 * Created by nhattruong251295 on 4/12/2017.
 */

public class GetAllSurveyItemResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    public List<Question> questionList;
}
