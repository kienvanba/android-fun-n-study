package com.horical.gito.mvp.leaving.newLeaving.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.leaving.CreateLeavingRequest;
import com.horical.gito.interactor.api.response.leaving.CreateLeavingResponse;
import com.horical.gito.model.User;
import com.horical.gito.mvp.leaving.newLeaving.approverAdapter.LeavingApproverItem;
import com.horical.gito.mvp.leaving.newLeaving.view.ILeavingNewView;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.utils.HDate;

import java.util.ArrayList;
import java.util.List;

public class LeavingNewPresenter extends BasePresenter implements ILeavingNewPresneter {
    private List<LeavingApproverItem> mListApproverDialog;
    private List<User> mListSelectedApprover;

    public LeavingNewPresenter() {
        mListApproverDialog = new ArrayList<>();
        mListSelectedApprover = new ArrayList<>();
        GenerateTestData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public ILeavingNewView getView(){
        return (ILeavingNewView) getIView();
    }


    public List<LeavingApproverItem> getListAdd() {
        return mListApproverDialog;
    }

    public List<User> getListSelected() {
        return mListSelectedApprover;
    }

    public void GenerateTestData() {
//        mListApproverDialog.add(new LeavingApproverItem("Person 1 Test Code 1 2 3 4", 0));
//        mListApproverDialog.add(new LeavingApproverItem("Person 2 Test Code 1 9 6 4", 1));
//        mListApproverDialog.add(new LeavingApproverItem("Person 3 Test Code 5 8 9 4", 2));
//        mListApproverDialog.add(new LeavingApproverItem("Person 4 Test Code 5 6 8 4", 3));
//        mListApproverDialog.add(new LeavingApproverItem("Person 5 Test Code 7 6 4 9", 4));
//        mListApproverDialog.add(new LeavingApproverItem("Person 6 Test Code 4 2 0 9", 5));
    }
    public void createLeaving(HDate fromDate, HDate toDate, String reason){
        CreateLeavingRequest request = new CreateLeavingRequest();
        request.setFromDate(DateUtils.convertStringToDate(fromDate.toDataString(),"yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        request.setToDate(DateUtils.convertStringToDate(toDate.toDataString(),"yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        request.setReason(reason);
        if(mListSelectedApprover != null && mListSelectedApprover.size()>0){
            request.setApproverListId(mListSelectedApprover);
        }
        getView().showLoading();
        getApiManager().createLeaving(request, new ApiCallback<CreateLeavingResponse>() {
            @Override
            public void success(CreateLeavingResponse response) {
                getView().hideLoading();
                getView().createLeavingSuccess();
            }
            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().createLevingFailure(error);
            }
        });

    }

}
