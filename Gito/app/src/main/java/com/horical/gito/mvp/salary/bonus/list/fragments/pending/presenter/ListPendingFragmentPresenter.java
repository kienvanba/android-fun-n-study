package com.horical.gito.mvp.salary.bonus.list.fragments.pending.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.salary.inputAddon.FilterInputAddonRequest;
import com.horical.gito.interactor.api.response.salary.bonus.GetFilterInputAddonResponse;
import com.horical.gito.model.InputAddon;
import com.horical.gito.mvp.salary.bonus.list.fragments.pending.view.IListPendingFragmentView;
import com.horical.gito.mvp.salary.dto.bonus.BonusListDto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListPendingFragmentPresenter extends BasePresenter implements IPendingFragmentPresenter {

    public void attachView(IListPendingFragmentView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IListPendingFragmentView getView() {
        return (IListPendingFragmentView) getIView();
    }

    private List<InputAddon> inputAddons;

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public List<InputAddon> getListInputAddon() {
        if (inputAddons == null) {
            inputAddons = new ArrayList<>();
        }
        return inputAddons;
    }

    @Override
    public void requestGetPendingInputAddon() {
        FilterInputAddonRequest filterInputAddonRequest = new FilterInputAddonRequest(false, true, false);
        getApiManager().filterInputAddon(filterInputAddonRequest, new ApiCallback<GetFilterInputAddonResponse>() {
            @Override
            public void success(GetFilterInputAddonResponse res) {
                getListInputAddon().clear();
                getListInputAddon().addAll(res.getAddons());
                getView().requestGetPendingInputAddonSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().requestGetPendingInputAddonError(error);
            }
        });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
