package com.horical.gito.interactor.api.request.TodoNote.Note;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Luong on 12-Apr-17.
 */

public class UpdateAttachFileNoteRequest {

    @SerializedName("attachFiles")
    @Expose
    public List<String> attachFiles;
}
