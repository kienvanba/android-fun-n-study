package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Meeting implements Serializable {

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("creatorId")
    @Expose
    public CreatorId creatorId;

    @SerializedName("meetingId")
    @Expose
    public Integer meetingId;

    @SerializedName("companyId")
    @Expose
    public String companyId;

    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("endDate")
    @Expose
    public Date endDate;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("room")
    @Expose
    public Room room;

    @SerializedName("members")
    @Expose
    public List<User> members;

    @SerializedName("attachFiles")
    @Expose
    public List<TokenFile> attachFiles;

    @SerializedName("desc")
    @Expose
    public String desc;

    @SerializedName("note")
    @Expose
    public List<Note> note;

    @SerializedName("createdDate")
    @Expose
    public String createdDate;

    public void setId(String id) {
        this.id = id;
    }

    public void setCreatorId(CreatorId creatorId) {
        this.creatorId = creatorId;
    }

    public void setMeetingId(Integer meetingId) {
        this.meetingId = meetingId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public void setAttachFiles(List<TokenFile> attachFiles) {
        this.attachFiles = attachFiles;
    }

    public void setNote(List<Note> note) {
        this.note = note;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getId() {
        return id;
    }

    public CreatorId getCreatorId() {
        return creatorId;
    }

    public Integer getMeetingId() {
        return meetingId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getName() {
        return name;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Room getRoom() {
        return room;
    }

    public List<User> getMembers() {
        return members;
    }

    public List<TokenFile> getAttachFiles() {
        return attachFiles;
    }

    public String getDesc() {
        return desc;
    }

    public List<Note> getNote() {
        return note;
    }

    public String getCreatedDate() {
        return createdDate;
    }
}


