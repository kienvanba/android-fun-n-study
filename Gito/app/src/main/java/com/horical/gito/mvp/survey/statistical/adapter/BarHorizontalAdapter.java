package com.horical.gito.mvp.survey.statistical.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.R;
import com.horical.gito.mvp.survey.statistical.model.CustomViewBar;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nhattruong251295 on 4/8/2017.
 */

public class BarHorizontalAdapter extends RecyclerView.Adapter<BarHorizontalAdapter.BarHorizontalHolder>{

    public static final int MAX_WIDTH = 300;

    private Context mContext;
    private List<Integer> mList;

    public BarHorizontalAdapter(Context context, List<Integer> list) {
        this.mContext = context;
        this.mList = list;
    }

    public int getMaxSelectedOption(){
        return Collections.max(mList);
    }

    @Override
    public BarHorizontalHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_bar_horizontal,parent,false);
        return new BarHorizontalHolder(view);
    }

    @Override
    public void onBindViewHolder(BarHorizontalHolder holder, int position) {
        int lengthBar = mList.get(position) * MAX_WIDTH / getMaxSelectedOption();
        holder.customViewBar.setMinimumWidth(lengthBar);
        holder.customViewBar.setColor(ContextCompat.getColor(mContext,R.color.app_color));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }


    public class BarHorizontalHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.view_bar_horizontal)
        CustomViewBar customViewBar;

        public BarHorizontalHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}

