package com.horical.gito.mvp.survey.newsurvey.presenter;

import android.util.Log;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyItemResponse;
import com.horical.gito.interactor.api.response.survey.GetSubmiterResponse;
import com.horical.gito.model.Question;
import com.horical.gito.model.User;
import com.horical.gito.mvp.survey.newsurvey.view.INewSurveyView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.makeLogTag;
/**
 * Created by thanhle on 11/12/16.
 */

public class NewSurveyPresenter extends BasePresenter implements INewSurveyPresenter {
    private static final String TAG = makeLogTag(NewSurveyPresenter.class);

    private List<User> mListUser;

    private List<Question> mListQuestion;

    public void attachView(INewSurveyView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public INewSurveyView getView() {
        return (INewSurveyView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        mListQuestion = new ArrayList<>();
        mListUser = new ArrayList<>();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<User> getListUser() {
        return mListUser;
    }

    public void setListUser(List<User> listUser) {
        this.mListUser = listUser;
    }

    public List<Question> getListQuestion(){ return mListQuestion;}

    public void setListQuestion(List<Question> listQuestion){ this.mListQuestion = listQuestion;}

    @Override
    public void getSubmiter() {
        getApiManager().getSubmiter(new ApiCallback<GetSubmiterResponse>() {
            @Override
            public void success(GetSubmiterResponse res) {
                if (mListUser != null) {
                    mListUser.clear();
                } else {
                    mListUser = new ArrayList<>();
                }
                if (res.userList !=null) mListUser.addAll(res.userList);
                //mListUser.addAll(res.userList);
                getView().getDataSuccess();

            }

            @Override
            public void failure(RestError error) {
                if(mListUser != null){
                    mListUser.clear();
                } else {
                    mListUser = new ArrayList<>();
                }
                getView().getDataFailure(error.message);
            }
        });
    }

    @Override
    public void getAllSurveyItem(){
        getApiManager().getAllSurveyItem("58ef22ea9002e70c22c24af0", new ApiCallback<GetAllSurveyItemResponse>() {
            @Override
            public void success(GetAllSurveyItemResponse res) {
                Log.i("TAG","gallsurveyitem success");
                if (mListQuestion != null) {
                    mListQuestion.clear();
                } else {
                    mListQuestion = new ArrayList<>();
                }
                if (res.questionList != null){
                    mListQuestion.addAll(res.questionList);
                }
                getView().getDataSuccess();
            }

            @Override
            public void failure(RestError error) {
                Log.i("TAG","getallsurveyitem failure");
                if(mListQuestion != null){
                    mListQuestion.clear();
                } else {
                    mListQuestion = new ArrayList<>();
                }
                getView().getDataFailure(error.message);
            }
        });
    }
}
