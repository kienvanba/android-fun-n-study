package com.horical.gito.interactor.api.response.report;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Group;

import java.util.ArrayList;
import java.util.List;


public class GroupResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    private List<Group> list;

    public GroupResponse(List<Group> list) {
        if (list == null) {
            list = new ArrayList<>();
        }
        this.list = list;
    }

    public List<Group> getList() {
        return list;
    }

    public void setList(List<Group> list) {
        this.list = list;
    }
}
