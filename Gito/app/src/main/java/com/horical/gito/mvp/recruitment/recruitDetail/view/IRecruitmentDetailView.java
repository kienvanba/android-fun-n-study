package com.horical.gito.mvp.recruitment.recruitDetail.view;

import com.horical.gito.base.IView;


public interface IRecruitmentDetailView extends IView {

    void showLoading();

    void dismissLoading();
}
