package com.horical.gito.interactor.api.response.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.MemberAndRole;
import com.horical.gito.model.User;

import java.util.List;

public class GetMemberResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<MemberAndRole> memberList;
}
