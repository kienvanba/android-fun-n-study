package com.horical.gito.mvp.salary.detail.metricGroup.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.detail.metricGroup.presenter.DetailMetricGroupPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailMetricGroupModule {
    @PerActivity
    @Provides
    DetailMetricGroupPresenter provideSalaryPresenter(){
        return new DetailMetricGroupPresenter();
    }
}
