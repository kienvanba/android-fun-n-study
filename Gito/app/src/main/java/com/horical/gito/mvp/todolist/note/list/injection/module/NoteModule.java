package com.horical.gito.mvp.todolist.note.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.todolist.note.list.presenter.NotePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Luong on 21-Mar-17.
 */

@Module
public class NoteModule  {
    @Provides
    @PerActivity
    NotePresenter provideNotePresenter(){
        return new NotePresenter();
    }
}