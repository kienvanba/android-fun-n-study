package com.horical.gito.caches.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;

import static com.horical.gito.utils.LogUtils.LOGI;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class ImageLoader {

    private static final String TAG = makeLogTag(ImageLoader.class);

    public static void display(Context context, String url, final ImageView imageView) {
        LOGI(TAG, "display() url: " + url);
        Glide.with(context).load(url).into(imageView);
    }

    public static void load(Context context, String url, final ImageView imageView, final ProgressBar progressBar) {

        if (url == null || url.isEmpty()) {
            if (progressBar != null)
                progressBar.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(R.mipmap.ic_launcher);
            return;
        }

        imageView.setVisibility(View.INVISIBLE);
        if (progressBar != null)
            progressBar.setVisibility(View.VISIBLE);

        String token = MainApplication.getAppComponent().getPreferManager().getToken();

        GlideUrl glideUrl = new GlideUrl(url, new LazyHeaders.Builder()
                .addHeader("token",
                        AppConstants.BEGIN_TOKEN
                                + token.substring(8, token.length() - 8)
                                + AppConstants.END_TOKEN)
                .build()
        );

        Glide.with(context)
                .load(glideUrl)
                .centerCrop()
                .error(ContextCompat.getDrawable(context, R.mipmap.ic_launcher))
                .crossFade()
                .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                        if (progressBar != null)
                            progressBar.setVisibility(View.GONE);
                        imageView.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        if (progressBar != null)
                            progressBar.setVisibility(View.GONE);
                        imageView.setVisibility(View.VISIBLE);
                        return false;
                    }
                })
                .into(imageView);
    }

    public static void load(Context context, String url, final ImageLoaderCallback callback) {
        LOGI(TAG, "load() url: " + url);
        Glide.with(context).load(url).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap bm, GlideAnimation<? super Bitmap> glideAnimation) {
                callback.onCompleted(bm);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                callback.onFailed(e);
            }

        });
    }

    public static void load(Context context, String url, int width, int height, final ImageLoaderCallback callback) {
        LOGI(TAG, "load() url: " + url);
        Glide.with(context).load(url).asBitmap().into(new SimpleTarget<Bitmap>(width, height) {
            @Override
            public void onResourceReady(Bitmap bm, GlideAnimation<? super Bitmap> glideAnimation) {
                callback.onCompleted(bm);
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                callback.onFailed(e);
            }

        });
    }

    public static Bitmap getRoundedCornerImage(Bitmap bitmap, int width, int height) {
        Bitmap output = Bitmap.createBitmap(width,
                height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, width, height);
        final RectF rectF = new RectF(rect);
        final float roundPx = 400;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;

    }

}
