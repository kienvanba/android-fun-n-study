package com.horical.gito.interactor.socket;

import com.horical.gito.BuildConfig;
import com.horical.gito.interactor.event.EventManager;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.horical.gito.utils.LogUtils.LOGI;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class SocketManager {

    private final String TAG = makeLogTag(SocketManager.class);

    private Socket mSocket;
    private EventManager mEventManager;

    public SocketManager(EventManager eventManager) {
        mEventManager = eventManager;
    }

    public void connect() {
        try {
            mSocket = null;
            mSocket = IO.socket(BuildConfig.SOCKET_HOST);
            mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectTimeout);
            mSocket.on(Socket.EVENT_CONNECT, onConnect);
            mSocket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        if (mSocket != null) {
            mSocket.disconnect();
            mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectTimeout);
            mSocket.off(Socket.EVENT_CONNECT, onConnect);
        }
    }

    public boolean isConnected() {
        return mSocket != null && mSocket.connected();
    }

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            LOGI(TAG, "Connect socket error");
        }
    };

    private Emitter.Listener onConnectTimeout = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            LOGI(TAG, "Connect socket timeout");
        }
    };

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            LOGI(TAG, "Connect socket success");
        }
    };

}
