package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.injection.module.BranchModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.view.BranchFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = BranchModule.class)
public interface BranchComponent {
    void inject(BranchFragment fragment);
}
