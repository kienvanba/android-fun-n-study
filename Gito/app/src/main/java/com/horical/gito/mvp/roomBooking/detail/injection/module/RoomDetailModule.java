package com.horical.gito.mvp.roomBooking.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.roomBooking.detail.presenter.RoomDetailPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lemon on 3/21/2017.
 */
@Module
public class RoomDetailModule {
    @PerActivity
    @Provides
    RoomDetailPresenter provideRoomDetailPresenter() {
        return new RoomDetailPresenter();
    }
}
