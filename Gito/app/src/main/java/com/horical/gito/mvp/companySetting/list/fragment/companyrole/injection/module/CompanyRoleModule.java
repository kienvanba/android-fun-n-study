package com.horical.gito.mvp.companySetting.list.fragment.companyrole.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.list.fragment.companyrole.presenter.CompanyRolePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class CompanyRoleModule {
    @PerFragment
    @Provides
    CompanyRolePresenter provideCompanyRolePresenter(){
        return new CompanyRolePresenter();
    }
}
