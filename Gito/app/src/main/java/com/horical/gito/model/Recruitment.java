package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Recruitment implements Serializable{
    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("creatorId")
    @Expose
    private String creatorId;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("recruitmentId")
    @Expose
    private String recruitmentId;

    @SerializedName("interviewDate")
    @Expose
    private Date interviewDate;

    @SerializedName("createdDate")
    @Expose(serialize = false, deserialize = true)
    private Date createdDate;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("skill")
    @Expose
    private Skill skill;

    @SerializedName("candidates")
    @Expose
    private List<Candidate> candidates;

    @SerializedName("interviewers")
    @Expose
    private List<InterViewer> interviewers;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getRecruitmentId() {
        return recruitmentId;
    }

    public void setRecruitmentId(String recruitmentId) {
        this.recruitmentId = recruitmentId;
    }

    public Date getInterviewDate() {
        return interviewDate;
    }

    public void setInterviewDate(Date interviewDate) {
        this.interviewDate = interviewDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public List<Candidate> getCandidates() {
        if (candidates == null) {
            candidates = new ArrayList<>();
        }
        return candidates;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public List<InterViewer> getInterviewers() {
        return interviewers;
    }

    public void setInterviewers(List<InterViewer> interviewers) {
        this.interviewers = interviewers;
    }
}
