package com.horical.gito.mvp.notification.newNotification.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.notification.newNotification.view.INotificationNewView;

/**
 * Created by Dragonoid on 3/24/2017.
 */

public class NotificationNewPresenter extends BasePresenter implements INotificationNewPresenter {

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public void attachView(INotificationNewView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }
}
