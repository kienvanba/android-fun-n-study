package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.view.IDetailView;

/**
 * Created by Tin on 23-Nov-16.
 */

public class CodeCommitDetailPresenter extends BasePresenter implements ICodeCommitDetailPresenter {
    public void attachView(IDetailView view) {
        super.attachView(view);
    }

    public IDetailView getView() {
        return (IDetailView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
