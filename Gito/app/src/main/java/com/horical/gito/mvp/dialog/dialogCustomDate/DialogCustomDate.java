package com.horical.gito.mvp.dialog.dialogCustomDate;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import com.aigestudio.wheelpicker.widgets.WheelDatePicker;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.interactor.prefer.PreferManager;

import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 2/28/17.
 */

public class DialogCustomDate extends Dialog implements View.OnClickListener {
    @Bind(R.id.view_start_date)
    WheelDatePicker viewStartDate;

    @Bind(R.id.view_end_date)
    WheelDatePicker viewEndDate;

    @Bind(R.id.tv_cancel)
    TextView tvCancel;

    @Bind(R.id.btn_save)
    Button btnSave;

    private String mStartDate = null;
    private String mEndDate = null;

    private Context mContext;

    @Inject
    PreferManager mPreferManager;

    public DialogCustomDate(Context context) {
        super(context);
        mContext = context;
    }

    public DialogCustomDate(Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
    }

    public void setDate(String startDate, String endDate) {
        mStartDate = startDate;
        mEndDate = endDate;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getWindow() != null) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.dialog_custom_date);
        ButterKnife.bind(this);
        MainApplication.getAppComponent().inject(this);

        initData();

        tvCancel.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    private void initData() {
        viewStartDate.setCyclic(true);
        viewStartDate.setSelectedItemTextColor(mContext.getResources().getColor(R.color.black));
        viewStartDate.setIndicator(true);
        viewStartDate.setCurtain(true);
        viewStartDate.setAtmospheric(true);

        viewEndDate.setCyclic(true);
        viewEndDate.setSelectedItemTextColor(mContext.getResources().getColor(R.color.black));
        viewEndDate.setIndicator(true);
        viewEndDate.setCurtain(true);
        viewEndDate.setAtmospheric(true);

        String[] startDateArr = null;
        if (mStartDate != null) {
            startDateArr = mStartDate.split("/");
        }
        if (startDateArr != null && startDateArr.length == 3) {
            viewStartDate.setSelectedYear(Integer.parseInt(startDateArr[2]));
            viewStartDate.setSelectedMonth(Integer.parseInt(startDateArr[0]));
            viewStartDate.setSelectedDay(Integer.parseInt(startDateArr[1]));
        }

        String[] endDateArr = null;
        if (mEndDate != null) {
            endDateArr = mEndDate.split("/");
        }
        if (endDateArr != null && endDateArr.length == 3) {
            viewEndDate.setSelectedYear(Integer.parseInt(endDateArr[2]));
            viewEndDate.setSelectedMonth(Integer.parseInt(endDateArr[0]));
            viewEndDate.setSelectedDay(Integer.parseInt(endDateArr[1]));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel: {
                dismiss();
                break;
            }
            case R.id.btn_save: {
                mStartDate = viewStartDate.getCurrentMonth() + "/" + viewStartDate.getCurrentDay() + "/" + viewStartDate.getCurrentYear();
                mEndDate = viewEndDate.getCurrentMonth() + "/" + viewEndDate.getCurrentDay() + "/" + viewEndDate.getCurrentYear();
                if (checkDateValid(mStartDate, mEndDate)) {
                    mListener.onClickSaveCustomDate(mStartDate, mEndDate);
                    dismiss();
                } else {
                    Toast.makeText(mContext, "Date invalid", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    private OnDialogClickListener mListener;

    public void setOnDialogClickListener(OnDialogClickListener listener) {
        mListener = listener;
    }

    public interface OnDialogClickListener {
        void onClickSaveCustomDate(String startDate, String endDate);
    }

    private boolean checkDateValid(String startDate, String endDate) {
        SimpleDateFormat format = new java.text.SimpleDateFormat("MM/dd/yyyy", Locale.US);
        try {
            Date startDateObj = format.parse(startDate);
            Date endDateObj = format.parse(endDate);
            return !(startDateObj.after(endDateObj));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}