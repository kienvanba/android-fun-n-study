package com.horical.gito.injection.module;

import com.horical.gito.interactor.event.EventManager;
import com.horical.gito.interactor.socket.SocketManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SocketModule {

    @Provides
    @Singleton
    SocketManager provideSocketManager(EventManager eventManager) {
        return new SocketManager(eventManager);
    }

}
