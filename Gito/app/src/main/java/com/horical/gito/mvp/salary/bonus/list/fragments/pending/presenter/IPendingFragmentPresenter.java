package com.horical.gito.mvp.salary.bonus.list.fragments.pending.presenter;

import com.horical.gito.model.InputAddon;
import com.horical.gito.mvp.salary.dto.bonus.BonusListDto;

import java.util.List;

public interface IPendingFragmentPresenter {
    List<InputAddon> getListInputAddon();

    void requestGetPendingInputAddon();
}
