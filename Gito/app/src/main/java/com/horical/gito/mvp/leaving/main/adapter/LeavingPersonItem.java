package com.horical.gito.mvp.leaving.main.adapter;

import com.horical.gito.base.IRecyclerItem;

/**
 * Created by Dragonoid on 2/10/2017.
 */

public class LeavingPersonItem implements IRecyclerItem {
    @Override
    public int getItemViewType() {
        return 0;
    }

    String mName;
    String mPosition;

    public LeavingPersonItem(String name, String position) {
        mName = name;
        mPosition = position;
    }
}
