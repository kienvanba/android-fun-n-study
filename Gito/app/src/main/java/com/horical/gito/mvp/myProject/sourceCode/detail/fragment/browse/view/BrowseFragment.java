package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.BaseSourceCodeFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.adapter.BrowseAdapter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.injection.component.BrowseComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.injection.component.DaggerBrowseComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.injection.module.BrowseModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.presenter.BrowsePresenter;
import com.horical.gito.mvp.myProject.sourceCode.detail.view.SourceCodeDetailActivity;
import com.horical.gito.utils.LogUtils;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BrowseFragment extends BaseSourceCodeFragment implements IBrowseView, View.OnClickListener {

    public static final String TAG = LogUtils.makeLogTag(BrowseFragment.class);

    @Bind(R.id.code_detail_browse_list)
    RecyclerView rcvBrowse;

    private BrowseAdapter adapterBrowse;

    @Inject
    BrowsePresenter mPresenter;

    public static BrowseFragment newInstance() {
        Bundle args = new Bundle();
        BrowseFragment fragment = new BrowseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_code_browse;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        BrowseComponent component = DaggerBrowseComponent.builder()
                .browseModule(new BrowseModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
//        adapterBrowse = new BrowseAdapter(getContext(), mPresenter.getListBrowse());
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
//        rcvBrowse.setLayoutManager(layoutManager);
//        rcvBrowse.setHasFixedSize(false);
//        rcvBrowse.setAdapter(adapterBrowse);
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
}
