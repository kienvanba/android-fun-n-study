package com.horical.gito.mvp.checkin.detail.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

/**
 * Created by Penguin on 28-Dec-16.
 */

public interface ICheckin2View extends IView {
    void showLoading();

    void hideLoading();

    void getAllCheckinSuccess();

    void getAllCheckinFailure(RestError error);


}