package com.horical.gito.mvp.companySetting.list.fragment.payment.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.list.fragment.payment.presenter.PaymentPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class PaymentModule {
    @PerFragment
    @Provides
    PaymentPresenter providePaymentPresenter(){
        return new PaymentPresenter();
    }
}
