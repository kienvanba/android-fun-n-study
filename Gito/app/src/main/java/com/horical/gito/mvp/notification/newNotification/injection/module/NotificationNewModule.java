package com.horical.gito.mvp.notification.newNotification.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.notification.newNotification.presenter.NotificationNewPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class NotificationNewModule {
    @PerActivity
    @Provides
    NotificationNewPresenter provideNotificationNewPresenter(){return new NotificationNewPresenter();}
}
