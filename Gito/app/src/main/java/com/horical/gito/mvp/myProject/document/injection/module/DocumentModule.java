package com.horical.gito.mvp.myProject.document.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.myProject.document.presenter.DocumentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DocumentModule {

    @Provides
    @PerActivity
    DocumentPresenter provideDocumentPresenter(){
        return new DocumentPresenter();
    }
}
