package com.horical.gito.mvp.roomBooking.newRoom.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.roomBooking.newRoom.presenter.NewRoomPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lemon on 3/27/2017.
 */

@Module
public class NewRoomModule {
    @PerActivity
    @Provides
    NewRoomPresenter provideNewRoomPresenter(){
        return new NewRoomPresenter();
    }
}
