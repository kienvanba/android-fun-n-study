package com.horical.gito.interactor.api.request.report;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InFormToParam {


    public InFormToParam(List<String> userParam, List<String> group) {
        this.userParam = userParam;
        this.group = group;
    }

    @SerializedName("user")
    @Expose
    private List<String> userParam;

    @SerializedName("group")
    @Expose
    private List<String> group;
}
