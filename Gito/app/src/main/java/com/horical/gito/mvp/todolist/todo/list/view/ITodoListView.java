package com.horical.gito.mvp.todolist.todo.list.view;

import com.horical.gito.base.IView;

/**
 * Created by Luong on 14-Mar-17.
 */

public interface ITodoListView extends IView {

    void showLoading();

    void hideLoading();

    void getAllTodoListSuccess();

    void getAllTodoListFailure(String error);

    void deleteTodoListSuccess(int position);

    void deleteTodoListFailure();
}

