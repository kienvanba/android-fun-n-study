package com.horical.gito.mvp.leaving.newLeaving.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

/**
 * Created by Dragonoid on 12/9/2016.
 */

public interface ILeavingNewView extends IView {
    void showLoading();
    void hideLoading();
    void createLeavingSuccess();
    void createLevingFailure(RestError error);
}
