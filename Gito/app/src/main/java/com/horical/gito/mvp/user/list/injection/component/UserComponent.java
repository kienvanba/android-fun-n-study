package com.horical.gito.mvp.user.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.user.list.injection.module.UserModule;
import com.horical.gito.mvp.user.list.view.UserActivity;

import dagger.Component;

/**
 * Created by Tin on 15-Dec-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class , modules = UserModule.class)
public interface UserComponent {
    void inject(UserActivity activity);
}

