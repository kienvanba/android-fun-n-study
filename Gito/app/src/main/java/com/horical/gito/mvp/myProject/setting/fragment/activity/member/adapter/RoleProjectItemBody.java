package com.horical.gito.mvp.myProject.setting.fragment.activity.member.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.RoleProject;

public class RoleProjectItemBody implements IRecyclerItem{
    private RoleProject roleProject;

    public RoleProjectItemBody(RoleProject roleProject){
        this.roleProject = roleProject;
    }

    public RoleProject getRoleProject() {
        return roleProject;
    }

    public void setRoleProject(RoleProject roleProject) {
        this.roleProject = roleProject;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.ROLE_ITEM;
    }
}
