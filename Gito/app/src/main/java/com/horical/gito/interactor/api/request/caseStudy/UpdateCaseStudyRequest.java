package com.horical.gito.interactor.api.request.caseStudy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/3/17.
 */

public class UpdateCaseStudyRequest {

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("desc")
    @Expose
    public String desc;

    @SerializedName("status")
    @Expose
    public int status;
}
