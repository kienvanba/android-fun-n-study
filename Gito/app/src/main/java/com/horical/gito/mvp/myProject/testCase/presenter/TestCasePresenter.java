package com.horical.gito.mvp.myProject.testCase.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.testCase.view.ITestCaseView;

public class TestCasePresenter extends BasePresenter implements ITestCasePresenter {

    public void attachView(ITestCaseView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public ITestCaseView getView() {
        return (ITestCaseView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

}
