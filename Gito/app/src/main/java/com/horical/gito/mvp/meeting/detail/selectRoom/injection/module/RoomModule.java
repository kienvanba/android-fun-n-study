package com.horical.gito.mvp.meeting.detail.selectRoom.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.meeting.detail.selectRoom.presenter.RoomPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lemon on 4/13/2017.
 */
@Module
public class RoomModule {
    @PerActivity
    @Provides
    RoomPresenter provideRoomPresenter(){
        return new RoomPresenter();
    }
}
