package com.horical.gito.mvp.caseStudy.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.caseStudy.detail.injection.module.CaseStudyDetailModule;
import com.horical.gito.mvp.caseStudy.detail.view.CaseStudyDetailActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = CaseStudyDetailModule.class)
public interface CaseStudyDetailComponent {
    void inject(CaseStudyDetailActivity activity);
}
