package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.mvp.report.view.ReportActivity;

import java.util.ArrayList;
import java.util.List;


public class ResultsReport {

    @Expose(serialize = false, deserialize = false)
    private boolean selected;

    @SerializedName("_id")
    @Expose
    private DataUser dataUser;

    @SerializedName("listReport")
    @Expose
    private List<Reports> reports;

    public DataUser getDataUser() {
        return dataUser;
    }

    public void setDataUser(DataUser dataUser) {
        this.dataUser = dataUser;
    }

    public List<Reports> getReports() {
        if (reports == null) {
            reports = new ArrayList<>();
        }
        return reports;
    }

    public void setReports(List<Reports> reports) {
        this.reports = reports;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSubmitted() {
        for (int i = 0; i < getReports().size(); i++) {
            if (getReports().get(i).getStatus() == ReportActivity.STATUS_SUBMITTED) {
                return true;
            }
        }
        return false;
    }
}
