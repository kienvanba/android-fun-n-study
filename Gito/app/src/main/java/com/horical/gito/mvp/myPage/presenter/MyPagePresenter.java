package com.horical.gito.mvp.myPage.presenter;

import android.content.Context;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.mypage.GetMyPageRequest;
import com.horical.gito.interactor.api.response.mypage.GetMyPageResponse;
import com.horical.gito.model.MyPage;
import com.horical.gito.model.Section;
import com.horical.gito.mvp.myPage.injection.module.MyPageModule;
import com.horical.gito.mvp.myPage.view.IMyPageView;
import com.horical.gito.mvp.popup.dateSelected.DateDto;
import com.horical.gito.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

public class MyPagePresenter extends BasePresenter implements IMyPagePresenter {

    private MyPage myPage;
    private DateDto currentDateFilter;

    private IMyPageView getView() {
        return (IMyPageView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public DateDto getCurrentDateFilter() {
        return currentDateFilter;
    }

    public void setCurrentDateFilter(DateDto currentDateFilter) {
        this.currentDateFilter = currentDateFilter;
    }

    @Override
    public void getData() {

        getView().showLoading();
        GetMyPageRequest request = new GetMyPageRequest(
                currentDateFilter.getStart(),
                currentDateFilter.getEnd()
        );

        getApiManager().getMyPage(request, new ApiCallback<GetMyPageResponse>() {
            @Override
            public void success(GetMyPageResponse res) {
                getView().hideLoading();
                myPage = res.myPage;
                getView().getMyPageSuccess(myPage);
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().getMyPageFailure(error.message);
            }
        });
    }

    public MyPage getMyPage() {
        return myPage;
    }
}
