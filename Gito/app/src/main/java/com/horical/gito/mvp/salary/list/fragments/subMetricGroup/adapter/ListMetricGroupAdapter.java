package com.horical.gito.mvp.salary.list.fragments.subMetricGroup.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.SalaryMetricGroup;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListMetricGroupAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<SalaryMetricGroup> items;
    private MetricAdapterListener callback;

    public ListMetricGroupAdapter(Context context, List<SalaryMetricGroup> items, MetricAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    public void setDataChanged(List<SalaryMetricGroup> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_metric_group, parent, false);
        return new MetricGroupHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SalaryMetricGroup metricGroup = items.get(position);
        MetricGroupHolder metricGroupHolder = (MetricGroupHolder) holder;
        metricGroupHolder.tvTitle.setText(metricGroup.getTitle());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class MetricGroupHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_title)
        TextView tvTitle;

        @Bind(R.id.imv_delete)
        ImageView imvDelete;

        View view;

        MetricGroupHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onSelected(getAdapterPosition());
                }
            });

            imvDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onRemove(getAdapterPosition());
                }
            });
        }
    }

    public interface MetricAdapterListener {
        void onSelected(int position);

        void onRemove(int position);
    }
}