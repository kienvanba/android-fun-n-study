package com.horical.gito.mvp.dialog.dialogSelectDate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.horical.gito.R;
import com.horical.gito.base.BaseDialogFragment;

import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;

@SuppressLint("ValidFragment")
public class SelectDateDialog extends BaseDialogFragment {

    @Bind(R.id.date_picker)
    DatePicker vDate;

    @Bind(R.id.time_picker)
    TimePicker vTime;

    @Bind(R.id.tv_cancel)
    TextView tvCancel;

    @Bind(R.id.tv_done)
    TextView tvDone;

    private Context context;
    private Date mDate;
    Calendar mCalendar;

    @Override
    protected int layoutID() {
        return R.layout.layout_dialog_select_date;
    }

    public SelectDateDialog(Context context, Date date, OnDoneClickListener callback) {
        this.context = context;
        this.mDate = date;
        this.mCallBack = callback;
    }

    @Override
    protected void initData() {
        mCalendar = Calendar.getInstance();
        if (mDate != null) {
            mCalendar.setTime(mDate);
        }

        vDate.init(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int day) {
                mCalendar.set(year, month, day, vTime.getCurrentHour(), vTime.getCurrentMinute());
            }
        });

        vTime.setCurrentHour(mCalendar.get(Calendar.HOUR));
        vTime.setCurrentMinute(mCalendar.get(Calendar.MINUTE));
        vTime.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hour, int minute) {
                mCalendar.set(vDate.getYear(), vDate.getMonth(), vDate.getDayOfMonth(), hour, minute);
            }
        });



    }

    @Override
    protected void initListener() {

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallBack != null) {
                    mCallBack.onDone(mCalendar.getTime());
                }
                dismiss();
            }
        });
    }

    private OnDoneClickListener mCallBack;

    public interface OnDoneClickListener {
        void onDone(Date date);
    }
}
