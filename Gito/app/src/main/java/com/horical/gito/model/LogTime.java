package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class LogTime implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("taskId")
    @Expose
    private Task taskId;

    @SerializedName("creatorId")
    @Expose
    private User creatorId;

    @SerializedName("projectId")
    @Expose
    private Project projectId;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("hour")
    @Expose
    private int hour;

    @SerializedName("logTimeDate")
    @Expose
    private Date logTimeDate;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("total")
    @Expose
    private int total;

    public Task getTaskId() {
        return taskId;
    }

    public void setTaskId(Task taskId) {
        this.taskId = taskId;
    }

    public User getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(User creatorId) {
        this.creatorId = creatorId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Project getProjectId() {
        return projectId;
    }

    public void setProjectId(Project projectId) {
        this.projectId = projectId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public Date getLogTimeDate() {
        return logTimeDate;
    }

    public void setLogTimeDate(Date logTimeDate) {
        this.logTimeDate = logTimeDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
