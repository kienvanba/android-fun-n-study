package com.horical.gito.mvp.companySetting.list.fragment.companyrole.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.view.NewCompanyRoleActivity;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.companyrole.adapter.CompanyRoleAdapter;
import com.horical.gito.mvp.companySetting.list.fragment.companyrole.injection.component.CompanyRoleComponent;
import com.horical.gito.mvp.companySetting.list.fragment.companyrole.injection.component.DaggerCompanyRoleComponent;
import com.horical.gito.mvp.companySetting.list.fragment.companyrole.injection.module.CompanyRoleModule;
import com.horical.gito.mvp.companySetting.list.fragment.companyrole.presenter.CompanyRolePresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class CompanyRoleFragment extends BaseCompanyFragment implements ICompanyRoleView, View.OnClickListener {
    public static final String TAG = makeLogTag(CompanyRoleFragment.class);

    @Bind(R.id.refresh)
    SwipeRefreshLayout refresh;

    @Bind(R.id.rcv_company_role)
    RecyclerView rcvCompanyRole;

    private CompanyRoleAdapter adapterCompanyRole;

    @Inject
    CompanyRolePresenter mPresenter;

    public static CompanyRoleFragment newInstance() {
        Bundle args = new Bundle();
        CompanyRoleFragment fragment = new CompanyRoleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_company_setting_company_role;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        CompanyRoleComponent component = DaggerCompanyRoleComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .companyRoleModule(new CompanyRoleModule())
                .build();

        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        adapterCompanyRole = new CompanyRoleAdapter(getContext(), mPresenter.getListCompanyRole());
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvCompanyRole.setLayoutManager(llm);
        rcvCompanyRole.setAdapter(adapterCompanyRole);

        mPresenter.getAllCompanyRole();
    }

    @Override
    protected void initListener() {
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getAllCompanyRole();
            }
        });
        adapterCompanyRole.setOnItemClickListener(new CompanyRoleAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(int position) {
                Intent intent = new Intent(getContext(), NewCompanyRoleActivity.class);
                intent.putExtra(NewCompanyRoleActivity.COMPANY_ROLE, mPresenter.getListCompanyRole().get(position));
                startActivity(intent);
            }

            @Override
            public void onDelete(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.are_you_sure_for_delete_this,"company role"));
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mPresenter.deleteCompanyRole(mPresenter.getListCompanyRole().get(position).getId(), position);
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void getAllCompanyRoleSuccess() {
        refresh.setRefreshing(false);
        adapterCompanyRole.notifyDataSetChanged();
    }

    @Override
    public void getAllCompanyRoleFailure(String err) {
        refresh.setRefreshing(false);
        adapterCompanyRole.notifyDataSetChanged();
    }

    @Override
    public void deleteCompanyRoleSuccess(int position) {
        mPresenter.getListCompanyRole().remove(position);
        adapterCompanyRole.notifyDataSetChanged();
    }

    @Override
    public void deleteCompanyRoleFailure(String err) {
        adapterCompanyRole.notifyDataSetChanged();
    }
}
