package com.horical.gito.mvp.salary.bonus.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.InputAddonUser;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BonusDetailUserAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<InputAddonUser> items;
    private UserAdapterListener callback;
    private boolean isEditable;

    public BonusDetailUserAdapter(Context context, List<InputAddonUser> items, UserAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    public void setDataChanged(List<InputAddonUser> items) {
        this.items = items;
    }

    public void setEditable(boolean isEditable) {
        this.isEditable = isEditable;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_user, parent, false);
        return new InputAddonUserHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        InputAddonUser inputAddonUser = items.get(position);
        InputAddonUserHolder inputAddonUserHolder = (InputAddonUserHolder) holder;
        inputAddonUserHolder.tvName.setText(inputAddonUser.getUserId().getDisplayName());
        inputAddonUserHolder.tvRole.setText(inputAddonUser.getUserId().getRoleCompany().getRoleName());
        inputAddonUserHolder.tvAmount.setText(CommonUtils.amountCurrency(inputAddonUser.getAmount(), "usd"));
        if (isEditable) {
            inputAddonUserHolder.imvRemove.setVisibility(View.VISIBLE);
        } else {
            inputAddonUserHolder.imvRemove.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class InputAddonUserHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.tv_role)
        TextView tvRole;

        @Bind(R.id.tv_amount)
        TextView tvAmount;

        @Bind(R.id.imv_remove)
        ImageView imvRemove;

        InputAddonUserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imvRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onRemoveUser(getAdapterPosition());
                }
            });
        }
    }

    public interface UserAdapterListener {
        void onRemoveUser(int position);
    }
}
