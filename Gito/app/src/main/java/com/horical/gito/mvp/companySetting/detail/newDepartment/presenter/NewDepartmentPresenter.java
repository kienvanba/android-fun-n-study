package com.horical.gito.mvp.companySetting.detail.newDepartment.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.companySetting.CreateDepartmentRequest;
import com.horical.gito.interactor.api.response.companySetting.CreateDepartmentResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateDepartmentResponse;
import com.horical.gito.model.Department;
import com.horical.gito.mvp.companySetting.detail.newDepartment.view.INewDepartmentView;

/**
 * Created by Tin on 23-Nov-16.
 */

public class NewDepartmentPresenter extends BasePresenter implements INewDepartmentPresenter {

    public void attachView(INewDepartmentView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public INewDepartmentView getView() {
        return (INewDepartmentView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void updateDepartment(final Department department) {
        getApiManager().updateDepartment(department.get_id(), department, new ApiCallback<GetUpdateDepartmentResponse>() {
            @Override
            public void success(GetUpdateDepartmentResponse res) {
                getView().updateDepartmentSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().updateDepartmentFailure(error.message);
            }
        });
    }

    @Override
    public void createDepartment(Department department) {
        CreateDepartmentRequest createDepartmentRequest = new CreateDepartmentRequest();
        createDepartmentRequest.name = department.getName();
        createDepartmentRequest.desc = department.getDesc();
        getApiManager().createDepartment(createDepartmentRequest, new ApiCallback<CreateDepartmentResponse>() {
            @Override
            public void success(CreateDepartmentResponse res) {
                getView().createDepartmentSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().createDepartmentFailure(error.message);
            }
        });
    }
}

