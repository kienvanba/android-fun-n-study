package com.horical.gito.mvp.leaving.main.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.widget.recycleview.VerticalDividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 12/5/2016.
 */

public class LeavingMonthInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<LeavingMonthInfoItem> mList;
    Context mContext;

    public LeavingMonthInfoAdapter(List<LeavingMonthInfoItem> items, Context context) {
        mList = items;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leaving_month, parent, false);
        return new LeavingMonthInfoHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        LeavingMonthInfoHolder infoHolder = (LeavingMonthInfoHolder) holder;
        LeavingMonthInfoItem infoItem = (LeavingMonthInfoItem) mList.get(position);
        infoHolder.setListView(infoItem.mList, this.mContext);
        infoHolder.mMonthName.setText(infoItem.mMonthName);
        infoHolder.mHour.setText(""+infoItem.mHour );
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class LeavingMonthInfoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.leaving_lv_month)
        RecyclerView mListView;
        @Bind(R.id.leaving_tv_month_monthName)
        TextView mMonthName;
        @Bind(R.id.leaving_tv_month_hour)
        TextView mHour;
        @Bind(R.id.leaving_ic_month_extended)
        ImageView mExtend;

        LeavingMonthDetailAdapter mAdapter;
        Animation.AnimationListener mAnimationListener;

        public LeavingMonthInfoHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }

        public void setListView(List<IRecyclerItem> list, Context context) {
            mAdapter = new LeavingMonthDetailAdapter(list, context);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mListView.setAdapter(mAdapter);
            mListView.setLayoutManager(layoutManager);
            mListView.setHasFixedSize(true);
            mListView.addItemDecoration(new VerticalDividerItemDecoration(context, R.drawable.bg_leaving_divider));
            mListView.setVisibility(View.GONE);
            mExtend.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == mExtend.getId()) {
                if (mListView.getVisibility() == View.VISIBLE) {
                    mListView.setVisibility(View.GONE);
                    mExtend.startAnimation(RotateDown());

                } else if (mListView.getVisibility() == View.GONE) {
                    mListView.setVisibility(View.VISIBLE);
                    mExtend.startAnimation(RotateUp());
                }
            }
            // change Image View source
        }

        private Animation RotateUp() {
            Animation animation = new RotateAnimation(0f, 180f, RotateAnimation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            animation.setAnimationListener(mAnimationListener);
            animation.setFillAfter(true);
            animation.setDuration(250);
            return animation;
        }

        private Animation RotateDown() {
            Animation animation = new RotateAnimation(180f, 0f, RotateAnimation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            animation.setAnimationListener(mAnimationListener);
            animation.setFillAfter(true);
            animation.setDuration(250);
            return animation;
        }
    }

}
