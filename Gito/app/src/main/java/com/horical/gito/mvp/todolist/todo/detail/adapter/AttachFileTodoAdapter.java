package com.horical.gito.mvp.todolist.todo.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.model.TokenFile;
import com.horical.gito.mvp.meeting.detail.adapter.MTAttachFileAdapter;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Luong on 05-Apr-17.
 */

public class AttachFileTodoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<TokenFile> attachFiles;
    private AttachFileTodoAdapter.OnItemClickListener callback;

    public AttachFileTodoAdapter(Context context, List<TokenFile> attachFiles, AttachFileTodoAdapter.OnItemClickListener callback) {
        this.context = context;
        this.attachFiles = attachFiles;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_todo_content, parent, false);
        return new AttachFileTodoAdapter.AttachFileViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TokenFile attachFile = attachFiles.get(position);
        AttachFileTodoAdapter.AttachFileViewHolder viewHolder = (AttachFileTodoAdapter.AttachFileViewHolder) holder;
        viewHolder.tvContent.setText(attachFile.getName());


    }

    @Override
    public int getItemCount() {
        return attachFiles != null ? attachFiles.size() : 0;
    }

    public class AttachFileViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view;

        @Bind(R.id.cb_content)
        CheckBox cbContent;

        @Bind(R.id.tv_content)
        GitOTextView tvContent;

        @Bind(R.id.imv_content)
        ImageView ivContent;


        public AttachFileViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
            view.setOnClickListener(this);
            ivContent.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == view.getId()) {
                callback.onAttachFileTodoClick(getAdapterPosition());
            } else if (v.getId() == ivContent.getId()) {
                callback.onAttachFileTodoDelete(getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onAttachFileTodoDelete(int position);

        void onAttachFileTodoClick(int position);
    }
}
