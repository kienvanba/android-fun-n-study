package com.horical.gito.mvp.meeting.detail.selectRoom.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;

import com.horical.gito.R;
import com.horical.gito.model.Room;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.RoomHolder> {

    private Context mContext;
    private List<Room> mList;

    private OnItemClickListener listener;

    public RoomAdapter(Context mContext, List<Room> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }


    @Override
    public RoomHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_user_group, parent, false);
        return new RoomHolder(view);
    }

    @Override
    public void onBindViewHolder(final RoomHolder holder, final int position) {
        final Room room = mList.get(position);

        holder.tvName.setText(room.getName());
        holder.tvDepartment.setText(room.getDesc());

        holder.cbRoom.setChecked(room.isSelected());

        StringBuilder sort_name = new StringBuilder();
        String[] temp = room.getName().split(" ");
        for (String s : temp) {
            char[] arrChar = s.toCharArray();
            sort_name.append(arrChar[0]);
        }

        holder.tvNamePhoto.setText(sort_name);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelected = !room.isSelected();
                room.setSelected(isSelected);
                holder.cbRoom.setChecked(isSelected);
                listener.onItemClickListener(position);
            }
        });
    }


    public class RoomHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.task_imv_avatar)
        CircleImageView imvAvatar;

        @Bind(R.id.task_tv_name)
        GitOTextView tvName;

        @Bind(R.id.tv_department)
        GitOTextView tvDepartment;

        @Bind(R.id.task_pgr_avt_loading)
        ProgressBar pgrLoading;

        @Bind(R.id.tv_name_photo)
        GitOTextView tvNamePhoto;

        @Bind(R.id.cb_user)
        CheckBox cbRoom;

        View view;

        public RoomHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public boolean isAllRoomChecked() {

        for (Room room : mList) {
            if (!room.isSelected()) return false;
        }
        return true;
    }

    public interface OnItemClickListener{
        void onItemClickListener(int position);
    }

}
