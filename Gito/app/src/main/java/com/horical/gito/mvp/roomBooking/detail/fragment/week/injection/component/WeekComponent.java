package com.horical.gito.mvp.roomBooking.detail.fragment.week.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.roomBooking.detail.fragment.week.injection.module.WeekModule;
import com.horical.gito.mvp.roomBooking.detail.fragment.week.view.WeekFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = WeekModule.class)
public interface WeekComponent {
    void inject(WeekFragment fragment);
}
