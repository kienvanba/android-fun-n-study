package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nxon on 3/17/17
 */

public class ChatRole implements Serializable {

    @SerializedName("leaveRoom")
    @Expose
    private Boolean leaveRoom;

    @SerializedName("ejectMember")
    @Expose
    private Boolean ejectMember;

    @SerializedName("addMember")
    @Expose
    private Boolean addMember;

    @SerializedName("createFreeRoom")
    @Expose
    private Boolean createFreeRoom;

    @SerializedName("createCompanyRoom")
    @Expose
    private Boolean createCompanyRoom;

    @SerializedName("view")
    @Expose
    private Boolean view;

    public Boolean isLeaveRoom() {
        return leaveRoom;
    }

    public void setLeaveRoom(Boolean leaveRoom) {
        this.leaveRoom = leaveRoom;
    }

    public Boolean isEjectMember() {
        return ejectMember;
    }

    public void setEjectMember(Boolean ejectMember) {
        this.ejectMember = ejectMember;
    }

    public Boolean isAddMember() {
        return addMember;
    }

    public void setAddMember(Boolean addMember) {
        this.addMember = addMember;
    }

    public Boolean isCreateFreeRoom() {
        return createFreeRoom;
    }

    public void setCreateFreeRoom(Boolean createFreeRoom) {
        this.createFreeRoom = createFreeRoom;
    }

    public Boolean isCreateCompanyRoom() {
        return createCompanyRoom;
    }

    public void setCreateCompanyRoom(Boolean createCompanyRoom) {
        this.createCompanyRoom = createCompanyRoom;
    }

    public Boolean isView() {
        return view;
    }

    public void setView(Boolean view) {
        this.view = view;
    }
}

