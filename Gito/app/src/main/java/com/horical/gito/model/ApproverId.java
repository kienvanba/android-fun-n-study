package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ApproverId implements Serializable {
    @SerializedName("_id")
    @Expose
    String id;

    @SerializedName("displayName")
    @Expose
    String display;

    @SerializedName("avatar")
    @Expose
    TokenFile avatar;

    @SerializedName("level")
    @Expose
    Level level;
}
