package com.horical.gito.mvp.companySetting.detail.newDepartment.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.Department;
import com.horical.gito.mvp.companySetting.detail.newDepartment.adapter.NewDepartmentAdapter;
import com.horical.gito.mvp.companySetting.detail.newDepartment.injection.component.DaggerNewDepartmentComponent;
import com.horical.gito.mvp.companySetting.detail.newDepartment.injection.component.NewDepartmentComponent;
import com.horical.gito.mvp.companySetting.detail.newDepartment.injection.module.NewDepartmentModule;
import com.horical.gito.mvp.companySetting.detail.newDepartment.presenter.NewDepartmentPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class NewDepartmentActivity extends BaseActivity implements INewDepartmentView {
    public static final String TAG = makeLogTag(NewDepartmentActivity.class);
    public static final String NEW_DEPARTMENT = "NewDepartment";

    public static final int MODE_DETAIL = 1;
    public static final int MODE_ADD = 2;
    public int currentMode = MODE_DETAIL;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.edt_title)
    EditText edtTitle;

    @Bind(R.id.edt_des)
    EditText edtDes;

    @Bind(R.id.rcv_user)
    RecyclerView rcvUser;

    private Department department;
    private NewDepartmentAdapter adapter;

    @Inject
    NewDepartmentPresenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_setting_new_department);
        ButterKnife.bind(this);

        NewDepartmentComponent component = DaggerNewDepartmentComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .newDepartmentModule(new NewDepartmentModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();


        department = (Department) getIntent().getSerializableExtra(NEW_DEPARTMENT);

        if (department != null) {
            currentMode = MODE_DETAIL;
        } else {
            currentMode = MODE_ADD;
        }

        initData();
        updateViewMode();
        initListener();
    }

    protected void initData() {
        if (department != null) {
            edtTitle.setText(department.getName());
            edtDes.setText(department.getDesc());

            adapter = new NewDepartmentAdapter(NewDepartmentActivity.this, department.getMembers());
            LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            rcvUser.setLayoutManager(llm);
            rcvUser.setAdapter(adapter);
        } else {
            department = new Department();
            edtTitle.setText("");
            edtDes.setText("");
        }
    }

    private void updateViewMode() {
        switch (currentMode) {
            case MODE_DETAIL:
                topBar.setTextTitle(department.getName());
                break;
            case MODE_ADD:
                topBar.setTextTitle("New Department");
                break;
        }
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        topBar.setTextViewRight("Save");
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

            }
        });
    }

    public void getDataDepartment() {
        if (edtTitle.getText().toString().equalsIgnoreCase("")) {
            showErrorDialog("Please input department title");
            return;
        }
        department.setName(edtTitle.getText().toString());
        department.setDesc(edtDes.getText().toString());
        if (currentMode == MODE_ADD) {
            mPresenter.createDepartment(department);
        } else {
            mPresenter.updateDepartment(department);
        }
    }

    @Override
    public void createDepartmentSuccess() {
        Toast.makeText(NewDepartmentActivity.this, "Success", Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void createDepartmentFailure(String err) {
        Toast.makeText(NewDepartmentActivity.this, "Failure", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateDepartmentSuccess() {
        Toast.makeText(NewDepartmentActivity.this, "Success", Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void updateDepartmentFailure(String err) {
        Toast.makeText(NewDepartmentActivity.this, "Failure", Toast.LENGTH_SHORT).show();
    }
}
