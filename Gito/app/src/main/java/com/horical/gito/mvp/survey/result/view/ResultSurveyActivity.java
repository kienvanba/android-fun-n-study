package com.horical.gito.mvp.survey.result.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.survey.result.adapter.user.UserAdapter;
import com.horical.gito.mvp.survey.result.injection.component.DaggerResultSurveyComponent;
import com.horical.gito.mvp.survey.result.injection.component.ResultSurveyComponent;
import com.horical.gito.mvp.survey.result.injection.module.ResultSurveyModule;
import com.horical.gito.mvp.survey.result.presenter.ResultSurveyPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 11/12/16.
 */

public class ResultSurveyActivity extends BaseActivity implements IResultSurveyView, View.OnClickListener {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.ll_expand_user)
    LinearLayout llExpandUser;

    @Bind(R.id.ll_expand_evaluate_user)
    LinearLayout llExpandEvaluateUser;

    @Bind(R.id.iv_expand_user)
    ImageView ivExpandUser;

    @Bind(R.id.iv_expand_evaluate_user)
    ImageView ivExpandEvaluateUser;

    @Bind(R.id.rcv_user)
    RecyclerView mRcvUser;

    @Bind(R.id.rcv_evaluate_user)
    RecyclerView mRcvEvaluateUser;

    @Bind(R.id.rcv_list_question)
    RecyclerView mRcvQuestion;

    UserAdapter userAdapter;

    UserAdapter evaluateUserAdapter;

    Boolean expandedUser = true;

    Boolean expandedEvaluateUser = true;

    List<String> mList;

    @Inject
    ResultSurveyPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_survey);
        ButterKnife.bind(this);
        ResultSurveyComponent component = DaggerResultSurveyComponent.builder()
                .resultSurveyModule(new ResultSurveyModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(getString(R.string.result_survey));
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_SUMMARY_WHITE);

        mList = new ArrayList<>();
        mList.add("Jay");
        mList.add("Jeremy");
        mList.add("Jack");
        mList.add("Jackson");
        mList.add("John");
        mList.add("Jessica");
        mList.add("Jane");
        mList.add("June");

        userAdapter = new UserAdapter(this, mList);
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false);
        mRcvUser.setLayoutManager(llm);
        mRcvUser.setHasFixedSize(false);
        mRcvUser.setAdapter(userAdapter);
        mRcvUser.setVisibility(View.VISIBLE);

        evaluateUserAdapter = new UserAdapter(this, mList);
        LinearLayoutManager llmEvaluateUser = new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false);
        mRcvEvaluateUser.setLayoutManager(llmEvaluateUser);
        mRcvEvaluateUser.setHasFixedSize(false);
        mRcvEvaluateUser.setAdapter(evaluateUserAdapter);
        mRcvEvaluateUser.setVisibility(View.VISIBLE);
    }

    protected void initListener() {
        ivExpandUser.setOnClickListener(this);

        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightButtonOneClicked() {

            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_expand_user:
                if (expandedUser == true) {
                    mRcvUser.setVisibility(View.VISIBLE);
                    ivExpandUser.setRotation(90);
                    expandedUser = false;
                    break;
                } else {
                    mRcvUser.setVisibility(View.GONE);
                    ivExpandUser.setRotation(-90);
                    expandedUser = true;
                    break;
                }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}
