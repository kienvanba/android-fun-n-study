package com.horical.gito.mvp.drawer.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.horical.gito.AppConstants;
import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.model.Project;
import com.horical.gito.mvp.drawer.dto.BodyDto;
import com.horical.gito.mvp.drawer.dto.BodyHolder;
import com.horical.gito.mvp.drawer.dto.FooterDto;
import com.horical.gito.mvp.drawer.dto.FooterHolder;
import com.horical.gito.mvp.drawer.dto.HeaderDto;
import com.horical.gito.mvp.drawer.dto.HeaderHolder;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.utils.ConvertDpPx;
import com.horical.gito.utils.LogUtils;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

public class MenuAdapter extends BaseRecyclerAdapter {

    public static final String TAG = LogUtils.makeLogTag(MenuAdapter.class);

    private int currentPosition;

    public OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        super.setOnItemClickListener(onItemClickListener);

        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener extends BaseRecyclerAdapter.OnItemClickListener {

        void onLogoClicked();

        void onLogOutClicked();
    }

    public MenuAdapter(List<IRecyclerItem> items, Context context) {
        super(items, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case AppConstants.MENU_HEADER:
                view = LayoutInflater.from(mContext).inflate(R.layout.header_menu, parent, false);
                return new HeaderHolder(view);
            case AppConstants.MENU_FOOTER:
                view = LayoutInflater.from(mContext).inflate(R.layout.footer_menu, parent, false);
                return new FooterHolder(view);
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.body_menu, parent, false);
                return new BodyHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        switch (getItemViewType(position)) {
            case AppConstants.MENU_HEADER: {
                final HeaderHolder header = (HeaderHolder) holder;
                HeaderDto headerDto = (HeaderDto) mItems.get(position);

                String url = CommonUtils.getURL(headerDto.avatar);
                ImageLoader.load(mContext, url, header.mIvLogoCompany, header.mProgressBar);

                header.mTvNameCompany.setText(headerDto.companyName);
                header.mIvLogoCompany.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClickListener.onLogoClicked();
                    }
                });
                break;
            }
            case AppConstants.MENU_FOOTER: {
                final FooterHolder footer = (FooterHolder) holder;
                FooterDto footerDto = (FooterDto) mItems.get(position);

                String url = CommonUtils.getURL(footerDto.avatar);
                ImageLoader.load(mContext, url, footer.mIvAvatarUser, footer.mProgressBar);

                footer.mTvName.setText(footerDto.name);
                footer.mTvName.setSelected(true);
                footer.mTvEmail.setText(footerDto.email);
                footer.mTvEmail.setSelected(true);
                footer.mTvLogout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClickListener.onLogOutClicked();
                    }
                });
                break;
            }
            default: {
                BodyHolder body = (BodyHolder) holder;
                BodyDto bodyDto = (BodyDto) mItems.get(position);

                ConvertDpPx converter = new ConvertDpPx(mContext);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        converter.dpToPx(20),
                        converter.dpToPx(20));
                lp.setMargins(
                        converter.dpToPx(50),
                        0,
                        converter.dpToPx(16), 0);

                LinearLayout.LayoutParams lpNormal = new LinearLayout.LayoutParams(
                        converter.dpToPx(25),
                        converter.dpToPx(25));

                lpNormal.setMargins(
                        converter.dpToPx(20),
                        0,
                        converter.dpToPx(16), 0);

                body.mTvTitle.setText(bodyDto.title);
                body.mTvTitle.setCustomTextColor(GitOTextView.BLACK_WHITE);
                body.mIvIcon.setImageResource(bodyDto.icon);

                boolean isSelected = bodyDto.isSelected;
                body.mTvTitle.setSelected(isSelected);
                body.mContainer.setSelected(isSelected);

                if (isSelected) {
                    currentPosition = position;
                    body.mTvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                } else {
                    body.mTvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                }

                switch (bodyDto.key) {
                    case AppConstants.NAV_DRAWER_ID_MY_PROJECT: {
                        body.mIvIcon.setLayoutParams(lpNormal);
                        body.mContainer.setEnabled(false);
                        Project project = GitOStorage.getInstance().getProject();
                        if (project != null)
                            body.mTvTitle.setText(project.getName());
                        else
                            body.mTvTitle.setText(mContext.getString(R.string.my_project));
                        break;
                    }
                    case AppConstants.NAV_DRAWER_ID_SUMMARY:
                    case AppConstants.NAV_DRAWER_ID_TASK:
                    case AppConstants.NAV_DRAWER_ID_GANTT:
                    case AppConstants.NAV_DRAWER_ID_DOCUMENT:
                    case AppConstants.NAV_DRAWER_ID_SOURCE_CODE:
                    case AppConstants.NAV_DRAWER_ID_TEST_CASE:
                    case AppConstants.NAV_DRAWER_ID_CHANGE_LOG:
                    case AppConstants.NAV_DRAWER_ID_PROJECT_SETTING:
                    case AppConstants.NAV_DRAWER_ID_POST_API:
                    case AppConstants.NAV_DRAWER_ID_CHAT_APP:
                    case AppConstants.NAV_DRAWER_ID_LOG_TIME: {
                        body.mIvIcon.setLayoutParams(lp);
                        break;
                    }
                    default: {
                        body.mIvIcon.setLayoutParams(lpNormal);
                        body.mContainer.setEnabled(true);
                        break;
                    }
                }
                break;
            }
        }
    }

    public int getCurrentPosition() {
        return currentPosition;
    }
}
