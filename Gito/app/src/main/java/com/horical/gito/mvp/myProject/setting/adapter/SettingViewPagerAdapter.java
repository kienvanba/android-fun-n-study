package com.horical.gito.mvp.myProject.setting.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.horical.gito.mvp.myProject.setting.fragment.recycler.view.component.ComponentFragment;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.view.feature.FeatureFragment;
import com.horical.gito.mvp.myProject.setting.fragment.group.view.GroupFragment;
import com.horical.gito.mvp.myProject.setting.fragment.information.view.InformationFragment;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.view.member.MemberFragment;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.view.version.VersionFragment;

public class SettingViewPagerAdapter extends FragmentPagerAdapter {
    private Fragment mFragment;
    private Fragment currentFragment;

    public SettingViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public Fragment getCurrentFragment(){
        return this.currentFragment;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                mFragment = new InformationFragment();
                break;
            case 1:
                mFragment = new VersionFragment();
                break;
            case 2:
                mFragment = new FeatureFragment();
                break;
            case 3:
                mFragment = new MemberFragment();
                break;
            case 4:
                mFragment = new GroupFragment();
                break;
            case 5:
                mFragment = new ComponentFragment();
                break;
        }
        return mFragment;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        currentFragment = (Fragment) object;
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public int getCount() {
        return 6;
    }
}
