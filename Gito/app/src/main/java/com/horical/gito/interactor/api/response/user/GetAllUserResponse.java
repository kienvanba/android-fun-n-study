package com.horical.gito.interactor.api.response.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thanhle on 3/6/17.
 */

public class GetAllUserResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    private List<User> users;

    public List<User> getUsers() {
        if (users == null) {
            users = new ArrayList<>();
        }
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
