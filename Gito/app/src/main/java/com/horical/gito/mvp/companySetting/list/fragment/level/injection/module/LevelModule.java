package com.horical.gito.mvp.companySetting.list.fragment.level.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.list.fragment.level.presenter.LevelPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class LevelModule {
    @PerFragment
    @Provides
    LevelPresenter provideLevelPresenter(){
        return new LevelPresenter();
    }
}
