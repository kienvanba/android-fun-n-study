package com.horical.gito.mvp.salary.detail.salary.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailStaffAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<ListStaffDto> items;
    private DetailStaffAdapterListener callback;

    public DetailStaffAdapter(Context context, List<ListStaffDto> items, DetailStaffAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_detail_salary_staff, parent, false);
        return new DetailStaffHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListStaffDto staffDto = items.get(position);
        DetailStaffHolder staffHolder = (DetailStaffHolder) holder;

        staffHolder.tvName.setText(staffDto.getName());
        staffHolder.tvTotal.setText(CommonUtils.amountFormatEnGB(staffDto.getSalary(), "USD"));
        staffHolder.tvMajor.setText(staffDto.getMajor());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class DetailStaffHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.tv_total)
        TextView tvTotal;

        @Bind(R.id.tv_major)
        TextView tvMajor;

        DetailStaffHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface DetailStaffAdapterListener {
        void onSelected(int position);
    }
}