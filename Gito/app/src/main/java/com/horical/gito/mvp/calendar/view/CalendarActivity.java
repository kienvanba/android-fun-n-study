package com.horical.gito.mvp.calendar.view;

import android.os.Bundle;
import android.view.View;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.calendar.injection.component.CalendarComponent;
import com.horical.gito.mvp.calendar.injection.component.DaggerCalendarComponent;
import com.horical.gito.mvp.calendar.injection.module.CalendarModule;
import com.horical.gito.mvp.calendar.presenter.CalendarPresenter;
import com.horical.gito.mvp.drawer.DrawerActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.FROM_MENU;
import static com.horical.gito.AppConstants.KEY_FROM;

public class CalendarActivity extends DrawerActivity implements View.OnClickListener, ICalendarView {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Inject
    CalendarPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_calendar;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_CALENDAR;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        CalendarComponent component = DaggerCalendarComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .calendarModule(new CalendarModule())
                .build();

        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(getString(R.string.calendar));
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_FILTER);

        int from = getIntent().getIntExtra(KEY_FROM, FROM_MENU);
        mPresenter.setFrom(from);
        if (from == FROM_MENU) {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        } else {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
            lockDrawer();
        }
    }

    protected void initListener() {
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                if (mPresenter.getFrom() == FROM_MENU) openDrawer();
                else onBackPressed();
            }

            @Override
            public void onRightButtonOneClicked() {

            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}

