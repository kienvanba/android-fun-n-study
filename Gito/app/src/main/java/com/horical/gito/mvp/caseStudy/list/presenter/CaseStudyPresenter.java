package com.horical.gito.mvp.caseStudy.list.presenter;

import android.util.Log;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.caseStudy.GetAllCaseStudyRequest;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetAllCaseStudyResponse;
import com.horical.gito.model.CaseStudy;
import com.horical.gito.mvp.caseStudy.list.view.ICaseStudyView;
import com.horical.gito.mvp.popup.dateSelected.DateDto;
import com.horical.gito.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

public class CaseStudyPresenter extends BasePresenter implements ICaseStudyPresenter {
    public final static int MINE = 0;
    public final static int APPROVED = 1;
    public final static int PENDING = 2;
    private DateDto dateRange;
    private List<CaseStudy> mList;

    public void attachView(ICaseStudyView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public ICaseStudyView getView() {
        return (ICaseStudyView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        mList = new ArrayList<>();
        dateRange = new DateDto(MainApplication.getAppComponent().getContext().getString(R.string.today),
                DateUtils.getTodayStart(), DateUtils.getTodayEnd());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<CaseStudy> getList() {
        return mList;
    }

    public void setList(List<CaseStudy> mList) {
        this.mList = mList;
    }

    @Override
    public void getAllCaseStudy(int type) {
        GetAllCaseStudyRequest caseStudyRequest = new GetAllCaseStudyRequest();
        caseStudyRequest.startDate = dateRange.getStart();
        caseStudyRequest.endDate = dateRange.getEnd();
        if (type == APPROVED) {
            caseStudyRequest.mine = false;
            caseStudyRequest.approve = true;
            caseStudyRequest.pending = false;
        } else if (type == PENDING) {
            caseStudyRequest.mine = false;
            caseStudyRequest.approve = false;
            caseStudyRequest.pending = true;
        } else if(type == MINE){
            caseStudyRequest.mine = true;
            caseStudyRequest.approve = false;
            caseStudyRequest.pending = false;
        }
        getApiManager().getAllCaseStudy(caseStudyRequest, new ApiCallback<GetAllCaseStudyResponse>() {
            @Override
            public void success(GetAllCaseStudyResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.caseStudies);
                getView().getAllCaseStudySuccess();
            }

            @Override
            public void failure(RestError error) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                getView().getAllCaseStudyFailure(error.message);
            }
        });
    }

    @Override
    public void deleteCaseStudy(String caseStudyId, final int position) {
        getApiManager().deleteCaseStudy(caseStudyId, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteCaseStudySuccess(position);
            }

            @Override
            public void failure(RestError error) {
                getView().deleteCaseStudyFailure();
            }
        });
    }

    @Override
    public List<CaseStudy> getListSearchCaseStudy(String keyword) {
        List<CaseStudy> searchCaseStudy = new ArrayList<>();
        if (keyword != null && keyword.trim().length() > 0) {
            for (CaseStudy caseStudy : mList) {
                if (caseStudy.getTitle() != null && caseStudy.getTitle().toLowerCase().contains(keyword.toLowerCase())) {
                    searchCaseStudy.add(caseStudy);
                }
            }
        } else {
            searchCaseStudy.addAll(mList);
        }
        return searchCaseStudy;
    }

    public DateDto getDateRange() {
        return dateRange;
    }

    public void setDateRange(DateDto dateDto) {
        dateRange = dateDto;
    }

}
