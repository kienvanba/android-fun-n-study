package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nxon on 3/17/17
 */

public class LogTimeRole implements Serializable {

    @SerializedName("approve")
    @Expose
    private boolean approve;

    @SerializedName("add")
    @Expose
    private boolean add;

    @SerializedName("del")
    @Expose
    private boolean del;

    @SerializedName("modify")
    @Expose
    private boolean modify;

    @SerializedName("view")
    @Expose
    private boolean view;

    public boolean isApprove() {
        return approve;
    }

    public void setApprove(boolean approve) {
        this.approve = approve;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isDel() {
        return del;
    }

    public void setDel(boolean del) {
        this.del = del;
    }

    public boolean isModify() {
        return modify;
    }

    public void setModify(boolean modify) {
        this.modify = modify;
    }

    public boolean isView() {
        return view;
    }

    public void setView(boolean view) {
        this.view = view;
    }
}
