package com.horical.gito.interactor.api.request.companySetting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by thanhle on 3/29/17.
 */

public class CreateDepartmentRequest {
    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("desc")
    @Expose
    public String desc;
}
