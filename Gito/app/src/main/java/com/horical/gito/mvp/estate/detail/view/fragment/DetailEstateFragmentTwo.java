package com.horical.gito.mvp.estate.detail.view.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.horical.gito.R;
import com.horical.gito.mvp.estate.detail.view.DetaiEstateListener;
import com.horical.gito.utils.LogUtils;
import com.horical.gito.widget.edittext.GitOEditText;
import com.horical.gito.widget.textview.GitOTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailEstateFragmentTwo extends Fragment implements View.OnClickListener{

    private static final String TAG = LogUtils.makeLogTag(DetailEstateFragmentTwo.class);

    @Bind(R.id.spinner_type)
    Spinner mSpinnerType;

    @Bind(R.id.tv_date)
    GitOTextView mTvDate;

    @Bind(R.id.imv_edit_date)
    ImageView mImvEditDate;

    @Bind(R.id.edt_importer)
    GitOEditText mTvImporter;

    @Bind(R.id.imv_edit_importer)
    ImageView mImvEditImporter;

    @Bind(R.id.tv_quantity)
    GitOEditText mTvQuantity;

    @Bind(R.id.imv_edit_quantity)
    ImageView mImvEditQuantity;

    @Bind(R.id.edt_content)
    GitOEditText mTvContent;

    @Bind(R.id.imv_edit_content)
    ImageView mImvEditContent;

    private int editTextIdCurrent;
    private int imvIdCurrent;

    DetaiEstateListener mListener;

    public static DetailEstateFragmentTwo newInstance() {

        Bundle args = new Bundle();

        DetailEstateFragmentTwo fragment = new DetailEstateFragmentTwo();
        fragment.setArguments(args);
        return fragment;
    }
    public DetailEstateFragmentTwo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_estate_two, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        disableAllEditText();
        initData();
        initListener();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (DetaiEstateListener) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (DetaiEstateListener) activity;
    }

    private void initListener() {
        mImvEditDate.setOnClickListener(this);
        mImvEditImporter.setOnClickListener(this);
        mImvEditQuantity.setOnClickListener(this);
        mImvEditContent.setOnClickListener(this);
    }

    private void initData() {
        List<String> listSpinner = new ArrayList<>();
        listSpinner.add("hello1");
        listSpinner.add("hello2");
        listSpinner.add("hello3");
        listSpinner.add("hello4");
        ArrayAdapter<String> spinnnerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, listSpinner);
        spinnnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerType.setAdapter(spinnnerAdapter);
    }

    private void disableAllEditText() {
        mTvImporter.setEnabled(false);
        mTvQuantity.setEnabled(true);
        mTvContent.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        mListener.disableCurrentView(editTextIdCurrent, imvIdCurrent);
        switch (v.getId()){
            case R.id.imv_edit_importer:
                mListener.enableCurrentView(R.id.edt_importer, R.id.imv_edit_importer);
                editTextIdCurrent = R.id.edt_importer;
                imvIdCurrent = R.id.imv_edit_importer;
                mTvImporter.requestFocus();

                break;

            case R.id.imv_edit_quantity:
//                mListener.enableCurrentView(R.id.edt_quantity, R.id.imv_edit_quantity);
                View editView = getActivity().findViewById(R.id.tv_quantity);
                View imageView = getActivity().findViewById(R.id.imv_edit_quantity);

                if (editView != null)
                    editView.setEnabled(true);
                if (imageView != null)
                    imageView.setVisibility(View.GONE);

                editTextIdCurrent = R.id.tv_quantity;
                imvIdCurrent = R.id.imv_edit_quantity;
                mTvQuantity.requestFocus();
                break;

            case R.id.imv_edit_content:
                mListener.enableCurrentView(R.id.edt_content, R.id.imv_edit_content);
                editTextIdCurrent = R.id.edt_content;
                imvIdCurrent = R.id.imv_edit_content;
                mTvContent.requestFocus();
                break;

            case R.id.imv_edit_date:
                editTextIdCurrent = R.id.tv_date;
                imvIdCurrent = R.id.imv_edit_date;
                mTvDate.requestFocus();

                final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date date = null;

                try {
                    date = sdf.parse(mTvDate.getText().toString());
                    mListener.showDialogPicker(date, new DetaiEstateListener.DialogPickerCallBack() {
                        @Override
                        public void success(Date date) {
                            mTvDate.setText(sdf.format(date));
                        }

                        @Override
                        public void failure() {
                            mTvDate.setText("");
                        }
                    });
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                break;
        }
    }

}
