package com.horical.gito.mvp.todolist.note.list.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.mvp.dialog.dialogTodoNote.FilterTodoNoteDialog;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.todolist.note.detail.view.NoteDetailActivity;
import com.horical.gito.mvp.todolist.note.list.adapter.NoteAdapter;
import com.horical.gito.mvp.todolist.note.list.injection.component.DaggerNoteComponent;
import com.horical.gito.mvp.todolist.note.list.injection.component.NoteComponent;
import com.horical.gito.mvp.todolist.note.list.injection.module.NoteModule;
import com.horical.gito.mvp.todolist.note.list.presenter.NotePresenter;
import com.horical.gito.mvp.todolist.todo.list.view.TodoListActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Luong on 21-Mar-17.
 */

public class NoteActivity extends DrawerActivity implements View.OnClickListener, INoteView {

    public final static int REQUEST_CODE_ADD_EDIT_NOTE = 1;
    private static final String TAG = makeLogTag(NoteActivity.class);


    @Bind(R.id.note_menu)
    LinearLayout listMenu;

    @Bind(R.id.imv_note_filter)
    ImageView ivNoteFilter;

    @Bind(R.id.imv_note_add)
    ImageView ivNoteAdd;

    @Bind(R.id.rdb_btn_note)
    RadioButton rbNote;

    @Bind(R.id.rdb_btn_todo)
    RadioButton rbTodo;

    @Bind(R.id.rcv_note)
    RecyclerView rvNote;

    @Bind(R.id.note_refresh_layout)
    SwipeRefreshLayout mRefreshNote;

    NoteAdapter mNoteAdapter;

    @Inject
    NotePresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_note;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_TODO;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        NoteComponent component = DaggerNoteComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .noteModule(new NoteModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();

    }

    protected void initData() {
        mNoteAdapter = new NoteAdapter(this, mPresenter.getListNote());
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvNote.setLayoutManager(llm);
        rvNote.setAdapter(mNoteAdapter);

        showLoading();
        mPresenter.getAllNote();

    }

    protected void initListener() {
        rbNote.setOnClickListener(this);
        rbTodo.setOnClickListener(this);
        ivNoteFilter.setOnClickListener(this);
        ivNoteAdd.setOnClickListener(this);
        listMenu.setOnClickListener(this);
        mRefreshNote.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                mPresenter.getAllNote();
            }
        });
        mNoteAdapter.setOnItemClickListener(new NoteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(NoteActivity.this, NoteDetailActivity.class);
                intent.putExtra(NoteDetailActivity.ARG_NOTE, mPresenter.getListNote().get(position));
                startActivityForResult(intent, REQUEST_CODE_ADD_EDIT_NOTE);
            }

            @Override
            public void onDelete(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(NoteActivity.this);
                builder.setMessage(R.string.note_show);
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                showDialog("Loading...");
                                mPresenter.deleteNote(mNoteAdapter.getListNote().get(position).getId(), position);
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.note_menu:
                openDrawer();
                break;
            case R.id.rdb_btn_note:
                break;
            case R.id.rdb_btn_todo:
                Intent intent = new Intent(this, TodoListActivity.class);
                startActivity(intent);
                break;
            case R.id.imv_note_filter:
                FilterTodoNoteDialog dialog = new FilterTodoNoteDialog(this);
                dialog.show();
                break;
            case R.id.imv_note_add:
                Intent intent1 = new Intent(this, NoteDetailActivity.class);
                startActivityForResult(intent1, REQUEST_CODE_ADD_EDIT_NOTE);
                break;
        }
    }


    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllNoteSuccess() {
        dismissDialog();
        mRefreshNote.setRefreshing(false);
        mNoteAdapter.notifyDataSetChanged();
        showToast("success");
    }

    @Override
    public void getAllNoteFailure(String error) {
        dismissDialog();
        mRefreshNote.setRefreshing(false);
        showToast("failure");
    }


    @Override
    public void deleteNoteSuccess(int position) {
        hideLoading();
        showToast("Delete Success");
        mNoteAdapter.getListNote().remove(position);
        mNoteAdapter.notifyItemRemoved(position);
        mNoteAdapter.notifyItemRangeChanged(position, mNoteAdapter.getItemCount());
    }

    @Override
    public void deleteNoteFailure() {
        hideLoading();
        showToast("failure");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            showLoading();
            mPresenter.getAllNote();
        }
    }

}
