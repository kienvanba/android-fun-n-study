package com.horical.gito.mvp.meeting.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.meeting.detail.injection.module.MeetingDetailModule;
import com.horical.gito.mvp.meeting.detail.view.MeetingDetailActivity;

import dagger.Component;


@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = MeetingDetailModule.class)
public interface MeetingDetailComponent {
    void inject(MeetingDetailActivity activity);
}
