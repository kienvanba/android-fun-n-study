package com.horical.gito.mvp.myProject.setting.fragment.group.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.myProject.setting.adapter.GroupExpandableAdapter;
import com.horical.gito.mvp.myProject.setting.adapter.GroupMemItemBody;
import com.horical.gito.mvp.myProject.setting.fragment.group.injection.component.DaggerGroupComponent;
import com.horical.gito.mvp.myProject.setting.fragment.group.injection.component.GroupComponent;
import com.horical.gito.mvp.myProject.setting.fragment.group.injection.module.GroupModule;
import com.horical.gito.mvp.myProject.setting.fragment.group.presenter.GroupPresenter;


import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;

public class GroupFragment extends BaseFragment implements IGroupFragment {
    @Bind(R.id.exp_group)
    ExpandableListView mEpGroups;

    @Inject
    GroupPresenter mPresenter;

    GroupExpandableAdapter mGroupAdapter;
    List<GroupMemItemBody> mItems;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GroupComponent component = DaggerGroupComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .groupModule(new GroupModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        mItems = mPresenter.getGroups();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }

    @Override
    protected void initData() {
        mPresenter.getAllGroup();
        mGroupAdapter = new GroupExpandableAdapter(getContext(), mItems);
        mEpGroups.setAdapter(mGroupAdapter);
    }

    @Override
    public void getAllGroupSuccess() {
        mGroupAdapter.notifyDataSetChanged();
        if(!mItems.isEmpty()) {
            mEpGroups.expandGroup(0);
        }
    }

    public void setOnAddGroupClickListener(){
        Toast.makeText(getContext(), "Clicked!",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_group_member_setting;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
