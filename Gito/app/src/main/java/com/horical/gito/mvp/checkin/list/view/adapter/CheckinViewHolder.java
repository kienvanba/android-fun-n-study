package com.horical.gito.mvp.checkin.list.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Penguin on 23-Dec-16.
 */

public class CheckinViewHolder extends RecyclerView.ViewHolder{

    @Bind(R.id.checkin_day)
    GitOTextView mRvCheckinDay;

    @Bind(R.id.checkin_dayofweek)
    GitOTextView mTvCheckinDayofWeek;

    @Bind(R.id.checkin_week)
    RelativeLayout mCheckin_week;

    @Bind(R.id.tv_RegIn)
    TextView mTvRegisterIn;

    @Bind(R.id.tv_RegOut)
    TextView mTvRegisterOut;

    @Bind(R.id.tv_WorkingIn)
    TextView mTvWorkingIn;

    @Bind(R.id.tv_WorkingOut)
    TextView mTvWorkingOut;

    public CheckinViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
