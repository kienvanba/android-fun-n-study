package com.horical.gito.mvp.drawer.dto;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;

/**
 * Created by nxon on 3/15/17.
 */

public class BodyDto implements IRecyclerItem {

    public int key;
    public int icon;
    public String title;
    public boolean isSelected;

    public BodyDto(int key, int icon, String title, int navId) {
        this.key = key;
        this.icon = icon;
        this.title = title;
        this.isSelected = key == navId;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.MENU_BODY;
    }
}
