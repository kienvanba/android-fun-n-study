package com.horical.gito.mvp.myProject.setting.fragment.activity.version.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.presenter.VersionPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class VersionModule {
    @PerActivity
    @Provides
    VersionPresenter provideVersionPresenter(){
        return new VersionPresenter();
    }
}
