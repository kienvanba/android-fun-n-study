package com.horical.gito.mvp.allProject.list.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.Project;
import com.horical.gito.model.TokenFile;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class AllProjectAdapter extends RecyclerView.Adapter<AllProjectAdapter.ProjectHolder> {

    private Context mContext;
    private List<Project> mList;

    public AllProjectAdapter(Context context, List<Project> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public ProjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project, null);
        return new ProjectHolder(view);
    }

    @Override
    public void onBindViewHolder(ProjectHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Project prj = mList.get(position);

        holder.tvName.setText(prj.getName());
        holder.tvDes.setText(prj.getDescription());

        int status = prj.getStatus();
        String statusMes;

        int color;
        if (status == 0) {
            statusMes = mContext.getString(R.string.status_active);
            color = ContextCompat.getColor(mContext, R.color.app_color_opacity);
        } else {
            statusMes = mContext.getString(R.string.status_locked);
            color = ContextCompat.getColor(mContext, R.color.red);
        }

        TokenFile avatar = prj.getAvatar();
        if (avatar != null) {
            String url = CommonUtils.getURL(avatar);
            ImageLoader.load(mContext, url, holder.imvAvatar, holder.pgrLoading);
        } else {
            holder.pgrLoading.setVisibility(View.GONE);
            holder.tvSortName.setText(prj.getName().substring(0, 1).toUpperCase());
            holder.tvSortName.setVisibility(View.VISIBLE);
        }
        holder.tvStatus.setTextColor(color);
        holder.tvStatus.setText(statusMes);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickListener(prj);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class ProjectHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.project_tv_item_sort_name)
        GitOTextView tvSortName;

        @Bind(R.id.project_imv_avatar)
        CircleImageView imvAvatar;

        @Bind(R.id.project_pgr_avt_loading)
        ProgressBar pgrLoading;

        @Bind(R.id.project_tv_item_name)
        GitOTextView tvName;

        @Bind(R.id.project_tv_item_des)
        GitOTextView tvDes;

        @Bind(R.id.project_tv_item_status)
        GitOTextView tvStatus;

        View view;

        public ProjectHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener callBack) {
        this.mOnItemClickListener = callBack;
    }

    public interface OnItemClickListener {
        void onItemClickListener(Project prj);
    }
}
