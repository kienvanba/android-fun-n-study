package com.horical.gito.model;

import com.horical.gito.base.IRecyclerItem;

import java.util.ArrayList;

public class Section {

    private int type;
    private String title;
    private int infoNumber;
    private ArrayList<IRecyclerItem> items = new ArrayList<>();

    public Section(int type, String title) {
        this.type = type;
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public int getInfoNumber() {
        if (items != null && !items.isEmpty()) return items.size();
        return infoNumber;
    }

    public void setInfoNumber(int infoNumber) {
        this.infoNumber = infoNumber;
    }

    public ArrayList<IRecyclerItem> getItems() {
        return items;
    }
}

