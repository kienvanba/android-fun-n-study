package com.horical.gito.widget.button;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.horical.gito.R;

public class GitOButton extends Button implements IButtonStyle {

    public GitOButton(Context context) {
        super(context);
    }

    public GitOButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GitOButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setBlueStyle() {
        setBackgroundResource(R.drawable.bg_btn_blue_white_corner);
    }

    @Override
    public void setWhiteStyle() {
        setBackgroundResource(R.drawable.bg_btn_white_blue_corner);
    }

    @Override
    public void setGreyStyle(boolean corner) {
        if (corner) setBackgroundResource(R.drawable.bg_btn_grey);
        else setBackgroundResource(R.drawable.bg_btn_grey_corner);
    }

}