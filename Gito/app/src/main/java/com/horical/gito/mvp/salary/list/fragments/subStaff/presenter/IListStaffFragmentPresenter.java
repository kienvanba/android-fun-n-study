package com.horical.gito.mvp.salary.list.fragments.subStaff.presenter;

import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;

import java.util.List;

public interface IListStaffFragmentPresenter {
    List<ListStaffDto> getStaffs();

    void setStaffs(List<ListStaffDto> staffs);
}
