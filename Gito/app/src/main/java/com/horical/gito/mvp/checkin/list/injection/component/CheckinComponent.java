package com.horical.gito.mvp.checkin.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.checkin.list.injection.module.CheckinModule;
import com.horical.gito.mvp.checkin.list.view.dto.CheckinActivity;

import dagger.Component;

/**
 * Created by Penguin on 28-Dec-16.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = CheckinModule.class)
public interface CheckinComponent {
    void inject(CheckinActivity checkinActivity);
}