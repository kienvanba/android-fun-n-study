package com.horical.gito.mvp.myProject.setting.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;

import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.myProject.document.adapter.TopHorizontalAdapter;
import com.horical.gito.mvp.allProject.list.view.AllProjectActivity;
import com.horical.gito.mvp.myProject.setting.adapter.SettingViewPagerAdapter;
import com.horical.gito.mvp.myProject.setting.adapter.TabItemBody;
import com.horical.gito.mvp.myProject.setting.fragment.group.view.GroupFragment;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.view.BaseRecyclerFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SettingActivity extends BaseActivity implements ISettingView {
    @Bind(R.id.setting_viewPager)
    ViewPager viewPager;
    @Bind(R.id.top_bar)
    CustomViewTopBar topBar;
    //tab
    @Bind(R.id.tab)
    RecyclerView mRvTab;

    //circle image
    @Bind(R.id.member)
    CircleImageView member;
    @Bind(R.id.feature)
    CircleImageView feature;
    @Bind(R.id.information)
    CircleImageView information;
    @Bind(R.id.group)
    CircleImageView group;
    @Bind(R.id.component)
    CircleImageView component;
    @Bind(R.id.version)
    CircleImageView version;

    TopHorizontalAdapter mTabAdapter;
    SettingViewPagerAdapter mViewPagerAdapter;
    List<IRecyclerItem> mTabItems;
    LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_setting);
        ButterKnife.bind(this);

        initData();
        initListener();
    }

    protected void initData() {
        setupTopBar();
        setTabItems();
        setupViewpager();
        setupTab();
    }

    private void setupTopBar(){
        topBar.setTextTitle(getString(R.string.setting));
        topBar.setImageViewLeft(R.drawable.ic_back_white);
        topBar.setImageViewRight(R.drawable.ic_add_transparent);
    }

    private void setTabItems() {
        mTabItems = new ArrayList<>();
        String[] listFragment = getResources().getStringArray(R.array.project_setting_fragment);
        for(String tabName : listFragment){
            mTabItems.add(new TabItemBody(tabName));
        }
    }

    private void setupViewpager() {
        mViewPagerAdapter = new SettingViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mViewPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                initDots();
                setTabSelectedPosition(position);
                switch (position) {
                    case 0:
                        information.setImageResource(R.color.blue);
                        break;
                    case 1:
                        version.setImageResource(R.color.blue);
                        break;
                    case 2:
                        feature.setImageResource(R.color.blue);
                        break;
                    case 3:
                        member.setImageResource(R.color.blue);
                        break;
                    case 4:
                        group.setImageResource(R.color.blue);
                        break;
                    case 5:
                        component.setImageResource(R.color.blue);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setTabSelectedPosition(int position) {
        mTabAdapter.setCurrentPosition(position);
        mTabAdapter.notifyDataSetChanged();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        layoutManager.scrollToPositionWithOffset(position, (width / 2) - (mTabAdapter.getCurrentViewWidth() / 2));
    }

    private void setupTab() {
        mRvTab.setLayoutManager(layoutManager);
        mTabAdapter = new TopHorizontalAdapter(this, mTabItems);
        mRvTab.setAdapter(mTabAdapter);
        mTabAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                setTabSelectedPosition(position);
                viewPager.setCurrentItem(position, true);
            }
        });
    }

    private void initDots() {
        member.setImageResource(R.color.grey);
        feature.setImageResource(R.color.grey);
        information.setImageResource(R.color.grey);
        group.setImageResource(R.color.grey);
        component.setImageResource(R.color.grey);
        version.setImageResource(R.color.grey);
    }

    protected void initListener() {
    }

//                Intent intent = new Intent(SettingActivity.this, AllProjectActivity.class);
//                startActivity(intent);
//                SettingActivity.this.finish();

            public void onAddClicked() {
                if (mViewPagerAdapter.getCurrentFragment() instanceof BaseRecyclerFragment) {
                    ((BaseRecyclerFragment) mViewPagerAdapter.getCurrentFragment()).setOnAddItemClickListener();
                } else if (mViewPagerAdapter.getCurrentFragment() instanceof GroupFragment) {
                    ((GroupFragment) mViewPagerAdapter.getCurrentFragment()).setOnAddGroupClickListener();
                }
            }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mViewPagerAdapter!=null){
            mViewPagerAdapter.notifyDataSetChanged();
        }
    }
}
