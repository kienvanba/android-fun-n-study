package com.horical.gito.mvp.dialog.dialogCompanySetting.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.R;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by thanhle on 3/20/17.
 */

public class FilterWorkingHolidayAdapter extends RecyclerView.Adapter<FilterWorkingHolidayAdapter.FilterWorkingHolidayHolder> {
    private Context mContext;
    private List<String> mList;
    private OnItemClickListener mOnItemClickListener;

    public FilterWorkingHolidayAdapter(Context context, List<String> list) {
        mContext = context;
        mList = list;
    }


    @Override
    public FilterWorkingHolidayHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_filter_working_holiday, parent, false);
        return new FilterWorkingHolidayHolder(view);
    }

    @Override
    public void onBindViewHolder(FilterWorkingHolidayHolder holder, final int position) {
        String data = mList.get(position);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class FilterWorkingHolidayHolder extends RecyclerView.ViewHolder{

        View view;


        public FilterWorkingHolidayHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
    public interface OnItemClickListener {
        void onItemClickSelected(int position);
    }
}
