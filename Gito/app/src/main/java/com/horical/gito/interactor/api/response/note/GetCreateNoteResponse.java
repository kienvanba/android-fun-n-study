package com.horical.gito.interactor.api.response.note;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Note;

/**
 * Created by Luong on 20-Mar-17.
 */

public class GetCreateNoteResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    public Note note;
}
