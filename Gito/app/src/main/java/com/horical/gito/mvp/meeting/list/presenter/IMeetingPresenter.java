package com.horical.gito.mvp.meeting.list.presenter;

public interface IMeetingPresenter {
    void getAllMeeting();

    void deleteMeeting(String meetingId, int position);
}
