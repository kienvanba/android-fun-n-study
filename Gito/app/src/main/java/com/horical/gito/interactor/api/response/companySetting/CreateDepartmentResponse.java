package com.horical.gito.interactor.api.response.companySetting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Department;

/**
 * Created by thanhle on 3/29/17.
 */

public class CreateDepartmentResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    public Department department;
}
