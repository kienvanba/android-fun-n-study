package com.horical.gito.mvp.myProject.gantt.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.mvp.myProject.gantt.dto.GanttDetailDto;
import com.horical.gito.utils.ConvertUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailAdapter extends BaseRecyclerAdapter {

    public DetailAdapter(List<IRecyclerItem> items, Context context) {
        super(items, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(mContext).inflate(R.layout.item_gantt_detail, parent, false);
        return new GanttDetailHolder(item);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        GanttDetailItem detailItem = (GanttDetailItem) mItems.get(holder.getAdapterPosition());
        GanttDetailHolder detailHolder = (GanttDetailHolder) holder;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ConvertUtils.convertDpToInt(mContext, 20), ViewGroup.LayoutParams.MATCH_PARENT);
        detailHolder.mWhiteViewGanttDetail.setLayoutParams(params);

        detailHolder.mViewGantDetail.getLayoutParams().width = detailItem.lengh;
    }

    public class GanttDetailHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.view_gantt_detail_white)
        View mWhiteViewGanttDetail;

        @Bind(R.id.view_gantt_detail)
        View mViewGantDetail;

        public GanttDetailHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
