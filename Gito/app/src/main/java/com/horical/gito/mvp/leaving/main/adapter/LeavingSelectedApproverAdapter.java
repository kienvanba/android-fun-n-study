//package com.horical.gito.mvp.leaving.main.adapter;
//
//import android.content.Context;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.horical.gito.R;
//import com.horical.gito.caches.images.ImageLoader;
//import com.horical.gito.model.TokenFile;
//import com.horical.gito.model.User;
//import com.horical.gito.utils.CommonUtils;
//import com.horical.gito.widget.chartview.circle.CircleView;
//import com.horical.gito.widget.textview.GitOTextView;
//
//import java.util.List;
//
//import butterknife.Bind;
//import butterknife.ButterKnife;
//import de.hdodenhof.circleimageview.CircleImageView;
//
///**
// * Created by Dragonoid on 12/20/2016.
// */
//
//public class LeavingSelectedApproverAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//
//    List<User> mItems;
//    Context mContext;
//
//    public LeavingSelectedApproverAdapter(List<User> items,Context context){
//        mItems = items;
//        mContext = context;
//    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leaving_selected_approver,parent,false);
//        return new LeavingSelectedApproverHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
//        LeavingSelectedApproverHolder itemHolder = (LeavingSelectedApproverHolder) holder;
//        User item = mItems.get(position);
//        itemHolder.mSelectedName.setText(item.getDisplayName());
//        itemHolder.mPosition.setText(item.getRoleCompany().getRoleName());
//        itemHolder.mDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mItems.remove(position);
//                notifyDataSetChanged();
//            }
//        });
//        TokenFile avatar = item.getAvatar();
//        if (avatar != null) {
//            String url = CommonUtils.getURL(avatar);
//            ImageLoader.load(mContext,url,itemHolder.mAvatar,itemHolder.mPgrLoading);
//        }else {
//            itemHolder.mPgrLoading.setVisibility(View.GONE);
//            itemHolder.mSortName.setText(item.getDisplayName().substring(0, 2).toUpperCase());
//            itemHolder.mSortName.setVisibility(View.VISIBLE);
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return mItems.size() ;
//    }
//
//
//    public class LeavingSelectedApproverHolder extends RecyclerView.ViewHolder {
//
//        @Bind(R.id.leaving_tv_selected_approver_delete)
//        ImageView mDelete;
//        @Bind(R.id.leaving_tv_selected_approver_name)
//        TextView mSelectedName;
//        @Bind(R.id.leaving_tv_selected_approver_position)
//        TextView mPosition;
//        @Bind(R.id.leaving_img_approver_avatar)
//        CircleImageView mAvatar;
//        @Bind(R.id.leaving_tv_approver_sort_name)
//        GitOTextView mSortName;
//        @Bind(R.id.leaving_pgr_approver_progress)
//        ProgressBar mPgrLoading;
//
//
//        public LeavingSelectedApproverHolder (View view) {
//            super(view);
//            ButterKnife.bind(this, view);
//        }
//    }
//}
