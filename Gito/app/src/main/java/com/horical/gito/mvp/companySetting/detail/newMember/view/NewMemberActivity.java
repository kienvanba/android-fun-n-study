package com.horical.gito.mvp.companySetting.detail.newMember.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.companySetting.detail.newMember.adapter.MemberCompanyRoleAdapter;
import com.horical.gito.mvp.companySetting.detail.newMember.adapter.MemberDepartmentAdapter;
import com.horical.gito.mvp.companySetting.detail.newMember.adapter.MemberLevelAdapter;
import com.horical.gito.mvp.companySetting.detail.newMember.injection.component.DaggerNewMemberComponent;
import com.horical.gito.mvp.companySetting.detail.newMember.injection.component.NewMemberComponent;
import com.horical.gito.mvp.companySetting.detail.newMember.injection.module.NewMemberModule;
import com.horical.gito.mvp.companySetting.detail.newMember.presenter.NewMemberPresenter;
import com.horical.gito.utils.CommonUtils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class NewMemberActivity extends BaseActivity implements INewMemberView {
    public static final String TAG = makeLogTag(NewMemberActivity.class);

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.edt_display_name)
    EditText edtDisplayName;

    @Bind(R.id.edt_email)
    EditText edtEmail;

    @Bind(R.id.edt_password)
    EditText edtPassWord;

    @Bind(R.id.rcv_company_role)
    RecyclerView rcvCompanyRole;

    @Bind(R.id.rcv_level)
    RecyclerView rcvLevel;

    @Bind(R.id.rcv_department)
    RecyclerView rcvDepartment;

    private MemberCompanyRoleAdapter adapterCompanyRole;
    private MemberDepartmentAdapter adapterDepartment;
    private MemberLevelAdapter adapterLevel;

    @Inject
    NewMemberPresenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_setting_new_member);
        ButterKnife.bind(this);

        NewMemberComponent component = DaggerNewMemberComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .newMemberModule(new NewMemberModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle("New Member");
        topBar.setTextViewLeft("Cancel");
        topBar.setTextViewRight("Add");

        adapterCompanyRole = new MemberCompanyRoleAdapter(this, GitOStorage.getInstance().getListRoleCompany());
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvCompanyRole.setLayoutManager(llm);
        rcvCompanyRole.setAdapter(adapterCompanyRole);

        adapterDepartment = new MemberDepartmentAdapter(this, GitOStorage.getInstance().getListDepartment());
        LinearLayoutManager llmDepartment = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llmDepartment.setOrientation(LinearLayoutManager.VERTICAL);
        rcvDepartment.setLayoutManager(llmDepartment);
        rcvDepartment.setAdapter(adapterDepartment);

        adapterLevel = new MemberLevelAdapter(this, GitOStorage.getInstance().getListLevel());
        LinearLayoutManager llmLevel = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llmLevel.setOrientation(LinearLayoutManager.VERTICAL);
        rcvLevel.setLayoutManager(llmLevel);
        rcvLevel.setAdapter(adapterLevel);
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {
                getData();
            }

        });

        adapterCompanyRole.setOnItemClickListener(new MemberCompanyRoleAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(String id) {
                mPresenter.setIdCompanyRole(id);
            }
        });
        adapterLevel.setOnItemClickListener(new MemberLevelAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(String id) {
                mPresenter.setIdLevel(id);
            }
        });
        adapterDepartment.setOnItemClickListener(new MemberDepartmentAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(String id) {
                mPresenter.setIdDepartment(id);
            }
        });
    }

    private void getData() {
        if (edtDisplayName.getText().toString().equalsIgnoreCase("")) {
            showErrorDialog("Please enter displayName");
            return;
        }

        if (edtEmail.getText().toString().equalsIgnoreCase("")) {
            showErrorDialog("Please enter a valid email");
            return;
        }

        if (!CommonUtils.isEmailValid(edtEmail.getText().toString())) {
            showErrorDialog("Email invalid");
            return;
        }

        if (edtPassWord.getText().toString().equalsIgnoreCase("")) {
            showErrorDialog("Password invalid");
            return;
        }

        if (!CommonUtils.isPassword(edtPassWord.getText().toString())) {
            showErrorDialog("8 character up to\n" +
                    "Less 1 Upper character, less 1 lowercase, less 1 number\n" +
                    "and less 1 special character");
            return;
        }

        if (mPresenter.getIdCompanyRole().equalsIgnoreCase("")) {
            showErrorDialog("Please choose company role");
            return;
        }

        if (mPresenter.getIdDepartment().equalsIgnoreCase("")) {
            showErrorDialog("Please choose department");
            return;
        }

        if (mPresenter.getIdLevel().equalsIgnoreCase("")) {
            showErrorDialog("Please choose level");
            return;
        }

        showDialog("Loading");
        mPresenter.createMember(edtDisplayName.getText().toString(), edtPassWord.getText().toString(),
                edtEmail.getText().toString());
    }

    @Override
    public void createMemberSuccess() {
        dismissDialog();
        showToast("Success");
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void createMemberFailure(String err) {
        dismissDialog();
        showToast(err);
    }
}
