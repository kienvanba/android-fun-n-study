package com.horical.gito.mvp.salary.bonus.list.fragments.pending.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.bonus.list.fragments.pending.presenter.ListPendingFragmentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListPendingFragmentModule {
    @PerActivity
    @Provides
    ListPendingFragmentPresenter provideSalaryPresenter(){
        return new ListPendingFragmentPresenter();
    }
}
