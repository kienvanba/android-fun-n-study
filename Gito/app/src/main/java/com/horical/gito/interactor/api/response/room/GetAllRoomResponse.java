package com.horical.gito.interactor.api.response.room;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Room;

import java.util.List;

/**
 * Created by Lemon on 3/8/2017
 */

public class GetAllRoomResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Room> room;
}
