package com.horical.gito.mvp.companySetting.list.fragment.members.dto;

import com.horical.gito.model.Member;

/**
 * Created by kut3b on 11/04/2017.
 */

public class MemberDto {
    private String name;
    private String id;

    public MemberDto(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof MemberDto) {
            MemberDto memberDto = (MemberDto) obj;
            result = this.name.equals(memberDto.name);
        }
        return result;
    }

    private int getIntFromString(String data) {
        int count = 0;
        for (int i = 0; i < data.length(); i++) {
            count += Character.getNumericValue(data.charAt(i));
        }
        return count;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + getIntFromString(this.name);
        return result;
    }
}
