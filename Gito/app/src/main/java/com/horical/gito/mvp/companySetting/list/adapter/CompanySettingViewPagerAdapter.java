package com.horical.gito.mvp.companySetting.list.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;

import java.util.List;

public class CompanySettingViewPagerAdapter extends FragmentPagerAdapter {
    private List<BaseCompanyFragment> fragments;

    public CompanySettingViewPagerAdapter(FragmentManager fm, List<BaseCompanyFragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments != null ? fragments.size() : 0;
    }
}
