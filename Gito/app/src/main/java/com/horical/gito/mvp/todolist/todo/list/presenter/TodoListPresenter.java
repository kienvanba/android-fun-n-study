package com.horical.gito.mvp.todolist.todo.list.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.todolist.GetAllTodoListResponse;
import com.horical.gito.model.TodoList;
import com.horical.gito.mvp.todolist.todo.list.view.ITodoListView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class TodoListPresenter extends BasePresenter implements ITodoListPresenter {

    private int from;

    public List<TodoList> mList;

    private static final String TAG = makeLogTag(TodoListPresenter.class);

    public void attachView(ITodoListView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public ITodoListView getView() {
        return (ITodoListView) getIView();
    }

    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<TodoList> getListTodoList() {
        return mList;
    }

    public void setListTodoList(List<TodoList> todolist) {
        this.mList = todolist;
    }

    @Override
    public void getAllTodoList() {
        getApiManager().getAllTodoList(new ApiCallback<GetAllTodoListResponse>() {
            @Override
            public void success(GetAllTodoListResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.todoLists);
                getView().getAllTodoListSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getAllTodoListFailure(error.message);
            }
        });
    }

    public void deleteTodoList(String todolistId, final int position) {
        getApiManager().deleteTodoList(todolistId, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteTodoListSuccess(position);
            }

            @Override
            public void failure(RestError error) {
                getView().deleteTodoListFailure();
            }
        });

    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }
}