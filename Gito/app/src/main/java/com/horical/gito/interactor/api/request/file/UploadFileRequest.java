package com.horical.gito.interactor.api.request.file;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

/**
 * Created by nhonvt.dk on 3/30/17
 */

public class UploadFileRequest {

    public MultipartBody.Part file;

    public UploadFileRequest(MediaType type, File file) {
        RequestBody body = RequestBody.create(type, file);
        this.file = MultipartBody.Part.createFormData("file", file.getName(), body);
    }
}
