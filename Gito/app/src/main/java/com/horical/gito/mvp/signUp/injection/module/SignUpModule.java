package com.horical.gito.mvp.signUp.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.signUp.presenter.SignUpPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SignUpModule {
    @Provides
    @PerActivity
    SignUpPresenter provideSignUpPresenter(){
        return new SignUpPresenter();
    }
}
