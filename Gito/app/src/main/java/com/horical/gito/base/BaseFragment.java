package com.horical.gito.base;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.horical.gito.R;

import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

abstract public class BaseFragment extends Fragment {

    public static final String TAG = makeLogTag(BaseFragment.class);
    private ProgressDialog mLoadingDialog;
    private AlertDialog mErrorDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    /**
     * @return R.id.xxx - Id Layout Fragment
     */
    abstract protected int getLayoutId();

    protected void initData(){

    }

    protected void initListener() {

    }

    protected void showDialog(String message) {
        dismissDialog();
        mLoadingDialog = new ProgressDialog(getContext());
//        mLoadingDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.loading_spiner));
//        mLoadingDialog.setIndeterminate(true);
        mLoadingDialog.setCancelable(false);
        mLoadingDialog.setCanceledOnTouchOutside(false);
        mLoadingDialog.setMessage(message);
        mLoadingDialog.show();
    }

    protected void dismissDialog() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
    }

    protected boolean checkPermissions(String[] permissions) {
        for (String s : permissions) {
            if (ContextCompat.checkSelfPermission(getContext(), s)
                    != PackageManager.PERMISSION_GRANTED)
                return false;
        }
        return true;
    }

    protected void showErrorDialog(String message, DialogInterface.OnClickListener listener) {
        dismissErrorDialog();
        AlertDialog.Builder ab = new AlertDialog.Builder(getContext());
        ab.setTitle(R.string.error);
        ab.setMessage(message);
        ab.setPositiveButton(R.string.ok, listener);
        ab.setCancelable(false);
        mErrorDialog = ab.show();
    }

    protected void dismissErrorDialog() {
        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            mErrorDialog.dismiss();
            mErrorDialog = null;
        }
    }

    protected void showNoNetworkErrorDialog() {
        showErrorDialog(getString(R.string.no_internet_network), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismissErrorDialog();
            }
        });
    }
}
