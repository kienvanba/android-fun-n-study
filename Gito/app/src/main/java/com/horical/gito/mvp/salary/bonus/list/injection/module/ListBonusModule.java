package com.horical.gito.mvp.salary.bonus.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.bonus.list.presenter.ListBonusPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListBonusModule {
    @PerActivity
    @Provides
    ListBonusPresenter provideSalaryPresenter(){
        return new ListBonusPresenter();
    }
}