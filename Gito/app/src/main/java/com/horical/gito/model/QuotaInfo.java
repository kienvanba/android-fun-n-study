package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/27/17.
 */

public class QuotaInfo implements Serializable {

    @SerializedName("quotaSourceCode")
    @Expose
    private int quotaSourceCode;

    @SerializedName("quotaProjectNumber")
    @Expose
    private int quotaProjectNumber;

    @SerializedName("quotaUserMember")
    @Expose
    private int quotaUserMember;

    @SerializedName("quotaDiskCapacitor")
    @Expose
    private int quotaDiskCapacitor;

    public int getQuotaSourceCode() {
        return quotaSourceCode;
    }

    public void setQuotaSourceCode(int quotaSourceCode) {
        this.quotaSourceCode = quotaSourceCode;
    }

    public int getQuotaProjectNumber() {
        return quotaProjectNumber;
    }

    public void setQuotaProjectNumber(int quotaProjectNumber) {
        this.quotaProjectNumber = quotaProjectNumber;
    }

    public int getQuotaUserMember() {
        return quotaUserMember;
    }

    public void setQuotaUserMember(int quotaUserMember) {
        this.quotaUserMember = quotaUserMember;
    }

    public int getQuotaDiskCapacitor() {
        return quotaDiskCapacitor;
    }

    public void setQuotaDiskCapacitor(int quotaDiskCapacitor) {
        this.quotaDiskCapacitor = quotaDiskCapacitor;
    }
}
