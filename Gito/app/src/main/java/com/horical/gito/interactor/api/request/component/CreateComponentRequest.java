package com.horical.gito.interactor.api.request.component;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateComponentRequest {

    @SerializedName("componentId")
    @Expose
    public String componentId;

    @SerializedName("title")
    @Expose
    public String title;
}
