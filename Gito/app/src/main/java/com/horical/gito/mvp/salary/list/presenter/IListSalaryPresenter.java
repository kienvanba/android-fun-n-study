package com.horical.gito.mvp.salary.list.presenter;

import com.horical.gito.mvp.salary.dto.SalaryHeaderDto;

import java.util.List;

public interface IListSalaryPresenter {

    List<SalaryHeaderDto> getHeaders();

    void setHeaders(List<SalaryHeaderDto> headers);
}
