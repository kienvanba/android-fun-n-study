package com.horical.gito.mvp.myProject.task.details.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.task.details.injection.module.TaskDetailsModule;
import com.horical.gito.mvp.myProject.task.details.view.TaskDetailsActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = TaskDetailsModule.class)
public interface TaskDetailsComponent {
    void inject(TaskDetailsActivity activity);
}
