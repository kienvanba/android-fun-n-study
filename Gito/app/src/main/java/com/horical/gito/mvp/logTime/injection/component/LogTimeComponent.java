package com.horical.gito.mvp.logTime.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.logTime.injection.module.LogTimeModule;
import com.horical.gito.mvp.logTime.view.LogTimeActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = LogTimeModule.class)
public interface LogTimeComponent {
    void inject(LogTimeActivity activity);
}
