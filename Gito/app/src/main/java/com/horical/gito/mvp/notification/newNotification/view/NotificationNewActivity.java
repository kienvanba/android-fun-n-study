package com.horical.gito.mvp.notification.newNotification.view;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.mvp.notification.newNotification.injection.component.DaggerNotificationNewComponent;
import com.horical.gito.mvp.notification.newNotification.injection.component.NotificationNewComponent;
import com.horical.gito.mvp.notification.newNotification.injection.module.NotificationNewModule;
import com.horical.gito.mvp.notification.newNotification.presenter.NotificationNewPresenter;

import javax.crypto.spec.IvParameterSpec;
import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 3/24/2017.
 */


public class NotificationNewActivity extends BaseActivity implements INotificationNewView{

    @Inject
    NotificationNewPresenter mPresenter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_new);
        ButterKnife.bind(this);

        NotificationNewComponent component = DaggerNotificationNewComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .notificationNewModule(new NotificationNewModule())
                .build();

        component.inject(this);

        mPresenter.onCreate();
        mPresenter.attachView(this);
    }
}
