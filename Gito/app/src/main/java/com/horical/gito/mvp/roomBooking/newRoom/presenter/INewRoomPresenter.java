package com.horical.gito.mvp.roomBooking.newRoom.presenter;

/**
 * Created by Lemon on 3/27/2017.
 */

public interface INewRoomPresenter {
    void createRoom(String name, String description);

}
