package com.horical.gito.mvp.login.presenter;

public interface ILoginPresenter {

    void login(String email, String password, String deviceName, String deviceDescription, String deviceOS, String deviceOSVersion, String deviceAPNSToken);

}
