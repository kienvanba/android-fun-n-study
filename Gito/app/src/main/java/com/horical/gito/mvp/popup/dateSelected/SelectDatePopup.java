package com.horical.gito.mvp.popup.dateSelected;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.interactor.prefer.PreferManager;
import com.horical.gito.utils.ConvertUtils;
import com.horical.gito.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class SelectDatePopup extends PopupWindow {

    public static final int GRAVITY_START = 1;
    public static final int GRAVITY_END = 3;

    @Bind(R.id.rcv_popup)
    RecyclerView rcv_last_month;

    private Context mContext;

    private List<DateDto> mList;

    private DateAdapter adapter;

    @Inject
    PreferManager mPreferManager;

    private DateDto mSelectedDateRange;

    public SelectDatePopup(Context context, DateDto currentDateRange, OnItemSelectedListener callBack) {
        this.mContext = context;
        this.mCallBack = callBack;
        this.mSelectedDateRange = currentDateRange;

        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setBackgroundDrawable(new BitmapDrawable());
    }

    public void showDateDefault(View anchorView, int width, int positionX, int positionY) {
        setFocusable(true);
        setWidth(anchorView.getWidth() + (int) ConvertUtils.convertDpToPixel(mContext, width));
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        View view = LayoutInflater.from(mContext).inflate(R.layout.popup_recycle_view, null, false);
        setContentView(view);
        ButterKnife.bind(this, view);
        MainApplication.getAppComponent().inject(this);
        initData();

        if (positionX == GRAVITY_START)
            positionX = getWidth() - anchorView.getWidth();
        else if (positionX == GRAVITY_END)
            positionX = anchorView.getWidth() - getWidth();
        else
            positionX = 0;

        showAsDropDown(anchorView, positionX, positionY);
    }

    private void initData() {
        mList = new ArrayList<>();
        String[] typeDateRange = mContext.getResources().getStringArray(R.array.date_range_array);
        // Yesterday
        mList.add(new DateDto(typeDateRange[0], DateUtils.getYesterdayStart(), DateUtils.getYesterdayEnd()));
        // Today
        mList.add(new DateDto(typeDateRange[1], DateUtils.getTodayStart(), DateUtils.getTodayEnd()));
        // Last Week
        String[] lastWeek = DateUtils.getLastWeek();
        mList.add(new DateDto(typeDateRange[2], DateUtils.getStartDate2(lastWeek[0]), DateUtils.getEndDate2(lastWeek[lastWeek.length - 1])));
        // This Week
        String[] thisWeek = DateUtils.getThisWeek();
        mList.add(new DateDto(typeDateRange[3], DateUtils.getStartDate2(thisWeek[0]), DateUtils.getEndDate2(thisWeek[thisWeek.length - 1])));
        // Last Month
        String[] lastMonth = DateUtils.getLastMonth();
        mList.add(new DateDto(typeDateRange[4], DateUtils.getStartDate2(lastMonth[0]), DateUtils.getEndDate2(lastMonth[lastMonth.length - 1])));
        // This Month
        String[] thisMonth = DateUtils.getThisMonth();
        mList.add(new DateDto(typeDateRange[5], DateUtils.getStartDate2(thisMonth[0]), DateUtils.getEndDate2(thisMonth[thisMonth.length - 1])));
        //this custom
        mList.add(new DateDto(typeDateRange[6], null, null));

        for (int i = 0; i < mList.size(); i++) {
            if (mSelectedDateRange != null && mSelectedDateRange.getTypeDate().equalsIgnoreCase(mList.get(i).getTypeDate())) {
                mList.get(i).setStart(mSelectedDateRange.getStart());
                mList.get(i).setEnd(mSelectedDateRange.getEnd());
                mList.get(i).setSelected(true);
                break;
            }
        }

        adapter = new DateAdapter(mContext, mList);
        rcv_last_month.setHasFixedSize(true);
        rcv_last_month.setLayoutManager(new LinearLayoutManager(mContext));
        rcv_last_month.setAdapter(adapter);

        adapter.setOnItemClickListener(new DateAdapter.OnItemClickListener() {
            @Override
            public void onClickItem(int position) {
                switch (position) {
                    case 0:
                        mCallBack.onClickItemDialog(mList.get(position));
                        dismiss();
                        break;

                    case 1:
                        mCallBack.onClickItemDialog(mList.get(position));
                        dismiss();
                        break;

                    case 2:
                        mCallBack.onClickItemDialog(mList.get(position));
                        dismiss();
                        break;

                    case 3:
                        mCallBack.onClickItemDialog(mList.get(position));
                        dismiss();
                        break;

                    case 4:
                        mCallBack.onClickItemDialog(mList.get(position));
                        dismiss();
                        break;

                    case 5:
                        mCallBack.onClickItemDialog(mList.get(position));
                        dismiss();
                        break;

                    case 6:
                        showCustomDateDialog(position);
                        dismiss();
                        break;

                }
            }
        });
    }

    private void showCustomDateDialog(final int position) {
        CustomDateDialog dialogCustomDate = new CustomDateDialog(mContext, mList.get(position));

        dialogCustomDate.setOnDialogClickListener(new CustomDateDialog.OnDialogClickListener() {
            @Override
            public void onClickSaveCustomDate(DateDto dateDto) {
                mList.get(position).setStart(dateDto.getStart());
                mList.get(position).setEnd(dateDto.getEnd());
                mCallBack.onClickItemDialog(mList.get(position));
                dismiss();
            }
        });
        dialogCustomDate.show();
    }

    private OnItemSelectedListener mCallBack;

    public interface OnItemSelectedListener {
        void onClickItemDialog(DateDto dateRange);
    }

}