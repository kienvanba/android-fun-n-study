package com.horical.gito.mvp.meeting.detail.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.caches.files.UserCaches;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.model.Meeting;
import com.horical.gito.model.wrapperModel.WrapperListRoom;
import com.horical.gito.model.wrapperModel.WrapperListUser;
import com.horical.gito.mvp.dialog.dialogInputEdit.InputEditDialog;
import com.horical.gito.mvp.dialog.dialogMeeting.MeetingDateTimePickerDialog;
import com.horical.gito.mvp.dialog.dialogMeeting.UploadAttachFileDialog;
import com.horical.gito.mvp.meeting.detail.adapter.MTAttachFileAdapter;
import com.horical.gito.mvp.meeting.detail.adapter.MTInviteAdapter;
import com.horical.gito.mvp.meeting.detail.adapter.MTNoteAttachAdapter;
import com.horical.gito.mvp.meeting.detail.adapter.MTRoomAdapter;
import com.horical.gito.mvp.meeting.detail.injection.component.DaggerMeetingDetailComponent;
import com.horical.gito.mvp.meeting.detail.injection.component.MeetingDetailComponent;
import com.horical.gito.mvp.meeting.detail.injection.module.MeetingDetailModule;
import com.horical.gito.mvp.meeting.detail.presenter.MeetingDetailPresenter;
import com.horical.gito.mvp.meeting.detail.selectRoom.view.RoomActivity;
import com.horical.gito.mvp.todolist.note.list.view.NoteActivity;
import com.horical.gito.mvp.user.list.view.UserActivity;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.utils.FileUtils;
import com.horical.gito.utils.NetworkUtils;
import com.horical.gito.widget.textview.GitOTextView;

import java.io.File;
import java.util.Date;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;


public class MeetingDetailActivity extends BaseActivity implements IMeetingDetailView,
        View.OnClickListener,
        MTRoomAdapter.OnItemClickListener,
        MTInviteAdapter.OnItemClickListener,
        MTAttachFileAdapter.OnItemClickListener,
        MTNoteAttachAdapter.OnItemClickListener {

    public static final String ARG_MEETING = "Meeting";

    public static final int MODE_ADD = 1;
    public static final int MODE_DETAIL = 2;
    public static final int MODE_EDIT = 3;
    public int currentMode = MODE_DETAIL;
    public static final int REQUEST_READ_LIBRARY = 4;
    public static final int REQUEST_PERMISSION_READ_LIBRARY = 5;
    public static final int REQUEST_CODE_USERS = 6;
    public static final int REQUEST_CODE_ROOM = 7;


    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.tv_meeting_start_date)
    GitOTextView mTvStartDate;
    @Bind(R.id.tv_meeting_start_time)
    GitOTextView mTvStartTime;

    @Bind(R.id.tv_meeting_end_date)
    GitOTextView mTvEndDate;
    @Bind(R.id.tv_meeting_end_time)
    GitOTextView mTvEndTime;
    @Bind(R.id.btn_pick_time)
    ImageView mBtnPickTime;

    @Bind(R.id.imv_add_room)
    ImageView mBtnAddMeetingRoom;
    @Bind(R.id.meeting_rv_list_room)
    RecyclerView mRvListRoom;

    @Bind(R.id.imv_add_invitee)
    ImageView mBtnAddInvitee;
    @Bind(R.id.meeting_rv_list_invitee)
    RecyclerView mRvListInvite;

    @Bind(R.id.imv_add_file_attatch)
    ImageView mBtnAddFileAttach;
    @Bind(R.id.meeting_rv_list_file_attatch)
    RecyclerView mRvListFileAttach;

    @Bind(R.id.imv_add_note_attatch)
    ImageView mBtnAddNoteAttach;
    @Bind(R.id.meeting_rv_list_note_attatch)
    RecyclerView mRvListNote;

    @Bind(R.id.imv_meeting_edit_description)
    ImageView mBtnEditDescription;
    @Bind(R.id.tv_meeting_edit_description)
    EditText mEdtDescription;

    @Bind(R.id.imv_edit_title)
    ImageView mBtnEditTitle;
    @Bind(R.id.edt_title)
    EditText mEdtTitle;

    @Bind(R.id.tv_invitee_title)
    GitOTextView mTvInviteeTitle;
    @Bind(R.id.tv_file_attach_title)
    GitOTextView mTvFileAttachTitle;
    @Bind(R.id.tv_note_attach_title)
    GitOTextView mTvNoteAttachTitle;

    @Inject
    MeetingDetailPresenter mPresenter;



    InputEditDialog inputDialog;

    private MeetingDateTimePickerDialog mMeetingPickTime;

    private Meeting meeting;
    private File mUploadFile;
    private WrapperListUser wrapperListUser;
    private WrapperListRoom wrapperListRoom;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_detail);
        ButterKnife.bind(this);
        MeetingDetailComponent component = DaggerMeetingDetailComponent.builder()
                .meetingDetailModule(new MeetingDetailModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        meeting = (Meeting) getIntent().getSerializableExtra(ARG_MEETING);
        if (meeting != null) {
            currentMode = MODE_DETAIL;
            mPresenter.setStartDate(meeting.getStartDate());
            mPresenter.setEndDate(meeting.getEndDate());
            if (meeting.getRoom() != null) {
                mPresenter.getRooms().add(meeting.getRoom());
            }
            if (meeting.getMembers() != null && meeting.getMembers().size() > 0) {
                mPresenter.getMembers().addAll(meeting.getMembers());
            }
            if (meeting.getAttachFiles() != null && meeting.getAttachFiles().size() > 0) {
                mPresenter.getAttachFiles().addAll(meeting.getAttachFiles());
            }
            if (meeting.getNote() != null && meeting.getNote().size() > 0) {
                mPresenter.getNotes().addAll(meeting.getNote());
            }
        } else {
            currentMode = MODE_ADD;
        }

        initView();
        initData();
        updateViewMode();
        updateViewHeader();
        initListener();
    }

    private void initView() {
        // room adapter
        LinearLayoutManager roomLlm = new LinearLayoutManager(this);
        roomLlm.setOrientation(LinearLayoutManager.VERTICAL);
        mRvListRoom.setLayoutManager(roomLlm);
        mRvListRoom.setAdapter(new MTRoomAdapter(this, mPresenter.getRooms(), this));

        // invite adapter
        LinearLayoutManager inviteLlm = new LinearLayoutManager(this);
        inviteLlm.setOrientation(LinearLayoutManager.VERTICAL);
        mRvListInvite.setLayoutManager(inviteLlm);
        mRvListInvite.setAdapter(new MTInviteAdapter(this, mPresenter.getMembers(), this));

        // attach file adapter
        LinearLayoutManager fileAttachLlm = new LinearLayoutManager(this);
        fileAttachLlm.setOrientation(LinearLayoutManager.VERTICAL);
        mRvListFileAttach.setLayoutManager(fileAttachLlm);
        mRvListFileAttach.setAdapter(new MTAttachFileAdapter(this, mPresenter.getAttachFiles(), this));

        // note adapter
        LinearLayoutManager noteLlm = new LinearLayoutManager(this);
        noteLlm.setOrientation(LinearLayoutManager.VERTICAL);
        mRvListNote.setLayoutManager(noteLlm);
        mRvListNote.setAdapter(new MTAttachFileAdapter(this, mPresenter.getAttachFiles(), this));
    }

    private void updateViewMode() {
        switch (currentMode) {
            case MODE_DETAIL:
                topBar.setTextTitle(getString(R.string.meeting_detail));
                topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);

                mEdtTitle.setEnabled(false);
                mEdtDescription.setEnabled(false);
                mBtnEditTitle.setEnabled(true);
                mBtnPickTime.setEnabled(true);
                mBtnAddMeetingRoom.setEnabled(true);
                mBtnAddInvitee.setEnabled(true);
                mBtnAddFileAttach.setEnabled(true);
                mBtnAddNoteAttach.setEnabled(true);

                break;
            case MODE_ADD:
                topBar.setTextTitle(getString(R.string.new_meeting));
                topBar.setTextViewLeft(getString(R.string.cancel));
                topBar.setTextViewRight(getString(R.string.save));

                mEdtTitle.setEnabled(false);
                mEdtDescription.setEnabled(false);
                mBtnEditTitle.setEnabled(true);
                mBtnPickTime.setEnabled(true);
                mBtnAddMeetingRoom.setEnabled(true);
                mBtnAddInvitee.setEnabled(true);
                mBtnAddFileAttach.setEnabled(true);
                mBtnAddNoteAttach.setEnabled(true);
                break;
            case MODE_EDIT:
                topBar.setTextTitle(getString(R.string.meeting_detail));
                topBar.setTextViewLeft(getString(R.string.cancel));
                topBar.setTextViewRight(getString(R.string.save));

                mEdtTitle.setEnabled(false);
                mEdtDescription.setEnabled(false);
                mBtnEditTitle.setEnabled(true);
                mBtnPickTime.setEnabled(true);
                mBtnAddMeetingRoom.setEnabled(true);
                mBtnAddInvitee.setEnabled(true);
                mBtnAddFileAttach.setEnabled(true);
                mBtnAddNoteAttach.setEnabled(true);
        }
    }

    private void updateViewHeader() {
        mTvInviteeTitle.setText(getResources().getString(R.string.meeting_count_invite, mPresenter.getMembers().size()));
        mTvFileAttachTitle.setText(getResources().getString(R.string.meeting_count_file_attach, mPresenter.getAttachFiles().size()));
        mTvNoteAttachTitle.setText(getResources().getString(R.string.meeting_count_note_attach, mPresenter.getNotes().size()));
    }

    protected void initData() {
        if (meeting != null) {
            mTvStartDate.setText(DateUtils.formatDate(mPresenter.getStartDate(), "dd/MM/yyyy"));
            mTvStartTime.setText(DateUtils.formatDate(mPresenter.getStartDate(), "hh:mm a"));
            mTvEndDate.setText(DateUtils.formatDate(mPresenter.getEndDate(), "dd/MM/yyyy"));
            mTvEndTime.setText(DateUtils.formatDate(mPresenter.getEndDate(), "hh:mm a"));
            mEdtTitle.setText(meeting.getName());
            mEdtDescription.setText(meeting.getDesc());
        }
    }

    protected void initListener() {
        mBtnEditTitle.setOnClickListener(this);
        mBtnPickTime.setOnClickListener(this);
        mBtnAddMeetingRoom.setOnClickListener(this);
        mBtnAddInvitee.setOnClickListener(this);
        mBtnAddFileAttach.setOnClickListener(this);
        mBtnAddNoteAttach.setOnClickListener(this);
        mBtnEditDescription.setOnClickListener(this);

        if (currentMode == MODE_DETAIL) {
            topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
                @Override
                public void onLeftClicked() {
                    onBackPressed();
                }

                @Override
                public void onRightClicked() {

                }
            });

        } else {
            topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
                @Override
                public void onLeftClicked() {
                    if (currentMode == MODE_ADD) {
                        onBackPressed();
                    } else {
                        if (currentMode == MODE_EDIT) {
                            currentMode = MODE_DETAIL;
                            updateViewMode();
                        }
                    }
                }

                @Override
                public void onRightClicked() {
                    if (currentMode == MODE_ADD) {
                        mPresenter.createMeeting(mEdtTitle.getText().toString(), mEdtDescription.getText().toString());
                    } else if (currentMode == MODE_EDIT) {
                        mPresenter.updateMeeting(meeting.getId(), mEdtTitle.getText().toString(), mEdtDescription.getText().toString());
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imv_meeting_edit_description: {
                inputData(mEdtDescription);
                break;
            }

            case R.id.imv_edit_title: {
                inputData(mEdtTitle);
                break;
            }

            case R.id.btn_pick_time: {
                initDataTimePicker();
                break;
            }

            case R.id.imv_add_room: {
                Intent intent = new Intent(MeetingDetailActivity.this, RoomActivity.class);
                intent.putExtra(RoomActivity.KEY_FROM, RoomActivity.FROM_NEW_MEETING);
                startActivityForResult(intent, REQUEST_CODE_ROOM);
                break;
            }

            case R.id.imv_add_invitee: {
                Intent intent = new Intent(MeetingDetailActivity.this, UserActivity.class);
                intent.putExtra(UserActivity.KEY_FROM, UserActivity.FROM_NEW_MEETING);
                startActivityForResult(intent, REQUEST_CODE_USERS);
                break;
            }

            case R.id.imv_add_file_attatch: {
                final UploadAttachFileDialog uploadAttachFileDialog = new UploadAttachFileDialog(this);
                uploadAttachFileDialog.setListener(new UploadAttachFileDialog.OnDialogEditFileClickListener() {
                    @Override
                    public void albumGallery() {
                        handleOpenAlBumGallery();
                        uploadAttachFileDialog.dismiss();
                    }

                    @Override
                    public void documentGallery() {
                        handleOpenDocumentGallery();
                        uploadAttachFileDialog.dismiss();
                    }
                });

                uploadAttachFileDialog.show();
                break;
            }

            case R.id.imv_add_note_attatch: {
                Intent intent = new Intent(MeetingDetailActivity.this, NoteActivity.class);
                startActivity(intent);
                break;
            }

        }
    }

    private void handleOpenAlBumGallery() {
        String s[] = {android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(s)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, REQUEST_READ_LIBRARY);

        } else {
            ActivityCompat.requestPermissions(this, s, REQUEST_PERMISSION_READ_LIBRARY);
        }
    }

    private void handleOpenDocumentGallery() {
        String s[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(s)) {
            Intent intent = new Intent();
            intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, REQUEST_READ_LIBRARY);
        } else {
            ActivityCompat.requestPermissions(this, s, REQUEST_PERMISSION_READ_LIBRARY);
        }

    }

    void inputData(final TextView textView) {
        if (inputDialog == null || !inputDialog.isShowing()) {
            inputDialog = new InputEditDialog(MeetingDetailActivity.this, textView.getText().toString(), new InputEditDialog.OnDoneListener() {
                @Override
                public void onDone(String text) {
                    textView.setText(text);
                    currentMode = MODE_EDIT;
                    updateViewMode();
                    inputDialog.dismiss();
                    updateData();

                }
            });
            inputDialog.show();

        }
    }

    void updateData(){
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {
                mPresenter.updateMeeting(meeting.getId(), mEdtTitle.getText().toString(), mEdtDescription.getText().toString());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == REQUEST_READ_LIBRARY) {
                System.out.println("File from library " + data.getData());
                if (!NetworkUtils.isConnected(this)) {
                    showNoNetworkErrorDialog();
                } else {
                    mUploadFile = FileUtils.convertUriToFile(this, data.getData());
                    mPresenter.uploadFile(
                            MediaType.parse(getContentResolver().getType(data.getData())),
                            mUploadFile);
                }
            }

            if (requestCode == REQUEST_CODE_USERS) {
                currentMode = MODE_EDIT;
                updateViewMode();
                updateData();
                wrapperListUser = (WrapperListUser) data.getSerializableExtra(UserActivity.KEY_INTENT_USER_LIST);
                if (wrapperListUser != null && !wrapperListUser.getMyUsers().isEmpty()) {
                    mPresenter.setMembers(wrapperListUser.getMyUsers());
                    mRvListInvite.getAdapter().notifyDataSetChanged();
                }
            }

            if (requestCode == REQUEST_CODE_ROOM){
                currentMode = MODE_EDIT;
                updateViewMode();
                updateData();
                wrapperListRoom = (WrapperListRoom) data.getSerializableExtra(RoomActivity.KEY_INTENT_ROOM_LIST);
                if (wrapperListRoom != null && !wrapperListRoom.getMyRooms().isEmpty()){
                    mPresenter.setRooms(wrapperListRoom.getMyRooms());
                    mRvListRoom.getAdapter().notifyDataSetChanged();
                }
            }
        }
    }

    public void initDataTimePicker() {
        mMeetingPickTime = new MeetingDateTimePickerDialog(this, new MeetingDateTimePickerDialog.DateTimeDialogCallback() {
            @Override
            public void onDoneClick(Date DateTimeFrom, Date DateTimeTo) {
                mPresenter.setStartDate(DateTimeFrom);
                mPresenter.setEndDate(DateTimeTo);
                mTvStartDate.setText(DateUtils.formatDate(mPresenter.getStartDate(), "dd/MM/yyyy"));
                mTvStartTime.setText(DateUtils.formatDate(mPresenter.getStartDate(), "hh:mm a"));
                mTvEndDate.setText(DateUtils.formatDate(mPresenter.getEndDate(), "dd/MM/yyyy"));
                mTvEndTime.setText(DateUtils.formatDate(mPresenter.getEndDate(), "hh:mm a"));
            }

            @Override
            public void onCancelClick() {
                mMeetingPickTime.dismiss();
            }
        });
        mMeetingPickTime.setFrom(new Date());
        mMeetingPickTime.setTo(new Date());
        mMeetingPickTime.show();
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void validateFailedNameEmpty() {
        showErrorDialog(getString(R.string.meeting_name_empty));
    }

    @Override
    public void validateFailedDescEmpty() {
        showErrorDialog(getString(R.string.meeting_desc_empty));
    }

    @Override
    public void validateFailedTime() {
        showErrorDialog(getString(R.string.meeting_time_empty));
    }

    @Override
    public void createMeetingSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void createMeetingFailure(RestError error) {
        showRestErrorDialog(error);
    }

    @Override
    public void updateMeetingSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void updateMeetingFailure(RestError error) {
        showRestErrorDialog(error);
    }

    @Override
    public void uploadSuccess(String url) {
        UserCaches.readCaches(this);
    }

    @Override
    public void uploadFailure(String error) {
        showErrorDialog(error);
    }


    @Override
    public void onMTNoteAttachDelete(int position) {
        mPresenter.getNotes().remove(position);
        mRvListNote.getAdapter().notifyItemRemoved(position);
        mRvListNote.getAdapter().notifyItemRangeChanged(position, mPresenter.getNotes().size());
    }

    @Override
    public void onMTRoomDelete(int position) {
        mPresenter.getRooms().remove(position);
        mRvListRoom.getAdapter().notifyItemRemoved(position);
        mRvListRoom.getAdapter().notifyItemRangeChanged(position, mPresenter.getRooms().size());
    }

    @Override
    public void onMTAttachFileDelete(int position) {
        mPresenter.getAttachFiles().remove(position);
        mRvListFileAttach.getAdapter().notifyItemRemoved(position);
        mRvListFileAttach.getAdapter().notifyItemRangeChanged(position, mPresenter.getAttachFiles().size());
    }

    @Override
    public void onMTAttachFileClick(int position) {
        // TODO click attach file
    }

    @Override
    public void onMTInviteDelete(int position) {
        mPresenter.getMembers().remove(position);
        mRvListInvite.getAdapter().notifyItemRemoved(position);
        mRvListInvite.getAdapter().notifyItemRangeChanged(position, mPresenter.getMembers().size());
    }


}