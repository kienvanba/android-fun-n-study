package com.horical.gito.mvp.popup.popupCompanySetting;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.horical.gito.R;
import com.horical.gito.mvp.companySetting.list.fragment.members.dto.MemberDto;
import com.horical.gito.mvp.popup.popupCompanySetting.adapter.PopupConpanySettingAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 3/27/17.
 */

public class PopupCompanySetting extends PopupWindow {

    @Bind(R.id.rcv_task)
    RecyclerView rcvPopup;

    private List<MemberDto> mListJob = new ArrayList<>();
    private PopupConpanySettingAdapter adapter;
    private Context mContext;

    public PopupCompanySetting(Context context, List<MemberDto> listjob, OnItemSelectedListener callBack) {
        this.mContext = context;
        this.mCallBack = callBack;
        this.mListJob = listjob;
        setOutsideTouchable(true);
        setBackgroundDrawable(new BitmapDrawable());
    }

    public void showJob(View anchorView, String key) {
        setFocusable(true);
        setWidth(anchorView.getWidth() + 200);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_popup_recycleview, null, false);
        setContentView(view);
        ButterKnife.bind(this, view);

        adapter = new PopupConpanySettingAdapter(mContext, mListJob, key);
        adapter.setOnItemClickListener(new PopupConpanySettingAdapter.OnItemClickListener() {
            @Override
            public void onClick(String key, String id) {
                mCallBack.onClickSelected(key, id);
                dismiss();
            }
        });
        rcvPopup.setHasFixedSize(true);
        rcvPopup.setLayoutManager(new LinearLayoutManager(mContext));
        rcvPopup.setAdapter(adapter);
        showAsDropDown(anchorView);
    }

    public void showDepartment(View anchorView, String key) {
        setFocusable(true);
        setWidth(anchorView.getWidth() + 200);
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_popup_recycleview, null, false);
        setContentView(view);
        ButterKnife.bind(this, view);

        adapter = new PopupConpanySettingAdapter(mContext, mListJob, key);
        adapter.setOnItemClickListener(new PopupConpanySettingAdapter.OnItemClickListener() {
            @Override
            public void onClick(String key, String id) {
                mCallBack.onClickSelected(key, id);
                dismiss();
            }
        });
        rcvPopup.setHasFixedSize(true);
        rcvPopup.setLayoutManager(new LinearLayoutManager(mContext));
        rcvPopup.setAdapter(adapter);
        showAsDropDown(anchorView);
    }

    private OnItemSelectedListener mCallBack;

    public interface OnItemSelectedListener {
        void onClickSelected(String codeFilter, String id);
    }
}