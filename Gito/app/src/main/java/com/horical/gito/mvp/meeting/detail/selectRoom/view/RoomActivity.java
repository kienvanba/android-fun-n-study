package com.horical.gito.mvp.meeting.detail.selectRoom.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.Room;
import com.horical.gito.model.wrapperModel.WrapperListRoom;
import com.horical.gito.mvp.meeting.detail.selectRoom.adapter.RoomAdapter;
import com.horical.gito.mvp.meeting.detail.selectRoom.injection.component.DaggerRoomComponent;
import com.horical.gito.mvp.meeting.detail.selectRoom.injection.component.RoomComponent;
import com.horical.gito.mvp.meeting.detail.selectRoom.injection.module.RoomModule;
import com.horical.gito.mvp.meeting.detail.selectRoom.presenter.RoomPresenter;
import com.horical.gito.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lemon on 4/13/2017.
 */

public class RoomActivity extends BaseActivity implements IRoomView, View.OnClickListener {
    public static final String TAG = LogUtils.makeLogTag(RoomActivity.class);
    public final static String KEY_FROM = "KEY_FROM";
    public static final String KEY_INTENT_ROOM_LIST = "USER_LIST";
    public final static int FROM_NEW_MEETING = 1;


    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.edt_search_user)
    EditText edtSearch;

    @Bind(R.id.imv_clear_search)
    ImageView imvClearSearch;

    @Bind(R.id.ll_check_all)
    LinearLayout llCheckAll;

    @Bind(R.id.chk_item_user)
    CheckBox chkCheckAll;

    @Bind(R.id.refresh_user)
    SwipeRefreshLayout refreshRoom;

    @Bind(R.id.rcv_user)
    RecyclerView mRvListRoom;

    RoomAdapter mRoomAdapter;

    private boolean isCheckAll;

    private int keyFrom;

    @Inject
    RoomPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);

        RoomComponent component = DaggerRoomComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .roomModule(new RoomModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        keyFrom = getIntent().getIntExtra(KEY_FROM, 2);

        initData();
        initListener();
    }

    protected void initData(){
        topBar.setTextTitle(getString(R.string.room_picker));
        topBar.setTextViewLeft(getString(R.string.cancel));
        topBar.setTextViewRight(getString(R.string.done));

        mRoomAdapter = new RoomAdapter(this, mPresenter.getSearchRoom());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRvListRoom.setLayoutManager(layoutManager);
        mRvListRoom.setHasFixedSize(false);
        mRvListRoom.setAdapter(mRoomAdapter);

        showLoading();
        mPresenter.getAllRoomBooking();

        switch (keyFrom){
            case FROM_NEW_MEETING:{
                topBar.setTextTitle(getString(R.string.user_picker));
                topBar.setTextViewLeft(getString(R.string.cancel));
                topBar.setTextViewRight(getString(R.string.done));

                mPresenter.getAllRoomBooking();
                break;
            }
        }
    }

    protected void initListener(){
        llCheckAll.setOnClickListener(this);
        chkCheckAll.setOnClickListener(this);
        imvClearSearch.setOnClickListener(this);

        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {
                boolean hasRoomChecked = false;
                List<Room> myRoom = new ArrayList<>();
                for (int i = 0; i < mPresenter.getSearchRoom().size(); i++) {
                    Room room = mPresenter.getSearchRoom().get(i);
                    if (room.isSelected()) {
                        hasRoomChecked = true;
                        myRoom.add(room);
                    }
                }

                if (hasRoomChecked) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(KEY_INTENT_ROOM_LIST, new WrapperListRoom(myRoom));
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    showErrorDialog(getString(R.string.error_room_picker));
                }
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                updateAdapterFromAllRoom();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) imvClearSearch.setVisibility(View.VISIBLE);
                else imvClearSearch.setVisibility(View.GONE);
            }
        });

         mRoomAdapter.setOnItemClickListener(new RoomAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                isCheckAll = mRoomAdapter.isAllRoomChecked();
                chkCheckAll.setChecked(mRoomAdapter.isAllRoomChecked());
            }
        });
    }

    private void updateAdapterFromAllRoom() {
        mPresenter.getListSearchRoom(edtSearch.getText().toString());
        mRoomAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_check_all:
            case R.id.chk_item_user:
                isCheckAll = !isCheckAll;
                chkCheckAll.setChecked(isCheckAll);

                mPresenter.setCheckAll(isCheckAll);
                mRoomAdapter.notifyDataSetChanged();
                break;

            case R.id.imv_clear_search:
                edtSearch.setText("");
                break;
        }
    }

    @Override
    public void getAllRoomBookingSuccess() {
        hideLoading();
        updateAdapterFromAllRoom();
    }

    @Override
    public void getAllRoomBookingFailure(String err) {
        hideLoading();
        showToast(err);
        updateAdapterFromAllRoom();

    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }


}
