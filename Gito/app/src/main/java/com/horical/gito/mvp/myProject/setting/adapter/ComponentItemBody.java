package com.horical.gito.mvp.myProject.setting.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.Component;

public class ComponentItemBody implements IRecyclerItem{
    public Component component;
    public ComponentItemBody(Component component){
        this.component = component;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.COMPONENT_ITEM;
    }
}
