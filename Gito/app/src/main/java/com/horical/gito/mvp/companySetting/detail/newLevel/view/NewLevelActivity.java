package com.horical.gito.mvp.companySetting.detail.newLevel.view;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.Level;
import com.horical.gito.mvp.companySetting.detail.newLevel.injection.component.DaggerNewLevelComponent;
import com.horical.gito.mvp.companySetting.detail.newLevel.injection.component.NewLevelComponent;
import com.horical.gito.mvp.companySetting.detail.newLevel.injection.module.NewLevelModule;
import com.horical.gito.mvp.companySetting.detail.newLevel.presenter.NewLevelPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class NewLevelActivity extends BaseActivity implements INewLevelView {
    public static final String TAG = makeLogTag(NewLevelActivity.class);
    public static final String NEW_LEVEL = "NewLevel";

    public static final int MODE_DETAIL = 1;
    public static final int MODE_ADD = 2;
    public int currentMode = MODE_DETAIL;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.edt_title)
    EditText edtTitle;

    @Bind(R.id.edt_des)
    EditText edtDes;

    private Level level;

    @Inject
    NewLevelPresenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_setting_new_level);
        ButterKnife.bind(this);

        NewLevelComponent component = DaggerNewLevelComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .newLevelModule(new NewLevelModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        level = (Level) getIntent().getSerializableExtra(NEW_LEVEL);

        if (level != null) {
            currentMode = MODE_DETAIL;
        } else {
            currentMode = MODE_ADD;
        }

        initData();
        updateViewMode();
        initListener();
    }

    protected void initData() {
        if (level != null) {
            edtTitle.setText(level.getName());
            edtDes.setText(level.getDesc());
        } else {
            level = new Level();
            edtTitle.setText("");
            edtDes.setText("");
        }

    }

    private void updateViewMode() {
        switch (currentMode) {
            case MODE_DETAIL:
                topBar.setTextTitle(level.getName());

                break;
            case MODE_ADD:
                topBar.setTextTitle("New Level");
                break;
        }
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        topBar.setTextViewRight("Save");
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {
                getDataDepartment();
            }

        });
    }

    public void getDataDepartment() {
        if (edtTitle.getText().toString().equalsIgnoreCase("")) {
            showErrorDialog("Please input level title");
            return;
        }
        level.setName(edtTitle.getText().toString());
        level.setDesc(edtDes.getText().toString());
        if (currentMode == MODE_ADD) {
            mPresenter.createLevel(level);
        } else {
            mPresenter.updateLevel(level);
        }
    }

    @Override
    public void createLevelSuccess() {
        Toast.makeText(NewLevelActivity.this, "Success", Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void createLevelFailure(String err) {
        Toast.makeText(NewLevelActivity.this, "Failure", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateLevelSuccess() {
        Toast.makeText(NewLevelActivity.this, "Success", Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void updateLevelFailure(String err) {
        Toast.makeText(NewLevelActivity.this, "Failure", Toast.LENGTH_SHORT).show();
    }
}
