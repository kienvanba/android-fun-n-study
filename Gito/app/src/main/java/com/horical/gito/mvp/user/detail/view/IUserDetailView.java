package com.horical.gito.mvp.user.detail.view;

import com.horical.gito.base.IView;

/**
 * Created by thanhle on 4/7/17.
 */

public interface IUserDetailView extends IView{
    void updateMemberSuccess(int isAdmin, int isLock);

    void updateMemberFailure(String err);

    void deleteMemberSuccess();

    void deleteMemberFailure(String err);

    void changePasswordSuccess();

    void changePasswordFailure(String err);
}
