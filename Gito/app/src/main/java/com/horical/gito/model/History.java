package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by kut3b on 12/04/2017.
 */

public class History implements Serializable {
    @SerializedName("date")
    @Expose
    private Date date;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("amount")
    @Expose
    private int amount;

    @SerializedName("currency")
    @Expose
    private String currency;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
