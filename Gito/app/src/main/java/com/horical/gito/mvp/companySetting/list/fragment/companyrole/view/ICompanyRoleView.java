package com.horical.gito.mvp.companySetting.list.fragment.companyrole.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface ICompanyRoleView extends IView {

    void getAllCompanyRoleSuccess();

    void getAllCompanyRoleFailure(String err);

    void deleteCompanyRoleSuccess(int position);

    void deleteCompanyRoleFailure(String err);
}
