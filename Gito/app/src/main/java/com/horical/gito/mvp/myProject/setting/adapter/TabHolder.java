package com.horical.gito.mvp.myProject.setting.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TabHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.tab_item_container)
    public RelativeLayout mTabContainer;
    @Bind(R.id.tab_item_name)
    public TextView mTvTabName;

    public TabHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
