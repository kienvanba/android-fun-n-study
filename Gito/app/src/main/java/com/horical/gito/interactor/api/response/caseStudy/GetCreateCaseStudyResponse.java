package com.horical.gito.interactor.api.response.caseStudy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.CaseStudy;

/**
 * Created by thanhle on 3/3/17.
 */

public class GetCreateCaseStudyResponse extends BaseResponse{
    @SerializedName("results")
    @Expose
    public CaseStudy caseStudy;
}
