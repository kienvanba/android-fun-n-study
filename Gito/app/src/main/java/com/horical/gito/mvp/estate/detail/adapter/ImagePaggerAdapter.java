package com.horical.gito.mvp.estate.detail.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.caches.images.ImageLoaderCallback;
import com.horical.gito.model.TokenFile;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

/**
 * Created by hoangsang on 11/8/16.
 */

public class ImagePaggerAdapter extends PagerAdapter {

    private List<TokenFile> mTokenFiles;
    private Context mContext;

    public ImagePaggerAdapter(Context context, List<TokenFile> tokenFiles) {
        this.mContext = context;
        this.mTokenFiles = tokenFiles;
    }

    @Override
    public int getCount() {
        return mTokenFiles.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View item = LayoutInflater.from(mContext).inflate(R.layout.item_image, container, false);
        final ImageView imv = (ImageView) item.findViewById(R.id.imv);
        final ImageView btnAddImage = (ImageView) item.findViewById(R.id.btn_add_imv);
        final ImageView btnDeleteImage = (ImageView) item.findViewById(R.id.btn_delete_imv);

        String url = CommonUtils.getURL(mTokenFiles.get(position));
        ImageLoader.load(mContext, url, new ImageLoaderCallback() {
            @Override
            public void onCompleted(Bitmap bm) {
                imv.setImageBitmap(bm);
            }

            @Override
            public void onFailed(Exception e) {

            }
        });

        container.addView(item);
        return item;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = 30;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
