package com.horical.gito.mvp.recruitment.recruitDetail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.recruitment.recruitDetail.injection.module.RecruitmentDetailModule;
import com.horical.gito.mvp.recruitment.recruitDetail.view.RecruitmentDetailActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class , modules = RecruitmentDetailModule.class)
public interface RecruitmentDetailComponent {
    void inject(RecruitmentDetailActivity activity);
}
