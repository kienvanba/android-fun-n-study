package com.horical.gito.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.horical.gito.utils.LogUtils.LOGI;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class DateTimeUtils {

    private static final String TAG = makeLogTag(DateTimeUtils.class);

    public static String formatTime(long time) {
        long totalSeconds = time / 1000;
        long hours = totalSeconds / 3600;
        long minutes = (totalSeconds % 3600) / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static String formatDate(Date date, String format) {
        SimpleDateFormat output = new SimpleDateFormat(format, Locale.US);
        output.setTimeZone(TimeZone.getDefault());
        try {
            return output.format(date);
        } catch (NullPointerException e) {
            LOGI(TAG, "error when format date");
        }
        return "";
    }
}
