package com.horical.gito.mvp.report.view;

import com.horical.gito.base.IView;

/**
 * Created by thanhle on 3/2/17.
 */

public interface IReportView extends IView {
    void showError(String error);

    void showLoading();

    void dismissLoading();

    void onNotifyAdapterReport();

    void onNotifyAdapterByDate();

    void onNotifyAdapterByContent();

}
