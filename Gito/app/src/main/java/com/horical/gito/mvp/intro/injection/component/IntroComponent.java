package com.horical.gito.mvp.intro.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.intro.injection.module.IntroModule;
import com.horical.gito.mvp.intro.view.IntroActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = IntroModule.class)
public interface IntroComponent {
    void inject(IntroActivity activity);
}
