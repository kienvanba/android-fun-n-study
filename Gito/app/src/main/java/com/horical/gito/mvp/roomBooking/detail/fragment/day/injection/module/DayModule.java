package com.horical.gito.mvp.roomBooking.detail.fragment.day.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.roomBooking.detail.fragment.day.presenter.DayPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DayModule {
    @PerFragment
    @Provides
    DayPresenter provideDayPresenter(){
        return new DayPresenter();
    }
}
