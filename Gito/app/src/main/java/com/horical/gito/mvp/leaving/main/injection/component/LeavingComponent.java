package com.horical.gito.mvp.leaving.main.injection.component;


import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.leaving.main.injection.module.LeavingModule;
import com.horical.gito.mvp.leaving.main.view.LeavingActivity;

import dagger.Component;

/**
 * Created by Dragonoid on 12/7/2016.
 */

@PerActivity
@Component (dependencies = ApplicationComponent.class,modules = LeavingModule.class)
public interface LeavingComponent {
    void inject (LeavingActivity activity);
}
