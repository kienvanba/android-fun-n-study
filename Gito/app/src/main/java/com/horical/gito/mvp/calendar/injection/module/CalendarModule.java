package com.horical.gito.mvp.calendar.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.calendar.presenter.CalendarPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class CalendarModule {
    @PerActivity
    @Provides
    CalendarPresenter provideCalendarPresenter(){
        return new CalendarPresenter();
    }
}
