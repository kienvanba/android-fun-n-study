package com.horical.gito.mvp.salary.bonus.list.presenter;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.salary.bonus.list.view.IListBonusView;
import com.horical.gito.mvp.salary.dto.SalaryHeaderDto;

import java.util.ArrayList;
import java.util.List;

public class ListBonusPresenter extends BasePresenter implements IListBonusPresenter {

    public void attachView(IListBonusView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IListBonusView getView() {
        return (IListBonusView) getIView();
    }

    private List<SalaryHeaderDto> mHeaders;

    private void setHeader() {
        getHeaders().add(new SalaryHeaderDto(MainApplication.mContext.getString(R.string.approved)));
        getHeaders().add(new SalaryHeaderDto(MainApplication.mContext.getString(R.string.pending)));
        getHeaders().add(new SalaryHeaderDto(MainApplication.mContext.getString(R.string.mine)));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        setHeader();
    }

    @Override
    public List<SalaryHeaderDto> getHeaders() {
        if (mHeaders == null) {
            mHeaders = new ArrayList<>();
        }
        return mHeaders;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
