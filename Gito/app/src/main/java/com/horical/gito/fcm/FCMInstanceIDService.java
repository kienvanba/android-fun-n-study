package com.horical.gito.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.horical.gito.MainApplication;
import com.horical.gito.interactor.prefer.PreferManager;

import javax.inject.Inject;

public class FCMInstanceIDService extends FirebaseInstanceIdService {

    public FCMInstanceIDService() {
        MainApplication.getAppComponent().inject(this);
    }

    @Inject
    PreferManager preferManager;

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        preferManager.setFcmToken(refreshedToken);
    }

}