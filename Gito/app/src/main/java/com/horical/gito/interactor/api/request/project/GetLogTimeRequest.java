package com.horical.gito.interactor.api.request.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class GetLogTimeRequest {

    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("endDate")
    @Expose
    public Date endDate;

    @SerializedName("creatorId")
    @Expose
    public String creatorId;

    @SerializedName("status")
    @Expose
    public int status;
}
