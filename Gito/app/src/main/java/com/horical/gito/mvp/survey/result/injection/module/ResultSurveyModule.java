package com.horical.gito.mvp.survey.result.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.survey.result.presenter.ResultSurveyPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by nhattruong251295 on 3/26/2017.
 */
@Module
public class ResultSurveyModule {
    @PerActivity
    @Provides
    ResultSurveyPresenter providesResultSurveyPresenter(){ return new ResultSurveyPresenter();}
}
