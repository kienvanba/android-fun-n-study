package com.horical.gito.injection.module;

import android.app.Application;

import com.horical.gito.interactor.assets.AssetsManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AssetsModule {

    @Provides
    @Singleton
    AssetsManager provideAssetsManager(Application application) {
        return new AssetsManager(application);
    }

}
