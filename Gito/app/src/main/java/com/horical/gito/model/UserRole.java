package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserRole implements Serializable{
    @SerializedName("_id")
    @Expose
    public String _id;

    @SerializedName("roleName")
    @Expose
    public String roleName;
}
