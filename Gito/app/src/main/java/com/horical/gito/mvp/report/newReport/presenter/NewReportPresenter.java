package com.horical.gito.mvp.report.newReport.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.report.InFormToParam;
import com.horical.gito.interactor.api.request.report.NewReportRequest;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Group;
import com.horical.gito.model.User;
import com.horical.gito.mvp.report.newReport.adapter.UserDto;
import com.horical.gito.mvp.report.newReport.view.INewReportView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NewReportPresenter extends BasePresenter implements INewReportPresenter {

    public void attachView(INewReportView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public INewReportView getView() {
        return (INewReportView) getIView();
    }


    private List<UserDto> list = new ArrayList<>();

    public List<UserDto> getList() {
        return list;
    }

    private void setDataDefault() {
        list.add(new UserDto("Report to User", new ArrayList<>()));
        list.add(new UserDto("InForm to User", new ArrayList<>()));
        list.add(new UserDto("InForm to Group", new ArrayList<>()));

    }

    public void onCreateReport(Date date, String doing, String problem, String finish,int status) {
        getView().showLoading();
        //report to
        List<String> reportTo = new ArrayList<>();
        for (int i = 0; i < list.get(0).getList().size(); i++) {
            reportTo.add(((User) list.get(0).getList().get(i)).get_id());

        }
        //InFormToParam user
        List<String> userParam = new ArrayList<>();
        for (int i = 0; i < list.get(1).getList().size(); i++) {
            userParam.add(((User) list.get(1).getList().get(i)).get_id());
        }

        //InFormToParam group
        List<String> group = new ArrayList<>();
        for (int i = 0; i < list.get(2).getList().size(); i++) {
            group.add(((Group) list.get(2).getList().get(i)).get_id());
        }

        InFormToParam inFormTo = new InFormToParam(userParam, group);
        NewReportRequest param = new NewReportRequest(date, doing, problem, finish, reportTo, inFormTo, status);
        getApiManager().createReport(param, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().createReportSuccess();
                getView().dismissLoading();
            }

            @Override
            public void failure(RestError error) {
                getView().showErrorLoading(error.message);

            }
        });
    }

    public void onDeleteItem(int positionHeader, int positionItem) {
        list.get(positionHeader).getList().remove(positionItem);
    }

    public void onAddIem(int positionHeader, List<Object> object) {
        list.get(positionHeader).getList().clear();
        list.get(positionHeader).getList().addAll(object);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        setDataDefault();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

}
