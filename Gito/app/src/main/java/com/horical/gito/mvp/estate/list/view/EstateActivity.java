package com.horical.gito.mvp.estate.list.view;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.estate.Estate;
import com.horical.gito.mvp.dialog.dialogInputEdit.TwoRowsInputTextDialog;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.estate.detail.view.DetailEstateActivity;
import com.horical.gito.mvp.estate.list.adapter.EstateAdapter;
import com.horical.gito.mvp.estate.list.adapter.EstateItem;
import com.horical.gito.mvp.estate.list.injection.component.DaggerEstateComponent;
import com.horical.gito.mvp.estate.list.injection.component.EstateComponent;
import com.horical.gito.mvp.estate.list.injection.module.EstateModule;
import com.horical.gito.mvp.estate.list.presenter.EstatePresenter;
import com.horical.gito.widget.dialog.DialogPositiveNegative;
import com.horical.gito.widget.dialog.EditTwoOptionsDialog;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EstateActivity extends DrawerActivity implements IEstateView, View.OnClickListener,
        BaseRecyclerAdapter.OnItemClickListener {
    public final static String KEY_FROM = "KEY_FROM";

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.estate_refresh_layout)
    SwipeRefreshLayout mEstateRefreshLayout;

    @Bind(R.id.recycle_estate)
    RecyclerView mRecycleEstate;

    EstateAdapter mAdapter;

    @Inject
    EstatePresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_estate;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_ESTATE;
    }

    @Override
    public ComponentName getComponentName() {
        return super.getComponentName();
    }

    private String mWarehouseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        EstateComponent component = DaggerEstateComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .estateModule(new EstateModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initView();
        initData();
        initListener();
    }

    private void initView() {
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        updateTitle();

        topBar.setImageViewRight(R.drawable.ic_setting, R.drawable.ic_filter, R.drawable.ic_add_transparent);
    }

    private void updateTitle() {
        String title = getString(R.string.estate_item);
        if (mPresenter.getListItem() != null)
            title += " (" + mPresenter.getListItem().size() + ")";
        else
            title += " (0)";

        topBar.setTextTitle(title);
    }


    protected void initData() {
        Intent intent = getIntent();
        mWarehouseId = intent.getStringExtra(KEY_WAREHOUSE_ID);

        mPresenter.getAllEstateItem(mWarehouseId);
    }


    protected void initListener() {

        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightButtonOneClicked() {

                final TwoRowsInputTextDialog dialog = new TwoRowsInputTextDialog(EstateActivity.this,
                        getString(R.string.enter_name), getString(R.string.enter_estate_item_id),
                        getString(R.string.cancel), getString(R.string.done));
                dialog.setListener(new TwoRowsInputTextDialog.OnListener() {
                    @Override
                    public void onNegativeClick() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onPositiveClick() {
                        dialog.dismiss();
                        mPresenter.createEstateItem(mWarehouseId, dialog.getTextOne(), dialog.getTextTwo());
                    }
                });
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }

            @Override
            public void onRightButtonTwoClicked() {
                Toast.makeText(EstateActivity.this, "Filter", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRightButtonThreeClicked() {
                Toast.makeText(EstateActivity.this, "Setting", Toast.LENGTH_SHORT).show();
            }
        });

        mEstateRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getAllEstateItem(mWarehouseId);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();

    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(EstateActivity.this, DetailEstateActivity.class);
        if (position < mPresenter.getListItem().size()) {
            EstateItem item = (EstateItem) mPresenter.getListItem().get(position);
            intent.putExtra(KEY_ESTATE, item.estate);
            startActivity(intent);
        }

    }

    @Override
    public void showLoading() {
        showDialog("Loading...");
    }

    @Override
    public void hideLoading() {
        if (mEstateRefreshLayout.isRefreshing())
            mEstateRefreshLayout.setRefreshing(false);

        dismissDialog();
    }

    @Override
    public boolean isRefreshing() {
        return mEstateRefreshLayout.isRefreshing();
    }

    @Override
    public void getAllEstateItemSuccess() {
        updateTitle();
        setUpRecycleView();
    }

    @Override
    public void getAllEstateItemFailed() {
        showErrorDialog(getString(R.string.get_all_estate_failed));
    }

    private void setUpRecycleView() {
        if (mAdapter == null) {
            mAdapter = new EstateAdapter(mPresenter.getListItem(), this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            mRecycleEstate.setLayoutManager(layoutManager);
            mRecycleEstate.setHasFixedSize(true);

            mAdapter.setOnItemClickListener(this);
            mAdapter.setListener(new EstateAdapter.OnClick() {
                @Override
                public void onDelete(int position) {
                    if (position < mPresenter.getListItem().size()) {
                        EstateItem item = (EstateItem) mPresenter.getListItem().get(position);
                        final Estate estate = item.estate;
                        if (estate != null) {

                            String message = getString(R.string.question_delete) + " " + estate.name + " ?";
                            showConfirmDialog("", message, new DialogPositiveNegative.IPositiveNegativeDialogListener() {
                                @Override
                                public void onIPositiveNegativeDialogAnswerPositive(DialogPositiveNegative dialog) {
                                    mPresenter.deleteEstateItem(mWarehouseId, estate.id);
                                    dialog.dismiss();
                                }

                                @Override
                                public void onIPositiveNegativeDialogAnswerNegative(DialogPositiveNegative dialog) {
                                    dialog.dismiss();

                                }
                            });

                        }
                    }
                }
            });

            mRecycleEstate.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void createEstateItemSuccess() {
        mPresenter.getAllEstateItem(mWarehouseId);
    }

    @Override
    public void createEstateItemFailed(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void deleteEstateItemSuccess() {
        mPresenter.getAllEstateItem(mWarehouseId);
    }

    @Override
    public void deleteEstateItemFailed(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {

    }
}
