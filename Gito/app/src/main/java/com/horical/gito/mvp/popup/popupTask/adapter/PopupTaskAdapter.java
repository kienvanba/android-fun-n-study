package com.horical.gito.mvp.popup.popupTask.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 3/9/17.
 */

public class PopupTaskAdapter extends RecyclerView.Adapter<PopupTaskAdapter.ViewHolder> {
    ArrayList<String> mList;
    Context context;
    String keySelected = "";

    public PopupTaskAdapter(Context context, ArrayList<String> list, String keySelected) {
        this.context = context;
        this.keySelected = keySelected;
        this.mList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_popup_task, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if(keySelected.toLowerCase().equalsIgnoreCase("")){
            holder.tvName.setSelected(false);
        }else {
            if (keySelected.toLowerCase().equalsIgnoreCase(mList.get(position).toLowerCase())) {
                holder.tvName.setTextColor(context.getResources().getColor(R.color.app_color_opacity));
                holder.tvSelected.setBackgroundColor(context.getResources().getColor(R.color.app_color_opacity));
            }
        }

        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keySelected = mList.get(position);
                mCallBack.onClick(mList.get(position));
            }
        });
        holder.tvName.setText(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.edit_name)
        TextView tvName;

        @Bind(R.id.tv_selected)
        TextView tvSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    OnItemClickListener mCallBack;

    public void setOnItemClickListener(OnItemClickListener callBack) {
        this.mCallBack = callBack;
    }

    public interface OnItemClickListener {
        void onClick(String key);
    }
}
