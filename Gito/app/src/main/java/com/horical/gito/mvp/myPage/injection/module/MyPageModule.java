package com.horical.gito.mvp.myPage.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.myPage.presenter.MyPagePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MyPageModule {
    @Provides
    @PerActivity
    MyPagePresenter provideMyPagePresenter() {
        return new MyPagePresenter();
    }
}
