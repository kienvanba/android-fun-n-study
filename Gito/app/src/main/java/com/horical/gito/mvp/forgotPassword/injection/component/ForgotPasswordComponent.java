package com.horical.gito.mvp.forgotPassword.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.forgotPassword.injection.module.ForgotPasswordModule;
import com.horical.gito.mvp.forgotPassword.view.ForgotPasswordActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ForgotPasswordModule.class)
public interface ForgotPasswordComponent {
    void inject(ForgotPasswordActivity activity);
}
