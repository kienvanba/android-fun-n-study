package com.horical.gito.mvp.caseStudy.list.presenter;

import com.horical.gito.model.CaseStudy;

import java.util.List;

public interface ICaseStudyPresenter {

    List<CaseStudy> getListSearchCaseStudy(String keyword);

    void getAllCaseStudy(int type);

    void deleteCaseStudy(String caseStudyId, int position);
}
