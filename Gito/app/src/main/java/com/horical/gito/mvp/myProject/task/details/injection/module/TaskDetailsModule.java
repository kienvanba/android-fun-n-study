package com.horical.gito.mvp.myProject.task.details.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.myProject.task.details.presenter.TaskDetailsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TaskDetailsModule {
    @PerActivity
    @Provides
    TaskDetailsPresenter providerTaskDetailsPresenter() {
        return new TaskDetailsPresenter();
    }
}
