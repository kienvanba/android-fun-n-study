package com.horical.gito.mvp.salary.dialog;

import com.horical.gito.R;
import com.horical.gito.base.BaseDialogFragment;

public class DialogFragmentInputFunctional extends BaseDialogFragment {


    @Override
    protected int layoutID() {
        return R.layout.dialog_input_functional;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}
