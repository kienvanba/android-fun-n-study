package com.horical.gito.mvp.myProject.setting.fragment.recycler.view.component;

import com.horical.gito.R;
import com.horical.gito.interactor.api.request.component.CreateComponentRequest;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.view.BaseRecyclerFragment;
import com.horical.gito.widget.dialog.DialogEditText;

public class ComponentFragment extends BaseRecyclerFragment {
    @Override
    protected void requestData() {
        mPresenter.getAllComponent();
    }

    @Override
    protected void setOnItemClickListener(int position) {

    }

    @Override
    protected void setOnDeleteItemClickListener(int position) {
        mPresenter.deleteComponent(position);
    }

    @Override
    public void setOnAddItemClickListener() {
        final DialogEditText mDialog = new DialogEditText(getContext()) {
            @Override
            public void setSaveButtonClickListener() {
                if(!this.editText.getText().toString().trim().equals("")){
                    CreateComponentRequest request = new CreateComponentRequest();
                    request.title = this.editText.getText().toString();
                    request.componentId = request.title.trim().toLowerCase().replaceAll("\\W","");

                    mPresenter.createComponent(request);
                    this.dismiss();
                }
            }
        };
        mDialog.setHint(getString(R.string.hint_name));
        mDialog.show();
    }
}
