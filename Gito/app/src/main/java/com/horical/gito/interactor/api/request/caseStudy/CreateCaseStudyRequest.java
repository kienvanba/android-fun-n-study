package com.horical.gito.interactor.api.request.caseStudy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by thanhle on 4/5/17.
 */

public class CreateCaseStudyRequest {

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("desc")
    @Expose
    public String desc;

    @SerializedName("status")
    @Expose
    public int status;
}
