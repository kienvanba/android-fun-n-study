package com.horical.gito.mvp.survey.statistical.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.survey.statistical.view.IStatisticalView;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by nhattruong251295 on 4/7/2017.
 */

public class StatisticalPresenter extends BasePresenter implements IStatisticalPresenter {
    private static final String TAG = makeLogTag(StatisticalPresenter.class);

    public void attachView(IStatisticalView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IStatisticalView getView() {
        return (IStatisticalView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
