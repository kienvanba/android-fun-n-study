package com.horical.gito.mvp.salary.mySalary.detail.view;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.mvp.salary.mySalary.detail.adapter.DetailMySalaryAdapter;
import com.horical.gito.mvp.salary.mySalary.detail.injection.component.DaggerIDetailMySalaryComponent;
import com.horical.gito.mvp.salary.mySalary.detail.injection.component.IDetailMySalaryComponent;
import com.horical.gito.mvp.salary.mySalary.detail.injection.module.DetailMySalaryModule;
import com.horical.gito.mvp.salary.mySalary.detail.presenter.DetailMySalaryPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailMySalaryActivity extends BaseActivity implements View.OnClickListener, IDetailMySalaryView {

    @Bind(R.id.ln_back)
    LinearLayout lnBack;

    @Bind(R.id.tv_title)
    TextView tvTitle;

    @Inject
    DetailMySalaryPresenter mPresenter;

    private DetailMySalaryAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_my_salary);
        ButterKnife.bind(this);

        IDetailMySalaryComponent component = DaggerIDetailMySalaryComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .detailMySalaryModule(new DetailMySalaryModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {

    }

    protected void initListener() {
        lnBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ln_back:
                finish();
                break;

            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}

