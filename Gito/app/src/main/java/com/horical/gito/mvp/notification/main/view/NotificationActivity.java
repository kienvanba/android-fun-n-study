package com.horical.gito.mvp.notification.main.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.notification.main.adapter.NotificationAdapter;
import com.horical.gito.mvp.notification.main.adapter.NotificationSpinnerAdapter;
import com.horical.gito.mvp.notification.main.injection.component.DaggerNotificationComponent;
import com.horical.gito.mvp.notification.main.injection.component.NotificationComponent;
import com.horical.gito.mvp.notification.main.injection.module.NotificationModule;
import com.horical.gito.mvp.notification.main.presenter.NotificationPresenter;
import com.horical.gito.mvp.notification.newNotification.view.NotificationNewActivity;
import com.horical.gito.utils.LogUtils;
import com.horical.gito.widget.textview.GitOTextView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.FROM_MENU;
import static com.horical.gito.AppConstants.KEY_FROM;

public class NotificationActivity extends DrawerActivity implements View.OnClickListener, INotificationView {

    public static final String TAG = LogUtils.makeLogTag(NotificationActivity.class);

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.notification_tv_approved)
    GitOTextView mApproved;

    @Bind(R.id.notification_tv_pending)
    GitOTextView mPending;

    @Bind(R.id.notification_tv_mine)
    GitOTextView mMine;

    @Bind(R.id.notification_lv_info)
    RecyclerView mNotificationList;

    @Bind(R.id.notification_spinner_filter)
    Spinner mFilter;

    NotificationAdapter mNotificationAdapter;
    NotificationSpinnerAdapter mNotificationSpinnerAdapter;

    @Inject
    NotificationPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_notification;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_NOTIFICATION;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        NotificationComponent component = DaggerNotificationComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .notificationModule(new NotificationModule())
                .build();

        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(getString(R.string.notification));
        int from = getIntent().getIntExtra(KEY_FROM, FROM_MENU);
        mPresenter.setFrom(from);
        if (from == FROM_MENU) {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        } else {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
            lockDrawer();
        }
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);

        turnOn(mApproved);

        mNotificationAdapter = new NotificationAdapter(
                this,
                mPresenter.getListApproved()
        );
        mNotificationList.setLayoutManager(
                new LinearLayoutManager(
                        this,
                        LinearLayoutManager.VERTICAL,
                        false
                )
        );
        mNotificationList.setHasFixedSize(true);
        mNotificationList.setAdapter(mNotificationAdapter);
        mNotificationAdapter.notifyDataSetChanged();

        mNotificationSpinnerAdapter = new NotificationSpinnerAdapter(
                this,
                R.layout.item_notification_spinner_selected,
                mPresenter.getListSpinner()
        );
        mFilter.setAdapter(mNotificationSpinnerAdapter);
    }

    protected void initListener() {

        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            public void onLeftClicked() {
                if (mPresenter.getFrom() == FROM_MENU) openDrawer();
                else onBackPressed();
            }

            @Override
            public void onRightButtonOneClicked() {
                Intent intent = new Intent(NotificationActivity.this, NotificationNewActivity.class);
                startActivity(intent);
            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }

        });
        mApproved.setOnClickListener(this);
        mPending.setOnClickListener(this);
        mMine.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mApproved.getId()) {
            OnApprovedClicked();
            return;
        }
        if (v.getId() == mPending.getId()) {
            OnPendingClicked();
            return;
        }
        if (v.getId() == mMine.getId()) {
            OnMineClicked();
            return;
        }
    }

    private void turnOn(TextView view) {
        view.setTextColor(getResources().getColor(R.color.white));
        view.setBackground(getResources().getDrawable(R.drawable.bg_btn_blue_simple));
    }

    private void turnOff(TextView view) {
        view.setTextColor(getResources().getColor(R.color.app_color));
        view.setBackground(getResources().getDrawable(R.drawable.bg_btn_white_simple));
    }

    private void OnApprovedClicked() {
        turnOn(mApproved);
        turnOff(mPending);
        turnOff(mMine);
        mNotificationAdapter.ChangeList(mPresenter.getListApproved());
    }

    private void OnPendingClicked() {
        turnOff(mApproved);
        turnOn(mPending);
        turnOff(mMine);
        mNotificationAdapter.ChangeList(mPresenter.getListPending());
    }

    private void OnMineClicked() {
        turnOff(mApproved);
        turnOff(mPending);
        turnOn(mMine);
        mNotificationAdapter.ChangeList(mPresenter.getListMine());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}

