package com.horical.gito.mvp.myProject.task.details.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.LogTime;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LogTimeAdapter extends RecyclerView.Adapter<LogTimeAdapter.LogtimeHolder> {

    private Context mContext;
    private List<LogTime> mList;
    private OnItemClickListener mOnItemClickListener;

    public LogTimeAdapter(Context context, List<LogTime> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public LogtimeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_task_logtime, parent, false);
        return new LogtimeHolder(view);
    }

    public List<LogTime> getList() {
        return mList;
    }

    public void setList(List<LogTime> mList) {
        this.mList = mList;
    }

    @Override
    public void onBindViewHolder(LogtimeHolder holder, int position) {
        LogTime logTime = mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class LogtimeHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_hour)
        TextView tvHour;

        @Bind(R.id.ll_delete)
        LinearLayout llDelete;

        @Bind(R.id.tv_desc)
        TextView tvDescription;

        @Bind(R.id.ll_edit)
        LinearLayout llEdit;

        View view;

        public LogtimeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}
