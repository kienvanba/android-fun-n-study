package com.horical.gito.mvp.companySetting.list.fragment.quota.presenter;

import android.util.Log;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.companySetting.GetInfoQuotaResponse;
import com.horical.gito.interactor.api.response.companySetting.GetSummaryQuotaResponse;
import com.horical.gito.model.QuotaInfo;
import com.horical.gito.model.QuotaSummary;
import com.horical.gito.mvp.companySetting.list.fragment.quota.view.IQuotaView;

import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class QuotaPresenter extends BasePresenter implements IQuotaPresenter {
    private QuotaInfo quotaInfo;
    private QuotaSummary quotaSummary;

    public void attachView(IQuotaView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IQuotaView getView() {
        return (IQuotaView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        quotaInfo = new QuotaInfo();
        quotaSummary = new QuotaSummary();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public QuotaInfo getQuotaInfo() {
        return quotaInfo;
    }

    public void setQuotaInfo(QuotaInfo quotaInfo) {
        this.quotaInfo = quotaInfo;
    }

    public QuotaSummary getQuotaSummary() {
        return quotaSummary;
    }

    public void setQuotaSummary(QuotaSummary quotaSummary) {
        this.quotaSummary = quotaSummary;
    }

    @Override
    public void getInfoQuota() {
        getApiManager().getInfoQuota(new ApiCallback<GetInfoQuotaResponse>() {

            @Override
            public void success(GetInfoQuotaResponse res) {
                setQuotaInfo(res.quotaInfo);
                getView().getInfoQuotaSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getInfoQuotaFailure(error.message);
            }
        });
    }

    @Override
    public void getSummaryQuota() {
        getApiManager().getSummaryQuota(new ApiCallback<GetSummaryQuotaResponse>() {

            @Override
            public void success(GetSummaryQuotaResponse res) {
                setQuotaSummary(res.quotaSummary);
                getView().getSummaryQuotaSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getSummaryQuotaFailure(error.message);
            }
        });
    }
}

