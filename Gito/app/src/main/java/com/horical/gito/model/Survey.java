package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by thanhle on 3/1/17
 */

public class Survey implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("creatorId")
    @Expose
    private String creatorId;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("modifiDate")
    @Expose
    private String modifiDate;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("listUserSubmit")
    @Expose
    private List<User> listUserSubmit;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("itemcount")
    @Expose
    private int itemCount;

    @SerializedName("userList")
    @Expose
    private List<User> userList;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("type")
    @Expose
    private int type;

    public Survey(int status, int type) {
        this.status = status;
        this.type = type;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getModifiDate() {
        return modifiDate;
    }

    public void setModifiDate(String modifiDate) {
        this.modifiDate = modifiDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public List<User> getListUserSubmit() {
        return listUserSubmit;
    }

    public void setListUserSubmit(List<User> listUserSubmit) {
        this.listUserSubmit = listUserSubmit;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
