package com.horical.gito.mvp.myProject.summary.view;

import com.horical.gito.base.IView;

public interface ISummaryView extends IView {

    void showLoading();

    void hideLoading();

    void onGetDataSuccess(boolean hasData);

    void onGetDataFailure(String error);
}
