package com.horical.gito.interactor.api.response.companySetting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Level;

import java.util.List;

/**
 * Created by thanhle on 3/23/17.
 */

public class GetAllLevelResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Level> levels;
}
