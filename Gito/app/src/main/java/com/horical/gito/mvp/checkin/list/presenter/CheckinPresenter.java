package com.horical.gito.mvp.checkin.list.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.checkin.GetAllCheckinResponse;
import com.horical.gito.mvp.checkin.list.view.dto.ICheckinView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.LOGE;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class CheckinPresenter extends BasePresenter implements ICheckinPresenter {
    private List<IRecyclerItem> mCheckin;
    private int from;

    private static final String TAG = makeLogTag(CheckinPresenter.class);

    public ICheckinView getView() {
        return (ICheckinView) getIView();
    }

    public void attachView(ICheckinView view) {
        super.attachView(view);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mCheckin = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public List<IRecyclerItem> getCheckin() {
        return mCheckin;
    }

    public void setCheckin(List<IRecyclerItem> checkin) {
        this.mCheckin = checkin;
    }

    public void getAllCheckin() {
        getView().showLoading();

        getApiManager().getAllCheckin(new ApiCallback<GetAllCheckinResponse>() {
            @Override
            public void success(GetAllCheckinResponse res) {
                if (!isViewAttached()) return;
                getView().hideLoading();
                if (res == null) return;

                mCheckin.clear();
                getView().getAllCheckinSuccess();
            }

            @Override
            public void failure(RestError error) {
                if (!isViewAttached()) return;
                getView().hideLoading();
                LOGE(TAG, error.message);
            }
        });
    }

}


