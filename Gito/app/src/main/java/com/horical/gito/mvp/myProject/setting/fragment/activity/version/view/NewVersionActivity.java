package com.horical.gito.mvp.myProject.setting.fragment.activity.version.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.interactor.api.request.version.Request;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.injection.component.VersionComponent;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.injection.module.VersionModule;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.presenter.VersionPresenter;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.injection.component.DaggerVersionComponent;
import com.horical.gito.utils.HDate;
import com.horical.gito.widget.chartview.line.TimeLineView;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewVersionActivity extends BaseActivity implements IVersionView, View.OnClickListener {
    @Bind(R.id.edit_name)
    EditText edName;
    @Bind(R.id.edit_description)
    EditText edDescription;
    @Bind(R.id.edit_release_note)
    EditText edReleaseNote;
    @Bind(R.id.btn_add_file)
    ImageButton btnAddFiles;
    @Bind(R.id.rcv_file_release)
    RecyclerView rcvFiles;
    @Bind(R.id.btn_submit)
    ImageButton btnSubmit;
    @Bind(R.id.top_bar)
    CustomViewTopBar topBar;
    @Bind(R.id.time_line)
    TimeLineView timeLine;

    @Inject
    VersionPresenter mPresenter;

    private Request request;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_version);
        ButterKnife.bind(this);

        VersionComponent component = DaggerVersionComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .versionModule(new VersionModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        setupTopBar();
        request = new Request();
    }

    private void setupTopBar() {
        topBar.setTextTitle(getResources().getString(R.string.new_version));
        topBar.setTextViewLeft(getResources().getString(R.string.cancel));
        topBar.setTextViewRight(getResources().getString(R.string.save));
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {
                if (!edName.getText().toString().trim().equals("")) {
                    request.name = edName.getText().toString();
                } else {
                    showToast("name is empty!");
                    return;
                }
                request.desc = edDescription.getText().toString();
                request.releaseNote = edReleaseNote.getText().toString();
                request.status = 0;
                request.versionId = edName.getText().toString().trim().toLowerCase().replaceAll("\\W", "");
                mPresenter.createVersion(request);
            }
        });
        btnSubmit.setOnClickListener(this);
        btnAddFiles.setOnClickListener(this);
        timeLine.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.time_line:
                final DatePickerDialog dialog = new DatePickerDialog(this);
                if (request.endDate != null && request.startDate != null) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(request.startDate);
                    dialog.dpStart.updateDate(
                            c.get(Calendar.YEAR),
                            c.get(Calendar.MONTH),
                            c.get(Calendar.DAY_OF_MONTH)
                    );
                    c.setTime(request.endDate);
                    dialog.dpEnd.updateDate(
                            c.get(Calendar.YEAR),
                            c.get(Calendar.MONTH),
                            c.get(Calendar.DAY_OF_MONTH)
                    );
                }
                dialog.setSaveClickListener(new DatePickerDialog.OnSaveClickListener() {
                    @Override
                    public void onClick() {
                        Calendar date = Calendar.getInstance();
                        /** start date */
                        date.set(
                                dialog.dpStart.getYear(),
                                dialog.dpStart.getMonth(),
                                dialog.dpStart.getDayOfMonth()
                        );
                        timeLine.setDateStart(new HDate(date));
                        request.startDate = date.getTime();
                        /** end date */
                        date.set(
                                dialog.dpEnd.getYear(),
                                dialog.dpEnd.getMonth(),
                                dialog.dpEnd.getDayOfMonth()
                        );
                        timeLine.setDateRelease(new HDate(date));
                        request.endDate = date.getTime();
                        /** validate function */
                        timeLine.invalidate();
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
        }
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void onRequestSuccess() {
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}
