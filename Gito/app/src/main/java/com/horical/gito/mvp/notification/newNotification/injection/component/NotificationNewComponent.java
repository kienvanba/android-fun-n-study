package com.horical.gito.mvp.notification.newNotification.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.notification.newNotification.injection.module.NotificationNewModule;
import com.horical.gito.mvp.notification.newNotification.view.NotificationNewActivity;

import javax.inject.Inject;

import dagger.Component;

/**
 * Created by Dragonoid on 3/24/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = NotificationNewModule.class)
public interface NotificationNewComponent {
    void inject(NotificationNewActivity activity);
}
