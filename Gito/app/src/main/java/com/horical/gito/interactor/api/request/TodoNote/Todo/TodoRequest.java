package com.horical.gito.interactor.api.request.TodoNote.Todo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Luong on 03-Apr-17.
 */

public class TodoRequest {

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("important")
    @Expose
    public Boolean important;

    @SerializedName("attachFiles")
    @Expose
    public List<String> attachFiles = new ArrayList<>();
}
