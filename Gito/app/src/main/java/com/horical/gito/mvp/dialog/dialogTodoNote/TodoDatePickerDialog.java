package com.horical.gito.mvp.dialog.dialogTodoNote;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;

import com.horical.gito.R;
import com.horical.gito.base.BaseDialog;
import com.horical.gito.interactor.prefer.PreferManager;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;


/**
 * Created by Luong on 30-Mar-17.
 */

public class TodoDatePickerDialog extends BaseDialog implements View.OnClickListener, DatePicker.OnDateChangedListener {


    private Context mContext;
    DateDialogCallback mcallback;

    @Inject
    PreferManager mPreferManager;

    public static final String BUNDLE = "bundel";

    @Bind(R.id.dpk_todo_from)
    DatePicker dpFrom;

    @Bind(R.id.dpk_todo_to)
    DatePicker dpTo;

    @Bind(R.id.tv_todo_from)
    GitOTextView tvFrom;

    @Bind(R.id.tv_todo_to)
    GitOTextView tvTo;

    @Bind(R.id.tv_todo_filter_cancel)
    GitOTextView tvCancel;

    @Bind(R.id.tv_todo_filter_done)
    GitOTextView tvDone;

    public TodoDatePickerDialog(final Context context, DateDialogCallback callback) {
        super(context);
        mContext = context;
        mcallback = callback;
        getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_todo_filter_datepicker;
    }

    @Override
    protected void initListener() {
        tvFrom.setOnClickListener(this);
        tvTo.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvDone.setOnClickListener(this);
        Calendar c = Calendar.getInstance();
        dpFrom.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), this);
        dpTo.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), this);
    }

    public Date getFrom() {
        Date Date = new Date(
                dpFrom.getYear(), dpFrom.getMonth(), dpFrom.getDayOfMonth());
        return Date;
    }

    public void setFrom(Date Date) {
        dpFrom.updateDate(Date.getYear(), Date.getMonth(), Date.getDay());
    }

    public Date getTo() {
        Date Date = new Date(
                dpTo.getYear(), dpTo.getMonth(), dpTo.getDayOfMonth());
        return Date;
    }

    public void setTo(Date Date) {
        dpTo.updateDate(Date.getYear(), Date.getMonth(), Date.getDay());
    }

    private boolean isSmallerDate(Date var1, Date var2) {
        if (!(var1.getYear() == var2.getYear())) {
            return var1.getYear() < var2.getYear();
        }
        if (!(var1.getMonth() == var2.getMonth())) {
            return var1.getMonth() < var2.getMonth();
        }
        if (!(var1.getDay() == var2.getDay())) {
            return var1.getDay() < var2.getDay();
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_todo_filter_cancel:
                mcallback.onCancelClick();
                cancel();
                break;
            case R.id.tv_todo_filter_done:
                mcallback.onDoneClick(getFrom(), getTo());
                cancel();
                break;
        }
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (view.getId() == dpFrom.getId()) {
            if (isSmallerDate(getTo(), getFrom())) setTo(getFrom());
            return;
        }
        if (view.getId() == dpTo.getId()) {
            if (isSmallerDate(getTo(), getFrom())) setFrom(getTo());
            return;
        }
    }

    public interface DateDialogCallback {
        void onDoneClick(Date DateFrom, Date DateTo);

        void onCancelClick();
    }

}
