package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.presenter.CommitPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */
@Module
public class CommitModule {
    @PerFragment
    @Provides
    CommitPresenter provideCodeCommitPresenter(){
        return new CommitPresenter();
    }
}
