package com.horical.gito.interactor.api.request.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.model.TokenFile;

public class CreateProjectRequest {

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("projectId")
    @Expose
    public String projectId;

    @SerializedName("desc")
    @Expose
    public String desc;

    @SerializedName("avatar")
    @Expose
    public TokenFile avatar;

    public CreateProjectRequest(String name, String projectId, String desc, TokenFile avatar) {
        this.name = name;
        this.projectId = projectId;
        this.desc = desc;
        this.avatar = avatar;
    }
}
