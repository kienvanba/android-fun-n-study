package com.horical.gito.mvp.login.presenter;

import com.google.gson.Gson;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.user.LoginRequest;
import com.horical.gito.interactor.api.response.user.LoginResponse;
import com.horical.gito.mvp.login.view.ILoginView;
import com.horical.gito.utils.EmailUtils;

public class LoginPresenter extends BasePresenter implements ILoginPresenter {

    public ILoginView getView() {
        return (ILoginView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void login(String email, String password, String deviceName, String deviceDescription, String deviceOS, String deviceOSVersion, String deviceAPNSToken) {
        if (!isViewAttached()) return;

        if (email == null || email.length() == 0 || password == null || password.length() == 0) {
            getView().errorEmptyInput();
            return;
        }
        if (!EmailUtils.isValidEmail(email)) {
            getView().errorEmailInvalid();
            return;
        }

        getView().showLoading();

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.email = email;
        loginRequest.password = password;
        loginRequest.deviceName = deviceName;
        loginRequest.deviceDescription = deviceDescription;
        loginRequest.deviceOS = deviceOS;
        loginRequest.deviceOSVersion = deviceOSVersion;
        loginRequest.deviceAPNSToken = deviceAPNSToken;
        loginRequest.deviceFcmToken = getPreferManager().getFcmToken();

        getApiManager().login(loginRequest, new ApiCallback<LoginResponse>() {

            @Override
            public void success(LoginResponse res) {
                if (!isViewAttached()) return;
                getView().hideLoading();
                getView().loginSuccess();
                getPreferManager().setUser(new Gson().toJson(res.results.user));
                getPreferManager().setToken(res.results.token);
            }

            @Override
            public void failure(RestError error) {
                if (!isViewAttached()) return;
                getView().hideLoading();
                getView().loginError(error);
            }
        });
    }
}
