package com.horical.gito.model.estate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.model.Company;
import com.horical.gito.model.TokenFile;

import java.io.Serializable;
import java.util.List;

/**
 * Created by hoangsang on 11/8/16
 */

public class Estate implements Serializable {

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("creatorId")
    @Expose
    public String creatorId;

    @SerializedName("companyId")
    @Expose
    public Company companyId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("estateId")
    @Expose
    public String estateId;

    @SerializedName("quanlity")
    @Expose
    public int quanlity;

    @SerializedName("tag")
    @Expose
    public String tag;

    @SerializedName("image")
    @Expose
    public List<TokenFile> images;

    @SerializedName("lastCheckDate")
    @Expose
    public String lastCheckDate;

    @SerializedName("createdDate")
    @Expose
    public String createdDate;

    @SerializedName("modifiedDate")
    @Expose
    public String modifiedDate;

    @SerializedName("type")
    @Expose
    public EstateType type;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("desc")
    @Expose
    public String desc;
}
