package com.horical.gito.mvp.myProject.sourceCode.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.myProject.sourceCode.detail.presenter.SourceCodeDetailPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */
@Module
public class SourceCodeDetailModule {
    @PerActivity
    @Provides
    SourceCodeDetailPresenter provideCodeDetailPresenter(){
        return new SourceCodeDetailPresenter();
    }
}
