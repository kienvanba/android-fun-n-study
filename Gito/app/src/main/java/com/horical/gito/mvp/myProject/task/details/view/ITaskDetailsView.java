package com.horical.gito.mvp.myProject.task.details.view;

import com.horical.gito.base.IView;
import com.horical.gito.model.Task;

public interface ITaskDetailsView extends IView {

    void showLoading();

    void hideLoading();

    void createTaskSuccess(Task task);

    void createTaskFailure(String error);

    void updateTaskOverViewSuccess(Task task);

    void updateTaskOverViewFailure(String error);
}
