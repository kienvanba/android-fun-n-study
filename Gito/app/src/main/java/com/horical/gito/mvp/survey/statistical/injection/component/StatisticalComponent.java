package com.horical.gito.mvp.survey.statistical.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.survey.statistical.injection.module.StatisticalModule;
import com.horical.gito.mvp.survey.statistical.view.StatisticalActivity;

import dagger.Component;

/**
 * Created by nhattruong251295 on 4/7/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = StatisticalModule.class)
public interface StatisticalComponent {
    void inject(StatisticalActivity activity);
}
