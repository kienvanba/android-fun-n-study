package com.horical.gito.mvp.user.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.user.list.presenter.UserPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 15-Dec-16.
 */
@Module
public class UserModule {
    @PerActivity
    @Provides
    UserPresenter userPresenter(){
        return  new UserPresenter();
    }
}
