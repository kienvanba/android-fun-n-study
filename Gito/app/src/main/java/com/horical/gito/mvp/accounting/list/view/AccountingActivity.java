package com.horical.gito.mvp.accounting.list.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.model.Accounting;
import com.horical.gito.mvp.accounting.detail.view.AccountingDetailActivity;
import com.horical.gito.mvp.accounting.list.adapter.AccountingAdapter;
import com.horical.gito.mvp.accounting.list.injection.component.AccountingComponent;
import com.horical.gito.mvp.accounting.list.injection.component.DaggerAccountingComponent;
import com.horical.gito.mvp.accounting.list.injection.module.AccountingModule;
import com.horical.gito.mvp.accounting.list.presenter.AccountingPresenter;
import com.horical.gito.mvp.dialog.dialogAccounting.CreateAccountingDialog;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.widget.edittext.GitOEditText;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class AccountingActivity extends DrawerActivity implements View.OnClickListener, IAccountingView {

    public final static int REQUEST_CODE_ADD_EDIT_ACCOUNTING = 1;
    private static final String TAG = makeLogTag(AccountingActivity.class);


    @Bind(R.id.accounting_menu)
    LinearLayout mListMenu;

    @Bind(R.id.accounting_tv_title)
    GitOTextView mTvTitle;

    @Bind(R.id.accounting_ln_search)
    LinearLayout mLnSearch;
    @Bind(R.id.imv_accounting_cancel_search)
    ImageView mImvCancelSearch;
    @Bind(R.id.edt_accounting_search)
    GitOEditText mEdtSearch;
    @Bind(R.id.imv_accounting_search)
    ImageView mImvSearch;
    @Bind(R.id.imv_accounting_add)
    ImageView mImvAdd;
    @Bind(R.id.imv_accounting_filter)
    ImageView mImvFilter;

    @Bind(R.id.rdb_btn_daily)
    RadioButton rbDaily;
    @Bind(R.id.rdb_btn_prePayment)
    RadioButton rbPrePayment;
    @Bind(R.id.rdb_btn_report_type)
    RadioButton rbReportType;

    @Bind(R.id.rcv_accounting)
    RecyclerView mRCVAccounting;

    @Bind(R.id.accounting_refresh_layout)
    SwipeRefreshLayout mRefresh;

    AccountingAdapter mAccountingAdapter;

    @Inject
    AccountingPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_accounting;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_ACCOUNTING;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        AccountingComponent component = DaggerAccountingComponent.builder()
                .accountingModule(new AccountingModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        mAccountingAdapter = new AccountingAdapter(this, mPresenter.getListAccountings());
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRCVAccounting.setLayoutManager(llm);
        mRCVAccounting.setAdapter(mAccountingAdapter);

        showLoading();
        mPresenter.getAllAccounting();
    }

    protected void initListener() {
        mListMenu.setOnClickListener(this);
        mImvCancelSearch.setOnClickListener(this);
        mImvSearch.setOnClickListener(this);
        mImvFilter.setOnClickListener(this);
        mImvAdd.setOnClickListener(this);

        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                mPresenter.getAllAccounting();
            }
        });

        mAccountingAdapter.setOnItemClickListener(new AccountingAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(AccountingActivity.this, AccountingDetailActivity.class);
                intent.putExtra(AccountingDetailActivity.ARG_ACCOUNTING, mPresenter.getListAccountings().get(position));
                startActivityForResult(intent, REQUEST_CODE_ADD_EDIT_ACCOUNTING);
            }

            @Override
            public void onDelete(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AccountingActivity.this);
                builder.setMessage(R.string.text_show);
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                showDialog("Loading...");
                                mPresenter.deleteAccounting(mAccountingAdapter.getListAccounting().get(position).getId(), position);
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateAdapterFromAllAccounting();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void updateAdapterFromAllAccounting() {
        List<Accounting> accountings = mPresenter.getListSearchAccounting(mEdtSearch.getText().toString());
        mAccountingAdapter.setListAccounting(accountings);
        mAccountingAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_accounting_search:
                mTvTitle.setVisibility(View.GONE);
                mImvAdd.setVisibility(View.GONE);
                mImvSearch.setVisibility(View.GONE);
                mImvFilter.setVisibility(View.GONE);

                mLnSearch.setVisibility(View.VISIBLE);
                mLnSearch.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_from_right));
                break;
            case R.id.imv_accounting_cancel_search:
                mTvTitle.setVisibility(View.VISIBLE);
                mImvAdd.setVisibility(View.VISIBLE);
                mImvSearch.setVisibility(View.VISIBLE);
                mImvFilter.setVisibility(View.VISIBLE);

                mLnSearch.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_from_left));
                mLnSearch.setVisibility(View.INVISIBLE);
                break;
            case R.id.accounting_menu:
                openDrawer();
                break;
            case R.id.imv_accounting_add:
                CreateAccountingDialog dialog = new CreateAccountingDialog(this);
                dialog.show();
                break;
            case R.id.imv_accounting_filter:

                ///cai nay lam dialog nhe e
//                Intent filter = new Intent(this, AccountingFilterActivity.class);
//                startActivity(filter);
                break;
        }
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllAccountingSuccess() {
        dismissDialog();
        mRefresh.setRefreshing(false);
        mAccountingAdapter.notifyDataSetChanged();
        showToast("Success");
        updateAdapterFromAllAccounting();
    }

    @Override
    public void getAllAccountingFailure(String error) {
        dismissDialog();
        mRefresh.setRefreshing(false);
        showToast("Failure");
        updateAdapterFromAllAccounting();
    }

    @Override
    public void deleteAccountingSuccess(int position) {
        hideLoading();
        showToast("Delete Success");
        mAccountingAdapter.getListAccounting().remove(position);
        mAccountingAdapter.notifyItemRemoved(position);
        mAccountingAdapter.notifyItemRangeChanged(position, mAccountingAdapter.getItemCount());
    }

    @Override
    public void deleteAccountingFailure() {
        hideLoading();
        showToast("failure");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            showLoading();
            mPresenter.getAllAccounting();
        }

    }
}
