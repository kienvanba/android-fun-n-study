package com.horical.gito.mvp.recruitment.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.recruitment.injection.module.RecruitmentModule;
import com.horical.gito.mvp.recruitment.view.RecruitmentActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class , modules = RecruitmentModule.class)
public interface RecruitmentComponent {
    void inject(RecruitmentActivity activity);
}
