package com.horical.gito.mvp.todolist.todo.detail.presenter;

import java.io.File;

import okhttp3.MediaType;

/**
 * Created by Luong on 16-Mar-17.
 */

public interface ITodoListDetailPresenter {

    void createTodo(String title);

    void updateTodo(String todoId, String title);

    void uploadFile(MediaType type, File file);
}
