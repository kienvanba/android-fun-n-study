package com.horical.gito.mvp.roomBooking.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Room;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RoomBookingAdapter extends RecyclerView.Adapter<RoomBookingAdapter.RoomBookingHolder> {

    private List<Room> mList;
    private Context mContext;

    public RoomBookingAdapter(Context mContext, List<Room> mList) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public RoomBookingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_room, parent, false);
        return new RoomBookingHolder(view);
    }

    @Override
    public void onBindViewHolder(RoomBookingHolder holder, final int position) {
        Room room = mList.get(position);
        holder.tvTitle.setText(room.getName());

        if (room.getDesc() == null){
            holder.rlRoomDes.setVisibility(View.GONE);
        } else {
            holder.tvDes.setText(room.getDesc());
        }


        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickListener(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class RoomBookingHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.rl_room_des)
        RelativeLayout rlRoomDes;

        @Bind(R.id.tv_description_title_room)
        TextView tvDes;

        @Bind(R.id.ll_next_room)
        LinearLayout llNext;

        @Bind(R.id.tv_title_room)
        TextView tvTitle;

        View view;

        public RoomBookingHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener callBack) {
        this.mOnItemClickListener = callBack;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}