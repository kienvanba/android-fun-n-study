package com.horical.gito.interactor.api.response.accounting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Accounting;

/**
 * Created by Luong on 14-Mar-17
 */

public class GetCreateAccountingResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    public Accounting accounting;
}
