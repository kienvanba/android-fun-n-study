package com.horical.gito.mvp.companySetting.list.fragment.info.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.list.fragment.info.presenter.InfoPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class InfoModule {
    @PerFragment
    @Provides
    InfoPresenter provideInfoPresenter(){
        return new InfoPresenter();
    }
}
