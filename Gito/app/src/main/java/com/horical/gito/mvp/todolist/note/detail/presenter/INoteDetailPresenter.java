package com.horical.gito.mvp.todolist.note.detail.presenter;

import java.io.File;

import okhttp3.MediaType;

/**
 * Created by Luong on 28-Mar-17.
 */

public interface INoteDetailPresenter {
    void createNote(String title, String content);

    void updateNote(String noteId, String title, String content);

    void uploadFile(MediaType type, File file);
}
