package com.horical.gito.mvp.companySetting.list.fragment.info.presenter;

import java.io.File;

import okhttp3.MediaType;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IInfoPresenter {
    void getCompanyInfo();
    void uploadFile(MediaType type, File file);
    void updateInfo();
}
