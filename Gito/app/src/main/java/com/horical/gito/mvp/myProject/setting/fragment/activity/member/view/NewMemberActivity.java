package com.horical.gito.mvp.myProject.setting.fragment.activity.member.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.adapter.NewMemberAdapter;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.adapter.RoleProjectItemBody;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.adapter.UserItemBody;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.injection.component.DaggerUserComponent;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.injection.component.UserComponent;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.injection.module.UserModule;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.presenter.NewMemberPresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewMemberActivity extends BaseActivity implements INewMemberView {
    @Bind(R.id.top_bar)
    CustomViewTopBar topBar;
    @Bind(R.id.edit_search)
    EditText edSearch;
    @Bind(R.id.rcv_member)
    RecyclerView rcvMember;
    @Bind(R.id.rcv_role)
    RecyclerView rcvRole;

    @Inject
    NewMemberPresenter mPresenter;

    private List<IRecyclerItem> userItems, roleItems;
    private NewMemberAdapter userAdapter, roleAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);
        ButterKnife.bind(this);

        UserComponent component = DaggerUserComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .userModule(new UserModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        userItems = mPresenter.getUsers();
        roleItems = mPresenter.getRoles();
        setTopBar();
        mPresenter.getAllUsers();
        mPresenter.getAllRoles();
        userAdapter = new NewMemberAdapter(userItems, this);
        RecyclerView.LayoutManager userLayoutM =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcvMember.setLayoutManager(userLayoutM);
        rcvMember.setAdapter(userAdapter);

        roleAdapter = new NewMemberAdapter(roleItems, this);
        RecyclerView.LayoutManager roleLayoutM =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcvRole.setLayoutManager(roleLayoutM);
        rcvRole.setAdapter(roleAdapter);
    }

    protected void initListener() {
        userAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                UserItemBody userBody = (UserItemBody) userItems.get(position);
                userBody.setChecked(!userBody.isChecked());
                userAdapter.notifyDataSetChanged();
            }
        });
        roleAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                RoleProjectItemBody roleBody = (RoleProjectItemBody) roleItems.get(position);
                roleAdapter.setCurrentRolePosition(position);
                roleAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void getAllUserSuccess() {
        userItems = mPresenter.getUsers();
        userAdapter.notifyDataSetChanged();
    }

    @Override
    public void getAllRoleSuccess() {
        roleItems = mPresenter.getRoles();
        roleAdapter.notifyDataSetChanged();
    }

    private void setTopBar(){
        topBar.setTextTitle(getString(R.string.add_member));
        topBar.setTextViewLeft(getString(R.string.cancel));
        topBar.setTextViewRight(getString(R.string.done));
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}
