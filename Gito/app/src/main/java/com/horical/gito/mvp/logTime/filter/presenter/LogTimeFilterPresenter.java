package com.horical.gito.mvp.logTime.filter.presenter;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.logTime.filter.view.ILogTimeFilterView;
import com.horical.gito.mvp.popup.dateSelected.DateDto;
import com.horical.gito.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

public class LogTimeFilterPresenter extends BasePresenter implements ILogTimeFilterPresenter {


    private List<DateDto> mList;

    private ILogTimeFilterView getView() {
        return (ILogTimeFilterView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        mList = new ArrayList<>();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void onCreateData(DateDto currentDateFilter) {
        String[] typeDateRange = MainApplication.mContext.getResources().getStringArray(R.array.date_range_array);
        // Yesterday
        mList.add(new DateDto(typeDateRange[0], DateUtils.getYesterdayStart(), DateUtils.getYesterdayEnd()));
        // Today
        mList.add(new DateDto(typeDateRange[1], DateUtils.getTodayStart(), DateUtils.getTodayEnd()));
        // Last Week
        String[] lastWeek = DateUtils.getLastWeek();
        mList.add(new DateDto(typeDateRange[2], DateUtils.getStartDate2(lastWeek[0]), DateUtils.getEndDate2(lastWeek[lastWeek.length - 1])));
        // This Week
        String[] thisWeek = DateUtils.getThisWeek();
        mList.add(new DateDto(typeDateRange[3], DateUtils.getStartDate2(thisWeek[0]), DateUtils.getEndDate2(thisWeek[thisWeek.length - 1])));
        // Last Month
        String[] lastMonth = DateUtils.getLastMonth();
        mList.add(new DateDto(typeDateRange[4], DateUtils.getStartDate2(lastMonth[0]), DateUtils.getEndDate2(lastMonth[lastMonth.length - 1])));
        // This Month
        String[] thisMonth = DateUtils.getThisMonth();
        mList.add(new DateDto(typeDateRange[5], DateUtils.getStartDate2(thisMonth[0]), DateUtils.getEndDate2(thisMonth[thisMonth.length - 1])));
        //this custom
        if (currentDateFilter != null
                && currentDateFilter.getTypeDate().equalsIgnoreCase(typeDateRange[6]))
            mList.add(currentDateFilter);
        else
            mList.add(new DateDto(typeDateRange[6], null, null));

        if (currentDateFilter == null) {
            mList.get(1).setSelected(true);
        } else {
            for (DateDto dateDto : mList) {
                if (dateDto.getTypeDate().equalsIgnoreCase(currentDateFilter.getTypeDate()))
                    dateDto.setSelected(true);
            }
        }
    }

    @Override
    public void setCustomDate(DateDto dateDto) {
        DateDto custom = mList.get(mList.size() - 1);
        custom.setStart(dateDto.getStart());
        custom.setEnd(dateDto.getEnd());
    }

    public List<DateDto> getList() {
        return mList;
    }

}

