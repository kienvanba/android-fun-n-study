package com.horical.gito.mvp.user.list.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 15-Dec-16.
 */

public interface IUserView extends IView {

    void showLoading();

    void hideLoading();

    void getAllUserSuccess();

    void getAllUserFailure(String error);

}
