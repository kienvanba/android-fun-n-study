package com.horical.gito.mvp.dialog.dialogInputEdit;


import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.BaseDialog;

import butterknife.Bind;

public class InputEditDialog extends BaseDialog implements View.OnClickListener {

    @Bind(R.id.edt_input)
    EditText edtInput;

    @Bind(R.id.tv_cancel)
    TextView tvCanCel;

    @Bind(R.id.tv_done)
    TextView tvDone;

    private String text;
    private OnDoneListener mCallback;

    public InputEditDialog(Context context, String text, OnDoneListener callBack) {
        super(context);
        this.text = text;
        this.mCallback = callBack;
    }

    @Override
    public int getLayoutId() {
        return R.layout.layout_dialog_input_edit;
    }

    @Override
    protected void initData() {
        edtInput.setText(text);
        edtInput.setSelection(edtInput.getText().length());
    }

    @Override
    protected void initListener() {
        tvCanCel.setOnClickListener(this);
        tvDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_done:
                mCallback.onDone(edtInput.getText().toString());
                break;
        }
    }

    public interface OnDoneListener {
        void onDone(String text);
    }

}
