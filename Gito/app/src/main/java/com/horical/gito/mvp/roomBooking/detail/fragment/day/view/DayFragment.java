package com.horical.gito.mvp.roomBooking.detail.fragment.day.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.roomBooking.detail.fragment.day.adapter.DayAdapter;
import com.horical.gito.mvp.roomBooking.detail.fragment.day.injection.component.DaggerDayComponent;
import com.horical.gito.mvp.roomBooking.detail.fragment.day.injection.component.DayComponent;
import com.horical.gito.mvp.roomBooking.detail.fragment.day.injection.module.DayModule;
import com.horical.gito.mvp.roomBooking.detail.fragment.day.presenter.DayPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DayFragment extends BaseFragment implements IDayView, View.OnClickListener {
    @Bind(R.id.imv_back_day)
    ImageView mBtnBack;

    @Bind(R.id.imv_next_day)
    ImageView mBtnNext;

    @Bind(R.id.tv_day)
    TextView mBtnDay;

    @Bind(R.id.rv_hour_detail)
    RecyclerView mRvHourDetail;

    @Inject
    DayPresenter mPresenter;

    DayAdapter mDayAdapter;

    public static DayFragment newInstance() {
        Bundle args = new Bundle();
        DayFragment fragment = new DayFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_day;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        DayComponent component = DaggerDayComponent.builder()
                .dayModule(new DayModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }


    @Override
    protected void initData() {
        super.initData();
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRvHourDetail.setLayoutManager(llm);
        mRvHourDetail.setAdapter(mDayAdapter);

    }

    @Override
    protected void initListener() {
        super.initListener();
    }

    @Override
    public void onClick(View v) {

    }
}
