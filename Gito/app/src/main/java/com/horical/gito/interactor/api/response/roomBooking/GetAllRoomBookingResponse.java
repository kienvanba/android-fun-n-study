package com.horical.gito.interactor.api.response.roomBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Room;

import java.util.List;

/**
 * Created by Lemon on 3/7/2017
 */

public class GetAllRoomBookingResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Room> room;
}
