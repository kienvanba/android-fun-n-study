package com.horical.gito.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class PermissionUtils {

    public static boolean requestPermissions(Activity activity, String[] permissions, int requestCode) {
        List<String> notPermissions = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                notPermissions.add(permission);
            }
        }
        if (notPermissions.size() > 0) {
            ActivityCompat.requestPermissions(activity, notPermissions.toArray(new String[notPermissions.size()]), requestCode);
            return false;
        }
        return true;
    }

    public static boolean checkPermissions(Activity activity, String[] permissions) {
        List<String> notPermissions = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                notPermissions.add(permission);
            }
        }
        return notPermissions.size() == 0;
    }

    public static void goToAppSetting(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        context.startActivity(intent);
    }

}
