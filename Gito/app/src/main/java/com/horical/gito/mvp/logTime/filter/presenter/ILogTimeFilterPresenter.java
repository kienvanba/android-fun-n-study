package com.horical.gito.mvp.logTime.filter.presenter;

import com.horical.gito.mvp.popup.dateSelected.DateDto;

public interface ILogTimeFilterPresenter {

    void onCreateData(DateDto dateDto);

    void setCustomDate(DateDto dateDto);
}
