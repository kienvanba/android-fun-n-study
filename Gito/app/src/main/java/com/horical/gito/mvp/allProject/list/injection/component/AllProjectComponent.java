package com.horical.gito.mvp.allProject.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.allProject.list.injection.module.AllProjectModule;
import com.horical.gito.mvp.allProject.list.view.AllProjectActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = AllProjectModule.class)
public interface AllProjectComponent {
    void inject(AllProjectActivity activity);
}