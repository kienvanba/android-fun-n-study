package com.horical.gito.mvp.caseStudy.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.caseStudy.detail.presenter.CaseStudyDetailPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class CaseStudyDetailModule {

    @PerActivity
    @Provides
    CaseStudyDetailPresenter provideCaseStudyPresenter() {
        return new CaseStudyDetailPresenter();
    }
}
