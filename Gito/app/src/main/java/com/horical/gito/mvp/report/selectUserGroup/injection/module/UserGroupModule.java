package com.horical.gito.mvp.report.selectUserGroup.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.report.selectUserGroup.presenter.UserGroupPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 15-Dec-16.
 */
@Module
public class UserGroupModule {
    @PerActivity
    @Provides
    UserGroupPresenter userPresenter(){
        return  new UserGroupPresenter();
    }
}
