package com.horical.gito.mvp.report.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Reports;
import com.horical.gito.mvp.report.view.ReportActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ReportByDateAdapter extends RecyclerView.Adapter<ReportByDateAdapter.ViewHolderByDate> {

    private Context context;
    private List<Reports> list;

    public ReportByDateAdapter(Context context, List<Reports> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolderByDate onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_report_by_date, parent, false);
        return new ViewHolderByDate(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderByDate holder, final int position) {
        holder.tvDateCreate.setText(list.get(position).showDate());
        holder.tvStatus.setText(list.get(position).showStatus(context));
        holder.tvDoing.setText(list.get(position).getDoing());
        holder.tvProblem.setText(list.get(position).getProblem());
        holder.tvFinish.setText(list.get(position).getFinished());
        holder.tvReasonForRejected.setText(list.get(position).getRejectMsg());

        if (list.get(position).getStatus() == ReportActivity.STATUS_REJECTED) {
            holder.lnContentReject.setVisibility(View.VISIBLE);
        } else {
            holder.lnContentReject.setVisibility(View.GONE);
        }

        if (list.get(position).getStatus() == ReportActivity.STATUS_SUBMITTED) {
            holder.lnContentSubmitted.setVisibility(View.VISIBLE);
        } else {
            holder.lnContentSubmitted.setVisibility(View.GONE);
        }

        if (list.get(position).getStatus() == ReportActivity.STATUS_NEW) {
            holder.lnContentNew.setVisibility(View.VISIBLE);
        } else {
            holder.lnContentNew.setVisibility(View.GONE);
        }

        switch (list.get(position).getStatus()) {
            case ReportActivity.STATUS_NEW:
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.yellow));
                break;
            case ReportActivity.STATUS_APPROVED:
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.green_approved));
                break;

            case ReportActivity.STATUS_REJECTED:
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.red_reject));
                break;

            case ReportActivity.STATUS_SUBMITTED:
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.green_doing));
                break;
        }

        if (list.get(position).getStatus() == ReportActivity.STATUS_APPROVED || list.get(position).getStatus() == ReportActivity.STATUS_SUBMITTED) {
            holder.imvEditDoing.setVisibility(View.INVISIBLE);
            holder.imvEditProblem.setVisibility(View.INVISIBLE);
            holder.imvEditFinish.setVisibility(View.INVISIBLE);
        } else {
            holder.imvEditDoing.setVisibility(View.VISIBLE);
            holder.imvEditProblem.setVisibility(View.VISIBLE);
            holder.imvEditFinish.setVisibility(View.VISIBLE);
        }

        holder.imvEditDoing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallback != null) {
                    mCallback.onEditDoing(position);

                }
            }
        });

        holder.imvEditProblem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallback != null) {
                    mCallback.onEditProblem(position);

                }
            }
        });

        holder.imvEditFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallback != null) {
                    mCallback.onEditFinish(position);

                }
            }
        });

        holder.tvReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallback != null) {
                    mCallback.onButtonReject(position);

                }
            }
        });

        holder.tvApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallback != null) {
                    mCallback.onButtonApprove(position);

                }
            }
        });

        holder.tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallback != null) {
                    mCallback.onButtonSubmit(position);

                }
            }
        });


    }

    public void setOnItemClickListener(OnClickItemListener callback) {
        this.mCallback = callback;
    }

    public OnClickItemListener mCallback;

    public interface OnClickItemListener {
        void onEditDoing(int position);

        void onEditProblem(int position);

        void onEditFinish(int position);

        void onButtonReject(int position);

        void onButtonApprove(int position);

        void onButtonSubmit(int position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolderByDate extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_date_create)
        TextView tvDateCreate;

        @Bind(R.id.tv_status)
        TextView tvStatus;

        @Bind(R.id.imv_edit_doing)
        ImageView imvEditDoing;

        @Bind(R.id.tv_doing)
        TextView tvDoing;

        @Bind(R.id.imv_edit_problem)
        ImageView imvEditProblem;

        @Bind(R.id.tv_problem)
        TextView tvProblem;

        @Bind(R.id.imv_edit_finish)
        ImageView imvEditFinish;

        @Bind(R.id.tv_finish)
        TextView tvFinish;

        @Bind(R.id.tv_reasons_for_rejected)
        TextView tvReasonForRejected;

        @Bind(R.id.tv_reject)
        TextView tvReject;

        @Bind(R.id.tv_approve)
        TextView tvApprove;

        @Bind(R.id.tv_submit)
        TextView tvSubmit;

        @Bind(R.id.content_reason_for_reject)
        LinearLayout lnContentReject;

        @Bind(R.id.content_status_submitted)
        LinearLayout lnContentSubmitted;

        @Bind(R.id.ln_content_new)
        LinearLayout lnContentNew;


        public ViewHolderByDate(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
