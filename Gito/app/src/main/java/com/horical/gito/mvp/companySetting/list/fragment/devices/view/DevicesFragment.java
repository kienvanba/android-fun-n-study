package com.horical.gito.mvp.companySetting.list.fragment.devices.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.devices.adapter.DevicesAdapter;
import com.horical.gito.mvp.companySetting.list.fragment.devices.injection.component.DaggerDevicesComponent;
import com.horical.gito.mvp.companySetting.list.fragment.devices.injection.component.DevicesComponent;
import com.horical.gito.mvp.companySetting.list.fragment.devices.injection.module.DevicesModule;
import com.horical.gito.mvp.companySetting.list.fragment.devices.presenter.DevicesPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class DevicesFragment extends BaseCompanyFragment implements IDevicesView, View.OnClickListener {
    public static final String TAG = makeLogTag(DevicesFragment.class);

    @Bind(R.id.refresh)
    SwipeRefreshLayout refresh;

    @Bind(R.id.lv_devices)
    ExpandableListView lvDevices;

    private DevicesAdapter adapterDevices;

    @Inject
    DevicesPresenter mPresenter;


    public static DevicesFragment newInstance() {
        Bundle args = new Bundle();
        DevicesFragment fragment = new DevicesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_company_setting_devices;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        DevicesComponent component = DaggerDevicesComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .devicesModule(new DevicesModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        adapterDevices = new DevicesAdapter(getContext(), mPresenter.getListDataHeader(), mPresenter.getListDataChild());
        lvDevices.setAdapter(adapterDevices);

        mPresenter.getAllDevices();
    }

    @Override
    protected void initListener() {
        adapterDevices.setOnItemClickListener(new DevicesAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(final int position, final String idDevice) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.are_you_sure_for_delete_this,"device"));
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mPresenter.deleteDevice(idDevice, position);
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        lvDevices.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                //Nhan lan 1
            }
        });

        lvDevices.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                //Nhan lan 2
            }
        });

        lvDevices.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                return false;
            }
        });

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getAllDevices();
            }
        });
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void getAllDeviceSuccess() {
        refresh.setRefreshing(false);
        adapterDevices.notifyDataSetChanged();
    }

    @Override
    public void getAllDeviceFailure(String err) {
        refresh.setRefreshing(false);
        adapterDevices.notifyDataSetChanged();
    }

    @Override
    public void deleteDeviceSuccess(int position) {
        Toast.makeText(getContext(), "success", Toast.LENGTH_SHORT).show();
        mPresenter.getAllDevices();
    }

    @Override
    public void deleteDeviceFailure(String err) {
        Toast.makeText(getContext(), "Failure", Toast.LENGTH_SHORT).show();
    }
}
