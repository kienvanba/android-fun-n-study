package com.horical.gito.mvp.companySetting.list.fragment.companyrole.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllCompanyRoleResponse;
import com.horical.gito.model.RoleCompany;
import com.horical.gito.mvp.companySetting.list.fragment.companyrole.view.ICompanyRoleView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class CompanyRolePresenter extends BasePresenter implements ICompanyRolePresenter {

    private List<RoleCompany> listCompanyRole;

    public void attachView(ICompanyRoleView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public ICompanyRoleView getView() {
        return (ICompanyRoleView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        listCompanyRole = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<RoleCompany> getListCompanyRole() {
        return listCompanyRole;
    }

    public void setListCompanyRole(List<RoleCompany> listCompanyRole) {
        this.listCompanyRole = listCompanyRole;
    }

    @Override
    public void getAllCompanyRole() {
        getApiManager().getAllCompanyRole(new ApiCallback<GetAllCompanyRoleResponse>() {
            @Override
            public void success(GetAllCompanyRoleResponse res) {
                if (listCompanyRole != null) {
                    listCompanyRole.clear();
                } else {
                    listCompanyRole = new ArrayList<>();
                }
                listCompanyRole.addAll(res.roleCompanies);
                getView().getAllCompanyRoleSuccess();
            }

            @Override
            public void failure(RestError error) {
                if (listCompanyRole != null) {
                    listCompanyRole.clear();
                } else {
                    listCompanyRole = new ArrayList<>();
                }
                getView().getAllCompanyRoleFailure(error.message);
            }
        });
    }

    @Override
    public void deleteCompanyRole(String idCompanyRole, final int position) {
        getApiManager().deleteCompanyRole(idCompanyRole, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteCompanyRoleSuccess(position);
            }

            @Override
            public void failure(RestError error) {
                getView().deleteCompanyRoleFailure(error.message);
            }
        });
    }
}

