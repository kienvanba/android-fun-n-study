package com.horical.gito.mvp.checkin.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.checkin.list.presenter.CheckinPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Penguin on 23-Nov-16.
 */
@Module
public class CheckinModule {
    @PerActivity
    @Provides
    CheckinPresenter providesCheckinPresenter(){
        return new CheckinPresenter();
    }
}
