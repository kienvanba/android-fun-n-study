package com.horical.gito.mvp.salary.bonus.list.fragments.mine.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.salary.inputAddon.FilterInputAddonRequest;
import com.horical.gito.interactor.api.response.salary.bonus.GetFilterInputAddonResponse;
import com.horical.gito.model.InputAddon;
import com.horical.gito.mvp.salary.bonus.list.fragments.mine.view.IListMineFragmentView;
import com.horical.gito.mvp.salary.dto.bonus.BonusListDto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListMineFragmentPresenter extends BasePresenter implements IMineFragmentPresenter {

    public void attachView(IListMineFragmentView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IListMineFragmentView getView() {
        return (IListMineFragmentView) getIView();
    }

    private List<InputAddon> inputAddons;

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public List<InputAddon> getListInputAddon() {
        if (inputAddons == null) {
            inputAddons = new ArrayList<>();
        }
        return inputAddons;
    }

    @Override
    public void requestGetMineInputAddon() {
        FilterInputAddonRequest filterInputAddonRequest = new FilterInputAddonRequest(false, false, true);
        getApiManager().filterInputAddon(filterInputAddonRequest, new ApiCallback<GetFilterInputAddonResponse>() {
            @Override
            public void success(GetFilterInputAddonResponse res) {
                getListInputAddon().clear();
                getListInputAddon().addAll(res.getAddons());
                getView().requestGetMineInputAddonSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().requestGetMineInputAddonError(error);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
