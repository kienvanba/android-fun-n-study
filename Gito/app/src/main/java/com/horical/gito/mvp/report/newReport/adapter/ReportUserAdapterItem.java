package com.horical.gito.mvp.report.newReport.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.Group;
import com.horical.gito.model.User;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ReportUserAdapterItem extends RecyclerView.Adapter<ReportUserAdapterItem.ViewHolderItem> {

    private Context context;
    private List<Object> list;
    private OnItemClickListener mCallBack;

    public ReportUserAdapterItem(Context context, List<Object> list, OnItemClickListener callBack) {
        this.context = context;
        this.list = list;
        this.mCallBack = callBack;
    }

    @Override
    public ReportUserAdapterItem.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_normal_user_new_report, parent, false);
        return new ViewHolderItem(view);
    }

    @Override
    public void onBindViewHolder(ReportUserAdapterItem.ViewHolderItem holder, final int position) {
        holder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallBack != null) {
                    mCallBack.onDeleteItem(position);
                }
            }
        });

        if (list.get(position) instanceof User) {
            User itemUser = (User) list.get(position);
            holder.tvName.setText(itemUser.getDisplayName());
            holder.tvDepartment.setText(itemUser.getDepartment().getName());
            holder.rltImage.setVisibility(View.VISIBLE);
            holder.tvNameGroup.setVisibility(View.GONE);

            if (itemUser.getAvatar() == null || TextUtils.isEmpty(itemUser.getAvatar().getTokenFile())) {
                holder.tvPhoto.setVisibility(View.VISIBLE);
                holder.imvPhoto.setVisibility(View.GONE);
                holder.tvPhoto.setText(itemUser.getDisplayPhotoName());
            } else {
                holder.imvPhoto.setVisibility(View.VISIBLE);
                holder.tvPhoto.setVisibility(View.GONE);
                ImageLoader.load(context, CommonUtils.getURL(itemUser.getAvatar()), holder.imvPhoto, null);
            }

        } else if (list.get(position) instanceof Group) {
            holder.tvDepartment.setText("");
            holder.tvPhoto.setVisibility(View.VISIBLE);
            holder.imvPhoto.setVisibility(View.GONE);
            holder.tvName.setText("");
            holder.tvPhoto.setText(((Group) list.get(position)).getGroupName());
            holder.tvPhoto.setBackgroundColor(context.getResources().getColor(R.color.white));

            holder.tvNameGroup.setText(((Group) list.get(position)).getGroupName());
            holder.rltImage.setVisibility(View.GONE);
            holder.tvNameGroup.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_photo)
        TextView tvPhoto;

        @Bind(R.id.imv_photo)
        CircleImageView imvPhoto;

        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.tv_department)
        TextView tvDepartment;

        @Bind(R.id.imv_delete)
        ImageView imvDelete;

        @Bind(R.id.tv_name_group)
        TextView tvNameGroup;

        @Bind(R.id.rlt_image_view)
        RelativeLayout rltImage;


        public ViewHolderItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onDeleteItem(int positionItem);
    }
}
