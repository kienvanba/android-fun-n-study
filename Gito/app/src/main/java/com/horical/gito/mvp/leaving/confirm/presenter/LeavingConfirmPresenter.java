package com.horical.gito.mvp.leaving.confirm.presenter;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.horical.gito.R;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.base.IView;
import com.horical.gito.mvp.leaving.confirm.adapter.LeavingConfirmDetailItem;
import com.horical.gito.mvp.leaving.confirm.adapter.LeavingConfirmItem;
import com.horical.gito.utils.HDate;

import java.util.ArrayList;

/**
 * Created by Dragonoid on 1/17/2017.
 */

public class LeavingConfirmPresenter extends BasePresenter implements ILeavingConfirmPresenter {

    ArrayList<com.horical.gito.base.IRecyclerItem> mList;
    Resources mResources;

    public LeavingConfirmPresenter() {
        mList = new ArrayList<>(analyzeData(generateTestConfirmData()));
    }

    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public void setResource(Resources resource) {
        mResources = resource;
    }

    public ArrayList<com.horical.gito.base.IRecyclerItem> getList() {
        return mList;
    }

    public ArrayList<IRecyclerItem> analyzeData(ArrayList<IRecyclerItem> data) {
        ArrayList<IRecyclerItem> result = new ArrayList<IRecyclerItem>();
        for (IRecyclerItem _item : data) {
            LeavingConfirmDetailItem item = (LeavingConfirmDetailItem) _item;
            boolean listExist = false;
            for (IRecyclerItem _list : result) {
                LeavingConfirmItem list = (LeavingConfirmItem) _list;
                if (list.getDate().dateNumber() == item.getDate().dateNumber()) {
                    list.getList().add(item);
                    listExist = true;
                    break;
                }
            }
            if (listExist == false) {
                ArrayList<IRecyclerItem> newList = new ArrayList<IRecyclerItem>();
                newList.add(item);
                result.add(new LeavingConfirmItem(item.getDate(), newList));
            }
        }
        return result;
    }

    //------------------TEST AREA-----------------------------------//
    public ArrayList<IRecyclerItem> generateTestConfirmData() {
        Bitmap image = BitmapFactory.decodeResource(mResources, R.drawable.doraemon);
        ArrayList<IRecyclerItem> mTestList = new ArrayList<>();
        mTestList.add(new LeavingConfirmDetailItem(new HDate(1, 1, 1), "Person 1", "Pos 1", image, "Reason 1", new HDate(1, 1, 1, 8, 1, 1), new HDate(1, 1, 1, 8, 1, 1)));
        mTestList.add(new LeavingConfirmDetailItem(new HDate(2, 1, 1), "Person 2", "Pos 2", image, "", new HDate(2, 1, 1, 8, 1, 1), new HDate(2, 1, 1, 8, 1, 1)));
        mTestList.add(new LeavingConfirmDetailItem(new HDate(3, 1, 1), "Person 3", "Pos 3", image, "Reason 3", new HDate(2, 1, 1, 8, 1, 1), new HDate(2, 1, 1, 8, 1, 1)));
        mTestList.add(new LeavingConfirmDetailItem(new HDate(5, 1, 1), "Person 1", "Pos 1", image, "Reason 5", new HDate(2, 1, 1, 8, 1, 1), new HDate(2, 1, 1, 8, 1, 1)));
        mTestList.add(new LeavingConfirmDetailItem(new HDate(2, 1, 1), "Person 3", "Pos 3", image, "Reason 6", new HDate(2, 1, 1, 8, 1, 1), new HDate(2, 1, 1, 8, 1, 1)));
        mTestList.add(new LeavingConfirmDetailItem(new HDate(3, 1, 1), "Person 1", "Pos 1", image, "Reason 1", new HDate(2, 1, 1, 8, 1, 1), new HDate(2, 1, 1, 8, 1, 1)));
        mTestList.add(new LeavingConfirmDetailItem(new HDate(5, 1, 1), "Person 2", "Pos 2", image, "", new HDate(2, 1, 1, 8, 1, 1), new HDate(2, 1, 1, 8, 1, 1)));
        mTestList.add(new LeavingConfirmDetailItem(new HDate(3, 1, 1), "Person 4", "Pos 3", image, "Reason 3", new HDate(2, 1, 1, 8, 1, 1), new HDate(2, 1, 1, 8, 1, 1)));
        return mTestList;
    }

}
