package com.horical.gito.mvp.forgotPassword.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.user.ForgotPasswordRequest;
import com.horical.gito.interactor.api.response.user.ForgotPasswordResponse;
import com.horical.gito.mvp.forgotPassword.view.IForgotPasswordView;
import com.horical.gito.utils.EmailUtils;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class ForgotPasswordPresenter extends BasePresenter implements IForgotPasswordPresenter {
    private static final String TAG = makeLogTag(ForgotPasswordPresenter.class);

    public void attachView(IForgotPasswordView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IForgotPasswordView getView() {
        return (IForgotPasswordView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void requestForgotPassword(String email) {
        if (!isViewAttached()) return;
        if (email == null || email.length() == 0) {
            getView().errorEmptyInput();
            return;
        }
        if (!EmailUtils.isValidEmail(email)) {
            getView().errorInvalidEmail();
            return;
        }
        ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest();
        forgotPasswordRequest.email = email;
        getView().showLoading();
        getApiManager().forgotPassword(forgotPasswordRequest, new ApiCallback<ForgotPasswordResponse>() {
            @Override
            public void success(ForgotPasswordResponse res) {
                getView().hideLoading();
                getView().requestForgotPasswordSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().requestForgotPasswordError(error);
            }
        });
    }
}
