package com.horical.gito.mvp.salary.mySalary.detail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;
import com.horical.gito.mvp.salary.mySalary.detail.view.IDetailMySalaryView;

import java.util.ArrayList;
import java.util.List;

public class DetailMySalaryPresenter extends BasePresenter implements IDetailMySalaryPresenter {

    public void attachView(IDetailMySalaryView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IDetailMySalaryView getView() {
        return (IDetailMySalaryView) getIView();
    }

    private List<ListStaffDto> mStaffs;

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public List<ListStaffDto> getStaffs() {
        if (mStaffs == null) {
            mStaffs = new ArrayList<>();
        }
        return mStaffs;
    }

    @Override
    public void setStaffs(List<ListStaffDto> staffs) {
        this.mStaffs = staffs;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
