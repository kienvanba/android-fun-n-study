package com.horical.gito.mvp.checkin.detail.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.CheckIn;

/**
 * Created by Penguin on 05-Dec-16.
 */

public class DayofWeek implements IRecyclerItem {
    public CheckIn mCheckIn;

    public DayofWeek(CheckIn checkIn) {
        this.mCheckIn = checkIn;
    }
    public CheckIn getCheckin() {
        return mCheckIn;
    }

    public void setCheckin(CheckIn checkIn) {
        mCheckIn = checkIn;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.CHECK_IN_ITEM;
    }
}
