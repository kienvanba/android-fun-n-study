package com.horical.gito.widget.viewFlipper;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;

import com.horical.gito.AppConstants;
import com.horical.gito.R;

import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_UP;

public class IntroViewFlipper extends ViewFlipper {

    private float lastX;
    private TouchCallback mCallback;
    private Animation mInLeft;
    private Animation mInRight;
    private Animation mOutLeft;
    private Animation mOutRight;

    public IntroViewFlipper(Context context) {
        super(context);
        init();
    }

    public IntroViewFlipper(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mInRight = AnimationUtils.loadAnimation(getContext(), R.anim.in_from_right);
        mInLeft = AnimationUtils.loadAnimation(getContext(), R.anim.in_from_left);
        mOutLeft = AnimationUtils.loadAnimation(getContext(), R.anim.out_to_left);
        mOutRight = AnimationUtils.loadAnimation(getContext(), R.anim.out_to_right);

        setInAnimation(mInRight);
        setOutAnimation(mOutLeft);
    }

    private long exitTimer = Long.MIN_VALUE;

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        switch (event.getActionMasked()) {
            case ACTION_DOWN:
                if ((exitTimer + AppConstants.EXIT_INTERVAL) > System.currentTimeMillis()) {
                    lastX = event.getX();
                    if (mCallback != null) {
                        mCallback.onTouchDown();
                    }
                    exitTimer = System.currentTimeMillis();
                    break;
                }
            case ACTION_UP:
                float currentX = event.getX();
                // Swipe left to right
                if (lastX < currentX - 20) {
                    setInAnimation(mInLeft);
                    setOutAnimation(mOutRight);

                    showPrevious();
                }
                // Reset to default anim
                setInAnimation(mInRight);
                setOutAnimation(mOutLeft);

                // Swipe right to left.
                if (lastX > currentX + 20) {
                    setInAnimation(mInRight);
                    setOutAnimation(mOutLeft);

                    showNext();
                }
                if (mCallback != null) {
                    mCallback.onTouchUp();
                }
                break;
        }
        return true;
    }

    public void setTouchCallback(TouchCallback touchCallback) {
        mCallback = touchCallback;
    }

    public void setIndicator(ViewFlipperIndicator indicator) {
        mInLeft.setAnimationListener(indicator);
        mInRight.setAnimationListener(indicator);
    }

    public interface TouchCallback {
        void onTouchDown();

        void onTouchUp();
    }
}
