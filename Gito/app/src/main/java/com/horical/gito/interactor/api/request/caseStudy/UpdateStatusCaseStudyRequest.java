package com.horical.gito.interactor.api.request.caseStudy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by thanhle on 3/4/17.
 */

public class UpdateStatusCaseStudyRequest {

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("rejectMsg")
    @Expose
    public String rejectMsg;
}
