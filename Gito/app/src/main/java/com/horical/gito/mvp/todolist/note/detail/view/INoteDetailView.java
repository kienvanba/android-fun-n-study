package com.horical.gito.mvp.todolist.note.detail.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

/**
 * Created by Luong on 28-Mar-17.
 */

public interface INoteDetailView extends IView {
    void showLoading();

    void hideLoading();

    void validateFailedTitleEmpty();

    void validateFailedContentEmpty();

    void createNoteSuccess();

    void createNoteFailure(RestError error);

    void updateNoteSuccess();

    void updateNoteFailure(RestError error);

    void uploadSuccess(String url);

    void uploadFailure(String error);
}