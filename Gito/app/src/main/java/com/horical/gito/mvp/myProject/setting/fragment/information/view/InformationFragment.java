package com.horical.gito.mvp.myProject.setting.fragment.information.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.interactor.api.request.project.UpdateProjectRequest;
import com.horical.gito.model.Project;
import com.horical.gito.model.TokenFile;
import com.horical.gito.mvp.dialog.dialogPhoto.DialogPhoto;
import com.horical.gito.mvp.myProject.setting.fragment.information.injection.component.DaggerInformationComponent;
import com.horical.gito.mvp.myProject.setting.fragment.information.injection.component.InformationComponent;
import com.horical.gito.mvp.myProject.setting.fragment.information.injection.module.InformationModule;
import com.horical.gito.mvp.myProject.setting.fragment.information.presenter.InformationPresenter;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.utils.FileUtils;
import com.horical.gito.utils.NetworkUtils;
import com.horical.gito.widget.dialog.DialogEditText;

import java.io.File;

import javax.inject.Inject;

import butterknife.Bind;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;

public class InformationFragment extends BaseFragment implements IInformationFragment, View.OnClickListener {
    private static final int REQUEST_PERMISSION_CAPTURE_IMAGE = 0;
    private static final int REQUEST_PERMISSION_READ_LIBRARY = 1;

    private static final int REQUEST_CAPTURE_IMAGE = 3;
    private static final int REQUEST_READ_LIBRARY = 4;


    @Bind(R.id.img_avatar)
    CircleImageView mAvatar;
    @Bind(R.id.progress_avatar)
    ProgressBar progressAvatar;
    @Bind(R.id.project_non_ava)
    TextView mTvNonAva;
    @Bind(R.id.edit_name)
    TextView mTvName;
    @Bind(R.id.btn_edit_name)
    ImageButton mBtnEditName;
    @Bind(R.id.tv_id)
    TextView mTvId;
    @Bind(R.id.btn_edit_id)
    ImageButton mBtnEditId;
    @Bind(R.id.btn_edit_desc)
    ImageButton mBtnEditDescription;
    @Bind(R.id.tv_description)
    TextView mTvDescription;
    @Bind(R.id.btn_lock)
    ImageButton mBtnLock;
    @Bind(R.id.tv_lock)
    TextView mTvLock;
    @Bind(R.id.btn_delete)
    ImageButton mBtnDelete;
    @Bind(R.id.btn_edit_ava)
    ImageButton mBtnEditAvatar;

    @Inject
    InformationPresenter mPresenter;

    private File mUploadFile;
    private UpdateProjectRequest request;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        InformationComponent component = DaggerInformationComponent.builder()
                .informationModule(new InformationModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initListener();
    }

    @Override
    protected void initListener() {
        mBtnEditName.setOnClickListener(this);
        mBtnEditId.setOnClickListener(this);
        mBtnEditDescription.setOnClickListener(this);
        mBtnLock.setOnClickListener(this);
        mBtnDelete.setOnClickListener(this);
        mBtnEditAvatar.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        if (request == null) {
            request = new UpdateProjectRequest();
        }

        Project mProject = mPresenter.getProject();

        if (mProject.getAvatar() != null) {
            progressAvatar.setVisibility(View.VISIBLE);
            mTvNonAva.setVisibility(View.GONE);
            String url = CommonUtils.getURL(mProject.getAvatar());
            ImageLoader.load(getContext(), url, mAvatar, progressAvatar);
        } else {
            progressAvatar.setVisibility(View.GONE);
            mTvNonAva.setVisibility(View.VISIBLE);
        }

        mTvNonAva.setText(mProject.getName() != null
                ? mProject.getName().substring(0, 1).toUpperCase() : "");
        mTvName.setText(mProject.getName());
        mTvDescription.setText(mProject.getDescription());
        mTvId.setText(mProject.getProjectId());
        if (mProject.getStatus() == 0) {
            mBtnLock.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_lock));
            mTvLock.setText(getString(R.string.lock));
        } else {
            mBtnLock.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_unlock));
            mTvLock.setText(getString(R.string.unlock));
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_information_setting;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_edit_ava:
                final DialogPhoto dialogPhoto = new DialogPhoto(getContext());
                dialogPhoto.setListener(new DialogPhoto.OnDialogEditPhotoClickListener() {
                    @Override
                    public void albumGallery() {
                        handleOpenLibrary();
                        dialogPhoto.dismiss();
                    }

                    @Override
                    public void takePhoto() {
                        handleOpenCamera();
                        dialogPhoto.dismiss();
                    }
                });
                dialogPhoto.show();
                break;
            case R.id.btn_edit_name:
                DialogEditText dialog = new DialogEditText(getContext()) {
                    @Override
                    public void setSaveButtonClickListener() {
                        request.name = this.editText.getText().toString();
                        mPresenter.updateProject(request);
                        this.dismiss();
                    }
                };
                dialog.editText.setText(mPresenter.getProject().getName());
                dialog.setHint(getString(R.string.hint_name));
                dialog.show();
                break;
            case R.id.btn_edit_id:
                dialog = new DialogEditText(getContext()) {
                    @Override
                    public void setSaveButtonClickListener() {
                        request.projectId = this.editText.getText().toString();
                        mPresenter.updateProject(request);
                        this.dismiss();
                    }
                };
                dialog.editText.setText(mPresenter.getProject().getProjectId());
                dialog.setHint(getString(R.string.hint_id));
                dialog.show();
                break;
            case R.id.btn_edit_desc:
                dialog = new DialogEditText(getContext()) {
                    @Override
                    public void setSaveButtonClickListener() {
                        request.desc = this.editText.getText().toString();
                        mPresenter.updateProject(request);
                        this.dismiss();
                    }
                };
                dialog.editText.setText(mPresenter.getProject().getDescription());
                dialog.setHint(getString(R.string.hint_description));
                dialog.show();
                break;
            case R.id.btn_lock:
                if (mPresenter.getProject().getStatus() != 0) {
                    request.status = 0;
                    mPresenter.updateProject(request);
                } else {
                    request.status = 1;
                    mPresenter.updateProject(request);
                }
                break;
            case R.id.btn_delete:
                //
                break;
        }
    }

    private void handleOpenCamera() {
        String[] s = {android.Manifest.permission.CAMERA};
        if (checkPermissions(s)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            mUploadFile = FileUtils.createImageFile(getActivity());
            Uri uri = Uri.fromFile(mUploadFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, REQUEST_CAPTURE_IMAGE);

        } else {
            ActivityCompat.requestPermissions(getActivity(), s, REQUEST_PERMISSION_CAPTURE_IMAGE);
        }
    }

    private void handleOpenLibrary() {
        String s[] = {android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(s)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, REQUEST_READ_LIBRARY);

        } else {
            ActivityCompat.requestPermissions(getActivity(), s, REQUEST_PERMISSION_READ_LIBRARY);
        }
    }

    @Override
    public void getProjectSuccess() {

    }

    @Override
    public void uploadFileSuccess(TokenFile avatar) {
        UpdateProjectRequest request = new UpdateProjectRequest();
        request.avatar = avatar;
        mPresenter.updateProject(request);
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CAPTURE_IMAGE) {
            handleOpenCamera();
        } else if (requestCode == REQUEST_PERMISSION_READ_LIBRARY) {
            handleOpenLibrary();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAPTURE_IMAGE) {
                System.out.println("FILE " + mUploadFile.toString());
                if (!NetworkUtils.isConnected(getContext())) {
                    Toast.makeText(getContext(), "no internet connection!", Toast.LENGTH_SHORT).show();
                } else {
                    mPresenter.uploadFile(
                            MediaType.parse(android.webkit.MimeTypeMap.getFileExtensionFromUrl(mUploadFile.getName())),
                            mUploadFile);
                }
            } else if (requestCode == REQUEST_READ_LIBRARY) {
                System.out.println("File from library " + data.getData());
                if (!NetworkUtils.isConnected(getContext())) {
                    Toast.makeText(getContext(), "no internet connection!", Toast.LENGTH_SHORT).show();
                } else {

                    mUploadFile = FileUtils.convertUriToFile(getContext(), data.getData());
                    if (mUploadFile != null) {
                        mPresenter.uploadFile(MediaType.parse(
                                getContext().getContentResolver().getType(data.getData())),
                                mUploadFile);
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
