package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.view.ICommitView;

/**
 * Created by Tin on 23-Nov-16.
 */

public class CommitPresenter extends BasePresenter implements ICommitPresenter {
    public void attachView(ICommitView view) {
        super.attachView(view);
    }

    public ICommitView getView() {
        return (ICommitView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
