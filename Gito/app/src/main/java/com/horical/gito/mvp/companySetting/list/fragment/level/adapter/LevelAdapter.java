package com.horical.gito.mvp.companySetting.list.fragment.level.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Level;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 30-Nov-16.
 */
public class LevelAdapter extends RecyclerView.Adapter<LevelAdapter.LevelHolder> {
    private Context mContext;
    private List<Level> mList;
    private OnItemClickListener mOnItemClickListener;

    public LevelAdapter(Context context, List<Level> list) {
        mContext = context;
        mList = list;
    }


    @Override
    public LevelHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_company_setting_level, parent, false);
        return new LevelHolder(view);
    }

    @Override
    public void onBindViewHolder(LevelHolder holder, final int position) {
        Level level = mList.get(position);

        holder.tvTitle.setText(level.getName());
        holder.tvDes.setText(level.getDesc());
        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDelete(position);
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class LevelHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_title)
        TextView tvTitle;

        @Bind(R.id.ll_delete)
        LinearLayout llDelete;

        @Bind(R.id.tv_des)
        TextView tvDes;
        View view;

        public LevelHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);
        void onDelete(int position);
    }
}
