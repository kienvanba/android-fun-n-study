package com.horical.gito.mvp.myProject.setting.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;

public class TabItemBody implements IRecyclerItem {
    private String mTabName;

    public TabItemBody(String name){
        this.mTabName = name;
    }

    public String getTabName(){
        return this.mTabName;
    }
    @Override
    public int getItemViewType() {
        return AppConstants.TAB_ITEM;
    }
}
