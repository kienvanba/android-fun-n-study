package com.horical.gito.mvp.companySetting.list.fragment.members.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.fragment.members.injection.module.MembersModule;
import com.horical.gito.mvp.companySetting.list.fragment.members.view.MembersFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = MembersModule.class)
public interface MembersComponent {
    void inject(MembersFragment fragment);
}
