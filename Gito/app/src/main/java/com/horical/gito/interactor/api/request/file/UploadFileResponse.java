package com.horical.gito.interactor.api.request.file;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.TokenFile;

/**
 * Created by nhonvt.dk on 3/30/17
 */

public class UploadFileResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public TokenFile tokenFile;
}
