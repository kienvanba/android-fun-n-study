package com.horical.gito.mvp.recruitment.newCandidate.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.recruitment.newCandidate.injection.module.CandidateModule;
import com.horical.gito.mvp.recruitment.newCandidate.view.CandidateActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class , modules = CandidateModule.class)
public interface CandidateComponent {
    void inject(CandidateActivity activity);
}
