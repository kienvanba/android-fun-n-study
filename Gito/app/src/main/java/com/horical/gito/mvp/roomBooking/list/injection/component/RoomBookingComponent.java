package com.horical.gito.mvp.roomBooking.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.roomBooking.list.injection.module.RoomBookingModule;
import com.horical.gito.mvp.roomBooking.list.view.RoomBookingActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = RoomBookingModule.class)
public interface RoomBookingComponent {
    void inject(RoomBookingActivity activity);
}
