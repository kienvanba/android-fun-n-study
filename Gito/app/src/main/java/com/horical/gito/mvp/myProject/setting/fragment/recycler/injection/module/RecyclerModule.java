package com.horical.gito.mvp.myProject.setting.fragment.recycler.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.presenter.RecyclerPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class RecyclerModule {
    @PerFragment
    @Provides
    RecyclerPresenter provideRecyclerPresenter(){
        return new RecyclerPresenter();
    }
}
