package com.horical.gito.mvp.myProject.document.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PathHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.path)
    TextView mTvPath;
    @Bind(R.id.slash)
    ImageView mImSlash;

    public PathHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
