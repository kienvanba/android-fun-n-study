package com.horical.gito.mvp.logTime.view;

import com.horical.gito.base.IView;

public interface ILogTimeView extends IView {

    void showLoading();

    void hideLoading();

    void onGetMembersSuccess();

    void onGetMembersFailure(String message);

    void onGetLogTimesSuccess();

    void onGetLogTimesFailure(String error);

    void onGetLogTimeFailure(String message);

    void onGetLogTimeSuccess(boolean hasData);

    void approveOrRejectSuccess();

    void approveOrRejectFailure(String message);
}
