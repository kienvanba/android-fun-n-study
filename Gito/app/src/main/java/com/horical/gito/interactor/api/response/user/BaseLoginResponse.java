package com.horical.gito.interactor.api.response.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;

public class BaseLoginResponse extends BaseResponse {

    @SerializedName("token")
    @Expose
    public String token;
}