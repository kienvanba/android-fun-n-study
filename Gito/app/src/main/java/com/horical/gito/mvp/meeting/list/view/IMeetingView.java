package com.horical.gito.mvp.meeting.list.view;

import com.horical.gito.base.IView;

/**
 * Created by Lemon on 11/21/2016.
 */

public interface IMeetingView extends IView {
    void showLoading();

    void hideLoading();

    void getAllMeetingSuccess();

    void getAllMeetingFailure(String error);

    void deleteMeetingSuccess(int position);

    void deleteMeetingFailure();
}
