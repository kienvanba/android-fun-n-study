package com.horical.gito.injection.component;

import android.app.Application;
import android.content.Context;

import com.horical.gito.MainApplication;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.fcm.FCMInstanceIDService;
import com.horical.gito.fcm.FCMMessagingService;
import com.horical.gito.injection.ApplicationContext;
import com.horical.gito.injection.module.ApiModule;
import com.horical.gito.injection.module.ApplicationModule;
import com.horical.gito.injection.module.AssetsModule;
import com.horical.gito.injection.module.CachesModule;
import com.horical.gito.injection.module.EventModule;
import com.horical.gito.injection.module.NetworkModule;
import com.horical.gito.injection.module.PreferModule;
import com.horical.gito.injection.module.SocketModule;
import com.horical.gito.interactor.api.ApiManager;
import com.horical.gito.interactor.assets.AssetsManager;
import com.horical.gito.interactor.caches.CachesManager;
import com.horical.gito.interactor.event.EventManager;
import com.horical.gito.interactor.prefer.PreferManager;
import com.horical.gito.interactor.socket.SocketManager;
import com.horical.gito.mvp.dialog.dialogCustomDate.DialogCustomDate;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.popup.dateSelected.SelectDatePopup;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, ApiModule.class, EventModule.class, PreferModule.class, AssetsModule.class, SocketModule.class, CachesModule.class})
public interface ApplicationComponent {

    void inject(MainApplication mainApplication);

    void inject(DrawerActivity activity);

    void inject(BasePresenter basePresenter);

    void inject(SelectDatePopup popup);

    void inject(DialogCustomDate dialogCustomDate);

    void inject(FCMInstanceIDService service);

    void inject(FCMMessagingService service);

    @ApplicationContext
    Context getContext();

    Application getApplication();

    ApiManager getApiManager();

    EventManager getEventManager();

    PreferManager getPreferManager();

    AssetsManager getAssetManager();

    SocketManager getSocketManager();

    CachesManager getCachesManager();

}
