package com.horical.gito.mvp.myProject.task.details.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.SubTasks;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 3/10/17.
 */

public class SubTaskAdapter extends RecyclerView.Adapter<SubTaskAdapter.SubTaskHolder> {

    private Context mContext;
    private List<SubTasks> mList;
    private OnItemClickListener mOnItemClickListener;

    public SubTaskAdapter(Context context, List<SubTasks> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public SubTaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_task_subtask, parent, false);
        return new SubTaskHolder(view);
    }

    public List<SubTasks> getList() {
        return mList;
    }

    public void setList(List<SubTasks> mList) {
        this.mList = mList;
    }

    @Override
    public void onBindViewHolder(SubTaskHolder holder, int position) {
        SubTasks subTasks = mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class SubTaskHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.edit_name)
        TextView tvHour;

        @Bind(R.id.tv_status)
        TextView tvDescription;

        @Bind(R.id.ll_delete)
        LinearLayout llDelete;

        View view;

        public SubTaskHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}
