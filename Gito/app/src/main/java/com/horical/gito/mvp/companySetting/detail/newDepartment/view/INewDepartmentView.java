package com.horical.gito.mvp.companySetting.detail.newDepartment.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface INewDepartmentView extends IView {
    void createDepartmentSuccess();

    void createDepartmentFailure(String err);

    void updateDepartmentSuccess();

    void updateDepartmentFailure(String err);
}
