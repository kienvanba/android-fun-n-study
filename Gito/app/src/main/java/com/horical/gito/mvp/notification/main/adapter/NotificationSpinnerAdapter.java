package com.horical.gito.mvp.notification.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.load.engine.Resource;
import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 3/17/2017.
 */

public class NotificationSpinnerAdapter extends ArrayAdapter{
    List<NotificationSpinnerItem> mItems;
    Context mContext;
    public NotificationSpinnerAdapter(Context context, int resource, List<NotificationSpinnerItem> objects) {
        super(context,resource,objects);
        mContext = context;
        mItems = objects;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public int getItemCount() {
        return mItems.size();
    }

//    public View getCustomView(int position, View convertView, ViewGroup parent) {
//        View layout =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification_spinner,parent,false);
//        return layout;
//    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View layout =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification_spinner,parent,false);
        NotificationSpinnerItem item = mItems.get(position);
        GitOTextView mDate = (GitOTextView) layout.findViewById(R.id.notification_tv_spin_date);
        GitOTextView mTittle = (GitOTextView) layout.findViewById(R.id.notification_tv_spin_tittle);
        mDate.setText(item.mDate);
        mTittle.setText(item.mTittle);
        return layout;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View layout =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification_spinner_selected,parent,false);
        GitOTextView mTittle = (GitOTextView) layout.findViewById(R.id.notification_tv_spin_selected);
        mTittle.setText(mItems.get(position).mTittle);
        return layout;
    }

    public class NotificationSpinnerHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.notification_tv_spin_tittle)
        GitOTextView mTittle;
        @Bind(R.id.notification_tv_spin_date)
        GitOTextView mDate;

        public NotificationSpinnerHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }
}
