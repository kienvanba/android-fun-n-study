package com.horical.gito.mvp.popup.dateSelected;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DateAdapter extends RecyclerView.Adapter<DateAdapter.DateRangeViewHolder> {
    private Context mContext;
    private List<DateDto> mList;

    public DateAdapter(Context context, List<DateDto> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public DateRangeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_date_range, parent, false);
        return new DateRangeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DateRangeViewHolder holder, final int position) {

        holder.tvTypeDay.setText(mList.get(position).getTypeDate());
        String day = mList.get(position).showDateStartEnd();
        holder.tvDay.setText(day.isEmpty() ? mContext.getString(R.string.custom_day_pick) : day);
        holder.itemView.setSelected(mList.get(position).isSelected());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClickItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class DateRangeViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_type_day)
        TextView tvTypeDay;

        @Bind(R.id.tv_day)
        TextView tvDay;

        public DateRangeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnItemClickListener {
        void onClickItem(int position);
    }
}
