package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.view.ICommitDetailView;

import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class CommitDetailPresenter extends BasePresenter implements ICommitDetailPresenter {

    public void attachView(ICommitDetailView view) {
        super.attachView(view);
    }

    public ICommitDetailView getView() {
        return (ICommitDetailView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
