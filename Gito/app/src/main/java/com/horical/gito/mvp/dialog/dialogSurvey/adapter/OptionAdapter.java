package com.horical.gito.mvp.dialog.dialogSurvey.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.horical.gito.AppConstants;
import com.horical.gito.R;
import com.horical.gito.model.Option;

import java.util.List;
/**
 * Created by nhattruong251295 on 3/17/2017.
 */

public class OptionAdapter extends ArrayAdapter<Option>{
    private Context mContext;
    private int mResource;
    private List<Option> mList;
    private int mType = -1;

    public OptionAdapter(Context context, int resource, List<Option> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        this.mList = objects;
    }

    public void setType(int type){
        this.mType = type;
    }

    public int getType(){
        return mType;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View row = inflater.inflate(this.mResource,null);
        CheckBox chkOption = (CheckBox) row.findViewById(R.id.chk_option);
        final EditText edtOption = (EditText) row.findViewById(R.id.edt_option);
        LinearLayout llDeleteOption = (LinearLayout) row.findViewById(R.id.ll_delete_option);

        chkOption.setEnabled(false);

        if(getType() == AppConstants.TYPE_OPTION){
            chkOption.setButtonDrawable(R.drawable.bg_blue_white_checkbox_selector);
        }
        else if(getType() == AppConstants.TYPE_CHECK){
            chkOption.setButtonDrawable(R.drawable.bg_task_filter_check);
        }

        final String content = this.mList.get(position).getDesc();
        edtOption.setText(content);

       /* edtOption.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                mList.get(position).setDesc(textView.getText().toString());
                return true;
            }
        });*/

        llDeleteOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.deleteOption(position);
            }
        });

        edtOption.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mList.get(position).setDesc(edtOption.getText().toString());
            }
        });
        return row;
    }


    public List<Option> getList(){ return mList;}

    private OptionAdapter.OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OptionAdapter.OnItemClickListener callBack) {
        this.mOnItemClickListener = callBack;
    }

    public interface OnItemClickListener {
        void deleteOption(int position);
    }
}

/*
public class OptionAdapter  extends RecyclerView.Adapter<OptionAdapter.OptionHolder>{
    private List<String> mList;
    private Context mContext;
    private int mType;
    private int selectedPosition = -1;

    public OptionAdapter(Context mContext, List<String> mList, int type) {
        this.mList = mList;
        this.mContext = mContext;
        this.mType = type;
    }

    public void setType(int type){
        this.mType = type;
    }
    public int getType(){
        return mType;
    }

    public List<String> getmList(){ return mList;}

    @Override
    public OptionAdapter.OptionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_option,parent,false);
        return new OptionHolder(view);
    }

    @Override
    public void onBindViewHolder(OptionHolder holder, final int position) {
        if (mType == AppConstants.TYPE_OPTION){
            holder.chkOption.setButtonDrawable(R.drawable.bg_blue_white_checkbox_selector);
        }
        holder.edtContentQuestion.setText(mList.get(position));
        holder.llDeleteOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.deleteOption(position);
            }
        });

        if(selectedPosition == position){
            holder.chkOption.setChecked(true);
        }else {
            holder.chkOption.setChecked(false);
        }

        holder.chkOption.setOnClickListener(onStateChangedListener(holder.chkOption,position));

        holder.llDeleteOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.deleteOption(position);
            }
        });

    }

    private View.OnClickListener onStateChangedListener(final CheckBox checkBox, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    selectedPosition = position;
                } else {
                    selectedPosition = -1;
                }
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }


    public class OptionHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.chk_option)
        CheckBox chkOption;

        @Bind(R.id.edt_option)
        EditText edtContentQuestion;

        @Bind(R.id.ll_delete_option)
        LinearLayout llDeleteOption;

        View view;

        public OptionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            view = itemView;
        }
    }

    private OptionAdapter.OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OptionAdapter.OnItemClickListener callBack) {
        this.mOnItemClickListener = callBack;
    }

    public interface OnItemClickListener {
        void deleteOption(int position);
    }

}
*/
