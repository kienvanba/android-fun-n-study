package com.horical.gito.injection.module;

import com.horical.gito.interactor.api.ApiManager;
import com.horical.gito.interactor.event.EventManager;
import com.horical.gito.interactor.prefer.PreferManager;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ApiModule {

    @Provides
    @Singleton
    ApiManager provideApiManager(@Named("api") Retrofit retrofitApi, @Named("file") Retrofit retrofitFile, @Named("doc") Retrofit retrofitDoc, PreferManager preferManager, EventManager eventManager) {
        return new ApiManager(retrofitApi, retrofitFile, retrofitDoc, preferManager, eventManager);
    }
}
