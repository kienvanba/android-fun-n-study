package com.horical.gito.mvp.companySetting.list.fragment.companyrole.presenter;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface ICompanyRolePresenter {
    void getAllCompanyRole();
    void deleteCompanyRole(String idCompanyRole, int position);
}
