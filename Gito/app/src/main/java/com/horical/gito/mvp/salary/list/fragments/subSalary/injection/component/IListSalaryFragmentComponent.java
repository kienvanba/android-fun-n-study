package com.horical.gito.mvp.salary.list.fragments.subSalary.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.list.fragments.subSalary.injection.module.ListSalaryFragmentModule;
import com.horical.gito.mvp.salary.list.fragments.subSalary.view.ListSalaryFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ListSalaryFragmentModule.class)
public interface IListSalaryFragmentComponent {
    void inject(ListSalaryFragment activity);
}
