package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/23/17.
 */

public class OverTimeRole implements Serializable {
    @SerializedName("approve")
    @Expose
    private Boolean approve;

    @SerializedName("del")
    @Expose
    private Boolean del;

    @SerializedName("modify")
    @Expose
    private Boolean modify;

    @SerializedName("view")
    @Expose
    private Boolean view;;

    @SerializedName("create")
    @Expose
    private Boolean create;

    public Boolean getApprove() {
        return approve;
    }

    public void setApprove(Boolean approve) {
        this.approve = approve;
    }

    public Boolean getDel() {
        return del;
    }

    public void setDel(Boolean del) {
        this.del = del;
    }

    public Boolean getModify() {
        return modify;
    }

    public void setModify(Boolean modify) {
        this.modify = modify;
    }

    public Boolean getView() {
        return view;
    }

    public void setView(Boolean view) {
        this.view = view;
    }

    public Boolean getCreate() {
        return create;
    }

    public void setCreate(Boolean create) {
        this.create = create;
    }
}
