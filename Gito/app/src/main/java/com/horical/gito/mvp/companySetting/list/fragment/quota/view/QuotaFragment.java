package com.horical.gito.mvp.companySetting.list.fragment.quota.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.mvp.allProject.list.view.AllProjectActivity;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.quota.injection.component.DaggerQuotaComponent;
import com.horical.gito.mvp.companySetting.list.fragment.quota.injection.component.QuotaComponent;
import com.horical.gito.mvp.companySetting.list.fragment.quota.injection.module.QuotaModule;
import com.horical.gito.mvp.companySetting.list.fragment.quota.presenter.QuotaPresenter;
import com.horical.gito.mvp.estate.list.view.EstateActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class QuotaFragment extends BaseCompanyFragment implements IQuotaView, View.OnClickListener {
    public static final String TAG = makeLogTag(QuotaFragment.class);

    @Bind(R.id.ll_number_of_project)
    LinearLayout llNumberOfProject;

    @Bind(R.id.ll_number_of_user)
    LinearLayout llNumberOfUser;

    @Bind(R.id.ll_quota_source_code)
    LinearLayout llQuotaSourceCode;

    @Bind(R.id.ll_quota_estate)
    LinearLayout llQuotaEstate;

    @Bind(R.id.tv_number_of_project)
    TextView tvNumberProject;

    @Bind(R.id.tv_number_of_user)
    TextView tvNumberUser;

    @Bind(R.id.tv_quota_soure_code)
    TextView tvQuotaSoureCode;

    @Bind(R.id.tv_quota_estate)
    TextView tvQuotaEstate;

    private Intent intent;

    @Inject
    QuotaPresenter mPresenter;

    public static QuotaFragment newInstance() {
        Bundle args = new Bundle();
        QuotaFragment fragment = new QuotaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_company_setting_quota;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        QuotaComponent component = DaggerQuotaComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .quotaModule(new QuotaModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    public void setDataQuota() {
        tvNumberProject.setText(mPresenter.getQuotaSummary().getQuotaProjectNumber() + "/"
                + mPresenter.getQuotaInfo().getQuotaProjectNumber());
        tvNumberUser.setText(mPresenter.getQuotaSummary().getQuotaUserMember() + "/"
                + mPresenter.getQuotaInfo().getQuotaUserMember());
        tvQuotaSoureCode.setText(mPresenter.getQuotaSummary().getQuotaSourceCode() + "/"
                + mPresenter.getQuotaInfo().getQuotaSourceCode());
        tvQuotaEstate.setText(mPresenter.getQuotaSummary().getQuotaEstate() + "/"
                + mPresenter.getQuotaInfo().getQuotaDiskCapacitor());
    }

    @Override
    protected void initData() {
        showLoading();
        mPresenter.getInfoQuota();
    }

    @Override
    protected void initListener() {
        llNumberOfProject.setOnClickListener(this);
        llNumberOfUser.setOnClickListener(this);
        llQuotaSourceCode.setOnClickListener(this);
        llQuotaEstate.setOnClickListener(this);
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getInfoQuotaSuccess() {
        mPresenter.getSummaryQuota();
    }

    @Override
    public void getInfoQuotaFailure(String err) {
        hideLoading();
    }

    @Override
    public void getSummaryQuotaSuccess() {
        hideLoading();
        setDataQuota();
        Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getSummaryQuotaFailure(String err) {
        hideLoading();
        Toast.makeText(getContext(), "Failure", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_number_of_project:
                intent = new Intent(getContext(), AllProjectActivity.class);
                intent.putExtra(AllProjectActivity.KEY_FROM, "QuotaFragment");
                startActivity(intent);
                break;
            case R.id.ll_number_of_user:
                mCallBack.onClick();
                break;
            case R.id.ll_quota_source_code:

                break;
            case R.id.ll_quota_estate:
                intent = new Intent(getContext(), EstateActivity.class);
                intent.putExtra(AllProjectActivity.KEY_FROM, "QuotaFragment");
                startActivity(intent);
                break;
        }
    }

    private OnClickListener mCallBack;
    public void setOnClickListener(OnClickListener callBack){
        this.mCallBack = callBack;
    }
    public interface OnClickListener{
        void onClick();
    }
}
