package com.horical.gito.mvp.report.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.report.injection.module.ReportModule;
import com.horical.gito.mvp.report.view.ReportActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ReportModule.class)
public interface ReportComponent {
    void inject(ReportActivity activity);
}
