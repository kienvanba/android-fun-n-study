package com.horical.gito.mvp.myProject.summary.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.TokenFile;
import com.horical.gito.model.Issue;
import com.horical.gito.model.Version;
import com.horical.gito.mvp.myProject.summary.dto.ProgressItem;
import com.horical.gito.model.Section;
import com.horical.gito.mvp.myProject.summary.dto.TaskItem;
import com.horical.gito.mvp.myProject.summary.dto.VersionItem;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.utils.LogUtils;
import com.horical.gito.widget.chartview.circle.CircleAngleAnimation;
import com.horical.gito.widget.chartview.circle.CircleChartView;
import com.horical.gito.widget.chartview.line.LineChartView;
import com.horical.gito.widget.textview.GitOTextView;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;

/**
 * Created by nxon on 3/17/17
 */

public class SummaryAdapter extends SectioningAdapter {

    public static final String TAG = LogUtils.makeLogTag(SummaryAdapter.class);

    public static final int SUMMARY_HEADER_TYPE = 0;
    public static final int SUMMARY_PROGRESS_TYPE = 1;
    public static final int SUMMARY_VERSION_TYPE = 2;
    public static final int SUMMARY_TASK_TITLE_TYPE = 3;
    public static final int SUMMARY_TASK_TYPE = 4;

    private Context mContext;
    private List<Section> mSections;

    public SummaryAdapter(Context mContext, List<Section> mSections) {
        this.mContext = mContext;
        this.mSections = mSections;
    }

    // == VIEW HOLDER == //

    public class HeaderViewHolder extends SectioningAdapter.HeaderViewHolder {

        @Bind(R.id.common_tv_header)
        GitOTextView tvTitle;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class ProgressItemViewHolder extends SectioningAdapter.ItemViewHolder {

        @Bind(R.id.summary_tv_resolved)
        public GitOTextView resolved;

        @Bind(R.id.summary_tv_need_to_test)
        GitOTextView needToTest;

        @Bind(R.id.summary_tv_remain)
        public GitOTextView remain;

        @Bind(R.id.summary_ccv_task)
        CircleChartView circleChartView;

        ProgressItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class VersionItemViewHolder extends SectioningAdapter.ItemViewHolder {

        @Bind(R.id.summary_lcv_time)
        LineChartView lineChartView;

        VersionItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private class TaskItemTitleItemHolder extends SectioningAdapter.ItemViewHolder {

        TaskItemTitleItemHolder(View itemView) {
            super(itemView);
        }
    }

    class TaskItemViewHolder extends SectioningAdapter.ItemViewHolder {

        @Bind(R.id.task_imv_avatar)
        CircleImageView imvAvatar;

        @Bind(R.id.task_tv_name)
        GitOTextView tvName;

        @Bind(R.id.task_tv_role)
        GitOTextView tvRole;

        @Bind(R.id.task_tv_resolved)
        GitOTextView tvResolved;

        @Bind(R.id.task_tv_remain)
        GitOTextView tvRemain;

        @Bind(R.id.task_pgr_avt_loading)
        ProgressBar pgrLoading;

        @Bind(R.id.task_divider)
        View vDivider;

        @Bind(R.id.task_divider_bottom)
        View vDividerBottom;

        @Bind(R.id.task_tv_sort_name)
        GitOTextView tvSortName;

        TaskItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public int getNumberOfSections() {
        return mSections.size();
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        return mSections.get(sectionIndex).getItems().size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public boolean doesSectionHaveFooter(int sectionIndex) {
        return false;
    }

    @Override
    public int getSectionHeaderUserType(int sectionIndex) {
        return mSections.get(sectionIndex).getType();
    }

    @Override
    public int getSectionItemUserType(int sectionIndex, int itemIndex) {
        Section section = mSections.get(sectionIndex);
        return section.getItems().get(itemIndex).getItemViewType();
    }

    @Override
    public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (itemType) {
            case SUMMARY_PROGRESS_TYPE:
                return new ProgressItemViewHolder(inflater.inflate(R.layout.item_summary_progress, parent, false));

            case SUMMARY_VERSION_TYPE:
                return new VersionItemViewHolder(inflater.inflate(R.layout.item_summary_version, parent, false));

            case SUMMARY_TASK_TITLE_TYPE:
                return new TaskItemTitleItemHolder(inflater.inflate(R.layout.item_summary_task_title, parent, false));

            case SUMMARY_TASK_TYPE:
                return new TaskItemViewHolder(inflater.inflate(R.layout.item_summary_task, parent, false));
        }


        throw new IllegalArgumentException("Unrecognized itemType: " + itemType);
    }

    @Override
    public SectioningAdapter.HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (headerType) {
            case SUMMARY_HEADER_TYPE:
                return new HeaderViewHolder(inflater.inflate(R.layout.item_common_header, parent, false));
        }

        throw new IllegalArgumentException("Unrecognized headerType: " + headerType);
    }

    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, int sectionIndex, int itemIndex, int itemType) {
        Section s = mSections.get(sectionIndex);

        switch (itemType) {
            case SUMMARY_PROGRESS_TYPE: {
                ProgressItemViewHolder ivh = (ProgressItemViewHolder) viewHolder;
                ProgressItem progressItem = (ProgressItem) s.getItems().get(itemIndex);

                int resolved = progressItem.getTotalResolved();
                int remain = progressItem.getTotalRemain();
                int needToTest = progressItem.getTotalNeedToTest();
                int total = progressItem.getTotal();

                ivh.resolved.setText(String.valueOf(resolved));
                ivh.remain.setText(String.valueOf(remain));
                ivh.needToTest.setText(String.valueOf(needToTest));

                // Set data for CircleChartView
                ivh.circleChartView.setData(
                        100,
                        (resolved + needToTest) * 100 / total,
                        resolved * 100 / total);
                ivh.circleChartView.setIssues(total);

                CircleAngleAnimation angleAnimation = new CircleAngleAnimation(ivh.circleChartView);
                angleAnimation.setDuration(1000);
                ivh.circleChartView.startAnimation(angleAnimation);
                break;
            }
            case SUMMARY_VERSION_TYPE: {
                VersionItemViewHolder ivh = (VersionItemViewHolder) viewHolder;
                VersionItem versionItem = (VersionItem) s.getItems().get(itemIndex);

                Version v = versionItem.getVersion();

                if (v != null) {
                    ivh.lineChartView.setDateStart(v.getStartDate());
                    ivh.lineChartView.setDateEnd(v.getEndDate());

                    int resolved = v.getResolvedTask();
                    ivh.lineChartView.setResolvedTask(resolved);

                    int total = v.getTotalTask();
                    ivh.lineChartView.setTotalTask(total);
                    ivh.lineChartView.setPercent(resolved * 100 / total);
                }
                break;
            }
            case SUMMARY_TASK_TITLE_TYPE: {
                break;
            }
            case SUMMARY_TASK_TYPE: {
                TaskItemViewHolder ivh = (TaskItemViewHolder) viewHolder;
                TaskItem taskItem = (TaskItem) s.getItems().get(itemIndex);

                // Hide Divider 1st Item
                if (itemIndex == 1) {
                    ivh.vDivider.setVisibility(GONE);
                }

                // Show Divider bottom
                if (itemIndex == s.getItems().size() - 1) {
                    ivh.vDividerBottom.setVisibility(View.VISIBLE);
                }

                // Set issue data
                Issue issue = taskItem.getIssue();
                TokenFile avatar = issue.getAssigns().getAvatar();
                if (avatar != null) {
                    String url = CommonUtils.getURL(avatar);
                    ImageLoader.load(mContext, url, ivh.imvAvatar, ivh.pgrLoading);
                } else {
                    ivh.pgrLoading.setVisibility(View.GONE);
                    ivh.tvSortName.setText(issue.getAssigns().getDisplayName().substring(0, 2).toUpperCase());
                    ivh.tvSortName.setVisibility(View.VISIBLE);
                }

                // Set role
                if (issue.getAssigns().isAdmin()) {
                    ivh.tvRole.setText("Admin");
                } else {
                    ivh.tvRole.setText(issue.getAssigns().getRoleCompany().getRoleName());
                }

                ivh.tvName.setText(taskItem.getIssue().getAssigns().getDisplayName());
                ivh.tvResolved.setText(String.valueOf(issue.getResolve()));
                ivh.tvRemain.setText(String.valueOf(issue.getRemain()));
                break;
            }
            default:
                throw new IllegalArgumentException("Unrecognized itemType: " + itemType);
        }
    }

    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex, int headerType) {
        Section s = mSections.get(sectionIndex);

        switch (headerType) {
            case SUMMARY_HEADER_TYPE: {
                HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
                hvh.tvTitle.setText(s.getTitle());
                break;
            }
            default:
                throw new IllegalArgumentException("Unrecognized headerType: " + headerType);
        }
    }
}