package com.horical.gito.mvp.myProject.sourceCode.list.injection.component;

import com.horical.gito.mvp.myProject.sourceCode.list.injection.module.SourceCodeModule;
import com.horical.gito.mvp.myProject.sourceCode.list.view.SourceCodeActivity;
import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;

import dagger.Component;

/**
 * Created by Tin on 22-Nov-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = SourceCodeModule.class)
public interface SourceCodeComponent {
    void inject(SourceCodeActivity activity);
}
