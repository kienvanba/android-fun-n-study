package com.horical.gito.mvp.salary.bonus.list.fragments.approved.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.bonus.list.fragments.approved.presenter.ListApprovedFragmentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListApprovedFragmentModule {
    @PerActivity
    @Provides
    ListApprovedFragmentPresenter provideSalaryPresenter(){
        return new ListApprovedFragmentPresenter();
    }
}
