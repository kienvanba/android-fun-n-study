package com.horical.gito.mvp.myProject.sourceCode.detail.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.myProject.sourceCode.detail.adapter.CodeViewPagerAdapter;
import com.horical.gito.mvp.myProject.sourceCode.detail.injection.component.DaggerSourceCodeDetailComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.injection.component.SourceCodeDetailComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.injection.module.SourceCodeDetailModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.presenter.SourceCodeDetailPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class SourceCodeDetailActivity extends BaseActivity implements ISourceCodeDetailView, View.OnClickListener {

    private static final String TAG = makeLogTag(SourceCodeDetailActivity.class);

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.tv_browse_code)
    TextView tvBrowse;

    @Bind(R.id.tv_branch_code)
    TextView tvBranch;

    @Bind(R.id.tv_commit_code)
    TextView tvCommit;

    @Bind(R.id.tv_tag_code)
    TextView tvTag;

    @Bind(R.id.tv_user_code)
    TextView tvUser;

    @Bind(R.id.view_pager_code)
    ViewPager viewPager;

    @Bind(R.id.img_browse)
    CircleImageView imgBrowse;

    @Bind(R.id.img_branch)
    CircleImageView imgBranch;

    @Bind(R.id.img_commit)
    CircleImageView imgCommit;

    @Bind(R.id.img_tag)
    CircleImageView imgTag;

    @Bind(R.id.img_user)
    CircleImageView imgUser;


    private CodeViewPagerAdapter adapter;

    @Inject
    SourceCodeDetailPresenter mPresenter;

    protected int getLayoutId() {
        return R.layout.activity_detail_source_code;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);

        SourceCodeDetailComponent component = DaggerSourceCodeDetailComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .sourceCodeDetailModule(new SourceCodeDetailModule())
                .build();
        component.inject(this);

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle("UI IOS");
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);

        tvBrowse.setSelected(true);
        imgBrowse.setImageResource(R.color.blue);

        adapter = new CodeViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }

    protected void initListener() {
        tvBrowse.setOnClickListener(this);
        tvBranch.setOnClickListener(this);
        tvCommit.setOnClickListener(this);
        tvTag.setOnClickListener(this);
        tvUser.setOnClickListener(this);
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {

            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                imgBrowse.setImageResource(R.color.grey);
                imgBranch.setImageResource(R.color.grey);
                imgCommit.setImageResource(R.color.grey);
                imgTag.setImageResource(R.color.grey);
                imgUser.setImageResource(R.color.grey);
                switch (position) {
                    case 0:
                        updateTextViewSelected(tvBrowse);
                        imgBrowse.setImageResource(R.color.blue);
                        break;
                    case 1:
                        updateTextViewSelected(tvBranch);
                        imgBranch.setImageResource(R.color.blue);
                        break;
                    case 2:
                        updateTextViewSelected(tvCommit);
                        imgCommit.setImageResource(R.color.blue);
                        break;
                    case 3:
                        updateTextViewSelected(tvTag);
                        imgTag.setImageResource(R.color.blue);
                        break;
                    case 4:
                        updateTextViewSelected(tvUser);
                        imgUser.setImageResource(R.color.blue);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void updateTextViewSelected(TextView tv) {
        tvBrowse.setSelected(false);
        tvBranch.setSelected(false);
        tvCommit.setSelected(false);
        tvTag.setSelected(false);
        tvUser.setSelected(false);
        tv.setSelected(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_browse_code: {
                viewPager.setCurrentItem(0);
                imgBrowse.setImageResource(R.color.blue);
                break;
            }
            case R.id.tv_branch_code: {
                viewPager.setCurrentItem(1);
                imgBranch.setImageResource(R.color.blue);
                break;
            }
            case R.id.tv_commit_code: {
                viewPager.setCurrentItem(2);
                imgCommit.setImageResource(R.color.blue);
                break;
            }
            case R.id.tv_tag_code: {
                viewPager.setCurrentItem(3);
                imgTag.setImageResource(R.color.blue);
                break;
            }
            case R.id.tv_user_code: {
                viewPager.setCurrentItem(4);
                imgUser.setImageResource(R.color.blue);
                break;
            }

        }
    }

}
