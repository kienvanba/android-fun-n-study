package com.horical.gito.mvp.roomBooking.detail.fragment.week.adapter;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.mvp.roomBooking.detail.dto.WeekDetailDto;

/**
 * Created by Lemon on 3/31/2017.
 */

public class WeekDetailItem implements IRecyclerItem {
    public int lengh;

    public WeekDetailDto detailItem;

    public WeekDetailItem() {
    }

    public WeekDetailItem(WeekDetailDto detailItem) {
        this.detailItem = detailItem;
    }


    @Override
    public int getItemViewType() {
        return 0;
    }
}
