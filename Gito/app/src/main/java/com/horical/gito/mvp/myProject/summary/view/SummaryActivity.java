package com.horical.gito.mvp.myProject.summary.view;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.myProject.summary.adapter.SummaryAdapter;
import com.horical.gito.mvp.myProject.summary.injection.component.DaggerSummaryComponent;
import com.horical.gito.mvp.myProject.summary.injection.component.SummaryComponent;
import com.horical.gito.mvp.myProject.summary.injection.module.SummaryModule;
import com.horical.gito.mvp.myProject.summary.presenter.SummaryPresenter;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SummaryActivity extends DrawerActivity implements ISummaryView, View.OnClickListener {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.summary_rcv)
    RecyclerView rcvSummary;

    @Bind(R.id.summary_ln_no_data)
    LinearLayout lnNoData;

    @Bind(R.id.summary_refresh_layout)
    SwipeRefreshLayout mRfLayout;

    private SummaryAdapter mAdapter;

    @Inject
    SummaryPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_summary;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_SUMMARY;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        SummaryComponent component = DaggerSummaryComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .summaryModule(new SummaryModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(GitOStorage.getInstance().getProject().getName());
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);

        mAdapter = new SummaryAdapter(this, mPresenter.getSections());
        rcvSummary.setLayoutManager(new StickyHeaderLayoutManager());
        rcvSummary.setAdapter(mAdapter);

        mPresenter.getData();
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                openDrawer();
            }

            @Override
            public void onRightClicked() {

            }
        });

        mRfLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getData();
            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void onGetDataSuccess(boolean hasData) {
        mRfLayout.setRefreshing(false);
        if (hasData) {
            mAdapter.notifyAllSectionsDataSetChanged();
            lnNoData.setVisibility(View.GONE);
        } else {
            lnNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGetDataFailure(String error) {
        mRfLayout.setRefreshing(false);
        hideLoading();
        showErrorDialog(error);
    }
}