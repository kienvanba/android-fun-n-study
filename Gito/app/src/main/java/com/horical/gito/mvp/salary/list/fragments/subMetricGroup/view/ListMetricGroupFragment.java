package com.horical.gito.mvp.salary.list.fragments.subMetricGroup.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.mvp.salary.detail.metricGroup.view.DetailMetricGroupActivity;
import com.horical.gito.mvp.salary.list.fragments.BaseListSalaryFragment;
import com.horical.gito.mvp.salary.list.fragments.subMetricGroup.adapter.ListMetricGroupAdapter;
import com.horical.gito.mvp.salary.list.fragments.subMetricGroup.injection.component.DaggerIListMetricGroupFragmentComponent;
import com.horical.gito.mvp.salary.list.fragments.subMetricGroup.injection.component.IListMetricGroupFragmentComponent;
import com.horical.gito.mvp.salary.list.fragments.subMetricGroup.injection.module.ListMetricGroupFragmentModule;
import com.horical.gito.mvp.salary.list.fragments.subMetricGroup.presenter.ListMetricGroupFragmentPresenter;

import javax.inject.Inject;

import butterknife.Bind;

public class ListMetricGroupFragment extends BaseListSalaryFragment implements View.OnClickListener,
        IListMetricGroupFragmentView {

    @Bind(R.id.rcv_metric_group)
    RecyclerView rcvMetricGroup;

    @Bind(R.id.tv_no_data)
    TextView tvNoData;

    @Inject
    ListMetricGroupFragmentPresenter mPresenter;

    private ListMetricGroupAdapter mAdapter;

    public static ListMetricGroupFragment newInstance() {
        Bundle args = new Bundle();
        ListMetricGroupFragment fragment = new ListMetricGroupFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list_metric;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IListMetricGroupFragmentComponent component = DaggerIListMetricGroupFragmentComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .listMetricGroupFragmentModule(new ListMetricGroupFragmentModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        showLoading();
        mPresenter.requestGetAllMetricGroup();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        mAdapter = new ListMetricGroupAdapter(getActivity(), mPresenter.getMetricGroups(), new ListMetricGroupAdapter.MetricAdapterListener() {
            @Override
            public void onSelected(int position) {
                Intent detailMetricIntent = new Intent(getActivity(), DetailMetricGroupActivity.class);
                detailMetricIntent.putExtra(AppConstants.METRIC_GROUP_ID_KEY, mPresenter.getMetricGroups().get(position).get_id());
                startActivity(detailMetricIntent);
            }

            @Override
            public void onRemove(int position) {
                showLoading();
                mPresenter.requestDeleteMetricGroup(mPresenter.getMetricGroups().get(position).get_id());
            }
        });
        rcvMetricGroup.setAdapter(mAdapter);
        rcvMetricGroup.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvNoData.setText(getString(R.string.no_data, getString(R.string.metric_group)));
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void requestGetAllMetricGroupSuccess() {
        hideLoading();
        checkEmptyMetricGroup();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void requestGetAllMetricGroupError(RestError error) {
        hideLoading();
        showErrorDialog(error.message, null);
    }

    @Override
    public void requestDeleteMetricGroupSuccess() {
        hideLoading();
        checkEmptyMetricGroup();
        mAdapter.setDataChanged(mPresenter.getMetricGroups());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void requestDeleteMetricGroupError(RestError error) {
        hideLoading();
        showErrorDialog(error.message, null);
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }


    private void checkEmptyMetricGroup() {
        if (mPresenter.getMetricGroups().size() > 0) {
            tvNoData.setVisibility(View.GONE);
            rcvMetricGroup.setVisibility(View.VISIBLE);
        } else {
            tvNoData.setVisibility(View.VISIBLE);
            rcvMetricGroup.setVisibility(View.GONE);
        }
    }
}