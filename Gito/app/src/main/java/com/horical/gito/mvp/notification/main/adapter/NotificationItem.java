package com.horical.gito.mvp.notification.main.adapter;

import com.horical.gito.base.IRecyclerItem;

/**
 * Created by Dragonoid on 3/17/2017.
 */

public class NotificationItem implements IRecyclerItem {
    @Override
    public int getItemViewType() {
        return 0;
    }
    String mTittle;
    String mReason;
    String mUnit;
    int mStatus;
    public NotificationItem(String tittle, String reason,String unit,int status ){
        mTittle = tittle;
        mReason = reason;
        mUnit = unit;
        mStatus = status;
    }
}
