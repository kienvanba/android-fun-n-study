package com.horical.gito.mvp.salary.bonus.list.fragments.pending.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.mvp.salary.bonus.detail.view.DetailBonusActivity;
import com.horical.gito.mvp.salary.bonus.list.fragments.BaseListBonusFragment;
import com.horical.gito.mvp.salary.bonus.list.fragments.pending.adapter.BonusPendingAdapter;
import com.horical.gito.mvp.salary.bonus.list.fragments.pending.injection.component.DaggerIListPendingFragmentComponent;
import com.horical.gito.mvp.salary.bonus.list.fragments.pending.injection.component.IListPendingFragmentComponent;
import com.horical.gito.mvp.salary.bonus.list.fragments.pending.injection.module.ListPendingFragmentModule;
import com.horical.gito.mvp.salary.bonus.list.fragments.pending.presenter.ListPendingFragmentPresenter;

import javax.inject.Inject;

import butterknife.Bind;

public class ListPendingFragment extends BaseListBonusFragment implements View.OnClickListener,
        IListPendingFragmentView {

    @Bind(R.id.rcv_bonus)
    RecyclerView rcvBonus;

    @Inject
    ListPendingFragmentPresenter mPresenter;

    private BonusPendingAdapter mAdapter;

    public static ListPendingFragment newInstance() {
        Bundle args = new Bundle();
        ListPendingFragment fragment = new ListPendingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list_bonus;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IListPendingFragmentComponent component = DaggerIListPendingFragmentComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .listPendingFragmentModule(new ListPendingFragmentModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        showLoading();
        mPresenter.requestGetPendingInputAddon();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        mAdapter = new BonusPendingAdapter(getActivity(), mPresenter.getListInputAddon(), new BonusPendingAdapter.PendingAdapterListener() {
            @Override
            public void onSelected(int position) {
                Intent detailBonusIntent = new Intent(getActivity(), DetailBonusActivity.class);
                detailBonusIntent.putExtra(AppConstants.BONUS_ID_KEY, mPresenter.getListInputAddon().get(position).get_id());
                detailBonusIntent.putExtra(AppConstants.BONUS_MODE_KEY, getString(R.string.bonus_detail));
                startActivity(detailBonusIntent);
            }

            @Override
            public void onDelete(int position) {
                Toast.makeText(getActivity(), "delete at position:" + position, Toast.LENGTH_SHORT).show();
            }
        });
        rcvBonus.setAdapter(mAdapter);
        rcvBonus.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void requestGetPendingInputAddonSuccess() {
        hideLoading();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void requestGetPendingInputAddonError(RestError error) {
        hideLoading();
        showErrorDialog(error.message, null);
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }
}

