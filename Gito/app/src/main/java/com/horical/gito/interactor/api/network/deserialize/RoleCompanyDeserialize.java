package com.horical.gito.interactor.api.network.deserialize;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.horical.gito.model.RoleCompany;
import com.horical.gito.utils.GsonUtils;

import java.lang.reflect.Type;

public class RoleCompanyDeserialize implements JsonDeserializer<RoleCompany> {

    @Override
    public RoleCompany deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        RoleCompany roleCompany;
        try {
            roleCompany = GsonUtils.createGson(RoleCompany.class).fromJson(json, RoleCompany.class);
        } catch (Exception ex) {
            roleCompany = new RoleCompany();
            roleCompany.setId(json.getAsString());
        }
        return roleCompany;
    }
}
