package com.horical.gito.mvp.user.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.survey.list.presenter.SurveyPresenter;
import com.horical.gito.mvp.user.detail.presenter.UserDetailPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by thanhle on 4/7/17.
 */
@Module
public class UserDetailModule {
    @PerActivity
    @Provides
    UserDetailPresenter providerUserDetailPresenter(){
        return new UserDetailPresenter();
    }
}
