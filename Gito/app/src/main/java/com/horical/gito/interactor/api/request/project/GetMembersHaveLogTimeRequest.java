package com.horical.gito.interactor.api.request.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GetMembersHaveLogTimeRequest {

    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("endDate")
    @Expose
    public Date endDate;

    @SerializedName("creatorId")
    @Expose
    public List<String> creatorIds;

}