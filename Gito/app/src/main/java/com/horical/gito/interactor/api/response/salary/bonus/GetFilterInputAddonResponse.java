package com.horical.gito.interactor.api.response.salary.bonus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.InputAddon;

import java.util.ArrayList;
import java.util.List;

public class GetFilterInputAddonResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    private List<InputAddon> addons;

    public List<InputAddon> getAddons() {
        if (addons == null) {
            addons = new ArrayList<>();
        }
        return addons;
    }

    public void setAddons(List<InputAddon> addons) {
        this.addons = addons;
    }
}
