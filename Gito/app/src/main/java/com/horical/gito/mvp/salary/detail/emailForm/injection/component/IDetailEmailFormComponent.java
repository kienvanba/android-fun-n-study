package com.horical.gito.mvp.salary.detail.emailForm.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.detail.emailForm.injection.module.DetailEmailFormModule;
import com.horical.gito.mvp.salary.detail.emailForm.view.DetailEmailFormActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = DetailEmailFormModule.class)
public interface IDetailEmailFormComponent {
    void inject(DetailEmailFormActivity activity);
}
