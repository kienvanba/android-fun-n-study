package com.horical.gito.mvp.salary.bonus.list.fragments.approved.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

public interface IListApprovedFragmentView extends IView {
    void requestGetApprovedInputAddonSuccess();

    void requestGetApprovedInputAddonError(RestError error);

    void showLoading();

    void hideLoading();
}
