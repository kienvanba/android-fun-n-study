package com.horical.gito.interactor.api.network.deserialize;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.horical.gito.model.Company;
import com.horical.gito.utils.GsonUtils;

import java.lang.reflect.Type;

public class CompanyDeserialize implements JsonDeserializer<Company> {

    @Override
    public Company deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
        Company companyId;
        try {
            companyId = GsonUtils.createGson(Company.class).fromJson(json, Company.class);
        } catch (Exception e) {
            companyId = new Company();
            companyId.set_id(json.getAsString());
        }
        return companyId;
    }
}
