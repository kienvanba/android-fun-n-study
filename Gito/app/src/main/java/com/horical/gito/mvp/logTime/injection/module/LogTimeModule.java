package com.horical.gito.mvp.logTime.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.logTime.presenter.LogTimePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class LogTimeModule {

    @PerActivity
    @Provides
    LogTimePresenter provideLogTimePresenter(){
        return new LogTimePresenter();
    }
}
