package com.horical.gito.interactor.api.response.document;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Document;

import java.util.List;

public class GetAllDocumentResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Document> documents;
}
