package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Staff implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("userId")
    @Expose
    private User userId;

    @SerializedName("salary")
    @Expose
    private double salary;

    @SerializedName("salaryPerformance")
    @Expose
    private double salaryPerformance;

    @SerializedName("salaryFlexible")
    @Expose
    private double salaryFlexible;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getSalaryPerformance() {
        return salaryPerformance;
    }

    public void setSalaryPerformance(double salaryPerformance) {
        this.salaryPerformance = salaryPerformance;
    }

    public double getSalaryFlexible() {
        return salaryFlexible;
    }

    public void setSalaryFlexible(double salaryFlexible) {
        this.salaryFlexible = salaryFlexible;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
