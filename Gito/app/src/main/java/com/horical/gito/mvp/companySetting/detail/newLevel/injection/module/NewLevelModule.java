package com.horical.gito.mvp.companySetting.detail.newLevel.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.detail.newLevel.presenter.NewLevelPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class NewLevelModule {
    @PerActivity
    @Provides
    NewLevelPresenter provideNewLevelPresenter(){
        return new NewLevelPresenter();
    }
}
