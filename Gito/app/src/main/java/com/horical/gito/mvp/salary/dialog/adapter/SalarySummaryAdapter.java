package com.horical.gito.mvp.salary.dialog.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.salary.dto.metric.SummaryDto;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SalarySummaryAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<SummaryDto> items;
    private SummaryAdapterListener callback;

    public SalarySummaryAdapter(Context context, List<SummaryDto> items, SummaryAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    public void setDataChanged(List<SummaryDto> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_salary_list_summary, parent, false);
        return new SummaryHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SummaryDto summary = items.get(position);
        SummaryHolder summaryHolder = (SummaryHolder) holder;
        summaryHolder.tvAcronym.setText(summary.getAcronym());
        summaryHolder.tvTitle.setText(summary.getTitle());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class SummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.imv_check)
        ImageView imvCheck;

        @Bind(R.id.tv_title)
        TextView tvTitle;

        @Bind(R.id.tv_acronym)
        TextView tvAcronym;

        @Bind(R.id.imv_delete)
        ImageView imvDelete;


        SummaryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imvCheck.setOnClickListener(this);
            imvDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.imv_check:
                    callback.onCheckedChanged(getAdapterPosition());
                    break;

                case R.id.imv_delete:
                    callback.onRemove(getAdapterPosition());
                    break;

                default:
                    break;
            }
        }
    }

    public interface SummaryAdapterListener {
        void onCheckedChanged(int position);

        void onRemove(int position);
    }
}