package com.horical.gito.mvp.checkin.list.view.dto;

import com.horical.gito.base.IView;

/**
 * Created by Penguin on 23-Nov-16.
 */

public interface ICheckinView extends IView {
    void showLoading();

    void hideLoading();

    void getAllCheckinSuccess();

}
