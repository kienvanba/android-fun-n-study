package com.horical.gito.mvp.myProject.setting.fragment.activity.member.injection.module;

import com.horical.gito.mvp.myProject.setting.fragment.activity.member.presenter.NewMemberPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {
    @Provides
    NewMemberPresenter provideMemberPresenter(){
        return new NewMemberPresenter();
    }
}
