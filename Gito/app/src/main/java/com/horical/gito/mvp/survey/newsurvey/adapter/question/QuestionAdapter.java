package com.horical.gito.mvp.survey.newsurvey.adapter.question;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Question;
import com.horical.gito.mvp.survey.newsurvey.adapter.option.OptionAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nhattruong251295 on 3/27/2017.
 */

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionHolder>{
    private Context mContext;
    private List<Question> mList;

    public QuestionAdapter(Context mContext, List<Question> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public QuestionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_question,parent,false);
        return new QuestionHolder(view);
    }

    @Override
    public void onBindViewHolder(QuestionHolder holder, final int position) {
        holder.tvQuestion.setText("Question " + (position + 1));
        holder.tvContentQuestion.setText(mList.get(position).getTitle());

        Question question = mList.get(position);

        holder.optionAdapter = new OptionAdapter(mContext,question.getOptionList());
        holder.optionAdapter.setType(question.getType());
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        holder.mRcvOption.setLayoutManager(layoutManager);
        holder.mRcvOption.setHasFixedSize(false);
        holder.mRcvOption.setAdapter(holder.optionAdapter);
        holder.mRcvOption.setVisibility(View.VISIBLE);

        if(position == getItemCount() - 1){
            holder.viewQuestion.setVisibility(View.GONE);
        }

        holder.llDeleteQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.deleteOption(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class QuestionHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_question)
        TextView tvQuestion;

        @Bind(R.id.tv_content_question)
        TextView tvContentQuestion;

        @Bind(R.id.rcv_option)
        RecyclerView mRcvOption;

        @Bind(R.id.ll_delete_question)
        LinearLayout llDeleteQuestion;

        @Bind(R.id.view_question)
        View viewQuestion;

        OptionAdapter optionAdapter;
        public QuestionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    private QuestionAdapter.OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(QuestionAdapter.OnItemClickListener callBack) {
        this.mOnItemClickListener = callBack;
    }

    public interface OnItemClickListener {
        void deleteOption(int position);
    }
}

/*public class QuestionAdapter extends BaseRecyclerAdapter {
    public static final int QUESTION_ITEM = 0;
    public static final int OPTION_ITEM = 1;

    public QuestionAdapter(Context context, List<IRecyclerItem> items) {
        super(items, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case QUESTION_ITEM:
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_question, parent, false);
                return new QuestionHolder(view);
            case OPTION_ITEM:
                View option = LayoutInflater.from(mContext).inflate(R.layout.item_option, parent, false);
                return new OptionHolder(option);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        switch (holder.getItemViewType()){
            case QUESTION_ITEM:
                QuestionItem item = (QuestionItem) mItems.get(position);
                item.content = "This is content of question " + position;
                QuestionHolder questionHolder = (QuestionHolder) holder;
                questionHolder.tvContentQuestion.setText(item.content);
                break;
            case OPTION_ITEM:
                OptionItem optionItem = (OptionItem) mItems.get(position);
                optionItem.content = "Option " + position;
                OptionHolder optionHolder = (OptionHolder) holder;
                optionHolder.edtOption.setText(optionItem.content);
                optionHolder.llDeleteOption.setVisibility(View.GONE);
                break;
        }
    }

}*/
