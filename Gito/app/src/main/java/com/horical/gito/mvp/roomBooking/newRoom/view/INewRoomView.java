package com.horical.gito.mvp.roomBooking.newRoom.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

/**
 * Created by Lemon on 3/27/2017.
 */

public interface INewRoomView extends IView {

    void showLoading();

    void hideLoading();

    void createRoomSuccess();

    void createRoomFailure(RestError err);

    void validateFailedNameEmpty();
}
