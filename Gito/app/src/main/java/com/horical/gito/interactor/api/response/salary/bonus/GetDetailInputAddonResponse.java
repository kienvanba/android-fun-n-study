package com.horical.gito.interactor.api.response.salary.bonus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.InputAddon;

public class GetDetailInputAddonResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    private InputAddon inputAddon;

    public InputAddon getInputAddon() {
        if (inputAddon == null) {
            inputAddon = new InputAddon();
        }
        return inputAddon;
    }

    public void setInputAddon(InputAddon inputAddon) {
        this.inputAddon = inputAddon;
    }
}
