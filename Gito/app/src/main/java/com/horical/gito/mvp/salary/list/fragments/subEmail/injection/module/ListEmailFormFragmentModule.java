package com.horical.gito.mvp.salary.list.fragments.subEmail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.list.fragments.subEmail.presenter.ListEmailFormFragmentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListEmailFormFragmentModule {
    @PerActivity
    @Provides
    ListEmailFormFragmentPresenter provideSalaryPresenter(){
        return new ListEmailFormFragmentPresenter();
    }
}
