package com.horical.gito.mvp.salary.list.fragments.subMetricGroup.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.list.fragments.subMetricGroup.presenter.ListMetricGroupFragmentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListMetricGroupFragmentModule {
    @PerActivity
    @Provides
    ListMetricGroupFragmentPresenter provideSalaryPresenter(){
        return new ListMetricGroupFragmentPresenter();
    }
}
