package com.horical.gito.mvp.companySetting.list.fragment.department.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IDepartmentView extends IView {
    void getAllDepartmentSuccess();

    void getAllDepartmentFailure(String err);

    void deleteDepartmentSuccess(int position);

    void deleteDepartmentFailure(String err);
}
