package com.horical.gito.mvp.signUp.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.user.RegisterResponse;
import com.horical.gito.mvp.intro.view.IntroActivity;
import com.horical.gito.mvp.signUp.injection.component.DaggerSignUpComponent;
import com.horical.gito.mvp.signUp.injection.component.SignUpComponent;
import com.horical.gito.mvp.signUp.injection.module.SignUpModule;
import com.horical.gito.mvp.signUp.presenter.SignUpPresenter;
import com.horical.gito.widget.button.GitOButton;
import com.horical.gito.widget.dialog.DialogPositiveNegative;
import com.horical.gito.widget.edittext.GitOEditText;
import com.horical.gito.widget.textview.GitOTextView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class SignUpActivity extends BaseActivity implements ISignUpView, View.OnClickListener {

    private static final String TAG = makeLogTag(SignUpActivity.class);

    @Bind(R.id.sign_up_btn_back)
    ImageView mBtnBack;

    @Bind(R.id.sign_up_btn_sign_up)
    GitOButton mBtnSignUp;

    @Bind(R.id.sign_up_edt_email)
    GitOEditText mEdtEmail;

    @Bind(R.id.sign_up_edt_company_id)
    GitOEditText mEdtCompanyId;

    @Bind(R.id.sign_up_edt_password)
    GitOEditText mEdtPassword;

    @Bind(R.id.sign_up_edt_reenter_password)
    GitOEditText mEdtRePassword;

    @Bind(R.id.sign_up_tv_description)
    GitOTextView mTvDescription;

    @Inject
    SignUpPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        Window window = getWindow();
        window.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.transparent));
        }

        SignUpComponent component = DaggerSignUpComponent.builder()
                .signUpModule(new SignUpModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        Spannable spannable = new SpannableString(getString(R.string.sign_up_description));
        spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#009DF7")),
                30,
                getString(R.string.sign_up_description).length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTvDescription.setText(spannable);
//        -----------------------------------
    }

    protected void initListener() {
        mBtnBack.setOnClickListener(this);
        mBtnSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mBtnBack.getId()) {
            Intent intent = new Intent(this, IntroActivity.class);
            startActivity(intent);
            finish();
        } else if (v.getId() == mBtnSignUp.getId()) {
            mPresenter.registerUser(
                    mEdtEmail.getText().toString(),
                    mEdtCompanyId.getText().toString(),
                    mEdtPassword.getText().toString(),
                    mEdtRePassword.getText().toString()
            );
        }
    }

    @Override
    public void errorEmailInvalid() {
        showErrorDialog(getString(R.string.email_invalid));
    }

    @Override
    public void errorConfirmPassword() {
        showErrorDialog(getString(R.string.sign_up_wrong_confirm_password));
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void signUpError(RestError error) {
        showErrorDialog(error.message);
    }

    @Override
    public void signUpSuccess(RegisterResponse response) {
        showConfirmDialog(getString(R.string.register_success), getString(R.string.confirm_email), new DialogPositiveNegative.IPositiveNegativeDialogListener() {
            @Override
            public void onIPositiveNegativeDialogAnswerPositive(DialogPositiveNegative dialog) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_APP_EMAIL);
                startActivity(intent);
                startActivity(Intent.createChooser(intent, getString(R.string.choose_email_client)));
            }

            @Override
            public void onIPositiveNegativeDialogAnswerNegative(DialogPositiveNegative dialog) {
                dialog.dismiss();
            }
        });
    }
}
