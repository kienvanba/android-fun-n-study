package com.horical.gito.mvp.popup.popupTask;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.horical.gito.R;
import com.horical.gito.mvp.popup.popupTask.adapter.PopupTaskAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 3/9/17.
 */

public class PopupTask extends PopupWindow {

    @Bind(R.id.rcv_task)
    RecyclerView rcvPopup;

    private ArrayList<String> mList = new ArrayList<>();
    private PopupTaskAdapter adapter;
    private Context mContext;

    public PopupTask(Context context, OnItemSelectedListener callBack) {
        this.mContext = context;
        this.mCallBack = callBack;
        setOutsideTouchable(true);
        setBackgroundDrawable(new BitmapDrawable());
    }

    public void showSortBy(View anchorView, String key) {
        mList.add("Date");
        mList.add("Title");
        mList.add("Priority");
        mList.add("User");
        mList.add("Component");
        mList.add("Version");
        setFocusable(true);
        setWidth(anchorView.getWidth());
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_popup_recycleview, null, false);
        setContentView(view);
        ButterKnife.bind(this, view);

        adapter = new PopupTaskAdapter(mContext, mList, key);
        adapter.setOnItemClickListener(new PopupTaskAdapter.OnItemClickListener() {
            @Override
            public void onClick(String key) {
                mCallBack.onClickSelected(key);
                dismiss();
            }
        });
        rcvPopup.setHasFixedSize(true);
        rcvPopup.setLayoutManager(new LinearLayoutManager(mContext));
        rcvPopup.setAdapter(adapter);
        showAsDropDown(anchorView);
    }

    public void showTaskType(View anchorView, String key) {
        mList.add("Task");
        mList.add("Bug");
        mList.add("Feedback");
        mList.add("Question");
        mList.add("Inform");
        mList.add("Request");
        setFocusable(true);
        setWidth(anchorView.getWidth());
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_popup_recycleview, null, false);
        setContentView(view);
        ButterKnife.bind(this, view);

        adapter = new PopupTaskAdapter(mContext, mList, key);
        adapter.setOnItemClickListener(new PopupTaskAdapter.OnItemClickListener() {
            @Override
            public void onClick(String key) {
                mCallBack.onClickSelected(key);
                dismiss();
            }
        });
        rcvPopup.setHasFixedSize(true);
        rcvPopup.setLayoutManager(new LinearLayoutManager(mContext));
        rcvPopup.setAdapter(adapter);
        showAsDropDown(anchorView);
    }

    public void showStatus(View anchorView, String key) {
        mList.add("Open");
        mList.add("Doing");
        mList.add("Done");
        mList.add("Closed");
        mList.add("Pending");
        mList.add("Rejected");
        setFocusable(true);
        setWidth(anchorView.getWidth());
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_popup_recycleview, null, false);
        setContentView(view);
        ButterKnife.bind(this, view);

        adapter = new PopupTaskAdapter(mContext, mList, key);
        adapter.setOnItemClickListener(new PopupTaskAdapter.OnItemClickListener() {
            @Override
            public void onClick(String key) {
                mCallBack.onClickSelected(key);
                dismiss();
            }
        });
        rcvPopup.setHasFixedSize(true);
        rcvPopup.setLayoutManager(new LinearLayoutManager(mContext));
        rcvPopup.setAdapter(adapter);
        showAsDropDown(anchorView);
    }

    public void showPriority(View anchorView, String key) {
        mList.add("Immediate");
        mList.add("Urgent");
        mList.add("High");
        mList.add("Normal");
        mList.add("Low");
        mList.add("Next Phase");
        setFocusable(true);
        setWidth(anchorView.getWidth());
        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_popup_recycleview, null, false);
        setContentView(view);
        ButterKnife.bind(this, view);

        adapter = new PopupTaskAdapter(mContext, mList, key);
        adapter.setOnItemClickListener(new PopupTaskAdapter.OnItemClickListener() {
            @Override
            public void onClick(String key) {
                mCallBack.onClickSelected(key);
                dismiss();
            }
        });
        rcvPopup.setHasFixedSize(true);
        rcvPopup.setLayoutManager(new LinearLayoutManager(mContext));
        rcvPopup.setAdapter(adapter);
        showAsDropDown(anchorView);
    }
    private OnItemSelectedListener mCallBack;

    public interface OnItemSelectedListener {
        void onClickSelected(String codeFilter);
    }

    public static int getOrderInList(String type){
        switch (type){
            case "Task":case "Open":case "Immediate":
                return 0;
            case "Bug":case "Doing":case "Urgent":
                return 1;
            case "Feedback":case "Done":case "High":
                return 2;
            case "Question":case "Closed":case "Normal":
                return 3;
            case "Inform":case "Pending":case "Low":
                return 4;
            case "Request":case "Rejected":case "Next Phase":
                return 5;
        }
        return 0;
    }

    public static String getTypeByOrder(int index){
        switch (index){
            case 0:
                return "Task";
            case 1:
                return "Bug";
            case 2:
                return "Feedback";
            case 3:
                return "Question";
            case 4:
                return "Inform";
            case 5:
                return "Request";
            default:
                return "Unknown";
        }
    }

    public static String getStatusByOrder(int index){
        switch (index){
            case 0:
                return "Open";
            case 1:
                return "Doing";
            case 2:
                return "Done";
            case 3:
                return "Closed";
            case 4:
                return "Pending";
            case 5:
                return "Rejected";
            default:
                return "Unknown";
        }
    }

    public static String getPriorityByOrder(int index){
        switch (index){
            case 0:
                return "Immediate";
            case 1:
                return "Urgent";
            case 2:
                return "High";
            case 3:
                return "Normal";
            case 4:
                return "Low";
            case 5:
                return "Next Phase";
            default:
                return "Unknown";
        }
    }
}