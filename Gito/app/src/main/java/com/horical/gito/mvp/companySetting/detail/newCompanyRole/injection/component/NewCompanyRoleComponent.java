package com.horical.gito.mvp.companySetting.detail.newCompanyRole.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.injection.module.NewCompanyRoleModule;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.view.NewCompanyRoleActivity;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = NewCompanyRoleModule.class)
public interface NewCompanyRoleComponent {
    void inject(NewCompanyRoleActivity activity);
}
