package com.horical.gito.interactor.api.request.meeting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Lemon on 3/7/2017.
 */

public class UpdateMeetingRequest {

    @SerializedName("startDate")
    @Expose
    public String startDate;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("desc")
    @Expose
    public String desc;
}
