package com.horical.gito.mvp.estate.adding.presenter;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.horical.gito.base.BasePresenter;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.file.UploadFileRequest;
import com.horical.gito.interactor.api.request.file.UploadFileResponse;
import com.horical.gito.interactor.api.response.estate.CreateWarehouseEstateResponse;
import com.horical.gito.model.estate.WarehouseEstate;
import com.horical.gito.mvp.estate.adding.view.CreateWarehouseEstateActivity;
import com.horical.gito.mvp.estate.adding.view.ICreateWarehouseEstateView;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.utils.LogUtils;

import org.w3c.dom.Text;

/**
 * Created by hoangsang on 4/13/17.
 */

public class CreateWarehouseEstatePresenter extends BasePresenter implements ICreateWarehouseEstatePresenter {
    private static final String TAG = LogUtils.makeLogTag(CreateWarehouseEstatePresenter.class);

    private WarehouseEstate mWarehouseEstate = null;

    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        mWarehouseEstate = new WarehouseEstate();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public ICreateWarehouseEstateView getView() {
        return (ICreateWarehouseEstateView) getIView();
    }

    @Override
    public void createWarehouseEstate() {
        getView().showLoading();

        String name = getView().getName();
        String description = getView().getDescription();

        if (!TextUtils.isEmpty(name)) {
            mWarehouseEstate.name = name;
        } else {
            getView().hideLoading();
            getView().createWarehouseEstateFailed();
            return;
        }

        if (!TextUtils.isEmpty(description))
            mWarehouseEstate.desc = description;

        LogUtils.LOGI(TAG, "Warehouse Create : " + new Gson().toJson(mWarehouseEstate));

        getApiManager().createWarehouseEstate(mWarehouseEstate, new ApiCallback<CreateWarehouseEstateResponse>() {
            @Override
            public void success(CreateWarehouseEstateResponse res) {
                LogUtils.LOGI(TAG, "create new warehouseEstate success " + new Gson().toJson(res.warehouseEstate));
                getView().hideLoading();
                getView().createWarehouseEstateSuccess();
            }

            @Override
            public void failure(RestError error) {
                LogUtils.LOGE(TAG, "create new warehouseEstate failed " + error.message);
                getView().hideLoading();
                getView().createWarehouseEstateFailed();
            }
        });

    }

    @Override
    public void uploadFile(MediaType type, final File file) {
        getView().showLoading();
        final UploadFileRequest request = new UploadFileRequest(type, file);
        RequestBody requestBody = RequestBody.create(MultipartBody.FORM, "0");
        getApiManager().uploadFile(request.file, requestBody, new ApiCallback<UploadFileResponse>() {
            @Override
            public void success(UploadFileResponse res) {
                getView().hideLoading();
                LogUtils.LOGI(TAG, "upload file success " + file.toString());

                mWarehouseEstate.avatar = res.tokenFile;
                String url = CommonUtils.getURL(res.tokenFile);
                getView().uploadFileSuccess(url);
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                LogUtils.LOGE(TAG, "upload file failed : " + file.toString());
                getView().uploadFileFailed();
            }
        });
    }
}
