package com.horical.gito.mvp.salary.list.fragments.subMetricGroup.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

public interface IListMetricGroupFragmentView extends IView {
    void requestGetAllMetricGroupSuccess();

    void requestGetAllMetricGroupError(RestError error);

    void requestDeleteMetricGroupSuccess();

    void requestDeleteMetricGroupError(RestError error);

    void showLoading();

    void hideLoading();
}
