package com.horical.gito.mvp.myPage.injection.component;


import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myPage.injection.module.MyPageModule;
import com.horical.gito.mvp.myPage.view.MyPageActivity;
//import com.horical.gito.mvp.todolist.detail.injection.module.TodoListDetailModule;
//import com.horical.gito.mvp.todolist.detail.view.TodoListDetailActivity;

import dagger.Component;

/**
 * Created by thanhle on 2/26/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = MyPageModule.class)
public interface MyPageComponent {
    void inject(MyPageActivity activity);
}

