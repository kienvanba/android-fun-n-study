package com.horical.gito.interactor.api.request.caseStudy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by thanhle on 4/5/17.
 */

public class GetAllCaseStudyRequest {
    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("endDate")
    @Expose
    public Date endDate;

    @SerializedName("mine")
    @Expose
    public boolean mine;

    @SerializedName("approve")
    @Expose
    public boolean approve;

    @SerializedName("pending")
    @Expose
    public boolean pending;
}
