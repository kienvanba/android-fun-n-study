package com.horical.gito.model.wrapperModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.model.Room;

import java.io.Serializable;
import java.util.List;


public class WrapperListRoom implements Serializable {

    @SerializedName("myRooms")
    @Expose
    private List<Room> myRooms;

    public WrapperListRoom (List<Room> myRooms){
        this.myRooms = myRooms;
    }

    public List<Room> getMyRooms(){
        return  myRooms;
    }

    public void setMyRooms(List<Room> myRooms){
        this.myRooms = myRooms;
    }
}
