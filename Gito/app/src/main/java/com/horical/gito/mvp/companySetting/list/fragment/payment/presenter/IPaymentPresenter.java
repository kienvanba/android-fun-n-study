package com.horical.gito.mvp.companySetting.list.fragment.payment.presenter;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IPaymentPresenter {
    void getAllHistory();

    void getAllCard();
}
