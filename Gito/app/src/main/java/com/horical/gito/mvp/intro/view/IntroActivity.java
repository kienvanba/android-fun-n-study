package com.horical.gito.mvp.intro.view;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.model.Intro;
import com.horical.gito.mvp.forgotPassword.view.ForgotPasswordActivity;
import com.horical.gito.mvp.intro.injection.component.DaggerIntroComponent;
import com.horical.gito.mvp.intro.injection.component.IntroComponent;
import com.horical.gito.mvp.intro.injection.module.IntroModule;
import com.horical.gito.mvp.intro.presenter.IntroPresenter;
import com.horical.gito.mvp.login.view.LoginActivity;
import com.horical.gito.mvp.myPage.view.MyPageActivity;
import com.horical.gito.mvp.signUp.view.SignUpActivity;
import com.horical.gito.utils.XMLParsingData;
import com.horical.gito.widget.button.GitOButton;
import com.horical.gito.widget.textview.GitOTextView;
import com.horical.gito.widget.viewFlipper.IntroSlideChildView;
import com.horical.gito.widget.viewFlipper.IntroViewFlipper;
import com.horical.gito.widget.viewFlipper.ViewFlipperIndicator;

import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class IntroActivity extends BaseActivity implements IIntroView, View.OnClickListener {

    private static final String TAG = makeLogTag(IntroActivity.class);

    private static final long ANIM_VIEWPAGER_DELAY = 3000;

    @Bind(R.id.intro_view_flipper)
    IntroViewFlipper mViewFlipper;

    @Bind(R.id.intro_btn_login)
    GitOButton mBtnLogin;

    @Bind(R.id.intro_btn_sign_up)
    GitOButton mBtnSignUp;

    @Bind(R.id.intro_tv_forgot)
    GitOTextView mTvForgot;

    @Bind(R.id.detail_imv_estate_indicator)
    ViewFlipperIndicator mIndicator;

    @Inject
    IntroPresenter mPresenter;

    private Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);

        Window window = getWindow();
        window.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.transparent));
        }

        IntroComponent component = DaggerIntroComponent.builder()
                .introModule(new IntroModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
        initIntro();
    }

    protected void initData() {
        mBtnLogin.setBlueStyle();
        mBtnSignUp.setWhiteStyle();

        if (MainApplication.getAppComponent().getPreferManager().isLogin()) {
            Intent intent = new Intent(this, MyPageActivity.class);
            startActivity(intent);
            finish();
        }
    }

    protected void initListener() {
        mBtnLogin.setOnClickListener(this);
        mBtnSignUp.setOnClickListener(this);
        mTvForgot.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (handler != null) {
            handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.removeCallbacks(animateViewPager);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mBtnLogin.getId()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else if (v.getId() == mBtnSignUp.getId()) {
            Intent intent = new Intent(this, SignUpActivity.class);
            startActivity(intent);
            finish();
        } else if (v.getId() == mTvForgot.getId()) {
            Intent intent = new Intent(this, ForgotPasswordActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private Runnable animateViewPager = new Runnable() {
        public void run() {
            mViewFlipper.showNext();
            handler.removeCallbacks(animateViewPager);
            handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
        }
    };

    private void initIntro() {
        List<IntroSlideChildView> ls = createListIntroView();

        for (int i = 0; i < ls.size(); i++) {
            mViewFlipper.addView(ls.get(i), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
        mViewFlipper.setTouchCallback(new IntroViewFlipper.TouchCallback() {
            @Override
            public void onTouchDown() {
                handler.removeCallbacks(animateViewPager);
            }

            @Override
            public void onTouchUp() {
                handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
            }
        });

        mIndicator.setViewFlipper(mViewFlipper);
        mViewFlipper.setIndicator(mIndicator);
    }

    private List<IntroSlideChildView> createListIntroView() {
        List<IntroSlideChildView> result = new ArrayList<>();
        List<Intro> introData = null;
        try {
            introData = readerIntroData();
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
        if (introData != null && introData.size() > 0) {
            for (int i = 0; i < introData.size(); i++) {
                IntroSlideChildView view = new IntroSlideChildView(this);
                if (i == 0) {
                    view.setContent(introData.get(i).title, R.drawable.img_intro_welcome);
                } else if (i == 1) {
                    view.setContent(introData.get(i).title, R.drawable.img_intro_build_software);
                } else if (i == 2) {
                    view.setContent(introData.get(i).title, R.drawable.img_intro_document);
                }
                result.add(view);
            }
        }
        return result;
    }

    private List<Intro> readerIntroData() throws IOException, XmlPullParserException {
        XMLParsingData xmlParsing = new XMLParsingData();
        List<Intro> entries = new ArrayList<>();
        AssetManager assManager = getAssets();
        InputStream is = null;
        try {
            is = assManager.open(AppConstants.INTRO_FILE_DATA_XML_LOCALE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (is != null) {
            InputStream caInput = new BufferedInputStream(is);
            entries = xmlParsing.parse(caInput);
        }
        return entries;
    }

}
