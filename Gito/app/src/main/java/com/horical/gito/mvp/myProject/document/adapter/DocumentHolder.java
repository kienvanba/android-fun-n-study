package com.horical.gito.mvp.myProject.document.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DocumentHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.edit_name)
    TextView mTvName;
    @Bind(R.id.tv_size)
    TextView mTvSize;
    @Bind(R.id.tv_date)
    TextView mTvDate;
    @Bind(R.id.img_icon)
    ImageView mImIcon;

    public DocumentHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
