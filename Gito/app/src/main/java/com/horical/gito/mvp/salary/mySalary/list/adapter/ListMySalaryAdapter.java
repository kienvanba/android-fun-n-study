package com.horical.gito.mvp.salary.mySalary.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.salary.dto.mySalary.ListMySalaryDto;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListMySalaryAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<ListMySalaryDto> items;
    private ListMySalaryAdapterListener callback;

    public ListMySalaryAdapter(Context context, List<ListMySalaryDto> items, ListMySalaryAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_my_salary, parent, false);
        return new ListMySalaryHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListMySalaryDto mySalaryDto = items.get(position);
        ListMySalaryHolder listMySalaryHolder = (ListMySalaryHolder) holder;
        listMySalaryHolder.tvMonthYear.setText(mySalaryDto.getMonth() + "/" + mySalaryDto.getYear());
        listMySalaryHolder.tvTotal.setText(CommonUtils.amountFormatEnGB(mySalaryDto.getTotal(), "usd"));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ListMySalaryHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_month_year)
        TextView tvMonthYear;

        @Bind(R.id.tv_total)
        TextView tvTotal;

        ListMySalaryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onSelected(getAdapterPosition());
                }
            });
        }
    }

    public interface ListMySalaryAdapterListener {
        void onSelected(int position);
    }
}