package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by thanhle on 3/9/17
 */

public class RelatedTasks implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("taskId")
    @Expose
    private int taskId;

    @SerializedName("creatorId")
    @Expose
    private String creatorId;

    @SerializedName("ProjectId")
    @Expose
    private String projectId;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("version")
    @Expose
    private String version;

    @SerializedName("weight")
    @Expose
    private int weight;

    @SerializedName("comments")
    @Expose
    private List<Comments> comments;

    @SerializedName("logTimes")
    @Expose
    private List<LogTime> logTimes;

    @SerializedName("modifiedDate")
    @Expose
    private String modifiedDate;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("relatedTasks")
    @Expose
    private List<RelatedTasks> relatedTasks;

    @SerializedName("subTasks")
    @Expose
    private List<SubTasks> subTasks;

    @SerializedName("estimatedTime")
    @Expose
    private int estimatedTime;

    @SerializedName("spentTime")
    @Expose
    private int spentTime;

    @SerializedName("endDate")
    @Expose
    private String endDate;

    @SerializedName("startDate")
    @Expose
    private String startDate;

    @SerializedName("watchers")
    @Expose
    private List<Watchers> watchers;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("priority")
    @Expose
    private int priority;

    @SerializedName("attachFiles")
    @Expose
    private List<TokenFile> attachFiles;

    @SerializedName("assigns")
    @Expose
    private List<User> assigns;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("taskType")
    @Expose
    private int taskType;

    @SerializedName("components")
    @Expose
    private List<Component> components;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public List<LogTime> getLogTimes() {
        return logTimes;
    }

    public void setLogTimes(List<LogTime> logTimes) {
        this.logTimes = logTimes;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public List<RelatedTasks> getRelatedTasks() {
        return relatedTasks;
    }

    public void setRelatedTasks(List<RelatedTasks> relatedTasks) {
        this.relatedTasks = relatedTasks;
    }

    public List<SubTasks> getSubTasks() {
        return subTasks;
    }

    public void setSubTasks(List<SubTasks> subTasks) {
        this.subTasks = subTasks;
    }

    public int getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(int estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public int getSpentTime() {
        return spentTime;
    }

    public void setSpentTime(int spentTime) {
        this.spentTime = spentTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public List<Watchers> getWatchers() {
        return watchers;
    }

    public void setWatchers(List<Watchers> watchers) {
        this.watchers = watchers;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public List<TokenFile> getAttachFiles() {
        return attachFiles;
    }

    public void setAttachFiles(List<TokenFile> attachFiles) {
        this.attachFiles = attachFiles;
    }

    public List<User> getAssigns() {
        return assigns;
    }

    public void setAssigns(List<User> assigns) {
        this.assigns = assigns;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getTaskType() {
        return taskType;
    }

    public void setTaskType(int taskType) {
        this.taskType = taskType;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }
}
