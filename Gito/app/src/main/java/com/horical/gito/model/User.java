package com.horical.gito.model;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("displayName")
    @Expose
    private String displayName;

    @SerializedName("userId")
    @Expose
    private String userId;

    @SerializedName("companyId")
    @Expose
    private Company companyId;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("gitId")
    @Expose
    private int gitId;

    @SerializedName("avatar")
    @Expose
    private TokenFile avatar;

    @SerializedName("level")
    @Expose
    private Level level;

    @SerializedName("roleCompany")
    @Expose
    private RoleCompany roleCompany;

    @SerializedName("department")
    @Expose
    private Department department;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("isAdmin")
    @Expose
    private boolean isAdmin;

    @SerializedName("isAgent")
    @Expose
    private boolean isAgent;

    @SerializedName("createdDate")
    @Expose
    private Date createdDate;

    @SerializedName("language")
    @Expose
    private String language;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("phone")
    @Expose
    private String phone;

    @Expose(serialize = false, deserialize = false)
    private int amount;

    private boolean isSelected;

    private boolean needApprove;

    private boolean isChecked;

    private int totalLogTime;

    public User() {
    }

    public User(String userId, String displayName, RoleCompany roleCompany) {
        this.userId = userId;
        this.displayName = displayName;
        this.roleCompany = roleCompany;
    }

    public int getTotalLogTime() {
        return totalLogTime;
    }

    public void setTotalLogTime(int totalLogTime) {
        this.totalLogTime = totalLogTime;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isNeedApprove() {
        return needApprove;
    }

    public void setNeedApprove(boolean needApprove) {
        this.needApprove = needApprove;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDisplayName() {
        if (displayName == null) {
            displayName = "";
        }
        return displayName;
    }

    public String getDisplayPhotoName() {
        if (TextUtils.isEmpty(displayName)) {
            return "";
        } else {
            try {
                String CurrentString = displayName.trim();
                String[] separated = CurrentString.split(" ");
                if (separated.length == 1) {
                    return separated[0].substring(0, 2).toUpperCase();
                } else {
                    return separated[0].substring(0, 1).toUpperCase() + separated[1].substring(0, 1).toUpperCase();
                }
            } catch (Exception e) {
                return displayName.substring(0, 1).toUpperCase();
            }

        }
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Company getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Company companyId) {
        this.companyId = companyId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getGitId() {
        return gitId;
    }

    public void setGitId(int gitId) {
        this.gitId = gitId;
    }

    public TokenFile getAvatar() {
        if (avatar == null)
            avatar = new TokenFile();
        return avatar;
    }

    public void setAvatar(TokenFile avatar) {
        this.avatar = avatar;
    }

    public Level getLevel() {
        if (level == null) level = new Level();
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public RoleCompany getRoleCompany() {
        if (roleCompany == null)
            roleCompany = new RoleCompany();
        return roleCompany;
    }

    public void setRoleCompany(RoleCompany roleCompany) {
        this.roleCompany = roleCompany;
    }

    public Department getDepartment() {
        if (department == null) {
            department = new Department();
        }
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isAgent() {
        return isAgent;
    }

    public void setAgent(boolean agent) {
        isAgent = agent;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
