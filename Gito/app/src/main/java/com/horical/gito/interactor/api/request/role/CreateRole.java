package com.horical.gito.interactor.api.request.role;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tran on 9/13/2016.
 */
public class CreateRole {

    @SerializedName("companyId")
    @Expose
    public String companyId;

    @SerializedName("roleId")
    @Expose
    public String roleId;

    @SerializedName("roleName")
    @Expose
    public String roleName;

    @SerializedName("listPermissions")
    @Expose
    public String listPermissions;
}
