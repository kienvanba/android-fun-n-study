package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.R;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Tin on 01-Dec-16.
 */

public class CommitDetailAdapter extends RecyclerView.Adapter<CommitDetailAdapter.CommitDetailHolder> {
    private Context mContext;
    private List<String> mList;
    private OnItemClickListener mOnItemClickListener;

    public CommitDetailAdapter(Context context, List<String> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public CommitDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_code_browse, parent, false);
        return new CommitDetailHolder(view);
    }

    @Override
    public void onBindViewHolder(CommitDetailHolder holder, final int position) {

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class CommitDetailHolder extends RecyclerView.ViewHolder {

        View view;

        public CommitDetailHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener =  listener;
    }
    public interface OnItemClickListener {
        void onItemClickSelected(int position);
    }
}
