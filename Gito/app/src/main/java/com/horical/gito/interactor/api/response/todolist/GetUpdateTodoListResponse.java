package com.horical.gito.interactor.api.response.todolist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.TodoList;

/**
 * Created by Luong on 20-Mar-17.
 */

public class GetUpdateTodoListResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    public TodoList todoList;
}