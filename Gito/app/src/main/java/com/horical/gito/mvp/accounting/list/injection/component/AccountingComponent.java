package com.horical.gito.mvp.accounting.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.accounting.list.injection.module.AccountingModule;
import com.horical.gito.mvp.accounting.list.view.AccountingActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = AccountingModule.class)
public interface AccountingComponent {
    void inject(AccountingActivity accountingActivity);
}
