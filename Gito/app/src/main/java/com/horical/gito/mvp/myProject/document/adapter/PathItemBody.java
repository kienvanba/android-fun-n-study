package com.horical.gito.mvp.myProject.document.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;

public class PathItemBody implements IRecyclerItem {
    public String path;
    public String linkFile;

    public PathItemBody(String linkFile, String path){
        this.path = path;
        this.linkFile = linkFile;
    }
    @Override
    public int getItemViewType() {
        return AppConstants.PATH_ITEM;
    }
}
