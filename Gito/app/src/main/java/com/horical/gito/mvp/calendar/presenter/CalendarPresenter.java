package com.horical.gito.mvp.calendar.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.model.CaseStudy;
import com.horical.gito.mvp.calendar.view.ICalendarView;
import com.horical.gito.mvp.caseStudy.list.view.ICaseStudyView;

import java.util.List;

public class CalendarPresenter extends BasePresenter implements ICalendarPresenter {

    private int from;

    public ICalendarView getView() {
        return (ICalendarView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }
}
