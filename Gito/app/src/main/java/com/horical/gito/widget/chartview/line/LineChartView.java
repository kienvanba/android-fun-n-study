package com.horical.gito.widget.chartview.line;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;

import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.horical.gito.R;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.utils.LogUtils;
import com.horical.gito.widget.chartview.ChartView;

import java.util.Date;

public class LineChartView extends ChartView {

    public static final String TAG = LogUtils.makeLogTag(LineChartView.class);

    /**
     * Date - Start Project
     */
    protected Date mDateStart;

    /**
     * Date - End Project (Due)
     */
    protected Date mDateEnd;

    /**
     * Total Task in Version
     */
    protected int totalTask;

    /**
     * Resolved Task in Version
     */
    protected int resolvedTask;

    /**
     * Date - Current Day
     */
    protected Date mToday;

    /**
     * Date - mTextSize
     */
    protected float mTextSize;

    /**
     * Status: Invisible - Visible
     */
    protected String mLineType;

    protected float mHeightBarTop;

    protected float mHeightBarBot;

    /**
     * Invisible status
     */
    public static String LINE_ONLY = "line_only";

    /**
     * Visible status
     */
    public static String LINE_DATE = "line_date";

    @Override
    public void setOnLongClickListener(OnLongClickListener l) {
        super.setOnLongClickListener(l);
    }

    public LineChartView(Context context) {
        super(context);
    }

    public LineChartView(Context context, AttributeSet attrs) {
        super(context, attrs);

        /** Get the attributes specified in attrs.xml using the name we included */
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.LineChartView, 0, 0);
        try {
            /** Get the text and colors specified using the names in attrs.xml */
            mLineType = typedArray.getString(R.styleable.LineChartView_lineType);
            mTextSize = typedArray.getDimension(R.styleable.LineChartView_textSize, 0);
            mToday = new Date();
        } finally {
            typedArray.recycle();
        }
    }

    public void setLineType(String mLineType) {
        this.mLineType = mLineType;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mHeightBarTop = mTextSize + convertDp(5);
        mHeightBarBot = getHeight() - convertDp(10) - mTextSize;

        /** Draw background divider **/
        onDrawBackground(canvas);

        /** Draw String Data **/
        if (mLineType.equalsIgnoreCase(LINE_DATE)) onDrawData(canvas);
        else {
            onDrawForegroundPercent(canvas, getWidth());
        }
    }

    protected void onDrawBackground(Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.grey));
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.FILL);

        canvas.drawRect(
                0,
                mHeightBarTop,
                getWidth(),
                mHeightBarBot,
                mPaint);
    }

    protected void onDrawForegroundPercent(Canvas canvas, float widthParent) {
        mPaint.setColor(getResources().getColor(R.color.app_color));
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.FILL);

        float dXDrawProgress = mPercent * widthParent / 100;

        canvas.drawRect(
                0,
                mHeightBarTop,
                dXDrawProgress,
                mHeightBarBot,
                mPaint
        );

        if (mLineType.equalsIgnoreCase(LINE_DATE)) {
            mPaint.setTextSize(mTextSize);
            mPaint.setColor(getResources().getColor(R.color.white));
            mPaint.setTextAlign(Paint.Align.CENTER);
            canvas.drawText(
                    (int) mPercent + "%",
                    dXDrawProgress == 0 ? getWidth() / 2 : dXDrawProgress / 2,
                    (mHeightBarBot + mHeightBarTop) / 2 + mTextSize / 3,
                    mPaint
            );
        }

        // Draw total hours
        if (dXDrawProgress == 0) dXDrawProgress = getWidth() / 2;

        String process = resolvedTask + "/" + totalTask;
        if (getWidth() - dXDrawProgress < process.length() * mTextSize / 3)
            mPaint.setTextAlign(Paint.Align.RIGHT);
        else
            mPaint.setTextAlign(Paint.Align.CENTER);

        canvas.drawText(process,
                dXDrawProgress,
                mTextSize,
                mPaint);
    }

    protected void onDrawData(Canvas canvas) {
        mPaint.setTextSize(mTextSize);

        float dXDrawToday;
        float dXDrawEnd;

        // Draw date start
        mPaint.setColor(getResources().getColor(R.color.black));
        mPaint.setTextAlign(Paint.Align.LEFT);
        canvas.drawText(
                DateUtils.formatDate(mDateStart),
                0,
                getHeight() - convertDp(5),
                mPaint);

        canvas.drawText("Start",
                0,
                mTextSize,
                mPaint);

        long diffStart;
        long diffEnd = mDateEnd.getTime() - mToday.getTime();

        if (diffEnd < 0) {
            diffStart = mToday.getTime() - mDateStart.getTime();
            dXDrawToday = getWidth();
            dXDrawEnd = (mDateEnd.getTime() - mDateStart.getTime()) * dXDrawToday / diffStart;
        } else if (diffEnd > 0) {
            diffStart = mDateEnd.getTime() - mDateStart.getTime();
            dXDrawEnd = getWidth();
            dXDrawToday = (mToday.getTime() - mDateStart.getTime()) * dXDrawEnd / diffStart;
        } else {
            dXDrawToday = getWidth();
            dXDrawEnd = getWidth();
        }

        if (dXDrawEnd < convertDp(120)) dXDrawEnd = convertDp(120);

        /** Draw percent progress **/
        onDrawForegroundPercent(canvas, dXDrawEnd);

        // Draw date end
        mPaint.setColor(getResources().getColor(R.color.white));
        canvas.drawLine(dXDrawEnd,
                mHeightBarTop - convertDp(3),
                dXDrawEnd,
                mHeightBarBot + convertDp(3),
                mPaint);

        String dateEnd = DateUtils.formatDate(mDateEnd);
        if (getWidth() - dXDrawEnd < dateEnd.length() * mTextSize / 3)
            mPaint.setTextAlign(Paint.Align.RIGHT);
        else
            mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setColor(getResources().getColor(R.color.black));
        canvas.drawText(dateEnd,
                dXDrawEnd,
                getHeight() - convertDp(5),
                mPaint);

        canvas.drawText("End",
                dXDrawEnd,
                mTextSize,
                mPaint);

        // Draw pin today
        mPaint.setColor(getResources().getColor(R.color.red));
        canvas.drawLine(dXDrawToday,
                mHeightBarTop - convertDp(3),
                dXDrawToday,
                mHeightBarBot + convertDp(3),
                mPaint);

        // Draw hours count util today
        String today = getResources().getString(R.string.today);
        if (getWidth() - dXDrawToday < today.length() * mTextSize / 2)
            mPaint.setTextAlign(Paint.Align.RIGHT);
        else
            mPaint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(today,
                dXDrawToday,
                Math.abs(dXDrawToday - dXDrawEnd) < convertDp(100) ? mTextSize : getHeight() - convertDp(5),
                mPaint);
    }

    public void setTotalTask(int totalTask) {
        this.totalTask = totalTask;
    }

    public void setResolvedTask(int resolvedTask) {
        this.resolvedTask = resolvedTask;
    }

    public void setDateStart(Date mDateStart) {
        this.mDateStart = mDateStart;
    }

    public void setDateEnd(Date mDateEnd) {
        this.mDateEnd = mDateEnd;
    }
}
