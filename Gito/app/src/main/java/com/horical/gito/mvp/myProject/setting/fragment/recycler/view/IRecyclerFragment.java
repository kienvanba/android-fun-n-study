package com.horical.gito.mvp.myProject.setting.fragment.recycler.view;

import com.horical.gito.base.IView;

public interface IRecyclerFragment extends IView {
    void getAllRecyclerSuccess();
    void deleteItemSuccess(int position);
    void showLoading();
    void hideLoading();
    void showToast(String mess);
}
