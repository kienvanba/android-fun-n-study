package com.horical.gito.widget.viewFlipper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

import com.horical.gito.R;

public class ViewFlipperIndicator extends LinearLayout implements Animation.AnimationListener {

    private ViewFlipper mFlipper;
    private LayoutInflater mLayoutInflater;
    private int mSelectedIndex;

    public ViewFlipperIndicator(Context context) {
        super(context);
    }

    public ViewFlipperIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ViewFlipperIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setViewFlipper(ViewFlipper viewFlipper) {
        if (viewFlipper == null) {
            return;
        }
        mFlipper = viewFlipper;
        int count = viewFlipper.getChildCount();
        for (int i = 0; i < count; i++) {
            View indicator = createIndicator();
            addView(indicator);
        }
        mSelectedIndex = mFlipper.getDisplayedChild();
        getChildAt(mSelectedIndex).setSelected(true);
    }

    public void setViewFlipper(int count) {
        for (int i = 0; i < count; i++) {
            View indicator = createIndicator();
            addView(indicator);
        }
        mSelectedIndex = mFlipper.getDisplayedChild();
        getChildAt(mSelectedIndex).setSelected(true);
    }

    private View createIndicator() {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(getContext());
        }
        return mLayoutInflater.inflate(R.layout.view_flipper_indicator, this, false);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        getChildAt(mSelectedIndex).setSelected(false);
        mSelectedIndex = mFlipper.getDisplayedChild();
        getChildAt(mSelectedIndex).setSelected(true);
    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
