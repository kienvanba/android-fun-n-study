package com.horical.gito.mvp.report.newReport.adapter;


import java.util.ArrayList;
import java.util.List;

public class UserDto {
    private String title;
    private List<Object> list;

    public UserDto(String title, List<Object> list) {
        this.title = title;
        this.list = list;
    }

    public String getTitle() {
        return " " + title + "(" + getList().size() + ")";
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Object> getList() {
        if (list == null) {
            list = new ArrayList<>();
        }
        return list;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }
}
