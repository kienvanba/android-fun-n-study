package com.horical.gito.utils;

import android.content.Context;
import android.util.DisplayMetrics;

public class ConvertDpPx {

    Context mContext;

    public ConvertDpPx(Context context) {
        mContext = context;
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public float pxToDp(int px) {
        float displayMetrics = mContext.getResources().getDisplayMetrics().density;

        return px * displayMetrics + 0.5f;
    }
}
