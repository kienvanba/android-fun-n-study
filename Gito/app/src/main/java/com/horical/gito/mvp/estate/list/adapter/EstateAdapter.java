package com.horical.gito.mvp.estate.list.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.caches.images.ImageLoaderCallback;
import com.horical.gito.model.estate.Estate;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by hoangsang on 11/8/16.
 */

public class EstateAdapter extends BaseRecyclerAdapter {

    public EstateAdapter(List<IRecyclerItem> items, Context context) {
        super(items, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(mContext).inflate(R.layout.item_estate, parent, false);
        return new EstateHolder(item);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        final EstateHolder mHolder = (EstateHolder) holder;
        EstateItem estateItem = (EstateItem) mItems.get(holder.getAdapterPosition());

        if (estateItem.estate != null) {

            final Estate estate = estateItem.estate;

            if (estate.images != null && !estate.images.isEmpty()) {
                String url = CommonUtils.getURL(estate.images.get(0));

                ImageLoader.load(mContext, url, new ImageLoaderCallback() {
                    @Override
                    public void onCompleted(Bitmap bm) {
                        if (bm != null) {
                            mHolder.imvEstate.setImageBitmap(bm);
                        }
                    }

                    @Override
                    public void onFailed(Exception e) {

                    }
                });
            }

            String nameText = ": " + estateItem.estate.name;
            mHolder.tvNameEstate.setText(nameText);
            String quantityText = ": " + String.valueOf(estateItem.estate.quanlity);
            mHolder.tvQuantityEstate.setText(quantityText);
        }

        mHolder.mImvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onDelete(mHolder.getAdapterPosition());
            }
        });
    }

    public interface OnClick {
        void onDelete(int position);
    }

    private OnClick mListener;

    public void setListener(OnClick mListener) {
        this.mListener = mListener;
    }

    public class EstateHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imv_estate)
        ImageView imvEstate;

        @Bind(R.id.pgbar_imv)
        ProgressBar pgbarImv;

        @Bind(R.id.tv_name_estate)
        GitOTextView tvNameEstate;

        @Bind(R.id.tv_quantity_estate)
        GitOTextView tvQuantityEstate;

        @Bind(R.id.imv_delete)
        ImageView mImvDelete;

        public void showLoading() {
            imvEstate.setVisibility(View.GONE);
            pgbarImv.setVisibility(View.VISIBLE);
        }

        public void hideLoading() {
            imvEstate.setVisibility(View.VISIBLE);
            pgbarImv.setVisibility(View.GONE);
        }

        public EstateHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
