package com.horical.gito.mvp.caseStudy.list.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.model.CaseStudy;
import com.horical.gito.mvp.caseStudy.detail.view.CaseStudyDetailActivity;
import com.horical.gito.mvp.caseStudy.list.adapter.CaseStudyAdapter;
import com.horical.gito.mvp.caseStudy.list.injection.component.CaseStudyComponent;
import com.horical.gito.mvp.caseStudy.list.injection.component.DaggerCaseStudyComponent;
import com.horical.gito.mvp.caseStudy.list.injection.module.CaseStudyModule;
import com.horical.gito.mvp.caseStudy.list.presenter.CaseStudyPresenter;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.popup.dateSelected.DateDto;
import com.horical.gito.mvp.popup.dateSelected.SelectDatePopup;
import com.horical.gito.widget.edittext.GitOEditText;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CaseStudyActivity extends DrawerActivity implements ICaseStudyView, View.OnClickListener {

    public final static int REQUEST_CODE_ADD_EDIT_CASESTUDY = 1;

    public final static int MINE = 0;
    public final static int APPROVED = 1;
    public final static int PENDING = 2;

    public final static int NEW = 0;
    public final static int REJECTED = 2;
    public final static int SUBMITED = 3;

    @Bind(R.id.ll_list_menu)
    LinearLayout llListMenu;

    @Bind(R.id.tv_title_case_study)
    GitOTextView tvTitle;

    @Bind(R.id.img_search_case_study)
    ImageView imgSearch;

    @Bind(R.id.img_add_case_study)
    ImageView imgAdd;

    @Bind(R.id.ll_search_case_study)
    LinearLayout llSearch;

    @Bind(R.id.edt_search_case_study)
    GitOEditText edtSearch;

    @Bind(R.id.img_cancel_search_case_study)
    ImageView imgCancelSearch;

    @Bind(R.id.tv_approved_casestudy)
    TextView tvApproved;

    @Bind(R.id.tv_pending_casestudy)
    TextView tvPending;

    @Bind(R.id.tv_mine_casestudy)
    TextView tvMine;

    @Bind(R.id.date_filter)
    GitOTextView tvDateFilter;

    @Bind(R.id.refresh_case_study)
    SwipeRefreshLayout mRefresh;

    @Bind(R.id.rcv_case_study)
    RecyclerView rcvCaseStudy;

    private CaseStudyAdapter mAdapter;
    private int currentSelection;

    @Inject
    CaseStudyPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_case_study;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_CASE_STUDY;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        CaseStudyComponent component = DaggerCaseStudyComponent.builder()
                .caseStudyModule(new CaseStudyModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        currentSelection = APPROVED;
        tvApproved.setSelected(true);

        tvDateFilter.setText(mPresenter.getDateRange().getTypeDate());

        mAdapter = new CaseStudyAdapter(this, mPresenter.getListSearchCaseStudy(edtSearch.getText().toString()));
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvCaseStudy.setLayoutManager(llm);
        rcvCaseStudy.setAdapter(mAdapter);

        showLoading();
        mPresenter.getAllCaseStudy(currentSelection);
    }

    private void updateAdapterFromAllCaseStudy() {
        List<CaseStudy> caseStudies = mPresenter.getListSearchCaseStudy(edtSearch.getText().toString());
        mAdapter.setListCaseStudy(caseStudies);
        mAdapter.notifyDataSetChanged();
    }

    protected void initListener() {
        llListMenu.setOnClickListener(this);
        imgSearch.setOnClickListener(this);
        imgCancelSearch.setOnClickListener(this);
        imgAdd.setOnClickListener(this);
        tvApproved.setOnClickListener(this);
        tvMine.setOnClickListener(this);
        tvPending.setOnClickListener(this);
        tvDateFilter.setOnClickListener(this);
        mAdapter.setOnItemClickListener(new CaseStudyAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                Intent intent = new Intent(CaseStudyActivity.this, CaseStudyDetailActivity.class);
                intent.putExtra(CaseStudyDetailActivity.ARG_CASESTUDY, mAdapter.getListCaseStudy().get(position));
                startActivityForResult(intent, REQUEST_CODE_ADD_EDIT_CASESTUDY);
            }

            @Override
            public void onDelete(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CaseStudyActivity.this);
                builder.setMessage(R.string.are_you_sure_to_delete_this_case_study);
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                showDialog("Loading...");
                                mPresenter.deleteCaseStudy(mAdapter.getListCaseStudy().get(position).get_id(), position);
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                mPresenter.getAllCaseStudy(currentSelection);
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateAdapterFromAllCaseStudy();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_list_menu:
                openDrawer();
                break;
            case R.id.img_search_case_study:
                tvTitle.setVisibility(View.GONE);
                imgAdd.setVisibility(View.GONE);
                imgSearch.setVisibility(View.GONE);
                llSearch.setVisibility(View.VISIBLE);
                llSearch.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_from_right));
                break;
            case R.id.img_cancel_search_case_study:
                tvTitle.setVisibility(View.VISIBLE);
                imgAdd.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.VISIBLE);
                llSearch.setVisibility(View.GONE);
                llSearch.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_from_left));
                edtSearch.setText("");
                break;
            case R.id.img_add_case_study:
                Intent intent = new Intent(CaseStudyActivity.this, CaseStudyDetailActivity.class);
                startActivityForResult(intent, REQUEST_CODE_ADD_EDIT_CASESTUDY);
                break;
            case R.id.tv_approved_casestudy:
                tvApproved.setSelected(true);
                tvMine.setSelected(false);
                tvPending.setSelected(false);
                currentSelection = APPROVED;
                showLoading();
                mPresenter.getAllCaseStudy(currentSelection);
                break;
            case R.id.tv_mine_casestudy:
                tvApproved.setSelected(false);
                tvMine.setSelected(true);
                tvPending.setSelected(false);
                currentSelection = MINE;
                showLoading();
                mPresenter.getAllCaseStudy(currentSelection);
                break;
            case R.id.tv_pending_casestudy:
                tvApproved.setSelected(false);
                tvMine.setSelected(false);
                tvPending.setSelected(true);
                currentSelection = PENDING;
                showLoading();
                mPresenter.getAllCaseStudy(currentSelection);
                break;
            case R.id.date_filter:
                SelectDatePopup selectDateRangePopup = new SelectDatePopup(this, mPresenter.getDateRange(),
                        new SelectDatePopup.OnItemSelectedListener() {
                            @Override
                            public void onClickItemDialog(DateDto dateRange) {
                                mPresenter.setDateRange(dateRange);
                                tvDateFilter.setText(dateRange.getTypeDate());
                                showLoading();
                                mPresenter.getAllCaseStudy(currentSelection);
                            }
                        });
                selectDateRangePopup.showDateDefault(
                        tvDateFilter,
                        0,
                        0,
                        0);
                break;
        }
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllCaseStudySuccess() {
        mRefresh.setRefreshing(false);
        hideLoading();
        showToast("Success");
        updateAdapterFromAllCaseStudy();
    }

    @Override
    public void getAllCaseStudyFailure(String error) {
        mRefresh.setRefreshing(false);
        hideLoading();
        showToast(error);
        updateAdapterFromAllCaseStudy();
    }

    @Override
    public void deleteCaseStudySuccess(int position) {
        hideLoading();
        showToast("Success");
        mAdapter.getListCaseStudy().remove(position);
        mAdapter.notifyItemRemoved(position);
        mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());
    }

    @Override
    public void deleteCaseStudyFailure() {
        hideLoading();
        showToast("failure");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            showLoading();
            mPresenter.getAllCaseStudy(currentSelection);
        }
    }
}