package com.horical.gito.mvp.meeting.detail.selectRoom.presenter;

import com.horical.gito.model.Room;

import java.util.List;

/**
 * Created by Lemon on 4/13/2017.
 */

public interface IRoomPresenter{
    void getAllRoomBooking();
    List<Room> getListSearchRoom(String keyword);
}
