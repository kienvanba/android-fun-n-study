package com.horical.gito.mvp.survey.list.injection.component;

/**
 * Created by thanhle on 11/9/16.
 */
import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.survey.list.injection.module.SurveyModule;
import com.horical.gito.mvp.survey.list.view.SurveyActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = SurveyModule.class)
public interface SurveyComponent {
    void inject(SurveyActivity activity);
}