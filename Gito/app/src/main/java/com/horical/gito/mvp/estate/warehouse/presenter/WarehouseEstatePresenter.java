package com.horical.gito.mvp.estate.warehouse.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.estate.GetAllWarehourseEstateResponse;
import com.horical.gito.model.estate.WarehouseEstate;
import com.horical.gito.mvp.estate.warehouse.adapter.WarehouseEstateItem;
import com.horical.gito.mvp.estate.warehouse.view.IWarehouseEstateView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.LOGE;
import static com.horical.gito.utils.LogUtils.LOGI;
import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by hoangsang on 4/12/17.
 */

public class WarehouseEstatePresenter extends BasePresenter implements IWarehouseEstatePresener {
    private static final String TAG = makeLogTag(WarehouseEstatePresenter.class);

    private List<IRecyclerItem> mList;

    public List<IRecyclerItem> getList() {
        return mList;
    }

    public void setList(List<IRecyclerItem> mList) {
        this.mList = mList;
    }

    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public IWarehouseEstateView getIView() {
        return (IWarehouseEstateView) super.getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);

        getAllWarehouseEstate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void getAllWarehouseEstate() {
        if (!getIView().isRefreshing())
            getIView().showLoading();

        getApiManager().getAllWarehourseEstate(new ApiCallback<GetAllWarehourseEstateResponse>() {
            @Override
            public void success(GetAllWarehourseEstateResponse res) {
                LOGI(TAG, "get All Warehourse Estate success");

                mList.clear();
                mList.addAll(convertWarehourseEstateToItem(res.warehouseEstateList));

                getIView().loadAllWarehourseEstateSuccess();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, "get All Warehourse Estate failed");
                getIView().loadAllWarehourseEstateFailed();
            }
        });
    }

    private List<IRecyclerItem> convertWarehourseEstateToItem(List<WarehouseEstate> warehouseEstates) {
        List<IRecyclerItem> items = new ArrayList<>();

        for (WarehouseEstate warehourse : warehouseEstates) {
            IRecyclerItem item = new WarehouseEstateItem(warehourse);
            items.add(item);
        }

        return items;
    }

    @Override
    public void deleteWarehouseId(String warehouseId) {
        getIView().showLoading();

        getApiManager().deleteWarehouseEstate(warehouseId, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                LOGI(TAG, "delete warehouse estate success");
                getIView().hideLoading();
                getIView().deleteWarehouseEstateSuccess();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, "delete warehouse estate failed");
                getIView().hideLoading();
                getIView().deleteWarehouseEstateFailed();
            }
        });
    }
}
