package com.horical.gito.mvp.salary.dto.bonus;

import java.io.Serializable;
import java.util.Date;

public class BonusListDto implements Serializable {
    private String id;
    private String title;
    private String status;
    private int user;
    private String acronym;
    private Date dateFrom;
    private Date dateTo;

    public BonusListDto() {
    }

    public BonusListDto(String title, String status, int user, String acronym, Date dateFrom, Date dateTo) {
        this.title = title;
        this.status = status;
        this.user = user;
        this.acronym = acronym;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }
}
