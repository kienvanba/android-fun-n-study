package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.presenter.UserPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 21-Dec-16.
 */
@Module
public class UserModule {
    @PerFragment
    @Provides
    UserPresenter provideUserPresenter(){
        return new UserPresenter();
    }
}
