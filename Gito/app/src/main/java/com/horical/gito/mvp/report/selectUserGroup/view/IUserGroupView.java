package com.horical.gito.mvp.report.selectUserGroup.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 15-Dec-16.
 */

public interface IUserGroupView extends IView {

    void showLoading();

    void hideLoading();

    void onSuccessList();

    void onFailList(String error);

}
