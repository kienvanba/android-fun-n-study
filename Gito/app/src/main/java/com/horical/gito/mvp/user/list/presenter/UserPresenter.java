package com.horical.gito.mvp.user.list.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.user.GetAllUserResponse;
import com.horical.gito.model.User;
import com.horical.gito.mvp.user.list.view.IUserView;

import java.util.ArrayList;
import java.util.List;

public class UserPresenter extends BasePresenter implements IUserPresenter {

    private List<User> mList = new ArrayList<>();
    private List<User> searchUser = new ArrayList<>();

    private IUserView getView() {
        return (IUserView) getIView();
    }

    public List<User> getSearchUser() {
        return searchUser;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public void setListUser(List<User> list) {
        mList.clear();
        searchUser.addAll(list);
        mList.addAll(list);
    }


    public void setCheckAll(boolean check) {
        for (User item : searchUser) {
            item.setSelected(check);
        }
    }

    @Override
    public void getAllUser() {

        getView().showLoading();
        getApiManager().getAllUser(new ApiCallback<GetAllUserResponse>() {
            @Override
            public void success(GetAllUserResponse res) {
                mList.clear();
                mList.addAll(res.getUsers());
                getView().getAllUserSuccess();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                getView().getAllUserFailure(error.message);
                getView().hideLoading();
            }
        });
    }

    @Override
    public List<User> getListSearchUser(String keyword) {
        searchUser.clear();
        if (keyword != null && keyword.trim().length() > 0) {
            for (User myUser : mList) {
                if (myUser.getDisplayName() != null
                        && myUser.getDisplayName().toLowerCase().contains(keyword.toLowerCase())) {
                    searchUser.add(myUser);
                }
            }
        } else {
            searchUser.addAll(mList);
        }
        return searchUser;
    }

}
