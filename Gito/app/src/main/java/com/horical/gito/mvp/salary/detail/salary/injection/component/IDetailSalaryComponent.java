package com.horical.gito.mvp.salary.detail.salary.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.detail.salary.injection.module.DetailSalaryModule;
import com.horical.gito.mvp.salary.detail.salary.view.DetailSalaryActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = DetailSalaryModule.class)
public interface IDetailSalaryComponent {
    void inject(DetailSalaryActivity activity);
}
