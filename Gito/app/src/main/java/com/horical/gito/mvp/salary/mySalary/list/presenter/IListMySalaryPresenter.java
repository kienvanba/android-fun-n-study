package com.horical.gito.mvp.salary.mySalary.list.presenter;

import com.horical.gito.mvp.salary.dto.mySalary.ListMySalaryDto;
import com.horical.gito.mvp.salary.dto.mySalary.MySalaryDto;

import java.util.List;

public interface IListMySalaryPresenter {
    List<ListMySalaryDto> getMySalaries();
}
