package com.horical.gito.mvp.myProject.summary.dto;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.Version;
import com.horical.gito.mvp.myProject.summary.adapter.SummaryAdapter;

/**
 * Created by nhonvt.dk on 3/21/17
 */

public class VersionItem implements IRecyclerItem {

    private Version version;

    public VersionItem(Version version) {
        this.version = version;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    @Override
    public int getItemViewType() {
        return SummaryAdapter.SUMMARY_VERSION_TYPE;
    }
}
