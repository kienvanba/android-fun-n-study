package com.horical.gito.mvp.report.presenter;

import android.os.Handler;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.report.MemberRequest;
import com.horical.gito.interactor.api.request.report.RejectApprovedRequest;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.report.AllReportResponse;
import com.horical.gito.interactor.api.response.report.MemberResponse;
import com.horical.gito.interactor.api.response.report.UpdateReportResponse;
import com.horical.gito.model.Reports;
import com.horical.gito.model.ResultsReport;
import com.horical.gito.mvp.popup.dateSelected.DateDto;
import com.horical.gito.mvp.report.objectDto.ContentDto;
import com.horical.gito.mvp.report.objectDto.ReportByContentDto;
import com.horical.gito.mvp.report.view.IReportView;
import com.horical.gito.mvp.report.view.ReportActivity;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

public class ReportPresenter extends BasePresenter implements IReportPresenter {

    public IReportView getView() {
        return (IReportView) getIView();
    }

    private List<ResultsReport> listMember = new ArrayList<>();

    private List<Reports> listByDate = new ArrayList<>();

    private List<ReportByContentDto> listByContent = new ArrayList<>();

    public List<ResultsReport> getListMember() {
        return listMember;
    }

    public List<Reports> getListByDate() {
        return listByDate;
    }

    public List<ReportByContentDto> getListByContent() {
        return listByContent;
    }

    private int from;

    private DateDto date;
    private Reports itemOld;

    public DateDto getDate() {
        if (date == null) {
            date = new DateDto(MainApplication.getAppComponent().getContext().getString(R.string.today)
                    , DateUtils.getTodayStart()
                    , DateUtils.getTodayEnd());
        }
        return date;
    }

    public void setDate(DateDto date) {
        this.date.setTypeDate(date.getTypeDate());
        this.date.setStart(date.getStart());
        this.date.setEnd(date.getEnd());
    }

    @Override
    public void getDataMember() {
        getView().showLoading();
        getApiManager().getMemberReport(new MemberRequest(getDate().getStart()
                        , getDate().getEnd()
                        , getPreferManager().getUser().get_id())
                , new ApiCallback<MemberResponse>() {
                    @Override
                    public void success(MemberResponse res) {
                        for (int i = 0; i < res.getResults().size(); i++) {
                            if (res.getResults().get(i).getDataUser().getId().equalsIgnoreCase(getPreferManager().getUser().get_id())) {
                                res.getResults().remove(i);
                                break;
                            }
                        }

                        listMember.clear();
                        listMember.addAll(res.getResults());

                        if (listMember.isEmpty()) {
                            getDataByDate(new ArrayList<Reports>());
                            getDateByContent(new ArrayList<Reports>());
                        } else {
                            listMember.get(0).setSelected(true);
                            getDataByDate(listMember.get(0).getReports());
                            getDateByContent(listMember.get(0).getReports());

                        }

                        getView().dismissLoading();
                        getView().onNotifyAdapterReport();
                    }

                    @Override
                    public void failure(RestError error) {
                        getView().dismissLoading();
                        getView().showError(error.message);
                    }
                });
    }

    @Override
    public void getMyReport() {
        getView().showLoading();
        getApiManager().getAllMemberReport(new ApiCallback<AllReportResponse>() {
            @Override
            public void success(AllReportResponse res) {
                for (int i = 0; i < listMember.size(); i++) {
                    if (listMember.get(i).isSelected()) {
                        listMember.get(i).setSelected(false);
                        getView().onNotifyAdapterReport();
                        break;
                    }
                }

                getDataByDate(res.getResults());
                getDateByContent(res.getResults());

                getView().dismissLoading();
            }

            @Override
            public void failure(RestError error) {
                getView().showError(error.message);
            }
        });
    }

    @Override
    public void getDataByDate(List<Reports> reports) {
        listByDate.clear();
        listByDate.addAll(reports);
        getView().onNotifyAdapterByDate();
    }

    @Override
    public void getDateByContent(List<Reports> reports) {
        listByContent.clear();

        List<ContentDto> listDoing = new ArrayList<>();
        List<ContentDto> listProblem = new ArrayList<>();
        List<ContentDto> listFinished = new ArrayList<>();
        List<ContentDto> listRejectMsg = new ArrayList<>();

        if (!reports.isEmpty()) {
            for (int i = 0; i < reports.size(); i++) {
                listDoing.add(new ContentDto(reports.get(i).getCreatedDate(), reports.get(i).getDoing()));
                listProblem.add(new ContentDto(reports.get(i).getCreatedDate(), reports.get(i).getProblem()));
                listFinished.add(new ContentDto(reports.get(i).getCreatedDate(), reports.get(i).getFinished()));

                if (reports.get(i).getRejectMsg() != null) {
                    listRejectMsg.add(new ContentDto(reports.get(i).getCreatedDate(), reports.get(i).getRejectMsg()));
                }
            }
        }

        if (!listDoing.isEmpty()) {
            listByContent.add(new ReportByContentDto("Doing", listDoing));
        }

        if (!listProblem.isEmpty()) {
            listByContent.add(new ReportByContentDto("Problem", listProblem));
        }

        if (!listFinished.isEmpty()) {
            listByContent.add(new ReportByContentDto("Finish", listFinished));
        }

        if (!listRejectMsg.isEmpty()) {
            listByContent.add(new ReportByContentDto("Reject", listRejectMsg));
        }
        getView().onNotifyAdapterByContent();
    }

    @Override
    public void setOnChooseMember(int position) {
        for (ResultsReport item : listMember) {
            item.setSelected(false);
        }
        listMember.get(position).setSelected(true);
        getDataByDate(listMember.get(position).getReports());
        getDateByContent(listMember.get(position).getReports());
        getView().onNotifyAdapterReport();

    }

    @Override
    public void setDataDoingForByDate(int position, String text) {
        itemOld = (Reports) CommonUtils.deepCopy(listByDate.get(position));
        listByDate.get(position).setDoing(text);
        listByDate.get(position).setStatus(ReportActivity.STATUS_NEW);
        upDateReport(position);
    }

    @Override
    public void setDataProblemForByDate(int position, String text) {
        itemOld = (Reports) CommonUtils.deepCopy(listByDate.get(position));
        listByDate.get(position).setProblem(text);
        listByDate.get(position).setStatus(ReportActivity.STATUS_NEW);
        upDateReport(position);
    }

    @Override
    public void setDataFinishForByDate(int position, String text) {
        itemOld = (Reports) CommonUtils.deepCopy(listByDate.get(position));
        listByDate.get(position).setFinished(text);
        listByDate.get(position).setStatus(ReportActivity.STATUS_NEW);
        upDateReport(position);
    }

    @Override
    public void setDataRejectForByDate(int position, String text) {
        itemOld = (Reports) CommonUtils.deepCopy(listByDate.get(position));
        listByDate.get(position).setRejectMsg(text);
        listByDate.get(position).setStatus(ReportActivity.STATUS_REJECTED);
        upDateReportStatus(position);
    }

    @Override
    public void setStatusSubmittedReportByDate(int position) {
        itemOld = (Reports) CommonUtils.deepCopy(listByDate.get(position));
        listByDate.get(position).setStatus(ReportActivity.STATUS_SUBMITTED);
        upDateReport(position);
    }


    @Override
    public void setStatusApproveReportByDate(int position) {
        itemOld = (Reports) CommonUtils.deepCopy(listByDate.get(position));
        listByDate.get(position).setStatus(ReportActivity.STATUS_APPROVED);
        upDateReportStatus(position);
    }

    @Override
    public void upDateReport(final int position) {
        getView().showLoading();
        getApiManager().updateReport(listByDate.get(position).getId(), listByDate.get(position), new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().dismissLoading();
                getView().onNotifyAdapterByDate();
                getDateByContent(getListByDate());

            }

            @Override
            public void failure(RestError error) {
                getView().showError(error.message);
                listByDate.set(position, itemOld);
            }
        });
    }

    @Override
    public void upDateReportStatus(final int position) {
        getView().showLoading();
        RejectApprovedRequest param = new RejectApprovedRequest(listByDate.get(position).getStatus(), listByDate.get(position).getRejectMsg());
        getApiManager().updateReportStatus(listByDate.get(position).getId(), param, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().dismissLoading();
                getView().onNotifyAdapterByDate();

                getDateByContent(getListByDate());

            }

            @Override
            public void failure(RestError error) {
                getView().showError(error.message);
                listByDate.set(position, itemOld);
            }
        });
    }

    public boolean isCheckPermission() {
        for (int i = 0; i < listMember.size(); i++) {
            if (listMember.get(i).isSelected()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }
}
