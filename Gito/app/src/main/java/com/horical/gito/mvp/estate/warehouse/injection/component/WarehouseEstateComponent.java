package com.horical.gito.mvp.estate.warehouse.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.estate.warehouse.injection.module.WarehouseEstateModule;
import com.horical.gito.mvp.estate.warehouse.view.WarehouseEstateActivity;

import dagger.Component;

/**
 * Created by hoangsang on 4/12/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = WarehouseEstateModule.class)
public interface WarehouseEstateComponent {
    void inject(WarehouseEstateActivity activity);
}
