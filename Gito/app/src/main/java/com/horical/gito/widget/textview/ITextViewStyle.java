package com.horical.gito.widget.textview;

/**
 * Created by NXON on 10/28/16.
 */

public interface ITextViewStyle {

    // == Circle TextView == //
    void setCircleStyle();

    // == TextView with 14sp and bold == //
    void setTypeText(int size, int type);

    void setCustomTextColor(int color);
}
