package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.presenter.BrowsePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */
@Module
public class BrowseModule {
    @PerFragment
    @Provides
    BrowsePresenter provideBrowsePresenter() {
        return new BrowsePresenter();
    }
}
