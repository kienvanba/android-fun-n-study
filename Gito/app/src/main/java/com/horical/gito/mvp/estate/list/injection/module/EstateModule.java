package com.horical.gito.mvp.estate.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.estate.list.presenter.EstatePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hoangsang on 11/8/16.
 */

@Module
public class EstateModule {

    @PerActivity
    @Provides
    EstatePresenter provideEstatePresenter(){
        return new EstatePresenter();
    }
}
