package com.horical.gito.mvp.companySetting.list.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.view.NewCompanyRoleActivity;
import com.horical.gito.mvp.companySetting.detail.newDepartment.view.NewDepartmentActivity;
import com.horical.gito.mvp.companySetting.detail.newLevel.view.NewLevelActivity;
import com.horical.gito.mvp.companySetting.detail.newMember.view.NewMemberActivity;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.view.NewProjectRoleActivity;
import com.horical.gito.mvp.companySetting.list.adapter.CompanySettingAdapter;
import com.horical.gito.mvp.companySetting.list.adapter.CompanySettingViewPagerAdapter;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.companyrole.view.CompanyRoleFragment;
import com.horical.gito.mvp.companySetting.list.fragment.department.view.DepartmentFragment;
import com.horical.gito.mvp.companySetting.list.fragment.devices.view.DevicesFragment;
import com.horical.gito.mvp.companySetting.list.fragment.feature.view.FeatureFragment;
import com.horical.gito.mvp.companySetting.list.fragment.info.view.InfoFragment;
import com.horical.gito.mvp.companySetting.list.fragment.level.view.LevelFragment;
import com.horical.gito.mvp.companySetting.list.fragment.members.view.MembersFragment;
import com.horical.gito.mvp.companySetting.list.fragment.payment.view.PaymentFragment;
import com.horical.gito.mvp.companySetting.list.fragment.projectrole.view.ProjectRoleFragment;
import com.horical.gito.mvp.companySetting.list.fragment.quota.view.QuotaFragment;
import com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.view.WorkingAndHolidayFragment;
import com.horical.gito.mvp.companySetting.list.injection.component.CompanySettingComponent;
import com.horical.gito.mvp.companySetting.list.injection.component.DaggerCompanySettingComponent;
import com.horical.gito.mvp.companySetting.list.injection.module.CompanySettingModule;
import com.horical.gito.mvp.companySetting.list.presenter.CompanySettingPresenter;
import com.horical.gito.mvp.drawer.DrawerActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by thanhle on 3/13/17
 */

public class CompanySettingActivity extends DrawerActivity implements ICompanySettingView, View.OnClickListener {
    public final static int REQUEST_CODE_ADD_MEMBERS = 1;
    public final static int REQUEST_CODE_ADD_PROJECTROLE = 2;
    public final static int REQUEST_CODE_ADD_COMPANYROLE = 3;
    public final static int REQUEST_CODE_ADD_LEVEL = 4;
    public final static int REQUEST_CODE_ADD_DEPARTMENT = 5;

    public static final String INFO = "info";
    public static final String FEATURE = "feature";
    public static final String MEMBERS = "members";
    public static final String WORKINGHOLIDAY = "workingandholiday";
    public static final String QUOTA = "quota";
    public static final String PAYMENT = "payment";
    public static final String PROJECTROLE = "projectrole";
    public static final String COMPANYROLE = "companyrole";
    public static final String DEVICES = "devices";
    public static final String LEVEL = "level";
    public static final String DEPARTMENT = "department";
    private String current = INFO;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.view_pager_company_setting)
    ViewPager viewPager;

    @Bind(R.id.img_info)
    CircleImageView imgInfo;

    @Bind(R.id.img_feature)
    CircleImageView imgFeature;

    @Bind(R.id.img_members)
    CircleImageView imgMembers;

    @Bind(R.id.img_working_holiday)
    CircleImageView imgWorkingHoliday;

    @Bind(R.id.img_quota)
    CircleImageView imgQuota;

    @Bind(R.id.img_payment)
    CircleImageView imgPayment;

    @Bind(R.id.img_project_role)
    CircleImageView imgProjectRole;

    @Bind(R.id.img_company_role)
    CircleImageView imgCompanyRole;

    @Bind(R.id.img_devices)
    CircleImageView imgDevices;

    @Bind(R.id.img_level)
    CircleImageView imgLevel;

    @Bind(R.id.img_department)
    CircleImageView imgDepartment;

    @Bind(R.id.list_item)
    RecyclerView rcvItem;

    private List<BaseCompanyFragment> mFragments;
    private CompanySettingViewPagerAdapter adapterViewPager;
    private CompanySettingAdapter adapterCompanySetting;
    private Intent intent;

    @Inject
    CompanySettingPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_company_setting;
    }

    @Override
    protected int getNavId() {
        return 0;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        CompanySettingComponent component = DaggerCompanySettingComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .companySettingModule(new CompanySettingModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    public void initFragments() {
        mFragments = new ArrayList<>();
        mFragments.add(InfoFragment.newInstance());
        mFragments.add(FeatureFragment.newInstance());
        mFragments.add(MembersFragment.newInstance());
        mFragments.add(WorkingAndHolidayFragment.newInstance());
        QuotaFragment quotaFragment = QuotaFragment.newInstance();
        quotaFragment.setOnClickListener(new QuotaFragment.OnClickListener() {
            @Override
            public void onClick() {
                adapterCompanySetting.setSelectedIndex(2);
                adapterCompanySetting.notifyDataSetChanged();
                viewPager.setCurrentItem(2);
                imgMembers.setImageResource(R.color.blue);
                imgQuota.setImageResource(R.color.grey);
                topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);
                current = MEMBERS;
            }
        });
        mFragments.add(quotaFragment);
        mFragments.add(PaymentFragment.newInstance());
        mFragments.add(ProjectRoleFragment.newInstance());
        mFragments.add(CompanyRoleFragment.newInstance());
        mFragments.add(DevicesFragment.newInstance());
        mFragments.add(LevelFragment.newInstance());
        mFragments.add(DepartmentFragment.newInstance());
    }

    protected void initData() {
        initFragments();
        topBar.setTextTitle(getString(R.string.company_setting));
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);

        adapterCompanySetting = new CompanySettingAdapter(CompanySettingActivity.this, mPresenter.getListData());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rcvItem.setLayoutManager(layoutManager);
        rcvItem.setHasFixedSize(false);
        rcvItem.setAdapter(adapterCompanySetting);

        imgInfo.setImageResource(R.color.blue);
        adapterViewPager = new CompanySettingViewPagerAdapter(getSupportFragmentManager(), mFragments);
        viewPager.setAdapter(adapterViewPager);
    }

    protected void initListener() {
        
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                openDrawer();
            }

            @Override
            public void onRightButtonOneClicked() {
                if (current.equalsIgnoreCase(MEMBERS)) {
                    intent = new Intent(CompanySettingActivity.this, NewMemberActivity.class);
                    startActivityForResult(intent, REQUEST_CODE_ADD_MEMBERS);
                } else if (current.equalsIgnoreCase(PROJECTROLE)) {
                    intent = new Intent(CompanySettingActivity.this, NewProjectRoleActivity.class);
                    startActivityForResult(intent, REQUEST_CODE_ADD_PROJECTROLE);
                } else if (current.equalsIgnoreCase(COMPANYROLE)) {
                    intent = new Intent(CompanySettingActivity.this, NewCompanyRoleActivity.class);
                    startActivityForResult(intent, REQUEST_CODE_ADD_COMPANYROLE);
                } else if (current.equalsIgnoreCase(LEVEL)) {
                    intent = new Intent(CompanySettingActivity.this, NewLevelActivity.class);
                    startActivityForResult(intent, REQUEST_CODE_ADD_LEVEL);
                } else if (current.equalsIgnoreCase(DEPARTMENT)) {
                    intent = new Intent(CompanySettingActivity.this, NewDepartmentActivity.class);
                    startActivityForResult(intent, REQUEST_CODE_ADD_DEPARTMENT);
                }
            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });

        adapterCompanySetting.setOnItemClickListener(new CompanySettingAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                adapterCompanySetting.setSelectedIndex(position);
                adapterCompanySetting.notifyDataSetChanged();
                viewPager.setCurrentItem(position);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                imgInfo.setImageResource(R.color.grey);
                imgFeature.setImageResource(R.color.grey);
                imgMembers.setImageResource(R.color.grey);
                imgWorkingHoliday.setImageResource(R.color.grey);
                imgQuota.setImageResource(R.color.grey);
                imgPayment.setImageResource(R.color.grey);
                imgProjectRole.setImageResource(R.color.grey);
                imgCompanyRole.setImageResource(R.color.grey);
                imgDevices.setImageResource(R.color.grey);
                imgLevel.setImageResource(R.color.grey);
                imgDepartment.setImageResource(R.color.grey);

                adapterCompanySetting.setSelectedIndex(position);
                adapterCompanySetting.notifyDataSetChanged();
                rcvItem.smoothScrollToPosition(position);
                switch (position) {
                    case 0:
                        imgInfo.setImageResource(R.color.blue);
                        current = INFO;
                        break;
                    case 1:
                        imgFeature.setImageResource(R.color.blue);
                        current = FEATURE;
                        break;
                    case 2:
                        imgMembers.setImageResource(R.color.blue);
                        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);
                        current = MEMBERS;
                        break;
                    case 3:
                        imgWorkingHoliday.setImageResource(R.color.blue);
                        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_FILTER);
                        current = WORKINGHOLIDAY;
                        break;
                    case 4:
                        imgQuota.setImageResource(R.color.blue);
                        current = QUOTA;
                        break;
                    case 5:
                        imgPayment.setImageResource(R.color.blue);
                        current = PAYMENT;
                        break;
                    case 6:
                        imgProjectRole.setImageResource(R.color.blue);
                        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);
                        current = PROJECTROLE;
                        break;
                    case 7:
                        imgCompanyRole.setImageResource(R.color.blue);
                        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);
                        current = COMPANYROLE;
                        break;
                    case 8:
                        imgDevices.setImageResource(R.color.blue);
                        current = DEVICES;
                        break;
                    case 9:
                        imgLevel.setImageResource(R.color.blue);
                        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);
                        current = LEVEL;
                        break;
                    case 10:
                        imgDepartment.setImageResource(R.color.blue);
                        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);
                        current = DEPARTMENT;
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_ADD_DEPARTMENT) {
                if (viewPager.getCurrentItem() == 10) {
                    mFragments.get(10).onUpdateData();
                }
            } else if (requestCode == REQUEST_CODE_ADD_LEVEL) {
                if (viewPager.getCurrentItem() == 9) {
                    mFragments.get(9).onUpdateData();
                }
            } else if (requestCode == REQUEST_CODE_ADD_MEMBERS) {
                if (viewPager.getCurrentItem() == 2) {
                    mFragments.get(2).onUpdateData();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}
