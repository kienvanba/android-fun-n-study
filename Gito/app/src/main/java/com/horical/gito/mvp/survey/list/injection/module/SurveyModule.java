package com.horical.gito.mvp.survey.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.survey.list.presenter.SurveyPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by thanhle on 11/9/16.
 */
@Module
public class SurveyModule {

    @PerActivity
    @Provides
    SurveyPresenter providerSurveyPresenter(){
        return new SurveyPresenter();
    }
}