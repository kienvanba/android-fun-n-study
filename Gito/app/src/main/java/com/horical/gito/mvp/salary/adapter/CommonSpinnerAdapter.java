package com.horical.gito.mvp.salary.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.Arrays;
import java.util.List;

public class CommonSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mItems;
    private int mSelectedPos;
    private ISpinnerAdapterListener mCallback;
    private TextView mTv;

    public CommonSpinnerAdapter(Context context, String[] items, ISpinnerAdapterListener callback) {
        this.mContext = context;
        this.mItems = Arrays.asList(items);
        this.mCallback = callback;
    }

    public CommonSpinnerAdapter(Context context, List<String> items, ISpinnerAdapterListener callback) {
        this.mContext = context;
        this.mItems = items;
        this.mCallback = callback;
    }

    public void setDataChanged(List<String> items) {
        this.mItems = items;
        notifyDataSetChanged();
    }

    private int setSelectedPos() {
        for (int i = 0; i < mItems.size() - 1; i++) {
            if (TextUtils.equals(mItems.get(i), mTv.getText())) {
                return i;
            }
        }
        return -1;
    }

    public void setTextView(TextView tv) {
        this.mTv = tv;
        this.mSelectedPos = setSelectedPos();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int i) {
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_custom_spinner, viewGroup, false);
        }
        TextView names = (TextView) view.findViewById(R.id.tv_text);
        names.setText(mItems.get(position));

        names.setSelected(position == mSelectedPos);
        names.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCustomItemSelected(mItems.get(position), mTv);
            }
        });
        return view;
    }

    public interface ISpinnerAdapterListener {
        void onCustomItemSelected(String text, View view);
    }
}