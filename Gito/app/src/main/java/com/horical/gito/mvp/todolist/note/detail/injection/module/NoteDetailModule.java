package com.horical.gito.mvp.todolist.note.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.todolist.note.detail.presenter.NoteDetailPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Luong on 28-Mar-17.
 */

@Module
public class NoteDetailModule {
    @Provides
    @PerActivity
    NoteDetailPresenter provideNoteDetailPresenter() {
        return new NoteDetailPresenter();
    }
}