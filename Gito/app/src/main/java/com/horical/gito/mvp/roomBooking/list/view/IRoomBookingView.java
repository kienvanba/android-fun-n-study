package com.horical.gito.mvp.roomBooking.list.view;

import com.horical.gito.base.IView;

public interface IRoomBookingView extends IView {
    void showLoading();

    void hideLoading();

    void getAllRoomBookingSuccess();

    void getAllRoomBookingFailure(String err);
}
