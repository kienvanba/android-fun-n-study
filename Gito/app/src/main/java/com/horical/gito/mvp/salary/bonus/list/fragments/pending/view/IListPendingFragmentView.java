package com.horical.gito.mvp.salary.bonus.list.fragments.pending.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

public interface IListPendingFragmentView extends IView {
    void requestGetPendingInputAddonSuccess();

    void requestGetPendingInputAddonError(RestError error);

    void showLoading();

    void hideLoading();
}
