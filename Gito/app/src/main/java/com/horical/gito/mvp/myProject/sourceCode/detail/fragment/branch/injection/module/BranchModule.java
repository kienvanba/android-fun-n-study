package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.presenter.BranchPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class BranchModule {
    @PerFragment
    @Provides
    BranchPresenter provideBranchPresenter(){
        return new BranchPresenter();
    }
}
