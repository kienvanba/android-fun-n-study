package com.horical.gito.mvp.myProject.setting.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.User;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import static android.view.View.GONE;

public class GroupExpandableAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private List<GroupMemItemBody> mItems;

    public GroupExpandableAdapter(Context context, List<GroupMemItemBody> list) {
        this.mContext = context;
        this.mItems = list;
    }

    @Override
    public int getGroupCount() {
        return mItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mItems.get(groupPosition).group.getMembers().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mItems.get(groupPosition).group.getMembers().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean b, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(mContext).inflate(R.layout.item_group_member_setting, viewGroup, false);
        TextView gName = (TextView) view.findViewById(R.id.tv_name);
        String count = " (" + getChildrenCount(groupPosition) + ")";
        gName.setText(mItems.get(groupPosition).group.getGroupName() + count);
        return view;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(mContext).inflate(R.layout.item_group_child, viewGroup, false);
        ImageView cAva = (ImageView) view.findViewById(R.id.img_avatar);
        ProgressBar progressAva = (ProgressBar) view.findViewById(R.id.progress_avatar);
        TextView cNonAva = (TextView) view.findViewById(R.id.tv_avatar);
        TextView cName = (TextView) view.findViewById(R.id.tv_name);
        TextView cRole = (TextView) view.findViewById(R.id.tv_role);
        ImageButton cDelBtn = (ImageButton) view.findViewById(R.id.btn_delete);
        View cDivider = view.findViewById(R.id.child_divider);

        User body = mItems.get(groupPosition).group.getMembers().get(childPosition);

        if (body.getAvatar() == null) {
            cNonAva.setText(body.getDisplayName().substring(0, 1).toUpperCase());
            progressAva.setVisibility(GONE);
        } else {
            cNonAva.setVisibility(GONE);
            String url = CommonUtils.getURL(body.getAvatar());
            ImageLoader.load(mContext, url, cAva, progressAva);
        }

        cName.setText(body.getDisplayName());
        cRole.setText(body.getLevel() != null ? body.getLevel().getDesc() : "N/A");
        cDelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItems.get(groupPosition).group.getMembers().remove(childPosition);
                notifyDataSetChanged();
            }
        });
        if (childPosition == mItems.get(groupPosition).group.getMembers().size() - 1) {
            cDivider.setVisibility(GONE);
        } else {
            cDivider.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }
}
