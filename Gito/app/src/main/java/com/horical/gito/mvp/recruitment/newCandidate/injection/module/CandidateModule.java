package com.horical.gito.mvp.recruitment.newCandidate.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.recruitment.newCandidate.presenter.CandidatePresenter;

import dagger.Module;
import dagger.Provides;


@Module
public class CandidateModule {
    @PerActivity
    @Provides
    CandidatePresenter provideNewRecruitmentPresenter() {
        return new CandidatePresenter();
    }
}

