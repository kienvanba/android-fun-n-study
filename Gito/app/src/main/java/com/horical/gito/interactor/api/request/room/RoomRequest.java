package com.horical.gito.interactor.api.request.room;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.model.Company;


public class RoomRequest {

    @SerializedName("companyId")
    @Expose
    public Company companyId;
    @SerializedName("desc")
    @Expose
    public String desc;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("_id")
    @Expose
    public String id;

}
