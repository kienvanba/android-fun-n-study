package com.horical.gito.interactor.api.response.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.LogTime;

import java.util.List;

public class GetMembersHaveLogTimeResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<LogTime> logTimes;
}
