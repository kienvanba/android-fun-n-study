package com.horical.gito.model.wrapperModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.model.User;

import java.io.Serializable;
import java.util.List;

public class WrapperListUser implements Serializable {

    @SerializedName("myUsers")
    @Expose
    private List<User> myUsers;

    public WrapperListUser(List<User> myUsers) {
        this.myUsers = myUsers;
    }

    public List<User> getMyUsers() {
        return myUsers;
    }

    public void setMyUsers(List<User> myUsers) {
        this.myUsers = myUsers;
    }
}
