package com.horical.gito.mvp.estate.warehouse.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.estate.WarehouseEstate;

import java.util.List;

/**
 * Created by hoangsang on 4/12/17.
 */

public class WarehouseEstateAdapter extends BaseRecyclerAdapter {

    public WarehouseEstateAdapter(List<IRecyclerItem> items, Context context) {
        super(items, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(mContext).inflate(R.layout.item_warehourse_estate, parent, false);
        return new WarehouseItemHolder(item);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        WarehouseEstateItem item = (WarehouseEstateItem) mItems.get(holder.getAdapterPosition());
        final WarehouseItemHolder mHolder = (WarehouseItemHolder) holder;

        WarehouseEstate warehourse = item.warehouseEstate;
        if (warehourse != null) {

            String nameText = ": " + warehourse.name;
            mHolder.mTvName.setText(nameText);

            String itemCountText = ": " + warehourse.itemCount;
            mHolder.mTvItem.setText(itemCountText);

            mHolder.mTvDescription.setText(warehourse.desc);
        }

        mHolder.mImvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDelete(mHolder.getAdapterPosition());
            }
        });

    }

    private OnClick mListener;

    public void setListener(OnClick mListener) {
        this.mListener = mListener;
    }

    public interface OnClick {
        void onDelete(int position);
    }
}
