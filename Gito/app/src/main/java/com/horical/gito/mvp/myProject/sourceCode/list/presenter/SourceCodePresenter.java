package com.horical.gito.mvp.myProject.sourceCode.list.presenter;

import android.util.Log;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.sourceCode.GetAllCodeResponse;
import com.horical.gito.interactor.api.response.sourceCode.GetCreateCodeResponse;
import com.horical.gito.model.Code;
import com.horical.gito.mvp.myProject.sourceCode.list.view.ISourceCodeView;

import java.util.ArrayList;
import java.util.List;

public class SourceCodePresenter extends BasePresenter implements ISourceCodePresenter {

    private List<Code> mList;

    public void attachView(ISourceCodeView view) {
        super.attachView(view);
    }

    public ISourceCodeView getView() {
        return (ISourceCodeView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Code> getListCode() {
        return mList;
    }

    public void setListCode(List<Code> mCode) {
        this.mList = mCode;
    }

    @Override
    public void getAllCode(int perPage, int page) {
        getApiManager().getAllCode(GitOStorage.getInstance().getProject().getId(), perPage, page, new ApiCallback<GetAllCodeResponse>() {
            @Override
            public void success(GetAllCodeResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.codes);
                getView().getAllCodeSuccess();
            }

            @Override
            public void failure(RestError error) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                getView().getAllCodeFailure(error.message);
            }
        });
    }

    @Override
    public void createCode(String nameCode) {
        getApiManager().createCode(getPreferManager().getProjectId(), nameCode, new ApiCallback<GetCreateCodeResponse>() {
            @Override
            public void success(GetCreateCodeResponse res) {
                mList.add(res.code);
                getView().createCodeSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().createCodeFailure(error.message);
            }
        });
    }


}
