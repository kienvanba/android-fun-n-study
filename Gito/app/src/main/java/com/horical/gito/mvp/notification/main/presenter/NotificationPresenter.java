package com.horical.gito.mvp.notification.main.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.notification.main.adapter.NotificationItem;
import com.horical.gito.mvp.notification.main.adapter.NotificationSpinnerItem;
import com.horical.gito.mvp.notification.main.view.INotificationView;
import com.horical.gito.utils.HDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;

public class NotificationPresenter extends BasePresenter implements INotificationPresenter {

    private List<NotificationItem> mListApproved = new ArrayList<>();
    private List<NotificationItem> mListPending = new ArrayList<>();
    private List<NotificationItem> mListMine = new ArrayList<>();
    private List<NotificationSpinnerItem> mListSpinner = new ArrayList<>();
    private int from;

    public List<NotificationItem> getListApproved() {
        return mListApproved;
    }

    public List<NotificationItem> getListPending() {
        return mListPending;
    }

    public List<NotificationItem> getListMine() {
        return mListMine;
    }

    public List<NotificationSpinnerItem> getListSpinner() {return mListSpinner;}

    public void attachView(INotificationView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public INotificationView getView() {
        return (INotificationView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        GenerateTestData();
        InitData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    private void InitData(){
        // Generate Spinner
        Calendar c = Calendar.getInstance();
        HDate today = new HDate(c.get(YEAR),c.get(MONTH),c.get(DAY_OF_MONTH),c.get(HOUR_OF_DAY),c.get(MINUTE),c.get(SECOND)) ;
        mListSpinner.add(new NotificationSpinnerItem("Yesterday",today.yesterday().toStringDayMonthYear()));
        mListSpinner.add(new NotificationSpinnerItem("Today",today.toStringDayMonthYear()));
        HDate[] weekList = today.getListDayLastWeek();
        mListSpinner.add(new NotificationSpinnerItem("Last Week",weekList[0].toStringDayMonthYear()+"-"+weekList[weekList.length-1].toStringDayMonthYear()));
        weekList = today.getListDayThisWeek();
        mListSpinner.add(new NotificationSpinnerItem("This Week",weekList[0].toStringDayMonthYear()+"-"+weekList[weekList.length-1].toStringDayMonthYear()));
        mListSpinner.add(new NotificationSpinnerItem("Last Month",new HDate(today.year,(today.month+11)%12,1).toStringMonthYear()));
        mListSpinner.add(new NotificationSpinnerItem("This Month",today.toStringMonthYear()));
        mListSpinner.add(new NotificationSpinnerItem("Custom","Select Start - End day"));
    }

    public void GenerateTestData(){
        mListApproved.add(new NotificationItem("Tittle1","Reason 1","Unit 1",1));
        mListApproved.add(new NotificationItem("Tittle2","Reason 2","Unit 2",5));
        mListApproved.add(new NotificationItem("Tittle3","Reason 3","Unit 3",5));
        mListApproved.add(new NotificationItem("Tittle4","Reason 4","Unit 4",1));
        mListApproved.add(new NotificationItem("Tittle5","Reason 5","Unit 5",1));

        mListPending.add(new NotificationItem("Tittle1","Reason 1","Unit 1",1));
        mListPending.add(new NotificationItem("Tittle2","Reason 2","Unit 2",5));
        mListPending.add(new NotificationItem("Tittle4","Reason 4","Unit 4",1));
        mListPending.add(new NotificationItem("Tittle3","Reason 3","Unit 3",5));

        mListMine.add(new NotificationItem("Tittle1","Reason 1","Unit 1",1));
        mListMine.add(new NotificationItem("Tittle2","Reason 2","Unit 2",5));
        mListMine.add(new NotificationItem("Tittle5","Reason 5","Unit 5",1));
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }
}
