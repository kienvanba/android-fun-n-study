package com.horical.gito.mvp.caseStudy.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.TokenFile;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by thanhle on 4/10/17.
 */

public class AttachFileAdapter extends RecyclerView.Adapter<AttachFileAdapter.AttachFileHolder> {
    private List<TokenFile> mList;
    private OnItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<String> mListName;

    public AttachFileAdapter(Context context, List<TokenFile> list, List<String> listname) {
        mContext = context;
        mList = list;
        mListName = listname;
    }

    public List<TokenFile> getListTokenFile() {
        return mList;
    }

    public void setListTokenFile(List<TokenFile> mList) {
        this.mList = mList;
    }

    @Override
    public AttachFileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_attach_file, parent, false);
        return new AttachFileHolder(view);
    }

    @Override
    public void onBindViewHolder(AttachFileHolder holder, final int position) {
        TokenFile tokenFile = mList.get(position);
        String name = mListName.get(position);
        if (tokenFile != null) {
            String url = CommonUtils.getURL(tokenFile);
            ImageLoader.load(mContext, url, holder.imgAvatar, holder.pgBarAvatar);
        } else {
            holder.pgBarAvatar.setVisibility(View.GONE);
            holder.imgAvatar.setImageResource(R.drawable.ic_upload_file);
        }
        holder.tvNameFile.setText(name);

        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDelete(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class AttachFileHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.img_avatar)
        CircleImageView imgAvatar;

        @Bind(R.id.pgbar_avatar)
        ProgressBar pgBarAvatar;

        @Bind(R.id.tv_name_file)
        TextView tvNameFile;

        @Bind(R.id.ll_delete)
        LinearLayout llDelete;

        View view;

        public AttachFileHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onDelete(int position);
    }
}
