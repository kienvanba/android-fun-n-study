package com.horical.gito.mvp.todolist.note.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.model.Note;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Luong on 22-Mar-17.
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteHolder> {

    private List<Note> mList;
    private Context mContext;
    OnItemClickListener mOnItemClickListener;

    public NoteAdapter(Context context, List<Note> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public NoteHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_note, parent, false);
        return new NoteHolder(view);
    }

    @Override
    public void onBindViewHolder(NoteHolder holder, final int position) {

        Note note = mList.get(position);
        holder.tvNoteTitle.setText(note.title);
        holder.tvNoteDate.setText(DateUtils.formatDate(note.getCreatedDate(), "dd/MM/yyyy hh:mm a"));
        holder.tvNoteReason.setText(note.desc);

        holder.ivNoteTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDelete(position);
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(position);
            }
        });
    }

    public List<Note> getListNote() {
        return mList;
    }

    public void setListNote(List<Note> mList) {
        this.mList = mList;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class NoteHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_note_title)
        GitOTextView tvNoteTitle;

        @Bind(R.id.imv_note_delete_title)
        ImageView ivNoteTitle;

        @Bind(R.id.tv_note_date)
        GitOTextView tvNoteDate;

        @Bind(R.id.tv_note_reason)
        GitOTextView tvNoteReason;

        View view;


        public NoteHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }


    public void setOnItemClickListener(OnItemClickListener callBack) {
        this.mOnItemClickListener = callBack;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDelete(int position);
    }
}
