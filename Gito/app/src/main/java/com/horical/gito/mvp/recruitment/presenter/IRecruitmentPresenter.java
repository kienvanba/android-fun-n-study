package com.horical.gito.mvp.recruitment.presenter;

public interface IRecruitmentPresenter {
    void getListRecruitmentFromServer();

    void deleteRecruitment(int position);
}
