package com.horical.gito.mvp.myProject.task.details.fragment.comment.injection.component;


import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.comment.injection.module.CommentModule;
import com.horical.gito.mvp.myProject.task.details.fragment.comment.view.CommentFragment;

import dagger.Component;


@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = CommentModule.class)
public interface CommentComponent {
    void inject(CommentFragment fragment);
}
