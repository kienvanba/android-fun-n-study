package com.horical.gito.mvp.salary.list.fragments.subMetricGroup.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.salary.metric.GetDeleteMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetAllMetricGroupResponse;
import com.horical.gito.model.SalaryMetricGroup;
import com.horical.gito.mvp.salary.list.fragments.subMetricGroup.view.IListMetricGroupFragmentView;

import java.util.ArrayList;
import java.util.List;

public class ListMetricGroupFragmentPresenter extends BasePresenter implements IListMetricGroupFragmentPresenter {

    public void attachView(IListMetricGroupFragmentView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IListMetricGroupFragmentView getView() {
        return (IListMetricGroupFragmentView) getIView();
    }

    private List<SalaryMetricGroup> mMetricGroups;

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public List<SalaryMetricGroup> getMetricGroups() {
        if (mMetricGroups == null) {
            mMetricGroups = new ArrayList<>();
        }
        return mMetricGroups;
    }

    @Override
    public void requestGetAllMetricGroup() {
        getApiManager().getAllMetricGroup(new ApiCallback<GetAllMetricGroupResponse>() {
            @Override
            public void success(GetAllMetricGroupResponse res) {
                getMetricGroups().clear();
                getMetricGroups().addAll(res.getMetricGroups());
                getView().requestGetAllMetricGroupSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().requestGetAllMetricGroupError(error);
            }
        });
    }

    @Override
    public void requestDeleteMetricGroup(String metricId) {
        getApiManager().deleteMetricGroup(metricId, new ApiCallback<GetDeleteMetricGroupResponse>() {
            @Override
            public void success(GetDeleteMetricGroupResponse res) {
                getView().requestDeleteMetricGroupSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().requestDeleteMetricGroupError(error);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}