package com.horical.gito.mvp.companySetting.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 3/13/17.
 */

public class CompanySettingAdapter extends RecyclerView.Adapter<CompanySettingAdapter.CompanySettingHolder> {
    private List<String> mList;
    private Context mContext;
    private int selected = 0;

    public CompanySettingAdapter(Context context, List<String> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public CompanySettingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_company_setting, parent, false);
        return new CompanySettingHolder(view);
    }

    @Override
    public void onBindViewHolder(final CompanySettingHolder holder, final int position) {
        String title = mList.get(position);
        holder.tvItem.setText(title);
        holder.tvItem.setSelected(selected == position);

        holder.tvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickListener(position);
            }
        });
    }

    public void setSelectedIndex(int cur) {
        this.selected = cur;
    }

    public List<String> getList() {
        return mList;
    }

    public void setList(List<String> mList) {
        this.mList = mList;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class CompanySettingHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_item)
        TextView tvItem;

        View view;

        public CompanySettingHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}
