package com.horical.gito.mvp.myProject.summary.dto;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.mvp.myProject.summary.adapter.SummaryAdapter;

/**
 * Created by nhonvt.dk on 3/21/17
 */

public class HeaderItem implements IRecyclerItem {

    private int type;
    private String title;

    public HeaderItem(int type, String title) {
        this.type = type;
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public int getItemViewType() {
        return SummaryAdapter.SUMMARY_HEADER_TYPE;
    }
}
