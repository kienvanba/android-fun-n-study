package com.horical.gito.mvp.recruitment.recruitDetail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.recruitment.recruitDetail.presenter.RecruitmentDetailPresenter;

import dagger.Module;
import dagger.Provides;


@Module
public class RecruitmentDetailModule {
    @PerActivity
    @Provides
    RecruitmentDetailPresenter provideNewRecruitmentPresenter() {
        return new RecruitmentDetailPresenter();
    }
}

