package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.view;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.BaseSourceCodeFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.adapter.CommitAdapter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.injection.component.CommitComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.injection.component.DaggerCommitComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.injection.module.CommitModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.presenter.CommitPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 23-Nov-16.
 */

public class CommitFragment extends BaseSourceCodeFragment implements ICommitView, View.OnClickListener {

    @Bind(R.id.code_detail_commit_list)
    RecyclerView mRVDetail;

    private CommitAdapter adapterCommit;

    @Inject
    CommitPresenter mPresenter;

    public static CommitFragment newInstance() {
        Bundle args = new Bundle();
        CommitFragment fragment = new CommitFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_code_commit;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        CommitComponent component = DaggerCommitComponent.builder()
                .commitModule(new CommitModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }


    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View view) {

    }
}
