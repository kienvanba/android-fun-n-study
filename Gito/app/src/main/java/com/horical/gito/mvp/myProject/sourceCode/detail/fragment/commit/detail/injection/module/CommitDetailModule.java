package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.presenter.CommitDetailPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */
@Module
public class CommitDetailModule {
    @PerActivity
    @Provides
    CommitDetailPresenter provideCodeCommitFilePresenter(){
        return new CommitDetailPresenter();
    }
}
