package com.horical.gito.mvp.leaving.main.adapter;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.utils.HDate;

import java.util.List;

/**
 * Created by Dragonoid on 12/6/2016.
 */

public class LeavingMonthInfoItem implements IRecyclerItem {
    @Override
    public int getItemViewType() {
        return 0;
    }
    List<IRecyclerItem> mList;
    String mMonthName;
    int mHour;
    public LeavingMonthInfoItem (HDate month, int hour, List<IRecyclerItem> list){
        mList = list;
        mMonthName = month.monthName();
        mHour = hour;
    }
    public LeavingMonthInfoItem(){

    }
    public List<IRecyclerItem> getList(){
        return mList;
    }
}
