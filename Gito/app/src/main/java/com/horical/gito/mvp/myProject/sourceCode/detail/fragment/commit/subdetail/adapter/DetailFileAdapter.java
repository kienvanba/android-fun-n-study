package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 01-Dec-16.
 */

public class DetailFileAdapter extends RecyclerView.Adapter<DetailFileAdapter.DetailFileHolder> {
    private Context mContext;
    private List<DetailFileItemBody> mList;
    private OnItemClickListener mOnItemClickListener;

    public DetailFileAdapter(Context context, List<DetailFileItemBody> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public DetailFileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_code_commit_file, parent, false);
        return new DetailFileHolder(view);
    }

    @Override
    public void onBindViewHolder(DetailFileHolder holder, final int position) {
        DetailFileItemBody detailFileHolder = mList.get(position);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class DetailFileHolder extends RecyclerView.ViewHolder {

        View view;
        public DetailFileHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);
    }
}
