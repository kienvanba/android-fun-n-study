package com.horical.gito.mvp.estate.list.view;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.base.IView;

import java.util.List;

/**
 * Created by hoangsang on 11/8/16.
 */

public interface IEstateView extends IView {
    String KEY_ESTATE = "KEY_ESTATE";
    String KEY_WAREHOUSE_ID = "KEY_WAREHOUSE_ID";

    void showLoading();

    void hideLoading();

    boolean isRefreshing();

    void getAllEstateItemSuccess();

    void getAllEstateItemFailed();

    void createEstateItemSuccess();

    void createEstateItemFailed(String errorMessage);

    void deleteEstateItemSuccess();

    void deleteEstateItemFailed(String errorMessage);

}
