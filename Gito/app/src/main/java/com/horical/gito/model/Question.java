package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nhattruong251295 on 4/11/2017.
 */

public class Question implements Serializable {
    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("type")
    @Expose
    private Integer type;

    @SerializedName("option")
    @Expose
    private List<Option> optionList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Option> getOptionList() {
        return optionList;
    }

    public void setOptionList(List<Option> optionList) {
        this.optionList = optionList;
    }
}
