package com.horical.gito.interactor.api.response.report;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Reports;

import java.util.ArrayList;
import java.util.List;

public class AllReportResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    private List<Reports> results;

    public List<Reports> getResults() {
        if (results == null) {
            results = new ArrayList<>();
        }
        return results;
    }

    public void setResults(List<Reports> results) {
        this.results = results;
    }
}
