package com.horical.gito.interactor.api.request.salary.inputAddon;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterInputAddonRequest {
    @SerializedName("approve")
    @Expose
    private boolean approve;

    @SerializedName("pending")
    @Expose
    private boolean pending;

    @SerializedName("mine")
    @Expose
    private boolean mine;

    public FilterInputAddonRequest(boolean approve, boolean pending, boolean mine) {
        this.approve = approve;
        this.pending = pending;
        this.mine = mine;
    }

    public boolean isMine() {
        return mine;
    }

    public void setMine(boolean mine) {
        this.mine = mine;
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    public boolean isApprove() {
        return approve;
    }

    public void setApprove(boolean approve) {
        this.approve = approve;
    }
}
