package com.horical.gito.mvp.myProject.setting.fragment.group.injection.component;


import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.setting.fragment.group.injection.module.GroupModule;
import com.horical.gito.mvp.myProject.setting.fragment.group.view.GroupFragment;

import dagger.Component;

@PerFragment
@Component (dependencies = ApplicationComponent.class, modules = GroupModule.class)
public interface GroupComponent {
    void inject(GroupFragment fragment);
}
