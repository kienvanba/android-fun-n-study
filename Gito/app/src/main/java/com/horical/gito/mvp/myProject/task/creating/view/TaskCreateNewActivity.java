package com.horical.gito.mvp.myProject.task.creating.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.Task;
import com.horical.gito.model.wrapperModel.WrapperListUser;
import com.horical.gito.mvp.dialog.dialogTask.TaskDateTimePickerDialog;
import com.horical.gito.mvp.myProject.task.creating.adapter.UserAdapter;
import com.horical.gito.mvp.myProject.task.creating.injection.component.DaggerTaskCreateNewComponent;
import com.horical.gito.mvp.myProject.task.creating.injection.component.TaskCreateNewComponent;
import com.horical.gito.mvp.myProject.task.creating.injection.module.TaskCreateNewModule;
import com.horical.gito.mvp.myProject.task.creating.presenter.TaskCreateNewPresenter;
import com.horical.gito.mvp.popup.popupTask.PopupTask;
import com.horical.gito.mvp.user.list.view.UserActivity;
import com.horical.gito.utils.HDate;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class TaskCreateNewActivity extends BaseActivity implements ITaskCreateNewView, View.OnClickListener {
    public final static int REQUEST_CODE_USER = 11;
    public static final String OVERVIEW = "overview";
    public static final String SUBTASK = "subtask";
    public static final String LOGTIME = "logtime";
    public static final String COMMENT = "comment";
    private String current = OVERVIEW;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.btn_pick_time)
    ImageView btnPickTime;

    @Bind(R.id.edt_title)
    EditText edtTitle;

    @Bind(R.id.edt_description)
    EditText edtDescription;

    @Bind(R.id.tv_type_overview)
    TextView tvType;

    @Bind(R.id.tv_status_overview)
    TextView tvStatus;

    @Bind(R.id.tv_priority_overview)
    TextView tvPriority;

    @Bind(R.id.btn_add_assignee)
    ImageView btnAddAssignee;

    @Bind(R.id.rcv_assignee)
    RecyclerView rcvAssignee;

    private UserAdapter userAdapter;

    public Task getTask() {
        Task task = new Task();
        task.setTitle(edtTitle.getText().toString());
        task.setDesc(edtDescription.getText().toString());
        task.setTaskType(PopupTask.getOrderInList(tvType.getText().toString()));
        task.setStatus(PopupTask.getOrderInList(tvStatus.getText().toString()));
        task.setPriority(PopupTask.getOrderInList(tvPriority.getText().toString()));
        return task;
    }

    private PopupTask popupTask;
    private String keyType;
    private String keyStatus;
    private String keyPriority;
    private TaskDateTimePickerDialog taskPickTime;

    private WrapperListUser wrapperListUser;

    @Inject
    TaskCreateNewPresenter mPresenter;

    private static final String TAG = makeLogTag(TaskCreateNewActivity.class);

    protected int getLayoutId() {
        return R.layout.activity_task_create_new;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);

        TaskCreateNewComponent component = DaggerTaskCreateNewComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .taskCreateNewModule(new TaskCreateNewModule())
                .build();

        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle("New Task");
        topBar.setTextViewLeft("Cancel");
        topBar.setTextViewRight("Save");

        keyType = "Task";
        tvType.setText("Task");
        keyStatus = "Open";
        tvStatus.setText("Open");
        keyPriority = "Immediate";
        tvPriority.setText("Immediate");

        List<String> listUser = new ArrayList<>();
        listUser.add("user A");
        listUser.add("user B");
        listUser.add("user C");

        userAdapter = new UserAdapter(getApplicationContext(), listUser);
        LinearLayoutManager llUser = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        rcvAssignee.setLayoutManager(llUser);
        rcvAssignee.setHasFixedSize(false);
        rcvAssignee.setAdapter(userAdapter);
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {
                mPresenter.createTask(getTask());
            }

        });

        tvType.setOnClickListener(this);
        tvStatus.setOnClickListener(this);
        tvPriority.setOnClickListener(this);
        btnPickTime.setOnClickListener(this);
        btnAddAssignee.setOnClickListener(this);
    }

    public void updateTextViewSelected(TextView tv) {
        tv.setSelected(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_type_overview:
                popupTask = new PopupTask(getApplicationContext(), new PopupTask.OnItemSelectedListener() {
                    @Override
                    public void onClickSelected(String codeFilter) {
                        tvType.setText(codeFilter);
                        keyType = codeFilter;
                    }
                });
                popupTask.showTaskType(tvType, keyType);
                break;
            case R.id.tv_status_overview:
                popupTask = new PopupTask(getApplicationContext(), new PopupTask.OnItemSelectedListener() {
                    @Override
                    public void onClickSelected(String codeFilter) {
                        tvStatus.setText(codeFilter);
                        keyStatus = codeFilter;
                    }
                });
                popupTask.showStatus(tvStatus, keyStatus);
                break;
            case R.id.tv_priority_overview:
                popupTask = new PopupTask(getApplicationContext(), new PopupTask.OnItemSelectedListener() {
                    @Override
                    public void onClickSelected(String codeFilter) {
                        tvPriority.setText(codeFilter);
                        keyPriority = codeFilter;
                    }
                });
                popupTask.showPriority(tvPriority, keyPriority);
                break;
            case R.id.btn_pick_time: {

                taskPickTime = new TaskDateTimePickerDialog(TaskCreateNewActivity.this, new TaskDateTimePickerDialog.DateTimeDialogCallback() {
                    @Override
                    public void onDoneClick(HDate DateTimeFrom, HDate DateTimeTo) {
                        // TODO use here
//                mPresenter.setStartDate(DateTimeFrom);
//                mPresenter.setEndDate(DateTimeTo);
//                mTvStartDate.setText(DateUtils.formatDate(mPresenter.getStartDate(), "dd/MM/yyyy hh:mm a"));
//                mTvEndDate.setText(DateUtils.formatDate(mPresenter.getEndDate(), "dd/MM/yyyy hh:mm a"));
                    }

                    @Override
                    public void onCancelClick() {
                        taskPickTime.dismiss();
                    }
                });
                taskPickTime.setFrom(new HDate());
                taskPickTime.setTo(new HDate());
                taskPickTime.show();
                break;
            }
            case R.id.btn_add_assignee:
                Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                intent.putExtra(
                        UserActivity.KEY_FROM,
                        UserActivity.FROM_NEW_SURVEY
                );

                startActivityForResult(intent, REQUEST_CODE_USER);
                break;
        }
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void createTaskSuccess() {
        hideLoading();
        showToast("Success");
        finish();
    }

    @Override
    public void createTaskFailure(String error) {
        hideLoading();
        showToast("Failure error: " + error);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_USER) {
            if (resultCode == Activity.RESULT_OK) {

                wrapperListUser = (WrapperListUser) data.getSerializableExtra("LIST_USER");
                for (int i = 0; i < wrapperListUser.getMyUsers().size(); i++) {
                    Log.d("TAG", wrapperListUser.getMyUsers().get(i).getDisplayName());
                }

            }
        }
    }
}
