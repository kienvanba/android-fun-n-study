package com.horical.gito.mvp.checkin.list.view.dto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.mvp.checkin.list.view.adapter.CheckinFilterAdapter;
import com.horical.gito.mvp.checkin.list.view.adapter.ListUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;

/**
 * Created by Penguin on 05-Dec-16.
 */

public class CheckinFilter extends BaseActivity implements ICheckinView, View.OnClickListener {

    LinearLayout rb_btn_back;

    TextView lastweek;
    TextView thisweek;
    TextView nextweek;
    TextView custom;

    RecyclerView user;

    List<ListUser> mList;

    CheckinFilterAdapter mcheckinfilteradapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setContentView(R.layout.activity_checkin_filter);

        findViewsById();

        rb_btn_back.setOnClickListener(this);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Calendar cal2 = Calendar.getInstance();
        Calendar cal3 = Calendar.getInstance();
        cal2.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal3.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        cal2.add(Calendar.DATE, -7);
        cal3.add(Calendar.DATE, -7);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        String strLastweek1 = sdf1.format(cal2.getTime());
        String strLastweek2 = sdf1.format(cal3.getTime());
        lastweek.setText(strLastweek1 + "\t-\t" + strLastweek2);

        Calendar cal4 = Calendar.getInstance();
        Calendar cal5 = Calendar.getInstance();
        cal4.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal5.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        String strThisweek1 = sdf.format(cal4.getTime());
        String strThisweek2 = sdf.format(cal5.getTime());
        thisweek.setText(strThisweek1 + "\t-\t" + strThisweek2);

        Calendar cal6 = Calendar.getInstance();
        Calendar cal7 = Calendar.getInstance();
        cal6.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal7.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        cal6.add(Calendar.DATE, 7);
        cal7.add(Calendar.DATE, 7);
        String strNextweek1 = sdf.format(cal6.getTime());
        String strNextweek2 = sdf1.format(cal7.getTime());
        nextweek.setText(strNextweek1 + "\t-\t" + strNextweek2);



        getData();
    }

    private void findViewsById() {
        rb_btn_back = (LinearLayout) findViewById(R.id.rb_btn_back);
        lastweek = (TextView) findViewById(R.id.tv_lastweek);
        thisweek = (TextView) findViewById(R.id.tv_thisweek);
        nextweek = (TextView) findViewById(R.id.tv_nextweek);
        custom  = (TextView) findViewById(R.id.tv_custom);
        user = (RecyclerView) findViewById(R.id.rv_checkin_filter_user);

    }

    private void getData() {
        mList = new ArrayList<>();

        final ListUser listUser = new ListUser(R.drawable.doraemon, "User A", "Admin", R.drawable.ic_delete);

        ListUser listUser1 = new ListUser(R.drawable.doraemon, "User B", "Tester", R.drawable.ic_delete);

        mList.add(listUser);
        mList.add(listUser1);

        mcheckinfilteradapter = new CheckinFilterAdapter(this, mList);
        LinearLayoutManager li = new LinearLayoutManager(this);
        li.setOrientation(LinearLayoutManager.VERTICAL);
        user.setLayoutManager(li);
        user.setAdapter(mcheckinfilteradapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rb_btn_back:
                Intent b = new Intent(this, CheckinPresence.class);
                startActivity(b);
                break;
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void getAllCheckinSuccess() {

    }

}

