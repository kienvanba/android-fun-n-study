package com.horical.gito.mvp.salary.detail.metricGroup.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.salary.metric.GetDeleteMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metric.GetDetailMetricResponse;
import com.horical.gito.model.SalaryMetricGroup;
import com.horical.gito.mvp.salary.detail.metricGroup.view.IDetailMetricGroupView;

public class DetailMetricGroupPresenter extends BasePresenter implements IDetailMetricGroupPresenter {

    public void attachView(IDetailMetricGroupView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IDetailMetricGroupView getView() {
        return (IDetailMetricGroupView) getIView();
    }

    private SalaryMetricGroup metricGroup;

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public SalaryMetricGroup getMetricGroup() {
        if (metricGroup == null) {
            metricGroup = new SalaryMetricGroup();
        }
        return metricGroup;
    }

    @Override
    public void requestGetMetricGroup(String metricGroupId) {
        getApiManager().getMetricById(metricGroupId, new ApiCallback<GetDetailMetricResponse>() {
            @Override
            public void success(GetDetailMetricResponse res) {

            }

            @Override
            public void failure(RestError error) {

            }
        });
    }

    @Override
    public void requestDeleteSalaryMetric(String metricId) {
        getApiManager().deleteMetric(metricId, new ApiCallback<GetDeleteMetricGroupResponse>() {
            @Override
            public void success(GetDeleteMetricGroupResponse res) {
                getView().requestDeleteMetricSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().requestDeleteMetricError(error);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
