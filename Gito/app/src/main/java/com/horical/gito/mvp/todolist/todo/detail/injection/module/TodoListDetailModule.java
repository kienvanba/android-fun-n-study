package com.horical.gito.mvp.todolist.todo.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.todolist.todo.detail.presenter.TodoListDetailPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Luong on 16-Mar-17.
 */

@Module
public class TodoListDetailModule {
    @Provides
    @PerActivity
    TodoListDetailPresenter provideTodoListDetailPresenter() {
        return new TodoListDetailPresenter();
    }
}