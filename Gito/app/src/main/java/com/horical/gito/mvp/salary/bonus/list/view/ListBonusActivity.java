package com.horical.gito.mvp.salary.bonus.list.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.mvp.salary.bonus.detail.view.DetailBonusActivity;
import com.horical.gito.mvp.salary.bonus.list.adapter.BonusHeaderAdapter;
import com.horical.gito.mvp.salary.bonus.list.adapter.ListBonusPagerAdapter;
import com.horical.gito.mvp.salary.bonus.list.fragments.BaseListBonusFragment;
import com.horical.gito.mvp.salary.bonus.list.fragments.approved.view.ListApprovedFragment;
import com.horical.gito.mvp.salary.bonus.list.fragments.mine.view.ListMineFragment;
import com.horical.gito.mvp.salary.bonus.list.fragments.pending.view.ListPendingFragment;
import com.horical.gito.mvp.salary.bonus.list.injection.component.DaggerIListBonusComponent;
import com.horical.gito.mvp.salary.bonus.list.injection.component.IListBonusComponent;
import com.horical.gito.mvp.salary.bonus.list.injection.module.ListBonusModule;
import com.horical.gito.mvp.salary.bonus.list.presenter.ListBonusPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class ListBonusActivity extends BaseActivity implements View.OnClickListener,
        IListBonusView {

    @Bind(R.id.ln_back)
    LinearLayout lnBack;

    @Bind(R.id.imv_new_bonus)
    ImageView imvNewBonus;

    @Bind(R.id.rcv_bonus_header)
    RecyclerView rcvHeader;

    @Bind(R.id.vp_bonus)
    ViewPager vpBonus;

    @Bind(R.id.indicator_bonus)
    CircleIndicator indicator;

    @Inject
    ListBonusPresenter mPresenter;

    private BonusHeaderAdapter mHeaderAdapter;
    private ListBonusPagerAdapter mPagerAdapter;
    private List<BaseListBonusFragment> mFragments = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_bonus);
        ButterKnife.bind(this);
        IListBonusComponent component = DaggerIListBonusComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .listBonusModule(new ListBonusModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    private void setFragments() {

        /* Approved */
        ListApprovedFragment approvedFragment = ListApprovedFragment.newInstance();

        /* Pending */
        ListPendingFragment pendingFragment = ListPendingFragment.newInstance();

        /* Mine */
        ListMineFragment mineFragment = ListMineFragment.newInstance();

        mFragments.add(approvedFragment);
        mFragments.add(pendingFragment);
        mFragments.add(mineFragment);
    }

    protected void initData() {
        setFragments();

        //header
        mHeaderAdapter = new BonusHeaderAdapter(this, mPresenter.getHeaders(), new BonusHeaderAdapter.HeaderAdapterListener() {
            @Override
            public void onSelected(int position) {
                mHeaderAdapter.setSelectedTab(position);
                mHeaderAdapter.notifyDataSetChanged();
                vpBonus.setCurrentItem(position);
            }
        });
        rcvHeader.setAdapter(mHeaderAdapter);
        rcvHeader.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        //pager
        mPagerAdapter = new ListBonusPagerAdapter(getSupportFragmentManager(), mFragments);
        vpBonus.setAdapter(mPagerAdapter);
        indicator.setViewPager(vpBonus);
        vpBonus.setOffscreenPageLimit(mFragments.size());
    }

    protected void initListener() {
        lnBack.setOnClickListener(this);
        imvNewBonus.setOnClickListener(this);
        vpBonus.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mHeaderAdapter.setSelectedTab(position);
                mHeaderAdapter.notifyDataSetChanged();
                rcvHeader.smoothScrollToPosition(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ln_back:
                finish();
                break;

            case R.id.imv_new_bonus:
                Intent intent = new Intent(ListBonusActivity.this, DetailBonusActivity.class);
                intent.putExtra(AppConstants.BONUS_MODE_KEY, getString(R.string.new_bonus));
                startActivity(intent);
                break;

            default:
                break;
        }
    }
}