package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nxon on 3/17/17
 */

public class GanttRole implements Serializable {

    @SerializedName("view")
    @Expose
    private boolean view;

    public boolean isView() {
        return view;
    }

    public void setView(boolean view) {
        this.view = view;
    }
}
