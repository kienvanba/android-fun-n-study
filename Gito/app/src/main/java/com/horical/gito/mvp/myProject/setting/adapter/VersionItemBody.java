package com.horical.gito.mvp.myProject.setting.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.Version;

public class VersionItemBody implements IRecyclerItem {
    public Version version;

    public VersionItemBody(Version version){
        this.version = version;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.VERSION_ITEM;
    }
}
