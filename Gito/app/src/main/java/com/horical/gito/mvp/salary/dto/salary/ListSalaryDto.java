package com.horical.gito.mvp.salary.dto.salary;

import java.io.Serializable;
import java.util.Date;

public class ListSalaryDto implements Serializable {
    private Date createDate;
    private double total;
    private String currency;
    private String name;
    private String major;
    private Date approvedDate;
    private String status;

    public ListSalaryDto() {
    }

    public ListSalaryDto(Date createDate, double total, String currency, String name, String major, Date approvedDate, String status) {
        this.createDate = createDate;
        this.total = total;
        this.currency = currency;
        this.name = name;
        this.major = major;
        this.approvedDate = approvedDate;
        this.status = status;
    }

    //get set
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
