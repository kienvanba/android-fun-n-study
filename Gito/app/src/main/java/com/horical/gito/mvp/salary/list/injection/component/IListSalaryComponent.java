package com.horical.gito.mvp.salary.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.list.injection.module.ListSalaryModule;
import com.horical.gito.mvp.salary.list.view.ListSalaryActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ListSalaryModule.class)
public interface IListSalaryComponent {
    void inject(ListSalaryActivity activity);
}
