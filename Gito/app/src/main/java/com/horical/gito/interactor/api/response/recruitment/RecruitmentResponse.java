package com.horical.gito.interactor.api.response.recruitment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Recruitment;

import java.util.ArrayList;
import java.util.List;


public class RecruitmentResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    private List<Recruitment> data;

    public List<Recruitment> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(List<Recruitment> data) {
        this.data = data;
    }
}
