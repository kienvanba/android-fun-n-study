package com.horical.gito.interactor.api.request.report;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class MemberRequest {
    public MemberRequest(Date startDate, Date endDate, String creatorId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.creatorId = creatorId;
    }

    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("endDate")
    @Expose
    public Date endDate;

    @SerializedName("creatorId")
    @Expose
    public String creatorId;
}
