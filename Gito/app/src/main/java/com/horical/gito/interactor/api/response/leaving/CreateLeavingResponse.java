package com.horical.gito.interactor.api.response.leaving;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Leaving;

public class CreateLeavingResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    Leaving leaving;

    public Leaving getLeaving() {
        return leaving;
    }

    public void setLeaving(Leaving leaving) {
        this.leaving = leaving;
    }
}
