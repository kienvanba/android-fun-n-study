package com.horical.gito.mvp.report.objectDto;

import java.util.List;

public class ReportByContentDto {

    private String typeStatus;
    private List<ContentDto> list;

    public ReportByContentDto(String typeStatus, List<ContentDto> list) {
        this.typeStatus = typeStatus;
        this.list = list;
    }

    public String getTypeStatus() {
        return typeStatus;
    }

    public void setTypeStatus(String typeStatus) {
        this.typeStatus = typeStatus;
    }

    public List<ContentDto> getList() {
        return list;
    }

    public void setList(List<ContentDto> list) {
        this.list = list;
    }
}
