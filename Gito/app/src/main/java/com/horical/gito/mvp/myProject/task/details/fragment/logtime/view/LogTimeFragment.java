package com.horical.gito.mvp.myProject.task.details.fragment.logtime.view;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.myProject.task.details.fragment.logtime.injection.component.DaggerLogTimeComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.logtime.injection.component.LogTimeComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.logtime.injection.module.LogTimeModule;
import com.horical.gito.mvp.myProject.task.details.fragment.logtime.presenter.LogTimePresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class LogTimeFragment extends BaseFragment implements ILogTimeView, View.OnClickListener{

    public static final String TAG = makeLogTag(LogTimeFragment.class);

    @Bind(R.id.rcvLogtime)
    RecyclerView rcvLogtime;

    public static LogTimeFragment newInstance() {

        Bundle args = new Bundle();

        LogTimeFragment fragment = new LogTimeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    LogTimePresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_task_details_logtime;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        LogTimeComponent component = DaggerLogTimeComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .logTimeModule(new LogTimeModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }
}
