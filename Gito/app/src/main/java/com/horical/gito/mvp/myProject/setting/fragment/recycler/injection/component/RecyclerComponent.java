package com.horical.gito.mvp.myProject.setting.fragment.recycler.injection.component;


import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.injection.module.RecyclerModule;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.view.BaseRecyclerFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = RecyclerModule.class)
public interface RecyclerComponent {
    void inject(BaseRecyclerFragment fragment);
}
