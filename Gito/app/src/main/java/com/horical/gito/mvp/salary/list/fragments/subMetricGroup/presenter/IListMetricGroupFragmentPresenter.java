package com.horical.gito.mvp.salary.list.fragments.subMetricGroup.presenter;

import com.horical.gito.model.SalaryMetricGroup;

import java.util.List;

public interface IListMetricGroupFragmentPresenter {
    List<SalaryMetricGroup> getMetricGroups();

    void requestGetAllMetricGroup();

    void requestDeleteMetricGroup(String metricGroupId);

}
