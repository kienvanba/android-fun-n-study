package com.horical.gito.model;


import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataUser {
    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("displayName")
    @Expose
    private String displayName;

    @SerializedName("avatar")
    @Expose
    private TokenFile avatar;

    @SerializedName("level")
    @Expose
    private Level level;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String showNamePhoto() {
        if (TextUtils.isEmpty(displayName)) {
            return "";
        } else {
            try {
                String CurrentString = displayName.trim();
                String[] separated = CurrentString.split(" ");
                if (separated.length == 1) {
                    return separated[0].substring(0, 2).toUpperCase();
                } else {
                    return separated[0].substring(0, 1).toUpperCase() + separated[1].substring(0, 1).toUpperCase();
                }
            } catch (Exception e) {
                return displayName.substring(0, 1).toUpperCase();
            }

        }
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public TokenFile getAvatar() {
        if (avatar == null) {
            avatar = new TokenFile();
        }
        return avatar;
    }

    public Level getLevel() {
        if (level == null) {
            level = new Level();
        }
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public void setAvatar(TokenFile avatar) {
        this.avatar = avatar;
    }
}
