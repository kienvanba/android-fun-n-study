package com.horical.gito.mvp.user.list.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.TokenFile;
import com.horical.gito.model.User;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserHolder> {

    private Context mContext;
    private List<User> mList;

    private OnItemClickListener listener;

    public UserAdapter(Context context, List<User> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_user, parent, false);
        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(final UserHolder holder, final int position) {
        final User user = mList.get(position);

        holder.tvName.setText(user.getDisplayName());
        holder.tvRole.setText(user.getLevel().getName());
        holder.cbUser.setChecked(user.isSelected());

        String url = CommonUtils.getURL(user.getAvatar());
        if (!url.isEmpty()) {
            ImageLoader.load(
                    mContext,
                    url,
                    holder.imvAvatar,
                    holder.pgrLoading
            );
        } else {
            holder.pgrLoading.setVisibility(View.GONE);
            holder.tvSortName.setVisibility(View.VISIBLE);
            holder.tvSortName.setText(user.getDisplayName().substring(0, 1).toUpperCase());
        }

        // Set role
        if (user.isAdmin()) {
            holder.tvRole.setText(mContext.getString(R.string.admin));
        } else {
            holder.tvRole.setText(user.getRoleCompany().getRoleName());
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelected = !user.isSelected();
                user.setSelected(isSelected);
                holder.cbUser.setChecked(isSelected);
                listener.onItemClickListener(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }


    public class UserHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.task_imv_avatar)
        CircleImageView imvAvatar;

        @Bind(R.id.task_tv_name)
        GitOTextView tvName;

        @Bind(R.id.task_tv_role)
        GitOTextView tvRole;

        @Bind(R.id.task_pgr_avt_loading)
        ProgressBar pgrLoading;

        @Bind(R.id.task_tv_sort_name)
        GitOTextView tvSortName;

        @Bind(R.id.cb_user)
        CheckBox cbUser;

        View view;

        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }

    public boolean isAllUserChecked() {

        for (User user : mList) {
            if (!user.isSelected()) return false;
        }
        return true;
    }
}
