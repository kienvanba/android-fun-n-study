package com.horical.gito.mvp.salary.dto.mySalary;

public class ListMySalaryDto {
    private String month;
    private String year;
    private double total;

    public ListMySalaryDto(String month, String year, double total) {
        this.month = month;
        this.year = year;
        this.total = total;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
