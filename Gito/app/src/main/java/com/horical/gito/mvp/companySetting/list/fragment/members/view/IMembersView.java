package com.horical.gito.mvp.companySetting.list.fragment.members.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IMembersView extends IView {
    void showLoading();

    void hideLoading();

    void getAllMemberSuccess();

    void getAllMemberFailure(String err);

    void deleteMemberSuccess(int position);

    void deleteMemberFailure(String err);

    void updateMemberSuccess();

    void updateMemberFailure(String err);
}
