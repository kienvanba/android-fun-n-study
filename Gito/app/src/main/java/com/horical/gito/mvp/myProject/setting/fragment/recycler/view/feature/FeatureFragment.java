package com.horical.gito.mvp.myProject.setting.fragment.recycler.view.feature;

import com.horical.gito.mvp.myProject.setting.fragment.recycler.view.BaseRecyclerFragment;

public class FeatureFragment extends BaseRecyclerFragment {
    @Override
    protected void requestData() {
        mPresenter.getAllFeatures();
        dividerVisibility = false;
    }
    @Override
    protected void setOnItemClickListener(int position) {

    }

    @Override
    protected void setOnDeleteItemClickListener(int position) {

    }

    @Override
    public void setOnAddItemClickListener() {

    }
}
