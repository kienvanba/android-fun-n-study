package com.horical.gito.mvp.leaving.confirm.adapter;

import com.horical.gito.utils.HDate;
import com.horical.gito.base.IRecyclerItem;
import java.util.ArrayList;

/**
 * Created by Dragonoid on 1/17/2017.
 */

public class LeavingConfirmItem implements IRecyclerItem {
    @Override
    public int getItemViewType() {
        return 0;
    }

    ArrayList<IRecyclerItem> mListDetail;
    HDate mDate;
    String mDateString;

    public LeavingConfirmItem (HDate date,ArrayList<IRecyclerItem> list){
        mDate=date;
        mListDetail = list;
        mDateString = mDate.toStringDayMonthYear();
    }

    public HDate getDate(){
        return mDate;
    }
    public ArrayList<IRecyclerItem> getList(){
        return mListDetail;
    }

}
