package com.horical.gito.mvp.myProject.setting.fragment.group.presenter;

import com.horical.gito.mvp.myProject.setting.adapter.GroupMemItemBody;

import java.util.List;

public interface IGroupPresenter {
    List<GroupMemItemBody> getGroups();
    void getAllGroup();
}
