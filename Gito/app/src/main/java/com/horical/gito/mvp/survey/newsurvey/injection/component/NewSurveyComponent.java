package com.horical.gito.mvp.survey.newsurvey.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.survey.newsurvey.injection.module.NewSurveyModule;
import com.horical.gito.mvp.survey.newsurvey.view.NewSurveyActivity;

import dagger.Component;

/**
 * Created by thanhle on 11/12/16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = NewSurveyModule.class)
public interface NewSurveyComponent {
    void inject(NewSurveyActivity activity);
}
