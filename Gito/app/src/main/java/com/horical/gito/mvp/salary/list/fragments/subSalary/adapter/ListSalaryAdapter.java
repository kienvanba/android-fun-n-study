package com.horical.gito.mvp.salary.list.fragments.subSalary.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.salary.dto.salary.ListSalaryDto;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.utils.DateUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListSalaryAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<ListSalaryDto> items;
    private SalaryAdapterListener callback;

    public ListSalaryAdapter(Context context, List<ListSalaryDto> items, SalaryAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_salary, parent, false);
        return new SalaryHolder(view);
    }

    private void updateStatus(SalaryHolder bonusHolder, String status) {
        bonusHolder.tvStatus.setText(status);
        if (context.getString(R.string.rejected).equalsIgnoreCase(status)) {
            bonusHolder.imvDelete.setVisibility(View.VISIBLE);
            bonusHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.red));
        } else if (context.getString(R.string.new_salary).equalsIgnoreCase(status)) {
            bonusHolder.imvDelete.setVisibility(View.VISIBLE);
            bonusHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.orange));

        } else if (context.getString(R.string.approved).equalsIgnoreCase(status)) {
            bonusHolder.imvDelete.setVisibility(View.GONE);
            bonusHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.green_color));

        } else if (context.getString(R.string.submitted).equalsIgnoreCase(status)) {
            bonusHolder.imvDelete.setVisibility(View.GONE);
            bonusHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.green_doing));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListSalaryDto listSalaryDto = items.get(position);
        SalaryHolder salaryHolder = (SalaryHolder) holder;
        updateStatus(salaryHolder, listSalaryDto.getStatus());
        salaryHolder.tvCreateDate.setText(DateUtils.formatDate(listSalaryDto.getCreateDate()));
        salaryHolder.tvTotal.setText(CommonUtils.amountFormatEnGB(listSalaryDto.getTotal(), listSalaryDto.getCurrency()));
        salaryHolder.tvName.setText(listSalaryDto.getName());
        salaryHolder.tvMajor.setText(listSalaryDto.getMajor());
        salaryHolder.tvApprovedDate.setText(DateUtils.formatDate(listSalaryDto.getApprovedDate()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class SalaryHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_monthly_time)
        TextView tvMonthlyTime;

        @Bind(R.id.tv_status)
        TextView tvStatus;

        @Bind(R.id.imv_delete)
        ImageView imvDelete;

        @Bind(R.id.tv_create_date)
        TextView tvCreateDate;

        @Bind(R.id.tv_total)
        TextView tvTotal;

        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.tv_major)
        TextView tvMajor;

        @Bind(R.id.tv_approved_date)
        TextView tvApprovedDate;

        View view;

        SalaryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onSelected(getAdapterPosition());
                }
            });

            imvDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onRemove(getAdapterPosition());
                }
            });
        }
    }

    public interface SalaryAdapterListener {
        void onSelected(int position);

        void onRemove(int position);
    }
}