package com.horical.gito.mvp.estate.warehouse.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.estate.WarehouseEstate;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.estate.adding.view.CreateWarehouseEstateActivity;
import com.horical.gito.mvp.estate.list.view.EstateActivity;
import com.horical.gito.mvp.estate.list.view.IEstateView;
import com.horical.gito.mvp.estate.warehouse.adapter.WarehouseEstateAdapter;
import com.horical.gito.mvp.estate.warehouse.adapter.WarehouseEstateItem;
import com.horical.gito.mvp.estate.warehouse.injection.component.DaggerWarehouseEstateComponent;
import com.horical.gito.mvp.estate.warehouse.injection.component.WarehouseEstateComponent;
import com.horical.gito.mvp.estate.warehouse.injection.module.WarehouseEstateModule;
import com.horical.gito.mvp.estate.warehouse.presenter.WarehouseEstatePresenter;
import com.horical.gito.widget.dialog.DialogPositiveNegative;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class WarehouseEstateActivity extends DrawerActivity implements IWarehouseEstateView, BaseRecyclerAdapter.OnItemClickListener {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.warehourse_estate_refresh_layout)
    SwipeRefreshLayout mRefreshLayout;

    @Bind(R.id.recycle_warehourse_estate)
    RecyclerView mRecycleWarehourse;

    WarehouseEstateAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_warehourse_estate;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_ESTATE;
    }

    @Inject
    WarehouseEstatePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        WarehouseEstateComponent component = DaggerWarehouseEstateComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .warehouseEstateModule(new WarehouseEstateModule())
                .build();
        component.inject(this);

        initView();
        initData();
        initListener();

    }

    private void initView() {
        mPresenter.attachView(this);

        topBar.setTextTitle(getString(R.string.estate));
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        topBar.setImageViewRight(R.drawable.ic_add_transparent);
    }

    protected void initData() {
        mPresenter.onCreate();
    }

    protected void initListener() {
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                openDrawer();
            }

            @Override
            public void onRightButtonOneClicked() {
                Intent intent = new Intent(WarehouseEstateActivity.this, CreateWarehouseEstateActivity.class);
                startActivityForResult(intent, KEY_REQUEST_CREATE_WAREHOUSE);
            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getAllWarehouseEstate();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public void showLoading() {
        showDialog("Loading...");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public boolean isRefreshing() {
        return mRefreshLayout.isRefreshing();
    }

    @Override
    public void loadAllWarehourseEstateSuccess() {
        setupRecycleView();

        if (isRefreshing())
            mRefreshLayout.setRefreshing(false);

        hideLoading();
    }

    @Override
    public void loadAllWarehourseEstateFailed() {
        if (isRefreshing())
            mRefreshLayout.setRefreshing(false);
        hideLoading();
    }

    @Override
    public void deleteWarehouseEstateSuccess() {
        mPresenter.getAllWarehouseEstate();
    }

    @Override
    public void deleteWarehouseEstateFailed() {
        showErrorDialog(getString(R.string.delete_warehouse_estate_failed));
    }

    private void setupRecycleView() {
        if (mAdapter == null) {
            mAdapter = new WarehouseEstateAdapter(mPresenter.getList(), WarehouseEstateActivity.this);
            mAdapter.setOnItemClickListener(this);
            mAdapter.setListener(new WarehouseEstateAdapter.OnClick() {
                @Override
                public void onDelete(int position) {
                    if (position < mPresenter.getList().size()) {
                        WarehouseEstateItem item = (WarehouseEstateItem) mPresenter.getList().get(position);
                        WarehouseEstate warehouseEstate = item.warehouseEstate;

                        if (warehouseEstate != null) {
                            final String warehouseId = warehouseEstate.id;

                            if (warehouseId != null) {
                                String description = getString(R.string.question_delete) + " " + warehouseEstate.name + " ?";
                                showConfirmDialog(getString(R.string.app_name), description, new DialogPositiveNegative.IPositiveNegativeDialogListener() {
                                    @Override
                                    public void onIPositiveNegativeDialogAnswerPositive(DialogPositiveNegative dialog) {
                                        dialog.dismiss();
                                        mPresenter.deleteWarehouseId(warehouseId);
                                    }

                                    @Override
                                    public void onIPositiveNegativeDialogAnswerNegative(DialogPositiveNegative dialog) {
                                        dialog.dismiss();
                                    }
                                });

                            }
                        }

                    }
                }

            });

            mRecycleWarehourse.setLayoutManager(
                    new LinearLayoutManager(WarehouseEstateActivity.this, LinearLayoutManager.VERTICAL, false));
            mRecycleWarehourse.setHasFixedSize(true);
            mRecycleWarehourse.setAdapter(mAdapter);

        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(WarehouseEstateActivity.this, EstateActivity.class);
        WarehouseEstateItem item = (WarehouseEstateItem) mPresenter.getList().get(position);
        WarehouseEstate warehouseEstate = item.warehouseEstate;
        String warehouseId = "";
        if (warehouseEstate != null) {
            warehouseId = warehouseEstate.id;
        }

        intent.putExtra(IEstateView.KEY_WAREHOUSE_ID, warehouseId);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KEY_REQUEST_CREATE_WAREHOUSE) {
            if (resultCode == Activity.RESULT_OK) {
                mPresenter.getAllWarehouseEstate();
            }

        }
    }
}
