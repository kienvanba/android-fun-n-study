package com.horical.gito.mvp.salary.mySalary.list.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.salary.dto.mySalary.ListMySalaryDto;
import com.horical.gito.mvp.salary.dto.mySalary.MySalaryDto;
import com.horical.gito.mvp.salary.mySalary.list.view.IListMySalaryView;

import java.util.ArrayList;
import java.util.List;

public class ListMySalaryPresenter extends BasePresenter implements IListMySalaryPresenter {

    public void attachView(IListMySalaryView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IListMySalaryView getView() {
        return (IListMySalaryView) getIView();
    }

    private List<ListMySalaryDto> mMySalaries;

    private void setDummyData() {
        getMySalaries().add(new ListMySalaryDto("01", "2017", 100));
        getMySalaries().add(new ListMySalaryDto("02", "2017", 200));
        getMySalaries().add(new ListMySalaryDto("03", "2017", 300));
        getMySalaries().add(new ListMySalaryDto("04", "2017", 400));
        getMySalaries().add(new ListMySalaryDto("05", "2017", 500));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        setDummyData();
    }

    @Override
    public List<ListMySalaryDto> getMySalaries() {
        if (mMySalaries == null) {
            mMySalaries = new ArrayList<>();
        }
        return mMySalaries;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
