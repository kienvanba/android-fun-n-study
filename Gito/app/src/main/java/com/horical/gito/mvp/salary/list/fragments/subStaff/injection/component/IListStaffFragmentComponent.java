package com.horical.gito.mvp.salary.list.fragments.subStaff.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.list.fragments.subStaff.injection.module.ListStaffFragmentModule;
import com.horical.gito.mvp.salary.list.fragments.subStaff.view.ListStaffFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ListStaffFragmentModule.class)
public interface IListStaffFragmentComponent {
    void inject(ListStaffFragment activity);
}
