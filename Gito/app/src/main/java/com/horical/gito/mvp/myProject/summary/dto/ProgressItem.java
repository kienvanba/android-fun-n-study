package com.horical.gito.mvp.myProject.summary.dto;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.Progress;
import com.horical.gito.model.Version;
import com.horical.gito.mvp.myProject.summary.adapter.SummaryAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nhonvt.dk on 3/21/17
 */

public class ProgressItem implements IRecyclerItem {

    private List<Progress> progress;
    private List<Version> versions;
    private int totalRemain;
    private int totalResolved;
    private int totalNeedToTest;

    public ProgressItem(List<Progress> progress) {
        this.progress = progress;
        versions = new ArrayList<>();

        if (this.progress == null) {
            this.progress = new ArrayList<>();
        }

        boolean existed = false;
        for (Progress prg : this.progress) {
            int total = prg.getTotal();
            int status = prg.getStatus();
            if (status == AppConstants.TASK_STATUS_OPEN
                    || status == AppConstants.TASK_STATUS_DOING
                    || status == AppConstants.TASK_STATUS_PENDING) {
                totalRemain += prg.getTotal();
            } else if (status == AppConstants.TASK_STATUS_CLOSED
                    || status == AppConstants.TASK_STATUS_REJECTED) {
                totalResolved += prg.getTotal();
            } else {
                totalNeedToTest += prg.getTotal();
            }

            Version version = prg.getVersion();

            if (versions.isEmpty()) {
                version.increaseTotalTask(total);
                if (status == AppConstants.TASK_STATUS_CLOSED
                        || status == AppConstants.TASK_STATUS_REJECTED) {
                    version.increaseResolvedTask(total);
                }
                versions.add(version);
            } else {
                for (Version v : versions) {
                    String s1 = version.get_id();
                    String s2 = v.get_id();
                    if (s1 != null && s2 != null
                            && s1.equalsIgnoreCase(s2)) {
                        v.increaseTotalTask(total);
                        if (status == AppConstants.TASK_STATUS_CLOSED
                                || status == AppConstants.TASK_STATUS_REJECTED) {
                            v.increaseResolvedTask(total);
                        }
                        existed = true;
                    }
                }

                if (!existed) {
                    version.increaseTotalTask(total);
                    if (status == AppConstants.TASK_STATUS_CLOSED
                            || status == AppConstants.TASK_STATUS_REJECTED) {
                        version.increaseResolvedTask(total);
                    }
                    versions.add(version);
                } else existed = false;
            }
        }
    }

    public List<Progress> getProgress() {
        return progress;
    }

    public void setProgress(List<Progress> progress) {
        this.progress = progress;
    }

    public List<Version> getVersions() {
        return versions;
    }

    public int getTotalRemain() {
        return totalRemain;
    }

    public void setTotalRemain(int totalRemain) {
        this.totalRemain = totalRemain;
    }

    public int getTotalResolved() {
        return totalResolved;
    }

    public void setTotalResolved(int totalResolved) {
        this.totalResolved = totalResolved;
    }

    public int getTotalNeedToTest() {
        return totalNeedToTest;
    }

    public void setTotalNeedToTest(int totalNeedToTest) {
        this.totalNeedToTest = totalNeedToTest;
    }

    public int getTotal() {
        return totalRemain + totalNeedToTest + totalResolved;
    }

    @Override
    public int getItemViewType() {
        return SummaryAdapter.SUMMARY_PROGRESS_TYPE;
    }
}
