package com.horical.gito.model.estate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.model.Company;
import com.horical.gito.model.TokenFile;
import com.horical.gito.model.User;

import java.io.Serializable;
import java.util.List;

/**
 * Created by hoangsang on 4/12/17.
 */

public class WarehouseEstate implements Serializable {

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("creatorId")
    @Expose
    public String creatorId;

    @SerializedName("companyId")
    @Expose
    public Company companyId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("avatar")
    @Expose
    public TokenFile avatar;

    @SerializedName("userList")
    @Expose
    public List<User> userList;

    @SerializedName("desc")
    @Expose
    public String desc;

    @SerializedName("createdDate")
    @Expose
    public String createdDate;

    @SerializedName("itemCount")
    @Expose
    public int itemCount;

    @SerializedName("modifiedDate")
    @Expose
    public String modifiedDate;

    @SerializedName("image")
    @Expose
    public List<String> image;

}
