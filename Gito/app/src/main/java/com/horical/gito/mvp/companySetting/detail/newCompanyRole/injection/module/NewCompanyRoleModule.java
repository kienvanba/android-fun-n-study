package com.horical.gito.mvp.companySetting.detail.newCompanyRole.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.presenter.NewCompanyRolePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class NewCompanyRoleModule {
    @PerActivity
    @Provides
    NewCompanyRolePresenter provideNewCompanyRolePresenter() {
        return new NewCompanyRolePresenter();
    }
}
