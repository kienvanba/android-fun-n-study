package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/21/17.
 */

public class Id implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("displayName")
    @Expose
    private String displayName;

    @SerializedName("avatar")
    @Expose
    private TokenFile avatar;

    @SerializedName("level")
    @Expose
    private Level level;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDisplayName() {
        if(displayName == null || displayName.isEmpty()){
            return "";
        }
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public TokenFile getAvatar() {
        if(avatar == null){
            return new TokenFile();
        }
        return avatar;
    }

    public void setAvatar(TokenFile avatar) {
        this.avatar = avatar;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }
}
