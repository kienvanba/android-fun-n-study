package com.horical.gito.mvp.roomBooking.detail.fragment.week.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.roomBooking.detail.fragment.week.presenter.WeekPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by QuangHai on 08/11/2016.
 */
@Module
public class WeekModule {
    @PerFragment
    @Provides
    WeekPresenter provideWeekPresenter() {
        return new WeekPresenter();
    }
}
