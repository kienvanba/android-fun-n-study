package com.horical.gito.mvp.estate.adding.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.estate.adding.presenter.CreateWarehouseEstatePresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hoangsang on 4/13/17.
 */

@Module
public class CreateWarehouseEstateModule {
    @PerActivity
    @Provides
    CreateWarehouseEstatePresenter provideCreateWarehouseEstatePresenter() {
        return new CreateWarehouseEstatePresenter();
    }
}
