package com.horical.gito.mvp.leaving.confirm.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.leaving.confirm.injection.module.LeavingConfirmModule;
import com.horical.gito.mvp.leaving.confirm.view.LeavingConfirmActivity;

import dagger.Component;

/**
 * Created by Dragonoid on 1/17/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = LeavingConfirmModule.class)
public interface LeavingConfirmComponent {
    public void inject (LeavingConfirmActivity activity);
}
