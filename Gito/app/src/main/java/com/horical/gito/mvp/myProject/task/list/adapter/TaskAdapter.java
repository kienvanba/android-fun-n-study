package com.horical.gito.mvp.myProject.task.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Task;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskHolder> {

    private Context mContext;
    private List<Task> mList;
    private OnItemClickListener mOnItemClickListener;

    public TaskAdapter(Context context, List<Task> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_task, parent, false);
        return new TaskHolder(view);
    }

    public List<Task> getList() {
        return mList;
    }

    public void setList(List<Task> mList) {
        this.mList = mList;
    }

    @Override
    public void onBindViewHolder(TaskHolder holder, final int position) {
        Task task = mList.get(position);
        holder.tvTitle.setText("#" + task.getTaskId() + ": " + task.getTitle());
        holder.tvContent.setText(task.getDesc());
        switch (task.getTaskType()){
            case 0:
                holder.imgTaskType.setImageResource(R.drawable.ic_task);
                break;
            case 1:
                holder.imgTaskType.setImageResource(R.drawable.ic_bug);
                break;
            case 2:
                holder.imgTaskType.setImageResource(R.drawable.ic_feedback);
                break;
            case 3:
                holder.imgTaskType.setImageResource(R.drawable.ic_question);
                break;
            case 4:
                holder.imgTaskType.setImageResource(R.drawable.ic_inform);
                break;
            case 5:
                holder.imgTaskType.setImageResource(R.drawable.ic_request);
                break;
        }
        switch (task.getPriority()){
            case 0:
                holder.tvPriority.setText("Next Phase");
                holder.tvPriority.setBackgroundResource(R.drawable.bg_grey_btn);
                break;
            case 1:
                holder.tvPriority.setText("Low");
                holder.tvPriority.setBackgroundResource(R.drawable.bg_grey_conner_btn);
                break;
            case 2:
                holder.tvPriority.setText("Normal");
                holder.tvPriority.setBackgroundResource(R.drawable.bg_green_conner_btn);
                break;
            case 3:
                holder.tvPriority.setText("High");
                holder.tvPriority.setBackgroundResource(R.drawable.bg_green_tree_conner_btn);
                break;
            case 4:
                holder.tvPriority.setText("Urgent");
                holder.tvPriority.setBackgroundResource(R.drawable.bg_orange_conner_btn);
                break;
            case 5:
                holder.tvPriority.setText("Immediate");
                holder.tvPriority.setBackgroundResource(R.drawable.bg_red_conner_btn);
                break;
        }
        switch (task.getStatus()){
            case 0:
                holder.tvStatus.setText("Open");
                holder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.orange_new));
                break;
            case 1:
                holder.tvStatus.setText("Doing");
                holder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.green_doing));
                break;
            case 2:
                holder.tvStatus.setText("Done");
                holder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.blue_cross));
                break;
            case 3:
                holder.tvStatus.setText("Closed");
                holder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.black_opacity));
                break;
            case 4:
                holder.tvStatus.setText("Pending");
                holder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.grey_next));
                break;
            case 5:
                holder.tvStatus.setText("Rejected");
                holder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.red));
                break;
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onClickListener(mList.get(position));
            }
        });

        holder.imvDeleteTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDeleteListener(mList.get(position).get_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class TaskHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.img_task_type)
        ImageView imgTaskType;

        @Bind(R.id.tv_title_task)
        TextView tvTitle;

        @Bind(R.id.tv_priority_task)
        TextView tvPriority;

        @Bind(R.id.tv_status_task)
        TextView tvStatus;

        @Bind(R.id.tv_content_task)
        TextView tvContent;

        @Bind(R.id.imv_delete_task)
        ImageView imvDeleteTask;

        View view;

        public TaskHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;

    }

    public interface OnItemClickListener {
        void onClickListener(Task task);
        void onDeleteListener(String taskID);
    }
}
