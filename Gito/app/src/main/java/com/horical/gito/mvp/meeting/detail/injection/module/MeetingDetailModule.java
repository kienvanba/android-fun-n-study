package com.horical.gito.mvp.meeting.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.meeting.detail.presenter.MeetingDetailPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lemon on 11/24/2016.
 */

@Module
public class MeetingDetailModule {
    @PerActivity
    @Provides
    MeetingDetailPresenter provideMeetingPresenter(){
        return new MeetingDetailPresenter();
    }
}
