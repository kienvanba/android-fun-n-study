package com.horical.gito.interactor.api.network;

import com.horical.gito.interactor.api.request.file.UploadFileResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface FileServices {

    // UPLOAD FILE
    @Multipart
    @POST("fileitem")
    Call<UploadFileResponse> uploadFile(
            @Header("token") String token,
            @Part MultipartBody.Part file,
            @Part("status") RequestBody status
    );
}