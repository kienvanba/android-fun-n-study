package com.horical.gito.mvp.myProject.document.view;

import com.horical.gito.base.IView;

public interface IDocumentView extends IView {
    void showLoading();

    void hideLoading();

    void getAllDocumentSuccess();

    void getAllDocumentFailure(String err);
}
