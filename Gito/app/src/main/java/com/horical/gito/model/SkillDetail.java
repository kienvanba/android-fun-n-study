package com.horical.gito.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SkillDetail implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("weight")
    @Expose
    private double weight;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
