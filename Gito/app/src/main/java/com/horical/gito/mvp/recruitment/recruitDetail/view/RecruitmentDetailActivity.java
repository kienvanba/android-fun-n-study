package com.horical.gito.mvp.recruitment.recruitDetail.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.model.Recruitment;
import com.horical.gito.mvp.dialog.dialogInputEdit.InputEditDialog;
import com.horical.gito.mvp.dialog.dialogSelectDate.SelectDateDialog;
import com.horical.gito.mvp.recruitment.recruitDetail.adapter.InterviewerAdapter;
import com.horical.gito.mvp.recruitment.recruitDetail.injection.component.DaggerRecruitmentDetailComponent;
import com.horical.gito.mvp.recruitment.recruitDetail.injection.component.RecruitmentDetailComponent;
import com.horical.gito.mvp.recruitment.recruitDetail.injection.module.RecruitmentDetailModule;
import com.horical.gito.mvp.recruitment.recruitDetail.presenter.RecruitmentDetailPresenter;
import com.horical.gito.mvp.recruitment.view.RecruitmentActivity;
import com.horical.gito.utils.DateUtils;

import java.util.Date;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RecruitmentDetailActivity extends BaseActivity implements IRecruitmentDetailView, View.OnClickListener {

    @Bind(R.id.imv_back)
    ImageView imvBack;

    @Bind(R.id.tv_title)
    TextView tvTitle;

    @Bind(R.id.tv_save)
    TextView tvSave;

    @Bind(R.id.rcv_interview)
    RecyclerView mLvInterview;

    @Bind(R.id.imv_add_interviewer)
    ImageView imvAddInterviewer;

    @Bind(R.id.imv_add_candidate)
    ImageView imvAddCandidate;

    @Bind(R.id.tv_title_recruit)
    TextView tvTitleRecruit;

    @Bind(R.id.tv_interview_date)
    TextView tvDateInterview;

    @Bind(R.id.tv_skill_recruitment)
    TextView tvSkillRecruit;

    @Bind(R.id.imv_edit_title)
    ImageView imvEditTitle;

    @Bind(R.id.imv_edit_date)
    ImageView imvEditDate;

    @Bind(R.id.scr_view)
    NestedScrollView scrView;

    @Bind(R.id.rcv_candidate)
    RecyclerView mLvCandidate;

    @Inject
    RecruitmentDetailPresenter mPresenter;

    Recruitment mRecruitment;

    InterviewerAdapter adapterInterviewer;

    private InputEditDialog inputDialog;
    private SelectDateDialog dateDialog;

    private int getLayoutId() {
        return R.layout.activity_new_recruitment;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);

        RecruitmentDetailComponent component = DaggerRecruitmentDetailComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .recruitmentDetailModule(new RecruitmentDetailModule())
                .build();


        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        mRecruitment = (Recruitment) getIntent().getSerializableExtra(RecruitmentActivity.KEY_ID_RECRUIT);

        if (mRecruitment == null) {
            tvTitle.setText(getString(R.string.new_recruit));
            tvSave.setText(getString(R.string.save));
        } else {
            tvTitle.setText(getString(R.string.detail_recruit));
            mPresenter.setListInterview(mRecruitment.getInterviewers());
            tvSave.setText(getString(R.string.interview));
            tvTitleRecruit.setText(mRecruitment.getTitle());
            tvDateInterview.setText(DateUtils.formatFullDate(mRecruitment.getInterviewDate()));
            tvSkillRecruit.setText(mRecruitment.getSkill().getTitle());
            mPresenter.setDateInterview(mRecruitment.getInterviewDate());
        }

        adapterInterviewer = new InterviewerAdapter(this, mPresenter.getListInterview(), new InterviewerAdapter.OnItemClickListener() {
            @Override
            public void onDeleteItem(int positionItem) {

            }

            @Override
            public void onItemClick(int position) {

            }
        });
        mLvInterview.setLayoutManager(new LinearLayoutManager(this));
        mLvInterview.setHasFixedSize(true);
        mLvInterview.setAdapter(adapterInterviewer);
        mLvInterview.setNestedScrollingEnabled(false);

        mLvCandidate.setLayoutManager(new LinearLayoutManager(this));
        mLvCandidate.setHasFixedSize(true);
        mLvCandidate.setAdapter(adapterInterviewer);
        mLvCandidate.setNestedScrollingEnabled(false);

        focusOnView();

    }

    protected void initListener() {
        imvBack.setOnClickListener(this);
        imvEditTitle.setOnClickListener(this);
        imvEditDate.setOnClickListener(this);
        imvAddInterviewer.setOnClickListener(this);
        imvAddCandidate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_back:
                finish();
                break;
            case R.id.imv_edit_title:
                if (inputDialog == null || !inputDialog.isShowing()) {
                    inputDialog = new InputEditDialog(this, tvTitleRecruit.getText().toString(), new InputEditDialog.OnDoneListener() {
                        @Override
                        public void onDone(String text) {
                            tvTitleRecruit.setText(text);
                            inputDialog.dismiss();
                        }
                    });
                    inputDialog.show();
                }
                break;
            case R.id.imv_edit_date:
                if (dateDialog == null || dateDialog.getDialog() == null || !dateDialog.getDialog().isShowing()) {
                    dateDialog = new SelectDateDialog(this, mPresenter.getDateInterview(), new SelectDateDialog.OnDoneClickListener() {
                        @Override
                        public void onDone(Date date) {
                            mPresenter.setDateInterview(date);
                            tvDateInterview.setText(DateUtils.formatFullDate(date));
                            focusOnView();
                        }
                    });
                    dateDialog.show(getSupportFragmentManager(), "DialogDate");

                }
                break;
            case R.id.imv_add_interviewer:
                break;
            case R.id.imv_add_candidate:
                break;


        }
    }


    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void dismissLoading() {
        dismissDialog();
    }

    private void focusOnView() {
        scrView.post(new Runnable() {
            @Override
            public void run() {
                scrView.scrollTo(0, scrView.getTop());
            }
        });
    }
}