package com.horical.gito.mvp.myProject.setting.fragment.information.view;

import com.horical.gito.base.IView;
import com.horical.gito.model.TokenFile;

public interface IInformationFragment extends IView {
    void getProjectSuccess();

    void showLoading();

    void hideLoading();

    void uploadFileSuccess(TokenFile avatar);
}
