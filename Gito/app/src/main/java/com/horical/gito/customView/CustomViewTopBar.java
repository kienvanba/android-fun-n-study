package com.horical.gito.customView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.utils.LogUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CustomViewTopBar extends FrameLayout implements View.OnClickListener, ICustomViewTopBar {

    public static final String TAG = LogUtils.makeLogTag(CustomViewTopBar.class);

    public static final int LEFT_MENU = 0;
    public static final int LEFT_BACK = 1;

    public static final int DRAWABLE_FILTER = R.drawable.ic_filter;
    public static final int DRAWABLE_ADD_TRANSPARENT = R.drawable.ic_add_transparent;
    public static final int DRAWABLE_ADD_WHITE = R.drawable.ic_add_white;
    public static final int DRAWABLE_USER_REPORT = R.drawable.ic_my_page_report;
    public static final int DRAWABLE_USER_MONEY = R.drawable.ic_user_money;
    public static final int DRAWABLE_MONEY = R.drawable.ic_money;
    public static final int DRAWABLE_SEARCH = R.drawable.ic_search_white;
    public static final int DRAWABLE_USER_CONFIRM = R.drawable.ic_user_confirm;
    public static final int DRAWABLE_LAMP = R.drawable.ic_lamp_white;
    public static final int DRAWABLE_PHONE = R.drawable.ic_phone_white;
    public static final int DRAWABLE_KEY = R.drawable.ic_key_white;
    public static final int DRAWABLE_SUMMARY_WHITE = R.drawable.ic_summary_white;

    @Bind(R.id.ln_left)
    LinearLayout lnLeft;

    @Bind(R.id.ln_right)
    LinearLayout lnRight;

    @Bind(R.id.imv_left)
    ImageView imvLeft;

    @Bind(R.id.tv_title)
    TextView tvTitle;

    @Bind(R.id.tv_left)
    TextView tvLeft;

    @Bind(R.id.tv_right)
    TextView tvRight;

    @Bind(R.id.tv_status_user_count)
    TextView tvStatusUserCount;

    @Bind(R.id.imv_right_button_one)
    ImageView imvRightButtonOne;

    @Bind(R.id.imv_right_button_two)
    ImageView imvRightButtonTwo;

    @Bind(R.id.imv_right_button_three)
    ImageView imvRightButtonThree;

    OnItemClickListener onItemClickListener;
    OnLeftRightClickListener onLeftRightClickListener;

    public void setOnClickListener(OnItemClickListener callback) {
        onItemClickListener = callback;
    }

    public void setOnLeftRightClickListener(OnLeftRightClickListener callback) {
        onLeftRightClickListener = callback;
    }

    public interface OnLeftRightClickListener {

        void onLeftClicked();

        void onRightClicked();
    }

    /**
     * Left : Menu or Back
     * Right flow: [Three] -> [Two] -> [One] -> [End of Screen]
     */
    public interface OnItemClickListener {

        void onLeftClicked();

        void onRightButtonOneClicked();

        void onRightButtonTwoClicked();

        void onRightButtonThreeClicked();
    }

    public CustomViewTopBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        View view = LayoutInflater.from(context).inflate(
                R.layout.layout_topbar_base_activity,
                this,
                false
        );
        ButterKnife.bind(this, view);

        lnLeft.setOnClickListener(this);
        lnRight.setOnClickListener(this);

        imvRightButtonOne.setOnClickListener(this);
        imvRightButtonTwo.setOnClickListener(this);
        imvRightButtonThree.setOnClickListener(this);
        this.addView(view);
    }

    @Override
    public void setTextTitle(String title) {
        tvTitle.setText(title);
    }

    @Override
    public void setTextViewLeft(String text) {
        tvLeft.setVisibility(VISIBLE);
        tvLeft.setText(text);
    }

    @Override
    public void setTextViewRight(String text) {
        tvRight.setVisibility(VISIBLE);
        tvRight.setText(text);
    }

    /**
     * 0 : Menu
     * 1 : Back
     *
     * @param type Type Resource ImageView Left
     */
    public void setImageViewLeft(int type) {
        if (type == LEFT_MENU) {
            imvLeft.setImageResource(R.drawable.ic_menu);
        } else {
            imvLeft.setImageResource(R.drawable.ic_back_white);
        }
    }

    /**
     * @param one Type Resource ImageView
     */
    @Override
    public void setImageViewRight(int one) {
        imvRightButtonOne.setVisibility(VISIBLE);
        imvRightButtonOne.setImageResource(one);
    }

    /**
     * @param two Type Resource ImageView
     * @param one Type Resource ImageView
     */
    @Override
    public void setImageViewRight(int two, int one) {
        imvRightButtonTwo.setVisibility(VISIBLE);
        imvRightButtonTwo.setImageResource(two);

        setImageViewRight(one);
    }

    /**
     * @param three Type Resource ImageView
     * @param two   Type Resource ImageView
     * @param one   Type Resource ImageView
     */
    @Override
    public void setImageViewRight(int three, int two, int one) {
        // Three
        imvRightButtonThree.setVisibility(VISIBLE);
        imvRightButtonThree.setImageResource(three);

        setImageViewRight(two, one);
    }

    /**
     * @param one GONE or VISIBLE or INVISIBLE
     */
    @Override
    public void setImageViewRightVisible(int one) {
        imvRightButtonOne.setVisibility(one);
    }

    /**
     * @param two GONE or VISIBLE or INVISIBLE
     * @param one GONE or VISIBLE or INVISIBLE
     */
    @Override
    public void setImageViewRightVisible(int two, int one) {
        setImageViewRightVisible(one);
        imvRightButtonTwo.setVisibility(two);
    }

    /**
     * @param three GONE or VISIBLE or INVISIBLE
     * @param two   GONE or VISIBLE or INVISIBLE
     * @param one   GONE or VISIBLE or INVISIBLE
     */
    @Override
    public void setImageViewRightVisible(int three, int two, int one) {
        setImageViewRightVisible(two, one);
        imvRightButtonThree.setVisibility(three);
    }

    /**
     * Use Only Leaving Activity
     *
     * @param count User confirm count
     */
    @Override
    public void setTextUserCount(int count) {
        tvStatusUserCount.setVisibility(VISIBLE);
        tvStatusUserCount.setText(String.valueOf(count));
    }

    /**
     * User For Dialog
     *
     * @param textLeft  Cancel
     * @param title     Title Dialog
     * @param textRight Save or Done
     */
    public void setTextAll(String textLeft, String title, String textRight) {
        setTextTitle(title);
        setTextViewLeft(textLeft);
        setTextViewRight(textRight);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == lnLeft.getId()) {
            if (onLeftRightClickListener != null) onLeftRightClickListener.onLeftClicked();
            if (onItemClickListener != null) onItemClickListener.onLeftClicked();
        } else if (id == lnRight.getId()) {
            if (onLeftRightClickListener != null) onLeftRightClickListener.onRightClicked();
        } else if (id == imvRightButtonOne.getId()) {
            if (onItemClickListener != null) onItemClickListener.onRightButtonOneClicked();
        } else if (id == imvRightButtonTwo.getId()) {
            if (onItemClickListener != null) onItemClickListener.onRightButtonTwoClicked();
        } else if (id == imvRightButtonThree.getId()) {
            if (onItemClickListener != null) onItemClickListener.onRightButtonThreeClicked();
        }
    }
}
