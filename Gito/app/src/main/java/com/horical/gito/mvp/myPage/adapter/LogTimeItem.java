package com.horical.gito.mvp.myPage.adapter;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.LogTime;

public class LogTimeItem implements IRecyclerItem {

    private LogTime logTime;

    public LogTimeItem(LogTime logTime) {
        this.logTime = logTime;
    }

    public LogTime getLogTime() {
        return logTime;
    }

    public void setLogTime(LogTime logTime) {
        this.logTime = logTime;
    }

    @Override
    public int getItemViewType() {
        return 0;
    }
}
