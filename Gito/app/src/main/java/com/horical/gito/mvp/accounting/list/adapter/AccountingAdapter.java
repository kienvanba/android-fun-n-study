package com.horical.gito.mvp.accounting.list.adapter;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.model.Accounting;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class AccountingAdapter extends RecyclerView.Adapter<AccountingAdapter.AccountingHolder> {

    private List<Accounting> mList;
    private Context mContext;
    OnItemClickListener mOnItemClickListener;

    public AccountingAdapter(Context context, List<Accounting> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public AccountingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_accounting, parent, false);
        return new AccountingHolder(view);
    }

    @Override
    public void onBindViewHolder(AccountingHolder holder, final int position) {

        Accounting accounting = mList.get(position);
        holder.tvTitle.setText(accounting.title);
        holder.tvAmount.setText(String.format(String.valueOf(accounting.amount)));
        holder.tvContent.setText(accounting.desc);
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDelete(position);
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(position);
            }
        });
    }

    public List<Accounting> getListAccounting() {
        return mList;
    }

    public void setListAccounting(List<Accounting> mList) {
        this.mList = mList;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class AccountingHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_accounting_main_title)
        GitOTextView tvTitle;

        @Bind(R.id.imv_accounting_delete)
        ImageView imgDelete;

        @Bind(R.id.tv_accounting_amount_main)
        GitOTextView tvAmount;

        @Bind(R.id.tv_accounting_report_type)
        GitOTextView tvReportType;

        @Bind(R.id.tv_accounting_content_main)
        GitOTextView tvContent;

        View view;


        public AccountingHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }


    public void setOnItemClickListener(OnItemClickListener callBack) {
        this.mOnItemClickListener = callBack;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDelete(int position);
    }
}
