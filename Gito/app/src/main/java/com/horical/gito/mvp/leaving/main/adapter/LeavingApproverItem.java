package com.horical.gito.mvp.leaving.main.adapter;

import android.media.Image;

import com.horical.gito.base.IRecyclerItem;

/**
 * Created by Dragonoid on 12/12/2016.
 */

public class LeavingApproverItem implements IRecyclerItem {

    Boolean mChecked;
    String mName;
    public int mOrder;
    Image mImage;
    String mPosition;

    public LeavingApproverItem(String name,int order){
        mChecked = false;
        mName  = name;
        mOrder=order;
    }
    public LeavingApproverItem(String name,String position,Image image,int order){
        mChecked=false;
        mName = name;
        mImage = image;
        mPosition=position;
        mOrder=order;
    }

    @Override
    public int getItemViewType() {
        return 0;
    }
    public String getName(){
        return  mName;
    }
    public boolean isChecked() {return mChecked;}
    public void setChecked(){mChecked=true;}
    public void setUnchecked() {mChecked=false;}
    public int getPosition() {return mOrder;}
}
