package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Lemon on 3/7/2017
 */

public class RoomBooking implements Serializable {

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("creatorId")
    @Expose
    public CreatorId creatorId;

    @SerializedName("companyId")
    @Expose
    public String companyId;

    @SerializedName("roomId")
    @Expose
    public String roomId;

    @SerializedName("startDate")
    @Expose
    public String startDate;

    @SerializedName("endDate")
    @Expose
    public String endDate;

    @SerializedName("duration")
    @Expose
    public Integer duration;

    @SerializedName("createdDate")
    @Expose
    public String createdDate;

    @SerializedName("desc")
    @Expose
    public String desc;
}
