package com.horical.gito.mvp.myProject.setting.fragment.activity.version.view;

import com.horical.gito.base.IView;

public interface IVersionView extends IView{
    void showLoading();
    void hideLoading();
    void onRequestSuccess();
}
