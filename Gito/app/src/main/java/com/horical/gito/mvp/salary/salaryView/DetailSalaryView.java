package com.horical.gito.mvp.salary.salaryView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.horical.gito.R;
import com.horical.gito.widget.scrollview.StickyScrollView;

import butterknife.ButterKnife;

public class DetailSalaryView extends StickyScrollView {

    public DetailSalaryView(Context context) {
        super(context);
    }

    public DetailSalaryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_detail_salary, this, false);
        ButterKnife.bind(this,view);
        this.addView(view);
    }
}
