package com.horical.gito.mvp.companySetting.detail.newLevel.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface INewLevelView extends IView {
    void createLevelSuccess();
    void createLevelFailure(String err);
    void updateLevelSuccess();
    void updateLevelFailure(String err);
}
