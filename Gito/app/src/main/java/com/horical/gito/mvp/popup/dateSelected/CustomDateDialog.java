package com.horical.gito.mvp.popup.dateSelected;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.aigestudio.wheelpicker.widgets.WheelDatePicker;
import com.horical.gito.R;
import com.horical.gito.base.BaseDialog;
import com.horical.gito.mvp.logTime.filter.adapter.LogTimeFilterAdapter;

import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;

import static com.horical.gito.MainApplication.mContext;

public class CustomDateDialog extends BaseDialog implements View.OnClickListener {
    @Bind(R.id.view_start_date)
    WheelDatePicker viewStartDate;

    @Bind(R.id.view_end_date)
    WheelDatePicker viewEndDate;

    @Bind(R.id.tv_cancel)
    TextView tvCancel;

    @Bind(R.id.btn_save)
    Button btnSave;

    private DateDto mDate;

    public CustomDateDialog(Context context, DateDto dateDto) {
        super(context);
        mDate = dateDto;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_custom_date;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();

        tvCancel.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        viewStartDate.setCyclic(true);
        viewStartDate.setSelectedItemTextColor(mContext.getResources().getColor(R.color.black));
        viewStartDate.setIndicator(true);
        viewStartDate.setCurtain(true);
        viewStartDate.setAtmospheric(true);

        viewEndDate.setCyclic(true);
        viewEndDate.setSelectedItemTextColor(mContext.getResources().getColor(R.color.black));
        viewEndDate.setIndicator(true);
        viewEndDate.setCurtain(true);
        viewEndDate.setAtmospheric(true);


        if (mDate != null && mDate.getStart() != null && mDate.getEnd() != null) {
            Calendar calendarStart = Calendar.getInstance();
            calendarStart.setTime(mDate.getStart());
            viewStartDate.setSelectedYear(calendarStart.get(Calendar.YEAR));
            viewStartDate.setSelectedMonth(calendarStart.get(Calendar.MONTH) + 1);
            viewStartDate.setSelectedDay(calendarStart.get(Calendar.DATE));

            Calendar calendarEnd = Calendar.getInstance();
            calendarEnd.setTime(mDate.getEnd());
            viewEndDate.setSelectedYear(calendarEnd.get(Calendar.YEAR));
            viewEndDate.setSelectedMonth(calendarEnd.get(Calendar.MONTH) + 1);
            viewEndDate.setSelectedDay(calendarEnd.get(Calendar.DATE));

        } else {

            Calendar dateCalendar = Calendar.getInstance();
            dateCalendar.set(viewStartDate.getCurrentYear(), viewStartDate.getCurrentMonth() - 1, viewStartDate.getCurrentDay(), 0, 0);

            mDate = new DateDto();
            mDate.setStart(dateCalendar.getTime());
            mDate.setEnd(dateCalendar.getTime());
        }

        viewStartDate.setOnDateSelectedListener(new WheelDatePicker.OnDateSelectedListener() {
            @Override
            public void onDateSelected(WheelDatePicker picker, Date date) {
                if (mDate == null) {
                    mDate = new DateDto();
                }

                Calendar calendar = Calendar.getInstance();
                Date currentDate = calendar.getTime();

                if (date.after(currentDate)) {
                    viewStartDate.setSelectedYear(calendar.get(Calendar.YEAR));
                    viewStartDate.setSelectedMonth(calendar.get(Calendar.MONTH) + 1);
                    viewStartDate.setSelectedDay(calendar.get(Calendar.DATE));
                }

                mDate.setStart(date);
            }
        });

        viewEndDate.setOnDateSelectedListener(new WheelDatePicker.OnDateSelectedListener() {
            @Override
            public void onDateSelected(WheelDatePicker picker, Date date) {
                if (mDate == null) {
                    mDate = new DateDto();
                }

                Calendar calendar = Calendar.getInstance();
                Date currentDate = calendar.getTime();

                if (date.after(currentDate)) {
                    viewEndDate.setSelectedYear(calendar.get(Calendar.YEAR));
                    viewEndDate.setSelectedMonth(calendar.get(Calendar.MONTH) + 1);
                    viewEndDate.setSelectedDay(calendar.get(Calendar.DATE));
                }
                mDate.setEnd(date);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel: {
                dismiss();
                break;
            }
            case R.id.btn_save: {

                if (checkDateValid(mDate.getStart(), mDate.getEnd())) {
                    mListener.onClickSaveCustomDate(mDate);
                    dismiss();
                } else {
                    Toast.makeText(mContext, "Date invalid", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    private OnDialogClickListener mListener;

    public void setOnDialogClickListener(OnDialogClickListener listener) {
        mListener = listener;
    }

    public interface OnDialogClickListener {
        void onClickSaveCustomDate(DateDto dto);
    }

    private boolean checkDateValid(Date startDate, Date endDate) {
        try {
            return !(startDate.after(endDate));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


}
