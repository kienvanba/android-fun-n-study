package com.horical.gito.mvp.myProject.setting.fragment.activity.member.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.injection.module.UserModule;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.view.NewMemberActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = UserModule.class)
public interface UserComponent {
    void inject(NewMemberActivity activity);
}
