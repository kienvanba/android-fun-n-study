package com.horical.gito.mvp.myProject.setting.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.horical.gito.AppConstants;
import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.widget.dialog.DialogEditText;
import com.horical.gito.utils.HDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SettingRecyclerAdapter extends BaseRecyclerAdapter {
    public SettingRecyclerAdapter(Context context, List<IRecyclerItem> items) {
        super(items, context);
    }

    public OnDeleteItemClickListener listener;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case AppConstants.MEMBER_ITEM:
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_member_setting,parent,false);
                return new MemberHolder(view);
            case AppConstants.FEATURE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_feature_setting,parent,false);
                return new FeatureHolder(view);
            case AppConstants.COMPONENT_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_component_setting,parent,false);
                return new ComponentHolder(view);
            case AppConstants.VERSION_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_version_setting,parent,false);
                return new VersionHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        switch (getItemViewType(position)){
            /**   ------------Member-------------  **/
            case AppConstants.MEMBER_ITEM:
                final MemberItemBody mMemberBody = (MemberItemBody) mItems.get(position);
                final MemberHolder mMemberHolder = (MemberHolder) holder;

                mMemberHolder.mTvName.setText(mMemberBody.getMemberAndRole().getUserId().getDisplayName());

                if(mMemberBody.getMemberAndRole().getUserId().getAvatar()==null){
                    mMemberHolder.mTvNonAvatar.setText(mMemberBody.getMemberAndRole().getUserId().getDisplayName().substring(0,1));
                    mMemberHolder.progressAvatar.setVisibility(View.GONE);
                }else{
                    mMemberHolder.mTvNonAvatar.setVisibility(View.GONE);
                    String url = CommonUtils.getURL(mMemberBody.getMemberAndRole().getUserId().getAvatar());
                    ImageLoader.load(mContext, url, mMemberHolder.mImAvatar, mMemberHolder.progressAvatar);
                }

                String[] string = mContext.getResources().getStringArray(R.array.member_roles);
                ArrayList<String> list = new ArrayList<>(Arrays.asList(string));

                int selectPosition=0;
                if(!list.contains(mMemberBody.getMemberAndRole().getRoleId().roleName)){
                    list.add(mMemberBody.getMemberAndRole().getRoleId().roleName);
                }
                selectPosition = list.indexOf(mMemberBody.getMemberAndRole().getRoleId().roleName);

                final SpinnerAdapter mAdapter = new SpinnerAdapter(mContext, R.layout.item_spinner,list);
                mAdapter.setCurrentItem(selectPosition);
                mAdapter.notifyDataSetChanged();

                mMemberHolder.mSpRole.setAdapter(mAdapter);
                mMemberHolder.mSpRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mAdapter.setCurrentItem(position);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

                mMemberHolder.mBtnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItems.remove(mMemberBody);
                        notifyDataSetChanged();
                    }
                });
                break;
            /**     --------------Feature--------------    **/
            case AppConstants.FEATURE_ITEM:
                final FeatureItemBody mFeatureBody = (FeatureItemBody) mItems.get(position);
                final FeatureHolder mFeatureHolder = (FeatureHolder) holder;

                mFeatureHolder.mImIcon.setImageDrawable(mContext.getResources().getDrawable(mFeatureBody.icon));

                mFeatureHolder.mTvName.setText(mFeatureBody.featureName);

                mFeatureHolder.mCbCheck.setChecked(true);
                mFeatureHolder.mCbCheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //do something
                    }
                });
                break;
            /**   ------------Component-----------------   **/
            case AppConstants.COMPONENT_ITEM:
                final ComponentItemBody mComponentBody = (ComponentItemBody) mItems.get(position);
                ComponentHolder mComponentHolder  = (ComponentHolder) holder;

                mComponentHolder.mTvName.setText(mComponentBody.component.title);

                mComponentHolder.mBtnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onClick(position);
                    }
                });
                break;
            /**      ------------Version--------------     **/
            case AppConstants.VERSION_ITEM:
                final VersionItemBody mVersionBody = (VersionItemBody) mItems.get(position);
                final VersionHolder mVersionHolder = (VersionHolder) holder;

                mVersionHolder.mTvName.setText(mVersionBody.version.name);

                switch (mVersionBody.version.getStatus()){
                    case 1:
                        mVersionHolder.mTvStatus.setText(mContext.getResources().getString(R.string.approved));
                        mVersionHolder.mTvStatus.setTextColor(mContext.getResources().getColor(R.color.green));
                        break;
                    case 0:
                        mVersionHolder.mTvStatus.setText(mContext.getResources().getString(R.string.status_new));
                        mVersionHolder.mTvStatus.setTextColor(mContext.getResources().getColor(R.color.orange_new));
                        break;
                    default:
                        mVersionHolder.mTvStatus.setText(String.valueOf(mVersionBody.version.getStatus()));
                        break;
                }

                mVersionHolder.mTvDescription.setText(mVersionBody.version.description);

                mVersionHolder.mBtnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogEditText dialog = new DialogEditText(mContext) {
                            @Override
                            public void setSaveButtonClickListener() {
                                mVersionBody.version.description = this.editText.getText().toString();
                                mVersionHolder.mTvDescription.setText(this.editText.getText().toString());
                                this.dismiss();
                            }
                        };
                        dialog.editText.setText(mVersionBody.version.description);
                        if(mVersionBody.version.description.equals(""))dialog.setHint("Version's description");
                        dialog.show();
                    }
                });

                mVersionHolder.mBtnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onClick(position);
                    }
                });

                //-----------
                if(mVersionBody.version.startDate!=null && mVersionBody.version.getEndDate()!=null) {
                    Calendar timeLineDate = Calendar.getInstance();

                    timeLineDate.setTime(mVersionBody.version.getStartDate());
                    mVersionHolder.mTimeLine.setDateStart(new HDate(timeLineDate));

                    timeLineDate.setTime(mVersionBody.version.getEndDate());
                    mVersionHolder.mTimeLine.setDateRelease(new HDate(timeLineDate));
                }
                break;
        }
    }
    public void setOnDeleteItemClickListener(OnDeleteItemClickListener listener){
        this.listener = listener;
    }

    public interface OnDeleteItemClickListener{
        void onClick(int position);
    }
}
