package com.horical.gito.model;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Group implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("creatorId")
    @Expose
    private String creatorId;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("ProjectId")
    @Expose
    private String projectId;

    @SerializedName("groupName")
    @Expose
    private String groupName;

    @SerializedName("groupId")
    @Expose
    private String groupId;

    @SerializedName("members")
    @Expose
    private List<User> members;

    @Expose(deserialize = false, serialize = false)
    private boolean isSelected;

    public String getDisplayName() {
        if (getGroupName().isEmpty()) {
            return "";
        }
        return groupName.substring(0, 1).toUpperCase();
    }
    public String getDisplayPhotoName() {
        if (TextUtils.isEmpty(groupName)) {
            return "";
        } else {
            try {
                String CurrentString = groupName.trim();
                String[] separated = CurrentString.split(" ");
                if (separated.length == 1) {
                    return separated[0].substring(0, 2).toUpperCase();
                } else {
                    return separated[0].substring(0, 1).toUpperCase() + separated[1].substring(0, 1).toUpperCase();
                }
            } catch (Exception e) {
                return groupName.substring(0, 1).toUpperCase();
            }

        }
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getGroupName() {
        if (groupName == null) {
            groupName = "";
        }
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }
}
