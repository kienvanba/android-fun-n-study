package com.horical.gito.interactor.api.request.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.model.Feature;
import com.horical.gito.model.TokenFile;

public class UpdateProjectRequest {

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("projectId")
    @Expose
    public String projectId;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("avatar")
    @Expose
    public TokenFile avatar;

    @SerializedName("feature")
    @Expose
    public Feature feature;

    @SerializedName("content")
    @Expose
    public String content;

    @SerializedName("desc")
    @Expose
    public String desc;
}
