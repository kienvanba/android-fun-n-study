package com.horical.gito.mvp.roomBooking.list.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.room.GetAllRoomResponse;
import com.horical.gito.model.Room;
import com.horical.gito.mvp.roomBooking.list.view.IRoomBookingView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class RoomBookingPresenter extends BasePresenter implements IRoomBookingPresenter {
    private List<Room> mList;

    private static final String TAG = makeLogTag(RoomBookingPresenter.class);

    public IRoomBookingView getView() {
        return (IRoomBookingView) getIView();
    }

    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Room> getListRoomBooking() {
        return mList;
    }

    public void setListRoomBooking(List<Room> mList) {
        this.mList = mList;
    }

    @Override
    public void getAllRoomBooking() {
        getApiManager().getAllRoom(new ApiCallback<GetAllRoomResponse>() {
            @Override
            public void success(GetAllRoomResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.room);
                getView().getAllRoomBookingSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getAllRoomBookingFailure(error.message);
            }
        });
    }
}
