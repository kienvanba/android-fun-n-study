package com.horical.gito.mvp.caseStudy.detail.presenter;


import com.horical.gito.interactor.api.request.caseStudy.UpdateCaseStudyRequest;
import com.horical.gito.model.CaseStudy;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;

public interface ICaseStudyDetailPresenter {

    void createCaseStudy(CaseStudy caseStudy);

    void updateCaseStudy(CaseStudy caseStudy);

    void updateStatusCaseStudy(String id, int status, String rejectMsg);

    void uploadFile(MediaType type, File file);

    void addAttachFile(String idCaseStudy, String idAttachFile);

    void deleteAttachFile(String idCaseStudy, String idAttachFile);
}
