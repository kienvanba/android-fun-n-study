package com.horical.gito.mvp.notification.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 3/17/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<NotificationItem> mItems;
    Context mContext;
    public NotificationAdapter(Context context, List<NotificationItem> items){
        mItems = items;
        mContext = context;
    }

    public void ChangeList(List<NotificationItem> items){
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification_info,parent,false);
        return new NotificationHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NotificationHolder itemHolder = (NotificationHolder) holder;
        NotificationItem itemItem = mItems.get(position);
        itemHolder.mTittle.setText(itemItem.mTittle);
        itemHolder.mReason.setText(itemItem.mReason);
        itemHolder.mStatus.setText(itemItem.mStatus+"");
        itemHolder.mUnit.setText(itemItem.mUnit);
        if(itemItem.mStatus==5) itemHolder.setUndeletable();
    }


    public class NotificationHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.notification_tv_tittle)
        GitOTextView mTittle;
        @Bind(R.id.notification_tv_status)
        GitOTextView mStatus;
        @Bind(R.id.notification_tv_reason)
        GitOTextView mReason;
        @Bind(R.id.notification_tv_unit)
        GitOTextView mUnit;
        @Bind(R.id.notification_img_delete)
        ImageView mDelete;

        public NotificationHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
        public void setUndeletable(){
            mDelete.setVisibility(View.GONE);
        }
        public void setDeletable(){
            mDelete.setVisibility(View.VISIBLE);
        }
    }
}
