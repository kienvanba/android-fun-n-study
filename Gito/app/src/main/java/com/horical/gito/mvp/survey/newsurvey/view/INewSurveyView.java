package com.horical.gito.mvp.survey.newsurvey.view;

import com.horical.gito.base.IView;

/**
 * Created by thanhle on 11/10/16.
 */

public interface INewSurveyView extends IView {

    void showLoading();

    void hideLoading();

    void getDataSuccess();

    void getDataFailure(String error);
}
