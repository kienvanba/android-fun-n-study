package com.horical.gito.interactor.api.response.checkin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.CheckIn;

import java.util.List;

/**
 * Created by Penguin on 23-Dec-16
 */

public class GetAllCheckinResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<CheckIn> checkIn;
}
