package com.horical.gito.mvp.drawer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.IntentCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.ApiManager;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.caches.CachesManager;
import com.horical.gito.interactor.prefer.PreferManager;
import com.horical.gito.model.User;
import com.horical.gito.mvp.accounting.list.view.AccountingActivity;
import com.horical.gito.mvp.allProject.list.view.AllProjectActivity;
import com.horical.gito.mvp.calendar.view.CalendarActivity;
import com.horical.gito.mvp.caseStudy.list.view.CaseStudyActivity;
import com.horical.gito.mvp.checkin.list.view.dto.CheckinActivity;
import com.horical.gito.mvp.companySetting.list.view.CompanySettingActivity;
import com.horical.gito.mvp.drawer.adapter.MenuAdapter;
import com.horical.gito.mvp.drawer.dto.BodyDto;
import com.horical.gito.mvp.drawer.dto.FooterDto;
import com.horical.gito.mvp.drawer.dto.HeaderDto;
import com.horical.gito.mvp.estate.warehouse.view.WarehouseEstateActivity;
import com.horical.gito.mvp.intro.view.IntroActivity;
import com.horical.gito.mvp.leaving.main.view.LeavingActivity;
import com.horical.gito.mvp.logTime.view.LogTimeActivity;
import com.horical.gito.mvp.meeting.list.view.MeetingActivity;
import com.horical.gito.mvp.myPage.view.MyPageActivity;
import com.horical.gito.mvp.myProject.document.view.DocumentActivity;
import com.horical.gito.mvp.myProject.gantt.view.GanttActivity;
import com.horical.gito.mvp.myProject.setting.view.SettingActivity;
import com.horical.gito.mvp.myProject.sourceCode.list.view.SourceCodeActivity;
import com.horical.gito.mvp.myProject.summary.view.SummaryActivity;
import com.horical.gito.mvp.myProject.task.list.view.TaskActivity;
import com.horical.gito.mvp.myProject.testCase.view.TestCaseActivity;
import com.horical.gito.mvp.notification.main.view.NotificationActivity;
import com.horical.gito.mvp.recruitment.view.RecruitmentActivity;
import com.horical.gito.mvp.report.view.ReportActivity;
import com.horical.gito.mvp.salary.list.view.ListSalaryActivity;
import com.horical.gito.mvp.survey.list.view.SurveyActivity;
import com.horical.gito.mvp.todolist.todo.list.view.TodoListActivity;
import com.horical.gito.widget.dialog.DialogPositiveNegative;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

abstract public class DrawerActivity extends BaseActivity {

    // Main Menu
    public static final String nav_my_page = "My page";
    public static final String nav_all_project = "All project";

    // Sub Menu
    public static final String nav_my_project = "My Project";
    public static final String nav_summary = "Summary";
    public static final String nav_task = "Task";
    public static final String nav_gantt = "Gantt";
    public static final String nav_document = "Document";
    public static final String nav_source_code = "Source Code";
    public static final String nav_test_case = "Test Case";
    public static final String nav_change_log = "Change Log";
    public static final String nav_project_setting = "Project Setting";
    public static final String nav_post_api = "Post API";
    public static final String nav_chat_app = "Chat App";
    public static final String nav_log_time = "Log Time";
    // End Sub Menu

    public static final String nav_calendar = "Calendar";
    public static final String nav_todo = "Todo";
    public static final String nav_meeting = "Meeting";
    public static final String nav_report = "Report";
    public static final String nav_estate = "Estate";
    public static final String nav_case_study = "Case Study";
    public static final String nav_recruitment = "Recruitment";
    public static final String nav_notification = "Notification";
    public static final String nav_check_in = "Check in";
    public static final String nav_leaving_register = "Leaving register";
    public static final String nav_survey = "Survey";
    public static final String nav_accounting = "Accounting";
    public static final String nav_salary = "Salary";
    // End Main Menu

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.main_container)
    View mMainContainer;
    @Bind(R.id.rv_menu)
    RecyclerView mRvMenu;

    @Inject
    PreferManager mPreferManager;

    @Inject
    CachesManager mCachesManager;

    @Inject
    ApiManager mApiManager;

    MenuAdapter mMenuAdapter;

    private List<IRecyclerItem> mMenuList;
    private Handler mHandler;
    private static final int DRAWER_LAUNCH_DELAY = 50;

    abstract protected int getLayoutId();

    abstract protected int getNavId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        MainApplication.getAppComponent().inject(this);

        mMenuList = new ArrayList<>();
        setupListDrawer();
        setupNavDrawer();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void setupListDrawer() {
        mMenuList.clear();
        User user = mPreferManager.getUser();

        mMenuList.add(new HeaderDto(user.getCompanyId().getCompanyName(), user.getCompanyId().getAvatar()));

        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_MY_PAGE,
                R.drawable.ic_my_page_selector,
                nav_my_page,
                getNavId()));

        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_ALL_PROJECT,
                R.drawable.ic_all_project_selector,
                nav_all_project,
                getNavId()));

        if (GitOStorage.getInstance().isProjectDetails()) {
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_MY_PROJECT,
                    R.drawable.ic_my_project_selector,
                    nav_my_project,
                    getNavId()));
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_SUMMARY,
                    R.drawable.ic_summary_selector,
                    nav_summary,
                    getNavId()));
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_TASK,
                    R.drawable.ic_task_selector,
                    nav_task,
                    getNavId()));
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_GANTT,
                    R.drawable.ic_gantt_selector,
                    nav_gantt,
                    getNavId()));
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_DOCUMENT,
                    R.drawable.ic_document_selector,
                    nav_document,
                    getNavId()));
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_SOURCE_CODE,
                    R.drawable.ic_source_code_selector,
                    nav_source_code,
                    getNavId()));
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_TEST_CASE,
                    R.drawable.ic_test_case_selector,
                    nav_test_case,
                    getNavId()));
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_CHANGE_LOG,
                    R.drawable.ic_change_log_selector,
                    nav_change_log,
                    getNavId()));
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_PROJECT_SETTING,
                    R.drawable.ic_project_setting_selector,
                    nav_project_setting,
                    getNavId()));
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_POST_API,
                    R.drawable.ic_post_api_selector,
                    nav_post_api,
                    getNavId()));
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_CHAT_APP,
                    R.drawable.ic_chat_app_selector,
                    nav_chat_app,
                    getNavId()));
            mMenuList.add(new BodyDto(
                    AppConstants.NAV_DRAWER_ID_LOG_TIME,
                    R.drawable.ic_log_time_selector,
                    nav_log_time,
                    getNavId()));
        }

        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_CALENDAR,
                R.drawable.ic_callendar_selector,
                nav_calendar,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_TODO,
                R.drawable.ic_todo_list_selector,
                nav_todo,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_MEETING,
                R.drawable.ic_meeting_selector,
                nav_meeting,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_REPORT,
                R.drawable.ic_report_selector,
                nav_report,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_ESTATE,
                R.drawable.ic_estate_selector,
                nav_estate,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_CASE_STUDY,
                R.drawable.ic_case_study_selector,
                nav_case_study,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_RECRUITMENT,
                R.drawable.ic_recruitment_selector,
                nav_recruitment,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_NOTIFICATION,
                R.drawable.ic_notification_selector,
                nav_notification,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_CHECK_IN,
                R.drawable.ic_check_in_selector,
                nav_check_in,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_LEAVING_REGISTER,
                R.drawable.ic_leaving_register_selector,
                nav_leaving_register,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_SURVEY,
                R.drawable.ic_survey_selector,
                nav_survey,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_ACCOUNTING,
                R.drawable.ic_accounting_selector,
                nav_accounting,
                getNavId()));
        mMenuList.add(new BodyDto(
                AppConstants.NAV_DRAWER_ID_SALARY,
                R.drawable.ic_salary_selector,
                nav_salary,
                getNavId()));
        mMenuList.add(new FooterDto(
                user.getAvatar(),
                user.getDisplayName(),
                user.getEmail()));
    }

    private void setupNavDrawer() {
        mHandler = new Handler();
        if (mDrawerLayout == null) {
            return;
        }

        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                mRvMenu.smoothScrollToPosition(mMenuAdapter.getCurrentPosition());
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }
        });

        mRvMenu.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRvMenu.setLayoutManager(llm);

        mMenuAdapter = new MenuAdapter(mMenuList, this);
        mRvMenu.setAdapter(mMenuAdapter);
        mRvMenu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        mMenuAdapter.setOnItemClickListener(new MenuAdapter.OnItemClickListener() {

            @Override
            public void onLogoClicked() {
                Intent intent = new Intent(DrawerActivity.this, CompanySettingActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onLogOutClicked() {
                showConfirmDialog(getString(R.string.app_name), getString(R.string.confirm_logout), new DialogPositiveNegative.IPositiveNegativeDialogListener() {
                    @Override
                    public void onIPositiveNegativeDialogAnswerPositive(DialogPositiveNegative dialog) {
                        mApiManager.logout(new ApiCallback<BaseResponse>() {
                            @Override
                            public void success(BaseResponse res) {

                            }

                            @Override
                            public void failure(RestError error) {

                            }
                        });
                        mPreferManager.resetUser();
                        GitOStorage.getInstance().setProject(null);
                        Intent introIntent = new Intent(DrawerActivity.this, IntroActivity.class);
                        introIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(introIntent);
                    }

                    @Override
                    public void onIPositiveNegativeDialogAnswerNegative(DialogPositiveNegative dialog) {
                        dialog.dismiss();
                    }
                });
            }

            @Override
            public void onItemClick(int position) {
                IRecyclerItem item = mMenuList.get(position);

//                if (item instanceof SalaryHeaderDto) {
//
//                } else if (item instanceof FooterDto) {
//
//                } else {
                if (item instanceof BodyDto) {
                    mDrawerLayout.closeDrawer(Gravity.START);
                    final BodyDto body = (BodyDto) item;
                    if (!body.isSelected) {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                goToDrawerItem(body);
                            }
                        }, DRAWER_LAUNCH_DELAY);
                    }
                }
            }
        });

    }

    private void goToDrawerItem(BodyDto item) {
        switch (item.key) {
            case AppConstants.NAV_DRAWER_ID_ACCOUNTING: {
                Intent intent = new Intent(this, AccountingActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_MY_PAGE: {
                Intent intent = new Intent(this, MyPageActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_ALL_PROJECT: {
                GitOStorage.getInstance().setProject(null);
                Intent intent = new Intent(this, AllProjectActivity.class);
                startActivity(intent);
                finish();
                break;
            }

            // SUB MENU
            case AppConstants.NAV_DRAWER_ID_SUMMARY: {
                Intent intent = new Intent(this, SummaryActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_TASK: {
                Intent intent = new Intent(this, TaskActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_GANTT: {
                Intent intent = new Intent(this, GanttActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_DOCUMENT: {
                Intent intent = new Intent(this, DocumentActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_SOURCE_CODE: {
                Intent intent = new Intent(this, SourceCodeActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_TEST_CASE: {
                Intent intent = new Intent(this, TestCaseActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_CHANGE_LOG: {
                showToast(item.title + " coming soon !");
                break;
            }
            case AppConstants.NAV_DRAWER_ID_PROJECT_SETTING: {
                Intent intent = new Intent(this, SettingActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_POST_API: {
                showToast(item.title + " coming soon !");
                break;
            }
            case AppConstants.NAV_DRAWER_ID_CHAT_APP: {
                showToast(item.title + " coming soon !");
                break;
            }
            case AppConstants.NAV_DRAWER_ID_LOG_TIME: {
                Intent intent = new Intent(this, LogTimeActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_CALENDAR: {
                Intent intent = new Intent(this, CalendarActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_TODO: {
                Intent intent = new Intent(this, TodoListActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_MEETING: {
                Intent intent = new Intent(this, MeetingActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_REPORT: {
                Intent intent = new Intent(this, ReportActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_ESTATE: {
                Intent intent = new Intent(this, WarehouseEstateActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_CASE_STUDY: {
                Intent intent = new Intent(this, CaseStudyActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_RECRUITMENT: {
                Intent intent = new Intent(this, RecruitmentActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_NOTIFICATION: {
                Intent intent = new Intent(this, NotificationActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_CHECK_IN: {
                Intent intent = new Intent(this, CheckinActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_LEAVING_REGISTER: {
                Intent intent = new Intent(this, LeavingActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_SURVEY: {
                Intent intent = new Intent(this, SurveyActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case AppConstants.NAV_DRAWER_ID_SALARY: {
                Intent intent = new Intent(this, ListSalaryActivity.class);
                startActivity(intent);
                finish();
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (isDrawerOpen()) {
            closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    protected boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.START);
    }

    protected void closeDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(Gravity.START);
        }
    }

    protected void openDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.openDrawer(Gravity.START);
        }
    }

    protected void lockDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    protected void unLockDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }
}
