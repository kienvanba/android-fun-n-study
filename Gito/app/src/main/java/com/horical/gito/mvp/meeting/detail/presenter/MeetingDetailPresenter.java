package com.horical.gito.mvp.meeting.detail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.file.UploadFileRequest;
import com.horical.gito.interactor.api.request.file.UploadFileResponse;
import com.horical.gito.interactor.api.request.meeting.MeetingRequest;
import com.horical.gito.interactor.api.response.meeting.GetCreateMeetingResponse;
import com.horical.gito.interactor.api.response.meeting.GetUpdateMeetingResponse;
import com.horical.gito.model.Note;
import com.horical.gito.model.Room;
import com.horical.gito.model.TokenFile;
import com.horical.gito.model.User;
import com.horical.gito.mvp.meeting.detail.view.IMeetingDetailView;
import com.horical.gito.utils.CommonUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class MeetingDetailPresenter extends BasePresenter implements IMeetingDetailPresenter {

    private Date startDate;
    private Date endDate;
    private List<Room> rooms;
    private List<User> members;
    private List<TokenFile> attachFiles;
    private List<Note> notes;


    public void attachView(IMeetingDetailView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IMeetingDetailView getView() {
        return (IMeetingDetailView) getIView();
    }

    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);

        rooms = new ArrayList<>();
        members = new ArrayList<>();
        attachFiles = new ArrayList<>();
        notes = new ArrayList<>();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void createMeeting(String name, String desc) {
        // validate data
        if (name.trim().length() == 0) {
            getView().validateFailedNameEmpty();
            return;
        }
        if (desc.trim().length() == 0) {
            getView().validateFailedDescEmpty();
            return;
        }
        if (startDate == null || endDate == null) {
            getView().validateFailedTime();
            return;
        }

        // create request
        MeetingRequest request = new MeetingRequest();
        request.name = name;
        request.desc = desc;
        request.startDate = startDate;
        request.endDate = endDate;
        if (rooms != null && rooms.size() > 0) {
            request.room = rooms.get(0).getId();
        }
        if (members != null && members.size() > 0) {
            for (User user : members) {
                request.members.add(user.get_id());
            }
        }
        if (attachFiles != null && attachFiles.size() > 0) {
            for (TokenFile file : attachFiles) {
                request.attachFiles.add(file.getId());
            }
        }
        if (notes != null && notes.size() > 0) {
            for (Note note : notes) {
                request.note.add(note.getId());
            }
        }

        // send create meeting request
        getView().showLoading();
        getApiManager().createMeeting(request, new ApiCallback<GetCreateMeetingResponse>() {
            @Override
            public void success(GetCreateMeetingResponse res) {
                getView().hideLoading();
                getView().createMeetingSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().createMeetingFailure(error);
            }
        });
    }

    @Override
    public void updateMeeting(final String meetingId, String name, String desc) {
        // validate data
        if (name.trim().length() == 0) {
            getView().validateFailedNameEmpty();
            return;
        }
        if (desc.trim().length() == 0) {
            getView().validateFailedDescEmpty();
            return;
        }
        if (startDate == null || endDate == null) {
            getView().validateFailedTime();
            return;
        }

        // create request
        MeetingRequest request = new MeetingRequest();
        request.name = name;
        request.desc = desc;
        request.startDate = startDate;
        request.endDate = endDate;
        if (rooms != null && rooms.size() > 0) {
            request.room = rooms.get(0).getId();
        }
        if (members != null && members.size() > 0) {
            for (User user : members) {
                request.members.add(user.get_id());
            }
        }
        if (attachFiles != null && attachFiles.size() > 0) {
            for (TokenFile file : attachFiles) {
                request.attachFiles.add(file.getId());
            }
        }
        if (notes != null && notes.size() > 0) {
            for (Note note : notes) {
                request.note.add(note.getId());
            }
        }

        // send update meeting request
        getView().showLoading();
        getApiManager().updateContentMeeting(meetingId, request, new ApiCallback<GetUpdateMeetingResponse>() {
            @Override
            public void success(GetUpdateMeetingResponse res) {
                getView().hideLoading();
                getView().updateMeetingSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().updateMeetingFailure(error);
            }
        });
    }

    @Override
    public void uploadFile(MediaType type, final File file) {
        getView().showLoading();
        UploadFileRequest request = new UploadFileRequest(type, file);
        RequestBody status = RequestBody.create(okhttp3.MultipartBody.FORM, "0");
            getApiManager().uploadFile(request.file, status, new ApiCallback<UploadFileResponse>() {
            @Override
            public void success(UploadFileResponse res) {
                getView().hideLoading();
                attachFiles.add(res.tokenFile);

                String url = CommonUtils.getURL(res.tokenFile);
                getView().uploadSuccess(url);
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().uploadFailure(error.message);
            }
        });
    }


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms.clear();
        this.rooms.addAll(rooms);
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members.clear();
        this.members.addAll(members);
    }

    public List<TokenFile> getAttachFiles() {
        return attachFiles;
    }

    public void setAttachFiles(List<TokenFile> attachFiles) {
        this.attachFiles = attachFiles;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

}
