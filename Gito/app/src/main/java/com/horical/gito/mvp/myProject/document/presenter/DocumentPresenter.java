package com.horical.gito.mvp.myProject.document.presenter;


import android.util.Log;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.document.GetAllDocumentResponse;
import com.horical.gito.model.Document;
import com.horical.gito.mvp.myProject.document.adapter.DocumentItemBody;
import com.horical.gito.mvp.myProject.document.view.IDocumentView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class DocumentPresenter extends BasePresenter implements IDocumentPresenter {
    private List<IRecyclerItem> mDocuments;

    private static final String TAG = makeLogTag(DocumentPresenter.class);

    public IDocumentView getView(){
        return (IDocumentView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mDocuments = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public List<IRecyclerItem> getAllItems() {
        return mDocuments;
    }

    @Override
    public void getAllDocuments(String linkFile) {
        String folderId = getPreferManager().getProjectId();
        getView().showLoading();
        getApiManager().getAllDocument(folderId, linkFile, new ApiCallback<GetAllDocumentResponse>() {
            @Override
            public void success(GetAllDocumentResponse res) {
                if (!isViewAttached()) return;
                getView().hideLoading();
                if (res == null) return;
                mDocuments.clear();

                Log.e(TAG, "came");
                if(!res.documents.isEmpty()){
                    Log.e(TAG,"but empty");
                    for(Document document: res.documents){
                        DocumentItemBody body = new DocumentItemBody(document);
                        mDocuments.add(body);
                    }
                }
                getView().getAllDocumentSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().getAllDocumentFailure(error.message);
            }
        });
    }
}


