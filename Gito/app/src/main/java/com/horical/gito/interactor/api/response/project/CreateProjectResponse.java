package com.horical.gito.interactor.api.response.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Project;

import java.io.Serializable;

public class CreateProjectResponse extends BaseResponse implements Serializable {

    @SerializedName("results")
    @Expose
    Project project;
}
