// activity

package com.horical.gito.mvp.leaving.newLeaving.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.model.wrapperModel.WrapperListUser;
import com.horical.gito.mvp.dialog.dialogDateTimePicker.DateTimePickerDialog;
import com.horical.gito.mvp.leaving.main.adapter.LeavingAttachFileAdapter;
import com.horical.gito.mvp.leaving.newLeaving.approverAdapter.LeavingSelectedApproverAdapter;
import com.horical.gito.mvp.leaving.newLeaving.injection.component.DaggerLeavingNewComponent;
import com.horical.gito.mvp.leaving.newLeaving.injection.component.LeavingNewComponent;
import com.horical.gito.mvp.leaving.newLeaving.injection.module.LeavingNewModule;
import com.horical.gito.mvp.leaving.newLeaving.presenter.LeavingNewPresenter;
import com.horical.gito.mvp.user.list.view.UserActivity;
import com.horical.gito.utils.HDate;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.MainApplication.mContext;

/**
 * Created by Dragonoid on 12/9/2016.
 */

public class LeavingNewActivity extends BaseActivity implements View.OnClickListener, ILeavingNewView/*, TextWatcher */ {

    public final static int REQUEST_CODE_USER = 5;
    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;
    @Bind(R.id.leaving_tv_pick_from)
    TextView mFrom;
    @Bind(R.id.leaving_tv_pick_to)
    TextView mTo;
    @Bind(R.id.leaving_tv_pick_reason)
    EditText mReason;
    @Bind(R.id.leaving_img_pick_time)
    ImageView mTime;
    @Bind(R.id.leaving_btn_add)
    ImageView mAddApprover;
    @Bind(R.id.leaving_lv_approver)
    RecyclerView mListApprover;
    @Bind(R.id.leaving_btn_send)
    Button mSend;
    @Bind(R.id.leaving_lv_attach_file)
    RecyclerView mListFile;
    LeavingAttachFileAdapter mFileAdapter;
    ArrayList<String> mFileList;
    HDate mTimeFrom;
    HDate mTimeTo;
    @Inject
    LeavingNewPresenter mPresenter;
    DateTimePickerDialog mTimeDialog;
    //    LeavingApproverDialog mApproverDialog;
//    LeavingAddApproverAdapter mAddApproverAdapter;
    LeavingSelectedApproverAdapter mApproverSelectedAdapter;
    private WrapperListUser wrapperListUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaving_new);
        ButterKnife.bind(this);

        LeavingNewComponent component = DaggerLeavingNewComponent.builder()
                .leavingNewModule(new LeavingNewModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        initData();
        initListener();

        mPresenter.onCreate();
        mPresenter.attachView(this);
    }

    protected void initData() {
        topBar.setTextTitle("New Leaving");
        topBar.setTextViewLeft("Cancel");


        //Initial data
        mTimeDialog = new DateTimePickerDialog(this, new DateTimePickerDialog.OnClickDoneListener() {
            @Override
            public void onDoneClick(HDate DateTimeFrom, HDate DateTimeTo) {
                OnDoneClick(DateTimeFrom, DateTimeTo);
            }
        });

        mTimeDialog.setCanceledOnTouchOutside(true);

        mFileList = new ArrayList<>();
        //set Adapter

        mApproverSelectedAdapter = new LeavingSelectedApproverAdapter(mPresenter.getListSelected(), mContext);
        mApproverSelectedAdapter.notifyDataSetChanged();
        mListApprover.setAdapter(mApproverSelectedAdapter);
        mListApprover.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        mFileAdapter = new LeavingAttachFileAdapter(mFileList, this, new LeavingAttachFileAdapter.LeavingFileCallback() {
            @Override
            public void onItemDelete(int position) {
                mFileList.remove(position);
            }
        });
        mFileAdapter.notifyDataSetChanged();
        mListFile.setAdapter(mFileAdapter);
        mListFile.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        //--------test view-------------------------------------------------
        mFileList.add("Mock1.rar");
        mFileList.add("Mock2.rar");

        mFileAdapter.notifyDataSetChanged();
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

            }
        });
        mTime.setOnClickListener(this);
        mAddApprover.setOnClickListener(this);
        mSend.setOnClickListener(this);
    }

    private void OnDoneClick(HDate DateTimeFrom, HDate DateTimeTo) {
        mTimeFrom = DateTimeFrom;
        mTimeTo = DateTimeTo;
        mFrom.setText(
                DateTimeFrom.hour + ":" + DateTimeFrom.minute + " " +
                        DateTimeFrom.day + "/" + DateTimeFrom.month + "/" + DateTimeFrom.year);
        mTo.setText(
                DateTimeTo.hour + ":" + DateTimeTo.minute + " " +
                        DateTimeTo.day + "/" + DateTimeTo.month + "/" + DateTimeTo.year);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == mTime.getId()) {
            mTimeDialog.show();
            return;
        }
        if (v.getId() == mAddApprover.getId()) {
            Intent intent = new Intent(LeavingNewActivity.this, UserActivity.class);
            intent.putExtra(
                    UserActivity.KEY_FROM,
                    UserActivity.FROM_LEAVING_APPROVER
            );
            startActivityForResult(intent, REQUEST_CODE_USER);
        }
        if (v.getId() == mSend.getId()) {
            mPresenter.createLeaving(mTimeFrom, mTimeTo, mReason.getText().toString());
            return;
        }
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void createLeavingSuccess() {
        hideLoading();
        showToast("success");
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void createLevingFailure(RestError error) {
        hideLoading();
        showToast("failure");
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_USER) {
            if (resultCode == Activity.RESULT_OK) {
                //day la du lieu nhan dc
                wrapperListUser = (WrapperListUser) data.getSerializableExtra(UserActivity.KEY_INTENT_USER_LIST);
                mPresenter.getListSelected().clear();
                if (wrapperListUser != null && !wrapperListUser.getMyUsers().isEmpty()) {
                    for (int i = 0; i < wrapperListUser.getMyUsers().size(); i++) {
                        mPresenter.getListSelected().add(wrapperListUser.getMyUsers().get(i));
                    }
                }
                mApproverSelectedAdapter.notifyDataSetChanged();
                mListApprover.requestLayout();
            }
        }
    }
}

