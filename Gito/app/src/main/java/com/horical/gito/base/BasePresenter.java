package com.horical.gito.base;

import com.horical.gito.MainApplication;
import com.horical.gito.interactor.api.ApiManager;
import com.horical.gito.interactor.assets.AssetsManager;
import com.horical.gito.interactor.caches.CachesManager;
import com.horical.gito.interactor.event.EventManager;
import com.horical.gito.interactor.prefer.PreferManager;
import com.horical.gito.interactor.socket.SocketManager;
import com.horical.gito.model.Empty;

import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

public abstract class BasePresenter {

    private IView mView;

    @Inject
    ApiManager mApiManager;

    @Inject
    EventManager mEventManager;

    @Inject
    PreferManager mPreferManager;

    @Inject
    AssetsManager mAssetsManager;

    @Inject
    SocketManager mSocketManager;

    @Inject
    CachesManager mCachesManager;

    public BasePresenter() {
        MainApplication.getAppComponent().inject(this);
    }

    public ApiManager getApiManager() {
        return mApiManager;
    }

    public EventManager getEventManager() {
        return mEventManager;
    }

    public PreferManager getPreferManager() {
        return mPreferManager;
    }

    public AssetsManager getAssetsManager() {
        return mAssetsManager;
    }

    public SocketManager getSocketManager() {
        return mSocketManager;
    }

    public CachesManager getCachesManager() {
        return mCachesManager;
    }

    public void attachView(IView view) {
        mView = view;
    }

    public void detachView() {
        mView = null;
    }

    public boolean isViewAttached() {
        return mView != null;
    }

    public IView getIView() {
        return mView;
    }

    public void onCreate() {

    }

    public void onDestroy() {

    }

    public void onResume() {

    }

    public void onPause() {

    }

    @Subscribe
    public void onEventEmpty(Empty empty) {

    }
}

