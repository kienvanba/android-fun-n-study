package com.horical.gito.mvp.notification.main.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.notification.main.presenter.NotificationPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class NotificationModule {
    @PerActivity
    @Provides
    NotificationPresenter provideNotificationPresenter(){
        return new NotificationPresenter();
    }
}
