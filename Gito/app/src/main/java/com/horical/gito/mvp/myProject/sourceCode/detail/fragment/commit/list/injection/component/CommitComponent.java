package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.injection.module.CommitModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.view.CommitFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class  , modules = CommitModule.class)
public interface CommitComponent {
    void inject(CommitFragment fragment);
}
