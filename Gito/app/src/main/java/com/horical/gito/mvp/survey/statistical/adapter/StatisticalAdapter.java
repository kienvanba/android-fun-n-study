package com.horical.gito.mvp.survey.statistical.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.R;
import com.horical.gito.mvp.survey.statistical.model.CustomViewBar;
import com.horical.gito.mvp.survey.statistical.model.PieGraph;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nhattruong251295 on 4/7/2017.
 */

public class StatisticalAdapter extends RecyclerView.Adapter<StatisticalAdapter.StatisticalHolder> {
    private List<String> mList;
    private Context mContext;

    public StatisticalAdapter(List<String> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public StatisticalAdapter.StatisticalHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_question_statistical,parent,false);
        return new StatisticalHolder(view);
    }

    @Override
    public void onBindViewHolder(StatisticalHolder holder, int position) {
        holder.optionAdapter = new OptionAdapter(mContext,mList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        holder.mRcvOptionStatistical.setLayoutManager(layoutManager);
        holder.mRcvOptionStatistical.setHasFixedSize(false);
        holder.mRcvOptionStatistical.setAdapter(holder.optionAdapter);

        List<Integer> listInteger = new ArrayList<>();
        listInteger.add(10);
        listInteger.add(5);
        listInteger.add(10);

        /*holder.barHorizontalAdapter = new BarHorizontalAdapter(mContext,mList);
        LinearLayoutManager llmBar = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        holder.mRcvBarHorizontal.setLayoutManager(llmBar);
        holder.mRcvBarHorizontal.setHasFixedSize(false);
        holder.mRcvBarHorizontal.setAdapter(holder.barHorizontalAdapter);*/

        holder.viewVertical.setVisibility(View.VISIBLE);
        holder.viewVertical.setMinimumWidth(2);
        holder.viewVertical.setColor(R.color.grey_dare);
        holder.viewVertical.setMinimumHeight(mList.size()*90);

        holder.barHorizontalAdapter = new BarHorizontalAdapter(mContext,listInteger);
        LinearLayoutManager llmBar = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        holder.mRcvBarHorizontal.setLayoutManager(llmBar);
        holder.mRcvBarHorizontal.setHasFixedSize(false);
        holder.mRcvBarHorizontal.setAdapter(holder.barHorizontalAdapter);

        float[] data = {450, 300, 200};
        holder.pieGraph.setData(data);
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class StatisticalHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.view_vertical)
        CustomViewBar viewVertical;

        @Bind(R.id.rcv_option_statistical)
        RecyclerView mRcvOptionStatistical;

        @Bind(R.id.rcv_bar_horizontal)
        RecyclerView mRcvBarHorizontal;

        @Bind(R.id.pie_chart)
        PieGraph pieGraph;

        OptionAdapter optionAdapter;

        BarHorizontalAdapter barHorizontalAdapter;

        public StatisticalHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
