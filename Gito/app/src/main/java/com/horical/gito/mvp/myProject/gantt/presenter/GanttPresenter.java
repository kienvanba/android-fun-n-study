package com.horical.gito.mvp.myProject.gantt.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.gantt.view.IGanttView;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class GanttPresenter extends BasePresenter implements IGanttPresenter {
    private static final String TAG = makeLogTag(GanttPresenter.class);

    public void attachView(IGanttView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IGanttView getView() {
        return (IGanttView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
