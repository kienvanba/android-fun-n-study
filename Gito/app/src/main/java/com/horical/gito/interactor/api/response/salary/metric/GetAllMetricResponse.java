package com.horical.gito.interactor.api.response.salary.metric;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Metric;

import java.util.List;

public class GetAllMetricResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    private List<Metric> metrics;


    public List<Metric> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<Metric> metrics) {
        this.metrics = metrics;
    }
}
