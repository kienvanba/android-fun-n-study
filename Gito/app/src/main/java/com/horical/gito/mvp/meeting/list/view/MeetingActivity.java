package com.horical.gito.mvp.meeting.list.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.meeting.detail.view.MeetingDetailActivity;
import com.horical.gito.mvp.meeting.list.adapter.MeetingAdapter;
import com.horical.gito.mvp.meeting.list.injection.component.DaggerMeetingComponent;
import com.horical.gito.mvp.meeting.list.injection.component.MeetingComponent;
import com.horical.gito.mvp.meeting.list.injection.module.MeetingModule;
import com.horical.gito.mvp.meeting.list.presenter.MeetingPresenter;
import com.horical.gito.mvp.roomBooking.list.view.RoomBookingActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.FROM_MENU;
import static com.horical.gito.AppConstants.KEY_FROM;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class MeetingActivity extends DrawerActivity implements IMeetingView, View.OnClickListener {

    public final static int REQUEST_CODE_ADD_EDIT_MEETING = 1;
    private static final String TAG = makeLogTag(MeetingActivity.class);

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.rv_list_meeting)
    RecyclerView mRcvMeeting;

    @Bind(R.id.meeting_refresh_layout)
    SwipeRefreshLayout mRefresh;

    @Bind(R.id.rbt_btn_meeting)
    RadioButton mBtnMeeting;

    @Bind(R.id.rbt_btn_roombooking)
    RadioButton mBtnRoomBooking;

    private MeetingAdapter mAdapterMeeting;

    @Inject
    MeetingPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_meeting;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_MEETING;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        MeetingComponent component = DaggerMeetingComponent.builder()
                .meetingModule(new MeetingModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(getString(R.string.meeting));
        int from = getIntent().getIntExtra(KEY_FROM, FROM_MENU);
        mPresenter.setFrom(from);
        if (from == FROM_MENU) {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        } else {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
            lockDrawer();

        }
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);

        mAdapterMeeting = new MeetingAdapter(this, mPresenter.getListMeeting());
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRcvMeeting.setLayoutManager(llm);
        mRcvMeeting.setAdapter(mAdapterMeeting);

        showLoading();
        mPresenter.getAllMeeting();
    }

    protected void initListener() {
        mBtnMeeting.setSelected(true);
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                if (mPresenter.getFrom() == FROM_MENU) openDrawer();
                else onBackPressed();
            }

            @Override
            public void onRightButtonOneClicked() {
                Intent intent = new Intent(MeetingActivity.this, MeetingDetailActivity.class);
                startActivityForResult(intent, REQUEST_CODE_ADD_EDIT_MEETING);
            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });

        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                mPresenter.getAllMeeting();
            }
        });

        mAdapterMeeting.setOnItemClickListener(new MeetingAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(MeetingActivity.this, MeetingDetailActivity.class);
                intent.putExtra(MeetingDetailActivity.ARG_MEETING, mPresenter.getListMeeting().get(position));
                startActivityForResult(intent, REQUEST_CODE_ADD_EDIT_MEETING);

            }

            @Override
            public void onDelete(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MeetingActivity.this);
                builder.setMessage(R.string.are_you_sure_to_delete_this_meeting);
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                showDialog("Loading...");
                                mPresenter.deleteMeeting(mAdapterMeeting.getListMeeting().get(position).getId(), position);
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        mBtnMeeting.setOnClickListener(this);
        mBtnRoomBooking.setOnClickListener(this);

    }

    @Override
    public void getAllMeetingSuccess() {
        dismissDialog();
        mRefresh.setRefreshing(false);
        mAdapterMeeting.notifyDataSetChanged();
        showToast("Success");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            showLoading();
            mPresenter.getAllMeeting();
        }

    }

    @Override
    public void getAllMeetingFailure(String error) {
        dismissDialog();
        mRefresh.setRefreshing(false);
        showToast("Failure");
    }

    @Override
    public void deleteMeetingSuccess(int position) {
        hideLoading();
        showToast("Success");
        mAdapterMeeting.getListMeeting().remove(position);
        mAdapterMeeting.notifyItemRemoved(position);
        mAdapterMeeting.notifyItemRangeChanged(position, mAdapterMeeting.getItemCount());
    }

    @Override
    public void deleteMeetingFailure() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rbt_btn_roombooking: {
                mBtnRoomBooking.setSelected(true);
                mBtnMeeting.setSelected(false);
                Intent intent = new Intent(MeetingActivity.this, RoomBookingActivity.class);
                startActivity(intent);
                break;
            }
        }
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }


}
