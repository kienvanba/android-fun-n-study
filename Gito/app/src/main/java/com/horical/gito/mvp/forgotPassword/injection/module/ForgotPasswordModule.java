package com.horical.gito.mvp.forgotPassword.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.forgotPassword.presenter.ForgotPasswordPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ForgotPasswordModule {
    @Provides
    @PerActivity
    ForgotPasswordPresenter provideForgotPasswordPresenter() {
        return new ForgotPasswordPresenter();
    }
}
