package com.horical.gito.mvp.allProject.detail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.file.UploadFileRequest;
import com.horical.gito.interactor.api.request.file.UploadFileResponse;
import com.horical.gito.interactor.api.request.project.CreateProjectRequest;
import com.horical.gito.interactor.api.response.project.CreateProjectResponse;
import com.horical.gito.model.Project;
import com.horical.gito.mvp.allProject.detail.view.IProjectDetailView;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.utils.LogUtils;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ProjectDetailPresenter extends BasePresenter implements IProjectDetailPresenter {

    private static final String TAG = LogUtils.makeLogTag(ProjectDetailPresenter.class);

    private Project project;

    private IProjectDetailView getView() {
        return (IProjectDetailView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void createProject(String name, String projectId, String desc) {

        CreateProjectRequest request = new CreateProjectRequest(
                name,
                projectId,
                desc,
                project.getAvatar());

        getView().showLoading();
        getApiManager().createProject(
                request,
                new ApiCallback<CreateProjectResponse>() {
                    @Override
                    public void success(CreateProjectResponse res) {
                        getView().hideLoading();
                        getView().createSuccess();
                    }

                    @Override
                    public void failure(RestError error) {
                        getView().hideLoading();
                        getView().createFailure(error.message);
                    }
                });
    }

    @Override
    public void uploadFile(MediaType type, File file) {

        getView().showLoading();
        UploadFileRequest request = new UploadFileRequest(type, file);
        RequestBody status = RequestBody.create(okhttp3.MultipartBody.FORM, "0");
        getApiManager().uploadFile(request.file, status, new ApiCallback<UploadFileResponse>() {
            @Override
            public void success(UploadFileResponse res) {
                getView().hideLoading();

                if (project == null) {
                    project = new Project();
                }
                project.setAvatar(res.tokenFile);
                String url = CommonUtils.getURL(res.tokenFile);
                getView().uploadSuccess(url);
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().uploadError(error.message);
            }
        });
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}

