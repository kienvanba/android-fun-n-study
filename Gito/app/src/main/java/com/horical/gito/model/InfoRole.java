package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/23/17.
 */

public class InfoRole implements Serializable {
    @SerializedName("modify")
    @Expose
    private Boolean modify;

    @SerializedName("view")
    @Expose
    private Boolean view;

    public Boolean isModify() {
        return modify;
    }

    public void setModify(Boolean modify) {
        this.modify = modify;
    }

    public Boolean isView() {
        return view;
    }

    public void setView(Boolean view) {
        this.view = view;
    }
}