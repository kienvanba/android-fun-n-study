package com.horical.gito.mvp.myProject.testCase.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.testCase.injection.module.TestCaseModule;
import com.horical.gito.mvp.myProject.testCase.view.TestCaseActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = TestCaseModule.class)
public interface TestCaseComponent {
    void inject(TestCaseActivity activity);
}
