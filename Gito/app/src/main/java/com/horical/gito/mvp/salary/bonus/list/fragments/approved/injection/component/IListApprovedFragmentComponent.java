package com.horical.gito.mvp.salary.bonus.list.fragments.approved.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.bonus.list.fragments.approved.injection.module.ListApprovedFragmentModule;
import com.horical.gito.mvp.salary.bonus.list.fragments.approved.view.ListApprovedFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ListApprovedFragmentModule.class)
public interface IListApprovedFragmentComponent {
    void inject(ListApprovedFragment activity);
}
