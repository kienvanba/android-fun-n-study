package com.horical.gito.mvp.leaving.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 12/12/2016.
 */

public class LeavingAddApproverAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<LeavingApproverItem> mItems;
    Context mContext;
    public LeavingAddApproverAdapter(List<LeavingApproverItem> list, Context context){
        mItems=list;
        mContext=context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leaving_add_approver,parent,false);
        return new LeavingAddApproverHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final LeavingAddApproverHolder approverHolder = (LeavingAddApproverHolder) holder;
        final LeavingApproverItem approverItem = mItems.get(position);
        approverHolder.mAddName.setText(approverItem.mName);
        approverHolder.mAddName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(approverHolder,approverItem);
            }
        });
        if(approverItem.isChecked()) approverHolder.setCheck();
        else approverHolder.setUncheck();
        approverHolder.mCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(approverHolder,approverItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
    private void onItemClick(LeavingAddApproverHolder holder, LeavingApproverItem item){
        if(item.mChecked){
            holder.mCheck.setImageResource(R.drawable.ic_blue_check);
            item.mChecked=false;
        }else{
            holder.mCheck.setImageResource(R.drawable.ic_blue_checked);
            item.mChecked=true;
        }
    }
    public class LeavingAddApproverHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.leaving_iv_add_approver_check)
        ImageView mCheck;
        @Bind(R.id.leaving_tv_add_approver_name)
        GitOTextView mAddName;

        public LeavingAddApproverHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
            mCheck.setImageResource(R.drawable.ic_blue_checked);
        }
        public void setCheck(){
            mCheck.setImageResource(R.drawable.ic_blue_checked);
        }
        public void setUncheck(){
            mCheck.setImageResource(R.drawable.ic_blue_check);
        }
    }
}
