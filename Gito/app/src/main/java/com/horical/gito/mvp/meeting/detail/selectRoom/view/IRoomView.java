package com.horical.gito.mvp.meeting.detail.selectRoom.view;

import com.horical.gito.base.IView;


public interface IRoomView extends IView {
    void getAllRoomBookingSuccess();

    void getAllRoomBookingFailure(String err);

    void showLoading();

    void hideLoading();
}
