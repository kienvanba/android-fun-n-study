package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 30-Nov-16.
 */

public class BranchAdapter extends RecyclerView.Adapter<BranchAdapter.BranchHolder> {
    private Context mContext;
    private List<String> mList;
    private OnItemClickListener mOnItemClickListener;

    public BranchAdapter(Context context, List<String> list) {
        mContext = context;
        mList = list;
    }


    @Override
    public BranchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_code_branch, parent, false);
        return new BranchHolder(view);
    }

    @Override
    public void onBindViewHolder(BranchHolder holder, final int position) {
        String data = mList.get(position);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class BranchHolder extends RecyclerView.ViewHolder{

        View view;

        public BranchHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
    public interface OnItemClickListener {
        void onItemClickSelected(int position);
    }
}
