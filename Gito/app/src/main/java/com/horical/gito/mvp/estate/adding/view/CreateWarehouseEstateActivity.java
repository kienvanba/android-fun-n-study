package com.horical.gito.mvp.estate.adding.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.dialog.dialogPhoto.DialogPhoto;
import com.horical.gito.mvp.estate.adding.injection.component.CreateWarehouseEstateComponent;
import com.horical.gito.mvp.estate.adding.injection.component.DaggerCreateWarehouseEstateComponent;
import com.horical.gito.mvp.estate.adding.injection.module.CreateWarehouseEstateModule;
import com.horical.gito.mvp.estate.adding.presenter.CreateWarehouseEstatePresenter;
import com.horical.gito.utils.FileUtils;
import com.horical.gito.utils.LogUtils;
import com.horical.gito.utils.NetworkUtils;
import com.horical.gito.widget.edittext.GitOEditText;

import java.io.File;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;

//import com.horical.gito.mvp.estate.adding.injection.component.DaggerCreateWarehouseEstateComponent;

/**
 * Created by hoangsang on 4/12/17.
 */

public class CreateWarehouseEstateActivity extends BaseActivity implements ICreateWarehouseEstateView {

    private static final String TAG = LogUtils.makeLogTag(CreateWarehouseEstateActivity.class);

    @Bind(R.id.view_bar_top)
    public CustomViewTopBar mTopBar;

    @Bind(R.id.imv_add_image)
    public ImageView mImvAddImage;

    @Bind(R.id.pgbar_warehouse)
    public ProgressBar mPgbWarehouse;

    @Bind(R.id.edt_name)
    public GitOEditText mEdtName;

    @Bind(R.id.edt_description)
    public GitOEditText mEdtDescription;

    private File mUploadFile;

    @Inject
    CreateWarehouseEstatePresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_warehouse_estate);
        ButterKnife.bind(this);

        CreateWarehouseEstateComponent component = DaggerCreateWarehouseEstateComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .createWarehouseEstateModule(new CreateWarehouseEstateModule())
                .build();
        component.inject(this);

        initView();
        initData();
        initListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    private void initView() {
        mTopBar.setTextTitle(getString(R.string.new_estate));
        mTopBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        mTopBar.setTextViewRight(getString(R.string.create));
    }

    protected void initData() {
        mPresenter.attachView(this);
        mPresenter.onCreate();
    }

    protected void initListener() {
        mTopBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                finish();
            }

            @Override
            public void onRightClicked() {
                mPresenter.createWarehouseEstate();
            }
        });

        mImvAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DialogPhoto dialogPhoto = new DialogPhoto(CreateWarehouseEstateActivity.this);
                dialogPhoto.setListener(new DialogPhoto.OnDialogEditPhotoClickListener() {
                    @Override
                    public void albumGallery() {
                        handleOpenLibrary();
                        dialogPhoto.dismiss();
                    }

                    @Override
                    public void takePhoto() {
                        handleOpenCamera();
                        dialogPhoto.dismiss();
                    }
                });

                dialogPhoto.show();
            }
        });
    }

    private void handleOpenCamera() {
        String[] s = {Manifest.permission.CAMERA};
        if (checkPermissions(s)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            mUploadFile = FileUtils.createImageFile(this);
            Uri uri = Uri.fromFile(mUploadFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, REQUEST_CAPTURE_IMAGE);

        } else {
            ActivityCompat.requestPermissions(this, s, REQUEST_PERMISSION_CAPTURE_IMAGE);
        }
    }

    private void handleOpenLibrary() {
        String[] s = {Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(s)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, REQUEST_READ_LIBRARY);

        } else {
            ActivityCompat.requestPermissions(this, s, REQUEST_PERMISSION_READ_LIBRARY);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case REQUEST_PERMISSION_CAPTURE_IMAGE:
                    handleOpenCamera();
                    break;
                case REQUEST_PERMISSION_READ_LIBRARY:
                    handleOpenLibrary();
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAPTURE_IMAGE:
                    System.out.println("FILE : " + mUploadFile.toString());
                    if (!NetworkUtils.isConnected(this))
                        showNoNetworkErrorDialog();
                    else
                        mPresenter.uploadFile(
                                MediaType.parse(MimeTypeMap.getFileExtensionFromUrl(mUploadFile.getName())), mUploadFile
                        );

                    break;

                case REQUEST_READ_LIBRARY:
                    System.out.println("File from library " + data.getData());

                    if (!NetworkUtils.isConnected(this))
                        showNoNetworkErrorDialog();
                    else {
                        mUploadFile = FileUtils.convertUriToFile(this, data.getData());
                        mPresenter.uploadFile(MediaType.parse(getContentResolver().getType(data.getData())),
                                mUploadFile);
                    }

                    break;
            }

        }
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void uploadFileSuccess(String url) {
        LogUtils.LOGI(TAG, "upload success : " + url);
        ImageLoader.load(this, url, mImvAddImage, mPgbWarehouse);
        dismissDialog();
    }

    @Override
    public void uploadFileFailed() {
        LogUtils.LOGE(TAG, "upload failed");
        dismissDialog();
    }

    @Override
    public String getName() {
        return mEdtName.getText().toString().trim();
    }

    @Override
    public String getDescription() {
        return mEdtDescription.getText().toString().trim();
    }

    @Override
    public void createWarehouseEstateSuccess() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();

    }

    @Override
    public void createWarehouseEstateFailed() {
        showErrorDialog(getString(R.string.create_warehouse_estate_failed));
    }
}
