package com.horical.gito.mvp.recruitment.view;

import com.horical.gito.base.IView;


public interface IRecruitmentView extends IView {
    void getListRecruitmentSuccess();

    void getDataError(String error);

    void showLoading();

    void dismissLoading();
}
