package com.horical.gito.model.estate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hoangsang on 4/14/17.
 */

public class EstateType implements Serializable {

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("estateWareHouseId")
    @Expose
    public String estateWareHouseId;

    @SerializedName("creatorId")
    @Expose
    public String creatorId;

    @SerializedName("companyId")
    @Expose
    public String companyId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("desc")
    @Expose
    public String desc;

    @SerializedName("createdDate")
    @Expose
    public String createdDate;

    @SerializedName("modifiedDate")
    @Expose
    public String modifiedDate;

}
