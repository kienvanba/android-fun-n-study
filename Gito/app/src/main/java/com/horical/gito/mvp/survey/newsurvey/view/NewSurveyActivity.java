package com.horical.gito.mvp.survey.newsurvey.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.Question;
import com.horical.gito.model.Survey;
import com.horical.gito.model.User;
import com.horical.gito.model.wrapperModel.WrapperListUser;
import com.horical.gito.mvp.dialog.dialogInputEdit.InputEditDialog;
import com.horical.gito.mvp.dialog.dialogSurvey.DialogAddQuestionSurvey;
import com.horical.gito.mvp.survey.list.view.SurveyActivity;
import com.horical.gito.mvp.survey.newsurvey.adapter.question.QuestionAdapter;
import com.horical.gito.mvp.survey.newsurvey.adapter.user.UserAdapter;
import com.horical.gito.mvp.survey.newsurvey.injection.component.DaggerNewSurveyComponent;
import com.horical.gito.mvp.survey.newsurvey.injection.component.NewSurveyComponent;
import com.horical.gito.mvp.survey.newsurvey.injection.module.NewSurveyModule;
import com.horical.gito.mvp.survey.newsurvey.presenter.NewSurveyPresenter;
import com.horical.gito.mvp.user.list.view.UserActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.TYPE_CROSS;
import static com.horical.gito.AppConstants.TYPE_NORMAL;

/**
 * Created by thanhle on 11/10/16.
 */

public class NewSurveyActivity extends BaseActivity implements INewSurveyView, View.OnClickListener {
    public final static int REQUEST_CODE_USER = 1;
    public final static String SURVEY = "SURVEY";

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.ll_edit_title)
    LinearLayout llEditTitle;

    @Bind(R.id.edt_title_survey)
    EditText edtTitle;

    @Bind(R.id.chk_type_cross)
    CheckBox chkTypeCross;

    @Bind(R.id.chk_type_normal)
    CheckBox chkTypeNormal;

    @Bind(R.id.ll_status_survey)
    LinearLayout llStatusSurvey;

    @Bind(R.id.ll_add_question)
    LinearLayout llAddQuestion;

    @Bind(R.id.ll_add_aprrovedBy)
    LinearLayout llAddApprovedBy;

    @Bind(R.id.ll_add_user)
    LinearLayout llAddUser;

    @Bind(R.id.rcv_question)
    RecyclerView mRcvQuestion;

    @Bind(R.id.rcv_submiter)
    RecyclerView mRcvSubmiter;

    @Bind(R.id.rcv_user)
    RecyclerView mRcvUser;

    @Bind(R.id.tv_reject_survey)
    TextView tvReject;

    @Bind(R.id.tv_approve_survey)
    TextView tvApprove;

    private List<Question> mListQuestion;

    private List<User> mListSubmiter;

    private List<User> mListUser;

    private DialogAddQuestionSurvey dialog;

    private QuestionAdapter mQuestionAdapter;

    private UserAdapter mSubmitterAdapter;

    private UserAdapter mUserAdapter;

    private WrapperListUser wrapperListUser;

    InputEditDialog inputDialog;

    int status;

    @Inject
    NewSurveyPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_survey);
        ButterKnife.bind(this);
        NewSurveyComponent component = DaggerNewSurveyComponent.builder()
                .newSurveyModule(new NewSurveyModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();



        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(getString(R.string.new_survey));
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        topBar.setTextViewRight(getString(R.string.save));

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        Survey survey = (Survey) bundle.getSerializable(SURVEY);
        status = bundle.getInt(SurveyActivity.KEY_FROM, -1);

        if (survey != null) {
            if (survey.getType() == TYPE_CROSS) {
                updateTypeSurvey(chkTypeCross);
            } else if (survey.getType() == TYPE_NORMAL) {
                updateTypeSurvey(chkTypeNormal);
            }
        }

        if (status != SurveyActivity.KEY_ADD) {

            llEditTitle.setVisibility(View.GONE);
            llAddQuestion.setVisibility(View.GONE);
            llAddUser.setVisibility(View.GONE);
            llAddApprovedBy.setVisibility(View.GONE);

            if (status == SurveyActivity.KEY_APPROVED || status == SurveyActivity.KEY_MINE){
                tvReject.setVisibility(View.VISIBLE);
                tvReject.setText("Send");
            }else {
                tvReject.setVisibility(View.VISIBLE);
                tvApprove.setVisibility(View.VISIBLE);
            }

            mPresenter.getAllSurveyItem();
            mQuestionAdapter = new QuestionAdapter(this, mPresenter.getListQuestion());

            mPresenter.getSubmiter();
            mSubmitterAdapter = new UserAdapter(this,mPresenter.getListUser());
        }
        else {
            edtTitle.setEnabled(true);

            mListQuestion = new ArrayList<>();
            mQuestionAdapter = new QuestionAdapter(this, mListQuestion);

            mListSubmiter = new ArrayList<>();
            mSubmitterAdapter = new UserAdapter(this,mListSubmiter);

            mListUser = new ArrayList<>();
            mUserAdapter = new UserAdapter(this,mListUser);

        }

        tvReject.setVisibility(View.VISIBLE);

        LinearLayoutManager llmQuestion =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRcvQuestion.setLayoutManager(llmQuestion);
        mRcvQuestion.setHasFixedSize(false);
        mRcvQuestion.setAdapter(mQuestionAdapter);

        LinearLayoutManager llmSubmiter =
                new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mRcvSubmiter.setLayoutManager(llmSubmiter);
        mRcvSubmiter.setHasFixedSize(false);
        mRcvSubmiter.setAdapter(mSubmitterAdapter);

        LinearLayoutManager llmUser =
                new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mRcvUser.setLayoutManager(llmUser);
        mRcvUser.setHasFixedSize(false);
        mRcvUser.setAdapter(mUserAdapter);
    }

    protected void initListener() {
        chkTypeCross.setEnabled(false);
        chkTypeNormal.setEnabled(false);
        llAddQuestion.setOnClickListener(this);
        llAddUser.setOnClickListener(this);
        tvReject.setOnClickListener(this);
        tvApprove.setOnClickListener(this);


        if (status == SurveyActivity.KEY_ADD){
            chkTypeCross.setEnabled(true);
            chkTypeNormal.setEnabled(true);
            chkTypeCross.setOnClickListener(this);
            chkTypeNormal.setOnClickListener(this);
        }

        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

            }
        });

        if (status == SurveyActivity.KEY_ADD){
            mQuestionAdapter.setOnItemClickListener(new QuestionAdapter.OnItemClickListener() {
                @Override
                public void deleteOption(int position) {
                    mListQuestion.remove(position);
                    mQuestionAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chk_type_cross:
                updateTypeSurvey(chkTypeCross);
                break;

            case R.id.chk_type_normal:
                updateTypeSurvey(chkTypeNormal);
                break;

            case R.id.ll_add_question:
                dialog = new DialogAddQuestionSurvey(this);
                dialog.show();
                dialog.setOnAddQuestionListener(new DialogAddQuestionSurvey.OnAddQuestionListener() {
                    @Override
                    public void addQuestion(Question question) {
                        mListQuestion.add(question);
                    }
                });
                break;

            case R.id.ll_add_user:
                Intent intent = new Intent(NewSurveyActivity.this, UserActivity.class);
                intent.putExtra(
                        UserActivity.KEY_FROM,
                        UserActivity.FROM_NEW_SURVEY
                );

                startActivityForResult(intent, REQUEST_CODE_USER);
                break;

            case R.id.tv_reject_survey:
                tvReject.setSelected(true);
                tvApprove.setSelected(false);
               /* InputTextDialog textDialog = new InputTextDialog(this, "", new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {

                    }
                });
                textDialog.show();*/
                inputDialog = new InputEditDialog(NewSurveyActivity.this, "", new InputEditDialog.OnDoneListener() {
                    @Override
                    public void onDone(String text) {
                        inputDialog.dismiss();
                    }
                });
                inputDialog.show();
                break;
            case R.id.tv_approve_survey:
                tvReject.setSelected(false);
                tvApprove.setSelected(true);
        }
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getDataSuccess() {
        mQuestionAdapter.notifyDataSetChanged();
        mSubmitterAdapter.notifyDataSetChanged();
        hideLoading();
    }

    @Override
    public void getDataFailure(String error) {
        hideLoading();
        showToast(error);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_USER) {
            if (resultCode == Activity.RESULT_OK) {
                //day la du lieu nhan dc
                wrapperListUser = (WrapperListUser) data.getSerializableExtra("LIST_USER");
                for (int i = 0; i < wrapperListUser.getMyUsers().size(); i++) {
                    Log.d("TAG", wrapperListUser.getMyUsers().get(i).getDisplayName());
                }

                mListUser.addAll(wrapperListUser.getMyUsers());
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    public void updateTypeSurvey(CheckBox chk) {
        chkTypeCross.setChecked(false);
        chkTypeNormal.setChecked(false);
        chk.setChecked(true);
    }
}