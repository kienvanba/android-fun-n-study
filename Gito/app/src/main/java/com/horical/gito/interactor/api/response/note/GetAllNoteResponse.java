package com.horical.gito.interactor.api.response.note;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Note;

import java.util.List;

/**
 * Created by Luong on 20-Mar-17.
 */

public class GetAllNoteResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    public List<Note> notes;
}