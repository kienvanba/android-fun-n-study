package com.horical.gito.mvp.myProject.gantt.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.gantt.injection.module.GanttModule;
import com.horical.gito.mvp.myProject.gantt.view.GanttActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = GanttModule.class)
public interface GanttComponent {
    void inject(GanttActivity activity);
}
