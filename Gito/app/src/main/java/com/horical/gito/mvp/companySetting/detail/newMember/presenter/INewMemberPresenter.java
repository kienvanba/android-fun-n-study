package com.horical.gito.mvp.companySetting.detail.newMember.presenter;

import com.horical.gito.model.Member;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface INewMemberPresenter {
    void createMember(String name, String pass, String email);

}
