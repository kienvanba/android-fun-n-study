package com.horical.gito.mvp.companySetting.list.fragment.projectrole.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.fragment.projectrole.injection.module.ProjectRoleModule;
import com.horical.gito.mvp.companySetting.list.fragment.projectrole.view.ProjectRoleFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = ProjectRoleModule.class)
public interface ProjectRoleComponent {
    void inject(ProjectRoleFragment fragment);
}
