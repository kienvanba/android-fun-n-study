package com.horical.gito.mvp.salary.bonus.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.bonus.list.injection.module.ListBonusModule;
import com.horical.gito.mvp.salary.bonus.list.view.ListBonusActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ListBonusModule.class)
public interface IListBonusComponent {
    void inject(ListBonusActivity activity);
}
