package com.horical.gito.mvp.logTime.presenter;

import com.google.gson.Gson;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.project.GetLogTimeRequest;
import com.horical.gito.interactor.api.request.project.UpdateLogTimeRequest;
import com.horical.gito.interactor.api.response.project.GetLogTimeResponse;
import com.horical.gito.interactor.api.request.project.GetMembersHaveLogTimeRequest;
import com.horical.gito.interactor.api.response.project.GetMembersHaveLogTimeResponse;
import com.horical.gito.interactor.api.response.project.GetMemberResponse;
import com.horical.gito.interactor.api.response.project.UpdateLogTimeResponse;
import com.horical.gito.model.LogTime;
import com.horical.gito.model.MemberAndRole;
import com.horical.gito.model.RoleCompany;
import com.horical.gito.model.Section;
import com.horical.gito.model.User;
import com.horical.gito.model.wrapperModel.WrapperListUser;
import com.horical.gito.mvp.logTime.adapter.LogTimeAdapter;
import com.horical.gito.mvp.logTime.adapter.LogTimeItem;
import com.horical.gito.mvp.logTime.view.ILogTimeView;
import com.horical.gito.mvp.popup.dateSelected.DateDto;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.utils.LogUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LogTimePresenter extends BasePresenter implements ILogTimePresenter {

    private static final String TAG = LogUtils.makeLogTag(LogTimePresenter.class);

    private List<User> memberList;
    private List<Section> sections;
    private DateDto currentDateFilter;
    private int currentType = 4;

    private ILogTimeView getView() {
        return (ILogTimeView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        memberList = new ArrayList<>();
        sections = new ArrayList<>();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void getMemberData() {

        getView().showLoading();
        String projectId = GitOStorage.getInstance().getProject().getId();
        getApiManager().getMember(projectId, new ApiCallback<GetMemberResponse>() {
            @Override
            public void success(GetMemberResponse res) {
                memberList.clear();

                for (MemberAndRole memberAndRole : res.memberList) {
                    User user = memberAndRole.getUserId();
                    if (user.getRoleCompany() == null)
                        user.setRoleCompany(new RoleCompany());
                    user.getRoleCompany()
                            .setRoleName(memberAndRole
                                    .getRoleId().roleName);
                    user.setSelected(true);
                    memberList.add(user);
                }

                Collections.sort(memberList, new Comparator<User>() {
                    @Override
                    public int compare(User o1, User o2) {
                        return o1.getDisplayName().compareTo(o2.getDisplayName());
                    }
                });

                getView().hideLoading();
                getView().onGetMembersSuccess();
            }

            @Override
            public void failure(RestError error) {
                LogUtils.LOGE(TAG, new Gson().toJson(error));
                getView().hideLoading();
                getView().onGetMembersFailure(error.message);
            }
        });
    }

    @Override
    public void getLogTimes() {

        getView().showLoading();

        String projectId = GitOStorage.getInstance().getProject().getId();
        final GetMembersHaveLogTimeRequest request = new GetMembersHaveLogTimeRequest();
        request.startDate = currentDateFilter.getStart();
        request.endDate = currentDateFilter.getEnd();

        List<String> creatorIds = new ArrayList<>();
        for (User user : memberList) {
            if (user.isSelected()) {
                creatorIds.add(user.get_id());
            }
        }
        request.creatorIds = creatorIds;

        getApiManager().getLogTimes(projectId, request, new ApiCallback<GetMembersHaveLogTimeResponse>() {
            @Override
            public void success(GetMembersHaveLogTimeResponse res) {

                for (User user : memberList) {
                    user.setNeedApprove(false);
                }

                if (res.logTimes.isEmpty()) {
                    getView().onGetLogTimeSuccess(false);
                    getView().hideLoading();
                } else {
                    for (LogTime logTime : res.logTimes) {
                        for (User user : memberList) {
                            if (logTime.get_id().equalsIgnoreCase(user.get_id())
                                    && logTime.getTotal() > 0) {
                                user.setTotalLogTime(logTime.getTotal());
                                user.setNeedApprove(true);
                            }
                        }
                    }

                    Collections.sort(memberList, new Comparator<User>() {
                        @Override
                        public int compare(User o1, User o2) {
                            return Integer.valueOf(o2.getTotalLogTime()).compareTo(o1.getTotalLogTime());
                        }
                    });

                    getView().hideLoading();
                    getView().onGetLogTimesSuccess();
                }
            }

            @Override
            public void failure(RestError error) {
                getView().onGetLogTimesFailure(error.message);
                getView().hideLoading();
            }
        });
    }

    @Override
    public void getLogTime(int position) {

        getView().showLoading();

        String projectId = GitOStorage.getInstance().getProject().getId();
        final GetLogTimeRequest request = new GetLogTimeRequest();
        request.startDate = currentDateFilter.getStart();
        request.endDate = currentDateFilter.getEnd();
        request.creatorId = memberList.get(position).get_id();
        request.status = currentType;

        getApiManager().getLogTime(projectId, request, new ApiCallback<GetLogTimeResponse>() {
            @Override
            public void success(GetLogTimeResponse res) {
                sections.clear();

                if (res.logTimes.isEmpty()) {
                    getView().onGetLogTimeSuccess(false);
                    getView().hideLoading();
                } else {
                    for (LogTime logTime : res.logTimes) {
                        String date = DateUtils.formatDate(logTime.getLogTimeDate());
                        Section section = getHasTimeLog(date);

                        if (section != null) {
                            LogTimeItem timeItem = new LogTimeItem(logTime);
                            section.getItems().add(timeItem);
                        } else {
                            section = new Section(0, date);
                            LogTimeItem timeItem = new LogTimeItem(logTime);
                            section.getItems().add(timeItem);
                            sections.add(section);
                        }
                    }

                    getView().onGetLogTimeSuccess(true);
                    getView().hideLoading();
                }
            }

            @Override
            public void failure(RestError error) {
                getView().onGetLogTimeFailure(error.message);
                getView().hideLoading();
            }
        });
    }

    @Override
    public void approveAndRejectLogTime(int sectionIndex, int itemIndex, String rejectMsg) {

        getView().showLoading();
        String projectId = GitOStorage.getInstance().getProject().getId();
        final LogTime logTime = ((LogTimeItem) sections.get(sectionIndex).getItems().get(itemIndex)).getLogTime();
        final int status = rejectMsg == null ? LogTimeAdapter.APPROVE : LogTimeAdapter.REJECT;
        UpdateLogTimeRequest request = new UpdateLogTimeRequest(status, rejectMsg);

        getApiManager().updateLogTime(
                projectId, logTime.get_id(), request, new ApiCallback<UpdateLogTimeResponse>() {
                    @Override
                    public void success(UpdateLogTimeResponse res) {
                        logTime.setStatus(status);

                        boolean hasLogTimeNeedApprove = false;
                        for (Section section : sections) {
                            for (IRecyclerItem item : section.getItems()) {
                                LogTime lg = ((LogTimeItem) item).getLogTime();
                                if (lg.getStatus() == LogTimeAdapter.NEW) {
                                    hasLogTimeNeedApprove = true;
                                }
                            }
                        }
                        String userId = logTime.getCreatorId().get_id();
                        for (User user : memberList) {
                            if (userId.equalsIgnoreCase(user.get_id())) {
                                user.setNeedApprove(hasLogTimeNeedApprove);
                                break;
                            }
                        }

                        getView().approveOrRejectSuccess();
                        getView().hideLoading();
                    }

                    @Override
                    public void failure(RestError error) {
                        getView().approveOrRejectFailure(error.message);
                        getView().hideLoading();
                    }
                }
        );
    }

    private Section getHasTimeLog(String date) {

        for (Section section : sections) {
            if (section.getTitle().equalsIgnoreCase(date)) {
                return section;
            }
        }
        return null;
    }

    public List<Section> getLogTimeList() {
        return this.sections;
    }

    public List<User> getMemberList() {
        return this.memberList;
    }

    public DateDto getCurrentDateFilter() {
        return currentDateFilter;
    }

    public void setCurrentDateFilter(DateDto currentDateFilter) {
        this.currentDateFilter = currentDateFilter;
    }

    public int getCurrentType() {
        return currentType;
    }

    public void setCurrentType(int currentType) {
        this.currentType = currentType;
    }

    public WrapperListUser getUserList() {
        return new WrapperListUser(memberList);
    }

    public void setUserPicked(WrapperListUser wrapperListUser) {

        for (User user : memberList) {
            user.setSelected(false);
            user.setNeedApprove(false);
            for (User u : wrapperListUser.getMyUsers()) {
                if (user.get_id().equalsIgnoreCase(u.get_id())) {
                    user.setSelected(true);
                }
            }
        }
        getView().onGetMembersSuccess();
    }
}
