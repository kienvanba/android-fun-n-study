package com.horical.gito.mvp.myProject.document.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.AppConstants;
import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;

import java.util.List;

public class DocumentAdapter extends BaseRecyclerAdapter {
    private int mRvLayoutStyle;
    public DocumentAdapter(Context context, List<IRecyclerItem> items, int layoutStyle) {
        super(items, context);
        this.mRvLayoutStyle = layoutStyle;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (mRvLayoutStyle){
            case AppConstants.GRID_STYLE:
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_document_grid,parent,false);
                return new DocumentHolder(view);
            case AppConstants.LIST_STYLE:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_document_list,parent,false);
                return new DocumentHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        switch (getItemViewType(position)){
            case AppConstants.DOCUMENT_ITEM:
                DocumentHolder mDocumentHolder = (DocumentHolder) holder;
                DocumentItemBody mBody = (DocumentItemBody) mItems.get(position);
                int icon = R.drawable.ic_music_file;
                String sizeType = "";
                if(mBody.document==null) return;
                switch (mBody.document.getLastname()){
                    case AppConstants.FOLDER:
                            icon = R.drawable.ic_blue_folder;
                            break;
                    case AppConstants.DOC:
                            icon = R.drawable.ic_document_file;
                            sizeType = "kb";
                            break;
                    case AppConstants.RAR:
                    case AppConstants.ZIP:
                        icon = R.drawable.ic_rar_file;
                        sizeType = "kb";
                        break;
                }
                mDocumentHolder.mImIcon.setImageDrawable(mContext.getResources().getDrawable(icon));

                if(mRvLayoutStyle==AppConstants.GRID_STYLE){
                    mDocumentHolder .mTvName.setText(mBody.document.getName().length()>=25?mBody.document.getName().substring(0,22)+"...":mBody.document.getName());
                }else{
                    mDocumentHolder .mTvName.setText(mBody.document.getName());
                }
                String size = mBody.document.getSize()+" "+sizeType;
                mDocumentHolder.mTvSize.setText(size);
                mDocumentHolder.mTvDate.setText(mBody.document.getCreatedDate());
                break;
        }
    }
}
