package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Dragonoid on 12/30/2016
 */

public class LeavingApprover implements Serializable {

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("userId")
    @Expose
    public String userId;

    @SerializedName("companyId")
    @Expose
    public String companyId;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("level")
    @Expose
    public String level;

    @SerializedName("showMyPage")
    @Expose
    public Boolean showMyPage;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("createdDate")
    @Expose
    public String createdDate;

    @SerializedName("language")
    @Expose
    public String language;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("country")
    @Expose
    public String country;

    @SerializedName("phone")
    @Expose
    public int phone;

    @SerializedName("avatar")
    @Expose
    public String avatar;

    @SerializedName("displayName")
    @Expose
    public String displayName;
}
