package com.horical.gito.widget.chartview.circle;

import android.view.animation.Animation;
import android.view.animation.Transformation;


public class CircleAngleAnimation extends Animation {

    private CircleChartView circle;
    private float newAngle;
    private float newPercent;

    public CircleAngleAnimation(CircleChartView circle) {
        this.newAngle = circle.getPercent() * CircleChartView.START_ANGLE_POINT / 100;
        this.newPercent = circle.getPercent();
        this.circle = circle;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation transformation) {
        float angle = (newAngle * interpolatedTime);
        float percent = (newPercent * interpolatedTime);

        circle.setAngleDraw(angle);
        circle.setPercentDraw(percent);
        circle.requestLayout();
    }
}

