package com.horical.gito.mvp.accounting.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.accounting.detail.injection.module.AccountingDetailModule;
import com.horical.gito.mvp.accounting.detail.view.AccountingDetailActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = AccountingDetailModule.class)
public interface AccountingDetailComponent {
    void inject(AccountingDetailActivity activity);
}
