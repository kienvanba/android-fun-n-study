package com.horical.gito.mvp.roomBooking.detail.fragment.day.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.roomBooking.detail.fragment.day.injection.module.DayModule;
import com.horical.gito.mvp.roomBooking.detail.fragment.day.view.DayFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = DayModule.class)
public interface DayComponent {
    void inject(DayFragment fragment);
}
