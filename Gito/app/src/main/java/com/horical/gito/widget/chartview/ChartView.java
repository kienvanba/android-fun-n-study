package com.horical.gito.widget.chartview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.horical.gito.R;

public abstract class ChartView extends View {

    /**
     * The paint for draw
     **/
    protected Paint mPaint;

    /**
     * Text of index
     **/
    protected float mPercent;

    public ChartView(Context context) {
        super(context);
    }

    public ChartView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setAntiAlias(true);

        /** Get the attributes specified in attrs.xml using the name we included */
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.ChartView, 0, 0);

        mPercent = typedArray.getInt(R.styleable.ChartView_percent, 0);
    }

    protected float convertDp(int px) {
        return (px * getContext().getResources().getDisplayMetrics().density + 0.5f);
    }

    public float getPercent() {
        return mPercent;
    }

    public void setPercent(float mPercent) {
        this.mPercent = mPercent;
    }
}
