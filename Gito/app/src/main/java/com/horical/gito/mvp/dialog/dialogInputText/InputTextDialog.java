package com.horical.gito.mvp.dialog.dialogInputText;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;


public class InputTextDialog extends Dialog {

    public static final int PHONE_TEXT = 1;
    public static final int EMAIL_TEXT = 2;
    public static final int DEFAULT_TEXT = 3;

    @Bind(R.id.edt_text)
    EditText edtText;

    @Bind(R.id.edt_text_phone)
    EditText edtTextPhone;

    @Bind(R.id.edit_text_email)
    EditText edtTextEmail;

    @Bind(R.id.tv_cancel)
    TextView tvCancel;

    @Bind(R.id.tv_done)
    TextView tvDone;

    private int type;

    public InputTextDialog(Context context, int type, OnClickListener callBack) {
        super(context, R.style.DialogDefault);
        this.mCallBack = callBack;
        this.type = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_input_text);
        ButterKnife.bind(this);

        if (type == PHONE_TEXT) {
            edtTextPhone.setVisibility(View.VISIBLE);
            edtText.setVisibility(View.GONE);
            edtTextEmail.setVisibility(View.GONE);
        } else if (type == EMAIL_TEXT) {
            edtTextPhone.setVisibility(View.GONE);
            edtText.setVisibility(View.GONE);
            edtTextEmail.setVisibility(View.VISIBLE);
        } else {
            edtTextPhone.setVisibility(View.GONE);
            edtText.setVisibility(View.VISIBLE);
            edtTextEmail.setVisibility(View.GONE);
        }

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                edtText.clearFocus();
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type == PHONE_TEXT) {
                    mCallBack.onSave(edtTextPhone.getText().toString());
                } else if (type == EMAIL_TEXT) {
                    mCallBack.onSave(edtTextEmail.getText().toString());
                } else {
                    mCallBack.onSave(edtText.getText().toString());
                }
                dismiss();
                hideKeyboard();
            }
        });
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.RESULT_HIDDEN, 0);
    }

    private OnClickListener mCallBack;

    public interface OnClickListener {
        void onSave(String text);
    }
}
