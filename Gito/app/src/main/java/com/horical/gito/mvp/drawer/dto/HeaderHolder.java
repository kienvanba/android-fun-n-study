package com.horical.gito.mvp.drawer.dto;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nxon on 3/15/17.
 */

public class HeaderHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.menu_iv_avatar_company)
    public CircleImageView mIvLogoCompany;

    @Bind(R.id.menu_tv_name_company)
    public GitOTextView mTvNameCompany;

    @Bind(R.id.menu_pgr_icon_loading)
    public ProgressBar mProgressBar;

    public HeaderHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
