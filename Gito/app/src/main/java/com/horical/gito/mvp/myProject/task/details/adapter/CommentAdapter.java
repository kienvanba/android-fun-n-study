package com.horical.gito.mvp.myProject.task.details.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Comments;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by thanhle on 3/10/17.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentHolder> {

    private Context mContext;
    private List<Comments> mList;
    private OnItemClickListener mOnItemClickListener;

    public CommentAdapter(Context context, List<Comments> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_task_comments, parent, false);
        return new CommentHolder(view);
    }

    public List<Comments> getList() {
        return mList;
    }

    public void setList(List<Comments> mList) {
        this.mList = mList;
    }

    @Override
    public void onBindViewHolder(CommentHolder holder, int position) {
        Comments comments = mList.get(position);

        holder.tvDescription.setText(comments.getDesc());

        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.llEditDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class CommentHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_sub_name)
        TextView tvSubName;

        @Bind(R.id.img_picture)
        CircleImageView imgPicture;

        @Bind(R.id.edit_name)
        TextView tvName;

        @Bind(R.id.tv_hour)
        TextView tvHour;

        @Bind(R.id.ll_delete)
        LinearLayout llDelete;

        @Bind(R.id.tv)
        TextView tvDescription;

        @Bind(R.id.ll_edit_desc)
        LinearLayout llEditDesc;

        @Bind(R.id.tv_more)
        TextView tvMore;

        View view;

        public CommentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}
