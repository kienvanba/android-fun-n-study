package com.horical.gito.mvp.popup.dateSelected;


import com.horical.gito.utils.DateUtils;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;


public class DateDto implements Serializable {
    private String typeDate;
    private Date start;
    private Date end;
    private boolean isSelected;

    public DateDto(String typeDate, Date start, Date end) {
        this.typeDate = typeDate;
        this.start = start;
        this.end = end;
        isSelected = false;
    }

    public DateDto() {

    }

    private Date getDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.set(Calendar.YEAR, Calendar.MONTH, Calendar.DATE, 0, 0, 0);
        return calendar.getTime();
    }

    public String getTypeDate() {
        return typeDate;
    }

    public String showDateStartEnd() {
        if (start != null && end != null) {
            if (typeDate.equalsIgnoreCase("Yesterday") || typeDate.equalsIgnoreCase("today")) {
                return DateUtils.formatDate(start);
            } else {
                return DateUtils.formatDate(start) + " - " + DateUtils.formatDate(end);
            }
        } else return "";
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public void setTypeDate(String typeDate) {
        this.typeDate = typeDate;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
