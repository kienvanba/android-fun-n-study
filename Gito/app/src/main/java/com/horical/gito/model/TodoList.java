package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class TodoList implements Serializable {

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("important")
    @Expose
    public Boolean important;

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("attachFiles")
    @Expose
    public List<TokenFile> attachFiles;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Boolean getImportant() {
        return important;
    }

    public void setImportant(Boolean important) {
        this.important = important;
    }

    public List<TokenFile> getAttachFiles() {
        return attachFiles;
    }
}
