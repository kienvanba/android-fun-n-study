package com.horical.gito.mvp.roomBooking.detail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IView;
import com.horical.gito.mvp.roomBooking.detail.view.IRoomDetailView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lemon on 3/21/2017.
 */

public class RoomDetailPresenter extends BasePresenter implements IRoomDetailPresenter{
    private List<String> mList;

    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IRoomDetailView getView(){
        return (IRoomDetailView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        initData();
        getEventManager().register(this);
    }

    private void initData() {
        mList.add("Day");
        mList.add("Week");
        mList.add("Month");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<String> getListData(){
        return mList;
    }
}
