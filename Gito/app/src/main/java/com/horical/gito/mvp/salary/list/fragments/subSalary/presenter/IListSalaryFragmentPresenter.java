package com.horical.gito.mvp.salary.list.fragments.subSalary.presenter;

import com.horical.gito.mvp.salary.dto.salary.ListSalaryDto;

import java.util.List;

public interface IListSalaryFragmentPresenter {
    List<ListSalaryDto> getSalaries();

    void setSalaries(List<ListSalaryDto> mSalaries);
}
