package com.horical.gito.mvp.todolist.todo.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.model.TodoList;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TodoListAdapter extends RecyclerView.Adapter<TodoListAdapter.TodoListHolder> {

    private List<TodoList> mList;
    private Context mContext;
    OnItemClickListener mOnItemClickListener;

    public TodoListAdapter(Context context, List<TodoList> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public TodoListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_todolist, parent, false);
        return new TodoListHolder(view);
    }

    @Override
    public void onBindViewHolder(TodoListHolder holder, final int position) {

        TodoList todoList = mList.get(position);
        holder.tvTodoTitle.setText(todoList.title);
        holder.tvTodoDate.setText(DateUtils.formatDate(todoList.getStartDate(), "dd/MM/yyyy hh:mm a"));

        holder.ivTodoDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDelete(position);
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(position);
            }
        });
    }

    public List<TodoList> getListTodoList() {
        return mList;
    }

    public void setListTodoList(List<TodoList> mList) {
        this.mList = mList;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class TodoListHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_todo_title)
        GitOTextView tvTodoTitle;

        @Bind(R.id.cb_todo_important)
        CheckBox cbImportant;

        @Bind(R.id.tv_todo_date)
        GitOTextView tvTodoDate;

        @Bind(R.id.tv_todo_item)
        GitOTextView tvTodoItem;

        @Bind(R.id.imv_todo_delete)
        ImageView ivTodoDelete;

        View view;


        public TodoListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }


    public void setOnItemClickListener(OnItemClickListener callBack) {
        this.mOnItemClickListener = callBack;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDelete(int position);
    }
}
