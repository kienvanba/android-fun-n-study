package com.horical.gito.mvp.salary.detail.salary.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.salary.detail.salary.view.IDetailSalaryView;
import com.horical.gito.mvp.salary.dto.metric.SummaryDto;
import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;

import java.util.ArrayList;
import java.util.List;

public class DetailSalaryPresenter extends BasePresenter implements IDetailSalaryPresenter {

    public void attachView(IDetailSalaryView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IDetailSalaryView getView() {
        return (IDetailSalaryView) getIView();
    }

    private List<ListStaffDto> mStaffs;
    private List<SummaryDto> mSummaries;

    private void setDummyData() {
        //init summaries
        getSummaries().add(new SummaryDto("Summary 1", "S1", 100));
        getSummaries().add(new SummaryDto("Summary 2", "S2", 200));
        getSummaries().add(new SummaryDto("Summary 3", "S3", 300));
        getSummaries().add(new SummaryDto("Summary 4", "S4", 400));
        getSummaries().add(new SummaryDto("Summary 5", "S5", 500));
        getSummaries().add(new SummaryDto("Summary 6", "S6", 600));

        //init staff
        getStaffs().add(new ListStaffDto("Nam A", "Admin", "Hourly", 200, 300, 200));
        getStaffs().add(new ListStaffDto("Nam B", "Coder", "Monthly", 500, 100, 300));
        getStaffs().add(new ListStaffDto("Nam C", "Tester", "Monthly", 400, 200, 250));
        getStaffs().add(new ListStaffDto("Nam D", "QA", "Monthly", 300, 240, 150));
        getStaffs().add(new ListStaffDto("Nam E", "BI", "Monthly", 600, 150, 100));
        getStaffs().add(new ListStaffDto("Nam F", "BI", "Monthly", 600, 150, 100));
        getStaffs().add(new ListStaffDto("Nam G", "BI", "Monthly", 600, 150, 100));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        setDummyData();
    }

    @Override
    public List<ListStaffDto> getStaffs() {
        if (mStaffs == null) {
            mStaffs = new ArrayList<>();
        }
        return mStaffs;
    }

    @Override
    public List<SummaryDto> getSummaries() {
        if (mSummaries == null) {
            mSummaries = new ArrayList<>();
        }
        return mSummaries;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
