package com.horical.gito.mvp.myProject.summary.presenter;

import com.google.gson.Gson;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.summary.GetSummaryResponse;
import com.horical.gito.model.Issue;
import com.horical.gito.model.Summary;
import com.horical.gito.model.Version;
import com.horical.gito.mvp.myProject.summary.adapter.SummaryAdapter;
import com.horical.gito.mvp.myProject.summary.dto.ProgressItem;
import com.horical.gito.model.Section;
import com.horical.gito.mvp.myProject.summary.dto.TaskItem;
import com.horical.gito.mvp.myProject.summary.dto.TaskTitleItem;
import com.horical.gito.mvp.myProject.summary.dto.VersionItem;
import com.horical.gito.mvp.myProject.summary.view.ISummaryView;
import com.horical.gito.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.makeLogTag;


public class SummaryPresenter extends BasePresenter implements ISummaryPresenter {

    private static final String TAG = makeLogTag(SummaryPresenter.class);

    private Summary summary;
    private List<Section> mSections;

    public ISummaryView getView() {
        return (ISummaryView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSections = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public static String getTAG() {
        return TAG;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    @Override
    public void getData() {
        mSections.clear();

        String projectId = GitOStorage.getInstance().getProject().getId();

        getView().showLoading();
        getApiManager().getSummary(projectId, new ApiCallback<GetSummaryResponse>() {
            @Override
            public void success(GetSummaryResponse res) {
                boolean hasData = false;

                Section section;
                Summary summary = res.getSummary();

                if (summary.getProgresses() != null && !summary.getProgresses().isEmpty()) {
                    // Progress Process
                    section = new Section(SummaryAdapter.SUMMARY_HEADER_TYPE, "Progress");
                    ProgressItem progressItem = new ProgressItem(res.getSummary().getProgresses());
                    section.getItems().add(progressItem);
                    mSections.add(section);

                    if (progressItem.getVersions() != null && !progressItem.getVersions().isEmpty()) {
                        // Version Process
                        for (Version version : progressItem.getVersions()) {
                            if (version.get_id() != null) {
                                section = new Section(SummaryAdapter.SUMMARY_HEADER_TYPE, version.getName());
                                section.getItems().add(new VersionItem(version));
                                mSections.add(section);
                            }
                        }
                    }
                    hasData = true;
                }

                if (summary.getIssues() != null && !summary.getIssues().isEmpty()) {
                    // Task Process
                    section = new Section(SummaryAdapter.SUMMARY_HEADER_TYPE, "Task");
                    section.getItems().add(new TaskTitleItem());

                    for (Issue issue : res.getSummary().getIssues()) {
                        TaskItem item = new TaskItem(issue);
                        section.getItems().add(item);
                    }
                    mSections.add(section);
                    hasData = true;
                }

                // Process done
                getView().onGetDataSuccess(hasData);
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LogUtils.LOGE(TAG, new Gson().toJson(error));
                getView().hideLoading();
            }
        });
    }

    public List<Section> getSections() {
        return mSections;
    }

    public void setSections(List<Section> mSections) {
        this.mSections = mSections;
    }
}
