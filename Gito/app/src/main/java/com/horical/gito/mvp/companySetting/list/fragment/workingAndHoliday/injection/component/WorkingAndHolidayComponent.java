package com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.injection.module.WorkingAndHolidayModule;
import com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.view.WorkingAndHolidayFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = WorkingAndHolidayModule.class)
public interface WorkingAndHolidayComponent {
    void inject(WorkingAndHolidayFragment fragment);
}
