package com.horical.gito.mvp.report.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.interactor.api.response.report.MemberResponse;
import com.horical.gito.model.ResultsReport;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ReportMemberAdapter extends RecyclerView.Adapter<ReportMemberAdapter.ViewHolderMember> {

    private Context context;
    private List<ResultsReport> list;
    private OnItemClickListener mCallBack;

    public ReportMemberAdapter(Context context, List<ResultsReport> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ReportMemberAdapter.ViewHolderMember onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_report_member, parent, false);
        return new ViewHolderMember(view);
    }

    @Override
    public void onBindViewHolder(ReportMemberAdapter.ViewHolderMember holder, final int position) {

        if (TextUtils.isEmpty(list.get(position).getDataUser().getAvatar().getTokenFile())) {
            holder.tvPhoto.setVisibility(View.VISIBLE);
            holder.imvPhoto.setVisibility(View.GONE);
            holder.tvPhoto.setText(list.get(position).getDataUser().showNamePhoto());
        } else {
            holder.imvPhoto.setVisibility(View.VISIBLE);
            holder.tvPhoto.setVisibility(View.GONE);
            ImageLoader.load(context, CommonUtils.getURL(list.get(position).getDataUser().getAvatar()), holder.imvPhoto, null);
        }


        holder.tvName.setText(list.get(position).getDataUser().getDisplayName());
        holder.tvDepartment.setText(list.get(position).getDataUser().getLevel().getName());

        holder.itemView.setSelected(list.get(position).isSelected());

        if (list.get(position).isSubmitted()) {
            holder.vSubmitted.setVisibility(View.VISIBLE);
        } else {
            holder.vSubmitted.setVisibility(View.INVISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallBack != null) {
                    mCallBack.onClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolderMember extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_photo)
        TextView tvPhoto;

        @Bind(R.id.imv_photo)
        CircleImageView imvPhoto;

        @Bind(R.id.tv_name_member)
        TextView tvName;

        @Bind(R.id.tv_department)
        TextView tvDepartment;

        @Bind(R.id.tv_notify_submitted)
        View vSubmitted;

        public ViewHolderMember(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void setOnItemClickListener(OnItemClickListener callBack) {

        this.mCallBack = callBack;
    }

    public interface OnItemClickListener {
        void onClick(int position);
    }
}
