package com.horical.gito.mvp.companySetting.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.companySetting.list.presenter.CompanySettingPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by thanhle on 3/13/17.
 */

@Module
public class CompanySettingModule {
    @Provides
    @PerActivity
    CompanySettingPresenter provideCompanySettingPresenter(){
        return new CompanySettingPresenter();
    }
}
