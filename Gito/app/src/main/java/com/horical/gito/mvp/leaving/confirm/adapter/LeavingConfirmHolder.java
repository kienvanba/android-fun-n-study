package com.horical.gito.mvp.leaving.confirm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.IRecyclerItem;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 1/17/2017.
 */

public class LeavingConfirmHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.leaving_tv_confirm_date)
    TextView mDate;
    @Bind(R.id.leaving_lv_confirm_detail)
    RecyclerView mListDetail;

    public LeavingConfirmHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void setListDetail(List<IRecyclerItem> list, Context context, LeavingConfirmAdapter.ApproveRejectFromChild callback) {

    }
}
