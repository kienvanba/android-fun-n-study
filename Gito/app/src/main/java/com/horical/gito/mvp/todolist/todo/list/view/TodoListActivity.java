package com.horical.gito.mvp.todolist.todo.list.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.dialog.dialogTodoNote.FilterTodoNoteDialog;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.todolist.note.list.view.NoteActivity;
import com.horical.gito.mvp.todolist.todo.detail.view.TodoListDetailActivity;
import com.horical.gito.mvp.todolist.todo.list.adapter.TodoListAdapter;
import com.horical.gito.mvp.todolist.todo.list.injection.component.DaggerTodoListComponent;
import com.horical.gito.mvp.todolist.todo.list.injection.component.TodoListComponent;
import com.horical.gito.mvp.todolist.todo.list.injection.module.TodoListModule;
import com.horical.gito.mvp.todolist.todo.list.presenter.TodoListPresenter;


import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.FROM_MENU;
import static com.horical.gito.AppConstants.KEY_FROM;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class TodoListActivity extends DrawerActivity implements View.OnClickListener, ITodoListView {

    private static final String TAG = makeLogTag(TodoListActivity.class);
    public final static int REQUEST_CODE_ADD_EDIT_TODOLIST = 1;

    @Bind(R.id.action_bar)
    CustomViewTopBar topBar;

    @Bind(R.id.rdb_btn_todo)
    RadioButton rdbTodo;

    @Bind(R.id.rdb_btn_note)
    RadioButton rdbNote;

    @Bind(R.id.todo_refresh_layout)
    SwipeRefreshLayout todoRefresh;

    @Bind(R.id.rcv_todo)
    RecyclerView rvTodo;

    @Inject
    TodoListPresenter mPresenter;

    TodoListAdapter mTodoListAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_todo;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_TODO;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        TodoListComponent component = DaggerTodoListComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .todoListModule(new TodoListModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(getString(R.string.to_do_list));
        int from = getIntent().getIntExtra(KEY_FROM, FROM_MENU);
        mPresenter.setFrom(from);
        if (from == FROM_MENU) {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        } else {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
            lockDrawer();
        }
        topBar.setImageViewRight(
                CustomViewTopBar.DRAWABLE_FILTER,
                CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT
        );

        mTodoListAdapter = new TodoListAdapter(this, mPresenter.getListTodoList());
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvTodo.setLayoutManager(llm);
        rvTodo.setAdapter(mTodoListAdapter);

        showLoading();
        mPresenter.getAllTodoList();
    }

    protected void initListener() {
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                if (mPresenter.getFrom() == FROM_MENU) openDrawer();
                else onBackPressed();
            }

            @Override
            public void onRightButtonOneClicked() {
                Intent intent1 = new Intent(TodoListActivity.this, TodoListDetailActivity.class);
                startActivityForResult(intent1, REQUEST_CODE_ADD_EDIT_TODOLIST);
            }

            @Override
            public void onRightButtonTwoClicked() {
                FilterTodoNoteDialog dialog = new FilterTodoNoteDialog(TodoListActivity.this);
                dialog.show();
            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });

        rdbTodo.setOnClickListener(this);
        rdbNote.setOnClickListener(this);
        todoRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                mPresenter.getAllTodoList();
            }
        });

        mTodoListAdapter.setOnItemClickListener(new TodoListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(TodoListActivity.this, TodoListDetailActivity.class);
                intent.putExtra(TodoListDetailActivity.ARG_TODOLIST, mPresenter.getListTodoList().get(position));
                startActivityForResult(intent, REQUEST_CODE_ADD_EDIT_TODOLIST);
            }

            @Override
            public void onDelete(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(TodoListActivity.this);
                builder.setMessage(R.string.text_Show);
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                showDialog("Loading...");
                                mPresenter.deleteTodoList(mTodoListAdapter.getListTodoList().get(position).getId(), position);
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }

        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rdb_btn_todo:
                break;
            case R.id.rdb_btn_note:
                Intent intent = new Intent(this, NoteActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllTodoListSuccess() {
        dismissDialog();
        todoRefresh.setRefreshing(false);
        mTodoListAdapter.notifyDataSetChanged();
        showToast("Success");
    }

    @Override
    public void getAllTodoListFailure(String error) {
        dismissDialog();
        todoRefresh.setRefreshing(false);
        showToast("Failure");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            showLoading();
            mPresenter.getAllTodoList();
        }
    }

    @Override
    public void deleteTodoListSuccess(int position) {

        hideLoading();
        showToast("Delete Success");
        mTodoListAdapter.getListTodoList().remove(position);
        mTodoListAdapter.notifyItemRemoved(position);
        mTodoListAdapter.notifyItemRangeChanged(position, mTodoListAdapter.getItemCount());
    }

    @Override
    public void deleteTodoListFailure() {
        hideLoading();
        showToast("failure");
    }
}
