package com.horical.gito.mvp.roomBooking.detail.fragment.week.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IView;

public class WeekPresenter extends BasePresenter implements IWeekPresenter {
    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
