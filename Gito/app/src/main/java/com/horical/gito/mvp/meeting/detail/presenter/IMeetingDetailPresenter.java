package com.horical.gito.mvp.meeting.detail.presenter;

import java.io.File;

import okhttp3.MediaType;

public interface IMeetingDetailPresenter {

    void createMeeting(String name, String description);

    void updateMeeting(String meetingId, String name, String desc);

    void uploadFile(MediaType type, File file);

}
