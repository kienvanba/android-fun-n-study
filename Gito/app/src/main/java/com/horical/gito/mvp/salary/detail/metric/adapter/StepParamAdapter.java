package com.horical.gito.mvp.salary.detail.metric.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.StepParam;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class StepParamAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<StepParam> items;
    private DetailStaffAdapterListener callback;

    public StepParamAdapter(Context context, List<StepParam> items, DetailStaffAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    public void setDataChanged(List<StepParam> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_detail_metric_step_param, parent, false);
        return new StepParamHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        StepParam stepParam = items.get(position);
        StepParamHolder stepParamHolder = (StepParamHolder) holder;
        stepParamHolder.tvFrom.setText(String.valueOf(stepParam.getFrom()));
        stepParamHolder.tvTo.setText(String.valueOf(stepParam.getTo()));
        stepParamHolder.tvValue.setText(String.valueOf(stepParam.getValue()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class StepParamHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.tv_from)
        TextView tvFrom;

        @Bind(R.id.tv_to)
        TextView tvTo;

        @Bind(R.id.imv_from_to_edit)
        ImageView imvEditFromTo;

        @Bind(R.id.tv_value)
        TextView tvValue;

        @Bind(R.id.imv_value_edit)
        ImageView imvEditValue;

        StepParamHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imvEditFromTo.setOnClickListener(this);
            imvEditValue.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.imv_from_to_edit:
                    callback.onEditFromTo(getAdapterPosition());
                    break;

                case R.id.imv_value_edit:
                    callback.onEditValue(getAdapterPosition());
                    break;

                default:
                    break;
            }
        }
    }

    public interface DetailStaffAdapterListener {
        void onEditFromTo(int position);

        void onEditValue(int position);
    }
}