package com.horical.gito.utils;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class AnimationUtils {

    public static void expand(final View v, final int initialHeight) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ? initialHeight : (int) (initialHeight * interpolatedTime);
                v.requestLayout();
                if (interpolatedTime > 0.5) {
                    if (v instanceof ListView) {
                        ((BaseAdapter) ((ListView) v).getAdapter()).notifyDataSetChanged();
                    } else if (v instanceof RecyclerView) {
                        ((RecyclerView) v).getAdapter().notifyDataSetChanged();
                    }
                    if (v instanceof ViewGroup) {
                        for (int index = 0; index < ((ViewGroup) v).getChildCount(); ++index) {
                            View view = ((ViewGroup) v).getChildAt(index);
                            if (view instanceof ListView) {
                                ((BaseAdapter) ((ListView) view).getAdapter()).notifyDataSetChanged();
                            } else if (view instanceof RecyclerView) {
                                ((RecyclerView) view).getAdapter().notifyDataSetChanged();
                            }
                        }
                    }
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        animation.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(animation);
    }

    public static void collapse(final View v, final int initialHeight) {
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        animation.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(animation);
    }

}
