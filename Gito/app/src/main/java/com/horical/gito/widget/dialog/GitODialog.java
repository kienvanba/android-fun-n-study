package com.horical.gito.widget.dialog;

import android.app.Dialog;
import android.content.Context;

public class GitODialog extends Dialog {

    public GitODialog(Context context) {
        super(context);
    }

    public GitODialog(Context context, int themeResId) {
        super(context, themeResId);
    }
}