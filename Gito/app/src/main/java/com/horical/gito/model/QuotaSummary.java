package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/27/17.
 */

public class QuotaSummary implements Serializable {
    @SerializedName("quotaProjectNumber")
    @Expose
    private int quotaProjectNumber;

    @SerializedName("quotaUserMember")
    @Expose
    private int quotaUserMember;

    @SerializedName("quotaSourceCode")
    @Expose
    private int quotaSourceCode;

    @SerializedName("quotaEstate")
    @Expose
    private int quotaEstate;

    public int getQuotaProjectNumber() {
        return quotaProjectNumber;
    }

    public void setQuotaProjectNumber(int quotaProjectNumber) {
        this.quotaProjectNumber = quotaProjectNumber;
    }

    public int getQuotaUserMember() {
        return quotaUserMember;
    }

    public void setQuotaUserMember(int quotaUserMember) {
        this.quotaUserMember = quotaUserMember;
    }

    public int getQuotaSourceCode() {
        return quotaSourceCode;
    }

    public void setQuotaSourceCode(int quotaSourceCode) {
        this.quotaSourceCode = quotaSourceCode;
    }

    public int getQuotaEstate() {
        return quotaEstate;
    }

    public void setQuotaEstate(int quotaEstate) {
        this.quotaEstate = quotaEstate;
    }
}
