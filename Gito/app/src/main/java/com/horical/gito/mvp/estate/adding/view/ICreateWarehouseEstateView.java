package com.horical.gito.mvp.estate.adding.view;

import com.horical.gito.base.IView;

/**
 * Created by hoangsang on 4/12/17.
 */

public interface ICreateWarehouseEstateView extends IView {
    int REQUEST_PERMISSION_CAPTURE_IMAGE = 0;
    int REQUEST_PERMISSION_READ_LIBRARY = 1;

    int REQUEST_CAPTURE_IMAGE = 3;
    int REQUEST_READ_LIBRARY = 4;

    void showLoading();

    void hideLoading();

    void uploadFileSuccess(String url);

    void uploadFileFailed();

    String getName();

    String getDescription();

    void createWarehouseEstateSuccess();

    void createWarehouseEstateFailed();
}
