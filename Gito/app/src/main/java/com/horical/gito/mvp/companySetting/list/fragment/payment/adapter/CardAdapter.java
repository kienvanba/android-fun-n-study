package com.horical.gito.mvp.companySetting.list.fragment.payment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Card;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 30-Nov-16.
 */
public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardHolder> {
    private Context mContext;
    private List<Card> mList;
    private OnItemClickListener mOnItemClickListener;

    public CardAdapter(Context context, List<Card> list) {
        mContext = context;
        mList = list;
    }


    @Override
    public CardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_company_setting_card, parent, false);
        return new CardHolder(view);
    }

    @Override
    public void onBindViewHolder(CardHolder holder, final int position) {
        Card card = mList.get(position);

        holder.tvName.setText(card.getName());
        holder.llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class CardHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.ll_edit)
        LinearLayout llEdit;

        View view;

        public CardHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);
        void onClickEdit(int position);
    }
}
