package com.horical.gito.mvp.myProject.setting.fragment.information.injection.component;


import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.setting.fragment.information.injection.module.InformationModule;
import com.horical.gito.mvp.myProject.setting.fragment.information.view.InformationFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = InformationModule.class)
public interface InformationComponent {
    void inject(InformationFragment fragment);
}
