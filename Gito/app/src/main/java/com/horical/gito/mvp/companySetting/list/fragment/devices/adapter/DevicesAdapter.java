package com.horical.gito.mvp.companySetting.list.fragment.devices.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.TokenFile;
import com.horical.gito.model.Device;
import com.horical.gito.model.Id;
import com.horical.gito.utils.CommonUtils;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tin on 30-Nov-16.
 */
public class DevicesAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<Id> mListDataHeader;
    private HashMap<Id, List<Device>> mListDataChild;

    public DevicesAdapter(Context context, List<Id> listDataHeader, HashMap<Id, List<Device>> listChildData) {
        this.mContext = context;
        this.mListDataHeader = listDataHeader;
        this.mListDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final Device device = (Device) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_content_devices, null);
        }
        TextView tvVersion = (TextView) convertView.findViewById(R.id.tv_version);
        LinearLayout llDelete = (LinearLayout) convertView.findViewById(R.id.ll_delete);
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
        ImageView img_icon = (ImageView) convertView.findViewById(R.id.img_icon);
        LinearLayout llView = (LinearLayout) convertView.findViewById(R.id.ll_view);

        if (childPosition == mListDataChild.get(this.mListDataHeader.get(groupPosition)).size() - 1) {
            llView.setVisibility(View.GONE);
        } else {
            llView.setVisibility(View.VISIBLE);
        }
        if(device.getDeviceOS().equalsIgnoreCase("iOS")){
            img_icon.setImageResource(R.drawable.ic_ios);
        } else if(device.getDeviceOS().equalsIgnoreCase("android")){
            img_icon.setImageResource(R.drawable.ic_android);
        } else if(device.getDeviceOS().equalsIgnoreCase("wp")){
            img_icon.setImageResource(R.drawable.ic_window);
        } else if(device.getDeviceOS().equalsIgnoreCase("tizen")){
            img_icon.setImageResource(R.drawable.ic_tizen);
        } else if(device.getDeviceOS().equalsIgnoreCase("other")){
            img_icon.setImageResource(R.drawable.ic_other);
        } else if(device.getDeviceOS().equalsIgnoreCase("window")){
            img_icon.setImageResource(R.drawable.ic_window);
        } else if(device.getDeviceOS().equalsIgnoreCase("mac")){
            img_icon.setImageResource(R.drawable.ic_ios);
        } else if(device.getDeviceOS().equalsIgnoreCase("linux")){
            img_icon.setImageResource(R.drawable.ic_linux);
        }
        llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(childPosition, device.get_id());
            }
        });
        tvVersion.setText(device.getDeviceOSVersion());
        tvTitle.setText(device.getDeviceName());
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.mListDataChild.get(this.mListDataHeader.get(groupPosition)) != null ? this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
                .size() : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mListDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.mListDataHeader != null ? this.mListDataHeader.size() : 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Id header = mListDataHeader.get(groupPosition);
        Log.d("TAG", header.getDisplayName());
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_header_devices, null);
        }

        TextView tvShortname = (TextView) convertView.findViewById(R.id.tv_shortname);
        CircleImageView imgAvatar = (CircleImageView) convertView.findViewById(R.id.img_avatar);
        ProgressBar pgbar = (ProgressBar) convertView.findViewById(R.id.pgbar_avatar);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        TextView tvJob = (TextView) convertView.findViewById(R.id.tv_job);

        View view = convertView.findViewById(R.id.view_header);

        if (groupPosition == 0) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
        TokenFile avatar = header.getAvatar();
        if (avatar != null) {
            pgbar.setVisibility(View.VISIBLE);
            String url = CommonUtils.getURL(avatar);
            ImageLoader.load(mContext, url, imgAvatar, pgbar);
        } else {
            pgbar.setVisibility(View.GONE);
            tvShortname.setText(header.getDisplayName().substring(0, 1));
        }
        tvName.setText(header.getDisplayName());
        tvJob.setText(header.getLevel().getName());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position, String idDevice);
    }
}