package com.horical.gito.mvp.estate.warehouse.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by hoangsang on 4/12/17.
 */

public class WarehouseItemHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.txt_name)
    public GitOTextView mTvName;

    @Bind(R.id.txt_item)
    public GitOTextView mTvItem;

    @Bind(R.id.txt_description)
    public GitOTextView mTvDescription;

    @Bind(R.id.imv_delete)
    public ImageView mImvDelete;

    public WarehouseItemHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
