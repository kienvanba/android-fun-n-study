package com.horical.gito.mvp.todolist.note.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.todolist.note.detail.injection.module.NoteDetailModule;
import com.horical.gito.mvp.todolist.note.detail.view.NoteDetailActivity;

import dagger.Component;

/**
 * Created by Luong on 28-Mar-17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = NoteDetailModule.class)
public interface NoteDetailComponent {
    void inject(NoteDetailActivity activity);
}