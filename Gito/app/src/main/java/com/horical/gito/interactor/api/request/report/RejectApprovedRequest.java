package com.horical.gito.interactor.api.request.report;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RejectApprovedRequest {

    public RejectApprovedRequest(int status, String rejectMsg) {
        this.status = status;
        this.rejectMsg = rejectMsg;
    }

    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("rejectMsg")
    @Expose
    private String rejectMsg;
}
