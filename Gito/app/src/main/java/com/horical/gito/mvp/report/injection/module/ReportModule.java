package com.horical.gito.mvp.report.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.report.presenter.ReportPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ReportModule {
    @PerActivity
    @Provides
    ReportPresenter provideReportPresenter(){
        return new ReportPresenter();
    }
}
