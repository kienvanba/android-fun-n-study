package com.horical.gito.interactor.api.response.todolist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.TodoList;

import java.util.List;

/**
 * Created by TrungTien on 12/23/2016
 */

public class GetAllTodoListResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<TodoList> todoLists;
}
