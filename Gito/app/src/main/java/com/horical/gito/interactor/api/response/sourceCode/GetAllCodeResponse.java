package com.horical.gito.interactor.api.response.sourceCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Code;

import java.util.List;

/**
 * Created by Tin on 22-Dec-16
 */
public class GetAllCodeResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Code> codes;
}