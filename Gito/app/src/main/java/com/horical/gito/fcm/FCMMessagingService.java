package com.horical.gito.fcm;

import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.interactor.prefer.PreferManager;

import java.util.Map;

import javax.inject.Inject;

public class FCMMessagingService extends FirebaseMessagingService {

    @Inject
    PreferManager preferManager;

    public FCMMessagingService() {
        MainApplication.getAppComponent().inject(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData() != null && remoteMessage.getData().size() > 0) {
            Map<String, String> data = remoteMessage.getData();
            if (preferManager.isLogin()) {
                sendNotification(data.get("title"), data.get("body"));
            }
        }
    }

    private void sendNotification(String title, String message) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification_logo)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

}