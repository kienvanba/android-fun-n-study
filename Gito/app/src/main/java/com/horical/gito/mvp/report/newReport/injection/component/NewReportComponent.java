package com.horical.gito.mvp.report.newReport.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.report.newReport.injection.module.NewReportModule;
import com.horical.gito.mvp.report.newReport.view.NewReportActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = NewReportModule.class)
public interface NewReportComponent {
    void inject(NewReportActivity activity);
}
