package com.horical.gito.mvp.survey.newsurvey.adapter.option;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.horical.gito.AppConstants;
import com.horical.gito.R;
import com.horical.gito.model.Option;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nhattruong251295 on 4/4/2017.
 */

public class OptionAdapter extends RecyclerView.Adapter<OptionAdapter.OptionHolder>{
    private Context mContext;
    private List<Option> mList;
    private int selectedPosition = -1;
    private int mType = -1;
    public OptionAdapter(Context mContext, List<Option> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }
    public void setType(int type){
        this.mType = type;
    }
    public int getType(){
        return mType;
    }
    @Override
    public OptionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_option,parent,false);
        return new OptionHolder(view);
    }

    @Override
    public void onBindViewHolder(OptionHolder holder, int position) {
        holder.edtOption.setText(mList.get(position).getDesc());
        holder.llDeleteOption.setVisibility(View.GONE);

        if(getType() == AppConstants.TYPE_OPTION){
            holder.chkOption.setButtonDrawable(R.drawable.bg_blue_white_checkbox_selector);

            if(selectedPosition == position){
                holder.chkOption.setChecked(true);
            }else {
                holder.chkOption.setChecked(false);
            }
            holder.chkOption.setOnClickListener(onStateChangedListener(holder.chkOption,position));
        }
        else if(getType() == AppConstants.TYPE_CHECK){
            holder.chkOption.setButtonDrawable(R.drawable.bg_task_filter_check);
        }

    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    private View.OnClickListener onStateChangedListener(final CheckBox checkBox, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    selectedPosition = position;
                } else {
                    selectedPosition = -1;
                }
                notifyDataSetChanged();
            }
        };
    }

    class OptionHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.chk_option)
        CheckBox chkOption;

        @Bind(R.id.edt_option)
        EditText edtOption;

        @Bind(R.id.ll_delete_option)
        LinearLayout llDeleteOption;

        public OptionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
