package com.horical.gito.mvp.estate.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.estate.detail.injection.module.DetailEstateModule;
import com.horical.gito.mvp.estate.detail.view.DetailEstateActivity;

import dagger.Component;

/**
 * Created by hoangsang on 11/8/16.
 */

@PerActivity
@Component (dependencies = ApplicationComponent.class, modules = DetailEstateModule.class)
public interface DetailEstateComponent {
    void inject(DetailEstateActivity activity);
}
