package com.horical.gito.mvp.myProject.task.list.presenter;

import android.util.Log;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.task.GetAllTaskResponse;
import com.horical.gito.interactor.api.response.task.GetDeleteTaskResponse;
import com.horical.gito.model.Task;
import com.horical.gito.mvp.myProject.task.list.view.ITaskView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class TaskPresenter extends BasePresenter implements ITaskPresenter {

    private static final String TAG = makeLogTag(TaskPresenter.class);

    private List<Task> mList;

    public ITaskView getView() {
        return (ITaskView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Task> getListTasks() {
        return mList;
    }

    public void setListTasks(List<Task> mTasks) {
        this.mList = mTasks;
    }

    @Override
    public void getAllTask() {
        String projectId = GitOStorage.getInstance().getProject().getId();

        getApiManager().getAllTask(projectId, new ApiCallback<GetAllTaskResponse>() {
            @Override
            public void success(GetAllTaskResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.tasks);

                getView().getAllTaskSuccess();
            }

            @Override
            public void failure(RestError error) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                getView().getAllTaskFailure(error.message);
            }
        });
    }

    @Override
    public List<Task> getListSearchTask(String keyword) {
        List<Task> searchCaseStudy = new ArrayList<>();
        if (keyword != null && keyword.trim().length() > 0) {
            for (Task task : mList) {
                if (task.getTitle() != null && task.getTitle().toLowerCase().contains(keyword.toLowerCase())) {
                    searchCaseStudy.add(task);
                    continue;
                }
            }
        } else {
            searchCaseStudy.addAll(mList);
        }
        return searchCaseStudy;
    }

    @Override
    public void deleteTask(String taskID) {
        String projectID = GitOStorage.getInstance().getProject().getId();
        getApiManager().deleteTask(projectID, taskID, new ApiCallback<GetDeleteTaskResponse>() {
            @Override
            public void success(GetDeleteTaskResponse res) {
                getView().deleteTaskSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().deleteTaskFailure(error.message);
            }
        });
    }
}