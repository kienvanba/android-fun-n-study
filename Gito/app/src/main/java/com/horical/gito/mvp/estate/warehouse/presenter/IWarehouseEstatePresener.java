package com.horical.gito.mvp.estate.warehouse.presenter;

/**
 * Created by hoangsang on 4/12/17.
 */

public interface IWarehouseEstatePresener {

    void getAllWarehouseEstate();

    void deleteWarehouseId(String warehouseId);

}
