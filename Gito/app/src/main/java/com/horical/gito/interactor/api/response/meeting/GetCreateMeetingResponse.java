package com.horical.gito.interactor.api.response.meeting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Meeting;

/**
 * Created by Lemon on 3/3/2017
 */

public class GetCreateMeetingResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    public Meeting meeting;
}
