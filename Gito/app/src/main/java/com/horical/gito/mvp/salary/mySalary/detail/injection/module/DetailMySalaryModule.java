package com.horical.gito.mvp.salary.mySalary.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.mySalary.detail.presenter.DetailMySalaryPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailMySalaryModule {
    @PerActivity
    @Provides
    DetailMySalaryPresenter provideReportPresenter(){
        return new DetailMySalaryPresenter();
    }
}
