package com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IWorkingAndHolidayView extends IView {
    void getAllHolidaySuccess();

    void getAllHolidayFailure(String err);
}
