package com.horical.gito.mvp.allProject.list.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

public interface IAllProjectView extends IView {

    void showLoading();

    void hideLoading();

    void getAllProjectSuccess();

    void getAllProjectFailure(String error);

    void getProjectDetailsSuccess();

    void getProjectDetailsFailure(String error);
}
