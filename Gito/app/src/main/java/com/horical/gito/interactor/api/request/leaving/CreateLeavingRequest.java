package com.horical.gito.interactor.api.request.leaving;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.model.User;

import java.util.Date;
import java.util.List;

public class CreateLeavingRequest {
    @SerializedName("fromDate")
    @Expose
    Date fromDate;
    @SerializedName("toDate")
    @Expose
    Date toDate;
    @SerializedName("name")
    @Expose
    String name;
    @SerializedName("reason")
    @Expose
    String reason;
    @SerializedName("approverListId")
    @Expose
    List<User> approverListId;

    public List<User> getApproverListId() {
        return approverListId;
    }

    public void setApproverListId(List<User> approverListId) {
        this.approverListId = approverListId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
