package com.horical.gito.mvp.companySetting.detail.newLevel.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.detail.newLevel.injection.module.NewLevelModule;
import com.horical.gito.mvp.companySetting.detail.newLevel.view.NewLevelActivity;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = NewLevelModule.class)
public interface NewLevelComponent {
    void inject(NewLevelActivity fragment);
}
