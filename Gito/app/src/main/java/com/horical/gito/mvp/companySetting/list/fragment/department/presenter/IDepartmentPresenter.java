package com.horical.gito.mvp.companySetting.list.fragment.department.presenter;

import com.horical.gito.model.Department;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IDepartmentPresenter {
    void getAllDepartment();
    void deleteDepartment(String idDepartment, int position);
}
