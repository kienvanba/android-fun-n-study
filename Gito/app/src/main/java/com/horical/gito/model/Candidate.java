package com.horical.gito.model;


import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Candidate implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("candidateId")
    @Expose
    private String candidateId;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("totalMark")
    @Expose
    private double totalMark;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("createdDate")
    @Expose(deserialize = true, serialize = false)
    private Date createdDate;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("avatar")
    @Expose
    private TokenFile avatar;

    @SerializedName("skillRecruitments")
    @Expose
    private List<SkillRecruitment> skillRecruitments;

    public String getDisplayPhotoName() {
        if (TextUtils.isEmpty(name)) {
            return "";
        } else {
            try {
                String CurrentString = name.trim();
                String[] separated = CurrentString.split(" ");
                if (separated.length == 1) {
                    return separated[0].substring(0, 2).toUpperCase();
                } else {
                    return separated[0].substring(0, 1).toUpperCase() + separated[1].substring(0, 1).toUpperCase();
                }
            } catch (Exception e) {
                return name.substring(0, 1).toUpperCase();
            }

        }
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<SkillRecruitment> getSkillRecruitments() {
        return skillRecruitments;
    }

    public void setSkillRecruitments(List<SkillRecruitment> skillRecruitments) {
        this.skillRecruitments = skillRecruitments;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setTotalMark(double totalMark) {
        this.totalMark = totalMark;
    }

    public TokenFile getAvatar() {
        return avatar;
    }

    public void setAvatar(TokenFile avatar) {
        this.avatar = avatar;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getTotalMark() {
        return totalMark;
    }

    public void setTotalMark(int totalMark) {
        this.totalMark = totalMark;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
