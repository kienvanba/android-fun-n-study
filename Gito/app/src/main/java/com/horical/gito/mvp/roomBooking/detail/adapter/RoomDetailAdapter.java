package com.horical.gito.mvp.roomBooking.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lemon on 3/21/2017.
 */

public class RoomDetailAdapter extends RecyclerView.Adapter<RoomDetailAdapter.RoomDetailHolder> {

    private List<String> mList;
    private Context mContext;
    private int selected = 0;

    public RoomDetailAdapter(Context context, List<String> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public RoomDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_room_detail, parent, false);
        return new RoomDetailHolder(view);
    }

    @Override
    public void onBindViewHolder(final RoomDetailHolder holder, final int position) {
        String title = mList.get(position);
        holder.mTvItem.setText(title);
        holder.mTvItem.setSelected(selected == position);

        holder.mTvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickListener(position);
            }
        });
    }

    public void setSelectedIndex(int cur) {
        this.selected = cur;
    }

    public List<String> getList() {
        return mList;
    }

    public void setList(List<String> mList) {
        this.mList = mList;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class RoomDetailHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_room_detail_item)
        TextView mTvItem;

        View view;

        public RoomDetailHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}
