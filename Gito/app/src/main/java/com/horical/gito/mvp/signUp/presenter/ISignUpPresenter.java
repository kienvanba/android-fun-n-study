package com.horical.gito.mvp.signUp.presenter;

public interface ISignUpPresenter {

    void registerUser(
            String email,
            String companyId,
            String password,
            String confirmPassword);

}
