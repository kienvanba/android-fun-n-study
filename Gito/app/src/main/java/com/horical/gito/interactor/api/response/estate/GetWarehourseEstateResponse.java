package com.horical.gito.interactor.api.response.estate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.estate.WarehouseEstate;

import java.util.List;

/**
 * Created by hoangsang on 4/12/17.
 */

public class GetWarehourseEstateResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<WarehouseEstate> warehouseEstateList;

}
