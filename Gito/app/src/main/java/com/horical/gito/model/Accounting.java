package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;


public class Accounting implements Serializable {

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("currency")
    @Expose
    public String currency;

    @SerializedName("amount")
    @Expose
    public int amount;

    @SerializedName("desc")
    @Expose
    public String desc;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("status")
    @Expose
    public int status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
