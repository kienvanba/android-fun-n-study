package com.horical.gito.mvp.dialog.dialogSurvey;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import android.widget.ListView;
import android.widget.Toast;

import com.horical.gito.AppConstants;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.Option;
import com.horical.gito.model.Question;
import com.horical.gito.mvp.dialog.dialogSurvey.adapter.OptionAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 11/12/16.
 */

public class DialogAddQuestionSurvey extends Dialog implements View.OnClickListener {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.chk_type_text)
    CheckBox chkTypeText;

    @Bind(R.id.chk_type_option)
    CheckBox chkTypeOption;

    @Bind(R.id.chk_type_check)
    CheckBox chkTypeCheck;

    @Bind(R.id.edt_content_question)
    EditText edtContenQuestion;

    @Bind(R.id.ll_option)
    LinearLayout llOption;

    @Bind(R.id.ll_add_option)
    LinearLayout llAddOption;

    @Bind(R.id.lv_option)
    ListView mLvAddOption;

   /* @Bind(R.id.rcv_option)
    RecyclerView mRcvAddOption;*/

    private OptionAdapter adapterOption;

    private List<Option> mList;

    private int currentType = -1;

    private Context mContext;

    public DialogAddQuestionSurvey(Context context) {
        super(context, R.style.FullscreenDialog);
        this.mContext = context;
    }

    public List<Option> getList() {
        return mList;
    }

    public Question getQuestion() {
        Question question = new Question();
        question.setTitle(edtContenQuestion.getText().toString());
        question.setType(currentType);
        question.setOptionList(mList);
        return question;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getWindow() != null) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.dialog_add_question_survey);
        ButterKnife.bind(this);

        initData();
        initListener();
    }

    private void initData() {
        topBar.setTextTitle("Add Question");
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        topBar.setTextViewRight("Add");

        mList = new ArrayList<>();
        adapterOption = new OptionAdapter(mContext, R.layout.item_option, mList);
        mLvAddOption.setAdapter(adapterOption);

    }

    private void initListener() {

        chkTypeText.setOnClickListener(this);
        chkTypeOption.setOnClickListener(this);
        chkTypeCheck.setOnClickListener(this);

        llAddOption.setOnClickListener(this);

        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                dismiss();
            }

            @Override
            public void onRightClicked() {
                if (mList.size() > 0){
                    mOnAddQuestionListener.addQuestion(getQuestion());
                }
                Toast.makeText(getContext(),"Added",Toast.LENGTH_LONG).show();
            }
        });

        adapterOption.setOnItemClickListener(new OptionAdapter.OnItemClickListener() {
            @Override
            public void deleteOption(int position) {
                mList.remove(position);
                adapterOption.notifyDataSetChanged();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chk_type_text:
                updateTypeQuestion(chkTypeText);
                llOption.setVisibility(View.GONE);
                currentType = AppConstants.TYPE_TEXT;
                break;
            case R.id.chk_type_option:
                updateTypeQuestion(chkTypeOption);
                llOption.setVisibility(View.VISIBLE);
                currentType = AppConstants.TYPE_OPTION;
                adapterOption.setType(currentType);
                adapterOption.notifyDataSetChanged();
                break;
            case R.id.chk_type_check:
                updateTypeQuestion(chkTypeCheck);
                llOption.setVisibility(View.VISIBLE);
                currentType = AppConstants.TYPE_CHECK;
                adapterOption.setType(currentType);
                adapterOption.notifyDataSetChanged();
                break;
            case R.id.ll_add_option:
                Option option = new Option();
                option.setDesc("Option " + mList.size());
                mList.add(option);
                adapterOption.notifyDataSetChanged();
                break;
        }
    }

    public void updateTypeQuestion(CheckBox chk) {
        chkTypeText.setChecked(false);
        chkTypeOption.setChecked(false);
        chkTypeCheck.setChecked(false);
        chk.setChecked(true);
    }
    private OnAddQuestionListener mOnAddQuestionListener;

    public void setOnAddQuestionListener(OnAddQuestionListener callback){
        mOnAddQuestionListener = callback;
    }
    public interface OnAddQuestionListener{
        void addQuestion(Question question);
    }
}
