package com.horical.gito.interactor.api.response.companySetting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Information;

/**
 * Created by thanhle on 4/4/17.
 */

public class GetUpdateInfoResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public Information information;
}
