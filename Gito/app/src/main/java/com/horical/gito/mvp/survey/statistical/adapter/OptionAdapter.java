package com.horical.gito.mvp.survey.statistical.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nhattruong251295 on 4/7/2017.
 */

public class OptionAdapter extends RecyclerView.Adapter<OptionAdapter.OptionHolder>{
    private Context mContext;
    private List<String> mList;
    private int mType = -1;

    public OptionAdapter(Context mContext, List<String> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    public void setType(int type){
        this.mType = type;
    }
    public int getType(){
        return mType;
    }
    @Override
    public OptionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_option_statistical,parent,false);
        return new OptionHolder(view);
    }

    @Override
    public void onBindViewHolder(OptionHolder holder, int position) {
       if (position == 0){
           holder.ivOption.setColorFilter(ContextCompat.getColor(mContext, R.color.app_color));
       }
       else if(position == 1){
           holder.ivOption.setColorFilter(ContextCompat.getColor(mContext,R.color.blue_cross));
       }
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class OptionHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.iv_option)
        ImageView ivOption;

        @Bind(R.id.tv_option)
        TextView tvOption;

        public OptionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
