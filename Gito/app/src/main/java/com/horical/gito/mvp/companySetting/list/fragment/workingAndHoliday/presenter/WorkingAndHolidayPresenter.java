package com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.companySetting.GetAllHolidayResponse;
import com.horical.gito.model.Holiday;
import com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.view.IWorkingAndHolidayView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class WorkingAndHolidayPresenter extends BasePresenter implements IWorkingAndHolidayPresenter {

    private List<Holiday> listHoliday;

    public void attachView(IWorkingAndHolidayView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IWorkingAndHolidayView getView() {
        return (IWorkingAndHolidayView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        listHoliday = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Holiday> getListHoliday() {
        return listHoliday;
    }

    public void setListHoliday(List<Holiday> listHoliday) {
        this.listHoliday = listHoliday;
    }

    @Override
    public void getAllHoliday() {
        getApiManager().getAllHoliday(new ApiCallback<GetAllHolidayResponse>() {
            @Override
            public void success(GetAllHolidayResponse res) {
                if (listHoliday != null) {
                    listHoliday.clear();
                } else {
                    listHoliday = new ArrayList<>();
                }
                listHoliday.addAll(res.holidays);
                getView().getAllHolidaySuccess();
            }

            @Override
            public void failure(RestError error) {
                if (listHoliday != null) {
                    listHoliday.clear();
                } else {
                    listHoliday = new ArrayList<>();
                }
                getView().getAllHolidayFailure(error.message);
            }
        });
    }
}

