package com.horical.gito.mvp.allProject.list.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.Project;
import com.horical.gito.mvp.allProject.detail.view.ProjectDetailActivity;
import com.horical.gito.mvp.allProject.list.adapter.AllProjectAdapter;
import com.horical.gito.mvp.allProject.list.injection.component.AllProjectComponent;
import com.horical.gito.mvp.allProject.list.injection.component.DaggerAllProjectComponent;
import com.horical.gito.mvp.allProject.list.injection.module.AllProjectModule;
import com.horical.gito.mvp.allProject.list.presenter.AllProjectPresenter;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.myProject.summary.view.SummaryActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class AllProjectActivity extends DrawerActivity implements IAllProjectView, View.OnClickListener {

    private static final String TAG = makeLogTag(AllProjectActivity.class);
    public final static String KEY_FROM = "KEY_FROM";
    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.project_refresh_layout)
    SwipeRefreshLayout mRfLayout;

    @Bind(R.id.project_ln_no_data)
    LinearLayout lnNoData;

    @Bind(R.id.project_rv_projects)
    RecyclerView mRvProjects;

    private AllProjectAdapter mProjectAdapter;

    private String mStartFrom;

    @Inject
    AllProjectPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_all_project;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_ALL_PROJECT;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        AllProjectComponent component = DaggerAllProjectComponent.builder()
                .allProjectModule(new AllProjectModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        mStartFrom = getIntent().getStringExtra(KEY_FROM);

        initData();
        initListener();
    }

    protected void initData() {
        if ((mStartFrom != null) && (mStartFrom.equalsIgnoreCase("QuotaFragment"))) {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        } else {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        }
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);

        mProjectAdapter = new AllProjectAdapter(this, mPresenter.getListProject());
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRvProjects.setLayoutManager(llm);
        mRvProjects.setAdapter(mProjectAdapter);

        mPresenter.getAllProject();
    }

    protected void initListener() {
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                if ((mStartFrom != null) && (mStartFrom.equalsIgnoreCase("QuotaFragment"))) {
                    onBackPressed();
                } else {
                    openDrawer();
                }
            }

            @Override
            public void onRightButtonOneClicked() {
                Intent intent = new Intent(AllProjectActivity.this, ProjectDetailActivity.class);
                startActivityForResult(intent, 0);
            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });

        mRfLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getAllProject();
            }
        });

        mProjectAdapter.setOnItemClickListener(new AllProjectAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(Project prj) {
                mPresenter.getProjectDetails(prj.getId());
            }
        });
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllProjectSuccess() {
        mRfLayout.setRefreshing(false);
        if (mPresenter.getListProject().isEmpty()) {
            topBar.setTextTitle(getString(R.string.all_project));
            lnNoData.setVisibility(View.VISIBLE);
        } else {
            topBar.setTextTitle(getString(R.string.all_project) + " (" + mPresenter.getListProject().size() + ")");
            lnNoData.setVisibility(View.GONE);
        }
        mProjectAdapter.notifyDataSetChanged();
    }

    @Override
    public void getAllProjectFailure(String error) {
        showErrorDialog(error);
    }

    @Override
    public void getProjectDetailsSuccess() {
        Intent intent = new Intent(AllProjectActivity.this, SummaryActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void getProjectDetailsFailure(String error) {
        showErrorDialog(error);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        mPresenter.detachView();
        mPresenter.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppConstants.KEY_SUCCESS) {
            mPresenter.getAllProject();
        }
    }
}
