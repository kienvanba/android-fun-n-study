package com.horical.gito.interactor.api.response.component;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Component;

import java.util.List;


public class GetAllComponentResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Component> components;
}
