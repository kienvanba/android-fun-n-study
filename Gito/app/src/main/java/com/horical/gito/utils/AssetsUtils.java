package com.horical.gito.utils;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;

public class AssetsUtils {

    public static JSONObject readJsonObjectFromFile(Context context, String path) {
        JSONObject result = null;
        InputStream in = null;
        try {
            StringWriter sw = new StringWriter();
            in = context.getAssets().open(path);
            Reader reader = new BufferedReader(new InputStreamReader(in, "ISO-8859-1"));
            for (int c = reader.read(); c >= 0; c = reader.read()) {
                sw.append((char) c);
            }
            in.close();
            in = null;
            sw.flush();
            result = new JSONObject(sw.toString());
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    public static JSONArray readJsonArrayFromFile(Context context, String path) {
        JSONArray result = null;
        InputStream in = null;
        try {
            StringWriter sw = new StringWriter();
            in = context.getAssets().open(path);
            Reader reader = new BufferedReader(new InputStreamReader(in, "ISO-8859-1"));
            for (int c = reader.read(); c >= 0; c = reader.read()) {
                sw.append((char) c);
            }
            in.close();
            in = null;
            sw.flush();
            result = new JSONArray(sw.toString());
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

}
