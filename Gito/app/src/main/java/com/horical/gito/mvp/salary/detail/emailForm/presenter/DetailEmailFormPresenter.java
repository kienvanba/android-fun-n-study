package com.horical.gito.mvp.salary.detail.emailForm.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.salary.detail.emailForm.view.IDetailEmailFormView;
import com.horical.gito.mvp.salary.dto.emailForm.EmailFormDto;


public class DetailEmailFormPresenter extends BasePresenter implements IDetailEmailFormPresenter {

    public void attachView(IDetailEmailFormView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IDetailEmailFormView getView() {
        return (IDetailEmailFormView) getIView();
    }

    private EmailFormDto emailForm;

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public EmailFormDto getEmailForm() {
        if (emailForm == null) {
            emailForm = new EmailFormDto();
        }
        return emailForm;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
