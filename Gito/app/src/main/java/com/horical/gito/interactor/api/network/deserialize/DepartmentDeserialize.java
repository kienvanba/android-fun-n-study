package com.horical.gito.interactor.api.network.deserialize;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.horical.gito.model.Department;
import com.horical.gito.utils.GsonUtils;

import java.lang.reflect.Type;

/**
 * Created by nxon on 3/20/17
 */

public class DepartmentDeserialize implements JsonDeserializer<Department> {

    @Override
    public Department deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Department department;
        try {
            department = GsonUtils.createGson(Department.class).fromJson(json, Department.class);
        } catch (Exception ex) {
            department = new Department();
            department.set_id(json.getAsString());
        }
        return department;
    }
}
