package com.horical.gito.interactor.api.response.survey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Survey;

import java.util.List;

/**
 * Created by thanhle on 3/4/17
 */

public class GetAllSurveyResponse  extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Survey> surveyList;
}
