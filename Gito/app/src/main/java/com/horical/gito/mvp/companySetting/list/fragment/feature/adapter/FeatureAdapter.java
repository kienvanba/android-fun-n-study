package com.horical.gito.mvp.companySetting.list.fragment.feature.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Feature;
import com.horical.gito.mvp.companySetting.list.fragment.feature.dto.MenuFeature;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 30-Nov-16.
 */

public class FeatureAdapter extends RecyclerView.Adapter<FeatureAdapter.FeatureHolder>{
    private Context mContext;
    private List<MenuFeature> mList;
    private OnItemClickListener mOnItemClickListener;

    public FeatureAdapter(Context context, List<MenuFeature> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public FeatureHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_company_setting_feature, parent, false);
        return new FeatureHolder(view);
    }

    @Override
    public void onBindViewHolder(FeatureHolder holder, final int position) {
        holder.tvTitle.setText(mList.get(position).getTitle());
        holder.imgIcon.setImageResource(mList.get(position).getIconResource());
        holder.chkCheck.setChecked(mList.get(position).isSelected());

        holder.chkCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mList.get(position).setSelected(!mList.get(position).isSelected());
                notifyItemChanged(position);
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mList.get(position).setSelected(!mList.get(position).isSelected());
                notifyItemChanged(position);
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class FeatureHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.img_icon_feature)
        ImageView imgIcon;

        @Bind(R.id.tv_title_feature)
        TextView tvTitle;

        @Bind(R.id.chk_check_feature)
        CheckBox chkCheck;

        View view;

        public FeatureHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
    public interface OnItemClickListener {
        void onItemClickSelected(int position);
    }
}
