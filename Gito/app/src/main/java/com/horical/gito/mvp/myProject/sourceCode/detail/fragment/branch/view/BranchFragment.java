package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.BaseSourceCodeFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.adapter.BranchAdapter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.injection.component.BranchComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.injection.component.DaggerBranchComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.injection.module.BranchModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.presenter.BranchPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class BranchFragment extends BaseSourceCodeFragment implements IBranchView, View.OnClickListener {
    public static final String TAG = makeLogTag(BranchFragment.class);

    @Bind(R.id.code_detail_branch_list)
    RecyclerView rcvBranch;

    private BranchAdapter adapterBranch;

    @Inject
    BranchPresenter mPresenter;


    public static BranchFragment newInstance() {
        Bundle args = new Bundle();
        BranchFragment fragment = new BranchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_code_branch;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        BranchComponent component = DaggerBranchComponent.builder()
                .branchModule(new BranchModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
//        adapterBranch = new MemberCompanyRoleAdapter(getContext(), mPresenter.getListBranch());
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
//        rcvBranch.setLayoutManager(layoutManager);
//        rcvBranch.setHasFixedSize(false);
//        rcvBranch.setAdapter(adapterBranch);
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View view) {

    }
}
