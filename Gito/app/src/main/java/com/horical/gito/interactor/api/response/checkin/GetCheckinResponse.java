package com.horical.gito.interactor.api.response.checkin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.CheckIn;

/**
 * Created by Penguin on 28-Dec-16
 */

public class GetCheckinResponse extends BaseResponse{

    @SerializedName("results")
    @Expose
    public CheckIn checkIn;
}