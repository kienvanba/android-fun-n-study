package com.horical.gito.mvp.salary.list.fragments.subSalary.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.salary.dto.salary.ListSalaryDto;
import com.horical.gito.mvp.salary.list.fragments.subSalary.view.IListSalaryFragmentView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListSalaryFragmentPresenter extends BasePresenter implements IListSalaryFragmentPresenter {

    public void attachView(IListSalaryFragmentView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IListSalaryFragmentView getView() {
        return (IListSalaryFragmentView) getIView();
    }

    private List<ListSalaryDto> mSalaries;

    private void setDummyData() {
        getSalaries().add(new ListSalaryDto(new Date("01/04/2017"), 3900, "usd", "Nam A", "Coder", new Date("02/04/2017"), "New"));
        getSalaries().add(new ListSalaryDto(new Date("01/03/2017"), 3000, "usd", "Nam B", "Tester", new Date("02/03/2017"), "Rejected"));
        getSalaries().add(new ListSalaryDto(new Date("01/02/2017"), 2900, "usd", "Nam C", "Lead", new Date("02/02/2017"), "Submitted"));
        getSalaries().add(new ListSalaryDto(new Date("01/01/2017"), 1900, "usd", "Nam D", "PM", new Date("02/01/2017"), "Approved"));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        setDummyData();
    }

    @Override
    public List<ListSalaryDto> getSalaries() {
        if (mSalaries == null) {
            mSalaries = new ArrayList<>();
        }
        return mSalaries;
    }

    @Override
    public void setSalaries(List<ListSalaryDto> salaries) {
        this.mSalaries = salaries;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
