package com.horical.gito.mvp.estate.detail.presenter;

import com.google.gson.annotations.SerializedName;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IView;
import com.horical.gito.utils.LogUtils;

/**
 * Created by hoangsang on 11/8/16.
 */

public class DetailEstatePresenter extends BasePresenter implements IDetailEstatePresenter {
    public static final String TAG = LogUtils.makeLogTag(DetailEstatePresenter.class);

    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public IView getIView() {
        return super.getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
