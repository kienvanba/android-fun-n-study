package com.horical.gito.mvp.salary.bonus.list.fragments.mine.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.bonus.list.fragments.mine.presenter.ListMineFragmentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListMineFragmentModule {
    @PerActivity
    @Provides
    ListMineFragmentPresenter provideSalaryPresenter(){
        return new ListMineFragmentPresenter();
    }
}
