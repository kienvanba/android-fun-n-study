package com.horical.gito.mvp.companySetting.list.fragment.department.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.fragment.department.injection.module.DepartmentModule;
import com.horical.gito.mvp.companySetting.list.fragment.department.view.DepartmentFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = DepartmentModule.class)
public interface DepartmentComponent {
    void inject(DepartmentFragment fragment);
}
