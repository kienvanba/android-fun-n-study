package com.horical.gito.mvp.estate.detail.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.model.estate.Estate;
import com.horical.gito.model.TokenFile;
import com.horical.gito.mvp.estate.detail.adapter.ImagePaggerAdapter;
import com.horical.gito.mvp.estate.detail.view.DetaiEstateListener;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.utils.LogUtils;
import com.horical.gito.widget.dialog.EditTwoOptionsDialog;
import com.horical.gito.widget.textview.GitOTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;

/**
 * Created by hoangsang on 11/9/16.
 */

public class DetailEstateFragmentOne extends BaseFragment implements View.OnClickListener {

    private static final String TAG = LogUtils.makeLogTag(DetailEstateFragmentOne.class);

    @Bind(R.id.viewpagger_image)
    ViewPager mViewPagger;

    @Bind(R.id.detail_imv_estate_indicator)
    LinearLayout lnDetailImvIndicator;

    @Bind(R.id.edit_name)
    GitOTextView mTvName;

    @Bind(R.id.imv_edit_name)
    ImageView mImvEditName;

    @Bind(R.id.tv_type)
    GitOTextView mTvType;

    @Bind(R.id.imv_edit_type)
    ImageView mImvEditType;

    @Bind(R.id.imv_edit_description)
    ImageView mImvEditDescription;

    @Bind(R.id.tv)
    GitOTextView mTvDescription;

    @Bind(R.id.tv_quantity)
    GitOTextView mTvQuantity;

    @Bind(R.id.imv_edit_quantity)
    ImageView mImvEditQuantity;

    @Bind(R.id.tv_code)
    GitOTextView mTvCode;

    @Bind(R.id.imv_edit_code)
    ImageView mImvEditCode;

    @Bind(R.id.tv_entrance_date)
    GitOTextView mTvEntranceDate;

    @Bind(R.id.imv_edit_date)
    ImageView mImvEditDate;

    @Bind(R.id.tv_check)
    GitOTextView mTvCheck;

    @Bind(R.id.imv_edit_checking)
    ImageView mImvEditChecking;

    private int editTextIdCurrentEdit;
    private int imvIdCurrentEdit;

    private static final int NAME_EDIT = 1;
    private static final int TYPE_EDIT = 2;
    private static final int DESCRIPTION_EDIT = 3;
    private static final int QUANTITY_EDIT = 4;
    private static final int CODE_EDIT = 5;
    private static final int ENTRANCE_DATE = 6;
    private static final int LAST_CHECK_DATE = 7;

    DetaiEstateListener mListener;

    private Estate mEstate;

    public static DetailEstateFragmentOne newInstance() {

        Bundle args = new Bundle();

        DetailEstateFragmentOne fragment = new DetailEstateFragmentOne();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_detail_estate_one;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (DetaiEstateListener) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (DetaiEstateListener) activity;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initListener();
    }

    @Override
    protected void initData() {
        mEstate = mListener.getEstate();

        List<TokenFile> tokenFiles;

        if (mEstate != null) {
            tokenFiles = mEstate.images;
            LogUtils.LOGI(TAG, "Token FIle : " + new Gson().toJson(tokenFiles));

            mViewPagger.setAdapter(new ImagePaggerAdapter(getContext(), tokenFiles));
            initIndicatorOfImv(mViewPagger.getAdapter().getCount());

            updateUI();
        }
    }

    private void updateUI() {
        if (mEstate != null) {
            LogUtils.LOGI(TAG, "Estate in detail : " + new Gson().toJson(mEstate));

            mTvName.setText(mEstate.name);

            if (mEstate.type != null)
                mTvType.setText(mEstate.type.name);
            else
                mTvType.setText("");

            mTvDescription.setText(mEstate.desc);
            mTvQuantity.setText(String.valueOf(mEstate.quanlity));
            mTvCode.setText(mEstate.estateId);
            mTvEntranceDate.setText(DateUtils.convertDateServerToLocal(mEstate.createdDate));
            mTvCheck.setText(DateUtils.convertDateServerToLocal(mEstate.lastCheckDate));
        } else
            LogUtils.LOGE(TAG, "Estate in detail == null");
    }

    @Override
    protected void initListener() {
        mViewPagger.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updatePageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mImvEditName.setOnClickListener(this);
        mImvEditType.setOnClickListener(this);
        mImvEditDescription.setOnClickListener(this);
        mImvEditQuantity.setOnClickListener(this);
        mImvEditCode.setOnClickListener(this);
        mImvEditDate.setOnClickListener(this);
        mImvEditChecking.setOnClickListener(this);
    }

    private void initIndicatorOfImv(int count) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 0, 0);
        for (int i = 0; i < count; i++) {
            View indicator = new ImageView(getContext());
            indicator.setLayoutParams(layoutParams);

            if (i == 0)
                indicator.setBackground(getResources().getDrawable(R.drawable.ic_circle_selected));
            else
                indicator.setBackground(getResources().getDrawable(R.drawable.ic_circle_normal));

            lnDetailImvIndicator.addView(indicator);
        }
    }

    private void updatePageSelected(int position) {
        lnDetailImvIndicator.removeAllViews();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 0, 0);

        for (int i = 0; i < mViewPagger.getAdapter().getCount(); i++) {
            View view = new ImageView(getContext());
            view.setLayoutParams(layoutParams);

            if (position == i) {
                view.setBackground(getResources().getDrawable(R.drawable.ic_circle_selected));
            } else {
                view.setBackground(getResources().getDrawable(R.drawable.ic_circle_normal));
            }

            lnDetailImvIndicator.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        mListener.disableCurrentView(editTextIdCurrentEdit, imvIdCurrentEdit);
        switch (v.getId()) {
            case R.id.imv_edit_name:
                showEditerDialog("Edit name", NAME_EDIT);
                break;

            case R.id.imv_edit_type:
                showEditerDialog("Edit type", TYPE_EDIT);
                break;

            case R.id.imv_edit_description:
                showEditerDialog("Edit description", DESCRIPTION_EDIT);
                break;

            case R.id.imv_edit_quantity:
                showEditerDialog("Edit quantity", QUANTITY_EDIT);
                break;

            case R.id.imv_edit_code:
                showEditerDialog("Edit code", CODE_EDIT);
                break;

            case R.id.imv_edit_date:
                showDatePickerDialog(mTvEntranceDate.getText().toString().trim(), ENTRANCE_DATE);
                break;

            case R.id.imv_edit_checking:
                showDatePickerDialog(mTvCheck.getText().toString().trim(), LAST_CHECK_DATE);
                break;
        }
    }

    private void showDatePickerDialog(String dateString, final int type) {
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Date date = null;

        try {
            date = sdf.parse(dateString);
            mListener.showDialogPicker(date, new DetaiEstateListener.DialogPickerCallBack() {
                @Override
                public void success(Date date) {
                    switch (type) {
                        case ENTRANCE_DATE:
                            mTvEntranceDate.setText(sdf.format(date));
                            break;
                        case LAST_CHECK_DATE:
                            mTvCheck.setText(sdf.format(date));
                            break;
                    }
                }

                @Override
                public void failure() {
                    mTvEntranceDate.setText("");
                }
            });

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void showEditerDialog(String hintText, final int type) {
        EditTwoOptionsDialog dialog = new EditTwoOptionsDialog(getContext(), hintText);
        dialog.setListener(new EditTwoOptionsDialog.OnClick() {
            @Override
            public void eventDone(String text) {
                switch (type) {
                    case NAME_EDIT:
                        mTvName.setText(text);
                        break;

                    case TYPE_EDIT:
                        mTvType.setText(text);
                        break;

                    case DESCRIPTION_EDIT:
                        mTvDescription.setText(text);
                        break;

                    case QUANTITY_EDIT:
                        mTvQuantity.setText(text);
                        break;

                    case CODE_EDIT:
                        mTvCode.setText(text);
                        break;

                }
            }

            @Override
            public void eventCancel() {
            }
        });
        dialog.show();
    }
}
