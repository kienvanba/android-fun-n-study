package com.horical.gito.mvp.leaving.newLeaving.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.leaving.newLeaving.injection.module.LeavingNewModule;
import com.horical.gito.mvp.leaving.newLeaving.view.LeavingNewActivity;

import dagger.Component;

/**
 * Created by Dragonoid on 12/9/2016.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = LeavingNewModule.class)
public interface LeavingNewComponent {
    public void inject(LeavingNewActivity activity);
}
