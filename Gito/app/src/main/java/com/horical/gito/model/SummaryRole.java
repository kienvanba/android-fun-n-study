package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nhonvt.dk on 3/17/17
 */

public class SummaryRole implements Serializable {

    @SerializedName("view")
    @Expose
    private boolean view;
}
