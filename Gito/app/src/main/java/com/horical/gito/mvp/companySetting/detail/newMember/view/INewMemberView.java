package com.horical.gito.mvp.companySetting.detail.newMember.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface INewMemberView extends IView {
    void createMemberSuccess();

    void createMemberFailure(String err);
}
