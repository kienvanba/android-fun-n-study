package com.horical.gito.mvp.checkin.list.view.adapter;

import com.horical.gito.base.IRecyclerItem;

/**
 * Created by Penguin on 23-Dec-16.
 */

public class DayofMonth implements IRecyclerItem {
    public String Day;
    public String DayofWeek;
    public String Register;
    public String Working;

    public DayofMonth(String day, String dayofWeek, String register, String working) {
        this.Day = day;
        this.DayofWeek = dayofWeek;
        this.Register = register;
        this.Working = working;
    }

    @Override
    public int getItemViewType() {
        return 0;
    }
}