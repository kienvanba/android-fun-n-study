package com.horical.gito.mvp.companySetting.list.fragment.level.presenter;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface ILevelPresenter {
    void getAllLevel();
    void deleteLevel(String idLevel, int position);
}
