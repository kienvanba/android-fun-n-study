package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StepParam implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("to")
    @Expose
    private int to;

    @SerializedName("from")
    @Expose
    private int from;

    @SerializedName("value")
    @Expose
    private int value;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
