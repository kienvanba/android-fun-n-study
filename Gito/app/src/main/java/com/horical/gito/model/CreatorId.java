package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CreatorId implements Serializable {

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("displayName")
    @Expose
    public String displayName;

    @SerializedName("avatar")
    @Expose
    public TokenFile avatar;

    @SerializedName("level")
    @Expose
    public Level level;
}
