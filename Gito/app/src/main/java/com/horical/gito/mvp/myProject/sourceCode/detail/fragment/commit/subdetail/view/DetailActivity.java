package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.injection.component.DaggerDetailComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.injection.component.DetailComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.injection.module.DetailModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.presenter.CodeCommitDetailPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 23-Nov-16.
 */

public class DetailActivity extends BaseActivity implements IDetailView, View.OnClickListener {

    @Bind(R.id.code_detail_commit_file_list)
    RecyclerView mRVDetail;

    @Inject
    CodeCommitDetailPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_code_commit_detail_file);
        ButterKnife.bind(this);

        DetailComponent component = DaggerDetailComponent.builder()
                .detailModule(new DetailModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    private void initListener() {

    }

    private void initData() {

    }

    @Override
    public void onClick(View view) {

    }
}
