package com.horical.gito.mvp.companySetting.detail.newDepartment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.Department;
import com.horical.gito.model.Member;
import com.horical.gito.model.TokenFile;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by thanhle on 3/28/17.
 */

public class NewDepartmentAdapter extends RecyclerView.Adapter<NewDepartmentAdapter.NewDepartmentHolder> {
    private Context mContext;
    private List<Member> mList;
    private OnItemClickListener mOnItemClickListener;

    public NewDepartmentAdapter(Context context, List<Member> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public NewDepartmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_new_department, parent, false);
        return new NewDepartmentHolder(view);
    }

    @Override
    public void onBindViewHolder(NewDepartmentHolder holder, final int position) {
        Member member = mList.get(position);

        TokenFile avatar = member.getAvatar();
        if (avatar != null) {
            String url = CommonUtils.getURL(avatar);
            ImageLoader.load(mContext, url, holder.imvAvatar, holder.pgBar);
            holder.tvShortName.setVisibility(View.GONE);
        } else {
            holder.pgBar.setVisibility(View.GONE);
            holder.tvShortName.setText(member.getDisplayName().substring(0, 2).toUpperCase());
            holder.tvShortName.setVisibility(View.VISIBLE);
        }

        holder.tvName.setText(member.getDisplayName());
        holder.tvJob.setText(member.getLevel().getName());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class NewDepartmentHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imv_avatar)
        CircleImageView imvAvatar;

        @Bind(R.id.pgbar_avatar)
        ProgressBar pgBar;

        @Bind(R.id.tv_shortname)
        TextView tvShortName;

        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.tv_job)
        TextView tvJob;

        View view;

        public NewDepartmentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);
    }
}