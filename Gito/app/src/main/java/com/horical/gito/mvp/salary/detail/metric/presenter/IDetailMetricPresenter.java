package com.horical.gito.mvp.salary.detail.metric.presenter;

import com.horical.gito.model.Metric;
import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;

import java.util.List;

public interface IDetailMetricPresenter {
    List<ListStaffDto> getStaffs();

    Metric getMetric();

    void requestGetDetailMetric(String metricId);
}
