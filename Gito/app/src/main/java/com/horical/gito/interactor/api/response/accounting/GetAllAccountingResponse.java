package com.horical.gito.interactor.api.response.accounting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Accounting;

import java.util.List;

/**
 * Created by TrungTien on 12/29/2016
 */

public class GetAllAccountingResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Accounting> accountings;
}
