package com.horical.gito.mvp.logTime.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.horical.gito.R;
import com.horical.gito.model.LogTime;
import com.horical.gito.model.Section;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.utils.LogUtils;
import com.horical.gito.widget.button.GitOButton;
import com.horical.gito.widget.textview.GitOTextView;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LogTimeAdapter extends SectioningAdapter {

    public static final String TAG = LogUtils.makeLogTag(LogTimeAdapter.class);
    public static final int NEW = 0;
    public static final int APPROVE = 1;
    public static final int REJECT = 2;

    Context mContext;
    List<Section> mSections;
    OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onApproveClicked(int sectionIndex, int itemIndex);

        void onRejectClicked(int sectionIndex, int itemIndex);
    }

    public LogTimeAdapter(Context mContext, List<Section> mSections) {
        this.mContext = mContext;
        this.mSections = mSections;
    }

    public class HeaderViewHolder extends SectioningAdapter.HeaderViewHolder {

        @Bind(R.id.common_tv_header)
        GitOTextView tvTitle;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ItemViewHolder extends SectioningAdapter.ItemViewHolder {

        @Bind(R.id.tv_time)
        GitOTextView tvTime;

        @Bind(R.id.tv_title_content)
        GitOTextView tvTitleContent;

        @Bind(R.id.tv_status)
        GitOTextView tvStatus;

        @Bind(R.id.tv_content)
        GitOTextView tvContent;

        @Bind(R.id.ln_reject_approve)
        LinearLayout lnRejectApprove;

        @Bind(R.id.btn_reject)
        GitOButton btnReject;

        @Bind(R.id.btn_approve)
        GitOButton btnApprove;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public SectioningAdapter.HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerUserType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new HeaderViewHolder(inflater.inflate(R.layout.item_common_header, parent, false));
    }

    @Override
    public SectioningAdapter.ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemUserType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ItemViewHolder(inflater.inflate(R.layout.item_log_time_task, parent, false));
    }

    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex, int headerUserType) {
        Section s = mSections.get(sectionIndex);
        HeaderViewHolder hvh = (HeaderViewHolder) viewHolder;
        hvh.tvTitle.setText(s.getTitle());
    }

    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, final int sectionIndex, final int itemIndex, int itemUserType) {
        Section s = mSections.get(sectionIndex);
        LogTimeItem timeItem = (LogTimeItem) s.getItems().get(itemIndex);
        LogTime logTime = timeItem.getLogTime();

        ItemViewHolder ivh = (ItemViewHolder) viewHolder;

        ivh.tvTime.setText(DateUtils.getHourAndMinutes(logTime.getLogTimeDate()));
        ivh.tvTitleContent.setText(logTime.getTaskId().getTitle());
        ivh.tvContent.setText(logTime.getDesc());

        int status = logTime.getStatus();
        if (status == NEW) {
            ivh.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.orange_new));
            ivh.tvStatus.setText(mContext.getText(R.string.status_new));
            ivh.lnRejectApprove.setVisibility(View.VISIBLE);
        } else if (status == APPROVE) {
            ivh.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.blue_cross));
            ivh.tvStatus.setText(mContext.getText(R.string.status_approved));
            ivh.lnRejectApprove.setVisibility(View.GONE);
        } else if (status == REJECT) {
            ivh.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            ivh.tvStatus.setText(mContext.getText(R.string.status_rejected));
            ivh.lnRejectApprove.setVisibility(View.GONE);
        }

        ivh.btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onApproveClicked(sectionIndex, itemIndex);
            }
        });

        ivh.btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onRejectClicked(sectionIndex, itemIndex);
            }
        });
    }

    @Override
    public int getNumberOfSections() {
        return mSections.size();
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        return mSections.get(sectionIndex).getItems().size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public boolean doesSectionHaveFooter(int sectionIndex) {
        return false;
    }

    @Override
    public int getSectionHeaderUserType(int sectionIndex) {
        return mSections.get(sectionIndex).getType();
    }

    @Override
    public int getSectionItemUserType(int sectionIndex, int itemIndex) {
        Section section = mSections.get(sectionIndex);
        return section.getItems().get(itemIndex).getItemViewType();
    }
}
