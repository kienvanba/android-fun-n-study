package com.horical.gito.mvp.myProject.task.details.fragment.comment.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.task.details.fragment.comment.presenter.CommentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class CommentModule {

    @PerFragment
    @Provides
    CommentPresenter providerCommentPresenter() {
        return new CommentPresenter();
    }
}
