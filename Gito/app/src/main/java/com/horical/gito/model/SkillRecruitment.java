package com.horical.gito.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SkillRecruitment implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("skillDetail")
    @Expose
    private SkillDetail skillDetail;

    @SerializedName("candiId")
    @Expose
    private String candiId;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("total")
    @Expose
    private int total;

    @SerializedName("mark")
    @Expose
    private int mark;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public SkillDetail getSkillDetail() {
        return skillDetail;
    }

    public void setSkillDetail(SkillDetail skillDetail) {
        this.skillDetail = skillDetail;
    }

    public String getCandiId() {
        return candiId;
    }

    public void setCandiId(String candiId) {
        this.candiId = candiId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
