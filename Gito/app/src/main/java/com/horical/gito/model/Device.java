package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/21/17.
 */

public class Device implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("userId")
    @Expose
    private String userId;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("lastActive")
    @Expose
    private String lastActive;

    @SerializedName("deviceDescription")
    @Expose
    private String deviceDescription;

    @SerializedName("deviceOSVersion")
    @Expose
    private String deviceOSVersion;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("deviceOS")
    @Expose
    private String deviceOS;

    @SerializedName("deviceName")
    @Expose
    private String deviceName;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getLastActive() {
        return lastActive;
    }

    public void setLastActive(String lastActive) {
        this.lastActive = lastActive;
    }

    public String getDeviceDescription() {
        return deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }

    public String getDeviceOSVersion() {
        return deviceOSVersion;
    }

    public void setDeviceOSVersion(String deviceOSVersion) {
        this.deviceOSVersion = deviceOSVersion;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDeviceOS() {
        return deviceOS;
    }

    public void setDeviceOS(String deviceOS) {
        this.deviceOS = deviceOS;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
