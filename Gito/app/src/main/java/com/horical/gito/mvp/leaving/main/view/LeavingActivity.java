package com.horical.gito.mvp.leaving.main.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.wrapperModel.WrapperListUser;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.leaving.confirm.view.LeavingConfirmActivity;
import com.horical.gito.mvp.leaving.main.adapter.LeavingMonthInfoAdapter;
import com.horical.gito.mvp.leaving.main.adapter.LeavingPersonAdapter;
import com.horical.gito.mvp.leaving.main.adapter.LeavingPersonItem;
import com.horical.gito.mvp.leaving.main.injection.component.DaggerLeavingComponent;
import com.horical.gito.mvp.leaving.main.injection.component.LeavingComponent;
import com.horical.gito.mvp.leaving.main.injection.module.LeavingModule;
import com.horical.gito.mvp.leaving.main.presenter.LeavingPresenter;
import com.horical.gito.mvp.leaving.newLeaving.view.LeavingNewActivity;
import com.horical.gito.mvp.user.list.view.UserActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.FROM_MENU;
import static com.horical.gito.AppConstants.KEY_FROM;

public class LeavingActivity extends DrawerActivity implements View.OnClickListener, ILeavingView {

    @Bind(R.id.action_bar)
    CustomViewTopBar topBar;

    @Bind(R.id.leaving_layout_main)
    LinearLayout mLayout;

    @Bind(R.id.leaving_lv_month_info)
    RecyclerView mMonthList;

    @Bind(R.id.leaving_chart_holiday)
    LeavingChartView mChartHoliday;

    @Bind(R.id.leaving_chart_permit)
    LeavingChartView mChartPermit;

    @Bind(R.id.leaving_chart_leaveday)
    LeavingChartView mChartLeave;

    @Bind(R.id.leaving_tv_holiday)
    TextView mTvHoliday;

    @Bind(R.id.leaving_tv_permit_day)
    TextView mTvPermit;

    @Bind(R.id.leaving_tv_leaving_day)
    TextView mTvLeave;

    @Bind(R.id.leaving_btn_dropdown_person)
    ImageView mDropdownPerson;

    @Bind(R.id.leaving_lv_person)
    RecyclerView mPersonList;

    @Bind(R.id.leaving_layout_main_divider)
    FrameLayout mLayoutDivider;

    @Bind(R.id.leaving_layout_main_information)
    LinearLayout mLayoutInformation;

    @Inject
    LeavingPresenter mPresenter;

    LeavingMonthInfoAdapter mMonthAdapter;
    LeavingPersonAdapter mPersonAdapter;

    public final static int REQUEST_CODE_USER = 1;

//    LeavingNewDialog mNewDialog;

    Animation.AnimationListener mAnimationListenter;
    AnimationCollapseExpand mAniPersonHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        LeavingComponent component = DaggerLeavingComponent.builder()
                .leavingModule(new LeavingModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        initData();
        initListener();
//        mNewDialog = new LeavingNewDialog(this);
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_LEAVING_REGISTER;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_leaving;
    }

    protected void initData() {

        topBar.setTextTitle(getString(R.string.leaving));
        int from = getIntent().getIntExtra(KEY_FROM, FROM_MENU);
        mPresenter.setFrom(from);
        if (from == FROM_MENU) {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        } else {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
            lockDrawer();
        }
        topBar.setImageViewRight(
                CustomViewTopBar.DRAWABLE_USER_CONFIRM,
                CustomViewTopBar.DRAWABLE_FILTER,
                CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT
        );


        //-------------------test area-------------
        mPresenter.generateTestData();
//        mPresenter.setResources(getResources());


        //-----------------------------------------

        mPresenter.attachView(this);
        mPresenter.onCreate();

//        mPresenter.getAllLeaving();

        mMonthAdapter = new LeavingMonthInfoAdapter(mPresenter.getMonthList(), this);
        LinearLayoutManager layoutManagerMonth = new LinearLayoutManager(this);
        mMonthList.setLayoutManager(layoutManagerMonth);
        mMonthList.setHasFixedSize(true);
        mMonthList.setAdapter(mMonthAdapter);

        mPersonAdapter = new LeavingPersonAdapter(mPresenter.getPersonList(), this);
        LinearLayoutManager layoutManagerPerson = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mPersonList.setLayoutManager(layoutManagerPerson);
        mPersonList.setHasFixedSize(true);
        mPersonList.setAdapter(mPersonAdapter);

        mPersonList.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mAniPersonHelper = new AnimationCollapseExpand(mPersonList);
                mAniPersonHelper.Minimize();
                mPersonList.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

    }

    protected void initListener() {
        mDropdownPerson.setOnClickListener(this);

        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                if (mPresenter.getFrom() == FROM_MENU) openDrawer();
                else onBackPressed();
            }

            @Override
            public void onRightButtonOneClicked() {
                Intent intent = new Intent(LeavingActivity.this, LeavingNewActivity.class);
                startActivity(intent);
            }

            @Override
            public void onRightButtonTwoClicked() {
                Intent intent = new Intent(LeavingActivity.this, UserActivity.class);
                intent.putExtra(
                        UserActivity.KEY_FROM,
                        UserActivity.FROM_FILTER_LEAVING
                );
                startActivityForResult(intent, REQUEST_CODE_USER);
            }

            @Override
            public void onRightButtonThreeClicked() {
                Intent intent = new Intent(LeavingActivity.this, LeavingConfirmActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mDropdownPerson.getId()) {
            if (mAniPersonHelper.isExpanded()) {
                mAniPersonHelper.Collapse();
                animationPullupPerson();
            } else {
                mAniPersonHelper.Expand();
                animationDropdownPerson();
            }
            return;
        }
    }

    //GET DATA FROM FILTER
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_USER) {
            if (resultCode == Activity.RESULT_OK) {
                //day la du lieu nhan dc
                WrapperListUser wrapperListUser;
                wrapperListUser = (WrapperListUser) data.getSerializableExtra(UserActivity.KEY_INTENT_USER_LIST);
                if (wrapperListUser != null && !wrapperListUser.getMyUsers().isEmpty()) {
                    for (int i = 0; i < wrapperListUser.getMyUsers().size(); i++) {
                        mPresenter.getPersonList().add( wrapperListUser.getMyUsers().get(i));
                    }
                }
            }
        }
    }
    // Animation for Person Picked Bar --------------------------------------

    private void animationDropdownPerson() {
        Animation animationDown = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.ABSOLUTE, -mAniPersonHelper.getFullHeight(), Animation.ABSOLUTE, 0);
        animationDown.setDuration(300);
        animationDown.setAnimationListener(mAnimationListenter);
        animationDown.setFillAfter(true);
        mLayoutDivider.startAnimation(animationDown);
        mLayoutInformation.startAnimation(animationDown);
        mMonthList.startAnimation(animationDown);

    }

    private void animationPullupPerson() {
        Animation animationUp = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.ABSOLUTE, mAniPersonHelper.getFullHeight(), Animation.ABSOLUTE, 0);
        animationUp.setDuration(300);
        animationUp.setFillAfter(true);
        animationUp.setAnimationListener(mAnimationListenter);
        mLayoutDivider.startAnimation(animationUp);
        mLayoutInformation.startAnimation(animationUp);
        mMonthList.startAnimation(animationUp);
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllLeavingSuccess() {
        hideLoading();
        showToast("Success");
    }

    @Override
    public void getAllLeavingFailure(String error) {
        hideLoading();
        showToast("Failure");
    }

    @Override
    public void getLeavingByIdSuccess() {
        hideLoading();
        showToast("Success");
    }

    @Override
    public void getLeavingByIdFailure(String error) {
        hideLoading();
        showToast("Failure");
    }

    public class AnimationCollapseExpand {
        int mFullHeight;
        View mView;
        Animation mAnimationCollapse;
        Animation mAnimationExpand;

        private AnimationCollapseExpand(View view) {
            mView = view;
            mFullHeight = view.getHeight();

            mAnimationCollapse = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                    Animation.ABSOLUTE, 0, Animation.ABSOLUTE, -mFullHeight);
            mAnimationCollapse.setDuration(300);
            mAnimationCollapse.setFillAfter(true);

            mAnimationExpand = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                    Animation.ABSOLUTE, -mFullHeight, Animation.ABSOLUTE, 0);
            mAnimationExpand.setDuration(300);
            mAnimationExpand.setFillAfter(true);
        }

        private void Collapse() {
            mView.startAnimation(mAnimationCollapse);
            Minimize();
        }

        private void Expand() {
            mView.startAnimation(mAnimationExpand);
            Restore();
        }

        private boolean isExpanded() {
            if (mView.getVisibility() == View.GONE) return false;
            return true;
        }

        private void Minimize() {
            mView.setVisibility(View.GONE);
        }

        private void Restore() {
            mView.setVisibility(View.VISIBLE);
        }

        private int getFullHeight() {
            return mFullHeight;
        }
    }
}
