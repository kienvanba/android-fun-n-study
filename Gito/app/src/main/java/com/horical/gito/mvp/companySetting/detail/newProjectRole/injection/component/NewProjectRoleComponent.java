package com.horical.gito.mvp.companySetting.detail.newProjectRole.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.injection.module.NewProjectRoleModule;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.view.NewProjectRoleActivity;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = NewProjectRoleModule.class)
public interface NewProjectRoleComponent {
    void inject(NewProjectRoleActivity activity);
}
