package com.horical.gito.mvp.salary.dto.metric;

public class ListMetricDto {
    private String id;
    private String name;
    private String acronym;

    public ListMetricDto(String name, String acronym) {
        this.name = name;
        this.acronym = acronym;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }
}
