package com.horical.gito.mvp.myProject.setting.fragment.activity.version.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.injection.module.VersionModule;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.view.NewVersionActivity;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.view.VersionDetailActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = VersionModule.class)
public interface VersionComponent {
    void inject(VersionDetailActivity activity);
    void inject(NewVersionActivity activity);
}
