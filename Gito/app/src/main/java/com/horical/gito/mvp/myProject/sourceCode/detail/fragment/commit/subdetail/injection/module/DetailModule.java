package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.presenter.CodeCommitDetailPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */
@Module
public class DetailModule {
    @PerActivity
    @Provides
    CodeCommitDetailPresenter provideCodeCommitDetailPresenter(){
        return new CodeCommitDetailPresenter();
    }
}
