package com.horical.gito.mvp.myProject.sourceCode.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Code;
import com.horical.gito.utils.DateUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class SourceCodeAdapter extends RecyclerView.Adapter<SourceCodeAdapter.CodeHolder> {

    private List<Code> mList;
    private Context mContext;

    private OnItemClickListener mOnItemClickListener;

    public SourceCodeAdapter(Context mContext, List<Code> mList) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public CodeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_code, parent, false);
        return new CodeHolder(view);
    }

    @Override
    public void onBindViewHolder(CodeHolder holder, final int position) {
        Code code = mList.get(position);

        holder.tvLink.setText(code.getUrl());
        holder.tvTitle.setText(code.getName());
        holder.tvDate.setText(DateUtils.formatDate(code.getCreatedDate(), "yyyy-MM-dd"));
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickListener(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class CodeHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.code_tv_title)
        TextView tvTitle;

        @Bind(R.id.code_tv_link)
        TextView tvLink;

        @Bind(R.id.item_code_imv_delete)
        ImageView imgDelete;

        @Bind(R.id.code_tv_date)
        TextView tvDate;

        View view;

        public CodeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}
