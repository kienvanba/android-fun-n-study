package com.horical.gito.mvp.myProject.setting.fragment.activity.member.view;

import com.horical.gito.base.IView;

public interface INewMemberView extends IView{
    void showLoading();
    void hideLoading();
    void getAllUserSuccess();
    void getAllRoleSuccess();
}
