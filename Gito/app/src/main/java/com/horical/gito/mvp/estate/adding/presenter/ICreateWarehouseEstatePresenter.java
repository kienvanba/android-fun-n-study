package com.horical.gito.mvp.estate.adding.presenter;

import java.io.File;

import okhttp3.MediaType;

/**
 * Created by hoangsang on 4/13/17.
 */

public interface ICreateWarehouseEstatePresenter {

    void createWarehouseEstate();

    void uploadFile(MediaType type, File file);
}
