package com.horical.gito.model;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.R;
import com.horical.gito.mvp.report.view.ReportActivity;
import com.horical.gito.utils.DateUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Reports implements Serializable{

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("creatorId")
    @Expose
    private String creatorId;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("finished")
    @Expose
    private String finished;

    @SerializedName("problem")
    @Expose
    private String problem;

    @SerializedName("doing")
    @Expose
    private String doing;

    @SerializedName("rejectMsg")
    @Expose
    private String rejectMsg;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("createdDate")
    @Expose
    private Date createdDate;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("informTo")
    @Expose
    private InformTo informTo;

    @SerializedName("reportTo")
    @Expose
    private List<ReportTo> reportTo;

    @SerializedName("approverId")
    @Expose
    private ApproverId approverId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getFinished() {
        return finished;
    }

    public void setFinished(String finished) {
        this.finished = finished;
    }

    public String getProblem() {
        return problem;
    }

    public String getRejectMsg() {
        return rejectMsg;
    }

    public void setRejectMsg(String rejectMsg) {
        this.rejectMsg = rejectMsg;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getDoing() {
        return doing;
    }

    public void setDoing(String doing) {
        this.doing = doing;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public InformTo getInformTo() {
        return informTo;
    }

    public void setInformTo(InformTo informTo) {
        this.informTo = informTo;
    }

    public List<ReportTo> getReportTo() {
        return reportTo;
    }

    public void setReportTo(List<ReportTo> reportTo) {
        this.reportTo = reportTo;
    }

    public ApproverId getApproverId() {
        return approverId;
    }

    public void setApproverId(ApproverId approverId) {
        this.approverId = approverId;
    }

    public String showDate() {
        return DateUtils.formatDate(createdDate);
    }

    public String showStatus(Context context) {
        switch (status) {
            case ReportActivity.STATUS_NEW:
                return context.getString(R.string.status_new);
            case ReportActivity.STATUS_APPROVED:
                return context.getString(R.string.status_approved);
            case ReportActivity.STATUS_REJECTED:
                return context.getString(R.string.reject);
            case ReportActivity.STATUS_SUBMITTED:
                return context.getString(R.string.status_submitted);
        }
        return "";
    }
}
