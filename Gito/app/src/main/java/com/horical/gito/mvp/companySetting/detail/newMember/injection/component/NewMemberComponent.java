package com.horical.gito.mvp.companySetting.detail.newMember.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.detail.newMember.injection.module.NewMemberModule;
import com.horical.gito.mvp.companySetting.detail.newMember.view.NewMemberActivity;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = NewMemberModule.class)
public interface NewMemberComponent {
    void inject(NewMemberActivity fragment);
}
