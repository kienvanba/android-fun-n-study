package com.horical.gito.mvp.roomBooking.detail.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.horical.gito.mvp.estate.detail.adapter.FragmentPaggerAdapter;
import com.horical.gito.mvp.roomBooking.detail.fragment.day.view.DayFragment;
import com.horical.gito.mvp.roomBooking.detail.fragment.month.view.MonthFragment;
import com.horical.gito.mvp.roomBooking.detail.fragment.week.view.WeekFragment;

/**
 * Created by Lemon on 3/15/2017.
 */

public class RoomDetailViewPagerAdapter extends FragmentPaggerAdapter {
    public RoomDetailViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return DayFragment.newInstance();
            case 1:
                return WeekFragment.newInstance();
            case 2:
                return MonthFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
