package com.horical.gito.mvp.caseStudy.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.caseStudy.list.presenter.CaseStudyPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by QuangHai on 10/11/2016.
 */

@Module
public class CaseStudyModule {

    @PerActivity
    @Provides
    CaseStudyPresenter provideCaseStudyPresenter(){
        return new CaseStudyPresenter();
    }
}
