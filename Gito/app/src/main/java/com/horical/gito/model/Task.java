package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Task implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("taskId")
    @Expose
    private int taskId;
/*
    //false department, roleCompany
    @SerializedName("creatorId")
    @Expose
    private User creatorId;
*/
    @SerializedName("ProjectId")
    @Expose
    private String projectId;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("desc")
    @Expose
    private String desc;
/*
    //miss releaseNote
    @SerializedName("version")
    @Expose
    private Version version;
*/
    @SerializedName("weight")
    @Expose
    private int weight;
/*
    @SerializedName("comments")
    @Expose
    private List<Comments> comments;
*/
/*
    @SerializedName("logTimes")
    @Expose
    private List<LogTime> logTimes;
*/
    @SerializedName("modifiedDate")
    @Expose
    private Date modifiedDate;

    @SerializedName("createdDate")
    @Expose
    private Date createDate;

//    @SerializedName("relatedTasks")
//    public List<RelatedTasks> relatedTasks;
//
//    @SerializedName("subTasks")
//    public List<SubTasks> subTasks;

    @SerializedName("estimatedTime")
    @Expose
    private float estimatedTime;

    @SerializedName("spentTime")
    @Expose
    private float spentTime;

    @SerializedName("endDate")
    @Expose
    private Date endDate;

    @SerializedName("startDate")
    @Expose
    private Date startDate;

    @SerializedName("watchers")
    @Expose
    private List<Watchers> watchers;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("priority")
    @Expose
    private int priority;

    @SerializedName("attachFiles")
    @Expose
    private List<TokenFile> attachFiles;

    @SerializedName("assigns")
    @Expose
    public List<User> assigns;

    @SerializedName("taskType")
    @Expose
    private int taskType;

    @SerializedName("components")
    @Expose
    private List<Component> components;

    @SerializedName("dueDate")
    @Expose
    private String dueDate;

    @SerializedName("percent")
    @Expose
    private int percent;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }
/*
    public User getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(User creatorId) {
        this.creatorId = creatorId;
    }
*/
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
/*
    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }
*/
    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
/*
    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }
*/
/*
    public List<LogTime> getLogTimes() {
        return logTimes;
    }

    public void setLogTimes(List<LogTime> logTimes) {
        this.logTimes = logTimes;
    }
*/

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public float getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(float estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public float getSpentTime() {
        return spentTime;
    }

    public void setSpentTime(float spentTime) {
        this.spentTime = spentTime;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public List<Watchers> getWatchers() {
        return watchers;
    }

    public void setWatchers(List<Watchers> watchers) {
        this.watchers = watchers;
    }

    public List<User> getAssigns() {
        return assigns;
    }

    public void setAssigns(List<User> assigns) {
        this.assigns = assigns;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public List<TokenFile> getAttachFiles() {
        return attachFiles;
    }

    public void setAttachFiles(List<TokenFile> attachFiles) {
        this.attachFiles = attachFiles;
    }

    public int getTaskType() {
        return taskType;
    }

    public void setTaskType(int taskType) {
        this.taskType = taskType;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }
}

