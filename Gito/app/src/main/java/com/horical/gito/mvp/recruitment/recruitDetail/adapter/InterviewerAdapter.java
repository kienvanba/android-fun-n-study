package com.horical.gito.mvp.recruitment.recruitDetail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.InterViewer;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class InterviewerAdapter extends RecyclerView.Adapter<InterviewerAdapter.ViewHolderItem> {

    private Context context;
    private List<Object> list;
    private OnItemClickListener mCallBack;

    public InterviewerAdapter(Context context, List<Object> list, OnItemClickListener callBack) {
        this.context = context;
        this.list = list;
        this.mCallBack = callBack;
    }

    @Override
    public InterviewerAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_interviewer, parent, false);
        return new ViewHolderItem(view);
    }

    @Override
    public void onBindViewHolder(InterviewerAdapter.ViewHolderItem holder, final int position) {
        holder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallBack != null) {
                    mCallBack.onDeleteItem(position);
                }
            }
        });

        if (list.get(position) instanceof InterViewer) {
            InterViewer itemUser = (InterViewer) list.get(position);
            holder.tvName.setText(itemUser.getDisplayName());
            holder.tvDepartment.setText(itemUser.getLevel().getName());

            if (itemUser.getAvatar() == null || TextUtils.isEmpty(itemUser.getAvatar().getTokenFile())) {
                holder.tvPhoto.setVisibility(View.VISIBLE);
                holder.imvPhoto.setVisibility(View.GONE);
                holder.tvPhoto.setText(itemUser.showNamePhoto());
            } else {
                holder.imvPhoto.setVisibility(View.VISIBLE);
                holder.tvPhoto.setVisibility(View.GONE);
                ImageLoader.load(context, CommonUtils.getURL(itemUser.getAvatar()), holder.imvPhoto, null);
            }

        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallBack != null) {
                    mCallBack.onItemClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_photo)
        TextView tvPhoto;

        @Bind(R.id.imv_photo)
        CircleImageView imvPhoto;

        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.tv_department)
        TextView tvDepartment;

        @Bind(R.id.imv_delete)
        ImageView imvDelete;

        public ViewHolderItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onDeleteItem(int positionItem);

        void onItemClick(int position);
    }
}
