package com.horical.gito.mvp.salary.bonus.list.fragments.mine.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.bonus.list.fragments.mine.injection.module.ListMineFragmentModule;
import com.horical.gito.mvp.salary.bonus.list.fragments.mine.view.ListMineFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ListMineFragmentModule.class)
public interface IListMineFragmentComponent {
    void inject(ListMineFragment activity);
}
