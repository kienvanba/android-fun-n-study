package com.horical.gito.mvp.dialog.dialogInputEdit;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.horical.gito.R;
import com.horical.gito.base.BaseDialog;
import com.horical.gito.widget.button.GitOButton;
import com.horical.gito.widget.edittext.GitOEditText;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by hoangsang on 4/14/17.
 */

public class TwoRowsInputTextDialog extends Dialog implements View.OnClickListener {

    @Bind(R.id.edt_one)
    public GitOEditText mEdtOne;

    @Bind(R.id.edt_two)
    public GitOEditText mEdtTwo;

    @Bind(R.id.btn_negative)
    public GitOButton mBtnNegative;

    @Bind(R.id.btn_positive)
    public GitOButton mBtnPositive;

    private String hintTextOne;
    private String hintTextTwo;
    private String textBtnNegative;
    private String textBtnPositive;

    public TwoRowsInputTextDialog(Context context, String hintTextOne, String hintTextTwo,
                                  String textBtnNegative, String textBtnPositive) {
        super(context);
        this.hintTextOne = hintTextOne;
        this.hintTextTwo = hintTextTwo;
        this.textBtnNegative = textBtnNegative;
        this.textBtnPositive = textBtnPositive;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_two_rows_input_text);
        if (getWindow() != null) {
            getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        }

        this.setCanceledOnTouchOutside(false);
        ButterKnife.bind(this);

        initData();
        initListener();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_negative:
                mListener.onNegativeClick();
                break;

            case R.id.btn_positive:
                mListener.onPositiveClick();
                break;
        }
    }

    private void initData() {
        mEdtOne.setHint(hintTextOne);
        mEdtTwo.setHint(hintTextTwo);
        mBtnNegative.setText(textBtnNegative);
        mBtnPositive.setText(textBtnPositive);
    }

    private void initListener() {
        mBtnNegative.setOnClickListener(this);
        mBtnPositive.setOnClickListener(this);
    }

    private OnListener mListener;

    public void setListener(OnListener mListener) {
        this.mListener = mListener;
    }

    public interface OnListener {
        void onNegativeClick();

        void onPositiveClick();
    }

    public String getTextOne() {
        return mEdtOne.getText().toString();
    }

    public String getTextTwo() {
        return mEdtTwo.getText().toString();
    }


}
