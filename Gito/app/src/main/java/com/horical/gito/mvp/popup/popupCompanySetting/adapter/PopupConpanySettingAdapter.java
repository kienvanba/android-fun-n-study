package com.horical.gito.mvp.popup.popupCompanySetting.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.companySetting.list.fragment.members.dto.MemberDto;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 3/27/17.
 */

public class PopupConpanySettingAdapter extends RecyclerView.Adapter<PopupConpanySettingAdapter.ViewHolder> {
    private List<MemberDto> mList;
    private Context context;
    private String keySelected = "";

    public PopupConpanySettingAdapter(Context context, List<MemberDto> list, String keySelected) {
        this.context = context;
        this.keySelected = keySelected;
        this.mList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_popup_task, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if(keySelected.toLowerCase().equalsIgnoreCase("")){
            holder.tvName.setSelected(false);
        }else {
            if (keySelected.toLowerCase().equalsIgnoreCase(mList.get(position).getName().toLowerCase())) {
                holder.tvName.setTextColor(context.getResources().getColor(R.color.app_color_opacity));
                holder.tvSelected.setBackgroundColor(context.getResources().getColor(R.color.app_color_opacity));
            }
        }

        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keySelected = mList.get(position).getName();
                mCallBack.onClick(mList.get(position).getName(), mList.get(position).getId());
            }
        });
        holder.tvName.setText(mList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.edit_name)
        TextView tvName;

        @Bind(R.id.tv_selected)
        TextView tvSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    OnItemClickListener mCallBack;

    public void setOnItemClickListener(OnItemClickListener callBack) {
        this.mCallBack = callBack;
    }

    public interface OnItemClickListener {
        void onClick(String key, String id);
    }
}
