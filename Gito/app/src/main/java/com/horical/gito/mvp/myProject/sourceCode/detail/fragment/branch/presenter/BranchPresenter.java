package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.view.IBranchView;

import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class BranchPresenter extends BasePresenter implements IBranchPresenter {

    private List<String> mList;

    public void attachView(IBranchView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IBranchView getView() {
        return (IBranchView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<String> getListBranch() {
        return mList;
    }

    public void setListBranch(List<String> mList) {
        this.mList = mList;
    }
}

