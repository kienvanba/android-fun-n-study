package com.horical.gito.mvp.roomBooking.detail.fragment.day.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lemon on 4/9/2017.
 */

public class DayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<DayItem> mList;
    private OnItemClickListener callback;

    public DayAdapter(Context context, List<DayItem> mList, OnItemClickListener callback) {
        this.mList = mList;
        this.context = context;
        this.callback = callback;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hour_detail, parent, false);
        return new DayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DayItem dayItem = (DayItem) mList.get(position);
        DayViewHolder viewHolder = (DayViewHolder) holder;

    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class DayViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.tv_des_room)
        GitOTextView mTvDesRoom;

        @Bind(R.id.imv_edit)
        ImageView mBtnEdit;

        public DayViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == mBtnEdit.getId()) {
                callback.onDayEdit(getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onDayEdit(int position);
    }

}
