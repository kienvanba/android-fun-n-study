package com.horical.gito.mvp.roomBooking.detail.fragment.month.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.roomBooking.detail.fragment.month.presenter.MonthPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by QuangHai on 08/11/2016.
 */

@Module
public class MonthModule {
    @PerFragment
    @Provides
    MonthPresenter provideMonthPresenter() {
        return new MonthPresenter();
    }
}
