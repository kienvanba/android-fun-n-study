package com.horical.gito.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.horical.gito.interactor.api.network.deserialize.CompanyDeserialize;
import com.horical.gito.interactor.api.network.deserialize.DateSerializeAndDeserialize;
import com.horical.gito.interactor.api.network.deserialize.DepartmentDeserialize;
import com.horical.gito.interactor.api.network.deserialize.LevelDeserialize;
import com.horical.gito.interactor.api.network.deserialize.MemberDeserialize;
import com.horical.gito.interactor.api.network.deserialize.ProjectDeserialize;
import com.horical.gito.interactor.api.network.deserialize.RoleCompanyDeserialize;
import com.horical.gito.interactor.api.network.deserialize.TaskDeserialize;
import com.horical.gito.interactor.api.network.deserialize.TokenFileDeserialize;
import com.horical.gito.interactor.api.network.deserialize.UserDeserialize;
import com.horical.gito.model.Company;
import com.horical.gito.model.Department;
import com.horical.gito.model.Project;
import com.horical.gito.model.RoleCompany;
import com.horical.gito.model.Task;
import com.horical.gito.model.User;
import com.horical.gito.model.Level;
import com.horical.gito.model.Member;
import com.horical.gito.model.TokenFile;

import java.util.Date;

public class GsonUtils {

    public static Gson createGson(Class exClazz) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        if (exClazz != Company.class) {
            gsonBuilder.registerTypeAdapter(Company.class, new CompanyDeserialize());
        }
        if (exClazz != Department.class) {
            gsonBuilder.registerTypeAdapter(Department.class, new DepartmentDeserialize());
        }
        if (exClazz != Member.class) {
            gsonBuilder.registerTypeAdapter(Member.class, new MemberDeserialize());
        }
        if (exClazz != TokenFile.class) {
            gsonBuilder.registerTypeAdapter(TokenFile.class, new TokenFileDeserialize());
        }
        if (exClazz != Level.class) {
            gsonBuilder.registerTypeAdapter(Level.class, new LevelDeserialize());
        }
        if (exClazz != User.class) {
            gsonBuilder.registerTypeAdapter(User.class, new UserDeserialize());
        }
        if (exClazz != RoleCompany.class) {
            gsonBuilder.registerTypeAdapter(RoleCompany.class, new RoleCompanyDeserialize());
        }
        if (exClazz != Task.class) {
            gsonBuilder.registerTypeAdapter(Task.class, new TaskDeserialize());
        }
        if (exClazz != Project.class) {
            gsonBuilder.registerTypeAdapter(Project.class, new ProjectDeserialize());
        }
        gsonBuilder.registerTypeHierarchyAdapter(Date.class, new DateSerializeAndDeserialize());
        return gsonBuilder.create();
    }

}
