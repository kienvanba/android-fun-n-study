package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.utils.HDate;

import java.util.Date;
import java.util.List;

public class Version {

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("endDate")
    @Expose
    public Date endDate;

    @SerializedName("desc")
    @Expose
    public String description;

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("creatorId")
    @Expose
    private String creatorId;

    @SerializedName("ProjectId")
    @Expose
    private String projectId;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("versionId")
    @Expose
    private String versionId;

    @SerializedName("releaseFiles")
    @Expose
    private List<TokenFile> releaseFiles;

    @SerializedName("releaseNote")
    @Expose
    private String releaseNote;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("createDate")
    @Expose
    private String createDate;

    @SerializedName("percent")
    @Expose
    private int percent;

    private int totalTask = 0;

    private int resolvedTask = 0;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<TokenFile> getReleaseFiles() {
        return releaseFiles;
    }

    public void setReleaseFiles(List<TokenFile> releaseFiles) {
        this.releaseFiles = releaseFiles;
    }

    public String getReleaseNote() {
        return releaseNote;
    }

    public void setReleaseNote(String releaseNote) {
        this.releaseNote = releaseNote;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTotalTask() {
        return totalTask;
    }

    public void increaseTotalTask(int total) {
        this.totalTask += total;
    }

    public int getResolvedTask() {
        return resolvedTask;
    }

    public void increaseResolvedTask(int total) {
        this.resolvedTask += total;
    }
}
