package com.horical.gito.mvp.salary.detail.salary.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.salary.dto.metric.SummaryDto;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailSummaryAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<SummaryDto> items;
    private SummaryAdapterListener callback;

    public DetailSummaryAdapter(Context context, List<SummaryDto> items, SummaryAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_summary, parent, false);
        return new SummaryHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SummaryDto summaryDto = items.get(position);
        SummaryHolder summaryHolder = (SummaryHolder) holder;
        summaryHolder.tvName.setText(summaryDto.getTitle());
        summaryHolder.tvAcronym.setText(summaryDto.getAcronym());
        summaryHolder.tvTotal.setText(CommonUtils.amountFormatEnGB(summaryDto.getTotal(), "usd"));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class SummaryHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.tv_acronym)
        TextView tvAcronym;

        @Bind(R.id.tv_total)
        TextView tvTotal;

        @Bind(R.id.imv_remove)
        ImageView imvRemove;

        SummaryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imvRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onRemove(getAdapterPosition());
                }
            });
        }
    }

    public interface SummaryAdapterListener {
        void onRemove(int position);
    }
}