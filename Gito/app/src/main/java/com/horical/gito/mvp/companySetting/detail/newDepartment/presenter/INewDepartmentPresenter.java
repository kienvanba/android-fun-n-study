package com.horical.gito.mvp.companySetting.detail.newDepartment.presenter;

import com.horical.gito.model.Department;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface INewDepartmentPresenter {
    void updateDepartment(Department department);

    void createDepartment(Department department);
}
