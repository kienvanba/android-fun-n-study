package com.horical.gito.mvp.myProject.task.details.fragment.comment.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.task.details.fragment.comment.view.ICommentView;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.presenter.IOverViewPresenter;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class CommentPresenter extends BasePresenter implements IOverViewPresenter {

    private static final String TAG = makeLogTag(CommentPresenter.class);

    public ICommentView getView() {
        return ((ICommentView) getIView());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

}
