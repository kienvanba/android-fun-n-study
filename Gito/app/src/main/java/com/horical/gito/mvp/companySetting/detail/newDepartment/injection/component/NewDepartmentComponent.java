package com.horical.gito.mvp.companySetting.detail.newDepartment.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.detail.newDepartment.injection.module.NewDepartmentModule;
import com.horical.gito.mvp.companySetting.detail.newDepartment.view.NewDepartmentActivity;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = NewDepartmentModule.class)
public interface NewDepartmentComponent {
    void inject(NewDepartmentActivity fragment);
}
