package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.BaseSourceCodeFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.adapter.TagAdapter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.injection.component.DaggerTagComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.injection.component.TagComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.injection.moduel.TagModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.presenter.TagPresenter;
import com.horical.gito.mvp.myProject.sourceCode.detail.view.SourceCodeDetailActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 23-Nov-16.
 */

public class TagFragment extends BaseSourceCodeFragment implements ITagView, View.OnClickListener {

    @Bind(R.id.code_detail_branch_list)
    RecyclerView rcvTag;

    private TagAdapter adapterTag;

    @Inject
    TagPresenter mPresenter;

    public static TagFragment newInstance() {

        Bundle args = new Bundle();

        TagFragment fragment = new TagFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_code_branch;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        TagComponent component = DaggerTagComponent.builder()
                .tagModule(new TagModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }


    @Override
    protected void initData() {
//        adapterTag = new TagAdapter(getContext(), mPresenter.getListTag());
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
//        rcvTag.setLayoutManager(layoutManager);
//        rcvTag.setHasFixedSize(false);
//        rcvTag.setAdapter(adapterTag);
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View view) {

    }
}