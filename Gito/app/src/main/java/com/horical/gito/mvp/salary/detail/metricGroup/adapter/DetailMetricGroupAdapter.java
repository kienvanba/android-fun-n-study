package com.horical.gito.mvp.salary.detail.metricGroup.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Metric;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailMetricGroupAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<Metric> items;
    private MetricAdapterListener callback;

    public DetailMetricGroupAdapter(Context context, List<Metric> items, MetricAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    public void setDataChanged(List<Metric> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_metric, parent, false);
        return new MetricHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Metric metric = items.get(position);
        MetricHolder metricHolder = (MetricHolder) holder;
        metricHolder.tvName.setText(metric.getTitle());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class MetricHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_metric_name)
        TextView tvName;

        @Bind(R.id.imv_metric_delete)
        ImageView imvDelete;

        View view;

        MetricHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onSelected(getAdapterPosition());
                }
            });

            imvDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onRemove(getAdapterPosition());
                }
            });
        }
    }

    public interface MetricAdapterListener {
        void onSelected(int position);

        void onRemove(int position);
    }
}