package com.horical.gito.mvp.companySetting.list.fragment.devices.presenter;

import android.util.Log;

import com.horical.gito.AppConstants;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.companySetting.GetDevicesResponse;
import com.horical.gito.model.Device;
import com.horical.gito.model.Id;
import com.horical.gito.mvp.companySetting.list.fragment.devices.view.IDevicesView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DevicesPresenter extends BasePresenter implements IDevicesPresenter {

    private List<Id> listDataHeader;
    private HashMap<Id, List<Device>> listDataChild;

    public void attachView(IDevicesView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IDevicesView getView() {
        return (IDevicesView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        listDataHeader = new ArrayList<Id>();
        listDataChild = new HashMap<Id, List<Device>>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Id> getListDataHeader() {
        return listDataHeader;
    }

    public void setListDataHeader(List<Id> listDataHeader) {
        this.listDataHeader = listDataHeader;
    }

    public HashMap<Id, List<Device>> getListDataChild() {
        return listDataChild;
    }

    public void setListDataChild(HashMap<Id, List<Device>> listDataChild) {
        this.listDataChild = listDataChild;
    }

    @Override
    public void getAllDevices() {
        getApiManager().getAllDevices(new ApiCallback<GetDevicesResponse>() {
            @Override
            public void success(GetDevicesResponse res) {
                if(listDataHeader != null && listDataChild != null){
                    listDataHeader.clear();
                    listDataChild.clear();
                } else {
                    listDataHeader = new ArrayList<Id>();
                    listDataChild = new HashMap<Id, List<Device>>();
                }
                for (int i = 0; i < res.devices.size(); i++) {
                    listDataHeader.add(res.devices.get(i).get_id());
                    listDataChild.put(res.devices.get(i).get_id(), res.devices.get(i).getDevices());
                }
                getView().getAllDeviceSuccess();
            }

            @Override
            public void failure(RestError error) {
                if(listDataHeader != null && listDataChild != null){
                    listDataHeader.clear();
                    listDataChild.clear();
                } else {
                    listDataHeader = new ArrayList<Id>();
                    listDataChild = new HashMap<Id, List<Device>>();
                }
                getView().getAllDeviceFailure(error.message);
            }
        });
    }

    @Override
    public void deleteDevice(String idDevice, final int position) {
        getApiManager().deleteDevice(idDevice, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteDeviceSuccess(position);
            }

            @Override
            public void failure(RestError error) {
                getView().deleteDeviceFailure(error.message);
            }
        });
    }
}

