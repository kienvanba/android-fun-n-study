package com.horical.gito.mvp.allProject.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.allProject.detail.presenter.ProjectDetailPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ProjectDetailModule {

    @Provides
    @PerActivity
    ProjectDetailPresenter provideProjectDetailPresenter() {
        return new ProjectDetailPresenter();
    }

}
