package com.horical.gito.mvp.allProject.detail.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ProgressBar;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.allProject.detail.injection.component.DaggerProjectDetailComponent;
import com.horical.gito.mvp.allProject.detail.injection.component.ProjectDetailComponent;
import com.horical.gito.mvp.allProject.detail.injection.module.ProjectDetailModule;
import com.horical.gito.mvp.allProject.detail.presenter.ProjectDetailPresenter;
import com.horical.gito.mvp.dialog.dialogPhoto.DialogPhoto;
import com.horical.gito.utils.FileUtils;
import com.horical.gito.utils.NetworkUtils;
import com.horical.gito.widget.edittext.GitOEditText;

import java.io.File;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;

public class ProjectDetailActivity extends BaseActivity implements View.OnClickListener, IProjectDetailView {

    private static final int REQUEST_PERMISSION_CAPTURE_IMAGE = 0;
    private static final int REQUEST_PERMISSION_READ_LIBRARY = 1;

    private static final int REQUEST_CAPTURE_IMAGE = 3;
    private static final int REQUEST_READ_LIBRARY = 4;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.prj_details_imv_avatar)
    CircleImageView imvAvatar;

    @Bind(R.id.prj_details_edt_prj_name)
    GitOEditText edtProjectName;

    @Bind(R.id.prj_details_edt_prj_id)
    GitOEditText edtProjectID;

    @Bind(R.id.prj_details_edt_des)
    GitOEditText edtDes;

    @Bind(R.id.pgr_avt_loading)
    ProgressBar progressBar;

    private File mUploadFile;

    @Inject
    ProjectDetailPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail);
        ButterKnife.bind(this);

        ProjectDetailComponent component = DaggerProjectDetailComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .projectDetailModule(new ProjectDetailModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(getString(R.string.new_project));
        topBar.setTextViewLeft(getString(R.string.cancel));
        topBar.setTextViewRight(getString(R.string.done));
    }

    protected void initListener() {

        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                setResult(0);
                finish();
            }

            @Override
            public void onRightClicked() {
                String name = edtProjectName.getText().toString();
                String projectID = edtProjectID.getText().toString();
                String desc = edtDes.getText().toString();
                if (name.isEmpty() || projectID.isEmpty() || desc.isEmpty()) {
                    showErrorDialog(getString(R.string.existed_blank_data));
                } else {
                    mPresenter.createProject(name, projectID, desc);
                }
            }
        });

        edtProjectName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String[] arr = s.toString().split(" ");
                StringBuilder prjID = new StringBuilder();

                for (int i = 0; i < arr.length; i++) {
                    prjID.append(arr[i]);
                    if (i != arr.length - 1)
                        prjID.append("_");
                }
                edtProjectID.setText(prjID.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        imvAvatar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == imvAvatar.getId()) {
            final DialogPhoto dialogPhoto = new DialogPhoto(this);
            dialogPhoto.setListener(new DialogPhoto.OnDialogEditPhotoClickListener() {

                @Override
                public void albumGallery() {
                    handleOpenLibrary();
                    dialogPhoto.dismiss();
                }

                @Override
                public void takePhoto() {
                    handleOpenCamera();
                    dialogPhoto.dismiss();
                }
            });
            dialogPhoto.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void createSuccess() {
        showToast(getString(R.string.success));
        setResult(AppConstants.KEY_SUCCESS);
        finish();
    }

    @Override
    public void createFailure(String error) {
        showErrorDialog(error);
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void uploadSuccess(String url) {
        ImageLoader.load(this, url, imvAvatar, progressBar);
    }

    @Override
    public void uploadError(String error) {
        showErrorDialog(error);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CAPTURE_IMAGE) {
            handleOpenCamera();
        } else if (requestCode == REQUEST_PERMISSION_READ_LIBRARY) {
            handleOpenLibrary();
        }
    }

    private void handleOpenCamera() {
        String[] s = {android.Manifest.permission.CAMERA};
        if (checkPermissions(s)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            mUploadFile = FileUtils.createImageFile(this);
            Uri uri = Uri.fromFile(mUploadFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, REQUEST_CAPTURE_IMAGE);

        } else {
            ActivityCompat.requestPermissions(this, s, REQUEST_PERMISSION_CAPTURE_IMAGE);
        }
    }

    private void handleOpenLibrary() {
        String s[] = {android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(s)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, REQUEST_READ_LIBRARY);

        } else {
            ActivityCompat.requestPermissions(this, s, REQUEST_PERMISSION_READ_LIBRARY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAPTURE_IMAGE) {
                System.out.println("FILE " + mUploadFile.toString());
                if (!NetworkUtils.isConnected(this)) {
                    showNoNetworkErrorDialog();
                } else {
                    mPresenter.uploadFile(
                            MediaType.parse(android.webkit.MimeTypeMap.getFileExtensionFromUrl(mUploadFile.getName())),
                            mUploadFile);
                }
            } else if (requestCode == REQUEST_READ_LIBRARY) {
                System.out.println("File from library " + data.getData());
                if (!NetworkUtils.isConnected(this)) {
                    showNoNetworkErrorDialog();
                } else {
                    mUploadFile = FileUtils.convertUriToFile(this, data.getData());
                    mPresenter.uploadFile(
                            MediaType.parse(getContentResolver().getType(data.getData())),
                            mUploadFile);
                }
            }
        }
    }
}
