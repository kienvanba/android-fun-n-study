package com.horical.gito.mvp.companySetting.list.fragment.quota.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.fragment.quota.injection.module.QuotaModule;
import com.horical.gito.mvp.companySetting.list.fragment.quota.view.QuotaFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = QuotaModule.class)
public interface QuotaComponent {
    void inject(QuotaFragment fragment);
}
