package com.horical.gito.mvp.salary.list.fragments.subSalary.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.mvp.salary.detail.salary.view.DetailSalaryActivity;
import com.horical.gito.mvp.salary.list.fragments.BaseListSalaryFragment;
import com.horical.gito.mvp.salary.list.fragments.subSalary.adapter.ListSalaryAdapter;
import com.horical.gito.mvp.salary.list.fragments.subSalary.injection.component.DaggerIListSalaryFragmentComponent;
import com.horical.gito.mvp.salary.list.fragments.subSalary.injection.component.IListSalaryFragmentComponent;
import com.horical.gito.mvp.salary.list.fragments.subSalary.injection.module.ListSalaryFragmentModule;
import com.horical.gito.mvp.salary.list.fragments.subSalary.presenter.ListSalaryFragmentPresenter;

import javax.inject.Inject;

import butterknife.Bind;

public class ListSalaryFragment extends BaseListSalaryFragment implements View.OnClickListener,
        IListSalaryFragmentView {

    @Bind(R.id.rcv_salary)
    RecyclerView rcvSalary;

    @Bind(R.id.tv_new)
    TextView tvNew;

    @Bind(R.id.tv_submitted)
    TextView tvSubmitted;

    @Bind(R.id.tv_approved)
    TextView tvApproved;

    @Bind(R.id.tv_rejected)
    TextView tvRejected;

    @Inject
    ListSalaryFragmentPresenter mPresenter;

    private ListSalaryAdapter mAdapter;

    public static ListSalaryFragment newInstance() {

        Bundle args = new Bundle();

        ListSalaryFragment fragment = new ListSalaryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list_salary;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IListSalaryFragmentComponent component = DaggerIListSalaryFragmentComponent.builder()
                .listSalaryFragmentModule(new ListSalaryFragmentModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        mAdapter = new ListSalaryAdapter(getActivity(), mPresenter.getSalaries(), new ListSalaryAdapter.SalaryAdapterListener() {
            @Override
            public void onSelected(int position) {
                Intent detailSalaryIntent = new Intent(getActivity(), DetailSalaryActivity.class);
                startActivity(detailSalaryIntent);
            }

            @Override
            public void onRemove(int position) {
                mPresenter.getSalaries().remove(position);
                mAdapter.notifyDataSetChanged();
            }
        });
        rcvSalary.setAdapter(mAdapter);
        rcvSalary.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }
}

