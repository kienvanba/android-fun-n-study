package com.horical.gito.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.utils.LogUtils;

import java.util.List;

public class BaseRecyclerAdapter extends RecyclerView.Adapter {

    public static final String TAG = LogUtils.makeLogTag(BaseRecyclerAdapter.class);

    public Context mContext;
    public List<IRecyclerItem> mItems;

    private OnItemClickListener mListener;

    public BaseRecyclerAdapter(List<IRecyclerItem> items, Context context) {
        this.mItems = items;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (mItems.size() > 0) {
            return mItems.get(position).getItemViewType();
        }
        return -1;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

}
