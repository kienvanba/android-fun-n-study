package com.horical.gito.mvp.companySetting.list.fragment.projectrole.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.list.fragment.projectrole.presenter.ProjectRolePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class ProjectRoleModule {
    @PerFragment
    @Provides
    ProjectRolePresenter provideProjectRolePresenter(){
        return new ProjectRolePresenter();
    }
}
