package com.horical.gito.mvp.salary.bonus.detail.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.model.User;

import java.util.List;

public interface IDetailBonusView extends IView {

    void requestGetDetailInputAddonSuccess();

    void requestGetDetailInputAddonError(RestError error);

    void requestCreateInputAddonSuccess();

    void requestCreateInputAddonError(RestError error);

    void requestUpdateInputAddonSuccess();

    void requestUpdateInputAddonError(RestError error);

    void requestGetAllUserSuccess(List<User> users);

    void requestGetAllUserError(RestError error);

    void showLoading();

    void hideLoading();
}
