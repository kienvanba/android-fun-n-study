package com.horical.gito.mvp.survey.newsurvey.presenter;

/**
 * Created by thanhle on 11/12/16.
 */

public interface INewSurveyPresenter {
    void getSubmiter();
    void getAllSurveyItem();
}
