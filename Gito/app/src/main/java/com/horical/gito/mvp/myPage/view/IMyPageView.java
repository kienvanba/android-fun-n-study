package com.horical.gito.mvp.myPage.view;

import com.horical.gito.base.IView;
import com.horical.gito.model.MyPage;

public interface IMyPageView extends IView {
    void showLoading();

    void hideLoading();

    void getMyPageFailure(String message);

    void getMyPageSuccess(MyPage myPage);
}
