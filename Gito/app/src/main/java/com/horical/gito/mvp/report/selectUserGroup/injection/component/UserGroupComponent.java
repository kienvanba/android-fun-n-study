package com.horical.gito.mvp.report.selectUserGroup.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.report.selectUserGroup.injection.module.UserGroupModule;
import com.horical.gito.mvp.report.selectUserGroup.view.UserGroupActivity;

import dagger.Component;

/**
 * Created by Tin on 15-Dec-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class , modules = UserGroupModule.class)
public interface UserGroupComponent {
    void inject(UserGroupActivity activity);
}

