package com.horical.gito.mvp.login.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.model.User;

public interface ILoginView extends IView {

    void showLoading();

    void hideLoading();

    void loginError(RestError error);

    void loginSuccess();

    void errorEmailInvalid();

    void errorEmptyInput();

}
