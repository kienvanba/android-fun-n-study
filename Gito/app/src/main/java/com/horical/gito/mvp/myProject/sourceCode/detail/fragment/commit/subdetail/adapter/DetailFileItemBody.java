package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.adapter;

/**
 * Created by Tin on 01-Dec-16.
 */

public class DetailFileItemBody {
    public int number ;
    public int image;
    public String content;

    public DetailFileItemBody(int number, int image, String content) {
        this.number = number;
        this.image = image;
        this.content = content;
    }
}
