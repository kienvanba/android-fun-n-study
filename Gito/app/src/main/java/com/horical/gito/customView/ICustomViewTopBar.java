package com.horical.gito.customView;

public interface ICustomViewTopBar {

    void setTextTitle(String title);

    void setTextViewLeft(String text);

    void setTextViewRight(String text);

    void setTextAll(String textLeft, String title, String textRight);

    void setTextUserCount(int count);

    void setImageViewLeft(int type);

    void setImageViewRight(int one);

    void setImageViewRight(int two, int one);

    void setImageViewRight(int three, int two, int one);

    void setImageViewRightVisible(int one);

    void setImageViewRightVisible(int two, int one);

    void setImageViewRightVisible(int three, int two, int one);
}
