package com.horical.gito.mvp.estate.list.presenter;

import com.horical.gito.base.IRecyclerItem;

import java.util.List;

/**
 * Created by hoangsang on 11/8/16.
 */

public interface IEstatePresenter {

    List<IRecyclerItem> getListItem();

    void getAllEstateItem(String warehouseId);

    void createEstateItem(String warehouseId, String estateName, String estateId);

    void deleteEstateItem(String warehouseId, String estateItemId);

}
