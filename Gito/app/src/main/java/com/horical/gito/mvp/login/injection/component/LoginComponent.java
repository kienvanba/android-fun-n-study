package com.horical.gito.mvp.login.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.login.injection.module.LoginModule;
import com.horical.gito.mvp.login.view.LoginActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = LoginModule.class)
public interface LoginComponent {

    void inject(LoginActivity activity);

}
