package com.horical.gito.mvp.survey.result.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.survey.result.view.IResultSurveyView;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by nhattruong251295 on 3/22/2017.
 */

public class ResultSurveyPresenter extends BasePresenter implements IResultSurveyPresenter {
    private static final String TAG = makeLogTag(ResultSurveyPresenter.class);

    public void attachView(IResultSurveyView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public IResultSurveyView getIView() {
        return (IResultSurveyView) getIView();
    }
}
