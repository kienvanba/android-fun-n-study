package com.horical.gito.mvp.myProject.sourceCode.detail.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.branch.view.BranchFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.view.BrowseFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.list.view.CommitFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.view.TagFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.view.UserFragment;

/**
 * Created by Tin on 23-Nov-16.
 */

public class CodeViewPagerAdapter extends FragmentPagerAdapter {
    public CodeViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return BrowseFragment.newInstance();
            case 1:
                return BranchFragment.newInstance();
            case 2:
                return CommitFragment.newInstance();
            case 3:
                return TagFragment.newInstance();
            case 4:
                return UserFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 5;
    }
}
