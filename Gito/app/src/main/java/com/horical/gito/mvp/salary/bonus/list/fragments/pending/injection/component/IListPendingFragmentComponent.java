package com.horical.gito.mvp.salary.bonus.list.fragments.pending.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.bonus.list.fragments.pending.injection.module.ListPendingFragmentModule;
import com.horical.gito.mvp.salary.bonus.list.fragments.pending.view.ListPendingFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ListPendingFragmentModule.class)
public interface IListPendingFragmentComponent {
    void inject(ListPendingFragment activity);
}
