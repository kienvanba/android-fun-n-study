package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/23/17.
 */

public class CalendarRole implements Serializable {
    @SerializedName("view")
    @Expose
    private Boolean view;

    public Boolean isView() {
        return view;
    }

    public void setView(Boolean view) {
        this.view = view;
    }
}
