package com.horical.gito.mvp.myProject.task.creating.presenter;

import com.horical.gito.model.Task;

public interface ITaskCreateNewPresenter {
    void createTask(Task task);
}
