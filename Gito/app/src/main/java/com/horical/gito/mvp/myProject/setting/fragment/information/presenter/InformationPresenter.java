package com.horical.gito.mvp.myProject.setting.fragment.information.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.file.UploadFileRequest;
import com.horical.gito.interactor.api.request.file.UploadFileResponse;
import com.horical.gito.interactor.api.request.project.UpdateProjectRequest;
import com.horical.gito.interactor.api.response.project.UpdateProjectResponse;
import com.horical.gito.model.Project;
import com.horical.gito.mvp.myProject.setting.fragment.information.view.IInformationFragment;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.horical.gito.utils.LogUtils.LOGE;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class InformationPresenter extends BasePresenter implements IInformationPresenter {
    private static final String TAG = makeLogTag(InformationPresenter.class);

    private Project mProject;

    public IInformationFragment getView() {
        return (IInformationFragment) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        mProject = GitOStorage.getInstance().getProject();
    }

    public void updateProject(UpdateProjectRequest project) {
        if (!isViewAttached()) return;

        getView().showLoading();
        String projectId = mProject.getId();

        getApiManager().updateProject(projectId, project, new ApiCallback<UpdateProjectResponse>() {
            @Override
            public void success(UpdateProjectResponse res) {
                if (res.project != null) mProject = res.project;
                getView().getProjectSuccess();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    public void uploadFile(MediaType type, File file) {
        if (!isViewAttached()) return;
        getView().showLoading();

        UploadFileRequest request = new UploadFileRequest(type, file);
        RequestBody status = RequestBody.create(okhttp3.MultipartBody.FORM, "0");
        getApiManager().uploadFile(request.file, status, new ApiCallback<UploadFileResponse>() {
            @Override
            public void success(UploadFileResponse res) {
                if (res.tokenFile != null) {
                    getView().uploadFileSuccess(res.tokenFile);
                }
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    @Override
    public Project getProject() {
        return mProject;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
