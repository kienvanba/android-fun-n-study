package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.injection.module.DetailModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.subdetail.view.DetailActivity;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class , modules = DetailModule.class)
public interface DetailComponent {
    void inject(DetailActivity activity);
}
