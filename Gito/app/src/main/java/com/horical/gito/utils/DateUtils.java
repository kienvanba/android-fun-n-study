package com.horical.gito.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by thanhle on 2/28/17.
 */

public class DateUtils {

    public static final String GITO_DATE = "dd/MM/yyyy";
    public static final String GITO_HOUR_MINUTE = "HH:mm";
    public static final String GITO_FULL_DATE = "dd/MM/yyyy - HH:mm";

    private static final String SERVER_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    public static String convertDateServerToLocal(String dateServerStr) {
        String dateLocal = "";
        DateFormat dateFormat = new SimpleDateFormat(SERVER_FORMAT, Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = dateFormat.parse(dateServerStr);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (date != null) {

            DateFormat formatLocal = new SimpleDateFormat(GITO_DATE, Locale.getDefault());
            formatLocal.setTimeZone(TimeZone.getDefault());

            try {
                dateLocal = formatLocal.format(date);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return dateLocal;
    }

    public static String formatDate(Date dateCreated) {
        if (dateCreated == null) {
            return "";
        }
        SimpleDateFormat dateResultFormat = new SimpleDateFormat(GITO_DATE);
        dateResultFormat.setTimeZone(TimeZone.getDefault());
        return dateResultFormat.format(dateCreated);
    }

    public static String formatFullDate(Date dateCreated) {
        if (dateCreated == null) {
            return "";
        }
        SimpleDateFormat dateResultFormat = new SimpleDateFormat(GITO_FULL_DATE);
        dateResultFormat.setTimeZone(TimeZone.getDefault());
        return dateResultFormat.format(dateCreated);
    }

    public static String formatDate(Date dateCreated, String desFormat) {
        if (dateCreated == null) {
            return "";
        }
        SimpleDateFormat dateResultFormat = new SimpleDateFormat(desFormat);
        dateResultFormat.setTimeZone(TimeZone.getDefault());
        return dateResultFormat.format(dateCreated);
    }

    public static String getTargetDateTime(String sDate) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getDefault());
        Date date = null;
        try {
            date = format.parse(sDate);
        } catch (Exception e) {

        }
        format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    public static String getShortTargetDateTime(String sDate) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getDefault());
        Date date = null;
        try {
            date = format.parse(sDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        format = new SimpleDateFormat("dd/MM/yy");
        return format.format(date);
    }

    public static String getServerDateTime(String sDate) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getDefault());
        Date date = null;
        try {
            date = format.parse(sDate);
        } catch (Exception ignored) {

        }
        format = new SimpleDateFormat("dd/MM/yy");
        return format.format(date);
    }

    public static String formatDateTime(String sDate, String srcFormat, String desFormat) {
        if (sDate == null || sDate.equals("")) {
            return null;
        }
        DateFormat format = new SimpleDateFormat(srcFormat);
        format.setTimeZone(TimeZone.getDefault());
        Date date = null;
        try {
            date = format.parse(sDate);
        } catch (Exception ignored) {

        }
        format = new SimpleDateFormat(desFormat);
        return format.format(date);
    }

    public static Date getTodayStart() {
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date;
    }

    public static Date getTodayEnd() {
        Date date = new Date();
        date.setHours(23);
        date.setMinutes(59);
        date.setSeconds(59);

        return date;
    }

    public static Date getYesterdayStart() {

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date date = calendar.getTime();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);

        return date;

    }

    public static Date getYesterdayEnd() {

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date date = calendar.getTime();
        date.setHours(23);
        date.setMinutes(59);
        date.setSeconds(59);
        return date;

    }

    public static String[] getThisWeek() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getDefault());
        Calendar calendar = Calendar.getInstance();

        String[] days = new String[7];

        int delta = calendar.get(Calendar.DAY_OF_WEEK);

        int day = calendar.get(Calendar.DAY_OF_MONTH);

        for (int i = delta; i > 0; i--) {
            days[i - 1] = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, -1);
        }
        calendar.set(Calendar.DAY_OF_MONTH, day);

        for (int j = delta; j < 7; j++) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            days[j] = dateFormat.format(calendar.getTime());
        }

        return days;

    }

    public static String getStartDate(String date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getDefault());
        Date date1 = null;
        try {
            date1 = dateFormat.parse(date);
        } catch (Exception e) {
        }

        date1.setHours(0);
        date1.setMinutes(0);
        date1.setSeconds(0);

        return dateFormat.format(date1);

    }

    public static Date getStartDate2(String date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getDefault());
        Date date1 = null;
        try {
            date1 = dateFormat.parse(date);
        } catch (Exception e) {
        }

        date1.setHours(0);
        date1.setMinutes(0);
        date1.setSeconds(0);

        return date1;

    }

    public static String getEndDate(String date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getDefault());
        Date date1 = null;
        try {
            date1 = dateFormat.parse(date);
        } catch (Exception e) {
        }

        date1.setHours(23);
        date1.setMinutes(59);
        date1.setSeconds(59);

        return dateFormat.format(date1);

    }

    public static Date getEndDate2(String date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getDefault());
        Date date1 = null;
        try {
            date1 = dateFormat.parse(date);
        } catch (Exception e) {
        }

        date1.setHours(23);
        date1.setMinutes(59);
        date1.setSeconds(59);

        return date1;

    }

    public static String[] getLastWeek() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getDefault());
        Calendar calendar = Calendar.getInstance();

        String[] days = new String[7];

        calendar.add(Calendar.DAY_OF_MONTH, -7);

        int delta = calendar.get(Calendar.DAY_OF_WEEK);

        int day = calendar.get(Calendar.DAY_OF_MONTH);

        for (int i = delta; i > 0; i--) {
            days[i - 1] = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, -1);
        }
        calendar.set(Calendar.DAY_OF_MONTH, day);

        for (int j = delta; j < 7; j++) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            days[j] = dateFormat.format(calendar.getTime());
        }

        return days;
    }

    public static String[] getThisMonth() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getDefault());
        Calendar calendar = Calendar.getInstance();

        int max = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        String[] days = new String[max];

        for (int i = 0; i < max; i++) {
            calendar.set(Calendar.DAY_OF_MONTH, i + 1);
            days[i] = dateFormat.format(calendar.getTime());
        }

        return days;
    }

    public static String[] getLastMonth() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getDefault());
        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.MONTH, -1);

        int max = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        String[] days = new String[max];

        for (int i = 0; i < max; i++) {
            calendar.set(Calendar.DAY_OF_MONTH, i + 1);
            days[i] = dateFormat.format(calendar.getTime());
        }

        return days;
    }

    public static String[] getThisYear() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getDefault());
        Calendar calendar = Calendar.getInstance();

        String days[] = new String[2];

        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, 0);

        days[0] = dateFormat.format(calendar.getTime());

        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        days[1] = dateFormat.format(calendar.getTime());

        return days;
    }

    public static String[] getLastYear() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getDefault());
        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.YEAR, -1);

        String days[] = new String[2];

        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, 0);

        days[0] = dateFormat.format(calendar.getTime());

        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        days[1] = dateFormat.format(calendar.getTime());

        return days;
    }

    public static String getUTCDateDateFormat(String dateStr, boolean adjustOneSecond) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(dateStr);
        } catch (Exception e) {

        }

        // Set UTC after get Date as Local Time Zone
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (adjustOneSecond) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.SECOND, -1);
            date = cal.getTime();
        }

        return dateFormat.format(date);

    }

    public static String getUTCDateDateFormat(String dateStr, String format, boolean adjustOneSecond) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(TimeZone.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(dateStr);
        } catch (Exception e) {

        }

        // Set UTC after get Date as Local Time Zone
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (adjustOneSecond) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.SECOND, -1);
            date = cal.getTime();
        }

        return dateFormat.format(date);

    }

    public static String getUTCDateDateFormat(String dateStr) {
        return getUTCDateDateFormat(dateStr, false);
    }

    public static Date convertStringToDate(String dateInString, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        try {
            return formatter.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date(System.currentTimeMillis());
    }

    public static int compareDateToDate(Date date1, Date date2) {
        return date1.compareTo(date2);
    }

    public static String getUTCCurrentDateInFormat(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(new Date());

    }

    public static String formatDateWithMonthAndYear(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
        dateFormat.setTimeZone(TimeZone.getDefault());
        return dateFormat.format(date);
    }

    public static List<String> getYears() {
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(System.currentTimeMillis());

        int currentYear = now.get(Calendar.YEAR);

        int startYear = currentYear + 2; ///next 1 year (see iPad)

        List<String> results = new ArrayList<>();

        while (startYear >= 1900) {
            results.add(String.valueOf(startYear));
            startYear--;
        }
        return results;
    }

    public static String getHourAndMinutes(Date date) {
        DateFormat formatLocal = new SimpleDateFormat(GITO_HOUR_MINUTE, Locale.getDefault());
        formatLocal.setTimeZone(TimeZone.getDefault());
        return formatLocal.format(date);
    }
}
