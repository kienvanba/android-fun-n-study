package com.horical.gito.mvp.logTime.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.MemberAndRole;
import com.horical.gito.model.User;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.ViewHolderMember> {

    private Context context;
    private List<User> memberList;
    private MemberAdapter.OnItemClickListener callback;

    public MemberAdapter(Context context, List<User> list) {
        this.context = context;
        this.memberList = list;
    }

    @Override
    public MemberAdapter.ViewHolderMember onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_report_member, parent, false);
        return new MemberAdapter.ViewHolderMember(view);
    }

    @Override
    public void onBindViewHolder(final MemberAdapter.ViewHolderMember holder, final int position) {
        final User user = memberList.get(position);

        holder.view.setVisibility(View.VISIBLE);
        String name = user.getDisplayName();
        holder.tvName.setText(name);
        holder.frMember.setSelected(user.isChecked());

        if (user.isAdmin()) {
            holder.tvDepartment.setText(context.getString(R.string.admin));
        } else {
            holder.tvDepartment.setText(user.getRoleCompany().getRoleName());
        }

        String urlAvatar = CommonUtils.getURL(user.getAvatar());
        if (!urlAvatar.isEmpty()) {
            ImageLoader.load(
                    context,
                    urlAvatar,
                    holder.imvPhoto,
                    holder.progressBar
            );
        } else {
            holder.tvPhoto.setText(name.substring(0, 1).toUpperCase());
        }

        if (user.isNeedApprove())
            holder.vSubmitted.setVisibility(View.VISIBLE);
        else
            holder.vSubmitted.setVisibility(GONE);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unSelectOther(position);
                callback.onClick(position);
                notifyDataSetChanged();
            }
        });
    }

    private void unSelectOther(int position) {
        for (int i = 0; i < memberList.size(); i++) {
            if (i == position) {
                memberList.get(i).setChecked(true);
            } else memberList.get(i).setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return memberList == null ? 0 : memberList.size();
    }

    public class ViewHolderMember extends RecyclerView.ViewHolder {

        @Bind(R.id.fr_member)
        FrameLayout frMember;

        @Bind(R.id.tv_photo)
        TextView tvPhoto;

        @Bind(R.id.imv_photo)
        CircleImageView imvPhoto;

        @Bind(R.id.pgr_icon_loading)
        ProgressBar progressBar;

        @Bind(R.id.tv_name_member)
        TextView tvName;

        @Bind(R.id.tv_department)
        TextView tvDepartment;

        @Bind(R.id.tv_notify_submitted)
        View vSubmitted;

        View view;

        public ViewHolderMember(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(MemberAdapter.OnItemClickListener callBack) {
        this.callback = callBack;
    }

    public interface OnItemClickListener {
        void onClick(int position);
    }

    public void setMemberList(List<User> memberList) {
        this.memberList = memberList;
    }
}
