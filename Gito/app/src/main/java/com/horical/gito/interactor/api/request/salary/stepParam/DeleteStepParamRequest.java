package com.horical.gito.interactor.api.request.salary.stepParam;


import java.util.List;

public class DeleteStepParamRequest {
    private List<String> stepParams;

    public List<String> getStepParams() {
        return stepParams;
    }

    public void setStepParams(List<String> stepParams) {
        this.stepParams = stepParams;
    }
}
