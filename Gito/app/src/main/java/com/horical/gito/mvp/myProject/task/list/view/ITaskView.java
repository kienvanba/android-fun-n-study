package com.horical.gito.mvp.myProject.task.list.view;

import com.horical.gito.base.IView;

public interface ITaskView extends IView {
    void showLoading();

    void hideLoading();

    void getAllTaskSuccess();

    void getAllTaskFailure(String error);

    void deleteTaskSuccess();

    void deleteTaskFailure(String error);
}
