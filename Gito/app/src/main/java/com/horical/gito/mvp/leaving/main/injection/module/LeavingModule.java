package com.horical.gito.mvp.leaving.main.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.leaving.main.presenter.LeavingPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Dragonoid on 12/7/2016.
 */

@Module
public class LeavingModule {
    @PerActivity
    @Provides
    LeavingPresenter provideLeavingPresenter(){
        return new LeavingPresenter();
    }
}
