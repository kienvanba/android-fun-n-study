package com.horical.gito.mvp.estate.adding.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.estate.adding.injection.module.CreateWarehouseEstateModule;
import com.horical.gito.mvp.estate.adding.view.CreateWarehouseEstateActivity;

import dagger.Component;

/**
 * Created by hoangsang on 4/13/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = CreateWarehouseEstateModule.class)
public interface CreateWarehouseEstateComponent {

    void inject(CreateWarehouseEstateActivity activity);
}
