package com.horical.gito.mvp.salary.detail.metric.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

public interface IDetailMetricView extends IView {
    void requestGetDetailMetricSuccess();

    void requestGetDetailMetricError(RestError error);

    void showLoading();

    void hideLoading();
}
