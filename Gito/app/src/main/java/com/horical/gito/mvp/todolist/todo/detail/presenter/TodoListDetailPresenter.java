package com.horical.gito.mvp.todolist.todo.detail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.TodoNote.Todo.TodoRequest;
import com.horical.gito.interactor.api.request.file.UploadFileRequest;
import com.horical.gito.interactor.api.request.file.UploadFileResponse;
import com.horical.gito.interactor.api.request.meeting.MeetingRequest;
import com.horical.gito.interactor.api.response.meeting.GetCreateMeetingResponse;
import com.horical.gito.interactor.api.response.meeting.GetUpdateMeetingResponse;
import com.horical.gito.interactor.api.response.todolist.GetAllTodoListResponse;
import com.horical.gito.interactor.api.response.todolist.GetCreateTodoListResponse;
import com.horical.gito.interactor.api.response.todolist.GetUpdateTodoListResponse;
import com.horical.gito.model.Note;
import com.horical.gito.model.TodoList;
import com.horical.gito.model.TokenFile;
import com.horical.gito.model.User;
import com.horical.gito.mvp.todolist.todo.detail.view.ITodoListDetailView;
import com.horical.gito.utils.CommonUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Luong on 16-Mar-17.
 */

public class TodoListDetailPresenter extends BasePresenter implements ITodoListDetailPresenter {

    private Date startDate;
    private List<TokenFile> attachFiles;


    public void attachView(ITodoListDetailView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public ITodoListDetailView getView() {
        return (ITodoListDetailView) getIView();
    }

    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        attachFiles = new ArrayList<>();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }


    @Override
    public void createTodo(String title) {
        if (title.trim().length() == 0) {
            getView().validateFailedTitleEmpty();
        }
        if (startDate == null) {
            getView().validateFailedTime();
        }

        TodoRequest request = new TodoRequest();
        request.title = title;
        request.startDate = startDate;

        if (attachFiles != null && attachFiles.size() > 0) {
            for (TokenFile file : attachFiles) {
                request.attachFiles.add(file.getId());
            }
        }
        getView().showLoading();
        getApiManager().createTodoList(request, new ApiCallback<GetCreateTodoListResponse>() {
            @Override
            public void success(GetCreateTodoListResponse res) {
                getView().hideLoading();
                getView().createTodoSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().createTodoFailure(error);
            }
        });

    }

    @Override
    public void updateTodo(final String todoId, String title) {
        if (title.trim().length() == 0) {
            getView().validateFailedTitleEmpty();
        }
        if (startDate == null) {
            getView().validateFailedTime();
        }

        // create request
        TodoRequest request = new TodoRequest();
        request.title = title;
        request.startDate = startDate;

        if (attachFiles != null && attachFiles.size() > 0) {
            for (TokenFile file : attachFiles) {
                request.attachFiles.add(file.getId());
            }
        }

        getView().showLoading();
        getApiManager().updateContentTodo(todoId, request, new ApiCallback<GetUpdateTodoListResponse>() {
            @Override
            public void success(GetUpdateTodoListResponse res) {
                getView().hideLoading();
                getView().updateTodoSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().updateTodoFailure(error);
            }
        });
    }

    @Override
    public void uploadFile(MediaType type, File file) {
        getView().showLoading();
        UploadFileRequest request = new UploadFileRequest(type, file);
        RequestBody status = RequestBody.create(okhttp3.MultipartBody.FORM, "0");
        getApiManager().uploadFile(request.file, status, new ApiCallback<UploadFileResponse>() {
            @Override
            public void success(UploadFileResponse res) {
                getView().hideLoading();
                attachFiles.add(res.tokenFile);

                String url = CommonUtils.getURL(res.tokenFile);
                getView().uploadSuccess(url);
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().uploadFailure(error.message);
            }
        });
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public List<TokenFile> getAttachFiles() {
        return attachFiles;
    }

    public void setAttachFiles(List<TokenFile> attachFiles) {
        this.attachFiles = attachFiles;
    }
}