package com.horical.gito.mvp.salary.mySalary.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.mySalary.list.injection.module.ListMySalaryModule;
import com.horical.gito.mvp.salary.mySalary.list.view.ListMySalaryActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ListMySalaryModule.class)
public interface IListMySalaryComponent {
    void inject(ListMySalaryActivity activity);
}
