package com.horical.gito.mvp.myProject.task.list.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.model.Task;
import com.horical.gito.mvp.dialog.dialogTask.DialogFilterTask;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.view.OverViewFragment;
import com.horical.gito.mvp.myProject.task.details.view.TaskDetailsActivity;
import com.horical.gito.mvp.myProject.task.list.adapter.TaskAdapter;
import com.horical.gito.mvp.myProject.task.list.injection.component.DaggerTaskComponent;
import com.horical.gito.mvp.myProject.task.list.injection.component.TaskComponent;
import com.horical.gito.mvp.myProject.task.list.injection.module.TaskModule;
import com.horical.gito.mvp.myProject.task.list.presenter.TaskPresenter;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class TaskActivity extends DrawerActivity implements ITaskView, View.OnClickListener {

    private static final String TAG = makeLogTag(TaskActivity.class);

    @Bind(R.id.ll_menu_task)
    LinearLayout llMenuTask;

    @Bind(R.id.ln_search_task)
    LinearLayout llSearchTask;

    @Bind(R.id.edt_search_task)
    EditText edtSearchTask;

    @Bind(R.id.img_cancel_search_task)
    ImageView imgCancelSearchTask;

    @Bind(R.id.tv_title_task)
    TextView tvTitleTask;

    @Bind(R.id.img_search_task)
    ImageView imgSearchTask;

    @Bind(R.id.img_filter_task)
    ImageView imgFilterTask;

    @Bind(R.id.img_add_task)
    ImageView imgAddTask;

    @Bind(R.id.refresh_task)
    SwipeRefreshLayout refreshTask;

    @Bind(R.id.rcv_task)
    RecyclerView rcvTask;

    private TaskAdapter mAdapter;

    @Inject
    TaskPresenter mPresenter;

    protected int getLayoutId() {
        return R.layout.activity_task;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_TASK;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        TaskComponent component = DaggerTaskComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .taskModule(new TaskModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        mAdapter = new TaskAdapter(this, mPresenter.getListSearchTask(edtSearchTask.getText().toString()));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcvTask.setLayoutManager(layoutManager);
        rcvTask.setHasFixedSize(false);
        rcvTask.setAdapter(mAdapter);

        showLoading();
        mPresenter.getAllTask();
    }

    private void updateAdapterFromAllCaseStudy() {
        List<Task> tasks = mPresenter.getListSearchTask(edtSearchTask.getText().toString());
        mAdapter.setList(tasks);
        mAdapter.notifyDataSetChanged();
    }

    protected void initListener() {
        llMenuTask.setOnClickListener(this);
        imgSearchTask.setOnClickListener(this);
        imgFilterTask.setOnClickListener(this);
        imgAddTask.setOnClickListener(this);

        llSearchTask.setOnClickListener(this);
        imgCancelSearchTask.setOnClickListener(this);

        edtSearchTask.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateAdapterFromAllCaseStudy();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        refreshTask.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                mPresenter.getAllTask();
            }
        });

        mAdapter.setOnItemClickListener(new TaskAdapter.OnItemClickListener() {
            @Override
            public void onClickListener(Task task) {
                Intent in = new Intent(TaskActivity.this, TaskDetailsActivity.class);
                in.putExtra("task", (Serializable)task);
                in.putExtra("type", TaskDetailsActivity.OPEN_FOR_UPDATING);
                startActivity(in);
            }

            @Override
            public void onDeleteListener(String taskID) {
                String projectID = GitOStorage.getInstance().getProject().getId();
                mPresenter.deleteTask(taskID);
                showLoading();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_menu_task:
                openDrawer();
                break;
            case R.id.img_cancel_search_task:
                tvTitleTask.setVisibility(View.VISIBLE);
                imgAddTask.setVisibility(View.VISIBLE);
                imgSearchTask.setVisibility(View.VISIBLE);
                imgFilterTask.setVisibility(View.VISIBLE);

                edtSearchTask.setText("");
                llSearchTask.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_from_left));
                llSearchTask.setVisibility(View.GONE);
                break;
            case R.id.img_search_task:
                tvTitleTask.setVisibility(View.GONE);
                imgAddTask.setVisibility(View.GONE);
                imgSearchTask.setVisibility(View.GONE);
                imgFilterTask.setVisibility(View.GONE);

                llSearchTask.setVisibility(View.VISIBLE);
                llSearchTask.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_from_right));
                break;
            case R.id.img_filter_task:
                DialogFilterTask dialogFilterTask = new DialogFilterTask(TaskActivity.this);
                dialogFilterTask.show();
                break;
            case R.id.img_add_task:
/*
                Intent intent = new Intent(TaskActivity.this, TaskCreateNewActivity.class);
                startActivity(intent);
                break;
*/
                Intent intent = new Intent(TaskActivity.this, TaskDetailsActivity.class);
                intent.putExtra("type",TaskDetailsActivity.OPEN_FOR_CREATING);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllTaskSuccess() {
        hideLoading();
        refreshTask.setRefreshing(false);
        showToast("Success");
        updateAdapterFromAllCaseStudy();
    }

    @Override
    public void getAllTaskFailure(String error) {
        hideLoading();
        showToast("Failure: "+error);
    }

    @Override
    public void deleteTaskSuccess() {
        hideLoading();
        showToast("has deleted");
        mPresenter.getAllTask();
    }

    @Override
    public void deleteTaskFailure(String error) {
        hideLoading();
        showToast("deleting error - "+error);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}
