package com.horical.gito.mvp.leaving.confirm.adapter;

import android.graphics.Bitmap;
import android.media.Image;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.utils.HDate;

import java.util.Date;

/**
 * Created by Dragonoid on 1/18/2017.
 */

public class LeavingConfirmDetailItem implements IRecyclerItem {
    @Override
    public int getItemViewType() {
        return 0;
    }
    String mName;
    String mPosition;
    String mReason;
    HDate mDate;
    String mStatus;
    Bitmap mAvatar;
    HDate mDateFrom;
    HDate mDateTo;
    public LeavingConfirmDetailItem(HDate date, String name, String position, Bitmap avatar, String reason, HDate fromDate, HDate toDate){
        mName = name;
        mPosition = position;
        mReason = reason;
        mDate = date;
        mStatus = "Not Approved";
        mAvatar = avatar;
        mDateFrom = fromDate;
        mDateTo = toDate;
    }

    public HDate getDate(){ return mDate;}
}
