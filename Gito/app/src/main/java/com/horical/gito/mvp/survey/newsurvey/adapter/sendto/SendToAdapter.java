package com.horical.gito.mvp.survey.newsurvey.adapter.sendto;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 11/12/16.
 */

public class SendToAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<String> mList;

    public SendToAdapter(Context context, List<String> list) {
            mContext = context;
            mList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.item_add_survey_send_to, parent, false);
            viewHolder = new SendToViewHolder(view);
            return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            String name = mList.get(position);
        SendToViewHolder viewHolder = (SendToViewHolder) holder;
            viewHolder.mSendTo.setText(name);
    }

    @Override
    public int getItemCount() {
            return mList != null ? mList.size() : 0;
    }

    public class SendToViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.add_survey_detail_sendto)
        TextView mSendTo;

        public SendToViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
