package com.horical.gito.mvp.companySetting.list.fragment.projectrole.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.projectrole.adapter.ProjectRoleAdapter;
import com.horical.gito.mvp.companySetting.list.fragment.projectrole.injection.component.ProjectRoleComponent;
import com.horical.gito.mvp.companySetting.list.fragment.projectrole.injection.module.ProjectRoleModule;
import com.horical.gito.mvp.companySetting.list.fragment.projectrole.presenter.ProjectRolePresenter;
import com.horical.gito.mvp.companySetting.list.fragment.projectrole.injection.component.DaggerProjectRoleComponent;
import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class ProjectRoleFragment extends BaseCompanyFragment implements IProjectRoleView, View.OnClickListener {
    public static final String TAG = makeLogTag(ProjectRoleFragment.class);

    @Bind(R.id.refresh)
    SwipeRefreshLayout refresh;

    @Bind(R.id.rcv_project_role)
    RecyclerView rcvProjectRole;

    private ProjectRoleAdapter adapterProjectRole;

    @Inject
    ProjectRolePresenter mPresenter;


    public static ProjectRoleFragment newInstance() {
        Bundle args = new Bundle();
        ProjectRoleFragment fragment = new ProjectRoleFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_company_setting_project_role;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ProjectRoleComponent component = DaggerProjectRoleComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .projectRoleModule(new ProjectRoleModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        adapterProjectRole = new ProjectRoleAdapter(getContext(), mPresenter.getListProjectRole());
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvProjectRole.setLayoutManager(llm);
        rcvProjectRole.setAdapter(adapterProjectRole);

        mPresenter.getAllProjectRole();
    }

    @Override
    protected void initListener() {
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getAllProjectRole();
            }
        });
        adapterProjectRole.setOnItemClickListener(new ProjectRoleAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(int position) {

            }

            @Override
            public void onDelete(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.are_you_sure_for_delete_this,"role"));
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mPresenter.deleteProjectRole(mPresenter.getListProjectRole().get(position).getId(), position);
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public void onClick(View view) {

    }


    @Override
    public void getAllProjectRoleSuccess() {
        refresh.setRefreshing(false);
        Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
        adapterProjectRole.notifyDataSetChanged();
    }

    @Override
    public void getAllProjectRoleFailure(String err) {
        refresh.setRefreshing(false);
        Toast.makeText(getContext(), "Failure", Toast.LENGTH_SHORT).show();
        adapterProjectRole.notifyDataSetChanged();
    }

    @Override
    public void deleteProjectRoleSuccess(int position) {
        mPresenter.getListProjectRole().remove(position);
        adapterProjectRole.notifyDataSetChanged();
        Toast.makeText(getContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void deleteProjectRoleFailure(String err) {
        adapterProjectRole.notifyDataSetChanged();
        Toast.makeText(getContext(), "FAILURE", Toast.LENGTH_SHORT).show();
    }
}
