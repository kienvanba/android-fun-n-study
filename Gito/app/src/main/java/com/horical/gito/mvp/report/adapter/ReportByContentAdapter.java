package com.horical.gito.mvp.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.report.objectDto.ReportByContentDto;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ReportByContentAdapter extends SectioningAdapter {

    private Context mContext;
    private List<ReportByContentDto> list;

    public ReportByContentAdapter(Context mContext, List<ReportByContentDto> mSections) {
        this.mContext = mContext;
        this.list = mSections;
    }

    public class HeaderViewHolder extends SectioningAdapter.HeaderViewHolder {
        @Bind(R.id.tv_header)
        TextView tvHeader;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ItemNormalViewHolder extends SectioningAdapter.ItemViewHolder {

        @Bind(R.id.tv_date)
        TextView tvDate;

        @Bind(R.id.tv_data)
        TextView tvData;

        public ItemNormalViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public SectioningAdapter.HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        View viewHeader = LayoutInflater.from(mContext).inflate(R.layout.layout_item_header_report, parent, false);
        return new HeaderViewHolder(viewHeader);
    }

    @Override
    public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        View viewNormal = LayoutInflater.from(mContext).inflate(R.layout.layout_item_normal_report, parent, false);
        return new ItemNormalViewHolder(viewNormal);
    }

    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex, int headerType) {
        HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
        holder.tvHeader.setText(list.get(sectionIndex).getTypeStatus());
    }

    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, int sectionIndex, int itemIndex, int itemType) {
        ItemNormalViewHolder holder = (ItemNormalViewHolder) viewHolder;
        holder.tvDate.setText(list.get(sectionIndex).getList().get(itemIndex).showDate());
        holder.tvData.setText(list.get(sectionIndex).getList().get(itemIndex).showData());

    }

    @Override
    public int getNumberOfSections() {
        return list.size();
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        return list.get(sectionIndex).getList().size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public boolean doesSectionHaveFooter(int sectionIndex) {
        return false;
    }
}
