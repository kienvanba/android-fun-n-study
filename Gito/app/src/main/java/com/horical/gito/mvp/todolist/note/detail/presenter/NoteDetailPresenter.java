package com.horical.gito.mvp.todolist.note.detail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.TodoNote.Note.NoteRequest;
import com.horical.gito.interactor.api.request.TodoNote.Todo.TodoRequest;
import com.horical.gito.interactor.api.request.file.UploadFileRequest;
import com.horical.gito.interactor.api.request.file.UploadFileResponse;
import com.horical.gito.interactor.api.response.note.GetAllNoteResponse;
import com.horical.gito.interactor.api.response.note.GetCreateNoteResponse;
import com.horical.gito.interactor.api.response.note.GetUpdateNoteResponse;
import com.horical.gito.interactor.api.response.todolist.GetCreateTodoListResponse;
import com.horical.gito.interactor.api.response.todolist.GetUpdateTodoListResponse;
import com.horical.gito.model.Note;
import com.horical.gito.model.TokenFile;
import com.horical.gito.mvp.todolist.note.detail.view.INoteDetailView;
import com.horical.gito.mvp.todolist.todo.detail.view.ITodoListDetailView;
import com.horical.gito.utils.CommonUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Luong on 28-Mar-17.
 */

public class NoteDetailPresenter extends BasePresenter implements INoteDetailPresenter {

    private List<TokenFile> attachFiles;


    public void attachView(INoteDetailView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public INoteDetailView getView() {
        return (INoteDetailView) getIView();
    }

    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        attachFiles = new ArrayList<>();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }


    @Override
    public void createNote(String title, String content) {
        if (title.trim().length() == 0) {
            getView().validateFailedTitleEmpty();
        }
        if (content.trim().length() == 0) {
            getView().validateFailedContentEmpty();
        }

        NoteRequest request = new NoteRequest();
        request.title = title;
        request.desc = content;

        if (attachFiles != null && attachFiles.size() > 0) {
            for (TokenFile file : attachFiles) {
                request.attachFiles.add(file.getId());
            }
        }
        getView().showLoading();
        getApiManager().createNote(request, new ApiCallback<GetCreateNoteResponse>() {
            @Override
            public void success(GetCreateNoteResponse res) {
                getView().hideLoading();
                getView().createNoteSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().createNoteFailure(error);
            }
        });

    }

    @Override
    public void updateNote(final String noteId, String title, String content) {
        if (title.trim().length() == 0) {
            getView().validateFailedTitleEmpty();
        }

        if (content.trim().length() == 0) {
            getView().validateFailedContentEmpty();
        }
        // create request
        NoteRequest request = new NoteRequest();
        request.title = title;
        request.desc = content;

        if (attachFiles != null && attachFiles.size() > 0) {
            for (TokenFile file : attachFiles) {
                request.attachFiles.add(file.getId());
            }
        }

        getView().showLoading();
        getApiManager().updateContentNote(noteId, request, new ApiCallback<GetUpdateNoteResponse>() {
            @Override
            public void success(GetUpdateNoteResponse res) {
                getView().hideLoading();
                getView().updateNoteSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().updateNoteFailure(error);
            }
        });
    }

    @Override
    public void uploadFile(MediaType type, File file) {
        getView().showLoading();
        UploadFileRequest request = new UploadFileRequest(type, file);
        RequestBody status = RequestBody.create(okhttp3.MultipartBody.FORM, "0");
        getApiManager().uploadFile(request.file, status, new ApiCallback<UploadFileResponse>() {
            @Override
            public void success(UploadFileResponse res) {
                getView().hideLoading();
                attachFiles.add(res.tokenFile);

                String url = CommonUtils.getURL(res.tokenFile);
                getView().uploadSuccess(url);
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().uploadFailure(error.message);
            }
        });
    }

    public List<TokenFile> getAttachFiles() {
        return attachFiles;
    }

    public void setAttachFiles(List<TokenFile> attachFiles) {
        this.attachFiles = attachFiles;
    }
}
