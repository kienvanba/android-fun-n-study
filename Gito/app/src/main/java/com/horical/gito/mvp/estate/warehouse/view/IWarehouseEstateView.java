package com.horical.gito.mvp.estate.warehouse.view;

import com.horical.gito.base.IView;

/**
 * Created by hoangsang on 4/12/17.
 */

public interface IWarehouseEstateView extends IView {
    int KEY_REQUEST_CREATE_WAREHOUSE = 1;

    void showLoading();

    void hideLoading();

    boolean isRefreshing();

    void loadAllWarehourseEstateSuccess();

    void loadAllWarehourseEstateFailed();

    void deleteWarehouseEstateSuccess();

    void deleteWarehouseEstateFailed();
}
