package com.horical.gito.mvp.todolist.note.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.todolist.note.list.injection.module.NoteModule;
import com.horical.gito.mvp.todolist.note.list.view.NoteActivity;

import dagger.Component;

/**
 * Created by Luong on 21-Mar-17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = NoteModule.class)
public interface NoteComponent {
    void inject(NoteActivity noteActivity);
}