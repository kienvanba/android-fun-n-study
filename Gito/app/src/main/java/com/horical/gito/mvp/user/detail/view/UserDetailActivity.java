package com.horical.gito.mvp.user.detail.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.TokenFile;
import com.horical.gito.model.User;
import com.horical.gito.mvp.dialog.dialogChangePassWord.DialogChangePassword;
import com.horical.gito.mvp.dialog.dialogInputText.InputTextDialog;
import com.horical.gito.mvp.user.detail.injection.component.DaggerUserDetailComponent;
import com.horical.gito.mvp.user.detail.injection.component.UserDetailComponent;
import com.horical.gito.mvp.user.detail.injection.module.UserDetailModule;
import com.horical.gito.mvp.user.detail.presenter.UserDetailPresenter;
import com.horical.gito.utils.CommonUtils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by thanhle on 4/3/17.
 */

public class UserDetailActivity extends BaseActivity implements IUserDetailView, View.OnClickListener {
    public static final String ARG_MEMBER = "Member";
    public static final int NOT_ADMIN = 0;
    public static final int ADMIN = 1;
    public static final int NORMAL = 2;

    public static final int UNLOCK = 0;
    public static final int LOCK = 1;

    @Bind(R.id.ll_back)
    LinearLayout llBack;

    @Bind(R.id.tv_title)
    TextView tvTitle;

    @Bind(R.id.img_avatar)
    CircleImageView imvAvatar;

    @Bind(R.id.pgbar_avatar)
    ProgressBar pgbAvatar;

    @Bind(R.id.tv_user_name)
    TextView tvName;

    @Bind(R.id.img_edit_name)
    ImageView imgEditName;

    @Bind(R.id.tv_role_company)
    TextView tvRoleCompany;

    @Bind(R.id.ll_edit_role_company)
    LinearLayout llEditRoleCompany;

    @Bind(R.id.tv_position)
    TextView tvPosition;

    @Bind(R.id.ll_edit_position)
    LinearLayout llEditPosition;

    @Bind(R.id.tv_email)
    TextView tvEmail;

    @Bind(R.id.ll_edit_email)
    LinearLayout llEditEmail;

    @Bind(R.id.tv_password)
    TextView tvPassword;

    @Bind(R.id.ll_edit_pass)
    LinearLayout llEditPass;

    @Bind(R.id.tv_userid)
    TextView tvUserId;

    @Bind(R.id.ll_edit_userid)
    LinearLayout llEditUserId;

    @Bind(R.id.tv_phone)
    TextView tvPhone;

    @Bind(R.id.ll_edit_phone)
    LinearLayout llEditPhone;

    @Bind(R.id.tv_level)
    TextView tvLevel;

    @Bind(R.id.ll_edit_level)
    LinearLayout llEditLevel;

    @Bind(R.id.tv_department)
    TextView tvDepartment;

    @Bind(R.id.ll_edit_department)
    LinearLayout llEditDepartment;

    @Bind(R.id.tv_address)
    TextView tvAddress;

    @Bind(R.id.ll_edit_address)
    LinearLayout llEditAddress;

    @Bind(R.id.tv_delete)
    TextView tvDelete;

    @Bind(R.id.tv_lock)
    TextView tvLock;

    @Bind(R.id.tv_is_admin)
    TextView tvIsAdmin;

    private User user;
    private boolean isLock = false;
    private InputTextDialog dialog;
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;
    @Inject
    UserDetailPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);
        ButterKnife.bind(this);

        UserDetailComponent component = DaggerUserDetailComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .userDetailModule(new UserDetailModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        user = (User) getIntent().getSerializableExtra(ARG_MEMBER);

        initData();
        initListener();
    }

    protected void initData() {
        tvTitle.setText(user.getDisplayName());
        tvIsAdmin.setSelected(true);
        TokenFile avatar = user.getAvatar();
        if (avatar != null) {
            String url = CommonUtils.getURL(avatar);
            ImageLoader.load(UserDetailActivity.this, url, imvAvatar, pgbAvatar);
        } else {
            pgbAvatar.setVisibility(View.GONE);
        }

        tvName.setText(user.getDisplayName());
        tvRoleCompany.setText(user.getRoleCompany().getRoleName());
        tvPosition.setText(user.getLevel().getName());
        tvEmail.setText(user.getEmail());
        tvPassword.setText("Aa123456?");
        tvUserId.setText(user.getUserId());
        tvPhone.setText(user.getPhone());
        tvLevel.setText(user.getLevel().getName());
        tvDepartment.setText(user.getDepartment().getName());
        tvAddress.setText(user.getAddress());
        if (user.isAdmin()) {
            tvIsAdmin.setText(R.string.grant_to_admin);
        } else {
            tvIsAdmin.setText(R.string.revoke_admin);
        }
    }

    protected void initListener() {
        llBack.setOnClickListener(this);
        imgEditName.setOnClickListener(this);
        llEditRoleCompany.setOnClickListener(this);
        llEditPosition.setOnClickListener(this);
        llEditEmail.setOnClickListener(this);
        llEditPass.setOnClickListener(this);
        llEditUserId.setOnClickListener(this);
        llEditPhone.setOnClickListener(this);
        llEditLevel.setOnClickListener(this);
        llEditDepartment.setOnClickListener(this);
        llEditAddress.setOnClickListener(this);

        tvDelete.setOnClickListener(this);
        tvLock.setOnClickListener(this);
        tvIsAdmin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
//        list chon rolecompany
//        list level position
//        pass:
//        current password
//        new password
//        confirm pass
//        list department
        switch (v.getId()) {
            case R.id.ll_back:
                onBackPressed();
                break;
            case R.id.img_edit_name:
                dialog = new InputTextDialog(UserDetailActivity.this, InputTextDialog.DEFAULT_TEXT, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        tvName.setText(text);
                        user.setDisplayName(text);
                        mPresenter.updateUser(user, NORMAL, NORMAL, 0, text, null, null, null
                        , null, null, null, null);
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_role_company:
                dialog = new InputTextDialog(UserDetailActivity.this, InputTextDialog.DEFAULT_TEXT, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        tvRoleCompany.setText(text);
                        user.getRoleCompany().setRoleName(text);
                        //mPresenter.updateUser(user, NORMAL, NORMAL, null, null, null, null, null
                        //        , null, null, null, null);
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_position:
                dialog = new InputTextDialog(UserDetailActivity.this, InputTextDialog.DEFAULT_TEXT, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        tvPosition.setText(text);
                        user.getLevel().setName(text);
                        //mPresenter.updateUser(user, NORMAL, NORMAL, null, null, null, null, null
                        //        , null, null, null, null);
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_email:
                //chua ktr email
                dialog = new InputTextDialog(UserDetailActivity.this, InputTextDialog.EMAIL_TEXT, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        tvEmail.setText(text);
                        user.setEmail(text);
                        //mPresenter.updateUser(user, NORMAL, NORMAL, null, null, null, null, null
                        //        , null, null, null, null);
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_pass:
//                if(la this user){
//
//              } else(la admin){
//
//              }
                DialogChangePassword dialogChangePassword = new DialogChangePassword(UserDetailActivity.this);
                dialogChangePassword.setOnClickItemListener(new DialogChangePassword.OnClickItemListener() {
                    @Override
                    public void onClickDone(String currentPassword, String newPassword, String confirmPassword) {

                    }
                });
                dialogChangePassword.show();
                break;
            case R.id.ll_edit_userid:
                dialog = new InputTextDialog(UserDetailActivity.this, InputTextDialog.DEFAULT_TEXT, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        tvUserId.setText(text);
                        user.setUserId(text);
                        mPresenter.updateUser(user, NORMAL, NORMAL, 0, null, null, null, null
                                ,text, null, null, null);
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_phone:
                dialog = new InputTextDialog(UserDetailActivity.this, InputTextDialog.PHONE_TEXT, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        tvPhone.setText(text);
                        user.setPhone(text);
                        mPresenter.updateUser(user, NORMAL, NORMAL, 0, null, null, null, null
                                , null, text, null, null);
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_level:
                dialog = new InputTextDialog(UserDetailActivity.this, InputTextDialog.DEFAULT_TEXT, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        tvLevel.setText(text);
                        user.getLevel().setName(text);
                        //mPresenter.updateUser(user, NORMAL, NORMAL, null, null, null, null, null
                        //        , null, null, null, null);
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_department:
                dialog = new InputTextDialog(UserDetailActivity.this, InputTextDialog.DEFAULT_TEXT, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        tvDepartment.setText(text);
                        user.getDepartment().setName(text);
                        //mPresenter.updateUser(user, NORMAL, NORMAL, null, null, null, null, null
                        //        , null, null, null, null);
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_address:
                dialog = new InputTextDialog(UserDetailActivity.this, InputTextDialog.DEFAULT_TEXT, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        tvAddress.setText(text);
                        user.setAddress(text);
                        mPresenter.updateUser(user, NORMAL, NORMAL, 0, null, null, null, null
                                , null, null, null, text);
                    }
                });
                dialog.show();
                break;
            case R.id.tv_delete:
                builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure want to delete this user?");
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mPresenter.deleteMember(user.get_id());
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                alertDialog.cancel();
                            }
                        });

                alertDialog = builder.create();
                alertDialog.show();
                break;
            case R.id.tv_lock:
                isLock = !isLock;
                builder = new AlertDialog.Builder(this);
                if (isLock) {
                    builder.setMessage("Are you sure want to lock this user?");
                } else {
                    builder.setMessage("Are you sure want to unlock this user?");
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (isLock) {
                                    mPresenter.updateUser(user, NORMAL, LOCK,1, null, null, null, null
                                            , null, null, null, null);
                                } else {
                                    mPresenter.updateUser(user, NORMAL, UNLOCK,1, null, null, null, null
                                            , null, null, null, null);
                                }
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                alertDialog.cancel();
                            }
                        });

                alertDialog = builder.create();
                alertDialog.show();
                break;
            case R.id.tv_is_admin:
                builder = new AlertDialog.Builder(this);
                if (tvIsAdmin.getText().toString().equalsIgnoreCase("Revoke Admin")) {
                    builder.setMessage("Would you like to Grant to Admin this user?");
                } else {
                    builder.setMessage("Would you like to Revoke Admin this user?");
                }
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (tvIsAdmin.getText().toString().equalsIgnoreCase("Revoke Admin")) {
                                    mPresenter.updateUser(user, ADMIN, NORMAL, 1, null, null, null, null
                                            , null, null, null, null);
                                } else {
                                    mPresenter.updateUser(user, NOT_ADMIN, NORMAL, 1, null, null, null, null
                                            , null, null, null, null);
                                }
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                alertDialog.cancel();
                            }
                        });

                alertDialog = builder.create();
                alertDialog.show();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void updateMemberSuccess(int isAdmin, int isLock) {
        if (isAdmin == ADMIN) {
            tvIsAdmin.setText(R.string.grant_to_admin);
            showSuccessDialog("Grant to admin success!");
        } else if (isAdmin == NOT_ADMIN){
            tvIsAdmin.setText(R.string.revoke_admin);
            showSuccessDialog("Revoke Admin success!");
        }
        if(isLock == LOCK){
            tvLock.setText(R.string.unlock);
            tvLock.setSelected(true);
            showSuccessDialog("Lock user succsess!");
        } else if(isLock == UNLOCK){
            tvLock.setText(R.string.lock);
            tvLock.setSelected(false);
            showSuccessDialog("UnLock user succsess!");
        }
    }

    @Override
    public void updateMemberFailure(String err) {
        showToast(err);
    }

    @Override
    public void deleteMemberSuccess() {
        showToast("SUccess");
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void deleteMemberFailure(String err) {
        showToast("Failure");
    }

    @Override
    public void changePasswordSuccess() {
        showToast("Success");
    }

    @Override
    public void changePasswordFailure(String err) {
        showToast(err);
    }
}
