package com.horical.gito.mvp.leaving.confirm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 1/18/2017.
 */

public class LeavingConfirmDetailAdapter extends BaseRecyclerAdapter {

    private ApproveRejectFromChildView mCallback;

    public LeavingConfirmDetailAdapter(List<com.horical.gito.base.IRecyclerItem> items, Context context, ApproveRejectFromChildView callback) {
        super(items, context);
        mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leaving_confirm_detail, parent, false);
        return new LeavingConfirmDetailHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        LeavingConfirmDetailHolder detailHolder = (LeavingConfirmDetailHolder) holder;
        final LeavingConfirmDetailItem detailItem = (LeavingConfirmDetailItem) mItems.get(position);
        detailHolder.mName.setText(detailItem.mName);
        detailHolder.mPosition.setText(detailItem.mPosition);
        detailHolder.mReason.setText(detailItem.mReason);
        detailHolder.mApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailItem.mStatus = "Approved";
                if (mItems.size() > 1) {
                    mItems.remove(position);
                    notifyDataSetChanged();
                } else {
                    mCallback.isEmptyChildList();
                }
            }
        });
        detailHolder.mReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailItem.mStatus = "Rejected";
                if (mItems.size() > 1) {
                    mItems.remove(position);
                    notifyDataSetChanged();
                } else {
                    mCallback.isEmptyChildList();
                }

            }
        });
        detailHolder.mAvatar.setImageBitmap(detailItem.mAvatar);
        detailHolder.mDateFrom.setText(detailItem.mDateFrom.toStringFull());
        detailHolder.mDateTo.setText(detailItem.mDateTo.toStringFull());
    }

    public interface ApproveRejectFromChildView {
        void isEmptyChildList();
    }
    public class LeavingConfirmDetailHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.leaving_img_confirm_avatar)
        ImageView mAvatar;
        @Bind(R.id.leaving_tv_confirm_name)
        TextView mName;
        @Bind(R.id.leaving_tv_confirm_position)
        TextView mPosition;
        @Bind(R.id.leaving_tv_confirm_reason)
        TextView mReason;
        @Bind(R.id.leaving_btn_confirm_approve)
        Button mApprove;
        @Bind(R.id.leaving_btn_confirm_reject)
        Button mReject;
        @Bind(R.id.leaving_tv_confirm_date_to)
        GitOTextView mDateTo;
        @Bind(R.id.leaving_tv_confirm_date_from)
        GitOTextView mDateFrom;

        public LeavingConfirmDetailHolder (View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }
}
