package com.horical.gito.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

/**
 * Created by thanhle on 2/28/17.
 */

public class ConvertUtils {
    public static float convertDpToPixel(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }

    public static int convertDpToInt(Context context, float dp){
        float scale = context.getResources().getDisplayMetrics().density;
        return Math.round(dp * scale / 0.5f);
    }
}
