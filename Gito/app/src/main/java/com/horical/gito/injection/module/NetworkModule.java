package com.horical.gito.injection.module;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.horical.gito.BuildConfig;
import com.horical.gito.interactor.api.network.deserialize.CompanyDeserialize;
import com.horical.gito.interactor.api.network.deserialize.DateSerializeAndDeserialize;
import com.horical.gito.interactor.api.network.deserialize.DepartmentDeserialize;
import com.horical.gito.interactor.api.network.deserialize.LevelDeserialize;
import com.horical.gito.interactor.api.network.deserialize.MemberDeserialize;
import com.horical.gito.interactor.api.network.deserialize.ProjectDeserialize;
import com.horical.gito.interactor.api.network.deserialize.RoleCompanyDeserialize;
import com.horical.gito.interactor.api.network.deserialize.TaskDeserialize;
import com.horical.gito.interactor.api.network.deserialize.TokenFileDeserialize;
import com.horical.gito.interactor.api.network.deserialize.UserDeserialize;
import com.horical.gito.interactor.api.network.interceptor.HeadersInterceptor;
import com.horical.gito.model.Company;
import com.horical.gito.model.Department;
import com.horical.gito.model.Project;
import com.horical.gito.model.Task;
import com.horical.gito.model.User;
import com.horical.gito.model.Level;
import com.horical.gito.model.Member;
import com.horical.gito.model.RoleCompany;
import com.horical.gito.model.TokenFile;

import java.util.Date;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module
public class NetworkModule {

    @Provides
    @Named("api")
    @Singleton
    String provideApiUrl() {
        return BuildConfig.API_URL;
    }

    @Provides
    @Named("file")
    @Singleton
    String provideFileUrl() {
        return BuildConfig.FILE_URL;
    }

    @Provides
    @Named("doc")
    @Singleton
    String provideDocUrl() {
        return BuildConfig.DOC_URL;
    }

    @Provides
    @Singleton
    Cache provideOkHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        gsonBuilder.registerTypeAdapter(Company.class, new CompanyDeserialize());
        gsonBuilder.registerTypeAdapter(Department.class, new DepartmentDeserialize());
        gsonBuilder.registerTypeAdapter(Member.class, new MemberDeserialize());
        gsonBuilder.registerTypeAdapter(TokenFile.class, new TokenFileDeserialize());
        gsonBuilder.registerTypeHierarchyAdapter(Date.class, new DateSerializeAndDeserialize());
        gsonBuilder.registerTypeAdapter(RoleCompany.class, new RoleCompanyDeserialize());
        gsonBuilder.registerTypeAdapter(Level.class, new LevelDeserialize());
        gsonBuilder.registerTypeAdapter(User.class, new UserDeserialize());
        gsonBuilder.registerTypeAdapter(Task.class, new TaskDeserialize());
        gsonBuilder.registerTypeAdapter(Project.class, new ProjectDeserialize());
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    HeadersInterceptor provideHeadersInterceptor() {
        return new HeadersInterceptor();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HeadersInterceptor headersInterceptor, Cache cache) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        builder.addInterceptor(logging);
        builder.addInterceptor(headersInterceptor);
        builder.cache(cache);
        return builder.build();
    }

    @Provides
    @Named("api")
    @Singleton
    Retrofit provideRetrofitApi(@Named("api") String baseUrl, Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Named("file")
    @Singleton
    Retrofit provideRetrofitFile(@Named("file") String baseUrl, Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Named("doc")
    @Singleton
    Retrofit provideRetrofitDoc(@Named("doc") String baseUrl, Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
    }

}