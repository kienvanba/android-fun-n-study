package com.horical.gito.widget.chartview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.horical.gito.R;

/**
 * Created by Tin on 28-Dec-16.
 */

public class SorceLineChartView extends ChartView {


    /**
     * Date - mTextSize
     */
    protected float mTextSize;


    protected float mHeightBarTop;

    protected float mHeightBarBot;

    protected float mWidthBar;

    /**
     * Sorce
     */
    protected String Sorce;


    @Override
    public void setOnLongClickListener(OnLongClickListener l) {
        super.setOnLongClickListener(l);
    }


    public SorceLineChartView(Context context) {
        super(context);
    }

    public SorceLineChartView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.SorceLineChartView, 0, 0);

        try {
            mTextSize = typedArray.getDimension(R.styleable.SorceLineChartView_textSizeSorce, 0);
            Sorce = typedArray.getString(R.styleable.SorceLineChartView_text);
        } finally {
            typedArray.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mHeightBarTop = mTextSize + convertDp(24);
        mHeightBarBot = getHeight() - convertDp(14) - mTextSize;
        mWidthBar = getWidth() - convertDp(15);

        /** Draw background divider **/
        onDrawBackground(canvas);

        /** Draw percent done **/
        onDrawForegroundPercent(canvas);

        /** Draw foregroud points **/
        onDrawForegroudPoints(canvas);

        /** Draw Points**/
        onDrawPoints(canvas);

        /** Draw from Bitmap **/
        onDrawCheck(canvas);

    }


    private void onDrawBackground(Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.grey));
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.FILL);

        canvas.drawRect(
                convertDp(10),
                mHeightBarTop,
                mWidthBar - convertDp(20),
                mHeightBarBot,
                mPaint);
    }

    private void onDrawForegroundPercent(Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.app_color));
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.FILL);

        canvas.drawRect(
                convertDp(10),
                mHeightBarTop,
                mPercent * mWidthBar / 100 - convertDp(20),
                mHeightBarBot,
                mPaint
        );
    }

    private void onDrawCheck(Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.blue));
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setTextSize(mTextSize);

        Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.oval);


        canvas.drawBitmap(
                icon,
                (mPercent - 10) * mWidthBar / 100,
                getHeight() - convertDp(70),
                mPaint);

        mPaint.setColor(getResources().getColor(R.color.white));
        mPaint.setTextAlign(Paint.Align.CENTER);

        canvas.drawText(
                Sorce,
                (mPercent - 10) * (getWidth() - convertDp(10)) / 100 + convertDp(9),
                mTextSize + convertDp(5),
                mPaint
        );

    }

    private void onDrawPoints(Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.white));
        mPaint.setStrokeWidth(3);
        mPaint.setStyle(Paint.Style.FILL);

        /** draw 10 points **/
        canvas.drawCircle(
                mWidthBar / 10 - mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 8,
                mPaint
        );
        canvas.drawCircle(
                mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 8,
                mPaint
        );
        canvas.drawCircle(
                2 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 8,
                mPaint
        );
        canvas.drawCircle(
                3 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 8,
                mPaint
        );
        canvas.drawCircle(
                4 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 8,
                mPaint
        );
        canvas.drawCircle(
                5 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 8,
                mPaint
        );
        canvas.drawCircle(
                6 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 8,
                mPaint
        );
        canvas.drawCircle(
                7 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 8,
                mPaint
        );
        canvas.drawCircle(
                8 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 8,
                mPaint
        );
        canvas.drawCircle(
                9 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 8,
                mPaint
        );
    }

    private void onDrawForegroudPoints(Canvas canvas) {

        mPaint.setColor(getResources().getColor(R.color.blue));
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.FILL);


        canvas.drawCircle(
                mWidthBar / 10 - mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 7,
                mPaint
        );
        canvas.drawCircle(
                mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 7,
                mPaint
        );
        canvas.drawCircle(
                2 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 7,
                mPaint
        );
        canvas.drawCircle(
                3 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 7,
                mPaint
        );
        canvas.drawCircle(
                4 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 7,
                mPaint
        );
        canvas.drawCircle(
                5 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 7,
                mPaint
        );
        canvas.drawCircle(
                6 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 7,
                mPaint
        );
        canvas.drawCircle(
                7 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 7,
                mPaint
        );
        canvas.drawCircle(
                8 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 7,
                mPaint
        );
        canvas.drawCircle(
                9 * mWidthBar / 10 + convertDp(14),
                getHeight() - convertDp(30),
                mHeightBarTop / 7,
                mPaint
        );

    }

}
