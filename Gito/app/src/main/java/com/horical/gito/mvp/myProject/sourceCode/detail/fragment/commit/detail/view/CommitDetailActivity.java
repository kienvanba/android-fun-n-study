package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.injection.component.CommitDetailComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.adapter.CommitDetailAdapter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.injection.component.DaggerCommitDetailComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.injection.module.CommitDetailModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.presenter.CommitDetailPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CommitDetailActivity extends BaseActivity implements ICommitDetailView, View.OnClickListener {

    @Bind(R.id.code_detail_commit_file)
    RecyclerView mRVDetail;

    private CommitDetailAdapter commitDetailAdapter;

    @Inject
    CommitDetailPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_code_commit_detail);
        ButterKnife.bind(this);

        CommitDetailComponent component = DaggerCommitDetailComponent.builder()
                .commitDetailModule(new CommitDetailModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    private void initListener() {

    }

    private void initData() {

    }

    @Override
    public void onClick(View view) {

    }
}
