package com.horical.gito.mvp.myProject.task.details.fragment.overview.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.injection.module.OverViewModule;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.view.OverViewFragment;

import dagger.Component;


@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = OverViewModule.class)
public interface OverViewComponent {
    void inject(OverViewFragment fragment);
}
