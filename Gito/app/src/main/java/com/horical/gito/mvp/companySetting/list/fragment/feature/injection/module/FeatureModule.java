package com.horical.gito.mvp.companySetting.list.fragment.feature.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.list.fragment.feature.presenter.FeaturePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class FeatureModule {
    @PerFragment
    @Provides
    FeaturePresenter provideFeaturePresenter(){
        return new FeaturePresenter();
    }
}
