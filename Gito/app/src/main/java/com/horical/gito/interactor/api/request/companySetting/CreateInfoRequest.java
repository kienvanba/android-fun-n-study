package com.horical.gito.interactor.api.request.companySetting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.model.TokenFile;
import com.horical.gito.model.WorkingDayWeek;

import java.util.Date;

/**
 * Created by thanhle on 4/4/17.
 */

public class CreateInfoRequest {
    @SerializedName("timeZone")
    @Expose
    public int timeZone;

    @SerializedName("yearlyHoliday")
    @Expose
    public int yearlyHoliday;

    @SerializedName("monthlyHoliday")
    @Expose
    public int monthlyHoliday;

    @SerializedName("workingDay")
    @Expose
    public int workingDay;

    @SerializedName("startOfWeekDay")
    @Expose
    public int startOfWeekDay;

    @SerializedName("workingDayWeek")
    @Expose
    public WorkingDayWeek workingDayWeek;

    @SerializedName("eveningEndTime")
    @Expose
    public Date eveningEndTime;

    @SerializedName("eveningStartTime")
    @Expose
    public Date eveningStartTime;

    @SerializedName("afternoonEndTime")
    @Expose
    public Date afternoonEndTime;

    @SerializedName("afternoonStartTime")
    @Expose
    public Date afternoonStartTime;

    @SerializedName("morningEndTime")
    @Expose
    public Date morningEndTime;

    @SerializedName("morningStartTime")
    @Expose
    public Date morningStartTime;

    @SerializedName("language")
    @Expose
    public String language;

    @SerializedName("country")
    @Expose
    public String country;

    @SerializedName("fax")
    @Expose
    public String fax;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("phone")
    @Expose
    public String phone;

    @SerializedName("domain")
    @Expose
    public String domain;

    @SerializedName("desc")
    @Expose
    public String desc;

    @SerializedName("companyName")
    @Expose
    public String companyName;

    @SerializedName("companyId")
    @Expose
    public String companyId;
}
