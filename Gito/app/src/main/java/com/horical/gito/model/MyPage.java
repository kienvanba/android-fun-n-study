package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MyPage implements Serializable {

    @SerializedName("task")
    @Expose
    private List<Task> tasks;

    @SerializedName("version")
    @Expose
    private List<Version> versions;

    @SerializedName("anouncement")
    @Expose
    private int announcement;

    @SerializedName("meeting")
    @Expose
    private int meeting;

    @SerializedName("recruitment")
    @Expose
    private int recruitment;

    @SerializedName("report")
    @Expose
    private int report;

    @SerializedName("birthday")
    @Expose
    private int birthday;

    @SerializedName("todolist")
    @Expose
    private int toDoList;

    @SerializedName("leavingregister")
    @Expose
    private int leavingRegister;

    @SerializedName("checkin")
    @Expose
    private int checkIn;

    @SerializedName("casestudy")
    @Expose
    private int caseStudy;

    @SerializedName("logtime")
    @Expose
    private List<LogTimeMyPage> logTimes;

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Version> getVersions() {
        return versions;
    }

    public void setVersions(List<Version> versions) {
        this.versions = versions;
    }

    public int getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(int announcement) {
        this.announcement = announcement;
    }

    public int getMeeting() {
        return meeting;
    }

    public void setMeeting(int meeting) {
        this.meeting = meeting;
    }

    public int getRecruitment() {
        return recruitment;
    }

    public void setRecruitment(int recruitment) {
        this.recruitment = recruitment;
    }

    public int getReport() {
        return report;
    }

    public void setReport(int report) {
        this.report = report;
    }

    public int getBirthday() {
        return birthday;
    }

    public void setBirthday(int birthday) {
        this.birthday = birthday;
    }

    public int getToDoList() {
        return toDoList;
    }

    public void setToDoList(int toDoList) {
        this.toDoList = toDoList;
    }

    public int getLeavingRegister() {
        return leavingRegister;
    }

    public void setLeavingRegister(int leavingRegister) {
        this.leavingRegister = leavingRegister;
    }

    public int getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(int checkIn) {
        this.checkIn = checkIn;
    }

    public int getCaseStudy() {
        return caseStudy;
    }

    public void setCaseStudy(int caseStudy) {
        this.caseStudy = caseStudy;
    }

    public List<LogTimeMyPage> getLogTimes() {
        return logTimes;
    }

    public void setLogTimes(List<LogTimeMyPage> logTimes) {
        this.logTimes = logTimes;
    }
}
