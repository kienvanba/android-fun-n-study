package com.horical.gito.mvp.roomBooking.newRoom.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.room.RoomRequest;
import com.horical.gito.interactor.api.response.room.GetCreateRoomResponse;
import com.horical.gito.model.Room;
import com.horical.gito.mvp.roomBooking.newRoom.view.INewRoomView;

import java.util.List;


public class NewRoomPresenter extends BasePresenter implements INewRoomPresenter{

    private List<Room> mListRoom;


    public void attachView(INewRoomView view) {
        super.attachView(view);
    }

    public INewRoomView getView() {
        return (INewRoomView) getIView();
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void createRoom(String name, String description) {
        // validate data
        if (name.trim().length() == 0) {
            getView().validateFailedNameEmpty();
            return;
        }

        //create request
        RoomRequest request = new RoomRequest();
        request.name = name;
        request.desc = description;

        // send create room request
        getView().showLoading();
        getApiManager().createRoom(request, new ApiCallback<GetCreateRoomResponse>() {
            @Override
            public void success(GetCreateRoomResponse res) {
                getView().hideLoading();
                getView().createRoomSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().createRoomFailure(error);
            }
        });


    }
}
