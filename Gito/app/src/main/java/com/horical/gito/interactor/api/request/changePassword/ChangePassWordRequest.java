package com.horical.gito.interactor.api.request.changePassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kut3b on 13/04/2017.
 */

public class ChangePassWordRequest {
    @SerializedName("newpass")
    @Expose
    public String newPass;

    @SerializedName("repass")
    @Expose
    public String rePass;

    @SerializedName("oldpassword")
    @Expose
    public String oldPassword;
}
