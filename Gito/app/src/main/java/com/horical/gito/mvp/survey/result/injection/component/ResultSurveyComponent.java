package com.horical.gito.mvp.survey.result.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.survey.result.injection.module.ResultSurveyModule;
import com.horical.gito.mvp.survey.result.view.ResultSurveyActivity;


import dagger.Component;

/**
 * Created by nhattruong251295 on 3/26/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ResultSurveyModule.class)
public interface ResultSurveyComponent {
    void inject(ResultSurveyActivity activity);
}
