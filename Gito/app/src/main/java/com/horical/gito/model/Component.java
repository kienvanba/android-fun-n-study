package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by KIEN on 12/16/2016
 */

public class Component {

    @SerializedName("_id")
    @Expose
    public String _id;

    @SerializedName("companyId")
    @Expose
    public Company companyId;

    @SerializedName("ProjectId")
    @Expose
    public String projectId;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("componentId")
    @Expose
    public String componentId;
}
