package com.horical.gito.mvp.companySetting.list.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IView;
import com.horical.gito.mvp.companySetting.list.view.ICompanySettingView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thanhle on 3/13/17.
 */

public class CompanySettingPresenter extends BasePresenter implements ICompanySettingPresenter {
    private List<String> mList;
    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public ICompanySettingView getView() {
        return (ICompanySettingView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        initData();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }


    public List<String> getListData() {
        return mList;
    }

    public void initData(){
        mList.add("Info");
        mList.add("Feature");
        mList.add("Members");
        mList.add("Working & Holiday");
        mList.add("QuotaInfo");
        mList.add("Payment");
        mList.add("Project role");
        mList.add("Company role");
        mList.add("Devices");
        mList.add("Level");
        mList.add("Department");
    }
}
