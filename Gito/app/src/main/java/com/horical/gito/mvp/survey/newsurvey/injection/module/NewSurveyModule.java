package com.horical.gito.mvp.survey.newsurvey.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.survey.newsurvey.presenter.NewSurveyPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by thanhle on 11/12/16.
 */
@Module
public class NewSurveyModule {
    @PerActivity
    @Provides
    NewSurveyPresenter providesNewSurveyPresenter(){
        return new NewSurveyPresenter();
    }
}
