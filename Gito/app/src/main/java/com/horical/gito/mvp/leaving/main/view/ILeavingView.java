package com.horical.gito.mvp.leaving.main.view;

import com.horical.gito.base.IView;

/**
 * Created by Dragonoid on 12/7/2016.
 */

public interface ILeavingView extends IView{

    void showLoading();

    void hideLoading();

    void getAllLeavingSuccess();

    void getAllLeavingFailure(String error);

    void getLeavingByIdSuccess();
    void getLeavingByIdFailure(String errorr);
}
