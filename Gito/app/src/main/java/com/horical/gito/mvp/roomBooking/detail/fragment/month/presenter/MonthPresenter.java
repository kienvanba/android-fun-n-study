package com.horical.gito.mvp.roomBooking.detail.fragment.month.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.roomBooking.detail.fragment.month.view.IMonthView;

/**
 * Created by QuangHai on 08/11/2016.
 */

public class MonthPresenter extends BasePresenter implements IMonthPresenter {
    public void attachView(IMonthView view) {
        super.attachView(view);
    }

    public IMonthView getView() {
        return (IMonthView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
