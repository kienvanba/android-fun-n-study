package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nxon on 3/17/17
 */

public class TaskRole implements Serializable {

    @SerializedName("updateReject")
    @Expose
    private boolean updateReject;

    @SerializedName("updateClose")
    @Expose
    private boolean updateClose;

    @SerializedName("updateFinish")
    @Expose
    private boolean updateFinish;

    @SerializedName("del")
    @Expose
    private boolean del;

    @SerializedName("add")
    @Expose
    private boolean add;

    @SerializedName("modify")
    @Expose
    private boolean modify;

    @SerializedName("view")
    @Expose
    private boolean view;

    public boolean isUpdateReject() {
        return updateReject;
    }

    public void setUpdateReject(boolean updateReject) {
        this.updateReject = updateReject;
    }

    public boolean isUpdateClose() {
        return updateClose;
    }

    public void setUpdateClose(boolean updateClose) {
        this.updateClose = updateClose;
    }

    public boolean isUpdateFinish() {
        return updateFinish;
    }

    public void setUpdateFinish(boolean updateFinish) {
        this.updateFinish = updateFinish;
    }

    public boolean isDel() {
        return del;
    }

    public void setDel(boolean del) {
        this.del = del;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isModify() {
        return modify;
    }

    public void setModify(boolean modify) {
        this.modify = modify;
    }

    public boolean isView() {
        return view;
    }

    public void setView(boolean view) {
        this.view = view;
    }
}
