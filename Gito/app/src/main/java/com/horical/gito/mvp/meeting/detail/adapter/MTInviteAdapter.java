package com.horical.gito.mvp.meeting.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.TokenFile;
import com.horical.gito.model.User;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MTInviteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<User> list;
    private OnItemClickListener callback;

    public MTInviteAdapter(Context context, List<User> list, OnItemClickListener callback) {
        this.context = context;
        this.list = list;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MemberViewHolder mMemberViewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_add_meeting_invitee, parent, false);
        mMemberViewHolder = new MemberViewHolder(view);
        return mMemberViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        User user = list.get(position);
        MemberViewHolder viewHolder = (MemberViewHolder) holder;

        viewHolder.mTvMemberName.setText(user.getDisplayName());
        viewHolder.mTvMemberRole.setText(user.getUserId());

        StringBuilder sort_name = new StringBuilder();
        String[] temp = user.getDisplayName().split(" ");
        for (String s : temp) {
            char[] arrChar = s.toCharArray();
            sort_name.append(arrChar[0]);
        }

        TokenFile avatar = user.getAvatar();
        if (avatar != null) {
            String url = CommonUtils.getURL(avatar);
            ImageLoader.load(context, url, viewHolder.mMemberAva, viewHolder.mPgrMember);
        } else {
            viewHolder.mPgrMember.setVisibility(View.GONE);
            viewHolder.mTvMemberNonAva.setText(sort_name.toString().toUpperCase());
        }
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public class MemberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @Bind(R.id.img_avatar)
        CircleImageView mMemberAva;

        @Bind(R.id.tv_avatar)
        TextView mTvMemberNonAva;

        @Bind(R.id.meeting_member_prg)
        ProgressBar mPgrMember;

        @Bind(R.id.edit_name)
        TextView mTvMemberName;

        @Bind(R.id.tv_role)
        TextView mTvMemberRole;

        @Bind(R.id.btn_delete)
        ImageButton mBtnMemberDelete;

        public MemberViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mBtnMemberDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == mBtnMemberDelete.getId()) {
                callback.onMTInviteDelete(getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onMTInviteDelete(int position);
    }
}
