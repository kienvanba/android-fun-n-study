package com.horical.gito.mvp.companySetting.list.fragment.devices.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.list.fragment.devices.presenter.DevicesPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class DevicesModule {
    @PerFragment
    @Provides
    DevicesPresenter provideDevicesPresenter(){
        return new DevicesPresenter();
    }
}
