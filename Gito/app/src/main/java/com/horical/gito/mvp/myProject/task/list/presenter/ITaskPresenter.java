package com.horical.gito.mvp.myProject.task.list.presenter;

import com.horical.gito.model.Task;

import java.util.List;

public interface ITaskPresenter {
    void getAllTask();

    List<Task> getListSearchTask(String keywords);
    void deleteTask(String taskID);
}
