package com.horical.gito.mvp.myProject.task.details.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.Task;
import com.horical.gito.mvp.myProject.task.details.adapter.TaskViewPagerAdapter;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.view.OverViewFragment;
import com.horical.gito.mvp.myProject.task.details.injection.component.DaggerTaskDetailsComponent;
import com.horical.gito.mvp.myProject.task.details.injection.component.TaskDetailsComponent;
import com.horical.gito.mvp.myProject.task.details.injection.module.TaskDetailsModule;
import com.horical.gito.mvp.myProject.task.details.presenter.TaskDetailsPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class TaskDetailsActivity extends BaseActivity implements ITaskDetailsView, View.OnClickListener {

    public static final String OVERVIEW = "overview";
    public static final String SUBTASK = "subtask";
    public static final String LOGTIME = "logtime";
    public static final String COMMENT = "comment";
    private String current = OVERVIEW;
    public static final int OPEN_FOR_CREATING = 0;
    public static final int OPEN_FOR_UPDATING = 1;

    private Task task;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.tv_overview_task)
    TextView tvOverView;

    @Bind(R.id.tv_sub_task)
    TextView tvSubTask;

    @Bind(R.id.tv_logtime_task)
    TextView tvLogtime;

    @Bind(R.id.tv_comment_task)
    TextView tvComment;

    @Bind(R.id.view_pager_task)
    ViewPager viewPager;

    @Bind(R.id.img_overview)
    CircleImageView imgOverview;

    @Bind(R.id.img_subtask)
    CircleImageView imgSubtask;

    @Bind(R.id.img_logtime)
    CircleImageView imgLogtime;

    @Bind(R.id.img_comment)
    CircleImageView imgComment;

    @Bind(R.id.fl_title)
    FrameLayout flTitle;

    @Bind(R.id.rl_current_page)
    RelativeLayout rlCurrentPage;


    private TaskViewPagerAdapter adapterViewPager;

    @Inject
    TaskDetailsPresenter mPresenter;

    private static final String TAG = makeLogTag(TaskDetailsActivity.class);

    protected int getLayoutId() {
        return R.layout.activity_task_details;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);

        TaskDetailsComponent component = DaggerTaskDetailsComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .taskDetailsModule(new TaskDetailsModule())
                .build();

        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        switch (getIntent().getIntExtra("type", 1)) {
            case OPEN_FOR_CREATING:
                showJustOverview();
                break;
            case OPEN_FOR_UPDATING:
                task = (Task)(getIntent().getSerializableExtra("task"));
                showAllTitle(task.getTaskId());
                break;
        }

        initData();
        initListener();
    }

    public void showJustOverview() {
        flTitle.setVisibility(View.GONE);
        rlCurrentPage.setVisibility(View.GONE);
        topBar.setTextTitle("New Task");
        topBar.setTextViewLeft("Cancel");
        topBar.setTextViewRight("Save");

    }

    public void showAllTitle(int taskOrder) {
        flTitle.setVisibility(View.VISIBLE);
        rlCurrentPage.setVisibility(View.VISIBLE);
        topBar.setTextTitle("Task #" + taskOrder);
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        topBar.setTextViewRight("Save");

    }

    protected void initData() {
        if (task == null) {
            task = new Task();
        }

        tvOverView.setSelected(true);
        imgOverview.setImageResource(R.color.blue);

        current = "overview";

        adapterViewPager = new TaskViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);

        tvOverView.setOnClickListener(this);
        tvSubTask.setOnClickListener(this);
        tvLogtime.setOnClickListener(this);
        tvComment.setOnClickListener(this);
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {
                if (task.get_id() == null || task.get_id().equals("")) {
                    initTask();
                    mPresenter.createTask(task);
                }
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                imgOverview.setImageResource(R.color.grey);
                imgSubtask.setImageResource(R.color.grey);
                imgLogtime.setImageResource(R.color.grey);
                imgComment.setImageResource(R.color.grey);
                switch (position) {
                    case 0:
                        updateTextViewSelected(tvOverView);
                        current = OVERVIEW;
                        imgOverview.setImageResource(R.color.blue);
                        break;
                    case 1:
                        updateTextViewSelected(tvSubTask);
                        current = SUBTASK;
                        imgSubtask.setImageResource(R.color.blue);
                        break;
                    case 2:
                        updateTextViewSelected(tvLogtime);
                        current = LOGTIME;
                        imgLogtime.setImageResource(R.color.blue);
                        break;
                    case 3:
                        updateTextViewSelected(tvComment);
                        current = COMMENT;
                        imgComment.setImageResource(R.color.blue);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void initTask() {
        task = ((OverViewFragment) (getSupportFragmentManager().getFragments().get(0))).initTask();
    }

    public void updateTextViewSelected(TextView tv) {
        tvOverView.setSelected(false);
        tvSubTask.setSelected(false);
        tvLogtime.setSelected(false);
        tvComment.setSelected(false);
        tv.setSelected(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_overview_task: {
                viewPager.setCurrentItem(0);
                imgOverview.setImageResource(R.color.blue);
                break;
            }
            case R.id.tv_sub_task: {
                viewPager.setCurrentItem(1);
                imgSubtask.setImageResource(R.color.blue);
                break;
            }
            case R.id.tv_logtime_task: {
                viewPager.setCurrentItem(2);
                imgLogtime.setImageResource(R.color.blue);
                break;
            }
            case R.id.tv_comment_task: {
                viewPager.setCurrentItem(3);
                imgComment.setImageResource(R.color.blue);
                break;
            }
        }
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void createTaskSuccess(Task task) {
        setTask(task);
        hideLoading();
        showAllTitle(1);
        showToast("create success");
    }

    @Override
    public void createTaskFailure(String error) {
        hideLoading();
        showToast("create error: " + error);
    }

    @Override
    public void updateTaskOverViewSuccess(Task task) {
        hideLoading();
        showToast("update success");
    }

    @Override
    public void updateTaskOverViewFailure(String error) {
        hideLoading();
        showToast("update error: " + error);
    }
}
