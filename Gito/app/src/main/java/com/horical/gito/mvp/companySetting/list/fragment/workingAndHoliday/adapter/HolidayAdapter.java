package com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Holiday;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HolidayAdapter extends RecyclerView.Adapter<HolidayAdapter.HolidayHolder> {
    private Context mContext;
    private List<Holiday> mList;
    private OnItemClickListener mOnItemClickListener;

    public HolidayAdapter(Context context, List<Holiday> list) {
        mContext = context;
        mList = list;
    }


    @Override
    public HolidayHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_company_setting_holiday, parent, false);
        return new HolidayHolder(view);
    }

    @Override
    public void onBindViewHolder(HolidayHolder holder, final int position) {
        Holiday holiday = mList.get(position);
        holder.tvName.setText(holiday.getName());

        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDelete(position);
            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class HolidayHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.tv_day)
        TextView tvDay;

        @Bind(R.id.ll_delete)
        LinearLayout llDelete;

        @Bind(R.id.ll_edit)
        LinearLayout llEdit;

        View view;


        public HolidayHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);

        void onDelete(int position);
    }
}
