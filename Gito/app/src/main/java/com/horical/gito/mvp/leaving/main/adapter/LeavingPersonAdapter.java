package com.horical.gito.mvp.leaving.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.TokenFile;
import com.horical.gito.model.User;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.widget.chartview.circle.CircleView;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Dragonoid on 2/10/2017.
 */

public class LeavingPersonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<User> mItems;
    Context mContext;

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public LeavingPersonAdapter(List<User> items, Context context) {
        mItems = items;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leaving_person, parent, false);
        return new LeavingPersonHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        LeavingPersonHolder personHolder = (LeavingPersonHolder) holder;
        User item =  mItems.get(position);
        personHolder.mName.setText(item.getDisplayName());
        personHolder.mPosition.setText(item.getRoleCompany().getRoleName());
        TokenFile avatar = item.getAvatar();
        if (avatar != null) {
            String url = CommonUtils.getURL(avatar);
            ImageLoader.load(mContext,url,personHolder.mAvatar,personHolder.mPrgLoading);
        }else {
            personHolder.mPrgLoading.setVisibility(View.GONE);
            personHolder.mSortName.setText(item.getDisplayName().substring(0, 2).toUpperCase());
            personHolder.mSortName.setVisibility(View.VISIBLE);
        }
    }

    public class LeavingPersonHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.leaving_img_person_avatar)
        CircleImageView mAvatar;
        @Bind(R.id.leaving_pgr_person_progress)
        ProgressBar mPrgLoading;
        @Bind(R.id.leaving_tv_person_sort_name)
        GitOTextView mSortName;
        @Bind(R.id.leaving_tv_person_name)
        TextView mName;
        @Bind(R.id.leaving_tv_person_position)
        TextView mPosition;

        public LeavingPersonHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

