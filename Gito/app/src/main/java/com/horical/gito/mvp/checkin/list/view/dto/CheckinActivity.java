package com.horical.gito.mvp.checkin.list.view.dto;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.checkin.list.injection.component.CheckinComponent;
import com.horical.gito.mvp.checkin.list.injection.component.DaggerCheckinComponent;
import com.horical.gito.mvp.checkin.list.injection.module.CheckinModule;
import com.horical.gito.mvp.checkin.list.presenter.CheckinPresenter;
import com.horical.gito.mvp.checkin.list.view.adapter.AvatarMajorItem;
import com.horical.gito.mvp.checkin.list.view.adapter.CheckinWeekAdapter;
import com.horical.gito.mvp.checkin.detail.view.Checkin2Activity;
import com.horical.gito.mvp.dialog.dialogCheckin.TimePickerDialog;
import com.horical.gito.mvp.drawer.DrawerActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.FROM_MENU;
import static com.horical.gito.AppConstants.KEY_FROM;

public class CheckinActivity extends DrawerActivity
        implements ICheckinView, View.OnClickListener {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.rv_checkin_user)
    RecyclerView rcvCheckinUser;

    @Bind(R.id.img_checkin_show)
    ImageView imgShow;

    @Bind(R.id.rdb_group_rg_type)
    RadioGroup rdbGroupType;

    @Bind(R.id.rdb_btn_day)
    RadioButton rdbDay;

    @Bind(R.id.rdb_btn_week)
    RadioButton rdbWeek;

    @Bind(R.id.rdb_btn_month)
    RadioButton rdbMonth;

    @Bind(R.id.img_back_day)
    ImageView imgBack;

    @Bind(R.id.tv_day)
    TextView tvDay;

    @Bind(R.id.img_next_day)
    ImageView imgNext;

    @Bind(R.id.tv_time_in)
    TextView tvTimeIn;

    @Bind(R.id.tv_time_out)
    TextView tvTimeOut;

    @Bind(R.id.img_pick_time)
    ImageView imgPickTime;

    @Bind(R.id.btn_check_in)
    Button btnCheckin;

    @Bind(R.id.btn_check_out)
    Button btnCheckOut;

    @Bind(R.id.btn_checkin_reject)
    Button btnReject;

    @Bind(R.id.btn_checkin_approve)
    Button btnApprove;

    Calendar cal;

    TimePickerDialog TimePickerDialog;

    CheckinWeekAdapter mAdapter;
    Animation.AnimationListener mAnimationListener;
    List<AvatarMajorItem> mList;

    @Inject
    CheckinPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_checkin;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_CHECK_IN;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(getLayoutId());
        ButterKnife.bind(this);
        CheckinComponent component = DaggerCheckinComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .checkinModule(new CheckinModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(getString(R.string.check_in));
        int from = getIntent().getIntExtra(KEY_FROM, FROM_MENU);
        mPresenter.setFrom(from);
        if (from == FROM_MENU) {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        } else {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
            lockDrawer();
        }
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_FILTER);

        cal = Calendar.getInstance();
        SimpleDateFormat dft = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        final String strDate = dft.format(cal.getTime());
        tvDay.setText(strDate);

        mList = new ArrayList<>();
        final AvatarMajorItem avatarMajorItem = new AvatarMajorItem(R.drawable.ic_add_blue, "Add user");
        mList.add(avatarMajorItem);
        mAdapter = new CheckinWeekAdapter(this, mList);
        LinearLayoutManager li = new LinearLayoutManager(this);
        li.setOrientation(LinearLayoutManager.HORIZONTAL);
        rcvCheckinUser.setLayoutManager(li);
        rcvCheckinUser.setAdapter(mAdapter);

    }

    protected void initListener() {
        rdbDay.setOnClickListener(this);
        rdbWeek.setOnClickListener(this);
        rdbMonth.setOnClickListener(this);
        btnCheckin.setOnClickListener(this);
        btnCheckOut.setOnClickListener(this);
        btnReject.setOnClickListener(this);
        btnApprove.setOnClickListener(this);
        imgPickTime.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgNext.setOnClickListener(this);
        imgShow.setOnClickListener(this);

        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                if (mPresenter.getFrom() == FROM_MENU) openDrawer();
                else onBackPressed();
            }

            @Override
            public void onRightButtonOneClicked() {

            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });
    }


    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.rdb_btn_day:
                break;
            case R.id.rdb_btn_week:
                Intent w = new Intent(this, Checkin2Activity.class);
                startActivity(w);
                break;
            case R.id.rdb_btn_month:
                Intent m = new Intent(this, Checkin3Activity.class);
                startActivity(m);
                break;
            case R.id.btn_check_in:
                tvTimeIn.setText("8:00 AM");
                tvTimeIn.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.btn_check_out:
                tvTimeOut.setText("5:00 PM");
                tvTimeOut.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.img_back_day:
                cal.add(Calendar.DATE, -1);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                String preDate = sdf.format(cal.getTime());
                tvDay.setText(preDate);
                break;
            case R.id.img_next_day:
                cal.add(Calendar.DATE, 1);
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                String nextDate = sdf1.format(cal.getTime());
                tvDay.setText(nextDate);
                break;
            case R.id.img_pick_time:
//                TimePickerDialog = new TimePickerDialog(this);
//                TimePickerDialog.show();
                break;
            case R.id.img_checkin_show:
                if (v.getId() == imgShow.getId()) {
                    if (rcvCheckinUser.getVisibility() == View.VISIBLE) {
                        rcvCheckinUser.setVisibility(View.GONE);
                        imgShow.startAnimation(RotateDown());

                    } else if (rcvCheckinUser.getVisibility() == View.GONE) {
                        rcvCheckinUser.setVisibility(View.VISIBLE);
                        imgShow.startAnimation(RotateUp());
                    }
                }
                break;
        }
    }

    private Animation RotateUp() {
        Animation animation = new RotateAnimation(0f, 180f, RotateAnimation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setAnimationListener(mAnimationListener);
        animation.setFillAfter(true);
        animation.setDuration(250);
        return animation;
    }

    private Animation RotateDown() {
        Animation animation = new RotateAnimation(180f, 0f, RotateAnimation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setAnimationListener(mAnimationListener);
        animation.setFillAfter(true);
        animation.setDuration(250);
        return animation;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void getAllCheckinSuccess() {

    }

}
