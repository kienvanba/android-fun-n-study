package com.horical.gito.mvp.report.newReport.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.model.ObjectSerialized;
import com.horical.gito.mvp.dialog.dialogInputEdit.InputEditDialog;
import com.horical.gito.mvp.dialog.dialogSelectDate.SelectDateDialog;
import com.horical.gito.mvp.report.newReport.adapter.ReportUserAdapter;
import com.horical.gito.mvp.report.newReport.injection.component.DaggerNewReportComponent;
import com.horical.gito.mvp.report.newReport.injection.component.NewReportComponent;
import com.horical.gito.mvp.report.newReport.injection.module.NewReportModule;
import com.horical.gito.mvp.report.newReport.presenter.NewReportPresenter;
import com.horical.gito.mvp.report.selectUserGroup.view.UserGroupActivity;
import com.horical.gito.mvp.report.view.ReportActivity;
import com.horical.gito.utils.DateUtils;

import java.util.Date;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class NewReportActivity extends BaseActivity implements View.OnClickListener, INewReportView {

    public static final int KEY_REQUEST = 34;

    @Bind(R.id.tv_cancel)
    TextView tvCancel;

    @Bind(R.id.tv_save)
    TextView tvSave;

    @Bind(R.id.tv_date)
    TextView tvDate;

    @Bind(R.id.imv_edit_date)
    ImageView imvEditDate;

    @Bind(R.id.imv_dit_doing)
    ImageView imvEditDoing;

    @Bind(R.id.tv_doing)
    TextView tvDoing;

    @Bind(R.id.imv_edit_problem)
    ImageView imvProblem;

    @Bind(R.id.tv_problem)
    TextView tvProblem;

    @Bind(R.id.imv_edit_finish)
    ImageView imvEditFinish;

    @Bind(R.id.tv_finish)
    TextView tvFinish;

    @Bind(R.id.tv_submit)
    TextView tvSubmit;

    @Bind(R.id.rcv_user_report)
    RecyclerView mLv;

    @Inject
    NewReportPresenter mPresenter;

    private SelectDateDialog selectDate;
    private Date mDate = null;
    private ReportUserAdapter adapter;
    private InputEditDialog inputDialog;
    private Intent intent;
    private int positionHeader = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_new_report_activity);
        ButterKnife.bind(this);

        NewReportComponent component = DaggerNewReportComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .newReportModule(new NewReportModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        adapter = new ReportUserAdapter(this, mPresenter.getList());
        mLv.setHasFixedSize(true);
        mLv.setLayoutManager(new LinearLayoutManager(this));
        mLv.setAdapter(adapter);

        adapter.setOnItemClickListener(new ReportUserAdapter.OnItemClickListener() {
            @Override
            public void onAddItem(int position) {
                positionHeader = position;
                intent = new Intent(NewReportActivity.this, UserGroupActivity.class);
                intent.putExtra(UserGroupActivity.KEY_FROM, position);
                startActivityForResult(intent, KEY_REQUEST);
            }

            @Override
            public void onDeleteItem(int positionHeader, int positionItem) {
                mPresenter.onDeleteItem(positionHeader, positionItem);
                adapter.notifyItemChanged(positionHeader);
            }
        });
    }

    protected void initListener() {
        tvCancel.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        imvEditDate.setOnClickListener(this);
        imvEditDoing.setOnClickListener(this);
        imvProblem.setOnClickListener(this);
        imvEditFinish.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        intent = null;
        if (requestCode == KEY_REQUEST) {
            if (resultCode == RESULT_OK) {
                ObjectSerialized item = (ObjectSerialized) data.getSerializableExtra(UserGroupActivity.KEY_SAVE_DATA);
                if (item != null && !item.getList().isEmpty()) {
                    mPresenter.onAddIem(positionHeader, item.getList());
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                finish();
                break;

            case R.id.tv_save:

                if (!isCheckSave().isEmpty()) {
                    showErrorDialog(isCheckSave());
                } else {
                    mPresenter.onCreateReport(mDate, tvDoing.getText().toString(), tvProblem.getText().toString(), tvFinish.getText().toString(), 0);
                }
                break;

            case R.id.imv_edit_date:
                if (selectDate == null || selectDate.getDialog() == null || !selectDate.getDialog().isShowing()) {
                    selectDate = new SelectDateDialog(NewReportActivity.this, mDate, new SelectDateDialog.OnDoneClickListener() {
                        @Override
                        public void onDone(Date date) {
                            mDate = date;
                            tvDate.setText(DateUtils.formatFullDate(date));

                        }
                    });
                    selectDate.show(getSupportFragmentManager(), "Dialog Select Date");
                }
                break;

            case R.id.imv_dit_doing:
                inPutData(tvDoing);
                break;

            case R.id.imv_edit_problem:
                inPutData(tvProblem);
                break;

            case R.id.imv_edit_finish:
                inPutData(tvFinish);
                break;
            case R.id.tv_submit:
                if (!isCheckSave().isEmpty()) {
                    showErrorDialog(isCheckSave());
                } else {
                    mPresenter.onCreateReport(mDate, tvDoing.getText().toString(), tvProblem.getText().toString(), tvFinish.getText().toString(), ReportActivity.STATUS_SUBMITTED);
                }
                break;
        }
    }

    String isCheckSave() {
        if (mDate == null
                || tvDoing.getText().toString().isEmpty()
                || tvDoing.getText().toString().isEmpty()
                || tvDoing.getText().toString().isEmpty()
                || mPresenter.getList().get(0).getList().isEmpty()) {
            return getString(R.string.error_input_report);
        } else {
            return "";
        }
    }

    void inPutData(final TextView textView) {
        if (inputDialog == null || !inputDialog.isShowing()) {
            inputDialog = new InputEditDialog(NewReportActivity.this, textView.getText().toString(), new InputEditDialog.OnDoneListener() {
                @Override
                public void onDone(String text) {
                    textView.setText(text);
                    inputDialog.dismiss();

                }
            });
            inputDialog.show();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void createReportSuccess() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void showErrorLoading(String error) {
        dismissDialog();
        showErrorDialog(error);
    }

    @Override
    public void dismissLoading() {
        dismissDialog();
    }
}

