package com.horical.gito.mvp.meeting.detail.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

public interface IMeetingDetailView extends IView {

    void showLoading();

    void hideLoading();

    void validateFailedNameEmpty();

    void validateFailedDescEmpty();

    void validateFailedTime();

    void createMeetingSuccess();

    void createMeetingFailure(RestError error);

    void updateMeetingSuccess();

    void updateMeetingFailure(RestError error);

    void uploadSuccess(String url);

    void uploadFailure(String error);

}
