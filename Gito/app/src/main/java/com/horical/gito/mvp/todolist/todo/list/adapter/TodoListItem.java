package com.horical.gito.mvp.todolist.todo.list.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.TodoList;

/**
 * Created by Luong on 15-Mar-17.
 */

public class TodoListItem implements IRecyclerItem {
    public TodoList mTodoList;

    public TodoListItem(TodoList todoList) {
        this.mTodoList = todoList;
    }

    public TodoList getTodoList() {
        return mTodoList;
    }

    public void setTodoList(TodoList todoList) {
        mTodoList = todoList;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.TODO_LIST_ITEM;
    }
}