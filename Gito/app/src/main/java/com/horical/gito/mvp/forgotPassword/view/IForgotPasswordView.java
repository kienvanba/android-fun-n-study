package com.horical.gito.mvp.forgotPassword.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

public interface IForgotPasswordView extends IView {

    void showLoading();

    void hideLoading();

    void errorEmptyInput();

    void errorInvalidEmail();

    void requestForgotPasswordSuccess();

    void requestForgotPasswordError(RestError error);
}
