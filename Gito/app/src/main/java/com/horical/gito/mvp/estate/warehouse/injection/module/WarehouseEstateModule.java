package com.horical.gito.mvp.estate.warehouse.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.estate.warehouse.presenter.WarehouseEstatePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hoangsang on 4/12/17.
 */

@Module
public class WarehouseEstateModule {

    @PerActivity
    @Provides
    WarehouseEstatePresenter provideWarehourseEstatePresenter(){
        return new WarehouseEstatePresenter();
    }
}
