package com.horical.gito.mvp.report.newReport.adapter;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ReportUserAdapter extends RecyclerView.Adapter<ReportUserAdapter.ViewHolderReport> {

    private Context mContext;
    private List<UserDto> list;

    public ReportUserAdapter(Context mContext, List<UserDto> mList) {
        this.mContext = mContext;
        this.list = mList;
    }

    @Override
    public ViewHolderReport onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_item_header_user_new_report, parent, false);
        return new ViewHolderReport(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderReport holder, final int position) {
        holder.tvHeader.setText(list.get(position).getTitle());
        holder.imvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallBack != null) {
                    mCallBack.onAddItem(position);
                }
            }
        });
        holder.mLv.setLayoutManager(new LinearLayoutManager(mContext));
        holder.mLv.setHasFixedSize(true);
        holder.mLv.setAdapter(new ReportUserAdapterItem(mContext, list.get(position).getList(), new ReportUserAdapterItem.OnItemClickListener() {
            @Override
            public void onDeleteItem(int positionItem) {
                mCallBack.onDeleteItem(position, positionItem);
            }
        }));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolderReport extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_title_header)
        TextView tvHeader;

        @Bind(R.id.imv_add)
        ImageView imvAdd;

        @Bind(R.id.rcv_user)
        RecyclerView mLv;

        public ViewHolderReport(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private OnItemClickListener mCallBack;

    public void setOnItemClickListener(OnItemClickListener callback) {
        this.mCallBack = callback;
    }

    public interface OnItemClickListener {

        void onAddItem(int position);

        void onDeleteItem(int positionHeader, int positionItem);

    }
}