package com.horical.gito.interactor.api.response.summary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Summary;

/**
 * Created by nxon on 3/17/17
 */

public class GetSummaryResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    private Summary summary;

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }
}
