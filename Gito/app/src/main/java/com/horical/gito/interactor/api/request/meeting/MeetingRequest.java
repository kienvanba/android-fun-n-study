package com.horical.gito.interactor.api.request.meeting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MeetingRequest {

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("desc")
    @Expose
    public String desc;

    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("endDate")
    @Expose
    public Date endDate;

    @SerializedName("room")
    @Expose
    public String room;

    @SerializedName("members")
    @Expose
    public List<String> members = new ArrayList<>();

    @SerializedName("note")
    @Expose
    public List<String> note = new ArrayList<>();

    @SerializedName("attachFiles")
    @Expose
    public List<String> attachFiles = new ArrayList<>();

}