package com.horical.gito.mvp.companySetting.detail.newProjectRole.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.presenter.NewProjectRolePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class NewProjectRoleModule {
    @PerActivity
    @Provides
    NewProjectRolePresenter provideNewProjectRolePresenter() {
        return new NewProjectRolePresenter();
    }
}
