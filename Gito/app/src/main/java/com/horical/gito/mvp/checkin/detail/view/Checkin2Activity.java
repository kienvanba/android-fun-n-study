package com.horical.gito.mvp.checkin.detail.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.mvp.checkin.list.view.adapter.AvatarMajorItem;
import com.horical.gito.mvp.checkin.list.view.adapter.CheckinWeekAdapter;
import com.horical.gito.mvp.checkin.list.view.dto.Checkin3Activity;
import com.horical.gito.mvp.checkin.list.view.dto.CheckinActivity;
import com.horical.gito.mvp.checkin.detail.adapter.CheckinAdapter;
import com.horical.gito.mvp.checkin.detail.injection.component.Checkin2Component;
import com.horical.gito.mvp.checkin.detail.injection.component.DaggerCheckin2Component;
import com.horical.gito.mvp.checkin.detail.injection.module.Checkin2Module;
import com.horical.gito.mvp.checkin.detail.presenter.Checkin2Presenter;
import com.horical.gito.mvp.checkin.list.view.dto.CheckinFilter;
import com.horical.gito.mvp.intro.view.IntroActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class Checkin2Activity extends BaseActivity implements ICheckin2View, View.OnClickListener {

    @Bind(R.id.rb_btn_day)
    RadioButton rb_btn_day;

    @Bind(R.id.rb_btn_week)
    RadioButton rb_btn_week;

    @Bind(R.id.rb_btn_month)
    RadioButton rb_btn_month;

    @Bind(R.id.lnl_total)
    LinearLayout total;

    @Bind(R.id.tv2)
    TextView tv2;

    @Bind(R.id.tv4)
    TextView tv4;

    @Bind(R.id.rb_btn_back)
    LinearLayout rb_btn_back;

    @Bind(R.id.rb_rv_checkin_day)
    RecyclerView day;

    @Bind(R.id.iv_Back)
    ImageView Back;

    @Bind(R.id.iv_Next)
    ImageView Next;

    @Bind(R.id.checkin2_layout)
    LinearLayout Lnl;

    @Bind(R.id.iv_checkin_filter)
    ImageView filter;

    @Bind(R.id.rv_checkin_user)
    RecyclerView user;

    @Bind(R.id.iv_checkin_show)
    ImageView show;

    Calendar cal, cal1;
    CheckinWeekAdapter mAdapter;
    Animation.AnimationListener mAnimationListener;
    List<AvatarMajorItem> mList;
    CheckinAdapter mcheckinadapter;

    @Inject
    Checkin2Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin_2);
        ButterKnife.bind(this);
        Checkin2Component component = DaggerCheckin2Component.builder()
                .checkin2Module(new Checkin2Module())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();
        initData();
        Init();
        setDateTime();
        setOnClick();
    }

    private void setDateTime() {
        cal = Calendar.getInstance();
        cal1 = Calendar.getInstance();
        SimpleDateFormat dft = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal1.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        String strDate1 = dft.format(cal.getTime());
        String strDate2 = dft.format(cal1.getTime());
        tv2.setText(strDate1);
        tv4.setText(strDate2);
    }

    private void setOnClick() {
        rb_btn_day.setOnClickListener(this);
        rb_btn_week.setOnClickListener(this);
        rb_btn_month.setOnClickListener(this);
        rb_btn_back.setOnClickListener(this);
        Back.setOnClickListener(this);
        Next.setOnClickListener(this);
        filter.setOnClickListener(this);
        show.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rb_btn_day:
                Intent d = new Intent(this, CheckinActivity.class);
                startActivity(d);
                break;
            case R.id.rb_btn_week:
                break;
            case R.id.rb_btn_month:
                Intent m = new Intent(this, Checkin3Activity.class);
                startActivity(m);
                break;
            case R.id.rb_btn_back:
                Intent b = new Intent(this, IntroActivity.class);
                startActivity(b);
                break;
            case R.id.iv_checkin_filter:
                Intent p1 = new Intent(this, CheckinFilter.class);
                startActivity(p1);
                break;
            case R.id.iv_Back:
                cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                cal1.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                cal.add(Calendar.DATE, -7);
                cal1.add(Calendar.DATE, -7);
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                String strLastweek1 = sdf1.format(cal.getTime());
                String strLastweek2 = sdf1.format(cal1.getTime());
                tv2.setText(strLastweek1);
                tv4.setText(strLastweek2);
                break;
            case R.id.iv_Next:
                cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                cal1.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                cal.add(Calendar.DATE, 7);
                cal1.add(Calendar.DATE, 7);
                SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                String strNextweek1 = sdf2.format(cal.getTime());
                String strNextweek2 = sdf2.format(cal1.getTime());
                tv2.setText(strNextweek1);
                tv4.setText(strNextweek2);
                break;
            case R.id.iv_checkin_show:
                if (v.getId() == show.getId()) {
                    if (user.getVisibility() == View.VISIBLE) {
                        user.setVisibility(View.GONE);
                        show.startAnimation(RotateDown());

                    } else if (user.getVisibility() == View.GONE) {
                        user.setVisibility(View.VISIBLE);
                        show.startAnimation(RotateUp());
                    }
                }
                break;
        }
    }

    protected void initData() {
        mcheckinadapter = new CheckinAdapter(mPresenter.getCheckin(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        day.setLayoutManager(linearLayoutManager);
        day.setHasFixedSize(false);
        day.setAdapter(mcheckinadapter);

        mPresenter.getAllCheckin();
    }

    public void Init() {
        mList = new ArrayList<>();

        final AvatarMajorItem avatarMajorItem = new AvatarMajorItem(R.drawable.ic_add_blue, "Add user");

        mList.add(avatarMajorItem);

        mAdapter = new CheckinWeekAdapter(this, mList);
        LinearLayoutManager li = new LinearLayoutManager(this);
        li.setOrientation(LinearLayoutManager.HORIZONTAL);
        user.setLayoutManager(li);
        user.setAdapter(mAdapter);

    }

    private Animation RotateUp() {
        Animation animation = new RotateAnimation(0f, 180f, RotateAnimation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setAnimationListener(mAnimationListener);
        animation.setFillAfter(true);
        animation.setDuration(250);
        return animation;
    }

    private Animation RotateDown() {
        Animation animation = new RotateAnimation(180f, 0f, RotateAnimation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setAnimationListener(mAnimationListener);
        animation.setFillAfter(true);
        animation.setDuration(250);
        return animation;
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();

    }

    @Override
    public void getAllCheckinSuccess() {
        mcheckinadapter.notifyDataSetChanged();
    }

    @Override
    public void getAllCheckinFailure(RestError error) {

    }


}
