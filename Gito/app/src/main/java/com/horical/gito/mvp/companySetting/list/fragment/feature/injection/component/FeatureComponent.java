package com.horical.gito.mvp.companySetting.list.fragment.feature.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.fragment.feature.injection.module.FeatureModule;
import com.horical.gito.mvp.companySetting.list.fragment.feature.view.FeatureFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = FeatureModule.class)
public interface FeatureComponent {
    void inject(FeatureFragment fragment);
}
