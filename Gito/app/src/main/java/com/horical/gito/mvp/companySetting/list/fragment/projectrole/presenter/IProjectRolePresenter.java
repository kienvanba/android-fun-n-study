package com.horical.gito.mvp.companySetting.list.fragment.projectrole.presenter;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IProjectRolePresenter {
    void getAllProjectRole();

    void deleteProjectRole(String idProjectRole, int position);
}
