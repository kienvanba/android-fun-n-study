package com.horical.gito.mvp.report.newReport.view;

import com.horical.gito.base.IView;

/**
 * Created by thanhle on 3/2/17.
 */

public interface INewReportView extends IView {

    void createReportSuccess();

    void showLoading();

    void showErrorLoading(String error);

    void dismissLoading();

}
