package com.horical.gito.mvp.intro.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.intro.view.IIntroView;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class IntroPresenter extends BasePresenter implements IIntroPresenter {

    private static final String TAG = makeLogTag(IntroPresenter.class);

    public void attachView(IIntroView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IIntroView getView() {
        return (IIntroView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
