package com.horical.gito.mvp.salary.list.presenter;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.salary.dto.SalaryHeaderDto;
import com.horical.gito.mvp.salary.list.view.IListSalaryView;

import java.util.ArrayList;
import java.util.List;

public class ListSalaryPresenter extends BasePresenter implements IListSalaryPresenter {

    public void attachView(IListSalaryView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IListSalaryView getView() {
        return (IListSalaryView) getIView();
    }

    private List<SalaryHeaderDto> mHeaders;

    private void initHeaders() {
        getHeaders().add(new SalaryHeaderDto(MainApplication.mContext.getString(R.string.salary)));
        getHeaders().add(new SalaryHeaderDto(MainApplication.mContext.getString(R.string.staff)));
        getHeaders().add(new SalaryHeaderDto(MainApplication.mContext.getString(R.string.metric_group)));
        getHeaders().add(new SalaryHeaderDto(MainApplication.mContext.getString(R.string.email_form)));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        initHeaders();
    }

    @Override
    public List<SalaryHeaderDto> getHeaders() {
        if (mHeaders == null) {
            mHeaders = new ArrayList<>();
        }
        return mHeaders;
    }

    @Override
    public void setHeaders(List<SalaryHeaderDto> headers) {
        this.mHeaders = headers;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
