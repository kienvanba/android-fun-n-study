package com.horical.gito.mvp.companySetting.detail.newCompanyRole.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.view.INewCompanyRoleView;

/**
 * Created by Tin on 23-Nov-16.
 */

public class NewCompanyRolePresenter extends BasePresenter implements INewCompanyRolePresenter {

    public void attachView(INewCompanyRoleView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public INewCompanyRoleView getView() {
        return (INewCompanyRoleView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}

