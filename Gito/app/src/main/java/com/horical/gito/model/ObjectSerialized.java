package com.horical.gito.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by thien on 4/10/17.
 */

public class ObjectSerialized implements Serializable{

    public ObjectSerialized(List<Object> list) {
        this.list = list;
    }

    public List<Object> getList() {
        return list;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }

    private List<Object> list;

}
