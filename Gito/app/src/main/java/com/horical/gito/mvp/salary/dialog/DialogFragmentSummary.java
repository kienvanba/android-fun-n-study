package com.horical.gito.mvp.salary.dialog;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.BaseDialogFragment;
import com.horical.gito.mvp.salary.dialog.adapter.SalarySummaryAdapter;
import com.horical.gito.mvp.salary.dto.metric.SummaryDto;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class DialogFragmentSummary extends BaseDialogFragment implements View.OnClickListener {

    @Bind(R.id.tv_cancel)
    TextView tvCancel;

    @Bind(R.id.tv_done)
    TextView tvDone;

    @Bind(R.id.rcv_summary)
    RecyclerView rcvSummary;

    @Bind(R.id.tv_no_data)
    TextView tvNoData;

    private List<SummaryDto> summaries;

    public static DialogFragmentSummary newInstance() {
        Bundle args = new Bundle();
        DialogFragmentSummary fragment = new DialogFragmentSummary();
        fragment.setArguments(args);
        return fragment;
    }

    public List<SummaryDto> getSummaries() {
        if (summaries == null) {
            summaries = new ArrayList<>();
        }
        return summaries;
    }

    public void setSummaries(List<SummaryDto> summaries) {
        this.summaries = summaries;
    }

    @Override
    protected int layoutID() {
        return R.layout.dialog_salary_summary;
    }

    @Override
    protected void initData() {
        SalarySummaryAdapter summaryAdapter = new SalarySummaryAdapter(getActivity(), getSummaries(), new SalarySummaryAdapter.SummaryAdapterListener() {
            @Override
            public void onCheckedChanged(int position) {

            }

            @Override
            public void onRemove(int position) {

            }
        });
        rcvSummary.setAdapter(summaryAdapter);
        rcvSummary.setLayoutManager(new LinearLayoutManager(getActivity()));

        checkEmptyMetricGroup();
    }

    @Override
    protected void initListener() {
        tvCancel.setOnClickListener(this);
        tvDone.setOnClickListener(this);
    }


    private void checkEmptyMetricGroup() {
        if (getSummaries().isEmpty()) {
            tvNoData.setVisibility(View.VISIBLE);
            rcvSummary.setVisibility(View.GONE);
        } else {
            tvNoData.setVisibility(View.GONE);
            rcvSummary.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                break;

            case R.id.tv_done:
                break;
        }
    }
}
