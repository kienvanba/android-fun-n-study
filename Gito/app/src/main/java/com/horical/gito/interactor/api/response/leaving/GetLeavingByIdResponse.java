package com.horical.gito.interactor.api.response.leaving;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Leaving;

import java.util.List;

/**
 * Created by Dragonoid on 3/28/2017.
 */

public class GetLeavingByIdResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    public List<Leaving> leavingsById;
}
