package com.horical.gito.mvp.checkin.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.CheckIn;

import java.util.List;

/**
 * Created by Penguin on 23-Nov-16.
 */

public class CheckinAdapter extends BaseRecyclerAdapter {

    private CheckinAdapter.OnItemClickListener monItemClickListener;

    public CheckinAdapter(List<IRecyclerItem> mItems, Context context) {
        super(mItems, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.item_checkin_dayofweek, parent, false);
        return new CheckinViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        CheckinViewHolder viewHolder = (CheckinViewHolder) holder;
        DayofWeek dayofWeek = (DayofWeek) mItems.get(position);
        CheckIn dofw = dayofWeek.mCheckIn;

        viewHolder.mTvCheckinDay.setText(dofw.createdDate);
        viewHolder.mTvRegisterIn.setText(dofw.registerInHour);
        viewHolder.mTvRegisterOut.setText(dofw.registerOutHour);
        viewHolder.mTvWorkingIn.setText(dofw.checkInHour);
        viewHolder.mTvWorkingOut.setText(dofw.checkOutHour);
    }



    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.monItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}