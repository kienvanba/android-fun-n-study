package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nxon on 3/20/17
 */

public class Summary implements Serializable {

    @SerializedName("progress")
    @Expose
    private List<Progress> progresses;

    @SerializedName("issue")
    @Expose
    private List<Issue> issues;

    public List<Progress> getProgresses() {
        return progresses;
    }

    public void setProgresses(List<Progress> progresses) {
        this.progresses = progresses;
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public void setIssues(List<Issue> issues) {
        this.issues = issues;
    }
}
