package com.horical.gito.mvp.meeting.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.model.Meeting;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MeetingAdapter extends RecyclerView.Adapter<MeetingAdapter.MeetingHolder> {
    private List<Meeting> mList;
    private Context mContext;

    public MeetingAdapter(Context context, List<Meeting> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public MeetingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_list_meeting, parent, false);
        return new MeetingHolder(v);
    }

    @Override
    public void onBindViewHolder(MeetingHolder holder, final int position) {
        Meeting meeting = mList.get(position);
        holder.tvName.setText(meeting.getName());
        holder.tvStartDay.setText(DateUtils.formatDate(meeting.getStartDate(), "dd/MM/yyyy"));
        holder.tvStartTime.setText(DateUtils.formatDate(meeting.getStartDate(), "hh:mm a"));
        holder.tvEndDay.setText(DateUtils.formatDate(meeting.getEndDate(), "dd/MM/yyyy"));
        holder.tvEndTime.setText(DateUtils.formatDate(meeting.getEndDate(), "hh:mm a"));

        holder.mBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.onDelete(position);
            }
        });



        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallBack.onItemClick(position);
            }
        });
    }

    public List<Meeting> getListMeeting() {
        return mList;
    }

    public void setListMeeting(List<Meeting> mList) {
        this.mList = mList;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }


    class MeetingHolder extends RecyclerView.ViewHolder {
        View view;

        @Bind(R.id.name_meeting_tv)
        GitOTextView tvName;

        @Bind(R.id.day_start_tv)
        GitOTextView tvStartDay;

        @Bind(R.id.time_start_tv)
        GitOTextView tvStartTime;

        @Bind(R.id.day_end_tv)
        GitOTextView tvEndDay;

        @Bind(R.id.time_end_tv)
        GitOTextView tvEndTime;

        @Bind(R.id.btn_delete)
        ImageView mBtnDelete;

        @Bind(R.id.imv_meeting_check)
        ImageView mImvCheck;

        public MeetingHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    OnItemClickListener mCallBack;

    public void setOnItemClickListener(OnItemClickListener callBack) {
        this.mCallBack = callBack;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onDelete(int position);
    }
}
