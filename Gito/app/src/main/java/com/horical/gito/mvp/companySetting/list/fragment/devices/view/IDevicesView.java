package com.horical.gito.mvp.companySetting.list.fragment.devices.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IDevicesView extends IView {

    void getAllDeviceSuccess();

    void getAllDeviceFailure(String err);

    void deleteDeviceSuccess(int position);

    void deleteDeviceFailure(String err);
}
