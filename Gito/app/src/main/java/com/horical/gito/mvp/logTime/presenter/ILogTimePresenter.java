package com.horical.gito.mvp.logTime.presenter;

public interface ILogTimePresenter {

    void getMemberData();

    void getLogTimes();

    void getLogTime(int position);

    void approveAndRejectLogTime(int sectionIndex, int itemIndex, String rejectMsg);
}
