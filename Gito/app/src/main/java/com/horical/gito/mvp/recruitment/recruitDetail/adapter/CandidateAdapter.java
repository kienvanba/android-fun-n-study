package com.horical.gito.mvp.recruitment.recruitDetail.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.Candidate;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CandidateAdapter extends RecyclerView.Adapter<CandidateAdapter.ViewHolderCandidate> {
    private final int STATUS_UNINTERVIEWED = 0;
    private final int STATUS_FAIL = 1;
    private final int STATUS_PASS = 2;
    private final int STATUS_ABSENT = 3;
    private Context context;
    private List<Candidate> list;

    public CandidateAdapter(Context context, List<Candidate> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public CandidateAdapter.ViewHolderCandidate onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_user_candidate, parent, false);
        return new ViewHolderCandidate(view);
    }

    @Override
    public void onBindViewHolder(CandidateAdapter.ViewHolderCandidate holder, int position) {

        Candidate item = list.get(position);

        if (item.getAvatar() == null || TextUtils.isEmpty(item.getAvatar().getTokenFile())) {
            holder.tvPhoto.setVisibility(View.VISIBLE);
            holder.imvPhoto.setVisibility(View.GONE);
            holder.tvPhoto.setText(item.getDisplayPhotoName());
        } else {
            holder.imvPhoto.setVisibility(View.VISIBLE);
            holder.tvPhoto.setVisibility(View.GONE);
            ImageLoader.load(context, CommonUtils.getURL(item.getAvatar()), holder.imvPhoto, null);
        }

        holder.tvName.setText(item.getName());
        holder.tvEmail.setText(item.getEmail());
        holder.tvPhone.setText(item.getPhone());

        switch (item.getStatus()) {

        }

        holder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolderCandidate extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_photo)
        TextView tvPhoto;

        @Bind(R.id.imv_photo)
        ImageView imvPhoto;

        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.tv_email)
        TextView tvEmail;

        @Bind(R.id.tv_phone)
        TextView tvPhone;

        @Bind(R.id.imv_delete)
        ImageView imvDelete;

        @Bind(R.id.tv_total_mark)
        TextView tvTotalMark;

        @Bind(R.id.tv_status)
        TextView tvStatus;


        public ViewHolderCandidate(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvTotalMark.setSelected(true);
        }
    }
}
