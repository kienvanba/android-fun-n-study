package com.horical.gito.mvp.checkin.detail.presenter;

import com.horical.gito.model.CheckIn;

/**
 * Created by Penguin on 28-Dec-16.
 */

public interface ICheckin2Presenter {
    void getAllCheckin();

    CheckIn getCheckin(int position);
}
