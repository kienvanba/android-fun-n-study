package com.horical.gito.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by thanhle on 3/21/17.
 */

public class Devices implements Serializable {
    @SerializedName("_id")
    @Expose
    private Id _id;

    @SerializedName("listDevices")
    @Expose
    private List<Device> devices;

    public Id get_id() {
        return _id;
    }

    public void set_id(Id _id) {
        this._id = _id;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }
}
