package com.horical.gito.mvp.myProject.task.details.fragment.comment.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.myProject.task.details.fragment.comment.injection.component.CommentComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.comment.injection.component.DaggerCommentComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.comment.injection.module.CommentModule;
import com.horical.gito.mvp.myProject.task.details.fragment.comment.presenter.CommentPresenter;

import javax.inject.Inject;

import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class CommentFragment extends BaseFragment implements ICommentView, View.OnClickListener{
    public static final String TAG = makeLogTag(CommentFragment.class);

    @Inject
    CommentPresenter mPresenter;

    public static CommentFragment newInstance() {
        Bundle args = new Bundle();
        CommentFragment fragment = new CommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        CommentComponent component = DaggerCommentComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .commentModule(new CommentModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_task_details_comment;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }
}
