package com.horical.gito.mvp.myProject.gantt.adapter;

import android.content.Context;
import android.support.annotation.BinderThread;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.mvp.myProject.gantt.dto.GanttDayDto;
import com.horical.gito.utils.ConvertUtils;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DayAdapter extends BaseRecyclerAdapter {

    public DayAdapter(List<IRecyclerItem> items, Context context) {
        super(items, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(mContext).inflate(R.layout.item_gantt_date, parent, false);
        return new GanttDateHolder(item);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        GanttDateHolder dateHolder = (GanttDateHolder) holder;
        GanttDayDto dayDto = (GanttDayDto) mItems.get(holder.getAdapterPosition());

        dateHolder.mTvMonth.setText(String.valueOf(dayDto.month));
        dateHolder.setDay(dayDto.startDay, dayDto.endDay);
    }

    public class GanttDateHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_month)
        GitOTextView mTvMonth;

        @Bind(R.id.tv_day1)
        GitOTextView mTvDay1;

        @Bind(R.id.tv_day2)
        GitOTextView mTvDay2;

        @Bind(R.id.tv_day3)
        GitOTextView mTvDay3;

        @Bind(R.id.tv_day4)
        GitOTextView mTvDay4;

        @Bind(R.id.tv_day5)
        GitOTextView mTvDay5;

        @Bind(R.id.tv_day6)
        GitOTextView mTvDay6;

        @Bind(R.id.tv_day7)
        GitOTextView mTvDay7;

        public GanttDateHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.getLayoutParams().width = ConvertUtils.convertDpToInt(mContext, 7 * 20);
        }

        public void setDay(int dayStart, int dayEnd) {
            for (int i = dayStart; i <= dayEnd; i++) {
                switch (i - dayStart) {
                    case 0:
                        mTvDay1.setText(String.valueOf(i));
                        break;
                    case 1:
                        mTvDay2.setText(String.valueOf(i));
                        break;
                    case 2:
                        mTvDay3.setText(String.valueOf(i));
                        break;
                    case 3:
                        mTvDay4.setText(String.valueOf(i));
                        break;
                    case 4:
                        mTvDay5.setText(String.valueOf(i));
                        break;
                    case 5:
                        mTvDay6.setText(String.valueOf(i));
                        break;
                    case 6:
                        mTvDay7.setText(String.valueOf(i));
                        break;

                }
            }

        }
    }
}
