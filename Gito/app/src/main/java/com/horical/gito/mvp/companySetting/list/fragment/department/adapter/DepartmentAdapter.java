package com.horical.gito.mvp.companySetting.list.fragment.department.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Department;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 30-Nov-16.
 */
public class DepartmentAdapter extends RecyclerView.Adapter<DepartmentAdapter.DepartmentHolder> {
    private Context mContext;
    private List<Department> mList;
    private OnItemClickListener mOnItemClickListener;

    public DepartmentAdapter(Context context, List<Department> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public DepartmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_company_setting_department, parent, false);
        return new DepartmentHolder(view);
    }

    @Override
    public void onBindViewHolder(DepartmentHolder holder, final int position) {
        Department department = mList.get(position);

        holder.tvTitle.setText(department.getName());
        holder.tvDes.setText(department.getDesc());

        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDelete(position);
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class DepartmentHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_title)
        TextView tvTitle;

        @Bind(R.id.ll_delete)
        LinearLayout llDelete;

        @Bind(R.id.tv_des)
        TextView tvDes;

        View view;

        public DepartmentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);

        void onDelete(int position);
    }
}
