package com.horical.gito.mvp.dialog.dialogAccounting;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CreateAccountingDialog extends Dialog implements View.OnClickListener{

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    private Context mContext;

    public CreateAccountingDialog(Context context) {
        super(context, R.style.FullscreenDialog);
        this.mContext = context;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_accounting);
        ButterKnife.bind(this);

        initData();
        initListener();
    }

    private void initData() {
        topBar.setTextTitle("Add Accounting");
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        topBar.setTextViewRight("Add");
    }

    private void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                dismiss();
            }

            @Override
            public void onRightClicked() {

            }
        });
    }

    @Override
    public void onClick(View v) {

    }
}