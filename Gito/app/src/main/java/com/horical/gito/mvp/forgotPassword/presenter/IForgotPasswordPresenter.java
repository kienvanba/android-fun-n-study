package com.horical.gito.mvp.forgotPassword.presenter;

public interface IForgotPasswordPresenter {

    void requestForgotPassword(String email);

}
