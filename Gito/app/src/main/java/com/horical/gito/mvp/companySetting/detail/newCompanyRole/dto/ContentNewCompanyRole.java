package com.horical.gito.mvp.companySetting.detail.newCompanyRole.dto;

/**
 * Created by thanhle on 3/20/17.
 */

public class ContentNewCompanyRole {
    private int idDrawalble;
    private String title;
    private Boolean isSelection;

    public ContentNewCompanyRole(String title, boolean isSelection) {
        this.title = title;
        this.isSelection = isSelection;
    }

    public int getIdDrawalble() {
        return idDrawalble;
    }

    public void setIdDrawalble(int idDrawalble) {
        this.idDrawalble = idDrawalble;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getSelection() {
        return isSelection;
    }

    public void setSelection(Boolean selection) {
        isSelection = selection;
    }
}
