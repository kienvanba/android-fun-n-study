package com.horical.gito.mvp.companySetting.list.fragment.level.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.companySetting.detail.newLevel.view.NewLevelActivity;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.level.adapter.LevelAdapter;
import com.horical.gito.mvp.companySetting.list.fragment.level.injection.component.DaggerLevelComponent;
import com.horical.gito.mvp.companySetting.list.fragment.level.injection.component.LevelComponent;
import com.horical.gito.mvp.companySetting.list.fragment.level.injection.module.LevelModule;
import com.horical.gito.mvp.companySetting.list.fragment.level.presenter.LevelPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class LevelFragment extends BaseCompanyFragment implements ILevelView {
    public static final String TAG = makeLogTag(LevelFragment.class);
    public final static int REQUEST_CODE_EDIT_LEVEL = 4;
    @Bind(R.id.refresh)
    SwipeRefreshLayout refresh;

    @Bind(R.id.rcv_level)
    RecyclerView rcvLevel;

    private LevelAdapter adapterLevel;

    @Inject
    LevelPresenter mPresenter;

    public static LevelFragment newInstance() {
        Bundle args = new Bundle();
        LevelFragment fragment = new LevelFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_company_setting_level;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        LevelComponent component = DaggerLevelComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .levelModule(new LevelModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        adapterLevel = new LevelAdapter(getContext(), mPresenter.getListLevel());
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvLevel.setLayoutManager(llm);
        rcvLevel.setAdapter(adapterLevel);

        mPresenter.getAllLevel();
    }

    @Override
    protected void initListener() {
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getAllLevel();
            }
        });
        adapterLevel.setOnItemClickListener(new LevelAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(int position) {
                Intent intent = new Intent(getContext(), NewLevelActivity.class);
                intent.putExtra(NewLevelActivity.NEW_LEVEL, mPresenter.getListLevel().get(position));
                getActivity().startActivityForResult(intent, REQUEST_CODE_EDIT_LEVEL);
            }

            @Override
            public void onDelete(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.are_you_sure_for_delete_this, "level"));
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mPresenter.deleteLevel(mPresenter.getListLevel().get(position).getId(), position);
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public void onUpdateData() {
        mPresenter.getAllLevel();
    }

    @Override
    public void getAllLevelSuccess() {
        refresh.setRefreshing(false);
        adapterLevel.notifyDataSetChanged();
    }

    @Override
    public void getAllLevelFailure(String err) {
        refresh.setRefreshing(false);
        adapterLevel.notifyDataSetChanged();
    }

    @Override
    public void deleteLevelSuccess(int position) {
        mPresenter.getListLevel().remove(position);
        adapterLevel.notifyDataSetChanged();
    }

    @Override
    public void deleteLevelFailure(String err) {
        adapterLevel.notifyDataSetChanged();
    }
}
