package com.horical.gito.mvp.salary.list.fragments.subStaff.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.mvp.salary.list.fragments.BaseListSalaryFragment;
import com.horical.gito.mvp.salary.list.fragments.subStaff.adapter.ListStaffAdapter;
import com.horical.gito.mvp.salary.list.fragments.subStaff.injection.component.DaggerIListStaffFragmentComponent;
import com.horical.gito.mvp.salary.list.fragments.subStaff.injection.component.IListStaffFragmentComponent;
import com.horical.gito.mvp.salary.list.fragments.subStaff.injection.module.ListStaffFragmentModule;
import com.horical.gito.mvp.salary.list.fragments.subStaff.presenter.ListStaffFragmentPresenter;

import javax.inject.Inject;

import butterknife.Bind;

public class ListStaffFragment extends BaseListSalaryFragment implements View.OnClickListener,
        IListStaffFragmentView {

    @Bind(R.id.rcv_staff)
    RecyclerView rcvStaff;

    @Inject
    ListStaffFragmentPresenter mPresenter;

    private ListStaffAdapter mAdapter;

    public static ListStaffFragment newInstance() {

        Bundle args = new Bundle();

        ListStaffFragment fragment = new ListStaffFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list_staff;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IListStaffFragmentComponent component = DaggerIListStaffFragmentComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .listStaffFragmentModule(new ListStaffFragmentModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        mAdapter = new ListStaffAdapter(getActivity(), mPresenter.getStaffs(), new ListStaffAdapter.StaffAdapterListener() {
            @Override
            public void onSelected(int position) {

            }
        });
        rcvStaff.setAdapter(mAdapter);
        rcvStaff.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }
}

