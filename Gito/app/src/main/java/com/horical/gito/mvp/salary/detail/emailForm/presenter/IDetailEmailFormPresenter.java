package com.horical.gito.mvp.salary.detail.emailForm.presenter;


import com.horical.gito.mvp.salary.dto.emailForm.EmailFormDto;

public interface IDetailEmailFormPresenter {
    EmailFormDto getEmailForm();
}
