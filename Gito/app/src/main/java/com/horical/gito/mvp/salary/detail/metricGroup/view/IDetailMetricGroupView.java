package com.horical.gito.mvp.salary.detail.metricGroup.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

public interface IDetailMetricGroupView extends IView {
    void requestGetAllMetricSuccess();

    void requestGetAllMetricError(RestError error);

    void requestDeleteMetricSuccess();

    void requestDeleteMetricError(RestError error);

    void showLoading();

    void hideLoading();
}
