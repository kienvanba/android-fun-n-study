package com.horical.gito.mvp.salary.bonus.list.fragments.approved.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.salary.inputAddon.FilterInputAddonRequest;
import com.horical.gito.interactor.api.response.salary.bonus.GetFilterInputAddonResponse;
import com.horical.gito.model.InputAddon;
import com.horical.gito.mvp.salary.bonus.list.fragments.approved.view.IListApprovedFragmentView;

import java.util.ArrayList;
import java.util.List;

public class ListApprovedFragmentPresenter extends BasePresenter implements IApprovedFragmentPresenter {

    public void attachView(IListApprovedFragmentView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IListApprovedFragmentView getView() {
        return (IListApprovedFragmentView) getIView();
    }

    private List<InputAddon> inputAddons;

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public List<InputAddon> getListInputAddon() {
        if (inputAddons == null) {
            inputAddons = new ArrayList<>();
        }
        return inputAddons;
    }

    @Override
    public void requestGetApprovedInputAddon() {
        FilterInputAddonRequest filterInputAddonRequest = new FilterInputAddonRequest(true, false, false);
        getApiManager().filterInputAddon(filterInputAddonRequest, new ApiCallback<GetFilterInputAddonResponse>() {
            @Override
            public void success(GetFilterInputAddonResponse res) {
                getListInputAddon().clear();
                getListInputAddon().addAll(res.getAddons());
                getView().requestGetApprovedInputAddonSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().requestGetApprovedInputAddonError(error);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}