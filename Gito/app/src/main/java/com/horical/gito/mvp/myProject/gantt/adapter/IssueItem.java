package com.horical.gito.mvp.myProject.gantt.adapter;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.mvp.myProject.gantt.dto.GanttIssueDto;

/**
 * Created by hoangsang on 3/6/17.
 */

public class IssueItem implements IRecyclerItem {

    public GanttIssueDto issue;

    public IssueItem() {
    }

    public IssueItem(GanttIssueDto issue) {
        this.issue = issue;
    }

    @Override
    public int getItemViewType() {
        return 0;
    }
}
