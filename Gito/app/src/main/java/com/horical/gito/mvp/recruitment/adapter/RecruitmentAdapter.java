package com.horical.gito.mvp.recruitment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Recruitment;
import com.horical.gito.utils.DateUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RecruitmentAdapter extends RecyclerView.Adapter<RecruitmentAdapter.ViewHolderRecruitment> {

    private Context context;
    private List<Recruitment> list;

    public RecruitmentAdapter(Context context, List<Recruitment> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecruitmentAdapter.ViewHolderRecruitment onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_recruitment, parent, false);
        return new ViewHolderRecruitment(view);
    }

    @Override
    public void onBindViewHolder(RecruitmentAdapter.ViewHolderRecruitment holder, final int position) {
        holder.tvTitle.setText(list.get(position).getTitle());
        holder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallBack != null) {
                    mCallBack.onDelete(position);
                }
            }
        });
        holder.tvCandidate.setText(String.valueOf(list.get(position).getCandidates().size()));
        holder.tvInterviewDate.setText(DateUtils.formatDate(list.get(position).getInterviewDate()));

        holder.tvTitle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallBack != null) {
                    mCallBack.onItemClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolderRecruitment extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_title)
        TextView tvTitle;

        @Bind(R.id.imv_delete)
        ImageView imvDelete;

        @Bind(R.id.tv_candidate)
        TextView tvCandidate;

        @Bind(R.id.tv_interview_date)
        TextView tvInterviewDate;

        public ViewHolderRecruitment(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private OnItemClickListener mCallBack;

    public void setOnItemClickListener(OnItemClickListener callBack) {
        this.mCallBack = callBack;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDelete(int position);
    }
}

