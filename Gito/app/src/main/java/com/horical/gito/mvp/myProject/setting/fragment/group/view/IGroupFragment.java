package com.horical.gito.mvp.myProject.setting.fragment.group.view;

import com.horical.gito.base.IView;

public interface IGroupFragment extends IView {
    void getAllGroupSuccess();
    void showLoading();
    void hideLoading();
}
