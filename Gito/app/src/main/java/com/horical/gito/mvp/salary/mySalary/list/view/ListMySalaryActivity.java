package com.horical.gito.mvp.salary.mySalary.list.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.mvp.salary.mySalary.detail.view.DetailMySalaryActivity;
import com.horical.gito.mvp.salary.mySalary.list.adapter.ListMySalaryAdapter;
import com.horical.gito.mvp.salary.mySalary.list.injection.component.DaggerIListMySalaryComponent;
import com.horical.gito.mvp.salary.mySalary.list.injection.component.IListMySalaryComponent;
import com.horical.gito.mvp.salary.mySalary.list.injection.module.ListMySalaryModule;
import com.horical.gito.mvp.salary.mySalary.list.presenter.ListMySalaryPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListMySalaryActivity extends BaseActivity implements View.OnClickListener,
        IListMySalaryView {

    @Bind(R.id.rcv_my_salary)
    RecyclerView rcvMySalary;

    @Inject
    ListMySalaryPresenter mPresenter;

    private ListMySalaryAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_my_salary);
        ButterKnife.bind(this);

        IListMySalaryComponent component = DaggerIListMySalaryComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .listMySalaryModule(new ListMySalaryModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        mAdapter = new ListMySalaryAdapter(this, mPresenter.getMySalaries(), new ListMySalaryAdapter.ListMySalaryAdapterListener() {
            @Override
            public void onSelected(int position) {
                Intent detailMySalary = new Intent(ListMySalaryActivity.this, DetailMySalaryActivity.class);
                startActivity(detailMySalary);
            }
        });
        rcvMySalary.setAdapter(mAdapter);
        rcvMySalary.setLayoutManager(new LinearLayoutManager(this));
    }

    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}

