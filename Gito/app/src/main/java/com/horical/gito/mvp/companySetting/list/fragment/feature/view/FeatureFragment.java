package com.horical.gito.mvp.companySetting.list.fragment.feature.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.LoginFilter;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.feature.adapter.FeatureAdapter;
import com.horical.gito.mvp.companySetting.list.fragment.feature.injection.component.DaggerFeatureComponent;
import com.horical.gito.mvp.companySetting.list.fragment.feature.injection.component.FeatureComponent;
import com.horical.gito.mvp.companySetting.list.fragment.feature.injection.module.FeatureModule;
import com.horical.gito.mvp.companySetting.list.fragment.feature.presenter.FeaturePresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class FeatureFragment extends BaseCompanyFragment implements IFeatureView, View.OnClickListener {
    public static final String TAG = makeLogTag(FeatureFragment.class);

    @Bind(R.id.refresh)
    SwipeRefreshLayout refresh;

    @Bind(R.id.rcv_menu)
    RecyclerView rcvMenu;

    private FeatureAdapter adapter;

    @Inject
    FeaturePresenter mPresenter;

    public static FeatureFragment newInstance() {
        Bundle args = new Bundle();
        FeatureFragment fragment = new FeatureFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_company_setting_feature;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        FeatureComponent component = DaggerFeatureComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .featureModule(new FeatureModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        adapter = new FeatureAdapter(getContext(), mPresenter.getMenuFeatures());
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvMenu.setLayoutManager(llm);
        rcvMenu.setAdapter(adapter);

        mPresenter.getDataFeature();
    }

    @Override
    protected void initListener() {
        adapter.setOnItemClickListener(new FeatureAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(int position) {
                mPresenter.getCurrentDataFeature();
                mPresenter.updateFeature(mPresenter.getFeature());
            }
        });
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getDataFeature();
            }
        });
    }

    @Override
    public void onClick(View view) {

    }
    @Override
    public void getFeatureSuccess() {
        refresh.setRefreshing(false);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void getFeatureFailure(String err) {
        refresh.setRefreshing(false);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void updateFeatureSuccess() {
        adapter.notifyDataSetChanged();
        Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateFeatureFailure(String err) {
        Toast.makeText(getContext(), "Failure", Toast.LENGTH_SHORT).show();
    }
}
