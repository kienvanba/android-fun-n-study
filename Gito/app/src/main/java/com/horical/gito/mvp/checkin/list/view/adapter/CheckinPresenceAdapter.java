package com.horical.gito.mvp.checkin.list.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Penguin on 20-Dec-16.
 */

public class CheckinPresenceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyRecyclerHeadersAdapter {

    private Context Icontext;
    private List<UserProfile> mList;
    private CheckinPresenceAdapter.OnItemClickListener mOnItemClickListener;

    public CheckinPresenceAdapter(Context context, List<UserProfile> list) {
        Icontext = context;
        mList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_checkin_presence, parent, false);
        viewHolder = new CheckinPresenceAdapter.CheckinViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserProfile userProfile = mList.get(position);
        CheckinPresenceAdapter.CheckinViewHolder viewHolder = (CheckinPresenceAdapter.CheckinViewHolder) holder;
        viewHolder.mIvAvatar.setImageResource(userProfile.Avatar);
        viewHolder.mTvName.setText(userProfile.UserName);
        viewHolder.mTvMajor.setText(userProfile.Major);
        viewHolder.mBtSun.setBackgroundResource(userProfile.Sun);
        viewHolder.mBtMon.setBackgroundResource(userProfile.Mon);
        viewHolder.mBtTue.setBackgroundResource(userProfile.Tue);
        viewHolder.mBtWed.setBackgroundResource(userProfile.Wed);
        viewHolder.mBtThu.setBackgroundResource(userProfile.Thu);
        viewHolder.mBtFri.setBackgroundResource(userProfile.Fri);
        viewHolder.mBtSat.setBackgroundResource(userProfile.Sat);

    }

    @Override
    public long getHeaderId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class CheckinViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.iv_filter_avatar)
        ImageView mIvAvatar;

        @Bind(R.id.tv_filter_name)
        TextView mTvName;

        @Bind(R.id.tv_filter_major)
        TextView mTvMajor;

        @Bind(R.id.btn1)
        Button mBtSun;

        @Bind(R.id.btn2)
        Button mBtMon;

        @Bind(R.id.btn3)
        Button mBtTue;

        @Bind(R.id.btn4)
        Button mBtWed;

        @Bind(R.id.btn5)
        Button mBtThu;

        @Bind(R.id.btn6)
        Button mBtFri;

        @Bind(R.id.btn7)
        Button mBtSat;

        public CheckinViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Calendar cal = Calendar.getInstance();


        }

    }

    public void setOnItemClickListener(CheckinPresenceAdapter.OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);
    }
}

