package com.horical.gito.mvp.report.objectDto;


import android.text.TextUtils;

import com.horical.gito.utils.DateUtils;

import java.util.Date;

public class ContentDto {
    private Date date;
    private String data;

    public ContentDto(Date date, String data) {
        this.date = date;
        this.data = data;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String showDate() {
        return DateUtils.formatDate(date);
    }

    public String showData() {
        if (TextUtils.isEmpty(data)) {
            return "";
        }
        return data;
    }
}
