package com.horical.gito.mvp.myProject.setting.fragment.recycler.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.mvp.myProject.setting.adapter.SettingRecyclerAdapter;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.injection.component.DaggerRecyclerComponent;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.injection.component.RecyclerComponent;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.injection.module.RecyclerModule;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.presenter.RecyclerPresenter;
import com.horical.gito.widget.recycleview.VerticalDividerItemDecoration;


import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;

public abstract class BaseRecyclerFragment extends BaseFragment implements IRecyclerFragment {
    @Bind(R.id.rcv_content)
    protected RecyclerView mRvSettings;
    @Bind(R.id.swipeRefresh)
    SwipeRefreshLayout mSRefresh;

    @Inject
    protected RecyclerPresenter mPresenter;

    protected List<IRecyclerItem> mItems;
    protected SettingRecyclerAdapter mSettingAdapter;
    protected RecyclerView.LayoutManager layoutManager;
    protected boolean dividerVisibility = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RecyclerComponent component = DaggerRecyclerComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .recyclerModule(new RecyclerModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initListener();
    }

    @Override
    protected void initListener(){
        mSettingAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                setOnItemClickListener(position);
                mSettingAdapter.notifyDataSetChanged();
            }
        });
        mSettingAdapter.setOnDeleteItemClickListener(new SettingRecyclerAdapter.OnDeleteItemClickListener() {
            @Override
            public void onClick(int position) {
                setOnDeleteItemClickListener(position);
            }
        });
        mSRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestData();
            }
        });
    }

    @Override
    protected void initData(){
        mItems = mPresenter.getRecyclerItems();
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mSettingAdapter = new SettingRecyclerAdapter(getContext(), mItems);
        mRvSettings.setLayoutManager(layoutManager);
        mRvSettings.setAdapter(mSettingAdapter);
        mRvSettings.setItemAnimator(new DefaultItemAnimator());

        requestData();
        VerticalDividerItemDecoration decoration = new VerticalDividerItemDecoration(getContext(), dividerVisibility);
        mRvSettings.addItemDecoration(decoration);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_setting;
    }

    @Override
    public void getAllRecyclerSuccess() {
        mSettingAdapter.notifyDataSetChanged();
        mSRefresh.setRefreshing(false);
    }

    @Override
    public void deleteItemSuccess(int position) {
        mItems.remove(position);
        mSettingAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void showToast(String mess) {
        Toast.makeText(getContext(), mess, Toast.LENGTH_SHORT).show();
    }

    protected abstract void setOnItemClickListener(int position);

    protected abstract void setOnDeleteItemClickListener(int position);

    public abstract void setOnAddItemClickListener();

    protected abstract void requestData();

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
