package com.horical.gito.interactor.api.response.companySetting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Level;

/**
 * Created by thanhle on 3/29/17.
 */

public class GetUpdateLevelResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public String result;
}
