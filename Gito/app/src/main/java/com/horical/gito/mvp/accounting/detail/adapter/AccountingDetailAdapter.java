package com.horical.gito.mvp.accounting.detail.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.horical.gito.R;
import com.horical.gito.model.Accounting;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Luong on 07-Mar-17.
 */

public class AccountingDetailAdapter extends RecyclerView.Adapter<AccountingDetailAdapter.AccountingHolder> {

    private List<Accounting> mList;
    private Context mContext;


    public AccountingDetailAdapter(Context context, List<Accounting> list) {
        this.mContext = context;
        this.mList = list;

    }

    @Override
    public AccountingDetailAdapter.AccountingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_accounting_detail, parent, false);
        return new AccountingDetailAdapter.AccountingHolder(view);
    }

    @Override
    public void onBindViewHolder(AccountingDetailAdapter.AccountingHolder holder, final int position) {

        Accounting accounting = mList.get(position);

//        holder.tvType.setText(String.format(String.valueOf(accounting.type)));
//        holder.tvAmount.setText(String.format(String.valueOf(accounting.amount)));
//        holder.tvAmountSubtitle.setText(accounting.currency);
//        holder.tvCurrency.setText(accounting.currency);
//        holder.tvName.setText(accounting.displayName);
//        holder.tvAvatarDesc.setText(String.format(String.valueOf(accounting.size)));
//        holder.tvContent.setText(accounting.desc);
//        holder.tvDate.setText(DateUtils.formatDate(accounting.getCreatedDate(), "dd/MM/yyyy hh:mm a"));
//        holder.tvProject.setText((CharSequence) accounting.getName());
    }


    public List<Accounting> getListAccounting() {
        return mList;
    }

    public void setListAccounting(List<Accounting> mList) {
        this.mList = mList;
    }


    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    class AccountingHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_accounting_type)
        GitOTextView tvType;

        @Bind(R.id.tv_accounting_amount)
        GitOTextView tvAmount;

        @Bind(R.id.tv_accounting_amount_subtitle)
        GitOTextView tvAmountSubtitle;

        @Bind(R.id.tv_accounting_currency)
        GitOTextView tvCurrency;

        @Bind(R.id.civ_accounting_avatar)
        CircleImageView civAvatar;

        @Bind(R.id.tv_accounting_name)
        GitOTextView tvName;

        @Bind(R.id.tv_accounting_avatar_desc)
        GitOTextView tvAvatarDesc;

        @Bind(R.id.tv_accounting_content)
        GitOTextView tvContent;

        @Bind(R.id.tv_accounting_date)
        GitOTextView tvDate;

        @Bind(R.id.tv_accounting_project)
        GitOTextView tvProject;

        @Bind(R.id.imv_accounting_type)
        ImageView ivAccountingType;

        @Bind(R.id.imv_accounting_amount)
        ImageView ivAccountingAmount;

        @Bind(R.id.imv_accounting_currency)
        ImageView ivAccountingCurrency;

        @Bind(R.id.imv_accounting_target_user)
        ImageView ivAccountingTargetUser;

        @Bind(R.id.imv_accounting_content)
        ImageView ivAccountingContent;

        @Bind(R.id.rcv_accounting_attatch_file)
        RecyclerView rcvAccountingAttatchFile;

        @Bind(R.id.imv_accounting_date)
        ImageView ivAccountingDate;

        @Bind(R.id.imv_accounting_project)
        ImageView ivAccountingProject;

        View view;


        public AccountingHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public class option extends AppCompatActivity {

        @Bind(R.id.option_accounting_cash)
        Spinner optionCash;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.item_accounting_detail);
            ButterKnife.bind(this);

            List<String> list = new ArrayList<>();
            list.add("Cash");
            list.add("Bank");
            list.add("Cheque");

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_accounting_spinner, list);
            adapter.setDropDownViewResource(R.layout.item_accounting_spinner_dropdown);
            optionCash.setAdapter(adapter);
            optionCash.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

    }

    OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener callBack) {
        this.mOnItemClickListener = callBack;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDelete(int position);
    }
}
