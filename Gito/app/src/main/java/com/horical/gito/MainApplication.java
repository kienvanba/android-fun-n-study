package com.horical.gito;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.IntentCompat;

import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.injection.component.DaggerApplicationComponent;
import com.horical.gito.injection.module.ApiModule;
import com.horical.gito.injection.module.ApplicationModule;
import com.horical.gito.injection.module.EventModule;
import com.horical.gito.injection.module.NetworkModule;
import com.horical.gito.injection.module.PreferModule;
import com.horical.gito.injection.module.SocketModule;
import com.horical.gito.interactor.event.EventManager;
import com.horical.gito.interactor.event.type.InvalidTokenType;
import com.horical.gito.interactor.prefer.PreferManager;
import com.horical.gito.mvp.intro.view.IntroActivity;

import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

public class MainApplication extends Application {

    static ApplicationComponent appComponent;

    public static Context mContext;

    @Inject
    EventManager eventManager;

    @Inject
    PreferManager preferManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();

        if (appComponent == null) {
            appComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .apiModule(new ApiModule())
                    .networkModule(new NetworkModule())
                    .eventModule(new EventModule())
                    .preferModule(new PreferModule())
                    .socketModule(new SocketModule())
                    .build();
        }
        getAppComponent().inject(this);

        eventManager.register(this);
    }

    @Override
    public void onTerminate() {
        eventManager.unRegister(this);
        super.onTerminate();
    }

    public static ApplicationComponent getAppComponent() {
        return appComponent;
    }

    @Subscribe
    public void onEventInvalidToken(InvalidTokenType type) {
        preferManager.resetUser();
        GitOStorage.getInstance().setProject(null);
        Intent intent = new Intent(mContext, IntroActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}