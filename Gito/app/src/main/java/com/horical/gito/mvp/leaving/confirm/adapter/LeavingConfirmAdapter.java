package com.horical.gito.mvp.leaving.confirm.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.widget.recycleview.VerticalDividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 1/17/2017.
 */

public class LeavingConfirmAdapter extends BaseRecyclerAdapter {

    private ApproveRejectFromChild mCallback;

    public LeavingConfirmAdapter(List<IRecyclerItem> items, Context context, ApproveRejectFromChild callback) {
        super(items, context);
        mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leaving_confirm_date, parent, false);
        return new LeavingConfirmHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        LeavingConfirmHolder detailHolder = (LeavingConfirmHolder) holder;
        LeavingConfirmItem detailItem = (LeavingConfirmItem) mItems.get(position);
        detailHolder.mDate.setText(detailItem.mDateString);

        LeavingConfirmDetailAdapter mAdapterDetail = new LeavingConfirmDetailAdapter(detailItem.getList(), mContext, new LeavingConfirmDetailAdapter.ApproveRejectFromChildView() {
            @Override
            public void isEmptyChildList() {
                mCallback.isEmptyChildList(position);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        detailHolder.mListDetail.setAdapter(mAdapterDetail);
        detailHolder.mListDetail.setLayoutManager(layoutManager);
        detailHolder.mListDetail.setHasFixedSize(true);
        detailHolder.mListDetail.addItemDecoration(new VerticalDividerItemDecoration(mContext, R.drawable.bg_leaving_divider));
    }

    public interface ApproveRejectFromChild {
        void isEmptyChildList(int position);
    }

    static class LeavingConfirmHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.leaving_tv_confirm_date)
        TextView mDate;

        @Bind(R.id.leaving_lv_confirm_detail)
        RecyclerView mListDetail;

        public LeavingConfirmHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
