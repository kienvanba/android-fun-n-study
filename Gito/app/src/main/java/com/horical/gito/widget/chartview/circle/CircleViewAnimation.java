package com.horical.gito.widget.chartview.circle;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by Tin on 29-Dec-16.
 */

public class CircleViewAnimation extends Animation {

    private CircleView circleView;
    private float newAngle;
    private float newPercent;

    public CircleViewAnimation(CircleView circleView) {
        this.newAngle = (float) (circleView.getPercent() * 3.6);
        this.newPercent = circleView.getPercent();
        this.circleView = circleView;
    }


    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);

        float angle = (newAngle * interpolatedTime);
        float percent = (newPercent * interpolatedTime);

        circleView.setAngleDraw(angle);
        circleView.setPercentDraw(percent);
        circleView.requestLayout();
    }
}
