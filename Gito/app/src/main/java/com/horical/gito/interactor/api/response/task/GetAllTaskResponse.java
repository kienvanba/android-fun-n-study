package com.horical.gito.interactor.api.response.task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Task;

import java.util.List;

public class GetAllTaskResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Task> tasks;

}
