package com.horical.gito.mvp.myProject.sourceCode.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.injection.module.SourceCodeDetailModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.view.SourceCodeDetailActivity;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class , modules = SourceCodeDetailModule.class)
public interface SourceCodeDetailComponent {
    void inject(SourceCodeDetailActivity activity);
}
