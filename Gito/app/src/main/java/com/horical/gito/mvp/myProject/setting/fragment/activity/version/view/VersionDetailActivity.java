package com.horical.gito.mvp.myProject.setting.fragment.activity.version.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.interactor.api.request.version.Request;
import com.horical.gito.model.Version;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.injection.component.VersionComponent;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.injection.module.VersionModule;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.presenter.VersionPresenter;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.injection.component.DaggerVersionComponent;
import com.horical.gito.utils.HDate;
import com.horical.gito.widget.chartview.line.TimeLineView;
import com.horical.gito.widget.dialog.DialogEditText;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VersionDetailActivity extends BaseActivity implements IVersionView, View.OnClickListener {
    @Bind(R.id.edit_name)
    TextView tvName;
    @Bind(R.id.tv_status)
    TextView tvStatus;
    @Bind(R.id.edit_description)
    TextView tvDescription;
    @Bind(R.id.edit_release_note)
    TextView tvReleaseNote;

    @Bind(R.id.top_bar)
    CustomViewTopBar topBar;
    @Bind(R.id.btn_edit_name)
    ImageButton btnNameEdit;
    @Bind(R.id.btn_edit_desc)
    ImageButton btnDescriptionEdit;
    @Bind(R.id.btn_edit_release_note)
    ImageButton btnReleaseNoteEdit;
    @Bind(R.id.btn_add_file)
    ImageButton btnFileAdd;
    @Bind(R.id.time_line)
    TimeLineView timeLine;
    @Bind(R.id.rcv_file_release)
    RecyclerView rcvFiles;

    @Inject
    VersionPresenter mPresenter;

    private Version mVersion;
    private Request request;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version_detail);
        ButterKnife.bind(this);

        VersionComponent component = DaggerVersionComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .versionModule(new VersionModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        String versionId = getIntent().getStringExtra("version_id");
        mPresenter.getVersion(versionId);

        request = new Request();

        setupTopBar();
    }

    private void setupTopBar() {
        topBar.setTextTitle(getResources().getString(R.string.version_detail));
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

            }
        });
        btnNameEdit.setOnClickListener(this);
        btnDescriptionEdit.setOnClickListener(this);
        btnReleaseNoteEdit.setOnClickListener(this);
        btnFileAdd.setOnClickListener(this);
        timeLine.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_edit_name:
                DialogEditText dialog = new DialogEditText(this) {
                    @Override
                    public void setSaveButtonClickListener() {
                        request.name = this.editText.getText().toString();
                        mPresenter.updateVersion(mVersion.get_id(), request);
                        this.dismiss();
                    }
                };
                dialog.setHint(getString(R.string.hint_name));
                dialog.editText.setText(mVersion.getName());
                dialog.show();
                break;
            case R.id.btn_edit_desc:
                dialog = new DialogEditText(this) {
                    @Override
                    public void setSaveButtonClickListener() {
                        request.desc = this.editText.getText().toString();
                        mPresenter.updateVersion(mVersion.get_id(), request);
                        this.dismiss();
                    }
                };
                dialog.setHint(getString(R.string.hint_description));
                dialog.editText.setText(mVersion.getDescription());
                dialog.show();
                break;
            case R.id.btn_edit_release_note:
                dialog = new DialogEditText(this) {
                    @Override
                    public void setSaveButtonClickListener() {
                        request.releaseNote = this.editText.getText().toString();
                        mPresenter.updateVersion(mVersion.get_id(), request);
                        dismiss();
                    }
                };
                dialog.setHint(getString(R.string.hint_note));
                dialog.editText.setText(mVersion.getReleaseNote());
                dialog.show();
                break;
            case R.id.btn_add_file:
                //
                break;
            case R.id.time_line:
                final DatePickerDialog pickerDialog = new DatePickerDialog(this);
                Calendar cs = Calendar.getInstance();
                cs.setTime(mVersion.getStartDate());
                pickerDialog.dpStart.updateDate(
                        cs.get(Calendar.YEAR),
                        cs.get(Calendar.MONTH),
                        cs.get(Calendar.DAY_OF_MONTH)
                );
                cs.setTime(mVersion.getEndDate());
                pickerDialog.dpEnd.updateDate(
                        cs.get(Calendar.YEAR),
                        cs.get(Calendar.MONTH),
                        cs.get(Calendar.DAY_OF_MONTH)
                );
                pickerDialog.setSaveClickListener(new DatePickerDialog.OnSaveClickListener() {
                    @Override
                    public void onClick() {
                        Calendar date = Calendar.getInstance();
                        /** start date */
                        date.set(
                                pickerDialog.dpStart.getYear(),
                                pickerDialog.dpStart.getMonth(),
                                pickerDialog.dpStart.getDayOfMonth()
                        );
                        timeLine.setDateStart(new HDate(date));
                        request.startDate = date.getTime();
                        /** end date */
                        date.set(
                                pickerDialog.dpEnd.getYear(),
                                pickerDialog.dpEnd.getMonth(),
                                pickerDialog.dpEnd.getDayOfMonth()
                        );
                        timeLine.setDateRelease(new HDate(date));
                        request.endDate = date.getTime();
                        /** validate function */
                        timeLine.invalidate();
                        mPresenter.updateVersion(mVersion.get_id(), request);
                        Log.e("version detail", request.startDate.toString() + " end date " + request.endDate.toString());
                        pickerDialog.dismiss();
                    }
                });
                pickerDialog.show();
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void onRequestSuccess() {
        mVersion = mPresenter.getVersion();
        tvName.setText(mVersion.getName());
        tvDescription.setText(mVersion.getDescription());
        tvReleaseNote.setText(mVersion.getReleaseNote());

        switch (mVersion.getStatus()) {
            case 1:
                tvStatus.setText(getResources().getString(R.string.approved));
                tvStatus.setTextColor(getResources().getColor(R.color.green));
                break;
            case 0:
                tvStatus.setText(getString(R.string.status_new));
                tvStatus.setTextColor(getResources().getColor(R.color.orange_new));
                break;
            default:
                tvStatus.setText(String.valueOf(mVersion.getStatus()));
                break;
        }

        Calendar timeLineDate = Calendar.getInstance();
        timeLineDate.setTime(mVersion.getStartDate());
        timeLine.setDateStart(new HDate(timeLineDate));

        timeLineDate.setTime(mVersion.getEndDate());
        timeLine.setDateRelease(new HDate(timeLineDate));
    }
}
