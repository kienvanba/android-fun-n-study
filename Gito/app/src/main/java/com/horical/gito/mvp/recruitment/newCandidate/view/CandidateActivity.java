package com.horical.gito.mvp.recruitment.newCandidate.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.mvp.recruitment.newCandidate.injection.component.CandidateComponent;
import com.horical.gito.mvp.recruitment.newCandidate.injection.component.DaggerCandidateComponent;
import com.horical.gito.mvp.recruitment.newCandidate.injection.module.CandidateModule;
import com.horical.gito.mvp.recruitment.newCandidate.presenter.CandidatePresenter;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class CandidateActivity extends BaseActivity implements ICandidateView, View.OnClickListener {


    @Inject
    CandidatePresenter mPresenter;

    private int getLayoutId() {
        return R.layout.activity_new_recruitment;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);

        CandidateComponent component = DaggerCandidateComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .candidateModule(new CandidateModule())
                .build();

        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {

    }

    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }


    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void dismissLoading() {
        dismissDialog();
    }
}