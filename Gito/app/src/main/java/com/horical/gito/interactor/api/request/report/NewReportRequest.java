package com.horical.gito.interactor.api.request.report;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.model.InformTo;

import java.util.Date;
import java.util.List;

public class NewReportRequest {

    public NewReportRequest(Date createdDate, String doing, String problem
            , String finished, List<String> listReportTo, InFormToParam informTo, int status) {
        this.createdDate = createdDate;
        this.doing = doing;
        this.problem = problem;
        this.finished = finished;
        this.listReportTo = listReportTo;
        this.informTo = informTo;
        this.status = status;
    }

    @SerializedName("createdDate")
    @Expose
    public Date createdDate;

    @SerializedName("problem")
    @Expose
    public String problem;

    @SerializedName("doing")
    @Expose
    public String doing;

    @SerializedName("finished")
    @Expose
    public String finished;

    @SerializedName("reportTo")
    @Expose
    public List<String> listReportTo;

    @SerializedName("informTo")
    @Expose
    public InFormToParam informTo;

    @SerializedName("status")
    @Expose
    public int status;

}
