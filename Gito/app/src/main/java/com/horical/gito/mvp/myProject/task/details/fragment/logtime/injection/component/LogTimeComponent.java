package com.horical.gito.mvp.myProject.task.details.fragment.logtime.injection.component;


import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.logtime.injection.module.LogTimeModule;
import com.horical.gito.mvp.myProject.task.details.fragment.logtime.view.LogTimeFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = LogTimeModule.class)
public interface LogTimeComponent {
    void inject(LogTimeFragment fragment);
}

