package com.horical.gito.mvp.myProject.setting.fragment.activity.member.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.User;

public class UserItemBody implements IRecyclerItem{
    private User user;
    private boolean isChecked;

    public UserItemBody(User user){
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isChecked(){
        return this.isChecked;
    }
    public void setChecked(boolean isChecked){
        this.isChecked = isChecked;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.USER_ITEM;
    }
}
