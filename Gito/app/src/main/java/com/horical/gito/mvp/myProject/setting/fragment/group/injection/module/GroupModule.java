package com.horical.gito.mvp.myProject.setting.fragment.group.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.setting.fragment.group.presenter.GroupPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class GroupModule {
    @PerFragment
    @Provides
    GroupPresenter provideGroupPresenter(){
        return new GroupPresenter();
    }
}
