package com.horical.gito.mvp.checkin.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.checkin.detail.presenter.Checkin2Presenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Penguin on 28-Dec-16.
 */

@Module
public class Checkin2Module {
    @PerActivity
    @Provides
    Checkin2Presenter providesCheckinPresenter(){
        return new Checkin2Presenter();
    }
}