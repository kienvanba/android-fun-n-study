package com.horical.gito.mvp.roomBooking.detail.fragment.month.view;

import android.os.Bundle;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.roomBooking.detail.fragment.month.injection.component.DaggerMonthComponent;
import com.horical.gito.mvp.roomBooking.detail.fragment.month.injection.component.MonthComponent;
import com.horical.gito.mvp.roomBooking.detail.fragment.month.injection.module.MonthModule;
import com.horical.gito.mvp.roomBooking.detail.fragment.month.presenter.MonthPresenter;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by QuangHai on 08/11/2016.
 */
@PerActivity
public class MonthFragment extends BaseFragment implements IMonthView, View.OnClickListener {

    @Inject
    MonthPresenter mPresenter;

    public static MonthFragment newInstance() {
        Bundle args = new Bundle();
        MonthFragment fragment = new MonthFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        MonthComponent component = DaggerMonthComponent.builder()
                .monthModule(new MonthModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_month;
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View v) {

    }
}
