package com.horical.gito.mvp.survey.list.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Survey;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.STATUS_APPROVED;
import static com.horical.gito.AppConstants.STATUS_NEW;
import static com.horical.gito.AppConstants.STATUS_REJECTED;
import static com.horical.gito.AppConstants.STATUS_SEND;
import static com.horical.gito.AppConstants.STATUS_SUMMITTED;
import static com.horical.gito.AppConstants.TYPE_CROSS;
import static com.horical.gito.AppConstants.TYPE_NORMAL;

public class SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.SurveyHolder> {
    private List<Survey> mList;
    private Context mContext;

    public SurveyAdapter(Context mContext, List<Survey> mList) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public SurveyAdapter.SurveyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_survey_header, parent, false);
        return new SurveyHolder(view);
    }


    @Override
    public void onBindViewHolder(SurveyAdapter.SurveyHolder holder, final int position) {
        Survey survey = mList.get(position);

        holder.tvName.setText(survey.getName());
        holder.tvItemCount.setText(String.valueOf(survey.getItemCount()) + " item");

        if (survey.getType() == TYPE_NORMAL) {
            holder.tvType.setText(R.string.normal);
            holder.tvType.setTextColor(ContextCompat.getColor(mContext, R.color.black_opacity));
        }
        if (survey.getType() == TYPE_CROSS){
            holder.tvType.setText(R.string.cross);
            holder.tvType.setTextColor(ContextCompat.getColor(mContext, R.color.blue_cross));
        }

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMarginEnd(20);
        switch (survey.getStatus()) {
            case STATUS_SUMMITTED:
                holder.ivIconApproved.setVisibility(View.GONE);
                holder.ivDelete.setVisibility(View.GONE);
                holder.llStatus.setLayoutParams(lp);
                holder.tvStatus.setText(R.string.status_submitted);
                holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green_doing));
                break;
            case STATUS_NEW:
                holder.ivIconApproved.setVisibility(View.GONE);
                holder.ivDelete.setVisibility(View.GONE);
                holder.llStatus.setLayoutParams(lp);
                holder.tvStatus.setText(R.string.status_new);
                holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                break;
            case STATUS_APPROVED:
                holder.ivIconApproved.setVisibility(View.VISIBLE);
                holder.ivDelete.setVisibility(View.VISIBLE);
                holder.tvStatus.setText(R.string.status_approved);
                holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.blue_cross));
                break;
            case STATUS_REJECTED:
                holder.ivIconApproved.setVisibility(View.GONE);
                holder.ivDelete.setVisibility(View.VISIBLE);
                holder.tvStatus.setText(R.string.status_rejected);
                holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red_reject));
                break;
            case STATUS_SEND:
                holder.ivIconApproved.setVisibility(View.GONE);
                holder.ivDelete.setVisibility(View.GONE);
                holder.llStatus.setLayoutParams(lp);
                holder.tvStatus.setText(R.string.status_send);
                holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.app_color));
                break;
        }

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickListener(position);
            }
        });
    }

    public List<Survey> getListSurvey() {
        return mList;
    }

    public void setListSurvey(List<Survey> mList) {
        this.mList = mList;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class SurveyHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_name_survey)
        TextView tvName;

        @Bind(R.id.tv_status_survey)
        TextView tvStatus;

        @Bind(R.id.ll_status_survey)
        LinearLayout llStatus;

        @Bind(R.id.iv_ic_approved)
        ImageView ivIconApproved;

        @Bind(R.id.iv_delete)
        ImageView ivDelete;

        @Bind(R.id.tv_item_count)
        TextView tvItemCount;

        @Bind(R.id.tv_type_survey)
        TextView tvType;

        View view;

        public SurveyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener callBack) {
        this.mOnItemClickListener = callBack;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}