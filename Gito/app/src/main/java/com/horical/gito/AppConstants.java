package com.horical.gito;

public class AppConstants {

    // Token Convert
    public static final String BEGIN_TOKEN = "cccc";
    public static final String END_TOKEN = "dddd";

    // Menu
    public static final int MENU_HEADER = 0;
    public static final int MENU_BODY = 1;
    public static final int MENU_FOOTER = 2;

    // Nav Drawer Main Menu
    public static final int NAV_DRAWER_ID_MY_PAGE = 0;
    public static final int NAV_DRAWER_ID_ALL_PROJECT = 1;

    // Sub Menu - Project Details
    public static final int NAV_DRAWER_ID_MY_PROJECT = 2;
    public static final int NAV_DRAWER_ID_SUMMARY = 3;
    public static final int NAV_DRAWER_ID_TASK = 4;
    public static final int NAV_DRAWER_ID_GANTT = 5;
    public static final int NAV_DRAWER_ID_DOCUMENT = 6;
    public static final int NAV_DRAWER_ID_SOURCE_CODE = 7;
    public static final int NAV_DRAWER_ID_TEST_CASE = 8;
    public static final int NAV_DRAWER_ID_CHANGE_LOG = 9;
    public static final int NAV_DRAWER_ID_PROJECT_SETTING = 10;
    public static final int NAV_DRAWER_ID_POST_API = 11;
    public static final int NAV_DRAWER_ID_CHAT_APP = 12;
    public static final int NAV_DRAWER_ID_LOG_TIME = 13;
    // End Sub Menu

    public static final int NAV_DRAWER_ID_CALENDAR = 14;
    public static final int NAV_DRAWER_ID_TODO = 15;
    public static final int NAV_DRAWER_ID_MEETING = 16;
    public static final int NAV_DRAWER_ID_REPORT = 17;
    public static final int NAV_DRAWER_ID_ESTATE = 18;
    public static final int NAV_DRAWER_ID_CASE_STUDY = 19;
    public static final int NAV_DRAWER_ID_RECRUITMENT = 20;
    public static final int NAV_DRAWER_ID_NOTIFICATION = 21;
    public static final int NAV_DRAWER_ID_CHECK_IN = 22;
    public static final int NAV_DRAWER_ID_LEAVING_REGISTER = 23;
    public static final int NAV_DRAWER_ID_SURVEY = 24;
    public static final int NAV_DRAWER_ID_ACCOUNTING = 25;
    public static final int NAV_DRAWER_ID_SALARY = 26;
    // End Nav Drawer Main Menu

    // MY PAGE KEY
    public final static String KEY_FROM = "KEY_FROM";
    public final static int FROM_MENU = 0;
    public final static int FROM_MY_PAGE = 1;

    public static final String INTRO_FILE_DATA_XML_LOCALE = "intro_data/intro_data.xml";

    // PROJECT
    public static final int KEY_SUCCESS = 1;

    // Project Details
    public static final String KEY_PROJECT = "PROJECT";
    public static final int PROJECT_ITEM = 0;
    public static final int ROLE_ITEM = 990;
    public static final int USER_ITEM = 991;
    public static final int MEMBER_ITEM = 999;
    public static final int FEATURE_ITEM = 998;
    public static final int COMPONENT_ITEM = 996;
    public static final int VERSION_ITEM = 995;
    public static final int PATH_ITEM = 994;
    public static final int TAB_ITEM = 993;

    // Document
    public static final int GRID_STYLE = 994;
    public static final int LIST_STYLE = 992;
    public static final int DOCUMENT_ITEM = 997;
    public static final String FOLDER = "folder";
    public static final String RAR = ".rar";
    public static final String ZIP = ".zip";
    public static final String DOC = ".docx";

    // Summary
    public static final int SUMMARY_ISSUE_ITEM = 0;
    public static final String PROJECT_ID = "PROJECT_ID";

    // Survey
    public static final String SURVEY = "SURVEY";
    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_CROSS = 1;
    public static final int STATUS_NEW = 0;
    public static final int STATUS_APPROVED = 1;
    public static final int STATUS_REJECTED = 2;
    public static final int STATUS_SUMMITTED = 3;
    public static final int STATUS_SEND = 4;
    public static final int TYPE_OPTION = 0;
    public static final int TYPE_CHECK = 1;
    public static final int TYPE_TEXT = 2;

    // Task
    public static final int TASK_ITEM = 0;
    public static final int TASK_STATUS_OPEN = 0;
    public static final int TASK_STATUS_DOING = 1;
    public static final int TASK_STATUS_DONE = 2;
    public static final int TASK_STATUS_CLOSED = 3;
    public static final int TASK_STATUS_PENDING = 4;
    public static final int TASK_STATUS_REJECTED = 5;

    // Meeting
    public static final int MEETING_ITEM = 0;
    public static final int MEETING_FILE_ATTACH_ITEM = 1;
    public static final int MEETING_NOTE_ATTACH_ITEM = 2;
    public static final int MEETING_INVITEE_ITEM = 3;
    public static final int MEETING_DETAIL_ITEM = 4;
    public static final String MEETING_ID = "MEETING_ID";

    // Check-in
    public static final int CHECK_IN_ITEM = 0;

    //Source Code
    public static final int CODE_ITEM = 0;

    //Todo List
    public static final int TODO_LIST_ITEM = 0;
    public static final String TODO_LIST = "TODO_LIST";
    public static final String FROM_SCREEN = "FROM_SCREEN";
    public static final String TODO_LIST_ID = "TODO_LIST_ID";

    //Note
    public static final int NOTE_ITEM = 0;
    // Accounting
    public static final int ACCOUNTING_ITEM = 0;

    public static final long EXIT_INTERVAL = 500L;

    // SERVER_DATE_FORMAT
    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";


    // Salary
    public static final String BONUS_ID_KEY = "BONUS_ID_KEY";
    public static final String BONUS_MODE_KEY = "BONUS_MODE_KEY";
    public static final String METRIC_ID_KEY = "METRIC_ID_KEY";
    public static final String METRIC_GROUP_ID_KEY = "METRIC_GROUP_ID_KEY";

    // SERVER ERROR CODE
    public static final int ERROR_CODE_TOKEN_FAILED = 1102;

    // PUSH FUNCTION CASE
    public static final String PUSH_CASE_ACCOUNTING = "Accounting";
    public static final String PUSH_CASE_ANNOUNCEMENT = "Announcement";
    public static final String PUSH_CASE_CASE_STUDY = "CaseStudy";
    public static final String PUSH_CASE_CHECK_IN = "Checkin";
    public static final String PUSH_CASE_DOCUMENT = "Document";
    public static final String PUSH_CASE_ESTATE = "Estate";
    public static final String PUSH_CASE_SALARY = "Salary";
    public static final String PUSH_CASE_LEAVING = "Leaving";
    public static final String PUSH_CASE_LOG_TIME = "LogTime";
    public static final String PUSH_CASE_OVERTIME = "Overtime";
    public static final String PUSH_CASE_PREPAYMENT = "Prepayment";
    public static final String PUSH_CASE_REPORT = "Report";
    public static final String PUSH_CASE_SURVEY = "Survey";
    public static final String PUSH_CASE_TASK_DETAIL = "TaskDetail";
    public static final String PUSH_CASE_USER = "User";
    public static final String PUSH_CASE_VERSION = "Version";
    public static final String PUSH_CASE_PAYBACK = "Payback";

}
