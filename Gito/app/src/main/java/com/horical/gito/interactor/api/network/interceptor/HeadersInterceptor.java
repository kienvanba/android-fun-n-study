package com.horical.gito.interactor.api.network.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HeadersInterceptor implements Interceptor {

//    private static final String X_API_KEY = "x-api-key";

    @Override
    public Response intercept(Chain chain) throws IOException {
        final Request originalRequest = chain.request();
        final Request requestWithUserAgent = originalRequest.newBuilder()
//                .addHeader(X_API_KEY, BuildConfig.X_API_KEY)
                .build();
        return chain.proceed(requestWithUserAgent);
    }
}