package com.horical.gito.mvp.allProject.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.allProject.detail.injection.module.ProjectDetailModule;
import com.horical.gito.mvp.allProject.detail.view.ProjectDetailActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ProjectDetailModule.class)
public interface ProjectDetailComponent {
    void inject(ProjectDetailActivity activity);
}