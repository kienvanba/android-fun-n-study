package com.horical.gito.mvp.accounting.list.presenter;


import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.accounting.GetAllAccountingResponse;
import com.horical.gito.model.Accounting;
import com.horical.gito.mvp.accounting.list.view.IAccountingView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.makeLogTag;


public class AccountingPresenter extends BasePresenter implements IAccountingPresenter {

    public List<Accounting> mList;

    private static final String TAG = makeLogTag(AccountingPresenter.class);

    public void attachView(IAccountingView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IAccountingView getView() {
        return (IAccountingView) getIView();
    }

    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Accounting> getListAccountings() {
        return mList;
    }

    public void setListAccountings(List<Accounting> accounting) {
        this.mList = accounting;
    }

    @Override
    public void getAllAccounting() {
        getApiManager().getAllAccounting(new ApiCallback<GetAllAccountingResponse>() {
            @Override
            public void success(GetAllAccountingResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.accountings);
                getView().getAllAccountingSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getAllAccountingFailure(error.message);
            }
        });
    }

    public void deleteAccounting(String accountingId, final int position) {
        getApiManager().deleteAccounting(accountingId, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteAccountingSuccess(position);
            }

            @Override
            public void failure(RestError error) {
                getView().deleteAccountingFailure();
            }
        });
    }

    public List<Accounting> getListSearchAccounting(String keyword) {
        List<Accounting> searchAccounting = new ArrayList<>();
        if (keyword != null && keyword.trim().length() > 0) {
            for (Accounting accounting : mList) {
                if (accounting.getTitle() != null && accounting.getTitle().toLowerCase().contains(keyword.toLowerCase())) {
                    searchAccounting.add(accounting);
                }
            }
        } else {
            searchAccounting.addAll(mList);
        }
        return searchAccounting;
    }
}
