package com.horical.gito.mvp.dialog.dialogCompanySetting;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 3/20/17.
 */

public class DialogFilterWorkingHoliday extends Dialog implements View.OnClickListener {
    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    public DialogFilterWorkingHoliday(@NonNull Context context) {
        super(context, R.style.FullscreenDialog);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_filter_working_holiday);
        ButterKnife.bind(this);

        initData();
        initListener();
    }

    private void initData() {
        topBar.setTextTitle("Filter");
        topBar.setTextViewLeft("Cancel");
        topBar.setTextViewRight("Done");
    }

    private void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {

            @Override
            public void onLeftClicked() {
                dismiss();
            }

            @Override
            public void onRightClicked() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }
}
