package com.horical.gito.mvp.companySetting.list.fragment.info.view;

import com.horical.gito.base.IView;
import com.horical.gito.model.Information;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IInfoView extends IView {
    void showLoading();

    void hideLoading();

    void getCompanyInfoSuccess(Information information);

    void getCompanyInfoFailure(String err);

    void uploadError(String error);

    void uploadSuccess(String url);

    void updateInfoSuccess();

    void updateInfoFailure(String err);
}
