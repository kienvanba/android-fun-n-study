package com.horical.gito.interactor.api.response.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.ProjectResult;

public class GetProjectResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    private ProjectResult projectResult;

    public ProjectResult getProjectResult() {
        return projectResult;
    }

    public void setProjectResult(ProjectResult projectResult) {
        this.projectResult = projectResult;
    }
}
