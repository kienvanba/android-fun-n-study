package com.horical.gito.interactor.api.response.version;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Version;

import java.util.List;

public class GetAllVersionResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Version> versions;
}
