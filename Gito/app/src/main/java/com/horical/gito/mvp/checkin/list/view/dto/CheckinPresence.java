package com.horical.gito.mvp.checkin.list.view.dto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.mvp.checkin.list.view.adapter.CheckinPresenceAdapter;
import com.horical.gito.mvp.checkin.list.view.adapter.UserProfile;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Penguin on 05-Dec-16.
 */

public class  CheckinPresence extends BaseActivity implements ICheckinView, View.OnClickListener {

    ImageView filter;

    LinearLayout rb_btn_back;

    RecyclerView user;

    List<UserProfile> mList;

    CheckinPresenceAdapter mcheckinpresenceadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setContentView(R.layout.activity_checkin_presence);

        findViewsById();

        filter.setOnClickListener(this);
        rb_btn_back.setOnClickListener(this);

        getData();
    }

    private void findViewsById() {
        filter = (ImageView) findViewById(R.id.iv_filter);

        rb_btn_back = (LinearLayout) findViewById(R.id.rb_btn_back);

        user = (RecyclerView) findViewById(R.id.rb_rv_checkin_presence);


    }

    private void getData() {
        mList = new ArrayList<>();

        final UserProfile userProfile = new UserProfile(R.drawable.doraemon, "User A", "Director", R.drawable.bg_btn_grey,
                R.drawable.bg_btn_blue_white_corner, R.drawable.bg_btn_blue_white_corner, R.drawable.bg_btn_blue_white_corner,
                R.drawable.bg_btn_blue_white_corner, R.drawable.bg_btn_blue_white_corner, R.drawable.bg_btn_grey);

        UserProfile userProfile1 = new UserProfile(R.drawable.doraemon, "User B", "Tester", R.drawable.bg_btn_grey,
                R.drawable.bg_btn_blue_white_corner, R.drawable.bg_btn_blue_white_corner, R.drawable.bg_btn_blue_white_corner,
                R.drawable.bg_btn_blue_white_corner, R.drawable.bg_btn_grey, R.drawable.bg_btn_grey);

        mList.add(userProfile);
        mList.add(userProfile1);

        mcheckinpresenceadapter = new CheckinPresenceAdapter(this, mList);
        LinearLayoutManager li = new LinearLayoutManager(this);
        li.setOrientation(LinearLayoutManager.VERTICAL);
        user.setLayoutManager(li);
        user.setAdapter(mcheckinpresenceadapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_filter:
                Intent f = new Intent(this, CheckinFilter.class);
                startActivity(f);
                break;
            case R.id.rb_btn_back:
                Intent b = new Intent(this, CheckinActivity.class);
                startActivity(b);
                break;
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void getAllCheckinSuccess() {

    }


}
