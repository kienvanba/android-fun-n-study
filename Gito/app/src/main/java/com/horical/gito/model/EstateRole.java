package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/23/17.
 */

public class EstateRole implements Serializable {
    @SerializedName("delEstateItem")
    @Expose
    private Boolean delEstateItem;

    @SerializedName("modifyEstateItem")
    @Expose
    private Boolean modifyEstateItem;

    @SerializedName("addEstateItem")
    @Expose
    private Boolean addEstateItem;

    @SerializedName("del")
    @Expose
    private Boolean del;

    @SerializedName("modify")
    @Expose
    private Boolean modify;

    @SerializedName("view")
    @Expose
    private Boolean view;

    @SerializedName("create")
    @Expose
    private Boolean create;

    public Boolean isDelEstateItem() {
        return delEstateItem;
    }

    public void setDelEstateItem(Boolean delEstateItem) {
        this.delEstateItem = delEstateItem;
    }

    public Boolean isModifyEstateItem() {
        return modifyEstateItem;
    }

    public void setModifyEstateItem(Boolean modifyEstateItem) {
        this.modifyEstateItem = modifyEstateItem;
    }

    public Boolean isAddEstateItem() {
        return addEstateItem;
    }

    public void setAddEstateItem(Boolean addEstateItem) {
        this.addEstateItem = addEstateItem;
    }

    public Boolean isDel() {
        return del;
    }

    public void setDel(Boolean del) {
        this.del = del;
    }

    public Boolean isModify() {
        return modify;
    }

    public void setModify(Boolean modify) {
        this.modify = modify;
    }

    public Boolean isView() {
        return view;
    }

    public void setView(Boolean view) {
        this.view = view;
    }

    public Boolean isCreate() {
        return create;
    }

    public void setCreate(Boolean create) {
        this.create = create;
    }
}