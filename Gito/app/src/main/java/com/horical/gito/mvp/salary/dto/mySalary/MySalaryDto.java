package com.horical.gito.mvp.salary.dto.mySalary;

public class MySalaryDto {
    private String month;
    private String year;
    private double total;
    private double tax;
    private double basicSalary;
    private double overtimeSalary;
    private double unionDues;

    public MySalaryDto() {
    }

    public MySalaryDto(String month, String year, double total, double tax, double basicSalary, double overtimeSalary, double unionDues) {
        this.month = month;
        this.year = year;
        this.total = total;
        this.tax = tax;
        this.basicSalary = basicSalary;
        this.overtimeSalary = overtimeSalary;
        this.unionDues = unionDues;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(double basicSalary) {
        this.basicSalary = basicSalary;
    }

    public double getOvertimeSalary() {
        return overtimeSalary;
    }

    public void setOvertimeSalary(double overtimeSalary) {
        this.overtimeSalary = overtimeSalary;
    }

    public double getUnionDues() {
        return unionDues;
    }

    public void setUnionDues(double unionDues) {
        this.unionDues = unionDues;
    }
}
