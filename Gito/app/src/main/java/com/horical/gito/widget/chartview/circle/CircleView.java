package com.horical.gito.widget.chartview.circle;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;

import com.horical.gito.R;
import com.horical.gito.widget.chartview.ChartView;

/**
 * Created by Tin on 28-Dec-16.
 */

public class CircleView extends ChartView {

    protected float mTextSize;

    protected RectF rectF;
    private float mPercentDraw;
    private float mAngleDraw;


    public CircleView(Context context) {
        super(context);
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CircleView, 0, 0);

        try {
            mTextSize = typedArray.getDimension(R.styleable.CircleView_textSizeCircle,0);

        }
        finally {
            typedArray.recycle();
        }
    }


    public void setPercentDraw(float mPercentDraw) {
        this.mPercentDraw = mPercentDraw;
    }

    public float getAngleDraw() {

        return mAngleDraw;
    }

    public void setAngleDraw(float mAngleDraw) {
        this.mAngleDraw = mAngleDraw;
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        /** backgroud **/
        onDrawForegroundBackgroud(canvas);

        /** backgroud blue **/
        onDrawBackgroudPercent(canvas);

        /**backgroud white**/
        onDrawSemiBackgroud(canvas);

    }

    private void onDrawSemiBackgroud(Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.white));
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.FILL);

        canvas.drawCircle(
                getWidth()/2.0f,
                getHeight()/2.0f,
                getWidth()/2.8f,
                mPaint
        );

        mPaint.setColor(getResources().getColor(R.color.grey_x));
        mPaint.setTextSize(mTextSize);
        mPaint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText((int)mPercent+"%",
                getWidth()/2.0f,
                getHeight()/2.0f,
                mPaint);


    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void onDrawBackgroudPercent(Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.app_color));
        mPaint.setStrokeWidth(30);
        mPaint.setStyle(Paint.Style.STROKE);

        rectF = new RectF(16 , 15 , getWidth()-convertDp(7), getHeight()-convertDp(7));

        canvas.drawArc(
                rectF ,
                270,
                mAngleDraw,
                false,
                mPaint
        );

    }

    private void onDrawForegroundBackgroud(Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.grey));
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.FILL);

        canvas.drawCircle(
                getWidth()/2.0f ,
                getHeight()/2.0f ,
                getWidth()/2.0f ,
                mPaint);
    }

}
