package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResults {

    @SerializedName("user")
    @Expose
    public User user;

    @SerializedName("token")
    @Expose
    public String token;
}
