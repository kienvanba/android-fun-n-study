package com.horical.gito.mvp.companySetting.list.fragment.feature.dto;

/**
 * Created by thanhle on 3/15/17.
 */

public class MenuFeature {
    private String title;
    private int iconResource;
    private boolean isSelected;

    public MenuFeature(String title, int iconResource, boolean isSelected) {
        this.title = title;
        this.iconResource = iconResource;
        this.isSelected = isSelected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIconResource() {
        return iconResource;
    }

    public void setIconResource(int iconResource) {
        this.iconResource = iconResource;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
