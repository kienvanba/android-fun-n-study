package com.horical.gito.interactor.api.response.todolist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.TodoList;

/**
 * Created by Luong on 11-Apr-17.
 */

public class GetUpdateAttachFileTodoResponse  extends BaseResponse {

    @SerializedName("results")
    @Expose
    public TodoList todoList;
}