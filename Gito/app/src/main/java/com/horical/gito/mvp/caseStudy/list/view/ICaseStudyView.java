package com.horical.gito.mvp.caseStudy.list.view;

import com.horical.gito.base.IView;

/**
 * Created by QuangHai on 10/11/2016.
 */

public interface ICaseStudyView extends IView {
    void showLoading();

    void hideLoading();

    void getAllCaseStudySuccess();

    void getAllCaseStudyFailure(String error);

    void deleteCaseStudySuccess(int position);

    void deleteCaseStudyFailure();
}
