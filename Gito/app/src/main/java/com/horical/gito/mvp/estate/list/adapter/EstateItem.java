package com.horical.gito.mvp.estate.list.adapter;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.estate.Estate;

/**
 * Created by hoangsang on 3/8/17.
 */

public class EstateItem implements IRecyclerItem {
    public Estate estate;

    public EstateItem() {
    }

    public EstateItem(Estate estate) {
        this.estate = estate;
    }

    @Override
    public int getItemViewType() {
        return 0;
    }
}
