package com.horical.gito.widget.textview;

import android.content.Context;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatTextView;

import com.horical.gito.R;

public class GitOTextView extends AppCompatTextView implements ITextViewStyle {

    public static final int BLACK = 0;
    public static final int WHITE = 1;
    public static final int BLACK_WHITE = 2;
    public static final int WHITE_BLACK = 3;
    public static final int WHITE_BLUE = 4;
    public static final int BLUE_WHITE = 5;

    public GitOTextView(Context context) {
        super(context);
        setTextColor(getResources().getColor(R.color.black));
    }

    public GitOTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GitOTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTextColor(getResources().getColor(R.color.black));
    }

    @Override
    public void setCircleStyle() {
        setBackgroundResource(R.drawable.bg_circle_grey);
    }

    @Override
    public void setTypeText(int size, int type) {
        setTextSize(size);
        setTypeface(null, type);
    }

    @Override
    public void setCustomTextColor(int color) {

        switch (color) {
            case BLACK: {
                setTextColor(getResources().getColor(R.color.black));
                break;
            }
            case WHITE: {
                setTextColor(getResources().getColor(R.color.white));
                break;
            }
            case BLACK_WHITE: {
                setTextColor(getResources().getColor(R.color.color_text_black_white));
                break;
            }
            case WHITE_BLACK: {
                setTextColor(getResources().getColor(R.color.white));
                break;
            }
            case BLUE_WHITE: {
                setTextColor(getResources().getColor(R.color.color_text_blue_white));
                break;
            }
            case WHITE_BLUE: {
                setTextColor(getResources().getColor(R.color.color_text_white_blue));
                break;
            }
        }
    }

}