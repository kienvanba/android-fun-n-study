package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class LogTimeMyPage implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("projectId")
    @Expose
    private Project projectId;

    @SerializedName("total")
    @Expose
    private List<LogTime> logTimes;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Project getProjectId() {
        return projectId;
    }

    public void setProjectId(Project projectId) {
        this.projectId = projectId;
    }

    public List<LogTime> getLogTimes() {
        return logTimes;
    }

    public void setLogTimes(List<LogTime> logTimes) {
        this.logTimes = logTimes;
    }
}
