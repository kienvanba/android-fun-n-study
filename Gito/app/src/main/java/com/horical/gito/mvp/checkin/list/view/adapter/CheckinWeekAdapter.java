package com.horical.gito.mvp.checkin.list.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by Luong on 27-Feb-17.
 */

public class CheckinWeekAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyRecyclerHeadersAdapter {
    private Context Icontext;
    private List<AvatarMajorItem> mList;
    private CheckinFilterAdapter.OnItemClickListener mOnItemClickListener;


    public CheckinWeekAdapter(Context context, List<AvatarMajorItem> list) {
        Icontext = context;
        mList = list;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_checkin_user, parent, false);
        viewHolder = new CheckinViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final AvatarMajorItem avatarMajorItem = mList.get(position);
        CheckinWeekAdapter.CheckinViewHolder viewHolder = (CheckinWeekAdapter.CheckinViewHolder) holder;
        viewHolder.avatar.setImageResource(avatarMajorItem.Avatar);
        viewHolder.major.setText(avatarMajorItem.Major);
    }


    @Override
    public long getHeaderId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class CheckinViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.iv_checkin_user_avatar)
        ImageView avatar;

        @Bind(R.id.tv_checkin_major)
        TextView major;

        public CheckinViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setOnItemClickListener(CheckinFilterAdapter.OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);
    }
}