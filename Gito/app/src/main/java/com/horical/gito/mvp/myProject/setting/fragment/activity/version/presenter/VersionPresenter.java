package com.horical.gito.mvp.myProject.setting.fragment.activity.version.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.version.Request;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.version.GetVersionResponse;
import com.horical.gito.model.Version;
import com.horical.gito.mvp.myProject.setting.fragment.activity.version.view.IVersionView;

import static com.horical.gito.utils.LogUtils.LOGE;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class VersionPresenter extends BasePresenter implements IVersionPresenter {
    private static final String TAG = makeLogTag(VersionPresenter.class);

    private final String projectId = GitOStorage.getInstance().getProject().getId();


    private Version mVersion;

    private IVersionView getView(){
        return (IVersionView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        getEventManager().register(this);
    }

    public Version getVersion(){
        return mVersion;
    }

    public void getVersion(String versionId){
        if(!isViewAttached()) return;
        getView().showLoading();

        getApiManager().getVersion(projectId, versionId, new ApiCallback<GetVersionResponse>() {
            @Override
            public void success(GetVersionResponse res) {
                if(res.version!=null) {
                    mVersion = res.version;
                    getView().onRequestSuccess();
                }
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    public void createVersion(Request request){
        if(!isViewAttached()) return;
        getView().showLoading();

        getApiManager().createVersion(projectId, request, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                if(res.success){
                    getView().onRequestSuccess();
                }
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    public void updateVersion(String versionId, Request request){
        if(!isViewAttached()) return;
        getView().showLoading();

        getApiManager().updateVersion(projectId, versionId, request, new ApiCallback<GetVersionResponse>() {
            @Override
            public void success(GetVersionResponse res) {
                if(res.success && res.version!=null){
                    mVersion = res.version;
                    getView().onRequestSuccess();
                }
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
