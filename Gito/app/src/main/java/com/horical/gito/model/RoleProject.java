package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class RoleProject implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("roleProjectId")
    @Expose
    private String roleProjectId;

    @SerializedName("postApi")
    @Expose
    private PostApiRole postApiRole;

    @SerializedName("testCase")
    @Expose
    private TestCaseRole testCaseRole;

    @SerializedName("chat")
    @Expose
    private ChatRole chatRole;

    @SerializedName("code")
    @Expose
    private CodeRole codeRole;

    @SerializedName("document")
    @Expose
    private DocumentRole documentRole;

    @SerializedName("logTime")
    @Expose
    private LogTimeRole logTimeRole;

    @SerializedName("task")
    @Expose
    private TaskRole taskRole;

    @SerializedName("component")
    @Expose
    private ComponentRole componentRole;

    @SerializedName("group")
    @Expose
    private GroupRole groupRole;

    @SerializedName("member")
    @Expose
    private MemberRole memberRole;

    @SerializedName("feature")
    @Expose
    private FeatureRole featureRole;

    @SerializedName("version")
    @Expose
    private VersionRole versionRole;

    @SerializedName("changelog")
    @Expose
    private ChangeLogRole changeLogRole;

    @SerializedName("summary")
    @Expose
    private SummaryRole summaryRole;

    @SerializedName("gantt")
    @Expose
    private GanttRole ganttRole;

    @SerializedName("createDate")
    @Expose
    private Date createDate;

    @SerializedName("roleName")
    @Expose
    private String roleName;

    @SerializedName("wasUsed")
    @Expose
    private boolean wasUsed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getRoleProjectId() {
        return roleProjectId;
    }

    public void setRoleProjectId(String roleProjectId) {
        this.roleProjectId = roleProjectId;
    }

    public PostApiRole getPostApiRole() {
        return postApiRole;
    }

    public void setPostApiRole(PostApiRole postApiRole) {
        this.postApiRole = postApiRole;
    }

    public TestCaseRole getTestCaseRole() {
        return testCaseRole;
    }

    public void setTestCaseRole(TestCaseRole testCaseRole) {
        this.testCaseRole = testCaseRole;
    }

    public ChatRole getChatRole() {
        return chatRole;
    }

    public void setChatRole(ChatRole chatRole) {
        this.chatRole = chatRole;
    }

    public CodeRole getCodeRole() {
        return codeRole;
    }

    public void setCodeRole(CodeRole codeRole) {
        this.codeRole = codeRole;
    }

    public DocumentRole getDocumentRole() {
        return documentRole;
    }

    public void setDocumentRole(DocumentRole documentRole) {
        this.documentRole = documentRole;
    }

    public LogTimeRole getLogTimeRole() {
        return logTimeRole;
    }

    public void setLogTimeRole(LogTimeRole logTimeRole) {
        this.logTimeRole = logTimeRole;
    }

    public TaskRole getTaskRole() {
        return taskRole;
    }

    public void setTaskRole(TaskRole taskRole) {
        this.taskRole = taskRole;
    }

    public ComponentRole getComponentRole() {
        return componentRole;
    }

    public void setComponentRole(ComponentRole componentRole) {
        this.componentRole = componentRole;
    }

    public GroupRole getGroupRole() {
        return groupRole;
    }

    public void setGroupRole(GroupRole groupRole) {
        this.groupRole = groupRole;
    }

    public MemberRole getMemberRole() {
        return memberRole;
    }

    public void setMemberRole(MemberRole memberRole) {
        this.memberRole = memberRole;
    }

    public FeatureRole getFeatureRole() {
        return featureRole;
    }

    public void setFeatureRole(FeatureRole featureRole) {
        this.featureRole = featureRole;
    }

    public VersionRole getVersionRole() {
        return versionRole;
    }

    public void setVersionRole(VersionRole versionRole) {
        this.versionRole = versionRole;
    }

    public ChangeLogRole getChangeLogRole() {
        return changeLogRole;
    }

    public void setChangeLogRole(ChangeLogRole changeLogRole) {
        this.changeLogRole = changeLogRole;
    }

    public SummaryRole getSummaryRole() {
        return summaryRole;
    }

    public void setSummaryRole(SummaryRole summaryRole) {
        this.summaryRole = summaryRole;
    }

    public GanttRole getGanttRole() {
        return ganttRole;
    }

    public void setGanttRole(GanttRole ganttRole) {
        this.ganttRole = ganttRole;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public boolean isWasUsed() {
        return wasUsed;
    }

    public void setWasUsed(boolean wasUsed) {
        this.wasUsed = wasUsed;
    }
}