package com.horical.gito.mvp.salary.detail.metric.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.salary.metric.GetDetailMetricResponse;
import com.horical.gito.model.Metric;
import com.horical.gito.mvp.salary.detail.metric.view.IDetailMetricView;
import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;

import java.util.ArrayList;
import java.util.List;

public class DetailMetricPresenter extends BasePresenter implements IDetailMetricPresenter {

    public void attachView(IDetailMetricView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IDetailMetricView getView() {
        return (IDetailMetricView) getIView();
    }

    private List<ListStaffDto> mStaffs;
    private Metric mMetric;


    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public List<ListStaffDto> getStaffs() {
        if (mStaffs == null) {
            mStaffs = new ArrayList<>();
        }
        return mStaffs;
    }

    @Override
    public Metric getMetric() {
        if (mMetric == null) {
            mMetric = new Metric();
        }
        return mMetric;
    }

    private void setMetric(Metric metric) {
        this.mMetric = metric;
    }

    @Override
    public void requestGetDetailMetric(String metricId) {
        getApiManager().getMetricById(metricId, new ApiCallback<GetDetailMetricResponse>() {
            @Override
            public void success(GetDetailMetricResponse res) {
                setMetric(res.getMetric());
                getView().requestGetDetailMetricSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().requestGetDetailMetricError(error);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
