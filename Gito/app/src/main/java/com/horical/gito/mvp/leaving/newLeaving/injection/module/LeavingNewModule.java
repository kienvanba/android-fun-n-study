package com.horical.gito.mvp.leaving.newLeaving.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.leaving.newLeaving.presenter.LeavingNewPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Dragonoid on 12/9/2016.
 */
@Module
public class LeavingNewModule {
    @PerActivity
    @Provides
    LeavingNewPresenter provideLeavingNewPresenter() {
        return new LeavingNewPresenter();
    }
}
