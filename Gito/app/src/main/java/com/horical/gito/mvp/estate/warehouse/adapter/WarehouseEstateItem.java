package com.horical.gito.mvp.estate.warehouse.adapter;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.estate.WarehouseEstate;

/**
 * Created by hoangsang on 4/12/17.
 */

public class WarehouseEstateItem implements IRecyclerItem {
    public WarehouseEstate warehouseEstate;

    public WarehouseEstateItem(WarehouseEstate warehouseEstate) {
        this.warehouseEstate = warehouseEstate;
    }

    @Override
    public int getItemViewType() {
        return 0;
    }
}
