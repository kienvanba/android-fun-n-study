package com.horical.gito.mvp.checkin.list.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Penguin on 23-Dec-16.
 */

public class Checkin3Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyRecyclerHeadersAdapter {

    private Context Icontext;
    private Checkin3Adapter.OnItemClickListener mOnItemClickListener;

    public Checkin3Adapter(Context context) {
        Icontext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_checkin_calendar, parent, false);
        viewHolder = new Checkin3Adapter.CheckinViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Checkin3Adapter.CheckinViewHolder viewHolder = (Checkin3Adapter.CheckinViewHolder) holder;

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public long getHeaderId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    public class CheckinViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_1)
        TextView t1;

        @Bind(R.id.tv_2)
        TextView t2;

        @Bind(R.id.tv_3)
        TextView t3;

        @Bind(R.id.tv_4)
        TextView t4;

        @Bind(R.id.tv_5)
        TextView t5;

        @Bind(R.id.tv_6)
        TextView t6;

        @Bind(R.id.tv_7)
        TextView t7;

        @Bind(R.id.tv_15)
        TextView t15;

        @Bind(R.id.tv_16)
        TextView t16;

        @Bind(R.id.tv_17)
        TextView t17;

        @Bind(R.id.tv_18)
        TextView t18;

        @Bind(R.id.tv_19)
        TextView t19;

        @Bind(R.id.tv_20)
        TextView t20;

        @Bind(R.id.tv_21)
        TextView t21;

        @Bind(R.id.tv_29)
        TextView t29;

        @Bind(R.id.tv_30)
        TextView t30;

        @Bind(R.id.tv_31)
        TextView t31;

        @Bind(R.id.tv_32)
        TextView t32;

        @Bind(R.id.tv_33)
        TextView t33;

        @Bind(R.id.tv_34)
        TextView t34;

        @Bind(R.id.tv_35)
        TextView t35;

        @Bind(R.id.tv_43)
        TextView t43;

        @Bind(R.id.tv_44)
        TextView t44;

        @Bind(R.id.tv_45)
        TextView t45;

        @Bind(R.id.tv_46)
        TextView t46;

        @Bind(R.id.tv_47)
        TextView t47;

        @Bind(R.id.tv_48)
        TextView t48;

        @Bind(R.id.tv_49)
        TextView t49;

        @Bind(R.id.tv_57)
        TextView t57;

        @Bind(R.id.tv_58)
        TextView t58;

        @Bind(R.id.tv_59)
        TextView t59;

        @Bind(R.id.tv_60)
        TextView t60;

        @Bind(R.id.tv_61)
        TextView t61;

        @Bind(R.id.tv_62)
        TextView t62;

        @Bind(R.id.tv_63)
        TextView t63;
        public CheckinViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public void setOnItemClickListener(Checkin3Adapter.OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);
    }
}

