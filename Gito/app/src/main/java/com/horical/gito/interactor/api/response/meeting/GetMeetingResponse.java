package com.horical.gito.interactor.api.response.meeting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Meeting;

/**
 * Created by Lemon on 12/22/2016
 */

public class GetMeetingResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public Meeting meeting;
}
