package com.horical.gito.mvp.salary.bonus.detail.presenter;

import com.horical.gito.model.InputAddon;
import com.horical.gito.model.InputAddonUser;
import com.horical.gito.model.User;
import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;

import java.util.List;

public interface IDetailBonusPresenter {
    InputAddon getInputAddon();

    InputAddon getInputAddonDefault();

    boolean isEditable();

    void requestGetDetailInputAddon(String inputAddonId);

    void requestCreateInputAddon();

    void requestUpdateInputAddon();

    void requestGetAllUser();

    List<InputAddonUser> getInputAddonUsersFromUsers(List<User> users);
}
