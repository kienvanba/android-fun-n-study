package com.horical.gito.mvp.companySetting.list.fragment.members.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.list.fragment.members.presenter.MembersPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class MembersModule {
    @PerFragment
    @Provides
    MembersPresenter provideMembersPresenter(){
        return new MembersPresenter();
    }
}
