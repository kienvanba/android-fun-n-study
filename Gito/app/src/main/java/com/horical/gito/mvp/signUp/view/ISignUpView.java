package com.horical.gito.mvp.signUp.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.user.RegisterResponse;

public interface ISignUpView extends IView {

    void errorEmailInvalid();

    void errorConfirmPassword();

    void showLoading();

    void hideLoading();

    void signUpError(RestError error);

    void signUpSuccess(RegisterResponse response);

}
