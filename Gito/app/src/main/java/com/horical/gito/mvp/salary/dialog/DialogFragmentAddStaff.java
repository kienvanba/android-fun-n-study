package com.horical.gito.mvp.salary.dialog;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.BaseDialogFragment;
import com.horical.gito.model.User;
import com.horical.gito.mvp.salary.dialog.adapter.StaffSalaryAdapter;
import com.horical.gito.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class DialogFragmentAddStaff extends BaseDialogFragment implements View.OnClickListener {

    @Bind(R.id.tv_cancel)
    TextView tvCancel;

    @Bind(R.id.tv_title)
    TextView tvTitle;

    @Bind(R.id.tv_save)
    TextView tvSave;

    @Bind(R.id.edt_amount)
    EditText edtAmount;

    @Bind(R.id.tv_header)
    TextView tvHeader;

    @Bind(R.id.edt_search)
    EditText edtSearch;

    @Bind(R.id.imv_check_all)
    ImageView imvCheckAll;

    @Bind(R.id.rcv_staff)
    RecyclerView rcvStaff;

    private StaffSalaryAdapter mAdapter;

    private List<User> originUsers;
    private List<User> filterUsers = new ArrayList<>();
    private String searchKeyword = "";
    private IUserAdapterListener callback;

    public static DialogFragmentAddStaff newInstance(String mode) {
        Bundle args = new Bundle();
        args.putString("data", mode);
        DialogFragmentAddStaff fragment = new DialogFragmentAddStaff();
        fragment.setArguments(args);
        return fragment;
    }

    public List<User> getUsers() {
        if (originUsers == null) {
            originUsers = new ArrayList<>();
        }
        return originUsers;
    }

    public void setUsers(List<User> users) {
        this.originUsers = users;
        this.filterUsers.clear();
        this.filterUsers.addAll(originUsers);
    }

    public void setCallback(IUserAdapterListener callback) {
        this.callback = callback;
    }

    @Override
    protected int layoutID() {
        return R.layout.dialog_salary_add_staff;
    }

    @Override
    protected void initData() {
        String mode = getArguments().getString("data", getString(R.string.user));
        tvTitle.setText(getString(R.string.add_mode, mode));
        tvHeader.setText(mode);

        mAdapter = new StaffSalaryAdapter(getActivity(), filterUsers, new StaffSalaryAdapter.IUserAdapterListener() {
            @Override
            public void onSelected(int position) {
                imvCheckAll.setSelected(isCheckAllUser());
            }
        });
        rcvStaff.setAdapter(mAdapter);
        rcvStaff.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private boolean isCheckAllUser() {
        for (User user : originUsers) {
            if (!user.isSelected()) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void initListener() {
        tvCancel.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        imvCheckAll.setOnClickListener(this);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchKeyword = s.toString();
                doSearch();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void doSearch() {
        filterUsers.clear();
        for (User originUser : originUsers) {
            if (originUser.getDisplayName().toLowerCase().contains(searchKeyword.trim().toLowerCase())) {
                filterUsers.add(originUser);
            } else {
                originUser.setSelected(false);
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismiss();
                break;

            case R.id.tv_save:
                List<User> selectedUser = new ArrayList<>();
                for (User user : originUsers) {
                    if (user.isSelected()) {
                        user.setAmount(CommonUtils.parseStringToInt(edtAmount.getText().toString()));
                        selectedUser.add(user);
                    }
                }
                if (callback != null) {
                    callback.onDone(selectedUser);
                    dismiss();
                }
                break;

            case R.id.imv_check_all:
                imvCheckAll.setSelected(!imvCheckAll.isSelected());
                for (User user : originUsers) {
                    user.setSelected(imvCheckAll.isSelected());
                }
                mAdapter.notifyDataSetChanged();
                break;

            default:
                break;
        }
    }

    public interface IUserAdapterListener {
        void onDone(List<User> selectedUsers);
    }
}
