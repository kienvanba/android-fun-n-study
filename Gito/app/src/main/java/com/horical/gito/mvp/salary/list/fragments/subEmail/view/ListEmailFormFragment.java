package com.horical.gito.mvp.salary.list.fragments.subEmail.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.mvp.salary.detail.emailForm.view.DetailEmailFormActivity;
import com.horical.gito.mvp.salary.list.fragments.BaseListSalaryFragment;
import com.horical.gito.mvp.salary.list.fragments.subEmail.adapter.ListEmailFormAdapter;
import com.horical.gito.mvp.salary.list.fragments.subEmail.injection.component.DaggerIListEmailFormFragmentComponent;
import com.horical.gito.mvp.salary.list.fragments.subEmail.injection.component.IListEmailFormFragmentComponent;
import com.horical.gito.mvp.salary.list.fragments.subEmail.injection.module.ListEmailFormFragmentModule;
import com.horical.gito.mvp.salary.list.fragments.subEmail.presenter.ListEmailFormFragmentPresenter;

import javax.inject.Inject;

import butterknife.Bind;

public class ListEmailFormFragment extends BaseListSalaryFragment implements View.OnClickListener,
        IListEmailFormFragmentView {

    @Bind(R.id.rcv_email_form)
    RecyclerView rcvEmailForm;

    @Inject
    ListEmailFormFragmentPresenter mPresenter;

    private ListEmailFormAdapter mAdapter;

    public static ListEmailFormFragment newInstance() {
        Bundle args = new Bundle();
        ListEmailFormFragment fragment = new ListEmailFormFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list_email_form;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IListEmailFormFragmentComponent component = DaggerIListEmailFormFragmentComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .listEmailFormFragmentModule(new ListEmailFormFragmentModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        mAdapter = new ListEmailFormAdapter(getActivity(), mPresenter.getEmailForms(), new ListEmailFormAdapter.EmailFormAdapterListener() {
            @Override
            public void onSelected(int position) {
                Intent intentEmailForm = new Intent(getActivity(), DetailEmailFormActivity.class);
                startActivity(intentEmailForm);
            }

            @Override
            public void onRemove(int position) {
                Toast.makeText(getActivity(), "Delete at position:" + position, Toast.LENGTH_SHORT).show();
            }
        });
        rcvEmailForm.setAdapter(mAdapter);
        rcvEmailForm.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }
}

