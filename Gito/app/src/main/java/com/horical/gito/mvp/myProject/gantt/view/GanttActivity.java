package com.horical.gito.mvp.myProject.gantt.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.HorizontalScrollView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.myProject.gantt.adapter.DayAdapter;
import com.horical.gito.mvp.myProject.gantt.adapter.DetailAdapter;
import com.horical.gito.mvp.myProject.gantt.adapter.GanttDetailItem;
import com.horical.gito.mvp.myProject.gantt.adapter.IssueAdapter;
import com.horical.gito.mvp.myProject.gantt.adapter.IssueItem;
import com.horical.gito.mvp.myProject.gantt.dto.GanttDayDto;
import com.horical.gito.mvp.myProject.gantt.dto.GanttDetailDto;
import com.horical.gito.mvp.myProject.gantt.dto.GanttIssueDto;
import com.horical.gito.mvp.myProject.gantt.injection.component.DaggerGanttComponent;
import com.horical.gito.mvp.myProject.gantt.injection.component.GanttComponent;
import com.horical.gito.mvp.myProject.gantt.injection.module.GanttModule;
import com.horical.gito.mvp.myProject.gantt.presenter.GanttPresenter;
import com.horical.gito.utils.ConvertUtils;
import com.horical.gito.widget.recycleview.GanttRecycleView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GanttActivity extends DrawerActivity implements IGanttView, View.OnClickListener {
    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.gantt_rv_day)
    RecyclerView mRvDay;

    @Bind(R.id.gantt_rv_issue)
    RecyclerView mRvIssue;

    @Bind(R.id.gantt_rv_detail)
    GanttRecycleView mRvDetail;

    @Bind(R.id.gantt_detail_scroll_horizontal)
    HorizontalScrollView mHorizontalScrollView;

    @Inject
    GanttPresenter mPresenter;

    DayAdapter dayAdapter;
    DetailAdapter detailAdapter;
    IssueAdapter issueAdapter;

    List<IRecyclerItem> ganttDetailDtos;
    List<IRecyclerItem> ganttIssueDtos;
    List<IRecyclerItem> ganttDayDtos;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_gantt;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_GANTT;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        GanttComponent component = DaggerGanttComponent.builder()
                .ganttModule(new GanttModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initView();
        initData();
        initListener();
    }

    private void initView() {
        topBar.setTextTitle("Gantt");
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_FILTER);

    }


    protected void initData() {

        ganttDetailDtos = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            GanttDetailDto detailDto = new GanttDetailDto();
            detailDto.ganttDetail = "button " + i;
            GanttDetailItem item = new GanttDetailItem(detailDto);
            item.lengh = ConvertUtils.convertDpToInt(this, 30) * 20;
            ganttDetailDtos.add(item);
        }

        ganttIssueDtos = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            GanttIssueDto issueDto = new GanttIssueDto();
            issueDto.name = "#" + i;
            IssueItem item = new IssueItem(issueDto);
            ganttIssueDtos.add(item);
        }


        ganttDayDtos = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            GanttDayDto dayDto = new GanttDayDto();
            dayDto.month = i;
            dayDto.startDay = i * 7;
            dayDto.endDay = (i + 1) * 7 - 1;
            ganttDayDtos.add(dayDto);
        }

        dayAdapter = new DayAdapter(ganttDayDtos, this);
        issueAdapter = new IssueAdapter(ganttIssueDtos, this);
        detailAdapter = new DetailAdapter(ganttDetailDtos, this);

        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRvDay.setLayoutManager(llm);

        LinearLayoutManager llm1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm1.setOrientation(LinearLayoutManager.VERTICAL);
        mRvIssue.setLayoutManager(llm1);

        LinearLayoutManager llm2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm2.setOrientation(LinearLayoutManager.VERTICAL);
        mRvDetail.setLayoutManager(llm2);
        mRvDetail.distanceHeight = ConvertUtils.convertDpToInt(this, 10);
        mRvDetail.distanceWidth = ConvertUtils.convertDpToInt(this, 20);

        mRvDay.setAdapter(dayAdapter);
        mRvIssue.setAdapter(issueAdapter);
        mRvDetail.setAdapter(detailAdapter);

        mRvIssue.setNestedScrollingEnabled(false);
        mRvIssue.setEnabled(false);
    }

    protected void initListener() {
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {


            @Override
            public void onLeftClicked() {
                finish();
            }

            @Override
            public void onRightButtonOneClicked() {

            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });
        mRvDetail.addOnScrollListener(detailScrollListener);
        mRvIssue.addOnScrollListener(issueScrollListener);

    }

    private RecyclerView.OnScrollListener detailScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (mRvIssue.getScrollState() == RecyclerView.SCROLL_STATE_IDLE)
                mRvIssue.scrollBy(0, dy);

        }
    };

    private RecyclerView.OnScrollListener issueScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (mRvDetail.getScrollState() == RecyclerView.SCROLL_STATE_IDLE)
                mRvDetail.scrollBy(0, dy);
        }
    };

    @Override
    public void onClick(View v) {

    }
}
