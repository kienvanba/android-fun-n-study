package com.horical.gito.mvp.todolist.note.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.model.TokenFile;
import com.horical.gito.mvp.todolist.todo.detail.adapter.AttachFileTodoAdapter;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Luong on 10-Apr-17.
 */

public class AttachFileNoteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<TokenFile> attachFiles;
    private AttachFileNoteAdapter.OnItemClickListener callback;

    public AttachFileNoteAdapter(Context context, List<TokenFile> attachFiles, AttachFileNoteAdapter.OnItemClickListener callback) {
        this.context = context;
        this.attachFiles = attachFiles;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note_attachfile, parent, false);
        return new AttachFileNoteAdapter.AttachFileViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TokenFile attachFile = attachFiles.get(position);
        AttachFileNoteAdapter.AttachFileViewHolder viewHolder = (AttachFileNoteAdapter.AttachFileViewHolder) holder;
        viewHolder.tvFileName.setText(attachFile.getName());


    }

    @Override
    public int getItemCount() {
        return attachFiles != null ? attachFiles.size() : 0;
    }

    public class AttachFileViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view;

        @Bind(R.id.tv_note_file_name)
        GitOTextView tvFileName;

        @Bind(R.id.imv_note_file_delete)
        ImageView ivDelete;


        public AttachFileViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
            view.setOnClickListener(this);
            ivDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == view.getId()) {
                callback.onAttachFileNoteClick(getAdapterPosition());
            } else if (v.getId() == ivDelete.getId()) {
                callback.onAttachFileNoteDelete(getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onAttachFileNoteDelete(int position);

        void onAttachFileNoteClick(int position);
    }
}
