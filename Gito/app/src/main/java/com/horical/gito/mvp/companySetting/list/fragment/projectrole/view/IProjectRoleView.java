package com.horical.gito.mvp.companySetting.list.fragment.projectrole.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IProjectRoleView extends IView {
    void getAllProjectRoleSuccess();

    void getAllProjectRoleFailure(String err);

    void deleteProjectRoleSuccess(int position);

    void deleteProjectRoleFailure(String err);
}
