package com.horical.gito.mvp.accounting.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.accounting.detail.presenter.AccountingDetailPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class AccountingDetailModule {
    @Provides
    @PerActivity
    AccountingDetailPresenter provideAccountingDetailPresenter(){
        return new AccountingDetailPresenter();
    }
}
