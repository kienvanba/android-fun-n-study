package com.horical.gito.mvp.logTime.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.model.wrapperModel.WrapperListUser;
import com.horical.gito.mvp.dialog.dialogInputText.InputTextDialog;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.logTime.adapter.LogTimeAdapter;
import com.horical.gito.mvp.logTime.adapter.MemberAdapter;
import com.horical.gito.mvp.logTime.filter.view.LogTimeFilterActivity;
import com.horical.gito.mvp.logTime.injection.component.DaggerLogTimeComponent;
import com.horical.gito.mvp.logTime.injection.component.LogTimeComponent;
import com.horical.gito.mvp.logTime.injection.module.LogTimeModule;
import com.horical.gito.mvp.logTime.presenter.LogTimePresenter;
import com.horical.gito.mvp.popup.dateSelected.DateDto;
import com.horical.gito.mvp.user.list.view.UserActivity;
import com.horical.gito.utils.AnimationUtils;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.utils.LogUtils;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LogTimeActivity extends DrawerActivity implements View.OnClickListener, ILogTimeView {

    public static final String TAG = LogUtils.makeLogTag(LogTimeActivity.class);

    public static final int REQUEST_CODE_FILTER = 0;
    public static final int REQUEST_CODE_USERS = 1;

    @Bind(R.id.ll_log_time_menu)
    LinearLayout llMenu;

    @Bind(R.id.log_time_imv_filter)
    ImageView imvFilter;

    @Bind(R.id.log_time_imv_add)
    ImageView imvAdd;

    @Bind(R.id.log_time_rcv_user_picker)
    RecyclerView rcvUserPicker;

    @Bind(R.id.log_time_imv_show_member)
    ImageView imvShowMember;

    @Bind(R.id.log_time_rcv)
    RecyclerView rcvLogTime;

    @Bind(R.id.log_time_ln_no_data)
    LinearLayout lnNoResults;

    @Inject
    LogTimePresenter mPresenter;

    MemberAdapter mMemberAdapter;

    LogTimeAdapter mLogTimeAdapter;

    private int initialMemberHeight;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_log_time;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_LOG_TIME;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        LogTimeComponent component = DaggerLogTimeComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .logTimeModule(new LogTimeModule())
                .build();

        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        mPresenter.getMemberData();
        if (mPresenter.getCurrentDateFilter() == null) {
            mPresenter.setCurrentDateFilter(new DateDto(
                    getString(R.string.today),
                    DateUtils.getTodayStart(),
                    DateUtils.getTodayEnd()));
        }

        mMemberAdapter = new MemberAdapter(this, mPresenter.getMemberList());
        rcvUserPicker.setHasFixedSize(true);
        rcvUserPicker.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rcvUserPicker.setAdapter(mMemberAdapter);

        mLogTimeAdapter = new LogTimeAdapter(this, mPresenter.getLogTimeList());
        rcvLogTime.setLayoutManager(new StickyHeaderLayoutManager());
        rcvLogTime.setAdapter(mLogTimeAdapter);
    }

    protected void initListener() {
        llMenu.setOnClickListener(this);
        imvAdd.setOnClickListener(this);
        imvFilter.setOnClickListener(this);
        imvShowMember.setOnClickListener(this);

        mMemberAdapter.setOnItemClickListener(new MemberAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                mPresenter.getLogTime(position);
            }
        });

        mLogTimeAdapter.setOnItemClickListener(new LogTimeAdapter.OnItemClickListener() {
            @Override
            public void onApproveClicked(int sectionIndex, int itemIndex) {
                mPresenter.approveAndRejectLogTime(sectionIndex, itemIndex, null);
            }

            @Override
            public void onRejectClicked(final int sectionIndex, final int itemIndex) {
                InputTextDialog textDialog = new InputTextDialog(
                        LogTimeActivity.this,
                        InputTextDialog.DEFAULT_TEXT,
                        new InputTextDialog.OnClickListener() {
                            @Override
                            public void onSave(String text) {
                                if (text != null && !text.isEmpty()) {
                                    mPresenter.approveAndRejectLogTime(
                                            sectionIndex,
                                            itemIndex,
                                            text
                                    );
                                } else {
                                    showErrorDialog(getString(R.string.error_blank_message_reject));
                                }
                            }
                        }
                );
                textDialog.show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_log_time_menu:
                openDrawer();
                break;
            case R.id.log_time_imv_filter:
                Intent intent = new Intent(this, LogTimeFilterActivity.class);
                intent.putExtra(
                        LogTimeFilterActivity.KEY_FILTER,
                        mPresenter.getCurrentDateFilter()
                );
                intent.putExtra(
                        LogTimeFilterActivity.KEY_TYPE,
                        mPresenter.getCurrentType()
                );
                startActivityForResult(intent, REQUEST_CODE_FILTER);
                break;
            case R.id.log_time_imv_add:
                Intent in = new Intent(this, UserActivity.class);
                in.putExtra(
                        UserActivity.KEY_INTENT_USER_LIST,
                        mPresenter.getUserList()
                );
                startActivityForResult(in, REQUEST_CODE_USERS);
                break;
            case R.id.log_time_imv_show_member:
                RotateAnimation rotate = new RotateAnimation(
                        180,
                        0,
                        Animation.RELATIVE_TO_SELF,
                        0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f
                );
                rotate.setDuration(500);
                rotate.setInterpolator(new LinearInterpolator());
                imvShowMember.startAnimation(rotate);
                if (rcvUserPicker.isShown()) {
                    initialMemberHeight = rcvUserPicker.getMeasuredHeight();
                    AnimationUtils.collapse(rcvUserPicker, initialMemberHeight);
                    imvShowMember.setRotation(90);
                } else {
                    AnimationUtils.expand(rcvUserPicker, initialMemberHeight);
                    imvShowMember.setRotation(-90);
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_FILTER) {
                mPresenter.setCurrentDateFilter(
                        (DateDto) data.getSerializableExtra(LogTimeFilterActivity.KEY_FILTER));
                mPresenter.setCurrentType(
                        data.getIntExtra(LogTimeFilterActivity.KEY_TYPE, LogTimeFilterActivity.TYPE_ALL));
                mPresenter.getLogTimes();
            } else if (requestCode == REQUEST_CODE_USERS) {
                WrapperListUser wrapperListUser = (WrapperListUser) data.getSerializableExtra(UserActivity.KEY_INTENT_USER_LIST);
                mMemberAdapter.setMemberList(wrapperListUser.getMyUsers());
                mPresenter.setUserPicked(wrapperListUser);
            }
        }
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void onGetMembersSuccess() {
        mPresenter.getLogTimes();
    }

    @Override
    public void onGetMembersFailure(String message) {
        showErrorDialog(message);
    }

    @Override
    public void onGetLogTimesSuccess() {
        rcvUserPicker.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onGetLogTimesFailure(String error) {
        lnNoResults.setVisibility(View.VISIBLE);
        showErrorDialog(error);
    }

    @Override
    public void onGetLogTimeFailure(String error) {
        lnNoResults.setVisibility(View.VISIBLE);
        showErrorDialog(error);
    }

    @Override
    public void onGetLogTimeSuccess(boolean hasData) {
        if (hasData) {
            lnNoResults.setVisibility(View.GONE);
            mLogTimeAdapter.notifyAllSectionsDataSetChanged();
        } else {
            lnNoResults.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void approveOrRejectSuccess() {
        mLogTimeAdapter.notifyAllSectionsDataSetChanged();
        rcvUserPicker.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void approveOrRejectFailure(String error) {
        showErrorDialog(error);
    }
}