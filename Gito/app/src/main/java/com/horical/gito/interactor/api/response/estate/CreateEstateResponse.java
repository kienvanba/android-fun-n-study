package com.horical.gito.interactor.api.response.estate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.estate.Estate;

/**
 * Created by hoangsang on 3/27/17.
 */

public class CreateEstateResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    public Estate estate;
}
