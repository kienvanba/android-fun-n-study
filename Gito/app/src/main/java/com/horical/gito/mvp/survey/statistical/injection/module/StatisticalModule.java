package com.horical.gito.mvp.survey.statistical.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.survey.statistical.presenter.StatisticalPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by nhattruong251295 on 4/7/2017.
 */
@Module
public class StatisticalModule {
    @PerActivity
    @Provides
    StatisticalPresenter providesStatisticalPresenter(){ return new StatisticalPresenter();}
}
