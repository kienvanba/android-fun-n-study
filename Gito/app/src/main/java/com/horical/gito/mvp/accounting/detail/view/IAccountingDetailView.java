package com.horical.gito.mvp.accounting.detail.view;

import com.horical.gito.base.IView;


public interface IAccountingDetailView extends IView {
    void showLoading();

    void hideLoading();

    void getAllAccountingSuccess();

    void getAllAccountingFailure(String error);

    void deleteAccountingSuccess(int position);

    void deleteAccountingFailure();
}
