package com.horical.gito.mvp.salary.list.fragments.subStaff.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;
import com.horical.gito.mvp.salary.list.fragments.subStaff.view.IListStaffFragmentView;

import java.util.ArrayList;
import java.util.List;

public class ListStaffFragmentPresenter extends BasePresenter implements IListStaffFragmentPresenter {

    public void attachView(IListStaffFragmentView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IListStaffFragmentView getView() {
        return (IListStaffFragmentView) getIView();
    }

    private List<ListStaffDto> mStaffs;

    private void setDummyData() {
        getStaffs().add(new ListStaffDto("Nam A", "Admin", "Hourly", 200, 300, 200));
        getStaffs().add(new ListStaffDto("Nam B", "Coder", "Monthly", 500, 100, 300));
        getStaffs().add(new ListStaffDto("Nam C", "Tester", "Monthly", 400, 200, 250));
        getStaffs().add(new ListStaffDto("Nam D", "QA", "Monthly", 300, 240, 150));
        getStaffs().add(new ListStaffDto("Nam E", "BI", "Monthly", 600, 150, 100));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        setDummyData();
    }

    @Override
    public List<ListStaffDto> getStaffs() {
        if (mStaffs == null) {
            mStaffs = new ArrayList<>();
        }
        return mStaffs;
    }

    @Override
    public void setStaffs(List<ListStaffDto> staffs) {
        this.mStaffs = staffs;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
