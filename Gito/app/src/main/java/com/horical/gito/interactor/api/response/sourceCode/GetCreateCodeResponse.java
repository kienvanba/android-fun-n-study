package com.horical.gito.interactor.api.response.sourceCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Code;

/**
 * Created by thanhle on 4/7/17.
 */

public class GetCreateCodeResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    public Code code;
}