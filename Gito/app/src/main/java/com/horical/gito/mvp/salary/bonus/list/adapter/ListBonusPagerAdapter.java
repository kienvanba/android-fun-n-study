package com.horical.gito.mvp.salary.bonus.list.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.horical.gito.mvp.salary.bonus.list.fragments.BaseListBonusFragment;

import java.util.List;

public class ListBonusPagerAdapter extends FragmentPagerAdapter {

    private List<BaseListBonusFragment> mBonusFragments;

    public ListBonusPagerAdapter(FragmentManager fm, List<BaseListBonusFragment> bonusFragments) {
        super(fm);
        this.mBonusFragments = bonusFragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mBonusFragments.get(position);
    }

    @Override
    public int getCount() {
        return mBonusFragments.size();
    }
}
