package com.horical.gito.mvp.myProject.task.details.fragment.logtime.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.presenter.IOverViewPresenter;
import com.horical.gito.mvp.myProject.task.details.fragment.logtime.view.ILogTimeView;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class LogTimePresenter extends BasePresenter implements IOverViewPresenter {

    private static final String TAG = makeLogTag(LogTimePresenter.class);

    public ILogTimeView getView() {
        return ((ILogTimeView) getIView());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

}
