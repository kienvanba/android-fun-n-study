package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.view.IBrowseView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class BrowsePresenter extends BasePresenter implements IBrowsePresenter {

    private List<String> mList;

    public void attachView(IBrowseView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IBrowseView getView() {
        return (IBrowseView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<String> getListBrowse() {
        return mList;
    }

    public void setListBrowse(List<String> mList) {
        this.mList = mList;
    }
}

