package com.horical.gito.mvp.salary.detail.metric.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.model.Metric;
import com.horical.gito.model.StepParam;
import com.horical.gito.mvp.salary.adapter.CommonSpinnerAdapter;
import com.horical.gito.mvp.salary.detail.metric.adapter.StepParamAdapter;
import com.horical.gito.mvp.salary.detail.metric.injection.component.DaggerIDetailMetricComponent;
import com.horical.gito.mvp.salary.detail.metric.injection.component.IDetailMetricComponent;
import com.horical.gito.mvp.salary.detail.metric.injection.module.DetailMetricModule;
import com.horical.gito.mvp.salary.detail.metric.presenter.DetailMetricPresenter;
import com.horical.gito.utils.CommonUtils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailMetricActivity extends BaseActivity implements View.OnClickListener, IDetailMetricView {

    public static final int MODE_DETAIL = 1;
    public static final int MODE_CREATE = 2;
    public static final int MODE_EDIT = 3;

    @Bind(R.id.ln_back)
    LinearLayout lnBack;

    @Bind(R.id.edt_title)
    EditText edtTitle;

    @Bind(R.id.imv_edit_title)
    ImageView imvEditTitle;

    @Bind(R.id.edt_acronym)
    EditText edtAcronym;

    @Bind(R.id.imv_edit_acronym)
    ImageView imvEditAcronym;

    @Bind(R.id.tv_input_type)
    TextView tvInputType;

    @Bind(R.id.sp_input_type)
    Spinner spInputType;

    @Bind(R.id.tv_calculate_type)
    TextView tvCalculateType;

    @Bind(R.id.sp_calculate_type)
    Spinner spCalculateType;

    @Bind(R.id.tv_input_source)
    TextView tvInputSource;

    @Bind(R.id.sp_input_source)
    Spinner spInputSource;

    @Bind(R.id.tv_isTotal)
    TextView tvIsTotal;

    @Bind(R.id.sw_isTotal)
    SwitchCompat swIsTotal;

    @Bind(R.id.imv_add_param)
    ImageView imvAddParam;

    @Bind(R.id.rcv_step_params)
    RecyclerView rcvStepParams;

    @Bind(R.id.tv_no_data)
    TextView tvNoData;

    @Inject
    DetailMetricPresenter mPresenter;

    private StepParamAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_metric);
        ButterKnife.bind(this);
        IDetailMetricComponent component = DaggerIDetailMetricComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .detailMetricModule(new DetailMetricModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        String metricId = getIntent().getStringExtra(AppConstants.METRIC_ID_KEY);
        if (!TextUtils.isEmpty(metricId)) {
            showLoading();
            mPresenter.requestGetDetailMetric(metricId);
        }

        initData();
        initListener();
    }

    private void initSpinner(final Spinner spinner, String[] items) {
        CommonSpinnerAdapter colorsAdapter = new CommonSpinnerAdapter(this, items, new CommonSpinnerAdapter.ISpinnerAdapterListener() {
            @Override
            public void onCustomItemSelected(String text, View view) {
                CommonUtils.hideSpinnerDropDown(spinner);
                ((TextView) view).setText(text);
            }
        });
        spinner.setAdapter(colorsAdapter);
    }

    private void showSpinner(Spinner spinner, View view) {
        TextView tv = (TextView) view;
        CommonSpinnerAdapter adapter = (CommonSpinnerAdapter) spinner.getAdapter();
        adapter.setTextView(tv);
        spinner.performClick();
    }

    protected void initData() {
        mAdapter = new StepParamAdapter(this, mPresenter.getMetric().getStepParam(), new StepParamAdapter.DetailStaffAdapterListener() {
            @Override
            public void onEditFromTo(int position) {

            }

            @Override
            public void onEditValue(int position) {

            }
        });
        rcvStepParams.setAdapter(mAdapter);
        rcvStepParams.setLayoutManager(new LinearLayoutManager(this));

        tvNoData.setText(getString(R.string.no_data, getString(R.string.step_params)));

        initSpinner(spInputType, getResources().getStringArray(R.array.input_type));
        initSpinner(spCalculateType, getResources().getStringArray(R.array.calculate_type));
        initSpinner(spInputSource, getResources().getStringArray(R.array.input_source));
    }

    private void updatMode(int mode) {
        switch (mode) {
            case MODE_DETAIL:
                break;

            case MODE_CREATE:
                break;

            case MODE_EDIT:
                break;

            default:
                break;
        }
    }

    private void updateData() {
        Metric metric = mPresenter.getMetric();
        edtTitle.setText(metric.getTitle());
        edtAcronym.setText(metric.getAcronym());
        tvInputType.setText(metric.getTitle());
        tvCalculateType.setText(metric.getTitle());
        tvInputSource.setText(metric.getTitle());
        tvIsTotal.setText(metric.isTotal() ? getString(R.string.yes) : getString(R.string.no));
        tvIsTotal.setSelected(metric.isTotal());
        swIsTotal.setSelected(metric.isTotal());
        mAdapter.setDataChanged(metric.getStepParam());
        mAdapter.notifyDataSetChanged();
    }

    protected void initListener() {
        lnBack.setOnClickListener(this);
        imvEditTitle.setOnClickListener(this);
        imvEditAcronym.setOnClickListener(this);
        tvInputType.setOnClickListener(this);
        tvCalculateType.setOnClickListener(this);
        tvInputSource.setOnClickListener(this);
        imvAddParam.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ln_back:
                finish();
                break;

            case R.id.tv_input_type:
                showSpinner(spInputType, v);
                break;

            case R.id.tv_calculate_type:
                showSpinner(spCalculateType, v);
                break;

            case R.id.tv_input_source:
                showSpinner(spInputSource, v);
                break;

            case R.id.imv_add_param:
                mPresenter.getMetric().getStepParam().add(new StepParam());
                mAdapter.notifyDataSetChanged();
                rcvStepParams.smoothScrollToPosition(mAdapter.getItemCount());
                rcvStepParams.setVisibility(View.VISIBLE);
                tvNoData.setVisibility(View.GONE);
                break;

            default:
                break;
        }
    }

    @Override
    public void requestGetDetailMetricSuccess() {
        hideLoading();
        if (mPresenter.getMetric().getStepParam().isEmpty()) {
            rcvStepParams.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
        } else {
            rcvStepParams.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);
        }
        updateData();
    }

    @Override
    public void requestGetDetailMetricError(RestError error) {
        hideLoading();
        showErrorDialog(error.message);
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}