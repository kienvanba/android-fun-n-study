package com.horical.gito.utils;

import android.util.Log;

import java.lang.Math;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HDate {

	public static Integer _startWeekDay = 1; // Sunday 0 is US style, Monday 1 is EURO and International Style
	public static Integer[] _arrMonthDay = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	public static String[] _arrWeekDay = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
	public static String[] _arrMonthName = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

	public static double _morningStartTime = 8.0;
	public static double _morningEndTime = 12.0;

	public static double _afternoonStartTime = 13.0;
	public static double _afternoonEndTime = 17.0;

	public static double _eveningStartTime = 18.0;
	public static double _eveningEndTime = 22.0;

	public Integer year;
	public Integer month;
	public Integer day;

	public Integer hour;
	public Integer minute;
	public Integer second;


    public static void main(String args[]) {
        System.out.println("Hello World!");
    }

    public HDate() {
    	Calendar now = Calendar.getInstance();
		this.year = now.get(Calendar.YEAR);
		this.month = now.get(Calendar.MONTH) + 1; // Note: zero based!
		this.day = now.get(Calendar.DAY_OF_MONTH);
		this.hour = now.get(Calendar.HOUR_OF_DAY);
		this.minute = now.get(Calendar.MINUTE);
		this.second = now.get(Calendar.SECOND);
    }

	public HDate(HDate date) {
    	this.year = date.year;
    	this.month = date.month;
    	this.day = date.day;
    	this.hour = date.hour;
    	this.minute = date.minute;
    	this.second = date.second;
    }

	public HDate(Integer year, Integer month, Integer day) {
    	this.year = year;
    	this.month = month;
    	this.day = day;
    }

	public HDate(Integer year, Integer month, Integer day, Integer hour, Integer minute, Integer second) {
    	this.year = year;
    	this.month = month;
    	this.day = day;
    	this.hour = hour;
    	this.minute = minute;
    	this.second = second;
    }
	public HDate (String dataString){
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		try{
			Date dataDate = format.parse(dataString);
			this.year = dataDate.getYear();
			this.month = dataDate.getMonth();
			this.day = dataDate.getDay();
			this.hour = dataDate.getHours();
			this.minute = dataDate.getMinutes();
			this.second = dataDate.getSeconds();
		}catch (Exception e){
			Log.e("error",e.toString());
		}
	}
	public HDate (Date date){
		this.year = date.getYear();
		this.month = date.getMonth();
		this.day = date.getDay();
		this.hour = date.getHours();
		this.minute = date.getMinutes();
		this.second = date.getSeconds();
	}
	public HDate(Calendar c){
		this.year = c.get(Calendar.YEAR);
		this.month = c.get(Calendar.MONTH)+1; // base zero
		this.day = c.get(Calendar.DAY_OF_MONTH);
		this.hour = c.get(Calendar.HOUR_OF_DAY);
		this.minute = c.get(Calendar.MINUTE);
		this.second = c.get(Calendar.SECOND);
	}
//	public HDate (Date date){
//		this.year = date.getYear();
//		this.month = date.getMonth();
//		this.day = date.getDay();
//		this.hour = date.getHours();
//		this.minute = date.getMinutes();
//		this.second = date.getSeconds();
//	}

    public boolean isLeapYear() {
		Integer y = this.year;
		return ((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0);
	}

	public Integer numberDayInThisMonth() {
		return this.numberDayInThisMonth(-1, -1);
	}

	public Integer numberDayInThisMonth(Integer month, Integer year) {
		Integer m = 0;
		if(month <= -1) {
			m = this.month-1;
		} else {
			m = month;
		}

		Integer y = 0;
		if(year <= -1) {
			y = this.year;
		} else {
			y = year;
		}

		Integer n = _arrMonthDay[m];

		// Feb
		if(m == 1) {
			if(this.isLeapYear()) {
				n++;
			}
		}
		return n;
	}

	public Integer numberDayInThisYear() {
		return this.isLeapYear() ? 366 : 365;
	}

	public Integer numberWeekInThisYear(){
		HDate d = new HDate(this.year, 11, 31);
		return d.getWeekInYear();
	}

	
	// Sunday is 0
	public Integer getDayInWeek() {
		long d = this.dateNumber();
    	long dp = d % 7;
    	return (int)dp;  //  0...6 for Sunday...Saturday
	}

	public Integer getDayInYear() {
		HDate d = new HDate(this);

		Integer n = d.day;
		Integer m = d.month; // 0-11

		for(Integer i=0; i<m; i++) {
			n += _arrMonthDay[i];
		}

		// From March
		if(m >= 2) {
			if(d.isLeapYear()) {
				n++;
			}
		}
		return n;
	}

	public Integer getWeekInYear() {
		HDate d = new HDate(this);

		Integer yDay = d.getDayInYear();
		HDate fDay = new HDate(d);
		fDay.month = 1;
		fDay.day = 1;

		Integer fwDay = fDay.getDayInWeek();

		Integer wd = yDay - (7 - fwDay + _startWeekDay);
		Integer wYear = (int)Math.ceil(wd / 7);
		return wYear;
	}


	public HDate addYear(Integer numberYear) {
		HDate date = new HDate(this);
		date.year += numberYear;
		return date;
	}

	public HDate addMonth(Integer numberMonth) {
		HDate date = new HDate(this);
		Integer y = date.year;
		Integer m = date.month;
		m += numberMonth;

		if(m >= 12) {
			y += (int)Math.floor(m / 12);
        	m = (m % 12);

    	} else if(m < 0) {
	        m = -m;
    	    y -= (int)Math.ceil(m / 12.0);
        	m = (12 - (m % 12) ) % 12;
    	}

		date.year = y;
		date.month = m;
		return date;
	}

	public HDate addDay(Integer numberDay) {
		HDate date = new HDate(this);
		if(numberDay != 0) {
			Integer d = date.day;
			Integer m = date.month;
			Integer y = date.year;

			d += numberDay;
			if(numberDay > 0) {
				while (true) {
					Integer md = date.numberDayInThisMonth(m, y);
					if(d > md) {
						d -= md;
						m++;

						if(m >= 12) {
							m = 0;
							y++;
						}

					} else {
						break;
					}
				}

			} else if(numberDay < 0) {
				while(true) {
					if(d <= 0) {
						m--;

						if(m < 0) {
							m = 11;
							y--;
						}

						Integer md = date.numberDayInThisMonth(m, y);
						d += md;

					} else {
						break;
					}
				}
			}

			date = new HDate(y, m, d);
		}

		return date;
	}

	public HDate addHour(Integer numberHour) {
		HDate date = new HDate(this);
    	Integer hour = date.hour + numberHour;

    	if(hour >= 24) {
        	Integer d = (int)Math.floor(hour / 24);
        	hour = hour % 24;
        	date = date.addDay(d);

    	} else if(hour < 0) {
        	Integer m = -hour;
        	hour = 24 - m % 24;
        	Integer d = (int)Math.ceil(m / 24.0);
        	date = date.addDay(-d);
    	}

    	date.hour = hour;
    	return date;
	}

	public HDate addMinute(Integer numberMinute) {
		HDate date = new HDate(this);
    	Integer minute = date.minute + numberMinute;

    	if(minute >= 60) {
        	Integer d = (int)Math.floor(minute / 60);
        	minute = minute % 60;
        	date = date.addHour(d);

    	} else if(minute < 0) {
        	Integer m = -minute;
        	minute = 60 - m % 60;
        	Integer d = (int)Math.ceil(m / 60.0);
        	date = date.addHour(-d);
    	}

		date.minute = minute;
    	return date;
	}

	public HDate addSecond(Integer numberSecond) {
		HDate date = new HDate(this);
    	Integer second = date.second + numberSecond;

    	if(second >= 60) {
        	Integer d = (int)Math.floor(second / 60);
        	second = second % 60;
        	date = date.addMinute(d);

    	} else if(second < 0) {
        	Integer m = -second;
        	second = 60 - m % 60;
        	Integer d = (int)Math.ceil(m / 60.0);
        	date = date.addMinute(-d);
    	}

		date.second = second;
    	return date;
	}

	public long intervalNumber() {
    	long inter = this.dateNumber() * 24;
    	Integer hour = this.hour;
    	Integer minute = this.minute;

    	if(hour > 0) inter *= (hour * 3600);
    	if(minute > 0) inter *= (minute * 60);
    	inter += this.second;
    	return inter;
	}

	public long dateNumber() {
    	Integer y = this.year - 1;
    	long d = y * 365 + this.getDayInYear();
    	long d4 = (long)Math.floor(y/4) - (long)Math.floor(y/100) + (long)Math.floor(y/400);
    	d += d4;
    	return d;
	}

	public long numberIntervalFrom(HDate date) {
    	long to = this.intervalNumber();
    	long from = date.intervalNumber();
    	long r = to - from;
    	return r;
	}

	public double numberHourFrom(HDate date) {
    	long interval = this.numberIntervalFrom(date);
    	return interval / 3600.0;
	}

	public long numberDayFrom(HDate date) {
    	long to = this.dateNumber();
    	long from = date.dateNumber();
    	long r = to - from;
    	return r;
	}

	public double getHourFully() {
		return this.hour + this.minute / 60.0 + this.second / 3600.0;
	}

	public double numberWorkingHourFrom(HDate date) {

    	double startHourOrigin = date.getHourFully();
    	double endHourOrigin = this.getHourFully();

    	if(endHourOrigin <= startHourOrigin) return 0;

    	double totalHour = 0;
    	double startHour = Math.max(_morningStartTime, startHourOrigin);
    	if(startHour < endHourOrigin) {
        	double endHour = Math.min(_morningEndTime, endHourOrigin);
        	if(endHour > startHour) {
            	totalHour += (endHour - startHour);
        	}

        	startHour = Math.max(_afternoonStartTime, startHourOrigin);
        	if(startHour < endHourOrigin) {
	            endHour = Math.min(_afternoonEndTime, endHourOrigin);
	            if(endHour > startHour) {
	                totalHour += (endHour - startHour);
	            }

	            startHour = Math.max(_eveningStartTime, startHourOrigin);
	            if(startHour < endHourOrigin) {
	                endHour = Math.min(_eveningEndTime, endHourOrigin);
	                if(endHour > startHour) {
	                    totalHour += (endHour - startHour);
	                }
	            }
	        }
	    }

    	return totalHour;
	}


	public HDate tomorrow() {
		return this.addDay(1);
	}

	public HDate yesterday() {
		return this.addDay(-1);
	}

	public HDate lastMonth() {
		return this.addMonth(1);
	}

	public HDate nextMonth() {
		return this.addMonth(-1);
	}

	public HDate lastYear() {
		return this.addYear(-1);
	}

	public HDate nextYear() {
		return this.addYear(1);
	}


	public String dayName() {
		HDate d = new HDate(this);

		Integer i = d.getDayInWeek();
		return _arrWeekDay[i];
	}

	public String monthName() {
		return this.monthName(-1);
	}

	public String monthName(Integer m) {
		if(m <= -1) {
			return _arrMonthName[this.month - 1];
		} else {
			return _arrMonthName[m - 1];
		}
	}

	public String monthName(boolean hasShortString) {
		String s = _arrMonthName[this.month-1];
		if(hasShortString) {
			return s.substring(0, 3); // Start, End Index

		}
		return s;
	}

	public String[] listWeekDayName() {
		return this.listWeekDayName(false);
	}

	public String[] listWeekDayName(boolean hasShortString) {
		// return Sunday, Monday, ...
		String[] arr = _arrWeekDay;
		String[] result = new String[7];
		for(Integer i=_startWeekDay; i<_startWeekDay+7; i++) {
			String s = arr[i%7];
			if(hasShortString) {
				result[result.length] = s.substring(0, 3);
			} else {
				result[result.length] = s;
			}
		}

		return result;
	}

	public String[] listMonthName() {
		return this.listMonthName(false);
	}

	public String[] listMonthName(boolean hasShortString) {
		String[] arr = new String[12];

		for(Integer i=0; i<12; i++) {
			if(hasShortString) {
				arr[i] = (_arrMonthName[i]).substring(0, 3);
			} else {
				arr[i] = _arrMonthName[i];
			}
		}

		return arr;
	}

	public HDate[] getListDayLastWeek() {
		return this.addDay(-7).getListDayThisWeek();
	}

	public HDate[] getListDayNextWeek() {
		return this.addDay(7).getListDayThisWeek();
	}

	public HDate[] getListDayThisWeek() {
		return this.getListDayThisWeek(false);
	}

	public HDate[] getListDayThisWeek(boolean hasShortString) {
		HDate date = new HDate(this);
		Integer wd = date.getDayInWeek();
		Integer od = Math.max(wd - _startWeekDay, 0);
		date = date.addDay(-od);

		HDate[] arr = new HDate[7];
		for(Integer i=0; i<7; i++) {
			arr[i] = date.addDay(i);
		}
		return arr;
	}

 	public HDate[] getListDayByWeekIndex(Integer weekIndex) {
		Integer wn = weekIndex - 1;
    	HDate date = new HDate(this.year, 0, 1);
    	Integer wd = date.getDayInWeek();
    	Integer od = (_startWeekDay + 7 - wd) % 7;

    	Integer d = (wn * 7) + od;
    	date = date.addDay(d);

    	HDate[] arr = new HDate[7];
    	for(Integer i=0; i<7; i++) {
        	arr[i] = date.addDay(i);
    	}
    	return arr;
	}

	public HDate[] getListDayLastMonth() {
		return this.lastMonth().getListDayThisMonth();
	}

	public HDate[] getListDayNextMonth() {
		return this.nextMonth().getListDayThisMonth();
	}

	public HDate[] getListDayThisMonth() {
		HDate d = new HDate(this);

		Integer y = d.year;
		Integer m = d.month;
		Integer md = d.numberDayInThisMonth();
	
		d.day = 1;
		Integer wd = d.getDayInWeek();

		Integer l = (7 + (wd - _startWeekDay)) % 7;
		HDate s = d.addDay(-l);

		Integer n = l + md;
		n += (7 - n%7);

		HDate[] arr = new HDate[n];
		for(Integer i=0; i<n ; i++) {
			arr[i] = s.addDay(i);
		}

		return arr;
	}

	//function get date 3 chu
	public String shortDisplay() {
		return this.monthName(true) + " " + this.day + " " + this.year; 
	}
	//convert to string format: day / month / year
	public String toStringDayMonthYear(){ return day.toString() +"/" + month.toString() + "/"+ year.toString();}
	//convert to string format: month/year
	public String toStringMonthYear(){ return month.toString() + "/"+ year.toString();}
	//convert to string format: day/month/year  hour/minute period
	public String toStringFull(){
		return day.toString() +"/" + month.toString() + "/"+ year.toString() + "    " + hour.toString() + ":" + minute.toString();
	}
	//convert from string
    public void setFromString( String dataString){
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		try{
			Date dataDate = format.parse(dataString);
			this.year = dataDate.getYear();
			this.month = dataDate.getMonth();
			this.day = dataDate.getDay();
			this.hour = dataDate.getHours();
			this.minute = dataDate.getMinutes();
			this.second = dataDate.getSeconds();
		}catch (Exception e){
			Log.e("error",e.toString());
		}
    }
    public String toDataString(){
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		Calendar c = Calendar.getInstance();
		c.set(year,month,day,hour==null?0:hour,minute==null?0:minute,second==null?0:second);
        Date date = c.getTime();
        return format.format(date);
    }
}