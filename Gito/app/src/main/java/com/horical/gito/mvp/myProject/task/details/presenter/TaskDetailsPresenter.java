package com.horical.gito.mvp.myProject.task.details.presenter;

import android.util.Log;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.task.GetCreateTaskResponse;
import com.horical.gito.model.Task;
import com.horical.gito.mvp.myProject.task.details.view.ITaskDetailsView;
import com.horical.gito.mvp.myProject.task.details.view.TaskDetailsActivity;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class TaskDetailsPresenter extends BasePresenter implements ITaskDetailsPresenter{

    private static final String TAG = makeLogTag(TaskDetailsPresenter.class);

    public ITaskDetailsView getView() {
        return (ITaskDetailsView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void createTask(final Task task) {
        String projectId = GitOStorage.getInstance().getProject().getId();

        getApiManager().createTask(projectId, task, new ApiCallback<GetCreateTaskResponse>() {
            @Override
            public void success(GetCreateTaskResponse res) {
                getView().createTaskSuccess(res.task);
            }

            @Override
            public void failure(RestError error) {
                getView().createTaskFailure(error.message);
            }
        });
    }

    @Override
    public void updateOverViewTask(final Task task) {
        //String projectId = GitOStorage.getInstance().getProject().getId();
    }
}
