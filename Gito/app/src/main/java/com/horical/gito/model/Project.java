package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Project implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("creatorId")
    @Expose
    private String creatorId;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("projectId")
    @Expose
    private String projectId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("avatar")
    @Expose
    private TokenFile avatar;

    @SerializedName("projectDefault")
    @Expose
    private boolean projectDefault;

    @SerializedName("feature")
    @Expose
    private Feature feature;

    @SerializedName("desc")
    @Expose
    private String description;

    @SerializedName("modifyDate")
    @Expose
    private Date modifyDate;

    @SerializedName("createDate")
    @Expose
    private Date createDate;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("members")
    @Expose
    private List<User> members;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TokenFile getAvatar() {
        return avatar;
    }

    public void setAvatar(TokenFile avatar) {
        this.avatar = avatar;
    }

    public boolean isProjectDefault() {
        return projectDefault;
    }

    public void setProjectDefault(boolean projectDefault) {
        this.projectDefault = projectDefault;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }
}
