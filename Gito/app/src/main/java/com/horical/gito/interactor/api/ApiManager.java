package com.horical.gito.interactor.api;

import com.horical.gito.AppConstants;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.ApiServices;
import com.horical.gito.interactor.api.network.DocServices;
import com.horical.gito.interactor.api.network.FileServices;
import com.horical.gito.interactor.api.network.RestCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.TodoNote.Note.NoteRequest;
import com.horical.gito.interactor.api.request.TodoNote.Note.UpdateAttachFileNoteRequest;
import com.horical.gito.interactor.api.request.TodoNote.Todo.TodoRequest;
import com.horical.gito.interactor.api.request.caseStudy.CreateCaseStudyRequest;
import com.horical.gito.interactor.api.request.caseStudy.GetAllCaseStudyRequest;
import com.horical.gito.interactor.api.request.caseStudy.UpdateAttachFileRequest;
import com.horical.gito.interactor.api.request.caseStudy.UpdateCaseStudyRequest;
import com.horical.gito.interactor.api.request.caseStudy.UpdateStatusCaseStudyRequest;
import com.horical.gito.interactor.api.request.changePassword.ChangePassWordRequest;
import com.horical.gito.interactor.api.request.companySetting.CreateDepartmentRequest;
import com.horical.gito.interactor.api.request.companySetting.CreateLevelRequest;
import com.horical.gito.interactor.api.request.component.CreateComponentRequest;
import com.horical.gito.interactor.api.request.file.UploadFileResponse;
import com.horical.gito.interactor.api.request.leaving.CreateLeavingRequest;
import com.horical.gito.interactor.api.request.meeting.MeetingRequest;
import com.horical.gito.interactor.api.request.mypage.GetMyPageRequest;
import com.horical.gito.interactor.api.request.project.CreateProjectRequest;
import com.horical.gito.interactor.api.request.project.GetLogTimeRequest;
import com.horical.gito.interactor.api.request.project.GetMembersHaveLogTimeRequest;
import com.horical.gito.interactor.api.request.project.UpdateLogTimeRequest;
import com.horical.gito.interactor.api.request.project.UpdateProjectRequest;
import com.horical.gito.interactor.api.request.report.MemberRequest;
import com.horical.gito.interactor.api.request.report.NewReportRequest;
import com.horical.gito.interactor.api.request.report.RejectApprovedRequest;
import com.horical.gito.interactor.api.request.room.RoomRequest;
import com.horical.gito.interactor.api.request.salary.inputAddon.CreateInputAddonRequest;
import com.horical.gito.interactor.api.request.salary.inputAddon.FilterInputAddonRequest;
import com.horical.gito.interactor.api.request.salary.stepParam.AddStepParamRequest;
import com.horical.gito.interactor.api.request.salary.stepParam.DeleteStepParamRequest;
import com.horical.gito.interactor.api.request.user.CreateMemberRequest;
import com.horical.gito.interactor.api.request.user.ForgotPasswordRequest;
import com.horical.gito.interactor.api.request.user.LoginRequest;
import com.horical.gito.interactor.api.request.user.RegisterRequest;
import com.horical.gito.interactor.api.request.user.UpdateMemberRequest;
import com.horical.gito.interactor.api.request.version.Request;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.accounting.GetAllAccountingResponse;
import com.horical.gito.interactor.api.response.accounting.GetCreateAccountingResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetAllCaseStudyResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetCreateCaseStudyResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetUpdateAttachFileResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetUpdateCaseStudyResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetUpdateStatusCaseStudyResponse;
import com.horical.gito.interactor.api.response.checkin.GetAllCheckinResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllCardResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllHistoryResponse;
import com.horical.gito.interactor.api.response.estate.CreateWarehouseEstateResponse;
import com.horical.gito.interactor.api.response.estate.GetAllWarehourseEstateResponse;
import com.horical.gito.interactor.api.response.project.GetLogTimeResponse;
import com.horical.gito.interactor.api.response.recruitment.RecruitmentResponse;
import com.horical.gito.interactor.api.response.report.GroupResponse;
import com.horical.gito.interactor.api.response.salary.metric.param.GetAddParamResponse;
import com.horical.gito.interactor.api.response.salary.metric.param.GetDeleteParamResponse;
import com.horical.gito.interactor.api.response.salary.staff.GetAllSalaryStaffResponse;
import com.horical.gito.interactor.api.response.salary.staff.GetCreateSalaryStaffResponse;
import com.horical.gito.interactor.api.response.salary.staff.GetDeleteSalaryStaffResponse;
import com.horical.gito.interactor.api.response.salary.staff.GetDetailSalaryStaffResponse;
import com.horical.gito.interactor.api.response.salary.staff.GetUpdateSalaryStaffResponse;
import com.horical.gito.interactor.api.response.sourceCode.GetAllCodeResponse;
import com.horical.gito.interactor.api.response.companySetting.CreateDepartmentResponse;
import com.horical.gito.interactor.api.response.companySetting.CreateLevelResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllCompanyRoleResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllDepartmentResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllHolidayResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllLevelResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllProjectRoleResponse;
import com.horical.gito.interactor.api.response.companySetting.GetCompanyInfoResponse;
import com.horical.gito.interactor.api.response.companySetting.GetDevicesResponse;
import com.horical.gito.interactor.api.response.companySetting.GetFeatureResponse;
import com.horical.gito.interactor.api.response.companySetting.GetInfoQuotaResponse;
import com.horical.gito.interactor.api.response.companySetting.GetSummaryQuotaResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateDepartmentResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateFeatureResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateInfoResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateLevelResponse;
import com.horical.gito.interactor.api.response.component.CreateComponentResponse;
import com.horical.gito.interactor.api.response.component.GetAllComponentResponse;
import com.horical.gito.interactor.api.response.document.GetAllDocumentResponse;
import com.horical.gito.interactor.api.response.estate.CreateEstateResponse;
import com.horical.gito.interactor.api.response.estate.GetAllEstatesResponse;
import com.horical.gito.interactor.api.response.group.GetAllGroupResponse;
import com.horical.gito.interactor.api.response.leaving.CreateLeavingResponse;
import com.horical.gito.interactor.api.response.leaving.GetAllLeavingResponse;
import com.horical.gito.interactor.api.response.leaving.GetLeavingByIdResponse;
import com.horical.gito.interactor.api.response.meeting.GetAllMeetingResponse;
import com.horical.gito.interactor.api.response.meeting.GetCreateMeetingResponse;
import com.horical.gito.interactor.api.response.meeting.GetUpdateMeetingResponse;
import com.horical.gito.interactor.api.response.mypage.GetMyPageResponse;
import com.horical.gito.interactor.api.response.note.GetAllNoteResponse;
import com.horical.gito.interactor.api.response.note.GetCreateNoteResponse;
import com.horical.gito.interactor.api.response.note.GetUpdateNoteResponse;
import com.horical.gito.interactor.api.response.project.CreateProjectResponse;
import com.horical.gito.interactor.api.response.project.GetAllProjectResponse;
import com.horical.gito.interactor.api.response.project.GetMemberAndRoleResponse;
import com.horical.gito.interactor.api.response.project.GetMemberResponse;
import com.horical.gito.interactor.api.response.project.GetMembersHaveLogTimeResponse;
import com.horical.gito.interactor.api.response.project.GetProjectResponse;
import com.horical.gito.interactor.api.response.project.UpdateLogTimeResponse;
import com.horical.gito.interactor.api.response.project.UpdateProjectResponse;
import com.horical.gito.interactor.api.response.report.AllReportResponse;
import com.horical.gito.interactor.api.response.report.MemberResponse;
import com.horical.gito.interactor.api.response.room.GetAllRoomResponse;
import com.horical.gito.interactor.api.response.room.GetCreateRoomResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetFilterInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetAllInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetCreateInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetDeleteInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetDetailInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetUpdateInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.metric.GetAllMetricResponse;
import com.horical.gito.interactor.api.response.salary.metric.GetCreateMetricResponse;
import com.horical.gito.interactor.api.response.salary.metric.GetDeleteMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metric.GetDetailMetricResponse;
import com.horical.gito.interactor.api.response.salary.metric.GetUpdateMetricResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetAddMetricMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetAllMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetCreateMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetDeleteMetricMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetDetailMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetUpdateMetricGroupResponse;
import com.horical.gito.interactor.api.response.sourceCode.GetCreateCodeResponse;
import com.horical.gito.interactor.api.response.summary.GetSummaryResponse;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyApprovedResponse;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyItemResponse;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyPendingResponse;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyResponse;
import com.horical.gito.interactor.api.response.survey.GetSubmiterResponse;
import com.horical.gito.interactor.api.response.task.GetAllTaskResponse;
import com.horical.gito.interactor.api.response.task.GetCreateTaskResponse;
import com.horical.gito.interactor.api.response.task.GetDeleteTaskResponse;
import com.horical.gito.interactor.api.response.todolist.GetAllTodoListResponse;
import com.horical.gito.interactor.api.response.todolist.GetCreateTodoListResponse;
import com.horical.gito.interactor.api.response.todolist.GetUpdateTodoListResponse;
import com.horical.gito.interactor.api.response.user.ForgotPasswordResponse;
import com.horical.gito.interactor.api.response.user.GetAllUserResponse;
import com.horical.gito.interactor.api.response.user.GetCreateMemberResponse;
import com.horical.gito.interactor.api.response.user.LoginResponse;
import com.horical.gito.interactor.api.response.user.RegisterResponse;
import com.horical.gito.interactor.api.response.version.GetAllVersionResponse;
import com.horical.gito.interactor.api.response.version.GetVersionResponse;
import com.horical.gito.interactor.event.EventManager;
import com.horical.gito.interactor.event.type.InvalidTokenType;
import com.horical.gito.interactor.prefer.PreferManager;
import com.horical.gito.model.Accounting;
import com.horical.gito.model.Department;
import com.horical.gito.model.estate.Estate;
import com.horical.gito.model.Feature;
import com.horical.gito.model.Information;
import com.horical.gito.model.InputAddon;
import com.horical.gito.model.Level;
import com.horical.gito.model.Metric;
import com.horical.gito.model.Reports;
import com.horical.gito.model.Staff;
import com.horical.gito.model.Task;
import com.horical.gito.model.estate.WarehouseEstate;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Retrofit;

public class ApiManager {

    private ApiServices mApiServices;
    private FileServices mFileServices;
    private DocServices mDocServices;
    private PreferManager mPreferManager;
    private EventManager mEventManager;

    public ApiManager(Retrofit retrofitApi, Retrofit retrofitFile, Retrofit retrofitDoc, PreferManager preferManager, EventManager eventManager) {
        mApiServices = retrofitApi.create(ApiServices.class);
        mFileServices = retrofitFile.create(FileServices.class);
        mDocServices = retrofitDoc.create(DocServices.class);
        mPreferManager = preferManager;
        mEventManager = eventManager;
    }

    // ==== GET TOKEN == //
    private String createToken() {
        return AppConstants.BEGIN_TOKEN
                + mPreferManager.getToken().substring(8, mPreferManager.getToken().length() - 8)
                + AppConstants.END_TOKEN;
    }

    private boolean handleSpecialCode(RestError error) {
        if (error.code == AppConstants.ERROR_CODE_TOKEN_FAILED) {
            mEventManager.sendEvent(new InvalidTokenType());
            return true;
        }
        return false;
    }

    // ===== Update File ===== //
    public void uploadFile(final MultipartBody.Part file, RequestBody status, final ApiCallback<UploadFileResponse> callback) {
        mFileServices.uploadFile(createToken(), file, status)
                .enqueue(new RestCallback<UploadFileResponse>() {
                    @Override
                    public void success(UploadFileResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // ===== Register ===== //
    public void register(final RegisterRequest registerRequest, final ApiCallback<RegisterResponse> callback) {
        mApiServices.register("application/json", registerRequest)
                .enqueue(new RestCallback<RegisterResponse>() {
                    @Override
                    public void success(RegisterResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // ==== Login ==== //
    public void login(final LoginRequest loginRequest, final ApiCallback<LoginResponse> callback) {
        mApiServices.login("application/json", loginRequest)
                .enqueue(new RestCallback<LoginResponse>() {
                    @Override
                    public void success(LoginResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // ==== Logout ==== //
    public void logout(final ApiCallback<BaseResponse> callback) {
        mApiServices.logout(createToken())
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // ========= Forgot password =========== //
    public void forgotPassword(final ForgotPasswordRequest forgotPasswordRequest, final ApiCallback<ForgotPasswordResponse> callback) {
        mApiServices.forgotPassword(forgotPasswordRequest)
                .enqueue(new RestCallback<ForgotPasswordResponse>() {
                    @Override
                    public void success(ForgotPasswordResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // ========== My Page ========== //
    public void getMyPage(final GetMyPageRequest request, final ApiCallback<GetMyPageResponse> callback) {
        mApiServices.getMyPage(createToken(), request)
                .enqueue(new RestCallback<GetMyPageResponse>() {
                    @Override
                    public void success(GetMyPageResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        callback.failure(error);
                    }
                });
    }

    // ========== Get All Project ========== //
    public void getAllProject(final ApiCallback<GetAllProjectResponse> callback) {
        mApiServices.getAllProject(createToken())
                .enqueue(new RestCallback<GetAllProjectResponse>() {
                    @Override
                    public void success(GetAllProjectResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // ========== Create Project ========= //
    public void createProject(CreateProjectRequest createProjectRequest, final ApiCallback<CreateProjectResponse> callback) {
        mApiServices.createProject(createToken(), createProjectRequest)
                .enqueue(new RestCallback<CreateProjectResponse>() {
                    @Override
                    public void success(CreateProjectResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // ========== Get Project By Id =========== //
    public void getProject(String projectId, final ApiCallback<GetProjectResponse> callback) {
        mApiServices.getProject(createToken(), projectId)
                .enqueue(new RestCallback<GetProjectResponse>() {
                    @Override
                    public void success(GetProjectResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get Member And Role ============== //
    public void getMemberAndRole(String projectId, final ApiCallback<GetMemberAndRoleResponse> callback) {
        mApiServices.getMemberAndRole(createToken(), projectId)
                .enqueue(new RestCallback<GetMemberAndRoleResponse>() {
                    @Override
                    public void success(GetMemberAndRoleResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    //=========== Update Project =============== //
    public void updateProject(String projectId, UpdateProjectRequest project, final ApiCallback<UpdateProjectResponse> callback) {
        mApiServices.updateProject(createToken(), projectId, project)
                .enqueue(new RestCallback<UpdateProjectResponse>() {
                    @Override
                    public void success(UpdateProjectResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    //=========== Get All Version ============//
    public void getAllVersion(String projectId, final ApiCallback<GetAllVersionResponse> callback) {
        mApiServices.getAllVersion(createToken(), projectId)
                .enqueue(new RestCallback<GetAllVersionResponse>() {
                    @Override
                    public void success(GetAllVersionResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteVersion(String projectId, String versionId, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteVersion(createToken(), projectId, versionId)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getVersion(String projectId, String versionId, final ApiCallback<GetVersionResponse> callback) {
        mApiServices.getVersion(createToken(), projectId, versionId)
                .enqueue(new RestCallback<GetVersionResponse>() {
                    @Override
                    public void success(GetVersionResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createVersion(String projectId, Request request, final ApiCallback<BaseResponse> callback) {
        mApiServices.createVersion(createToken(), projectId, request)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateVersion(String projectId, String versionId, Request request, final ApiCallback<GetVersionResponse> callback) {
        mApiServices.updateVersion(createToken(), projectId, versionId, request)
                .enqueue(new RestCallback<GetVersionResponse>() {
                    @Override
                    public void success(GetVersionResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All component =========== //
    public void getAllComponent(String projectId, final ApiCallback<GetAllComponentResponse> callback) {
        mApiServices.getAllComponent(createToken(), projectId)
                .enqueue(new RestCallback<GetAllComponentResponse>() {
                    @Override
                    public void success(GetAllComponentResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    //========= Delete Component ==================//
    public void deleteComponent(String projectId, String componentId, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteComponent(createToken(), projectId, componentId)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    //============ Create Component ==============//
    public void createComponent(String projectId, CreateComponentRequest request, final ApiCallback<CreateComponentResponse> callback) {
        mApiServices.createComponent(createToken(), projectId, request)
                .enqueue(new RestCallback<CreateComponentResponse>() {
                    @Override
                    public void success(CreateComponentResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    //Get All group
    public void getAllGroup(String projectId, final ApiCallback<GetAllGroupResponse> callback) {
        mApiServices.getAllGroup(createToken(), projectId)
                .enqueue(new RestCallback<GetAllGroupResponse>() {
                    @Override
                    public void success(GetAllGroupResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All Document =========== //
    public void getAllDocument(String folderId, String projectId, final ApiCallback<GetAllDocumentResponse> callback) {
        mApiServices.getAllDocument(createToken(), projectId)
                .enqueue(new RestCallback<GetAllDocumentResponse>() {
                    @Override
                    public void success(GetAllDocumentResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get Summary ===========//
    public void getSummary(final String projectId, final ApiCallback<GetSummaryResponse> callback) {
        mApiServices.getSummary(createToken(), projectId)
                .enqueue(new RestCallback<GetSummaryResponse>() {
                    @Override
                    public void success(GetSummaryResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All Task =========== //
    public void getAllTask(String projectId, final ApiCallback<GetAllTaskResponse> callback) {
        mApiServices.getAllTask(createToken(), projectId)
                .enqueue(new RestCallback<GetAllTaskResponse>() {
                    @Override
                    public void success(GetAllTaskResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    //========== Get Create Task Response ==========//
    public void createTask(String projectID, Task task, final ApiCallback<GetCreateTaskResponse> callback) {
        mApiServices.createTask(createToken(), projectID, task)
                .enqueue(new RestCallback<GetCreateTaskResponse>() {
                    @Override
                    public void success(GetCreateTaskResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    //========== Get Delete Task Response ==========//
    public void deleteTask(String projectID, String taskID, final ApiCallback<GetDeleteTaskResponse> callback) {
        mApiServices.deleteTask(createToken(), projectID, taskID)
                .enqueue(new RestCallback<GetDeleteTaskResponse>() {
                    @Override
                    public void success(GetDeleteTaskResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All Code =========== //
    public void getAllCode(String projectId, int perPage, int page, final ApiCallback<GetAllCodeResponse> callback) {
        mApiServices.getAllCode(createToken(), projectId, perPage, page)
                .enqueue(new RestCallback<GetAllCodeResponse>() {
                    @Override
                    public void success(GetAllCodeResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createCode(String projectId, String nameCode, final ApiCallback<GetCreateCodeResponse> callback) {
        mApiServices.createCode(createToken(), projectId, nameCode)
                .enqueue(new RestCallback<GetCreateCodeResponse>() {
                    @Override
                    public void success(GetCreateCodeResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Meeting =========== //
    public void getAllMeeting(final ApiCallback<GetAllMeetingResponse> callback) {
        mApiServices.getAllMeeting(createToken())
                .enqueue(new RestCallback<GetAllMeetingResponse>() {
                    @Override
                    public void success(GetAllMeetingResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createMeeting(MeetingRequest meeting, final ApiCallback<GetCreateMeetingResponse> callback) {
        mApiServices.createMeeting(createToken(), meeting)
                .enqueue(new RestCallback<GetCreateMeetingResponse>() {
                    @Override
                    public void success(GetCreateMeetingResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteMeeting(String meetingId, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteMeeting(createToken(), meetingId)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }


    public void updateContentMeeting(String meetingId, MeetingRequest request, final ApiCallback<GetUpdateMeetingResponse> callback) {
        mApiServices.updateContentMeeting(createToken(), meetingId, request)
                .enqueue(new RestCallback<GetUpdateMeetingResponse>() {
                    @Override
                    public void success(GetUpdateMeetingResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }


    //========= Get all Room =================//
    public void getAllRoom(final ApiCallback<GetAllRoomResponse> callback) {
        mApiServices.getAllRoom(createToken())
                .enqueue(new RestCallback<GetAllRoomResponse>() {
                    @Override
                    public void success(GetAllRoomResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createRoom(RoomRequest room, final ApiCallback<GetCreateRoomResponse> callback) {
        mApiServices.createRoom(createToken(), room)
                .enqueue(new RestCallback<GetCreateRoomResponse>() {
                    @Override
                    public void success(GetCreateRoomResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All Check-in =========== //
    public void getAllCheckin(final ApiCallback<GetAllCheckinResponse> callback) {
        mApiServices.getAllCheckin(createToken())
                .enqueue(new RestCallback<GetAllCheckinResponse>() {
                    @Override
                    public void success(GetAllCheckinResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All TodoList =========== //
    public void getAllTodoList(final ApiCallback<GetAllTodoListResponse> callback) {
        mApiServices.getAllTodoList(createToken())
                .enqueue(new RestCallback<GetAllTodoListResponse>() {
                    @Override
                    public void success(GetAllTodoListResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Delete TodoList =========== //
    public void deleteTodoList(String todolistId, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteTodoList(createToken(), todolistId)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Create TodoList =========== //
    public void createTodoList(TodoRequest todoList, final ApiCallback<GetCreateTodoListResponse> callback) {
        mApiServices.createTodoList(createToken(), todoList)
                .enqueue(new RestCallback<GetCreateTodoListResponse>() {
                    @Override
                    public void success(GetCreateTodoListResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateContentTodo(String todoId, TodoRequest request, final ApiCallback<GetUpdateTodoListResponse> callback) {
        mApiServices.updateContentTodo(createToken(), todoId, request)
                .enqueue(new RestCallback<GetUpdateTodoListResponse>() {
                    @Override
                    public void success(GetUpdateTodoListResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All Note =========== //
    public void getAllNote(final ApiCallback<GetAllNoteResponse> callback) {
        mApiServices.getAllNote(createToken())
                .enqueue(new RestCallback<GetAllNoteResponse>() {
                    @Override
                    public void success(GetAllNoteResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Delete Note =========== //
    public void deleteNote(String noteId, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteNote(createToken(), noteId)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Create Note =========== //
    public void createNote(NoteRequest request, final ApiCallback<GetCreateNoteResponse> callback) {
        mApiServices.createNote(createToken(), request)
                .enqueue(new RestCallback<GetCreateNoteResponse>() {
                    @Override
                    public void success(GetCreateNoteResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateContentNote(String noteId, NoteRequest request, final ApiCallback<GetUpdateNoteResponse> callback) {
        mApiServices.updateContentNote(createToken(), noteId, request)
                .enqueue(new RestCallback<GetUpdateNoteResponse>() {
                    @Override
                    public void success(GetUpdateNoteResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteAttachFileNote(String noteId, UpdateAttachFileNoteRequest attachFileRequest, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteFileNote(createToken(), noteId, attachFileRequest)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All Accounting =========== //
    public void getAllAccounting(final ApiCallback<GetAllAccountingResponse> callback) {
        mApiServices.getAllAccounting(createToken())
                .enqueue(new RestCallback<GetAllAccountingResponse>() {
                    @Override
                    public void success(GetAllAccountingResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Delete Accounting =========== //
    public void deleteAccounting(String accountingId, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteAccounting(createToken(), accountingId)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Create Accounting =========== //
    public void createAccounting(Accounting accounting, final ApiCallback<GetCreateAccountingResponse> callback) {
        mApiServices.createAccounting(createToken(), accounting)
                .enqueue(new RestCallback<GetCreateAccountingResponse>() {
                    @Override
                    public void success(GetCreateAccountingResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All CaseStudy =========== //
    public void getAllCaseStudy(GetAllCaseStudyRequest caseStudyRequest, final ApiCallback<GetAllCaseStudyResponse> callback) {
        mApiServices.getAllCaseStudy(createToken(), caseStudyRequest)
                .enqueue(new RestCallback<GetAllCaseStudyResponse>() {
                    @Override
                    public void success(GetAllCaseStudyResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteCaseStudy(String caseStudyId, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteCaseStudy(createToken(), caseStudyId)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createCaseStudy(CreateCaseStudyRequest caseStudyRequest, final ApiCallback<GetCreateCaseStudyResponse> callback) {
        mApiServices.createCaseStudy(createToken(), caseStudyRequest)
                .enqueue(new RestCallback<GetCreateCaseStudyResponse>() {
                    @Override
                    public void success(GetCreateCaseStudyResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateContentCaseStudy(String caseStudyId, UpdateCaseStudyRequest caseStudyRequest, final ApiCallback<GetUpdateCaseStudyResponse> callback) {
        mApiServices.updateCaseStudy(createToken(), caseStudyId, caseStudyRequest)
                .enqueue(new RestCallback<GetUpdateCaseStudyResponse>() {
                    @Override
                    public void success(GetUpdateCaseStudyResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateStatusCaseStudy(String caseStudyId, UpdateStatusCaseStudyRequest caseStudyRequest, final ApiCallback<GetUpdateStatusCaseStudyResponse> callback) {
        mApiServices.updateStatusCaseStudy(createToken(), caseStudyId, caseStudyRequest)
                .enqueue(new RestCallback<GetUpdateStatusCaseStudyResponse>() {
                    @Override
                    public void success(GetUpdateStatusCaseStudyResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateAttachFile(String caseStudyId, UpdateAttachFileRequest attachFileRequest, final ApiCallback<GetUpdateAttachFileResponse> callback) {
        mApiServices.updateAttachFile(createToken(), caseStudyId, attachFileRequest)
                .enqueue(new RestCallback<GetUpdateAttachFileResponse>() {
                    @Override
                    public void success(GetUpdateAttachFileResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteAttachFile(String caseStudyId, UpdateAttachFileRequest attachFileRequest, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteAttachFile(createToken(), caseStudyId, attachFileRequest)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get Company Setting =========== //
    public void getCompanyInfo(final ApiCallback<GetCompanyInfoResponse> callback) {
        mApiServices.getCompanyInfo(createToken())
                .enqueue(new RestCallback<GetCompanyInfoResponse>() {
                    @Override
                    public void success(GetCompanyInfoResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateInfo(Information information, final ApiCallback<GetUpdateInfoResponse> callback) {
        mApiServices.updateInfo(createToken(), information)
                .enqueue(new RestCallback<GetUpdateInfoResponse>() {
                    @Override
                    public void success(GetUpdateInfoResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getFeature(final ApiCallback<GetFeatureResponse> callback) {
        mApiServices.getFeature(createToken())
                .enqueue(new RestCallback<GetFeatureResponse>() {
                    @Override
                    public void success(GetFeatureResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateFeature(Feature feature, final ApiCallback<GetUpdateFeatureResponse> callback) {
        mApiServices.updateFeature(createToken(), feature)
                .enqueue(new RestCallback<GetUpdateFeatureResponse>() {
                    @Override
                    public void success(GetUpdateFeatureResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getInfoQuota(final ApiCallback<GetInfoQuotaResponse> callback) {
        mApiServices.getInfoQuota(createToken())
                .enqueue(new RestCallback<GetInfoQuotaResponse>() {
                    @Override
                    public void success(GetInfoQuotaResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getSummaryQuota(final ApiCallback<GetSummaryQuotaResponse> callback) {
        mApiServices.getSummaryQuota(createToken())
                .enqueue(new RestCallback<GetSummaryQuotaResponse>() {
                    @Override
                    public void success(GetSummaryQuotaResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getAllDevices(final ApiCallback<GetDevicesResponse> callback) {
        mApiServices.getAllDevice(createToken())
                .enqueue(new RestCallback<GetDevicesResponse>() {
                    @Override
                    public void success(GetDevicesResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getAllLevel(final ApiCallback<GetAllLevelResponse> callback) {
        mApiServices.getAllLevel(createToken())
                .enqueue(new RestCallback<GetAllLevelResponse>() {
                    @Override
                    public void success(GetAllLevelResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getAllDepartment(final ApiCallback<GetAllDepartmentResponse> callback) {
        mApiServices.getAllDepartment(createToken())
                .enqueue(new RestCallback<GetAllDepartmentResponse>() {
                    @Override
                    public void success(GetAllDepartmentResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getAllCompanyRole(final ApiCallback<GetAllCompanyRoleResponse> callback) {
        mApiServices.getAllCompanyRole(createToken())
                .enqueue(new RestCallback<GetAllCompanyRoleResponse>() {
                    @Override
                    public void success(GetAllCompanyRoleResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getAllProjectRole(final ApiCallback<GetAllProjectRoleResponse> callback) {
        mApiServices.getAllProjectRole(createToken())
                .enqueue(new RestCallback<GetAllProjectRoleResponse>() {
                    @Override
                    public void success(GetAllProjectRoleResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getAllHoliday(final ApiCallback<GetAllHolidayResponse> callback) {
        mApiServices.getAllHoliday(createToken())
                .enqueue(new RestCallback<GetAllHolidayResponse>() {
                    @Override
                    public void success(GetAllHolidayResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteDepartment(String idDepartment, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteDepartment(createToken(), idDepartment)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteMember(String idMember, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteMember(createToken(), idMember)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateMember(String idMember, UpdateMemberRequest memberRequest, final ApiCallback<BaseResponse> callback) {
        mApiServices.updateMember(createToken(), idMember, memberRequest)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void changePasswordByUser(ChangePassWordRequest changePassWordRequest, final ApiCallback<BaseResponse> callback) {
        mApiServices.changePasswordByUser(createToken(), changePassWordRequest)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void changePasswordByAdmin(String id, ChangePassWordRequest changePassWordRequest, final ApiCallback<BaseResponse> callback) {
        mApiServices.changePasswordByAdmin(createToken(), id, changePassWordRequest)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteProjectRole(String idProjectRole, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteProjectRole(createToken(), idProjectRole)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }


    public void deleteCompanyRole(String idCompanyRole, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteCompanyRole(createToken(), idCompanyRole)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteDevice(String idDevice, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteDevice(createToken(), idDevice)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteLevel(String idLevel, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteLevel(createToken(), idLevel)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateLevel(String levelId, Level level, final ApiCallback<GetUpdateLevelResponse> callback) {
        mApiServices.updateLevel(createToken(), levelId, level)
                .enqueue(new RestCallback<GetUpdateLevelResponse>() {
                    @Override
                    public void success(GetUpdateLevelResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createLevel(CreateLevelRequest createLevelRequest, final ApiCallback<CreateLevelResponse> callback) {
        mApiServices.createLevel(createToken(), createLevelRequest)
                .enqueue(new RestCallback<CreateLevelResponse>() {
                    @Override
                    public void success(CreateLevelResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }


    public void updateDepartment(String departmentId, Department department, final ApiCallback<GetUpdateDepartmentResponse> callback) {
        mApiServices.updateDepartment(createToken(), departmentId, department)
                .enqueue(new RestCallback<GetUpdateDepartmentResponse>() {
                    @Override
                    public void success(GetUpdateDepartmentResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createDepartment(CreateDepartmentRequest createDepartmentRequest, final ApiCallback<CreateDepartmentResponse> callback) {
        mApiServices.createDepartment(createToken(), createDepartmentRequest)
                .enqueue(new RestCallback<CreateDepartmentResponse>() {
                    @Override
                    public void success(CreateDepartmentResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createMember(CreateMemberRequest createMemberRequest, final ApiCallback<GetCreateMemberResponse> callback) {
        mApiServices.createMember(createToken(), createMemberRequest)
                .enqueue(new RestCallback<GetCreateMemberResponse>() {
                    @Override
                    public void success(GetCreateMemberResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getAllHistory(final ApiCallback<GetAllHistoryResponse> callback) {
        mApiServices.getAllHistory(createToken())
                .enqueue(new RestCallback<GetAllHistoryResponse>() {
                    @Override
                    public void success(GetAllHistoryResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getAllCard(final ApiCallback<GetAllCardResponse> callback) {
        mApiServices.getAllCard(createToken())
                .enqueue(new RestCallback<GetAllCardResponse>() {
                    @Override
                    public void success(GetAllCardResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All Leaving Register =========== //
    public void getAllLeaving(final ApiCallback<GetAllLeavingResponse> callback) {
        mApiServices.getAllLeaving(createToken())
                .enqueue(new RestCallback<GetAllLeavingResponse>() {
                    @Override
                    public void success(GetAllLeavingResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getLeavingById(String userId, final ApiCallback<GetLeavingByIdResponse> callback) {
        mApiServices.getLeavingById(createToken(), userId)
                .enqueue(new RestCallback<GetLeavingByIdResponse>() {
                    @Override
                    public void success(GetLeavingByIdResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // ============ Create Leaving Register ================//
    public void createLeaving(CreateLeavingRequest request, final ApiCallback<CreateLeavingResponse> callback) {
        mApiServices.createLeavingRegister(createToken(), request)
                .enqueue(new RestCallback<CreateLeavingResponse>() {
                    @Override
                    public void success(CreateLeavingResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });


    }

    // =========== Get All Survey =========== //
    public void getAllSurvey(final ApiCallback<GetAllSurveyResponse> callback) {
        mApiServices.getAllSurvey(createToken())
                .enqueue(new RestCallback<GetAllSurveyResponse>() {
                    @Override
                    public void success(GetAllSurveyResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All Survey Approved =========== //
    public void getAllSurveyApproved(final ApiCallback<GetAllSurveyApprovedResponse> callback) {
        mApiServices.getAllSurveyApproved(createToken())
                .enqueue(new RestCallback<GetAllSurveyApprovedResponse>() {
                    @Override
                    public void success(GetAllSurveyApprovedResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All Survey Pending =========== //
    public void getAllSurveyPending(final ApiCallback<GetAllSurveyPendingResponse> callback) {
        mApiServices.getAllSurveyPending(createToken())
                .enqueue(new RestCallback<GetAllSurveyPendingResponse>() {
                    @Override
                    public void success(GetAllSurveyPendingResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get Submiter =========== //
    public void getSubmiter(final ApiCallback<GetSubmiterResponse> callback) {
        mApiServices.getSubmiter(createToken())
                .enqueue(new RestCallback<GetSubmiterResponse>() {
                    @Override
                    public void success(GetSubmiterResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All Survey Item =========== //
    public void getAllSurveyItem(String surveyId, final ApiCallback<GetAllSurveyItemResponse> callback){
        mApiServices.getAllSurveyItem(createToken(),surveyId)
                .enqueue(new RestCallback<GetAllSurveyItemResponse>() {
                    @Override
                    public void success(GetAllSurveyItemResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All User =========== //
    public void getAllUser(final ApiCallback<GetAllUserResponse> callback) {
        mApiServices.getAllUser(createToken())
                .enqueue(new RestCallback<GetAllUserResponse>() {
                    @Override
                    public void success(GetAllUserResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get All Warehourse Estate =========== //
    public void getAllWarehourseEstate(final ApiCallback<GetAllWarehourseEstateResponse> callback) {
        mApiServices.getWarehourseEstate(createToken())
                .enqueue(new RestCallback<GetAllWarehourseEstateResponse>() {
                    @Override
                    public void success(GetAllWarehourseEstateResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error))
                            callback.failure(error);
                    }
                });
    }

    // =========== Create new Warehourse Estate =========== //
    public void createWarehouseEstate(WarehouseEstate warehouseEstate, final ApiCallback<CreateWarehouseEstateResponse> callback) {
        mApiServices.createWarehouseEstate(createToken(), warehouseEstate)
                .enqueue(new RestCallback<CreateWarehouseEstateResponse>() {
                    @Override
                    public void success(CreateWarehouseEstateResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error))
                            callback.failure(error);
                    }
                });
    }

    // =========== Delete Warehouse Estate =========== //
    public void deleteWarehouseEstate(String warehouseId, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteWarehouseEstate(createToken(), warehouseId)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error))
                            callback.failure(error);
                    }
                });
    }

    // =========== Get All Estate =========== //
    public void getAllEstates(String warehouseId, final ApiCallback<GetAllEstatesResponse> callback) {
        mApiServices.getAllEstate(createToken(), warehouseId)
                .enqueue(new RestCallback<GetAllEstatesResponse>() {
                    @Override
                    public void success(GetAllEstatesResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Create Estate Item =========== //
    public void createEstateItem(String warehouseId, Estate estate, final ApiCallback<CreateEstateResponse> callback) {
        mApiServices.createEstateItem(createToken(), warehouseId, estate)
                .enqueue(new RestCallback<CreateEstateResponse>() {
                    @Override
                    public void success(CreateEstateResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Delete Estate Item=========== //
    public void deleteEstateItem(String warehouseId, String estateItemId, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteEstateItem(createToken(), warehouseId, estateItemId)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== LOG TIME =========== //
    public void getMember(String projectId, final ApiCallback<GetMemberResponse> callback) {
        mApiServices.getMembers(createToken(), projectId)
                .enqueue(new RestCallback<GetMemberResponse>() {
                    @Override
                    public void success(GetMemberResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getLogTimes(String projectId, GetMembersHaveLogTimeRequest request, final ApiCallback<GetMembersHaveLogTimeResponse> callback) {
        mApiServices.getLogTimes(createToken(), projectId, request)
                .enqueue(new RestCallback<GetMembersHaveLogTimeResponse>() {
                    @Override
                    public void success(GetMembersHaveLogTimeResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getLogTime(String projectId, GetLogTimeRequest request, final ApiCallback<GetLogTimeResponse> callback) {
        mApiServices.getLogTime(createToken(), projectId, request)
                .enqueue(new RestCallback<GetLogTimeResponse>() {
                    @Override
                    public void success(GetLogTimeResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateLogTime(String projectId, String logTimeId, UpdateLogTimeRequest request, final ApiCallback<UpdateLogTimeResponse> callback) {
        mApiServices.updateLogTime(createToken(), projectId, logTimeId, request)
                .enqueue(new RestCallback<UpdateLogTimeResponse>() {
                    @Override
                    public void success(UpdateLogTimeResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Get Report=========== //

    public void getMemberReport(MemberRequest request, final ApiCallback<MemberResponse> callback) {
        mApiServices.getMemberReport(createToken(), request)
                .enqueue(new RestCallback<MemberResponse>() {
                    @Override
                    public void success(MemberResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getAllMemberReport(final ApiCallback<AllReportResponse> callback) {
        mApiServices.getAllReport(createToken())
                .enqueue(new RestCallback<AllReportResponse>() {
                    @Override
                    public void success(AllReportResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateReport(String id, Reports reports, final ApiCallback<BaseResponse> callback) {
        mApiServices.updateReport(createToken(), id, reports)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateReportStatus(String id, RejectApprovedRequest param, final ApiCallback<BaseResponse> callback) {
        mApiServices.updateReportSubmitAndReject(createToken(), id, param)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createReport(NewReportRequest param, final ApiCallback<BaseResponse> callback) {
        mApiServices.createReport(createToken(), param)
                .enqueue(new RestCallback<BaseResponse>() {
                             @Override
                             public void success(BaseResponse res) {
                                 callback.success(res);
                             }

                             @Override
                             public void failure(RestError error) {
                                 callback.failure(error);
                             }
                         }
                );
    }

    public void getAllUserReport(final ApiCallback<GetAllUserResponse> callback) {
        mApiServices.getUserReport(createToken())
                .enqueue(new RestCallback<GetAllUserResponse>() {
                             @Override
                             public void success(GetAllUserResponse res) {
                                 callback.success(res);
                             }

                             @Override
                             public void failure(RestError error) {
                                 callback.failure(error);
                             }
                         }
                );
    }

    public void getAllGroupReport(String id, final ApiCallback<GroupResponse> callback) {
        mApiServices.getGroupReport(createToken(), id)
                .enqueue(new RestCallback<GroupResponse>() {
                             @Override
                             public void success(GroupResponse res) {
                                 callback.success(res);
                             }

                             @Override
                             public void failure(RestError error) {
                                 callback.failure(error);
                             }
                         }
                );
    }

    // =========== Salary Staff =========== //
    public void getAllSalaryStaff(final ApiCallback<GetAllSalaryStaffResponse> callback) {
        mApiServices.getAllSalaryStaff(createToken())
                .enqueue(new RestCallback<GetAllSalaryStaffResponse>() {
                    @Override
                    public void success(GetAllSalaryStaffResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getDetailSalaryStaff(String staffId, final ApiCallback<GetDetailSalaryStaffResponse> callback) {
        mApiServices.getSalaryStaffById(createToken(), staffId)
                .enqueue(new RestCallback<GetDetailSalaryStaffResponse>() {
                    @Override
                    public void success(GetDetailSalaryStaffResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createSalaryStaff(Staff staff, final ApiCallback<GetCreateSalaryStaffResponse> callback) {
        mApiServices.createSalaryStaff(createToken(), staff)
                .enqueue(new RestCallback<GetCreateSalaryStaffResponse>() {
                    @Override
                    public void success(GetCreateSalaryStaffResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateSalaryStaff(String staffId, Staff staff, final ApiCallback<GetUpdateSalaryStaffResponse> callback) {
        mApiServices.updateSalaryStaff(createToken(), staffId, staff)
                .enqueue(new RestCallback<GetUpdateSalaryStaffResponse>() {
                    @Override
                    public void success(GetUpdateSalaryStaffResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteSalaryStaff(String staffId, final ApiCallback<GetDeleteSalaryStaffResponse> callback) {
        mApiServices.deleteSalaryStaff(createToken(), staffId)
                .enqueue(new RestCallback<GetDeleteSalaryStaffResponse>() {
                    @Override
                    public void success(GetDeleteSalaryStaffResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Salary Metric Group =========== //
    public void getAllMetricGroup(final ApiCallback<GetAllMetricGroupResponse> callback) {
        mApiServices.getAllMetricGroup(createToken())
                .enqueue(new RestCallback<GetAllMetricGroupResponse>() {
                    @Override
                    public void success(GetAllMetricGroupResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getMetricGroupById(String staffId, final ApiCallback<GetDetailMetricGroupResponse> callback) {
        mApiServices.getMetricGroupById(createToken(), staffId)
                .enqueue(new RestCallback<GetDetailMetricGroupResponse>() {
                    @Override
                    public void success(GetDetailMetricGroupResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createMetricGroup(Metric metric, final ApiCallback<GetCreateMetricGroupResponse> callback) {
        mApiServices.createMetricGroup(createToken(), metric)
                .enqueue(new RestCallback<GetCreateMetricGroupResponse>() {
                    @Override
                    public void success(GetCreateMetricGroupResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateMetricGroup(String metricId, Metric metric, final ApiCallback<GetUpdateMetricGroupResponse> callback) {
        mApiServices.updateMetricGroup(createToken(), metricId, metric)
                .enqueue(new RestCallback<GetUpdateMetricGroupResponse>() {
                    @Override
                    public void success(GetUpdateMetricGroupResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteMetricGroup(String metricId, final ApiCallback<GetDeleteMetricGroupResponse> callback) {
        mApiServices.deleteMetricGroup(createToken(), metricId)
                .enqueue(new RestCallback<GetDeleteMetricGroupResponse>() {
                    @Override
                    public void success(GetDeleteMetricGroupResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void addMetricToMetricGroup(String metricGroupId, Metric metric, final ApiCallback<GetAddMetricMetricGroupResponse> callback) {
        mApiServices.addMetricToMetricGroup(createToken(), metricGroupId, metric)
                .enqueue(new RestCallback<GetAddMetricMetricGroupResponse>() {
                    @Override
                    public void success(GetAddMetricMetricGroupResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteMetricFromMetricGroup(String metricGroupId, String metricId, final ApiCallback<GetDeleteMetricMetricGroupResponse> callback) {
        mApiServices.deleteMetricFromMetricGroup(createToken(), metricGroupId, metricId)
                .enqueue(new RestCallback<GetDeleteMetricMetricGroupResponse>() {
                    @Override
                    public void success(GetDeleteMetricMetricGroupResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Salary Metric =========== //
    public void getAllMetric(final ApiCallback<GetAllMetricResponse> callback) {
        mApiServices.getAllMetric(createToken())
                .enqueue(new RestCallback<GetAllMetricResponse>() {
                    @Override
                    public void success(GetAllMetricResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getMetricById(String staffId, final ApiCallback<GetDetailMetricResponse> callback) {
        mApiServices.getMetricById(createToken(), staffId)
                .enqueue(new RestCallback<GetDetailMetricResponse>() {
                    @Override
                    public void success(GetDetailMetricResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createMetric(Metric metric, final ApiCallback<GetCreateMetricResponse> callback) {
        mApiServices.createMetric(createToken(), metric)
                .enqueue(new RestCallback<GetCreateMetricResponse>() {
                    @Override
                    public void success(GetCreateMetricResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateMetric(String metricId, Metric metric, final ApiCallback<GetUpdateMetricResponse> callback) {
        mApiServices.updateMetric(createToken(), metricId, metric)
                .enqueue(new RestCallback<GetUpdateMetricResponse>() {
                    @Override
                    public void success(GetUpdateMetricResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteMetric(String metricId, final ApiCallback<GetDeleteMetricGroupResponse> callback) {
        mApiServices.deleteMetric(createToken(), metricId)
                .enqueue(new RestCallback<GetDeleteMetricGroupResponse>() {
                    @Override
                    public void success(GetDeleteMetricGroupResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Salary Param =========== //
    public void addMetricParam(AddStepParamRequest addStepParamRequest, final ApiCallback<GetAddParamResponse> callback) {
        mApiServices.addMetricParam(createToken(), addStepParamRequest)
                .enqueue(new RestCallback<GetAddParamResponse>() {
                    @Override
                    public void success(GetAddParamResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteMetricParam(String metricId, DeleteStepParamRequest deleteStepParamRequest, final ApiCallback<GetDeleteParamResponse> callback) {
        mApiServices.deleteMetricParam(createToken(), metricId, deleteStepParamRequest)
                .enqueue(new RestCallback<GetDeleteParamResponse>() {
                    @Override
                    public void success(GetDeleteParamResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    // =========== Salary Input Addon =========== //
    public void getAllInputAddon(final ApiCallback<GetAllInputAddonResponse> callback) {
        mApiServices.getAllInputAddon(createToken())
                .enqueue(new RestCallback<GetAllInputAddonResponse>() {
                    @Override
                    public void success(GetAllInputAddonResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getInputAddonById(String inputAddonId, final ApiCallback<GetDetailInputAddonResponse> callback) {
        mApiServices.getInputAddonById(createToken(), inputAddonId)
                .enqueue(new RestCallback<GetDetailInputAddonResponse>() {
                    @Override
                    public void success(GetDetailInputAddonResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void filterInputAddon(FilterInputAddonRequest filterInputAddonRequest, final ApiCallback<GetFilterInputAddonResponse> callback) {
        mApiServices.filterInputAddon(createToken(), filterInputAddonRequest)
                .enqueue(new RestCallback<GetFilterInputAddonResponse>() {
                    @Override
                    public void success(GetFilterInputAddonResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void createInputAddon(CreateInputAddonRequest createInputAddonRequest, final ApiCallback<GetCreateInputAddonResponse> callback) {
        mApiServices.createInputAddon(createToken(), createInputAddonRequest)
                .enqueue(new RestCallback<GetCreateInputAddonResponse>() {
                    @Override
                    public void success(GetCreateInputAddonResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void updateInputAddon(String staffId, InputAddon inputAddon, final ApiCallback<GetUpdateInputAddonResponse> callback) {
        mApiServices.updateInputAddon(createToken(), staffId, inputAddon)
                .enqueue(new RestCallback<GetUpdateInputAddonResponse>() {
                    @Override
                    public void success(GetUpdateInputAddonResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteInputAddon(String staffId, final ApiCallback<GetDeleteInputAddonResponse> callback) {
        mApiServices.deleteInputAddon(createToken(), staffId)
                .enqueue(new RestCallback<GetDeleteInputAddonResponse>() {
                    @Override
                    public void success(GetDeleteInputAddonResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void getListRecruitment(final ApiCallback<RecruitmentResponse> callback) {
        mApiServices.getListRecruitment(createToken())
                .enqueue(new RestCallback<RecruitmentResponse>() {
                    @Override
                    public void success(RecruitmentResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }

    public void deleteRecruitment(String id, final ApiCallback<BaseResponse> callback) {
        mApiServices.deleteRecruitment(createToken(), id)
                .enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse res) {
                        callback.success(res);
                    }

                    @Override
                    public void failure(RestError error) {
                        if (!handleSpecialCode(error)) {
                            callback.failure(error);
                        }
                    }
                });
    }
}