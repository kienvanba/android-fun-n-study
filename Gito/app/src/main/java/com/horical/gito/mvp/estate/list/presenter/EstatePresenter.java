package com.horical.gito.mvp.estate.list.presenter;

import android.text.TextUtils;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.estate.CreateEstateResponse;
import com.horical.gito.interactor.api.response.estate.GetAllEstatesResponse;
import com.horical.gito.model.estate.Estate;
import com.horical.gito.mvp.estate.list.adapter.EstateItem;
import com.horical.gito.mvp.estate.list.view.IEstateView;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.LOGE;
import static com.horical.gito.utils.LogUtils.LOGI;
import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by hoangsang on 11/8/16.
 */

public class EstatePresenter extends BasePresenter implements IEstatePresenter {
    private static final String TAG = makeLogTag(EstatePresenter.class);

    private List<IRecyclerItem> mList;

    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public IEstateView getIView() {
        return (IEstateView) super.getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public List<IRecyclerItem> getListItem() {
        return mList;
    }

    @Override
    public void getAllEstateItem(String warehouseId) {
        if (!getIView().isRefreshing())
            getIView().showLoading();

        getApiManager().getAllEstates(warehouseId, new ApiCallback<GetAllEstatesResponse>() {
            @Override
            public void success(GetAllEstatesResponse res) {
                LOGI(TAG, "load all estates success ");
                getIView().hideLoading();

                List<Estate> estateList = res.estateList;
                mList.clear();
                mList.addAll(convertEstateListToItemsList(estateList));
                getIView().getAllEstateItemSuccess();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, "Load all estates failure " + error.message);
                getIView().hideLoading();
                getIView().getAllEstateItemFailed();
            }
        });

    }

    private List<IRecyclerItem> convertEstateListToItemsList(List<Estate> estates) {
        List<IRecyclerItem> items = new ArrayList<>();
        for (Estate estate : estates) {
            EstateItem item = new EstateItem(estate);
            items.add(item);
        }

        return items;
    }

    @Override
    public void createEstateItem(String warehouseId, String estateName, String estateId) {
        getIView().showLoading();

        Estate estate = new Estate();

        if (TextUtils.isEmpty(estateName) || TextUtils.isEmpty(estateId)) {
            getIView().hideLoading();
            getIView().createEstateItemFailed(null);
            return;
        }

        estate.name = estateName;
        estate.estateId = estateId;

        getApiManager().createEstateItem(warehouseId, estate, new ApiCallback<CreateEstateResponse>() {
            @Override
            public void success(CreateEstateResponse res) {
                LOGI(TAG, "Create estate item success");
                getIView().createEstateItemSuccess();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, "Create estate item failed : " + error.message);
                getIView().hideLoading();
                getIView().createEstateItemFailed(error.message);
            }
        });
    }

    @Override
    public void deleteEstateItem(String warehouseId, String estateItemId) {
        getIView().showLoading();

        getApiManager().deleteEstateItem(warehouseId, estateItemId, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                LOGI(TAG, "Delete estate item success");
                getIView().deleteEstateItemSuccess();
            }

            @Override
            public void failure(RestError error) {
                getIView().hideLoading();
                LOGE(TAG, "Delete estate failed : " + error.message);
                getIView().deleteEstateItemFailed(error.message);
            }
        });
    }
}
