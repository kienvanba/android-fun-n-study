package com.horical.gito.mvp.companySetting.detail.newDepartment.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.companySetting.detail.newDepartment.presenter.NewDepartmentPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class NewDepartmentModule {
    @PerActivity
    @Provides
    NewDepartmentPresenter provideNewDepartmentPresenter(){
        return new NewDepartmentPresenter();
    }
}
