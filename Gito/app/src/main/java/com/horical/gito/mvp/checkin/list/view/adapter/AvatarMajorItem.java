package com.horical.gito.mvp.checkin.list.view.adapter;

import com.horical.gito.base.BaseActivity;
import com.horical.gito.base.IRecyclerItem;


/**
 * Created by Luong on 27-Feb-17.
 */

public class AvatarMajorItem extends BaseActivity implements IRecyclerItem {

    public int Avatar;
    public String Major;


    public AvatarMajorItem(int avatar, String major) {
        this.Avatar = avatar;
        this.Major = major;
    }

    @Override
    public int getItemViewType() {
        return 0;
    }
}
