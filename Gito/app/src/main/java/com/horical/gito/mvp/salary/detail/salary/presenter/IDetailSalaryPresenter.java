package com.horical.gito.mvp.salary.detail.salary.presenter;

import com.horical.gito.mvp.salary.dto.metric.SummaryDto;
import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;

import java.util.List;

public interface IDetailSalaryPresenter {
    List<ListStaffDto> getStaffs();
    List<SummaryDto> getSummaries();
}
