package com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.model.Information;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.adapter.HolidayAdapter;
import com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.injection.component.DaggerWorkingAndHolidayComponent;
import com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.injection.component.WorkingAndHolidayComponent;
import com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.injection.module.WorkingAndHolidayModule;
import com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.presenter.WorkingAndHolidayPresenter;
import com.horical.gito.mvp.dialog.dialogCompanySetting.DialogFilterWorkingHoliday;
import com.horical.gito.mvp.dialog.dialogDateTimePicker.DateTimePickerDialog;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.utils.HDate;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class WorkingAndHolidayFragment extends BaseCompanyFragment implements IWorkingAndHolidayView, View.OnClickListener {
    public static final String TAG = makeLogTag(WorkingAndHolidayFragment.class);

    @Bind(R.id.tv_morning_start)
    TextView tvMorningStart;

    @Bind(R.id.tv_morning_end)
    TextView tvMorningEnd;

    @Bind(R.id.img_edit_time_morning)
    ImageView imgEditMorning;

    @Bind(R.id.tv_afternoon_start)
    TextView tvAfternoonStart;

    @Bind(R.id.tv_afternoon_end)
    TextView tvAfternoonEnd;

    @Bind(R.id.tv_night_start)
    TextView tvNightStart;

    @Bind(R.id.tv_night_end)
    TextView tvNightEnd;

    @Bind(R.id.tv_working_day)
    TextView tvWorkingDay;

    @Bind(R.id.tv_monthly_holiday)
    TextView tvMonthlyHoliday;

    @Bind(R.id.tv_yearly_holiday)
    TextView tvYearlyHoliday;

    @Bind(R.id.tv_sun)
    TextView tvSun;

    @Bind(R.id.tv_mon)
    TextView tvMon;

    @Bind(R.id.tv_tue)
    TextView tvTue;

    @Bind(R.id.tv_wed)
    TextView tvWed;

    @Bind(R.id.tv_thu)
    TextView tvThu;

    @Bind(R.id.tv_fri)
    TextView tvFri;

    @Bind(R.id.tv_sat)
    TextView tvSat;

    @Bind(R.id.tv_start_sun)
    TextView tvStartSun;

    @Bind(R.id.tv_start_mon)
    TextView tvStartMon;

    @Bind(R.id.tv_start_tue)
    TextView tvStartTue;

    @Bind(R.id.tv_start_wed)
    TextView tvStartWed;

    @Bind(R.id.tv_start_thu)
    TextView tvStartThu;

    @Bind(R.id.tv_start_fri)
    TextView tvStartFri;

    @Bind(R.id.tv_start_sat)
    TextView tvStartSat;

    @Bind(R.id.ll_filter)
    LinearLayout llFilter;

    @Bind(R.id.rcv_holiday)
    RecyclerView rcvHoliday;

    private HolidayAdapter adapterHoliday;

    @Inject
    WorkingAndHolidayPresenter mPresenter;

    public static WorkingAndHolidayFragment newInstance() {
        Bundle args = new Bundle();
        WorkingAndHolidayFragment fragment = new WorkingAndHolidayFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_company_setting_working_holiday;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        WorkingAndHolidayComponent component = DaggerWorkingAndHolidayComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .workingAndHolidayModule(new WorkingAndHolidayModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        Information information = GitOStorage.getInstance().getInformation();
        if (information != null) {
            tvMorningStart.setText(DateUtils.formatDate(information.getMorningStartTime(), "hh:mm"));
            tvMorningEnd.setText(DateUtils.formatDate(information.getMorningEndTime(), "hh:mm"));
            tvAfternoonStart.setText(DateUtils.formatDate(information.getAfternoonStartTime(), "hh:mm"));
            tvAfternoonEnd.setText(DateUtils.formatDate(information.getAfternoonEndTime(), "hh:mm"));
            tvNightStart.setText(DateUtils.formatDate(information.getEveningStartTime(), "hh:mm"));
            tvNightEnd.setText(DateUtils.formatDate(information.getEveningEndTime(), "hh:mm"));

            tvWorkingDay.setText(String.valueOf(information.getWorkingDay()));
            tvMonthlyHoliday.setText(String.valueOf(information.getMonthlyHoliday()));
            tvYearlyHoliday.setText(String.valueOf(information.getYearlyHoliday()));
            tvSun.setSelected(information.getWorkingDayWeek().isSun());
            tvMon.setSelected(information.getWorkingDayWeek().isMon());
            tvTue.setSelected(information.getWorkingDayWeek().isTue());
            tvWed.setSelected(information.getWorkingDayWeek().isWed());
            tvThu.setSelected(information.getWorkingDayWeek().isThu());
            tvFri.setSelected(information.getWorkingDayWeek().isFri());
            tvSat.setSelected(information.getWorkingDayWeek().isSat());

            switch (information.getStartOfWeekDay()) {
                case 0:
                    tvStartSun.setSelected(true);
                    break;
                case 1:
                    tvStartMon.setSelected(true);
                    break;
                case 2:
                    tvStartTue.setSelected(true);
                    break;
                case 3:
                    tvStartWed.setSelected(true);
                    break;
                case 4:
                    tvStartThu.setSelected(true);
                    break;
                case 5:
                    tvStartFri.setSelected(true);
                    break;
                case 6:
                    tvStartSat.setSelected(true);
                    break;
            }
        }
        adapterHoliday = new HolidayAdapter(getContext(), mPresenter.getListHoliday());
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvHoliday.setLayoutManager(llm);
        rcvHoliday.setAdapter(adapterHoliday);

        mPresenter.getAllHoliday();
    }

    @Override
    protected void initListener() {
        imgEditMorning.setOnClickListener(this);
        llFilter.setOnClickListener(this);
        adapterHoliday.setOnItemClickListener(new HolidayAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(int position) {

            }

            @Override
            public void onDelete(int position) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_edit_time_morning:
                DateTimePickerDialog timePickerDialog = new DateTimePickerDialog(getContext(), new DateTimePickerDialog.OnClickDoneListener() {
                    @Override
                    public void onDoneClick(HDate DateTimeFrom, HDate DateTimeTo) {
                        tvMorningStart.setText(DateTimeFrom.hour + ":" + DateTimeFrom.minute);
                        tvMorningEnd.setText(DateTimeTo.hour + ":" + DateTimeTo.minute);
                    }
                });
                timePickerDialog.show();
                break;
            case R.id.ll_filter:
                DialogFilterWorkingHoliday dialogFilterWorkingHoliday = new DialogFilterWorkingHoliday(getContext());
                dialogFilterWorkingHoliday.show();
                break;
        }
    }

    @Override
    public void getAllHolidaySuccess() {
        adapterHoliday.notifyDataSetChanged();
    }

    @Override
    public void getAllHolidayFailure(String err) {
        adapterHoliday.notifyDataSetChanged();
    }
}
