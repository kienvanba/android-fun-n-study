package com.horical.gito.mvp.user.detail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.ApiManager;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.changePassword.ChangePassWordRequest;
import com.horical.gito.interactor.api.request.user.UpdateMemberRequest;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.User;
import com.horical.gito.mvp.user.detail.view.IUserDetailView;

/**
 * Created by thanhle on 4/7/17.
 */

public class UserDetailPresenter extends BasePresenter implements IUserDetailPresenter {

    public static final int NOT_ADMIN = 0;
    public static final int ADMIN = 1;
    public static final int NORMAL = 2;

    public static final int UNLOCK = 0;
    public static final int LOCK = 1;

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public IUserDetailView getView() {
        return (IUserDetailView) getIView();
    }

    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void updateUser(User user, final int isAdmin, final int isLock, int type, String displayName, String idRoleCompany,
                           String idPosition, String email, String userId, String phone,
                           String idDepartment, String address) {
        UpdateMemberRequest memberRequest = new UpdateMemberRequest();
        if (displayName != null) {
            memberRequest.status = user.getStatus();
            memberRequest.isAdmin = user.isAdmin();
            memberRequest.displayName = displayName;
        } else if (idRoleCompany != null) {
            memberRequest.status = user.getStatus();
            memberRequest.isAdmin = user.isAdmin();
            memberRequest.roleCompany = idRoleCompany;
        } else if (idPosition != null) {
            memberRequest.status = user.getStatus();
            memberRequest.isAdmin = user.isAdmin();
            memberRequest.level = idPosition;
        } else if (email != null) {
            memberRequest.status = user.getStatus();
            memberRequest.isAdmin = user.isAdmin();
            memberRequest.email = email;
        } else if (userId != null) {
            memberRequest.status = user.getStatus();
            memberRequest.isAdmin = user.isAdmin();
            memberRequest.userId = userId;
        } else if (phone != null) {
            memberRequest.status = user.getStatus();
            memberRequest.isAdmin = user.isAdmin();
            memberRequest.phone = phone;
        } else if (idDepartment != null) {
            memberRequest.status = user.getStatus();
            memberRequest.isAdmin = user.isAdmin();
            memberRequest.department = idDepartment;
        } else if (address != null) {
            memberRequest.status = user.getStatus();
            memberRequest.isAdmin = user.isAdmin();
            memberRequest.address = address;
        } else if (type == 1) {
            memberRequest.address = user.getAddress();
            memberRequest.level = user.getLevel().getId();
            memberRequest.department = user.getDepartment().get_id();
            memberRequest.displayName = user.getDisplayName();
            memberRequest.email = user.getEmail();
            if (isAdmin == NOT_ADMIN) {
                memberRequest.isAdmin = false;
            } else if (isAdmin == ADMIN) {
                memberRequest.isAdmin = true;
            } else if (isAdmin == NORMAL) {
                memberRequest.isAdmin = user.isAdmin();
            }
            memberRequest.phone = user.getPhone();
            memberRequest.roleCompany = user.getRoleCompany().getRoleCompanyId();
            if (isLock == LOCK) {
                memberRequest.status = LOCK;
            } else if (isLock == UNLOCK) {
                memberRequest.status = UNLOCK;
            } else {
                memberRequest.status = user.getStatus();
            }
            memberRequest.userId = user.getUserId();
        }

        getApiManager().updateMember(user.get_id(), memberRequest, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().updateMemberSuccess(isAdmin, isLock);
            }

            @Override
            public void failure(RestError error) {
                getView().updateMemberFailure(error.message);
            }
        });
    }

    @Override
    public void deleteMember(String idMember) {
        getApiManager().deleteMember(idMember, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteMemberSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().deleteMemberFailure(error.message);
            }
        });
    }

    @Override
    public void changePasswordByAdmin(String id, String newPassword) {
        ChangePassWordRequest changePassWordRequest = new ChangePassWordRequest();
        changePassWordRequest.newPass = newPassword;

        getApiManager().changePasswordByAdmin(id, changePassWordRequest, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().changePasswordSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().changePasswordFailure(error.message);
            }
        });
    }

    @Override
    public void changePasswordByThisUser(String currentPassword, String newPassword, String confirmPassword) {
        ChangePassWordRequest changePassWordRequest = new ChangePassWordRequest();
        changePassWordRequest.newPass = newPassword;
        changePassWordRequest.oldPassword = currentPassword;
        changePassWordRequest.rePass = confirmPassword;

        getApiManager().changePasswordByUser(changePassWordRequest, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().changePasswordSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().changePasswordFailure(error.message);
            }
        });
    }
}
