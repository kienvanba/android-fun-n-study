package com.horical.gito.mvp.companySetting.list.fragment.companyrole.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.fragment.companyrole.injection.module.CompanyRoleModule;
import com.horical.gito.mvp.companySetting.list.fragment.companyrole.view.CompanyRoleFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = CompanyRoleModule.class)
public interface CompanyRoleComponent {
    void inject(CompanyRoleFragment fragment);
}
