package com.horical.gito.mvp.companySetting.list.fragment.members.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.model.Department;
import com.horical.gito.model.Level;
import com.horical.gito.model.TokenFile;
import com.horical.gito.model.User;
import com.horical.gito.mvp.companySetting.list.fragment.members.dto.MemberDto;
import com.horical.gito.mvp.popup.popupCompanySetting.PopupCompanySetting;
import com.horical.gito.utils.CommonUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.MembersHolder> {
    private Context mContext;
    private List<User> mList;

    private HashSet<MemberDto> listJob = new HashSet<>();
    private HashSet<MemberDto> listDepartment = new HashSet<>();
    private List<MemberDto> mListJob;
    private List<MemberDto> mListDepartment;

    private OnItemClickListener mOnItemClickListener;
    private PopupCompanySetting popup;

    public MembersAdapter(Context context, List<User> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public MembersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_company_setting_member, parent, false);
        return new MembersHolder(view);
    }

    @Override
    public void onBindViewHolder(final MembersHolder holder, final int position) {

        if (GitOStorage.getInstance().getListLevel() != null) {
            for (Level level : GitOStorage.getInstance().getListLevel()) {
                if (!level.getName().isEmpty()) {
                    listJob.add(new MemberDto(level.getName(), level.getId()));
                }
            }
            mListJob = new ArrayList(listJob);
        }

        if (GitOStorage.getInstance().getListDepartment() != null) {
            for (Department department : GitOStorage.getInstance().getListDepartment()) {
                if (!department.getName().isEmpty()) {
                    listDepartment.add(new MemberDto(department.getName(), department.get_id()));
                }
            }
            mListDepartment = new ArrayList(listDepartment);
        }

        final User member = mList.get(position);
        TokenFile avatar = member.getAvatar();
        if (avatar != null) {
            holder.progressBar.setVisibility(View.VISIBLE);
            String url = CommonUtils.getURL(avatar);
            ImageLoader.load(mContext, url, holder.imgAvatar, holder.progressBar);
        } else {
            holder.progressBar.setVisibility(View.GONE);
            holder.tvShortName.setText(member.getUserId().substring(0, 1));
        }
        holder.tvName.setText(member.getUserId());

        if (member.getLevel().getName().equalsIgnoreCase("")) {
            holder.tvJob.setText("Not Submit");
            holder.tvJob.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        } else {
            holder.tvJob.setText(member.getLevel().getName());
        }

        if (member.getDepartment().getName().equalsIgnoreCase("")) {
            holder.tvDepartment.setText("Not Submit");
            holder.tvDepartment.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        } else {
            holder.tvDepartment.setText(member.getDepartment().getName());
        }

        holder.llJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupCompanySetting popup = new PopupCompanySetting(mContext, mListJob, new PopupCompanySetting.OnItemSelectedListener() {
                    @Override
                    public void onClickSelected(String codeFilter, String id) {
                        holder.tvJob.setText(codeFilter);
                        holder.tvJob.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                        mOnItemClickListener.onChangeLevel(position, id);
                    }
                });
                popup.showJob(holder.llJob, holder.tvJob.getText().toString());
            }
        });

        holder.llDepartment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupCompanySetting popup = new PopupCompanySetting(mContext, mListDepartment, new PopupCompanySetting.OnItemSelectedListener() {
                    @Override
                    public void onClickSelected(String codeFilter, String id) {
                        holder.tvDepartment.setText(codeFilter);
                        holder.tvDepartment.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                        mOnItemClickListener.onChangeDepartment(position, id);
                    }
                });
                popup.showDepartment(holder.llDepartment, holder.tvDepartment.getText().toString());
            }
        });

        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDelete(position);
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MembersHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_avatar_user)
        CircleImageView imgAvatar;

        @Bind(R.id.pgbar_avatar_user)
        ProgressBar progressBar;

        @Bind(R.id.tv_shortname_user)
        TextView tvShortName;

        @Bind(R.id.ll_drop_down_job)
        LinearLayout llJob;

        @Bind(R.id.tv_name_user)
        TextView tvName;

        @Bind(R.id.tv_job_user)
        TextView tvJob;

        @Bind(R.id.imv_job_user)
        ImageView imvJob;

        @Bind(R.id.ll_department)
        LinearLayout llDepartment;

        @Bind(R.id.tv_department)
        TextView tvDepartment;

        @Bind(R.id.ll_delete)
        LinearLayout llDelete;

        View view;

        public MembersHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);

        void onChangeLevel(int position, String id);

        void onChangeDepartment(int position, String id);

        void onDelete(int position);
    }
}
