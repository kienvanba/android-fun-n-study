package com.horical.gito.mvp.salary.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.list.presenter.ListSalaryPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListSalaryModule {
    @PerActivity
    @Provides
    ListSalaryPresenter provideSalaryPresenter(){
        return new ListSalaryPresenter();
    }
}
