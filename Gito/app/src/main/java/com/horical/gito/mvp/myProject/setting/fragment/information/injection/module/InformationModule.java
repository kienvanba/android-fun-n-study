package com.horical.gito.mvp.myProject.setting.fragment.information.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.setting.fragment.information.presenter.InformationPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class InformationModule {
    @PerFragment
    @Provides
    InformationPresenter provideInformationPresenter(){
        return new InformationPresenter();
    }
}
