package com.horical.gito.mvp.allProject.list.presenter;

public interface IAllProjectPresenter {
    void getAllProject();

    void getProjectDetails(String id);
}
