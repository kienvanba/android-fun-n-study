package com.horical.gito.mvp.roomBooking.newRoom.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.mvp.roomBooking.newRoom.injection.component.DaggerNewRoomComponent;
import com.horical.gito.mvp.roomBooking.newRoom.injection.component.NewRoomComponent;
import com.horical.gito.mvp.roomBooking.newRoom.injection.module.NewRoomModule;
import com.horical.gito.mvp.roomBooking.newRoom.presenter.NewRoomPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewRoomActivity extends BaseActivity implements INewRoomView {
    @Bind(R.id.new_room_top_bar)
    CustomViewTopBar topBar;

    @Bind(R.id.edit_room_name)
    EditText mRoomName;

    @Bind(R.id.edit_room_des)
    EditText mRoomDes;

    @Inject
    NewRoomPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roombooking_new);
        ButterKnife.bind(this);
        NewRoomComponent component = DaggerNewRoomComponent.builder()
                .newRoomModule(new NewRoomModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {

    }

    protected void initListener() {
        topBar.setTextTitle(getString(R.string.new_room));
        topBar.setTextViewLeft(getString(R.string.cancel));
        topBar.setTextViewRight(getString(R.string.create));

        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {
                mPresenter.createRoom(mRoomName.getText().toString(), mRoomDes.getText().toString());
            }
        });
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void createRoomSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void createRoomFailure(RestError err) {
        showRestErrorDialog(err);
    }

    @Override
    public void validateFailedNameEmpty() {
        showErrorDialog(getString(R.string.room_name_empty));
    }
}
