package com.horical.gito.interactor.event;

import com.horical.gito.interactor.event.type.InvalidTokenType;

import org.greenrobot.eventbus.EventBus;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class EventManager {

    private static final String TAG = makeLogTag(EventManager.class);

    public EventManager() {

    }

    public void register(Object obj) {
        EventBus.getDefault().register(obj);
    }

    public void unRegister(Object obj) {
        EventBus.getDefault().unregister(obj);
    }

    public void sendEvent(InvalidTokenType type) {
        EventBus.getDefault().post(type);
    }

}
