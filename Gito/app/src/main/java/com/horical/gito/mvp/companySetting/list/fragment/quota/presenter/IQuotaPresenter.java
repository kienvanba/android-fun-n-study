package com.horical.gito.mvp.companySetting.list.fragment.quota.presenter;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IQuotaPresenter {
    void getInfoQuota();
    void getSummaryQuota();
}
