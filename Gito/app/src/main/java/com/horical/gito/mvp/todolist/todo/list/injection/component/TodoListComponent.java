package com.horical.gito.mvp.todolist.todo.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.todolist.todo.list.injection.module.TodoListModule;
import com.horical.gito.mvp.todolist.todo.list.view.TodoListActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = TodoListModule.class)
public interface TodoListComponent {
    void inject(TodoListActivity todolistActivity);
}
