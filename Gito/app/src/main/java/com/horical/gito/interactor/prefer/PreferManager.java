package com.horical.gito.interactor.prefer;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.horical.gito.model.User;

public class PreferManager {

    SharedPreferences mPreferences;

    public PreferManager(SharedPreferences sharedPreferences) {
        mPreferences = sharedPreferences;
    }

    // Token
    private static final String KEY_USER_TOKEN = "USER_TOKEN";
    private static final String KEY_PROJECT_ID = "KEY_PROJECT_ID";
    private static final String KEY_USER = "USER_DATA";

    public String getToken() {
        return mPreferences.getString(KEY_USER_TOKEN, null);
    }

    public void setToken(String token) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(KEY_USER_TOKEN, token);
        editor.apply();
    }

    public String getProjectId() {
        return mPreferences.getString(KEY_PROJECT_ID, null);
    }

    public void setProjectId(String projectId) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(KEY_PROJECT_ID, projectId);
        editor.apply();
    }

    //    login
    public boolean isLogin() {
        return getToken() != null;
    }

    //logout
    public void resetUser() {
        setToken(null);
        setUser(null);
    }

    public void setUser(String user) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(KEY_USER, user);
        editor.apply();
    }

    public User getUser() {
        return new Gson().fromJson(mPreferences.getString(KEY_USER, new Gson().toJson(new User())), User.class);
    }

    // FCM token
    private static final String KEY_FCM_TOKEN = "KEY_FCM_TOKEN";

    public String getFcmToken() {
        return mPreferences.getString(KEY_FCM_TOKEN, null);
    }

    public void setFcmToken(String fcmToken) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(KEY_FCM_TOKEN, fcmToken);
        editor.apply();
    }

}
