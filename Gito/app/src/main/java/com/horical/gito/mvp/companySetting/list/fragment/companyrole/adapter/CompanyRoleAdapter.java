package com.horical.gito.mvp.companySetting.list.fragment.companyrole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.RoleCompany;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 30-Nov-16.
 */
public class CompanyRoleAdapter extends RecyclerView.Adapter<CompanyRoleAdapter.CompanyRoleHolder> {
    private Context mContext;
    private List<RoleCompany> mList;
    private OnItemClickListener mOnItemClickListener;

    public CompanyRoleAdapter(Context context, List<RoleCompany> list) {
        mContext = context;
        mList = list;
    }


    @Override
    public CompanyRoleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_company_setting_company_role, parent, false);
        return new CompanyRoleHolder(view);
    }

    @Override
    public void onBindViewHolder(CompanyRoleHolder holder, final int position) {
        RoleCompany roleCompany = mList.get(position);

        holder.tvName.setText(roleCompany.getRoleName());
        if(roleCompany.isWasUsed()){
            holder.llDelete.setVisibility(View.GONE);
        } else {
            holder.llDelete.setVisibility(View.VISIBLE);
        }
        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDelete(position);
            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class CompanyRoleHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.ll_delete)
        LinearLayout llDelete;

        View view;

        public CompanyRoleHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);

        void onDelete(int position);
    }
}
