package com.horical.gito.mvp.myProject.testCase.view;

import android.os.Bundle;
import android.view.View;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.myProject.testCase.injection.component.DaggerTestCaseComponent;
import com.horical.gito.mvp.myProject.testCase.injection.component.TestCaseComponent;
import com.horical.gito.mvp.myProject.testCase.injection.module.TestCaseModule;
import com.horical.gito.mvp.myProject.testCase.presenter.TestCasePresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by thanhle on 3/2/17.
 */

public class TestCaseActivity extends DrawerActivity implements View.OnClickListener, ITestCaseView {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;


    @Inject
    TestCasePresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_list_salary;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_SALARY;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        TestCaseComponent component = DaggerTestCaseComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .testCaseModule(new TestCaseModule())
                .build();

        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle("Test Case");
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_FILTER);
    }

    protected void initListener() {
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {

            @Override
            public void onLeftClicked() {
                openDrawer();
            }

            @Override
            public void onRightButtonOneClicked() {

            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}

