package com.horical.gito.mvp.signUp.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.user.RegisterRequest;
import com.horical.gito.interactor.api.response.user.RegisterResponse;
import com.horical.gito.mvp.signUp.view.ISignUpView;
import com.horical.gito.utils.EmailUtils;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class SignUpPresenter extends BasePresenter implements ISignUpPresenter {

    private static final String TAG = makeLogTag(SignUpPresenter.class);

    public void attachView(ISignUpView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public ISignUpView getView() {
        return (ISignUpView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void registerUser(String email, String companyId, String password, String confirmPassword) {

        if (!isViewAttached()) return;

        if (!EmailUtils.isValidEmail(email)) {
            getView().errorEmailInvalid();
            return;
        }

        if (!password.equals(confirmPassword)) {
            getView().errorConfirmPassword();
            return;
        }

        getView().showLoading();

        RegisterRequest registerRequest = new RegisterRequest(
                email,
                companyId,
                password
        );

        getApiManager().register(registerRequest, new ApiCallback<RegisterResponse>() {
            @Override
            public void success(RegisterResponse res) {
                if (!isViewAttached()) return;
                getView().hideLoading();
                getView().signUpSuccess(res);
            }

            @Override
            public void failure(RestError error) {
                if (!isViewAttached()) return;
                getView().hideLoading();
                getView().signUpError(error);
            }
        });
    }
}
