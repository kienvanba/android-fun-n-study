package com.horical.gito.mvp.allProject.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.allProject.list.presenter.AllProjectPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class AllProjectModule {

    @Provides
    @PerActivity
    AllProjectPresenter provideProjectPresenter() {
        return new AllProjectPresenter();
    }

}
