package com.horical.gito.mvp.meeting.detail.selectRoom.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.meeting.detail.selectRoom.injection.module.RoomModule;
import com.horical.gito.mvp.meeting.detail.selectRoom.view.RoomActivity;

import dagger.Component;

/**
 * Created by Lemon on 4/13/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = RoomModule.class)
public interface RoomComponent {
    void inject(RoomActivity activity);
}
