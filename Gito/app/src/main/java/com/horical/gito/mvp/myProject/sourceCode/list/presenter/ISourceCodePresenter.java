package com.horical.gito.mvp.myProject.sourceCode.list.presenter;

/**
 * Created by Tin on 24-Nov-16.
 */

public interface ISourceCodePresenter {
    void getAllCode(int perPage, int page);

    void createCode(String nameCode);
}
