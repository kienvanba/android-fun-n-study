package com.horical.gito.mvp.companySetting.list.fragment.info.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.model.Information;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.info.injection.component.DaggerInfoComponent;
import com.horical.gito.mvp.companySetting.list.fragment.info.injection.component.InfoComponent;
import com.horical.gito.mvp.companySetting.list.fragment.info.injection.module.InfoModule;
import com.horical.gito.mvp.companySetting.list.fragment.info.presenter.InfoPresenter;
import com.horical.gito.mvp.dialog.dialogInputText.InputTextDialog;
import com.horical.gito.mvp.dialog.dialogPhoto.DialogPhoto;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.utils.FileUtils;
import com.horical.gito.utils.NetworkUtils;

import java.io.File;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class InfoFragment extends BaseCompanyFragment implements IInfoView, View.OnClickListener {

    public static final String TAG = makeLogTag(InfoFragment.class);
    private static final int REQUEST_PERMISSION_CAPTURE_IMAGE = 0;
    private static final int REQUEST_PERMISSION_READ_LIBRARY = 1;

    private static final int REQUEST_CAPTURE_IMAGE = 3;
    private static final int REQUEST_READ_LIBRARY = 4;

    @Bind(R.id.refresh)
    SwipeRefreshLayout refresh;

    @Bind(R.id.img_logo)
    CircleImageView imgLogo;

    @Bind(R.id.pgbar_logo)
    ProgressBar pgBarLogo;

    @Bind(R.id.img_edit_company_name)
    ImageView imgEditCompanyName;

    @Bind(R.id.tv_company_name)
    TextView tvCompanyName;

    @Bind(R.id.tv_des)
    TextView tvDes;

    @Bind(R.id.ll_edit_desc)
    LinearLayout llEditDesc;

    @Bind(R.id.tv_company_id)
    TextView tvCompanyId;

    @Bind(R.id.tv_domain)
    TextView tvDomain;

    @Bind(R.id.ll_edit_domain)
    LinearLayout llEditDomain;

    @Bind(R.id.tv_phone)
    TextView tvPhone;

    @Bind(R.id.ll_edit_phone)
    LinearLayout llEditPhone;

    @Bind(R.id.tv_fax)
    TextView tvFax;

    @Bind(R.id.ll_edit_fax)
    LinearLayout llEditFax;

    @Bind(R.id.tv_email)
    TextView tvEmail;

    @Bind(R.id.ll_edit_email)
    LinearLayout llEditEmail;

    @Bind(R.id.tv_country)
    TextView tvCountry;

    @Bind(R.id.ll_edit_country)
    LinearLayout llEditCountry;

    @Bind(R.id.tv_language)
    TextView tvLanguage;

    @Bind(R.id.ll_edit_language)
    LinearLayout llEditLanguage;

    @Bind(R.id.tv_address)
    TextView tvAddress;

    @Bind(R.id.ll_edit_address)
    LinearLayout llEditAddress;

    private File mUploadFile;
    private InputTextDialog dialog;
    @Inject
    InfoPresenter mPresenter;

    public static InfoFragment newInstance() {
        Bundle args = new Bundle();
        InfoFragment fragment = new InfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_company_setting_info;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        InfoComponent component = DaggerInfoComponent.builder()
                .infoModule(new InfoModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        mPresenter.getCompanyInfo();
    }

    @Override
    protected void initListener() {
        imgLogo.setOnClickListener(this);
        imgEditCompanyName.setOnClickListener(this);

        llEditDesc.setOnClickListener(this);
        llEditDomain.setOnClickListener(this);
        llEditPhone.setOnClickListener(this);
        llEditFax.setOnClickListener(this);
        llEditEmail.setOnClickListener(this);
        llEditCountry.setOnClickListener(this);
        llEditLanguage.setOnClickListener(this);
        llEditAddress.setOnClickListener(this);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getCompanyInfo();
            }
        });
    }

    public void setData(Information info) {
        String url = CommonUtils.getURL(
                MainApplication.getAppComponent()
                        .getPreferManager()
                        .getUser()
                        .getCompanyId()
                        .getAvatar());
        ImageLoader.load(getContext(), url, imgLogo, pgBarLogo);

        tvCompanyName.setText(info.getCompanyName());
        tvCompanyName.setEnabled(false);
        tvCompanyId.setText(info.getCompanyId());
        tvCompanyId.setEnabled(false);
        tvDes.setText(info.getDesc());
        tvDes.setEnabled(false);
        tvDomain.setText(info.getDomain());
        tvDomain.setEnabled(false);
        tvPhone.setText(info.getPhone());
        tvPhone.setEnabled(false);
        tvFax.setText(info.getFax());
        tvFax.setEnabled(false);
        tvEmail.setText(info.getEmail());
        tvEmail.setEnabled(false);
        tvCountry.setText(info.getCountry());
        tvCountry.setEnabled(false);
        tvLanguage.setText(info.getLanguage());
        tvLanguage.setEnabled(false);
        tvAddress.setText(info.getAddress());
        tvAddress.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_logo:
                final DialogPhoto dialogPhoto = new DialogPhoto(getContext());
                dialogPhoto.setListener(new DialogPhoto.OnDialogEditPhotoClickListener() {

                    @Override
                    public void albumGallery() {
                        handleOpenLibrary();
                        dialogPhoto.dismiss();
                    }

                    @Override
                    public void takePhoto() {
                        handleOpenCamera();
                        dialogPhoto.dismiss();
                    }
                });
                dialogPhoto.show();
                break;
            case R.id.img_edit_company_name:
                dialog = new InputTextDialog(getContext(), 0, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        GitOStorage.getInstance().getInformation().setCompanyName(text);
                        tvCompanyName.setText(text);
                        mPresenter.updateInfo();
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_desc:
                dialog = new InputTextDialog(getContext(), 0, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        GitOStorage.getInstance().getInformation().setDesc(text);
                        tvDes.setText(text);
                        mPresenter.updateInfo();
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_domain:
                dialog = new InputTextDialog(getContext(), 0, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        GitOStorage.getInstance().getInformation().setDomain(text);
                        tvDomain.setText(text);
                        mPresenter.updateInfo();
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_phone:
                dialog = new InputTextDialog(getContext(), 1, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        GitOStorage.getInstance().getInformation().setPhone(text);
                        tvPhone.setText(text);
                        mPresenter.updateInfo();
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_fax:
                dialog = new InputTextDialog(getContext(), 1, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        GitOStorage.getInstance().getInformation().setFax(text);
                        tvFax.setText(text);
                        mPresenter.updateInfo();
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_email:
                dialog = new InputTextDialog(getContext(), 2, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        GitOStorage.getInstance().getInformation().setEmail(text);
                        tvEmail.setText(text);
                        mPresenter.updateInfo();
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_country:
                dialog = new InputTextDialog(getContext(), 0, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        GitOStorage.getInstance().getInformation().setCountry(text);
                        tvCountry.setText(text);
                        mPresenter.updateInfo();
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_language:
                dialog = new InputTextDialog(getContext(), 0, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        GitOStorage.getInstance().getInformation().setLanguage(text);
                        tvLanguage.setText(text);
                        mPresenter.updateInfo();
                    }
                });
                dialog.show();
                break;
            case R.id.ll_edit_address:
                dialog = new InputTextDialog(getContext(), 0, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        GitOStorage.getInstance().getInformation().setAddress(text);
                        tvAddress.setText(text);
                        mPresenter.updateInfo();
                    }
                });
                dialog.show();
                break;
        }
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getCompanyInfoSuccess(Information information) {
        refresh.setRefreshing(false);
        setData(information);
    }

    @Override
    public void getCompanyInfoFailure(String err) {
        refresh.setRefreshing(false);
    }

    @Override
    public void uploadSuccess(String url) {
        hideLoading();
        ImageLoader.load(getContext(), url, imgLogo, pgBarLogo);
        mPresenter.updateInfo();
    }

    @Override
    public void updateInfoSuccess() {
        Toast.makeText(getContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateInfoFailure(String err) {
        Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void uploadError(String error) {
        hideLoading();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CAPTURE_IMAGE) {
            handleOpenCamera();
        } else if (requestCode == REQUEST_PERMISSION_READ_LIBRARY) {
            handleOpenLibrary();
        }
    }

    private void handleOpenCamera() {
        String[] s = {android.Manifest.permission.CAMERA};
        if (checkPermissions(s)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            mUploadFile = FileUtils.createImageFile(getActivity());
            Uri uri = Uri.fromFile(mUploadFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, REQUEST_CAPTURE_IMAGE);

        } else {
            ActivityCompat.requestPermissions(getActivity(), s, REQUEST_PERMISSION_CAPTURE_IMAGE);
        }
    }

    private void handleOpenLibrary() {
        String s[] = {android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(s)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, REQUEST_READ_LIBRARY);

        } else {
            ActivityCompat.requestPermissions(getActivity(), s, REQUEST_PERMISSION_READ_LIBRARY);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAPTURE_IMAGE) {
                System.out.println("FILE " + mUploadFile.toString());
                if (!NetworkUtils.isConnected(getContext())) {
                    showNoNetworkErrorDialog();
                } else {
                    showLoading();
                    mPresenter.uploadFile(
                            MediaType.parse(android.webkit.MimeTypeMap.getFileExtensionFromUrl(mUploadFile.getName())),
                            mUploadFile);
                }
            } else if (requestCode == REQUEST_READ_LIBRARY) {
                System.out.println("File from library " + data.getData());
                if (!NetworkUtils.isConnected(getContext())) {
                    showNoNetworkErrorDialog();
                } else {
                    showLoading();
                    mUploadFile = FileUtils.convertUriToFile(getContext(), data.getData());
                    mPresenter.uploadFile(
                            MediaType.parse(getActivity().getContentResolver().getType(data.getData())),
                            mUploadFile);
                }
            }
        }
    }
}
