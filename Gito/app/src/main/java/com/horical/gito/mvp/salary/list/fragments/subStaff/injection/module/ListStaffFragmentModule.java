package com.horical.gito.mvp.salary.list.fragments.subStaff.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.list.fragments.subStaff.presenter.ListStaffFragmentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListStaffFragmentModule {
    @PerActivity
    @Provides
    ListStaffFragmentPresenter provideSalaryPresenter(){
        return new ListStaffFragmentPresenter();
    }
}
