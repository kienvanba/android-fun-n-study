package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/9/17
 */

public class CommentCreatorId implements Serializable{

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("displayName")
    @Expose
    private String displayName;

    @SerializedName("avatar")
    @Expose
    private TokenFile avatar;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public TokenFile getAvatar() {
        return avatar;
    }

    public void setAvatar(TokenFile avatar) {
        this.avatar = avatar;
    }
}
