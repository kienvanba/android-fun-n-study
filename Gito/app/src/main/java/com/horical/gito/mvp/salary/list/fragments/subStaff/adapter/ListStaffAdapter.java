package com.horical.gito.mvp.salary.list.fragments.subStaff.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListStaffAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<ListStaffDto> items;
    private StaffAdapterListener callback;

    public ListStaffAdapter(Context context, List<ListStaffDto> items, StaffAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_staff, parent, false);
        return new StaffHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListStaffDto listStaffDto = items.get(position);
        StaffHolder staffHolder = (StaffHolder) holder;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class StaffHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.tv_major)
        TextView tvMajor;

        @Bind(R.id.imv_info_edit)
        ImageView imvInfoEdit;

        @Bind(R.id.tv_type)
        TextView tvType;

        @Bind(R.id.tv_salary)
        TextView tvSalary;

        @Bind(R.id.imv_salary_edit)
        ImageView imvSalaryEdit;

        @Bind(R.id.tv_performance)
        TextView tvPerformance;

        @Bind(R.id.imv_performance_edit)
        ImageView imvPerformanceEdit;

        @Bind(R.id.tv_flexible)
        TextView tvFlexible;

        @Bind(R.id.imv_flexible_edit)
        ImageView imvFlexibleEdit;

        public StaffHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface StaffAdapterListener {
        void onSelected(int position);
    }
}