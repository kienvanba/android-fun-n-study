package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nxon on 3/17/17
 */

public class ChangeLogRole implements Serializable {

    @SerializedName("changelog")
    @Expose
    private boolean changeLog;

    public boolean isChangeLog() {
        return changeLog;
    }

    public void setChangeLog(boolean changeLog) {
        this.changeLog = changeLog;
    }
}
