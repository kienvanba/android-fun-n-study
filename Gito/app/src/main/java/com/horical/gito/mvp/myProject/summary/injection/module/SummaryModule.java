package com.horical.gito.mvp.myProject.summary.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.myProject.summary.presenter.SummaryPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SummaryModule {

    @Provides
    @PerActivity
    SummaryPresenter providerSummaryPresenter () {
        return new SummaryPresenter();
    }
}
