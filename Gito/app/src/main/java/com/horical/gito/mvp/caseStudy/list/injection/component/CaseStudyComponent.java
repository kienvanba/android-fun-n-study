package com.horical.gito.mvp.caseStudy.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.caseStudy.list.injection.module.CaseStudyModule;
import com.horical.gito.mvp.caseStudy.list.view.CaseStudyActivity;

import dagger.Component;

/**
 * Created by QuangHai on 10/11/2016.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = CaseStudyModule.class)
public interface CaseStudyComponent {
    void inject(CaseStudyActivity activity);
}
