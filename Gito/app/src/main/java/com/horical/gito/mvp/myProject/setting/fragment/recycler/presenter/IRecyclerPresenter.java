package com.horical.gito.mvp.myProject.setting.fragment.recycler.presenter;


import com.horical.gito.base.IRecyclerItem;

import java.util.List;

public interface IRecyclerPresenter {
    List<IRecyclerItem> getRecyclerItems();
}
