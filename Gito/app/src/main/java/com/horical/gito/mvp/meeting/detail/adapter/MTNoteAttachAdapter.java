package com.horical.gito.mvp.meeting.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.model.Note;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MTNoteAttachAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Note> mListAttachNote;
    private OnItemClickListener callback;

    public MTNoteAttachAdapter(Context context, List<Note> mListAttachNote, OnItemClickListener callback) {
        this.context = context;
        this.mListAttachNote = mListAttachNote;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_meeting_note_attatch, parent, false);
        return new AttachNoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Note note = mListAttachNote.get(position);
        AttachNoteViewHolder viewHolder = (AttachNoteViewHolder) holder;
        viewHolder.mTvAttachNote.setText(note.getTitle());
    }

    @Override
    public int getItemCount() {
        return mListAttachNote != null ? mListAttachNote.size() : 0;
    }

    public class AttachNoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.tv_add_meeting_detail_note_attatch)
        GitOTextView mTvAttachNote;

        @Bind(R.id.ic_delete_note_attatch)
        ImageView mBtnDeleteAttachNote;

        public AttachNoteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == mBtnDeleteAttachNote.getId()) {
                callback.onMTNoteAttachDelete(getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onMTNoteAttachDelete(int position);
    }

}
