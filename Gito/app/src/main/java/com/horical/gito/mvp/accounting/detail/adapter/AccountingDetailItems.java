package com.horical.gito.mvp.accounting.detail.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.Accounting;

/**
 * Created by Luong on 07-Mar-17.
 */

public class AccountingDetailItems implements IRecyclerItem {

    public Accounting mAccounting;

    public AccountingDetailItems(Accounting accounting) {
        this.mAccounting = accounting;
    }

    public Accounting getAccountingdetail() {
        return mAccounting;
    }

    public void setAccountingdetail(Accounting accounting) {
        mAccounting = accounting;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.ACCOUNTING_ITEM;
    }
}
