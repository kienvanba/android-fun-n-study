package com.horical.gito.mvp.todolist.note.list.view;

import com.horical.gito.base.IView;

/**
 * Created by Luong on 20-Mar-17.
 */

public interface INoteView extends IView {
    void showLoading();

    void hideLoading();

    void getAllNoteSuccess();

    void getAllNoteFailure(String error);

    void deleteNoteSuccess(int position);

    void deleteNoteFailure();
}
