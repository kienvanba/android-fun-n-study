package com.horical.gito.mvp.salary.dialog.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.model.User;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class StaffSalaryAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<User> items;
    private IUserAdapterListener callback;

    public StaffSalaryAdapter(Context context, List<User> items, IUserAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    public void setDataChanged(List<User> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_salary_staff, parent, false);
        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        User user = items.get(position);
        UserHolder userHolder = (UserHolder) holder;
        if (user.getAvatar() != null) {
            String url = CommonUtils.getURL(user.getAvatar());
            ImageLoader.load(context, url, userHolder.imvAvatar, userHolder.pgrLoading);
        } else {
            userHolder.pgrLoading.setVisibility(View.GONE);
            userHolder.tvShortName.setText(user.getDisplayName().substring(0, 2).toUpperCase());
            userHolder.tvShortName.setVisibility(View.VISIBLE);
        }
        userHolder.tvName.setText(user.getDisplayName());
        userHolder.tvRole.setText(user.getRoleCompany().getRoleName());
        userHolder.imvSelected.setSelected(user.isSelected());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imv_selected)
        ImageView imvSelected;

        @Bind(R.id.imv_avatar)
        CircleImageView imvAvatar;

        @Bind(R.id.pgr_avt_loading)
        ProgressBar pgrLoading;

        @Bind(R.id.tv_short_name)
        TextView tvShortName;

        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.tv_role)
        TextView tvRole;

        UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imvSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = items.get(getAdapterPosition());
                    user.setSelected(!user.isSelected());
                    imvSelected.setSelected(user.isSelected());
                    callback.onSelected(getAdapterPosition());
                }
            });
        }
    }

    public interface IUserAdapterListener {
        void onSelected(int position);
    }
}
