package com.horical.gito.mvp.companySetting.detail.newMember.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Department;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by kut3b on 11/04/2017.
 */

public class MemberDepartmentAdapter extends RecyclerView.Adapter<MemberDepartmentAdapter.DepartmentHolder> {
    private Context mContext;
    private List<Department> mList;

    public MemberDepartmentAdapter(Context context, List<Department> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public DepartmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_member, parent, false);
        return new DepartmentHolder(view);
    }

    @Override
    public void onBindViewHolder(final DepartmentHolder holder, final int position) {
        final Department department = mList.get(position);

        holder.tvName.setText(department.getName());
        holder.chkCheck.setChecked(department.isSelected());

        holder.chkCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                department.setSelected(!department.isSelected());
                notifyItemChanged(position);
                mOnItemClickListener.onItemClickSelected(department.get_id());
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                department.setSelected(!department.isSelected());
                notifyItemChanged(position);
                mOnItemClickListener.onItemClickSelected(department.get_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class DepartmentHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.chk_check)
        CheckBox chkCheck;

        @Bind(R.id.tv_name)
        TextView tvName;

        View view;

        public DepartmentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(String id);
    }
}