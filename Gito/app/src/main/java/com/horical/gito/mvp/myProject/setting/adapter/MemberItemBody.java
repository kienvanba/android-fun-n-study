package com.horical.gito.mvp.myProject.setting.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.MemberAndRole;

public class MemberItemBody implements IRecyclerItem {

    private MemberAndRole memberAndRole;

    public MemberItemBody(MemberAndRole memberAndRole){
        this.memberAndRole = memberAndRole;
    }

    public MemberAndRole getMemberAndRole() {
        return memberAndRole;
    }

    public void setMemberAndRole(MemberAndRole memberAndRole) {
        this.memberAndRole = memberAndRole;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.MEMBER_ITEM;
    }
}
