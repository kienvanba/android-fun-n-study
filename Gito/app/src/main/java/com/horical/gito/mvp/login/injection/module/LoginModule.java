package com.horical.gito.mvp.login.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.login.presenter.LoginPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {
    @Provides
    @PerActivity
    LoginPresenter provideLoginPresenter(){
        return new LoginPresenter();
    }
}
