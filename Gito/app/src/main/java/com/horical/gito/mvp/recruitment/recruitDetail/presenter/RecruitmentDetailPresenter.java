package com.horical.gito.mvp.recruitment.recruitDetail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.model.InterViewer;
import com.horical.gito.mvp.recruitment.recruitDetail.view.IRecruitmentDetailView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RecruitmentDetailPresenter extends BasePresenter implements IRecruitmentDetailPresenter {
    public void attachView(IRecruitmentDetailView view) {
        super.attachView(view);
    }

    public IRecruitmentDetailView getView() {
        return (IRecruitmentDetailView) getIView();
    }

    private List<Object> listInterview = new ArrayList<>();

    public List<Object> getListInterview() {
        return listInterview;
    }

    public void setListInterview(List<InterViewer> listInterview) {
        this.listInterview.addAll(listInterview);
    }
    private Date dateInterview;

    public Date getDateInterview() {
        return dateInterview;
    }

    public void setDateInterview(Date dateInterview) {
        this.dateInterview = dateInterview;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }


}
