package com.horical.gito.mvp.salary.bonus.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.bonus.detail.presenter.DetailBonusPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailBonusModule {
    @PerActivity
    @Provides
    DetailBonusPresenter provideSalaryPresenter(){
        return new DetailBonusPresenter();
    }
}