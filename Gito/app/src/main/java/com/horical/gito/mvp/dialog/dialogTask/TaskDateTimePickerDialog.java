package com.horical.gito.mvp.dialog.dialogTask;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.horical.gito.R;
import com.horical.gito.utils.HDate;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class TaskDateTimePickerDialog extends Dialog
        implements DatePicker.OnDateChangedListener, TimePicker.OnTimeChangedListener, View.OnClickListener {

    @Bind(R.id.dpk_task_start_day)
    DatePicker mDateFrom;

    @Bind(R.id.dpk_task_end_day)
    DatePicker mDateTo;

    @Bind(R.id.tpk_task_start_time)
    TimePicker mTimeFrom;

    @Bind(R.id.tpk_task_end_time)
    TimePicker mTimeTo;

    @Bind(R.id.tv_cancel)
    TextView mTvCancel;

    @Bind(R.id.tv_done)
    TextView mTvDone;


    DateTimeDialogCallback mCallback;

    Context mContext;

    public TaskDateTimePickerDialog(final Context context, DateTimeDialogCallback callback) {
        super(context);
        mContext = context;
        mCallback = callback;
        this.setContentView(R.layout.dialog_task_datatime_picker);
        ButterKnife.bind(this);
    }

    public HDate getFrom() {
        HDate DateTime = new HDate(
                mDateFrom.getYear(), mDateFrom.getMonth(), mDateFrom.getDayOfMonth(),
                mTimeFrom.getCurrentHour(), mTimeFrom.getCurrentMinute(), 0);
        return DateTime;
    }

    public void setFrom(HDate DateTime) {
        mDateFrom.updateDate(DateTime.year, DateTime.month, DateTime.day);
        mTimeFrom.setCurrentHour(DateTime.hour);
        mTimeFrom.setCurrentMinute(DateTime.minute);
    }

    public HDate getTo() {
        HDate DateTime = new HDate(
                mDateTo.getYear(), mDateTo.getMonth(), mDateTo.getDayOfMonth(),
                mTimeTo.getCurrentHour(), mTimeTo.getCurrentMinute(), 0);
        return DateTime;
    }

    public void setTo(HDate DateTime) {
        mDateTo.updateDate(DateTime.year, DateTime.month, DateTime.day);
        mTimeTo.setCurrentHour(DateTime.hour);
        mTimeTo.setCurrentMinute(DateTime.minute);
    }

    private boolean isSmallerDate(HDate var1, HDate var2) {
        if (!var1.year.equals(var2.year)) {
            return var1.year < var2.year;
        }
        if (!var1.month.equals(var2.month)) {
            return var1.month < var2.month;
        }
        if (!var1.day.equals(var2.day)) {
            return var1.day < var2.day;
        }
        return false;
    }

    private boolean isSmallerTime(HDate var1, HDate var2) {
        if (!var1.hour.equals(var2.hour)) return var1.hour < var2.hour;
        if (var1.minute.equals(var2.minute)) return var1.minute < var2.minute;
        else return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);
        mTimeFrom.setOnTimeChangedListener(this);
        mTimeTo.setOnTimeChangedListener(this);
        Calendar c = Calendar.getInstance();
        mDateFrom.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), this);
        mDateTo.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), this);
        mTvDone.setOnClickListener(this);
        mTvCancel.setOnClickListener(this);
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (view.getId() == mDateFrom.getId()) {
            if (isSmallerDate(getTo(), getFrom())) setTo(getFrom());
            return;
        }
        if (view.getId() == mDateTo.getId()) {
            if (isSmallerDate(getTo(), getFrom())) setFrom(getTo());
            return;
        }
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        if (!isSmallerDate(getFrom(), getTo()) && !isSmallerDate(getTo(), getFrom())) {
            if (view.getId() == mTimeFrom.getId()) {
                if (isSmallerTime(getTo(), getFrom())) setTo(getFrom());
                return;
            }
            if (view.getId() == mTimeTo.getId()) {
                if (isSmallerTime(getTo(), getFrom())) setFrom(getTo());
                return;
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (mTvDone.getId() == v.getId()) {
            mCallback.onDoneClick(getFrom(), getTo());
            cancel();
        }
        if (mTvCancel.getId() == v.getId()){
            mCallback.onCancelClick();
            cancel();
        }
    }

    public interface DateTimeDialogCallback {
        void onDoneClick(HDate DateTimeFrom, HDate DateTimeTo);
        void onCancelClick();
    }
}
