package com.horical.gito.mvp.drawer.dto;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.TokenFile;

/**
 * Created by nxon on 3/15/17.
 */

public class FooterDto implements IRecyclerItem {

    public TokenFile avatar;
    public String name;
    public String email;

    public FooterDto(TokenFile avatar, String name, String email) {
        this.avatar = avatar;
        this.name = name;
        this.email = email;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.MENU_FOOTER;
    }
}
