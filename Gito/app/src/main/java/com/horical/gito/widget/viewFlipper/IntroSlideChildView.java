package com.horical.gito.widget.viewFlipper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

public class IntroSlideChildView extends LinearLayout {

    private GitOTextView mTvTitle;
    private ImageView mIvBackground;

    public IntroSlideChildView(Context context) {
        super(context);
        initView();
    }

    public IntroSlideChildView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public IntroSlideChildView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        LayoutInflater.from(getContext()).inflate(R.layout.fragment_intro_sub, this, true);
        mTvTitle = (GitOTextView) findViewById(R.id.intro_tv_title);
        mIvBackground = (ImageView) findViewById(R.id.intro_iv_icon_slide);
    }

    public void setContent(String title, int src) {
        mTvTitle.setText(title);
        mIvBackground.setImageResource(src);
    }
}
