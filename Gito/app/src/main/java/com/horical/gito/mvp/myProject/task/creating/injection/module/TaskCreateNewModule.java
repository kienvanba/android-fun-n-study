package com.horical.gito.mvp.myProject.task.creating.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.myProject.task.creating.presenter.TaskCreateNewPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TaskCreateNewModule {
    @PerActivity
    @Provides
    TaskCreateNewPresenter providerTaskCreateNewPresenter() {
        return new TaskCreateNewPresenter();
    }
}
