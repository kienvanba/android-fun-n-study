package com.horical.gito.mvp.roomBooking.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.roomBooking.list.presenter.RoomBookingPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomBookingModule {
    @PerActivity
    @Provides
    RoomBookingPresenter provideRoomBookingPresenter() {
        return new RoomBookingPresenter();
    }
}
