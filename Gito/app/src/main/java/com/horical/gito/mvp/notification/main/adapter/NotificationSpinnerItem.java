package com.horical.gito.mvp.notification.main.adapter;

import com.horical.gito.base.IRecyclerItem;

/**
 * Created by Dragonoid on 3/17/2017.
 */

public class NotificationSpinnerItem implements IRecyclerItem {
    @Override
    public int getItemViewType() {
        return 0;
    }

    String mTittle;
    String mDate;
    public NotificationSpinnerItem(String tittle,String date ){
        mTittle = tittle;
        mDate = date;
    }
}
