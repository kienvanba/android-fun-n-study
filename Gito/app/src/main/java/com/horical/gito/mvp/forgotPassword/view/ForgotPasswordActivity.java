package com.horical.gito.mvp.forgotPassword.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.mvp.forgotPassword.injection.component.DaggerForgotPasswordComponent;
import com.horical.gito.mvp.forgotPassword.injection.component.ForgotPasswordComponent;
import com.horical.gito.mvp.forgotPassword.injection.module.ForgotPasswordModule;
import com.horical.gito.mvp.forgotPassword.presenter.ForgotPasswordPresenter;
import com.horical.gito.mvp.intro.view.IntroActivity;
import com.horical.gito.widget.button.GitOButton;
import com.horical.gito.widget.edittext.GitOEditText;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ForgotPasswordActivity extends BaseActivity implements IForgotPasswordView, View.OnClickListener {

    @Bind(R.id.forgot_btn_back)
    ImageView mBtnBack;

    @Bind(R.id.forgot_edt_email)
    GitOEditText mEdtEmail;

    @Bind(R.id.forgot_btn_send)
    GitOButton mBtnSend;

    @Inject
    ForgotPasswordPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        
        Window window = getWindow();
        window.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.transparent));
        }

        ForgotPasswordComponent component = DaggerForgotPasswordComponent.builder()
                .forgotPasswordModule(new ForgotPasswordModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        mBtnSend.setBlueStyle();
        //  hard code to test
        mEdtEmail.setText("long@gmail.com");
    }

    protected void initListener() {
        mBtnSend.setOnClickListener(this);
        mBtnBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mBtnBack.getId()) {
            Intent intent = new Intent(this, IntroActivity.class);
            startActivity(intent);
            finish();
        } else if (v.getId() == mBtnSend.getId()) {
            mPresenter.requestForgotPassword(mEdtEmail.getText().toString());
        }
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void errorEmptyInput() {
        showErrorDialog(getString(R.string.empty_input));
    }

    @Override
    public void errorInvalidEmail() {
        showErrorDialog(getString(R.string.email_invalid));
    }

    @Override
    public void requestForgotPasswordSuccess() {
        showToast(getString(R.string.forgot_notify_send_email));
    }

    @Override
    public void requestForgotPasswordError(RestError error) {
        showErrorDialog(getString(R.string.forgot_email_not_found));
    }
}
