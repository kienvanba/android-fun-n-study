package com.horical.gito.mvp.logTime.filter.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.logTime.filter.injection.module.LogTimeFilterModule;
import com.horical.gito.mvp.logTime.filter.view.LogTimeFilterActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = LogTimeFilterModule.class)
public interface LogTimeFilterComponent {
    void inject(LogTimeFilterActivity activity);
}
