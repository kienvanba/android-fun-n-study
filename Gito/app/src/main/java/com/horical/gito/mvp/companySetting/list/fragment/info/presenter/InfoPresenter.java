package com.horical.gito.mvp.companySetting.list.fragment.info.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.file.UploadFileRequest;
import com.horical.gito.interactor.api.request.file.UploadFileResponse;
import com.horical.gito.interactor.api.response.companySetting.GetCompanyInfoResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateInfoResponse;
import com.horical.gito.model.Information;
import com.horical.gito.mvp.companySetting.list.fragment.info.view.IInfoView;
import com.horical.gito.utils.CommonUtils;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;


public class InfoPresenter extends BasePresenter implements IInfoPresenter {
    public void attachView(IInfoView view) {
        super.attachView(view);
    }
    private Information information;
    @Override
    public void detachView() {
        super.detachView();
    }

    public IInfoView getView() {
        return (IInfoView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void getCompanyInfo() {
        getApiManager().getCompanyInfo(new ApiCallback<GetCompanyInfoResponse>() {

            @Override
            public void success(GetCompanyInfoResponse res) {
                getView().getCompanyInfoSuccess(res.information);
                GitOStorage.getInstance().setInformation(res.information);
            }

            @Override
            public void failure(RestError error) {
                getView().getCompanyInfoFailure(error.message);
            }
        });
    }

    @Override
    public void uploadFile(MediaType type, File file) {
        UploadFileRequest request = new UploadFileRequest(type, file);
        RequestBody status = RequestBody.create(okhttp3.MultipartBody.FORM, "0");
        getApiManager().uploadFile(request.file, status, new ApiCallback<UploadFileResponse>() {
            @Override
            public void success(UploadFileResponse res) {
                if (information == null) {
                    information = new Information();
                }
                GitOStorage.getInstance().getInformation().setAvatar(res.tokenFile);
                String url = CommonUtils.getURL(res.tokenFile);
                getView().uploadSuccess(url);
            }

            @Override
            public void failure(RestError error) {
                getView().uploadError(error.message);
            }
        });
    }

    @Override
    public void updateInfo() {
        getApiManager().updateInfo(GitOStorage.getInstance().getInformation(), new ApiCallback<GetUpdateInfoResponse>() {

            @Override
            public void success(GetUpdateInfoResponse res) {
                getView().updateInfoSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().updateInfoFailure(error.message);
            }
        });
    }
}

