package com.horical.gito.interactor.api.network.deserialize;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.horical.gito.model.Member;
import com.horical.gito.utils.GsonUtils;

import java.lang.reflect.Type;

public class MemberDeserialize implements JsonDeserializer<Member> {

    @Override
    public Member deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Member member;
        try {
            member = GsonUtils.createGson(Member.class).fromJson(json, Member.class);
        } catch (Exception ex) {
            member = new Member();
            member.set_id(json.getAsString());
        }
        return member;
    }
}
