package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/20/17.
 */

public class WorkingDayWeek implements Serializable {

    @SerializedName("sat")
    @Expose
    private boolean sat;

    @SerializedName("fri")
    @Expose
    private boolean fri;

    @SerializedName("thu")
    @Expose
    private boolean thu;

    @SerializedName("web")
    @Expose
    private boolean wed;

    @SerializedName("tue")
    @Expose
    private boolean tue;

    @SerializedName("mon")
    @Expose
    private boolean mon;

    @SerializedName("sun")
    @Expose
    private boolean sun;

    public boolean isSat() {
        return sat;
    }

    public void setSat(boolean sat) {
        this.sat = sat;
    }

    public boolean isFri() {
        return fri;
    }

    public void setFri(boolean fri) {
        this.fri = fri;
    }

    public boolean isThu() {
        return thu;
    }

    public void setThu(boolean thu) {
        this.thu = thu;
    }

    public boolean isWed() {
        return wed;
    }

    public void setWed(boolean wed) {
        this.wed = wed;
    }

    public boolean isTue() {
        return tue;
    }

    public void setTue(boolean tue) {
        this.tue = tue;
    }

    public boolean isMon() {
        return mon;
    }

    public void setMon(boolean mon) {
        this.mon = mon;
    }

    public boolean isSun() {
        return sun;
    }

    public void setSun(boolean sun) {
        this.sun = sun;
    }
}
