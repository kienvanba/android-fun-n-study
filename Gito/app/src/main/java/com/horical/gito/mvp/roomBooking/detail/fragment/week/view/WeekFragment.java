package com.horical.gito.mvp.roomBooking.detail.fragment.week.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.mvp.roomBooking.detail.dto.WeekDetailDto;
import com.horical.gito.mvp.roomBooking.detail.fragment.week.adapter.DetailWeekAdapter;
import com.horical.gito.mvp.roomBooking.detail.fragment.week.adapter.WeekDetailItem;
import com.horical.gito.mvp.roomBooking.detail.fragment.week.injection.component.DaggerWeekComponent;
import com.horical.gito.mvp.roomBooking.detail.fragment.week.injection.component.WeekComponent;
import com.horical.gito.mvp.roomBooking.detail.fragment.week.injection.module.WeekModule;
import com.horical.gito.mvp.roomBooking.detail.fragment.week.presenter.WeekPresenter;
import com.horical.gito.utils.ConvertUtils;
import com.horical.gito.widget.recycleview.RoomBookingRecyleView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;


public class WeekFragment extends BaseFragment implements IWeekView, View.OnClickListener {

    @Bind(R.id.imv_back)
    ImageView mBtnBack;

    @Bind(R.id.imv_next)
    ImageView mBtnNext;

    @Bind(R.id.tv_from)
    TextView mTvFrom;
    @Bind(R.id.tv_from_date)
    TextView mTvFromDate;

    @Bind(R.id.tv_to)
    TextView mTvTo;
    @Bind(R.id.tv_to_date)
    TextView mTvToDate;

    @Bind(R.id.rv_week_detail)
    RoomBookingRecyleView mRvWeekDetail;

    DetailWeekAdapter mWeekAdapter;
    List<IRecyclerItem> weekDetailDto;


    @Inject
    WeekPresenter mPresenter;

    public static WeekFragment newInstance() {
        Bundle args = new Bundle();
        WeekFragment fragment = new WeekFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        WeekComponent component = DaggerWeekComponent.builder()
                .weekModule(new WeekModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_week;
    }

    @Override
    protected void initData() {

        weekDetailDto = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            WeekDetailDto detailDto = new WeekDetailDto();
            detailDto.weekDetailDto = "button " + i;
            WeekDetailItem item = new WeekDetailItem(detailDto);
            item.lengh = ConvertUtils.convertDpToInt(getContext(), 7 ) * 30;
            weekDetailDto.add(item);
        }


        mWeekAdapter = new DetailWeekAdapter(weekDetailDto, getContext());

        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRvWeekDetail.setLayoutManager(llm);
        mRvWeekDetail.distanceHeight = ConvertUtils.convertDpToInt(getContext(), 10);
        mRvWeekDetail.distanceWidth = ConvertUtils.convertDpToInt(getContext(), 20);

        mRvWeekDetail.setAdapter(mWeekAdapter);

    }

    @Override
    public void onClick(View v) {

    }
}
