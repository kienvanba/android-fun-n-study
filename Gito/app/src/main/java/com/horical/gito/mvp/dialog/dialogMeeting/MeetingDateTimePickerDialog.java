package com.horical.gito.mvp.dialog.dialogMeeting;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.horical.gito.R;

import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Created by Lemon on 3/4/2017.
 */

public class MeetingDateTimePickerDialog extends Dialog
        implements DatePicker.OnDateChangedListener, TimePicker.OnTimeChangedListener, View.OnClickListener {

    @Bind(R.id.dpk_meeting_start_day)
    DatePicker mDateFrom;

    @Bind(R.id.dpk_meeting_end_day)
    DatePicker mDateTo;

    @Bind(R.id.tpk_meeting_start_time)
    TimePicker mTimeFrom;

    @Bind(R.id.tpk_meeting_end_time)
    TimePicker mTimeTo;

    @Bind(R.id.tv_cancel)
    TextView mTvCancel;

    @Bind(R.id.tv_done)
    TextView mTvDone;


    DateTimeDialogCallback mCallback;

    Context mContext;

    public MeetingDateTimePickerDialog(final Context context, DateTimeDialogCallback callback) {
        super(context);
        mContext = context;
        mCallback = callback;
        this.setContentView(R.layout.fragment_meeting_datatime_picker);
        ButterKnife.bind(this);
    }

    public Date getFrom() {
        Date DateTime = new Date(
                mDateFrom.getYear(), mDateFrom.getMonth(), mDateFrom.getDayOfMonth(),
                mTimeFrom.getCurrentHour(), mTimeFrom.getCurrentMinute(), 0);
        return DateTime;
    }

    public void setFrom(Date DateTime) {
        mDateFrom.updateDate(DateTime.getYear(), DateTime.getMonth(), DateTime.getDay());
        mTimeFrom.setCurrentHour(DateTime.getHours());
        mTimeFrom.setCurrentMinute(DateTime.getMinutes());
    }

    public Date getTo() {
        Date DateTime = new Date(
                mDateTo.getYear(), mDateTo.getMonth(), mDateTo.getDayOfMonth(),
                mTimeTo.getCurrentHour(), mTimeTo.getCurrentMinute(), 0);
        return DateTime;
    }

    public void setTo(Date DateTime) {
        mDateTo.updateDate(DateTime.getYear(), DateTime.getMonth(), DateTime.getDay());
        mTimeTo.setCurrentHour(DateTime.getHours());
        mTimeTo.setCurrentMinute(DateTime.getMinutes());
    }

    private boolean isSmallerDate(Date var1, Date var2) {
        if (!(var1.getYear() == var2.getYear())) {
            return var1.getYear() < var2.getYear();
        }
        if (!(var1.getMonth() == var2.getMonth())) {
            return var1.getMonth() < var2.getMonth();
        }
        if (!(var1.getDay() == var2.getDay())) {
            return var1.getDay() < var2.getDay();
        }
        return false;
    }

    private boolean isSmallerTime(Date var1, Date var2) {
        if (!(var1.getHours() == var2.getHours())) return var1.getHours() < var2.getHours();
        if (var1.getMinutes() == var2.getMinutes()) return var1.getMinutes() < var2.getMinutes();
        else return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);
        mTimeFrom.setOnTimeChangedListener(this);
        mTimeTo.setOnTimeChangedListener(this);
        Calendar c = Calendar.getInstance();
        mDateFrom.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), this);
        mDateTo.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), this);
        mTvDone.setOnClickListener(this);
        mTvCancel.setOnClickListener(this);
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (view.getId() == mDateFrom.getId()) {
            if (isSmallerDate(getTo(), getFrom())) setTo(getFrom());
            return;
        }
        if (view.getId() == mDateTo.getId()) {
            if (isSmallerDate(getTo(), getFrom())) setFrom(getTo());
            return;
        }
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        if (!isSmallerDate(getFrom(), getTo()) && !isSmallerDate(getTo(), getFrom())) {
            if (view.getId() == mTimeFrom.getId()) {
                if (isSmallerTime(getTo(), getFrom())) setTo(getFrom());
                return;
            }
            if (view.getId() == mTimeTo.getId()) {
                if (isSmallerTime(getTo(), getFrom())) setFrom(getTo());
                return;
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (mTvDone.getId() == v.getId()) {
            mCallback.onDoneClick(getFrom(), getTo());
            cancel();
        }
        if (mTvCancel.getId() == v.getId()){
            mCallback.onCancelClick();
            cancel();
        }
    }

    public interface DateTimeDialogCallback {
        void onDoneClick(Date DateTimeFrom, Date DateTimeTo);
        void onCancelClick();
    }
}
