package com.horical.gito.caches.files;

import android.content.Context;

import com.google.gson.Gson;
import com.horical.gito.model.User;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class UserCaches {
    public static final String CACHES_DIR = "user";
    public static final String CACHES_NAME = "user.caches";

    public static void writeCaches(Context context, User data) {
        if (data != null) {
            try {
                if (checkCaches(context)) {
                    deleteCaches(context);
                }
                String path = getDirCaches(context);
                File fileRoot = new File(path);
                if (!fileRoot.exists()) {
                    if (!fileRoot.mkdirs()) {
                        throw new Exception("Can not create dir");
                    }
                }
                FileWriter fileWriter = new FileWriter(new File(getFileCaches(context)));
                BufferedWriter bw = new BufferedWriter(fileWriter);

                Gson gson = new Gson();
                bw.write(gson.toJson(data));
                bw.close();
                fileWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static User readCaches(Context context) {
        User user = null;
        if (checkCaches(context)) {
            File file = new File(getFileCaches(context));
            try {
                FileReader fileReader = new FileReader(file);
                BufferedReader br = new BufferedReader(fileReader);
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                String data = sb.toString();
                Gson gson = new Gson();
                user = gson.fromJson(data, User.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    public static boolean checkCaches(Context context) {
        String path = getFileCaches(context);
        File file = new File(path);
        return file.exists();
    }

    public static boolean deleteCaches(Context context) {
        String path = getFileCaches(context);
        File file = new File(path);
        return file.delete();
    }

    private static String getFileCaches(Context context) {
        return context.getCacheDir() + File.separator + CACHES_DIR + File.separator + CACHES_NAME;
    }

    private static String getDirCaches(Context context) {
        return context.getCacheDir() + File.separator + CACHES_DIR;
    }
}
