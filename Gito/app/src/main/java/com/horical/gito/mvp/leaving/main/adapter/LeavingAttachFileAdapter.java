package com.horical.gito.mvp.leaving.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 3/10/2017.
 */

public class LeavingAttachFileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List mList = new ArrayList<>();
    Context mContext ;
    LeavingFileCallback mCallback;
    public LeavingAttachFileAdapter(List list, Context context, LeavingFileCallback callback){
        mContext = context;
        mList = list;
        mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leaving_new_attach_file,parent,false);
        return new LeavingAttachFileHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        LeavingAttachFileHolder itemHolder = (LeavingAttachFileHolder) holder;
        String itemItem = (String) mList.get(position);
        itemHolder.mFileName.setText(itemItem);
        itemHolder.mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onItemDelete(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class LeavingAttachFileHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.leaving_tv_new_file_name)
        TextView mFileName;
        @Bind(R.id.leaving_img_new_file_delete)
        ImageView mDelete;
        public LeavingAttachFileHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public interface LeavingFileCallback {
        void onItemDelete(int position);
    }
}
