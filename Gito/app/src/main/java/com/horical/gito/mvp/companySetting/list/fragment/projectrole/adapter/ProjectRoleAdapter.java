package com.horical.gito.mvp.companySetting.list.fragment.projectrole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.RoleProject;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 30-Nov-16.
 */
public class ProjectRoleAdapter extends RecyclerView.Adapter<ProjectRoleAdapter.ProjectRoleHolder> {
    private Context mContext;
    private List<RoleProject> mList;
    private OnItemClickListener mOnItemClickListener;

    public ProjectRoleAdapter(Context context, List<RoleProject> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public ProjectRoleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_company_setting_project_role, parent, false);
        return new ProjectRoleHolder(view);
    }

    @Override
    public void onBindViewHolder(ProjectRoleHolder holder, final int position) {
        RoleProject roleProject = mList.get(position);

        holder.tvName.setText(roleProject.getRoleName());
        if(roleProject.isWasUsed()){
            holder.llDelete.setVisibility(View.GONE);
        } else {
            holder.llDelete.setVisibility(View.VISIBLE);
        }

        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDelete(position);
            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ProjectRoleHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_name)
        TextView tvName;

        @Bind(R.id.ll_delete)
        LinearLayout llDelete;

        View view;

        public ProjectRoleHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);

        void onDelete(int position);
    }
}
