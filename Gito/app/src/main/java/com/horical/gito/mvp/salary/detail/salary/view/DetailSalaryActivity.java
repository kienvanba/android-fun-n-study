package com.horical.gito.mvp.salary.detail.salary.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.mvp.salary.detail.salary.adapter.DetailStaffAdapter;
import com.horical.gito.mvp.salary.detail.salary.adapter.DetailSummaryAdapter;
import com.horical.gito.mvp.salary.detail.salary.injection.component.DaggerIDetailSalaryComponent;
import com.horical.gito.mvp.salary.detail.salary.injection.component.IDetailSalaryComponent;
import com.horical.gito.mvp.salary.detail.salary.injection.module.DetailSalaryModule;
import com.horical.gito.mvp.salary.detail.salary.presenter.DetailSalaryPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailSalaryActivity extends BaseActivity implements View.OnClickListener, IDetailSalaryView {

    @Bind(R.id.rcv_summary)
    RecyclerView rcvSummary;

    @Bind(R.id.rcv_staff)
    RecyclerView rcvStaff;

    @Inject
    DetailSalaryPresenter mPresenter;

    private DetailStaffAdapter mStaffAdapter;
    private DetailSummaryAdapter mSummaryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_salary);
        ButterKnife.bind(this);

        IDetailSalaryComponent component = DaggerIDetailSalaryComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .detailSalaryModule(new DetailSalaryModule())
                .build();

        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        //summary
        mSummaryAdapter = new DetailSummaryAdapter(this, mPresenter.getSummaries(), new DetailSummaryAdapter.SummaryAdapterListener() {
            @Override
            public void onRemove(int position) {

            }
        });
        rcvSummary.setAdapter(mSummaryAdapter);
        rcvSummary.setLayoutManager(new LinearLayoutManager(this));

        //staff
        mStaffAdapter = new DetailStaffAdapter(this, mPresenter.getStaffs(), new DetailStaffAdapter.DetailStaffAdapterListener() {
            @Override
            public void onSelected(int position) {

            }
        });
        rcvStaff.setAdapter(mStaffAdapter);
        rcvStaff.setLayoutManager(new LinearLayoutManager(this));
    }

    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}