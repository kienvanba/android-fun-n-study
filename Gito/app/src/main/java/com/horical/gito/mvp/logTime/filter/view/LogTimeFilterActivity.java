package com.horical.gito.mvp.logTime.filter.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.logTime.filter.adapter.LogTimeFilterAdapter;
import com.horical.gito.mvp.logTime.filter.injection.component.DaggerLogTimeFilterComponent;
import com.horical.gito.mvp.logTime.filter.injection.component.LogTimeFilterComponent;
import com.horical.gito.mvp.logTime.filter.injection.module.LogTimeFilterModule;
import com.horical.gito.mvp.logTime.filter.presenter.LogTimeFilterPresenter;
import com.horical.gito.mvp.logTime.injection.component.LogTimeComponent;
import com.horical.gito.mvp.popup.dateSelected.CustomDateDialog;
import com.horical.gito.mvp.popup.dateSelected.DateDto;

import java.util.Date;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.LOGE;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class LogTimeFilterActivity extends BaseActivity implements View.OnClickListener, ILogTimeFilterView {

    private static final String TAG = makeLogTag(LogTimeFilterActivity.class);

    public static final String KEY_FILTER = "FILTER";
    public static final String KEY_TYPE = "TYPE";

    public static final int TYPE_APPROVED = 1;
    public static final int TYPE_REJECTED = 2;
    public static final int TYPE_ALL = 4;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar mTopBar;

    @Bind(R.id.cb_approved)
    CheckBox mCbApproved;

    @Bind(R.id.cb_rejected)
    CheckBox mCbRejected;

    @Bind(R.id.cb_all)
    CheckBox mCbAll;

    @Bind(R.id.rcv_filter_types)
    RecyclerView mRvFilterTypes;

    @Inject
    LogTimeFilterPresenter mPresenter;

    private LogTimeFilterAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_log_time_filter);
        ButterKnife.bind(this);

        LogTimeFilterComponent component = DaggerLogTimeFilterComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .logTimeFilterModule(new LogTimeFilterModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        mTopBar.setTextTitle(getString(R.string.filter));
        mTopBar.setTextViewLeft(getString(R.string.cancel));
        mTopBar.setTextViewRight(getString(R.string.done));

        int type = getIntent().getIntExtra(KEY_TYPE, 4);
        setTypeChecked(type);

        mPresenter.onCreateData((DateDto) getIntent().getSerializableExtra(KEY_FILTER));
        adapter = new LogTimeFilterAdapter(this, mPresenter.getList());
        mRvFilterTypes.setHasFixedSize(true);
        mRvFilterTypes.setLayoutManager(new LinearLayoutManager(this));
        mRvFilterTypes.setAdapter(adapter);
    }

    protected void initListener() {

        mTopBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {

            @Override
            public void onLeftClicked() {
                finish();
            }

            @Override
            public void onRightClicked() {
                int mType = TYPE_ALL;

                if (mCbApproved.isChecked()) {
                    mType = TYPE_APPROVED;
                } else if (mCbRejected.isChecked()) {
                    mType = TYPE_REJECTED;
                }

                Intent intent = getIntent();
                intent.putExtra(KEY_FILTER, adapter.getCurrentDateFilter());
                intent.putExtra(KEY_TYPE, mType);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        adapter.setOnItemClickListener(new LogTimeFilterAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(final int position) {
                CustomDateDialog dialogCustomDate = new CustomDateDialog(LogTimeFilterActivity.this, mPresenter.getList().get(position));

                dialogCustomDate.setOnDialogClickListener(new CustomDateDialog.OnDialogClickListener() {
                    @Override
                    public void onClickSaveCustomDate(DateDto dateDto) {
                        mPresenter.setCustomDate(dateDto);
                        adapter.notifyDataSetChanged();
                    }
                });
                dialogCustomDate.show();
            }
        });

        mCbApproved.setOnClickListener(this);
        mCbRejected.setOnClickListener(this);
        mCbAll.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        if (id == mCbApproved.getId()) {
            resetCheckbox();
            mCbApproved.setChecked(true);
        } else if (id == mCbRejected.getId()) {
            resetCheckbox();
            mCbRejected.setChecked(true);
        } else if (id == mCbAll.getId()) {
            resetCheckbox();
            mCbAll.setChecked(true);
        }
    }

    private void resetCheckbox() {
        mCbApproved.setChecked(false);
        mCbRejected.setChecked(false);
        mCbAll.setChecked(false);
    }

    private void setTypeChecked(int type) {
        if (type == TYPE_APPROVED) mCbApproved.setChecked(true);
        else if (type == TYPE_REJECTED) mCbRejected.setChecked(true);
        else mCbAll.setChecked(true);
    }

}
