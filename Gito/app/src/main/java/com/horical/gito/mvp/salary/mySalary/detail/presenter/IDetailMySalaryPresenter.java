package com.horical.gito.mvp.salary.mySalary.detail.presenter;

import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;

import java.util.List;

public interface IDetailMySalaryPresenter {
    List<ListStaffDto> getStaffs();

    void setStaffs(List<ListStaffDto> staffs);
}
