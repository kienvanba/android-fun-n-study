package com.horical.gito.widget.recycleview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.horical.gito.R;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.utils.ConvertUtils;

/**
 * Created by hoangsang on 3/7/17.
 */

public class GanttRecycleView extends RecyclerView {

    public int distanceWidth = 0;
    public int distanceHeight = 0;

    private Paint mPaint;

    public GanttRecycleView(Context context) {
        super(context);
    }

    public GanttRecycleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint();
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.GRAY);
        mPaint.setStrokeWidth(0);
        mPaint.setStyle(Paint.Style.FILL);

        if (distanceWidth > 0) {
            for (int i = distanceWidth / 2; i < getWidth(); i += distanceWidth)
                drawVerticalLine(canvas, i);
        }

        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.WHITE);
        mPaint.setStrokeWidth(8);
        if (distanceHeight > 0){
            for (int i = distanceHeight; i < getHeight(); i += distanceHeight)
                drawHorizontalLine(canvas, i);
        }

    }

    private void drawHorizontalLine(Canvas canvas, int h) {
        canvas.drawLine(0, h, getWidth(), h, mPaint);
    }

    private void drawVerticalLine(Canvas canvas, int w) {
        canvas.drawLine(w, 0, w, getHeight(), mPaint);
    }
}
