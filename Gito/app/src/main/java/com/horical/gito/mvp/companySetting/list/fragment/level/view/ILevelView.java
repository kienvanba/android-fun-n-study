package com.horical.gito.mvp.companySetting.list.fragment.level.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface ILevelView extends IView {
    void getAllLevelSuccess();

    void getAllLevelFailure(String err);

    void deleteLevelSuccess(int position);

    void deleteLevelFailure(String err);
}
