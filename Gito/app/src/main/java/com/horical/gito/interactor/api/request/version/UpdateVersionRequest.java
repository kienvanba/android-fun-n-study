package com.horical.gito.interactor.api.request.version;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class UpdateVersionRequest {

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("versionId")
    @Expose
    public String versionId;

    @SerializedName("releaseNote")
    @Expose
    public String releaseNote;

    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("dueDate")
    @Expose
    public Date dueDate;

    @SerializedName("status")
    @Expose
    public int status;
}
