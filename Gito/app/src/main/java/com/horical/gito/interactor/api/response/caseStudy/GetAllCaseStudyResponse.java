package com.horical.gito.interactor.api.response.caseStudy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.CaseStudy;

import java.util.List;

/**
 * Created by thanhle on 2/27/17
 */

public class GetAllCaseStudyResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<CaseStudy> caseStudies;
}
