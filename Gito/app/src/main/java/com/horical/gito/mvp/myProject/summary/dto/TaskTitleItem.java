package com.horical.gito.mvp.myProject.summary.dto;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.mvp.myProject.summary.adapter.SummaryAdapter;

/**
 * Created by nhonvt.dk on 3/21/17
 */

public class TaskTitleItem implements IRecyclerItem {

    @Override
    public int getItemViewType() {
        return SummaryAdapter.SUMMARY_TASK_TITLE_TYPE;
    }
}
