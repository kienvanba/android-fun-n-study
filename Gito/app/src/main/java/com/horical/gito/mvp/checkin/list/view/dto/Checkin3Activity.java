package com.horical.gito.mvp.checkin.list.view.dto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.mvp.checkin.detail.view.Checkin2Activity;
import com.horical.gito.mvp.checkin.list.view.adapter.AvatarMajorItem;
import com.horical.gito.mvp.checkin.list.view.adapter.CheckinWeekAdapter;
import com.horical.gito.mvp.intro.view.IntroActivity;
import com.horical.gito.widget.chartview.circle.CircleView;
import com.horical.gito.widget.chartview.circle.CircleViewAnimation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Penguin on 23-Nov-16.
 */

public class Checkin3Activity extends BaseActivity implements ICheckinView, View.OnClickListener {

    @Bind(R.id.rb_btn_day)
    RadioButton day;

    @Bind(R.id.rb_btn_week)
    RadioButton week;

    @Bind(R.id.rb_btn_month)
    RadioButton month;

    @Bind(R.id.iv_Back)
    ImageView Back;

    @Bind(R.id.iv_Next)
    ImageView Next;

    @Bind(R.id.tv_month)
    TextView tvMonth;

    @Bind(R.id.tv_year)
    TextView tvYear;

    @Bind(R.id.rb_btn_back)
    LinearLayout rb_btn_back;

    @Bind(R.id.iv_checkin_filter)
    ImageView filter;

    @Bind(R.id.circle_chart_checkin)
    CircleView circle;

    @Bind(R.id.rv_checkin_user)
    RecyclerView user;

    @Bind(R.id.img_show)
    ImageView show;

    @Bind(R.id.tv_1)
    TextView t1;
    @Bind(R.id.tv_2)
    TextView t2;
    @Bind(R.id.tv_3)
    TextView t3;
    @Bind(R.id.tv_4)
    TextView t4;
    @Bind(R.id.tv_5)
    TextView t5;
    @Bind(R.id.tv_6)
    TextView t6;
    @Bind(R.id.tv_7)
    TextView t7;
    @Bind(R.id.tv_8)
    TextView t8;
    @Bind(R.id.tv_9)
    TextView t9;
    @Bind(R.id.tv_10)
    TextView t10;
    @Bind(R.id.tv_11)
    TextView t11;
    @Bind(R.id.tv_12)
    TextView t12;
    @Bind(R.id.tv_13)
    TextView t13;
    @Bind(R.id.tv_14)
    TextView t14;
    @Bind(R.id.tv_15)
    TextView t15;
    @Bind(R.id.tv_16)
    TextView t16;
    @Bind(R.id.tv_17)
    TextView t17;
    @Bind(R.id.tv_18)
    TextView t18;
    @Bind(R.id.tv_19)
    TextView t19;
    @Bind(R.id.tv_20)
    TextView t20;
    @Bind(R.id.tv_21)
    TextView t21;
    @Bind(R.id.tv_22)
    TextView t22;
    @Bind(R.id.tv_23)
    TextView t23;
    @Bind(R.id.tv_24)
    TextView t24;
    @Bind(R.id.tv_25)
    TextView t25;
    @Bind(R.id.tv_26)
    TextView t26;
    @Bind(R.id.tv_27)
    TextView t27;
    @Bind(R.id.tv_28)
    TextView t28;
    @Bind(R.id.tv_29)
    TextView t29;
    @Bind(R.id.tv_30)
    TextView t30;
    @Bind(R.id.tv_31)
    TextView t31;
    @Bind(R.id.tv_32)
    TextView t32;
    @Bind(R.id.tv_33)
    TextView t33;
    @Bind(R.id.tv_34)
    TextView t34;
    @Bind(R.id.tv_35)
    TextView t35;

    Calendar cal;
    CheckinWeekAdapter mAdapter;
    Animation.AnimationListener mAnimationListener;
    List<AvatarMajorItem> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin_3);
        ButterKnife.bind(this);

        setOnClick();
        animation();
        setDate();
        getData();
        Init();

    }

    private void setOnClick() {
        day.setOnClickListener(this);
        week.setOnClickListener(this);
        month.setOnClickListener(this);
        Back.setOnClickListener(this);
        Next.setOnClickListener(this);
        rb_btn_back.setOnClickListener(this);
        filter.setOnClickListener(this);
        show.setOnClickListener(this);

    }

    private void setDate() {
        cal = Calendar.getInstance();
        SimpleDateFormat dft = new SimpleDateFormat("MMMM", Locale.getDefault());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy", Locale.getDefault());
        String strMonth = dft.format(cal.getTime());
        String strYear = sdf.format(cal.getTime());
        tvMonth.setText(strMonth);
        tvYear.setText(strYear);
    }

    private void animation() {
        CircleViewAnimation circleViewAnimation = new CircleViewAnimation(circle);
        circleViewAnimation.setDuration(2000);
        circle.startAnimation(circleViewAnimation);
    }

    public void Init() {
        mList = new ArrayList<>();

        final AvatarMajorItem avatarMajorItem = new AvatarMajorItem(R.drawable.ic_add_blue, "Add user");

        mList.add(avatarMajorItem);

        mAdapter = new CheckinWeekAdapter(this, mList);
        LinearLayoutManager li = new LinearLayoutManager(this);
        li.setOrientation(LinearLayoutManager.HORIZONTAL);
        user.setLayoutManager(li);
        user.setAdapter(mAdapter);

    }

    private void getData() {
        SimpleDateFormat sdf5 = new SimpleDateFormat("dd", Locale.getDefault());

        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.DAY_OF_MONTH, -2);
        String tv1 = sdf5.format(c1.getTime());
        t1.setText(tv1);
        t1.setTextColor(getResources().getColor(R.color.grey_x));

        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.DAY_OF_MONTH, -1);
        String tv2 = sdf5.format(c2.getTime());
        t2.setText(tv2);
        t2.setTextColor(getResources().getColor(R.color.grey_x));

        Calendar c3 = Calendar.getInstance();
        c3.set(Calendar.DAY_OF_MONTH, 0);
        String tv3 = sdf5.format(c3.getTime());
        t3.setText(tv3);
        t3.setTextColor(getResources().getColor(R.color.grey_x));

        Calendar c4 = Calendar.getInstance();
        c4.set(Calendar.DAY_OF_MONTH, 1);
        String tv4 = sdf5.format(c4.getTime());
        t4.setText(tv4);

        Calendar c5 = Calendar.getInstance();
        c5.set(Calendar.DAY_OF_MONTH, 2);
        String tv5 = sdf5.format(c5.getTime());
        t5.setText(tv5);

        Calendar c6 = Calendar.getInstance();
        c6.set(Calendar.DAY_OF_MONTH, 3);
        String tv6 = sdf5.format(c6.getTime());
        t6.setText(tv6);

        Calendar c7 = Calendar.getInstance();
        c7.set(Calendar.DAY_OF_MONTH, 4);
        String tv7 = sdf5.format(c7.getTime());
        t7.setText(tv7);

        Calendar c8 = Calendar.getInstance();
        c8.set(Calendar.DAY_OF_MONTH, 5);
        String tv8 = sdf5.format(c8.getTime());
        t8.setText(tv8);

        Calendar c9 = Calendar.getInstance();
        c9.set(Calendar.DAY_OF_MONTH, 6);
        String tv9 = sdf5.format(c9.getTime());
        t9.setText(tv9);

        Calendar c10 = Calendar.getInstance();
        c10.set(Calendar.DAY_OF_MONTH, 7);
        String tv10 = sdf5.format(c10.getTime());
        t10.setText(tv10);

        Calendar c11 = Calendar.getInstance();
        c11.set(Calendar.DAY_OF_MONTH, 8);
        String tv11 = sdf5.format(c11.getTime());
        t11.setText(tv11);

        Calendar c12 = Calendar.getInstance();
        c12.set(Calendar.DAY_OF_MONTH, 9);
        String tv12 = sdf5.format(c12.getTime());
        t12.setText(tv12);

        Calendar c13 = Calendar.getInstance();
        c13.set(Calendar.DAY_OF_MONTH, 10);
        String tv13 = sdf5.format(c13.getTime());
        t13.setText(tv13);

        Calendar c14 = Calendar.getInstance();
        c14.set(Calendar.DAY_OF_MONTH, 11);
        String tv14 = sdf5.format(c14.getTime());
        t14.setText(tv14);

        Calendar c15 = Calendar.getInstance();
        c15.set(Calendar.DAY_OF_MONTH, 12);
        String tv15 = sdf5.format(c15.getTime());
        t15.setText(tv15);

        Calendar c16 = Calendar.getInstance();
        c16.set(Calendar.DAY_OF_MONTH, 13);
        String tv16 = sdf5.format(c16.getTime());
        t16.setText(tv16);

        Calendar c17 = Calendar.getInstance();
        c17.set(Calendar.DAY_OF_MONTH, 14);
        String tv17 = sdf5.format(c17.getTime());
        t17.setText(tv17);

        Calendar c18 = Calendar.getInstance();
        c18.set(Calendar.DAY_OF_MONTH, 15);
        String tv18 = sdf5.format(c18.getTime());
        t18.setText(tv18);

        Calendar c19 = Calendar.getInstance();
        c19.set(Calendar.DAY_OF_MONTH, 16);
        String tv19 = sdf5.format(c19.getTime());
        t19.setText(tv19);

        Calendar c20 = Calendar.getInstance();
        c20.set(Calendar.DAY_OF_MONTH, 17);
        String tv20 = sdf5.format(c20.getTime());
        t20.setText(tv20);

        Calendar c21 = Calendar.getInstance();
        c21.set(Calendar.DAY_OF_MONTH, 18);
        String tv21 = sdf5.format(c21.getTime());
        t21.setText(tv21);

        Calendar c22 = Calendar.getInstance();
        c22.set(Calendar.DAY_OF_MONTH, 19);
        String tv22 = sdf5.format(c22.getTime());
        t22.setText(tv22);

        Calendar c23 = Calendar.getInstance();
        c23.set(Calendar.DAY_OF_MONTH, 20);
        String tv23 = sdf5.format(c23.getTime());
        t23.setText(tv23);

        Calendar c24 = Calendar.getInstance();
        c24.set(Calendar.DAY_OF_MONTH, 21);
        String tv24 = sdf5.format(c24.getTime());
        t24.setText(tv24);

        Calendar c25 = Calendar.getInstance();
        c25.set(Calendar.DAY_OF_MONTH, 22);
        String tv25 = sdf5.format(c25.getTime());
        t25.setText(tv25);

        Calendar c26 = Calendar.getInstance();
        c26.set(Calendar.DAY_OF_MONTH, 23);
        String tv26 = sdf5.format(c26.getTime());
        t26.setText(tv26);

        Calendar c27 = Calendar.getInstance();
        c27.set(Calendar.DAY_OF_MONTH, 24);
        String tv27 = sdf5.format(c27.getTime());
        t27.setText(tv27);

        Calendar c28 = Calendar.getInstance();
        c28.set(Calendar.DAY_OF_MONTH, 25);
        String tv28 = sdf5.format(c28.getTime());
        t28.setText(tv28);

        Calendar c29 = Calendar.getInstance();
        c29.set(Calendar.DAY_OF_MONTH, 26);
        String tv29 = sdf5.format(c29.getTime());
        t29.setText(tv29);

        Calendar c30 = Calendar.getInstance();
        c30.set(Calendar.DAY_OF_MONTH, 27);
        String tv30 = sdf5.format(c30.getTime());
        t30.setText(tv30);

        Calendar c31 = Calendar.getInstance();
        c31.set(Calendar.DAY_OF_MONTH, 28);
        String tv31 = sdf5.format(c31.getTime());
        t31.setText(tv31);

        Calendar c32 = Calendar.getInstance();
        c32.set(Calendar.DAY_OF_MONTH, 29);
        String tv32 = sdf5.format(c32.getTime());
        t32.setText(tv32);

        Calendar c33 = Calendar.getInstance();
        c33.set(Calendar.DAY_OF_MONTH, 30);
        String tv33 = sdf5.format(c33.getTime());
        t33.setText(tv33);

        Calendar c34 = Calendar.getInstance();
        c34.set(Calendar.DAY_OF_MONTH, 31);
        String tv34 = sdf5.format(c34.getTime());
        t34.setText(tv34);

        Calendar c35 = Calendar.getInstance();
        c35.set(Calendar.DAY_OF_MONTH, 32);
        String tv35 = sdf5.format(c35.getTime());
        t35.setText(tv35);
        t35.setTextColor(getResources().getColor(R.color.grey_x));

    }

    private void nextMonth() {
        SimpleDateFormat sdf6 = new SimpleDateFormat("dd", Locale.getDefault());

        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH);
        c1.set(Calendar.DAY_OF_MONTH, -3);
        c1.add(Calendar.DATE, 1);
        String tv1 = sdf6.format(c1.getTime());
        t1.setText(tv1);

        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH);
        c2.set(Calendar.DAY_OF_MONTH, -2);
        c2.add(Calendar.DAY_OF_MONTH, 1);
        String tv2 = sdf6.format(c2.getTime());
        t2.setText(tv2);

        Calendar c3 = Calendar.getInstance();
        c3.set(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH);
        c3.set(Calendar.DAY_OF_MONTH, -1);
        c3.add(Calendar.DAY_OF_MONTH, 1);
        String tv3 = sdf6.format(c3.getTime());
        t3.setText(tv3);

        Calendar c4 = Calendar.getInstance();
        c4.set(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH);
        c4.set(Calendar.DAY_OF_MONTH, 0);
        c4.add(Calendar.DAY_OF_MONTH, 1);
        String tv4 = sdf6.format(c4.getTime());
        t4.setText(tv4);

        Calendar c5 = Calendar.getInstance();
        c5.set(Calendar.DAY_OF_MONTH, 2);
        c5.add(Calendar.DAY_OF_MONTH, 1);
        String tv5 = sdf6.format(c5.getTime());
        t5.setText(tv5);

        Calendar c6 = Calendar.getInstance();
        c6.set(Calendar.DAY_OF_MONTH, 3);
        c6.add(Calendar.DAY_OF_MONTH, 1);
        String tv6 = sdf6.format(c6.getTime());
        t6.setText(tv6);

        Calendar c7 = Calendar.getInstance();
        c7.set(Calendar.DAY_OF_MONTH, 4);
        c7.add(Calendar.DAY_OF_MONTH, 1);
        String tv7 = sdf6.format(c7.getTime());
        t7.setText(tv7);

        Calendar c8 = Calendar.getInstance();
        c8.set(Calendar.DAY_OF_MONTH, 5);
        c8.add(Calendar.DAY_OF_MONTH, 1);
        String tv8 = sdf6.format(c8.getTime());
        t8.setText(tv8);

        Calendar c9 = Calendar.getInstance();
        c9.set(Calendar.DAY_OF_MONTH, 6);
        c9.add(Calendar.DAY_OF_MONTH, 1);
        String tv9 = sdf6.format(c9.getTime());
        t9.setText(tv9);

        Calendar c10 = Calendar.getInstance();
        c10.set(Calendar.DAY_OF_MONTH, 7);
        c10.add(Calendar.DAY_OF_MONTH, 1);
        String tv10 = sdf6.format(c10.getTime());
        t10.setText(tv10);

        Calendar c11 = Calendar.getInstance();
        c11.set(Calendar.DAY_OF_MONTH, 8);
        c11.add(Calendar.DAY_OF_MONTH, 1);
        String tv11 = sdf6.format(c11.getTime());
        t11.setText(tv11);

        Calendar c12 = Calendar.getInstance();
        c12.set(Calendar.DAY_OF_MONTH, 9);
        c12.add(Calendar.DAY_OF_MONTH, 1);
        String tv12 = sdf6.format(c12.getTime());
        t12.setText(tv12);

        Calendar c13 = Calendar.getInstance();
        c13.set(Calendar.DAY_OF_MONTH, 10);
        c13.add(Calendar.DAY_OF_MONTH, 1);
        String tv13 = sdf6.format(c13.getTime());
        t13.setText(tv13);

        Calendar c14 = Calendar.getInstance();
        c14.add(Calendar.DAY_OF_MONTH, 1);
        c14.set(Calendar.DAY_OF_MONTH, 11);
        String tv14 = sdf6.format(c14.getTime());
        t14.setText(tv14);

        Calendar c15 = Calendar.getInstance();
        c15.set(Calendar.DAY_OF_MONTH, 12);
        c15.add(Calendar.DAY_OF_MONTH, 1);
        String tv15 = sdf6.format(c15.getTime());
        t15.setText(tv15);

        Calendar c16 = Calendar.getInstance();
        c16.set(Calendar.DAY_OF_MONTH, 13);
        c16.add(Calendar.DAY_OF_MONTH, 1);
        String tv16 = sdf6.format(c16.getTime());
        t16.setText(tv16);

        Calendar c17 = Calendar.getInstance();
        c17.set(Calendar.DAY_OF_MONTH, 14);
        c17.add(Calendar.DAY_OF_MONTH, 1);
        String tv17 = sdf6.format(c17.getTime());
        t17.setText(tv17);

        Calendar c18 = Calendar.getInstance();
        c18.set(Calendar.DAY_OF_MONTH, 15);
        c18.add(Calendar.DAY_OF_MONTH, 1);
        String tv18 = sdf6.format(c18.getTime());
        t18.setText(tv18);

        Calendar c19 = Calendar.getInstance();
        c19.set(Calendar.DAY_OF_MONTH, 16);
        c19.add(Calendar.DAY_OF_MONTH, 1);
        String tv19 = sdf6.format(c19.getTime());
        t19.setText(tv19);

        Calendar c20 = Calendar.getInstance();
        c20.set(Calendar.DAY_OF_MONTH, 17);
        c20.add(Calendar.DAY_OF_MONTH, 1);
        String tv20 = sdf6.format(c20.getTime());
        t20.setText(tv20);

        Calendar c21 = Calendar.getInstance();
        c21.set(Calendar.DAY_OF_MONTH, 18);
        c21.add(Calendar.DAY_OF_MONTH, 1);
        String tv21 = sdf6.format(c21.getTime());
        t21.setText(tv21);

        Calendar c22 = Calendar.getInstance();
        c22.set(Calendar.DAY_OF_MONTH, 19);
        c22.add(Calendar.DAY_OF_MONTH, 1);
        String tv22 = sdf6.format(c22.getTime());
        t22.setText(tv22);

        Calendar c23 = Calendar.getInstance();
        c23.set(Calendar.DAY_OF_MONTH, 20);
        c23.add(Calendar.DAY_OF_MONTH, 1);
        String tv23 = sdf6.format(c23.getTime());
        t23.setText(tv23);

        Calendar c24 = Calendar.getInstance();
        c24.set(Calendar.DAY_OF_MONTH, 21);
        c24.add(Calendar.DAY_OF_MONTH, 1);
        String tv24 = sdf6.format(c24.getTime());
        t24.setText(tv24);

        Calendar c25 = Calendar.getInstance();
        c25.set(Calendar.DAY_OF_MONTH, 22);
        c25.add(Calendar.DAY_OF_MONTH, 1);
        String tv25 = sdf6.format(c25.getTime());
        t25.setText(tv25);

        Calendar c26 = Calendar.getInstance();
        c26.set(Calendar.DAY_OF_MONTH, 23);
        c26.add(Calendar.DAY_OF_MONTH, 1);
        String tv26 = sdf6.format(c26.getTime());
        t26.setText(tv26);

        Calendar c27 = Calendar.getInstance();
        c27.set(Calendar.DAY_OF_MONTH, 24);
        c27.add(Calendar.DAY_OF_MONTH, 1);
        String tv27 = sdf6.format(c27.getTime());
        t27.setText(tv27);

        Calendar c28 = Calendar.getInstance();
        c28.set(Calendar.DAY_OF_MONTH, 25);
        c28.add(Calendar.DAY_OF_MONTH, 1);
        String tv28 = sdf6.format(c28.getTime());
        t28.setText(tv28);

        Calendar c29 = Calendar.getInstance();
        c29.set(Calendar.DAY_OF_MONTH, 26);
        c29.add(Calendar.DAY_OF_MONTH, 1);
        String tv29 = sdf6.format(c29.getTime());
        t29.setText(tv29);

        Calendar c30 = Calendar.getInstance();
        c30.set(Calendar.DAY_OF_MONTH, 27);
        c30.add(Calendar.DAY_OF_MONTH, 1);
        String tv30 = sdf6.format(c30.getTime());
        t30.setText(tv30);

        Calendar c31 = Calendar.getInstance();
        c31.set(Calendar.DAY_OF_MONTH, 28);
        c31.add(Calendar.DAY_OF_MONTH, 1);
        String tv31 = sdf6.format(c31.getTime());
        t31.setText(tv31);

        Calendar c32 = Calendar.getInstance();
        c32.set(Calendar.DAY_OF_MONTH, 29);
        c32.add(Calendar.DAY_OF_MONTH, 1);
        String tv32 = sdf6.format(c32.getTime());
        t32.setText(tv32);

        Calendar c33 = Calendar.getInstance();
        c33.set(Calendar.DAY_OF_MONTH, 30);
        c33.add(Calendar.DAY_OF_MONTH, 1);
        String tv33 = sdf6.format(c33.getTime());
        t33.setText(tv33);

        Calendar c34 = Calendar.getInstance();
        c34.set(Calendar.DAY_OF_MONTH, 31);
        c34.add(Calendar.DAY_OF_MONTH, 1);
        String tv34 = sdf6.format(c34.getTime());
        t34.setText(tv34);

        Calendar c35 = Calendar.getInstance();
        c35.set(Calendar.DAY_OF_MONTH, 32);
        c35.add(Calendar.DAY_OF_MONTH, 1);
        String tv35 = sdf6.format(c35.getTime());
        t35.setText(tv35);

    }

    private void lastMonth() {
        SimpleDateFormat sdf7 = new SimpleDateFormat("dd", Locale.getDefault());

        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.DAY_OF_MONTH, -2);
        c1.add(Calendar.DATE, -1);
        String tv1 = sdf7.format(c1.getTime());
        t1.setText(tv1);

        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.DAY_OF_MONTH, -1);
        c2.add(Calendar.DAY_OF_MONTH, -1);
        String tv2 = sdf7.format(c2.getTime());
        t2.setText(tv2);

        Calendar c3 = Calendar.getInstance();
        c3.set(Calendar.DAY_OF_MONTH, 0);
        c3.add(Calendar.DAY_OF_MONTH, -1);
        String tv3 = sdf7.format(c3.getTime());
        t3.setText(tv3);

        Calendar c4 = Calendar.getInstance();
        c4.set(Calendar.DAY_OF_MONTH, 1);
        c4.add(Calendar.DAY_OF_MONTH, -1);
        String tv4 = sdf7.format(c4.getTime());
        t4.setText(tv4);

        Calendar c5 = Calendar.getInstance();
        c5.set(Calendar.DAY_OF_MONTH, 2);
        c5.add(Calendar.DAY_OF_MONTH, -1);
        String tv5 = sdf7.format(c5.getTime());
        t5.setText(tv5);

        Calendar c6 = Calendar.getInstance();
        c6.set(Calendar.DAY_OF_MONTH, 3);
        c6.add(Calendar.DAY_OF_MONTH, -1);
        String tv6 = sdf7.format(c6.getTime());
        t6.setText(tv6);

        Calendar c7 = Calendar.getInstance();
        c7.set(Calendar.DAY_OF_MONTH, 4);
        c7.add(Calendar.DAY_OF_MONTH, -1);
        String tv7 = sdf7.format(c7.getTime());
        t7.setText(tv7);

        Calendar c8 = Calendar.getInstance();
        c8.set(Calendar.DAY_OF_MONTH, 5);
        c8.add(Calendar.DAY_OF_MONTH, -1);
        String tv8 = sdf7.format(c8.getTime());
        t8.setText(tv8);

        Calendar c9 = Calendar.getInstance();
        c9.set(Calendar.DAY_OF_MONTH, 6);
        c9.add(Calendar.DAY_OF_MONTH, -1);
        String tv9 = sdf7.format(c9.getTime());
        t9.setText(tv9);

        Calendar c10 = Calendar.getInstance();
        c10.set(Calendar.DAY_OF_MONTH, 7);
        c10.add(Calendar.DAY_OF_MONTH, -1);
        String tv10 = sdf7.format(c10.getTime());
        t10.setText(tv10);

        Calendar c11 = Calendar.getInstance();
        c11.set(Calendar.DAY_OF_MONTH, 8);
        c11.add(Calendar.DAY_OF_MONTH, -1);
        String tv11 = sdf7.format(c11.getTime());
        t11.setText(tv11);

        Calendar c12 = Calendar.getInstance();
        c12.set(Calendar.DAY_OF_MONTH, 9);
        c12.add(Calendar.DAY_OF_MONTH, -1);
        String tv12 = sdf7.format(c12.getTime());
        t12.setText(tv12);

        Calendar c13 = Calendar.getInstance();
        c13.set(Calendar.DAY_OF_MONTH, 10);
        c13.add(Calendar.DAY_OF_MONTH, -1);
        String tv13 = sdf7.format(c13.getTime());
        t13.setText(tv13);

        Calendar c14 = Calendar.getInstance();
        c14.set(Calendar.DAY_OF_MONTH, 11);
        c14.add(Calendar.DAY_OF_MONTH, -1);
        String tv14 = sdf7.format(c14.getTime());
        t14.setText(tv14);

        Calendar c15 = Calendar.getInstance();
        c15.set(Calendar.DAY_OF_MONTH, 12);
        c15.add(Calendar.DAY_OF_MONTH, -1);
        String tv15 = sdf7.format(c15.getTime());
        t15.setText(tv15);

        Calendar c16 = Calendar.getInstance();
        c16.set(Calendar.DAY_OF_MONTH, 13);
        c16.add(Calendar.DAY_OF_MONTH, -1);
        String tv16 = sdf7.format(c16.getTime());
        t16.setText(tv16);

        Calendar c17 = Calendar.getInstance();
        c17.set(Calendar.DAY_OF_MONTH, 14);
        c17.add(Calendar.DAY_OF_MONTH, -1);
        String tv17 = sdf7.format(c17.getTime());
        t17.setText(tv17);

        Calendar c18 = Calendar.getInstance();
        c18.set(Calendar.DAY_OF_MONTH, 15);
        c18.add(Calendar.DAY_OF_MONTH, -1);
        String tv18 = sdf7.format(c18.getTime());
        t18.setText(tv18);

        Calendar c19 = Calendar.getInstance();
        c19.set(Calendar.DAY_OF_MONTH, 16);
        c19.add(Calendar.DAY_OF_MONTH, -1);
        String tv19 = sdf7.format(c19.getTime());
        t19.setText(tv19);

        Calendar c20 = Calendar.getInstance();
        c20.set(Calendar.DAY_OF_MONTH, 17);
        c20.add(Calendar.DAY_OF_MONTH, -1);
        String tv20 = sdf7.format(c20.getTime());
        t20.setText(tv20);

        Calendar c21 = Calendar.getInstance();
        c21.set(Calendar.DAY_OF_MONTH, 18);
        c21.add(Calendar.DAY_OF_MONTH, -1);
        String tv21 = sdf7.format(c21.getTime());
        t21.setText(tv21);

        Calendar c22 = Calendar.getInstance();
        c22.set(Calendar.DAY_OF_MONTH, 19);
        c22.add(Calendar.DAY_OF_MONTH, -1);
        String tv22 = sdf7.format(c22.getTime());
        t22.setText(tv22);

        Calendar c23 = Calendar.getInstance();
        c23.set(Calendar.DAY_OF_MONTH, 20);
        c23.add(Calendar.DAY_OF_MONTH, -1);
        String tv23 = sdf7.format(c23.getTime());
        t23.setText(tv23);

        Calendar c24 = Calendar.getInstance();
        c24.set(Calendar.DAY_OF_MONTH, 21);
        c24.add(Calendar.DAY_OF_MONTH, -1);
        String tv24 = sdf7.format(c24.getTime());
        t24.setText(tv24);

        Calendar c25 = Calendar.getInstance();
        c25.set(Calendar.DAY_OF_MONTH, 22);
        c25.add(Calendar.DAY_OF_MONTH, -1);
        String tv25 = sdf7.format(c25.getTime());
        t25.setText(tv25);

        Calendar c26 = Calendar.getInstance();
        c26.set(Calendar.DAY_OF_MONTH, 23);
        c26.add(Calendar.DAY_OF_MONTH, -1);
        String tv26 = sdf7.format(c26.getTime());
        t26.setText(tv26);

        Calendar c27 = Calendar.getInstance();
        c27.set(Calendar.DAY_OF_MONTH, 24);
        c27.add(Calendar.DAY_OF_MONTH, -1);
        String tv27 = sdf7.format(c27.getTime());
        t27.setText(tv27);

        Calendar c28 = Calendar.getInstance();
        c28.set(Calendar.DAY_OF_MONTH, 25);
        c28.add(Calendar.DAY_OF_MONTH, -1);
        String tv28 = sdf7.format(c28.getTime());
        t28.setText(tv28);

        Calendar c29 = Calendar.getInstance();
        c29.set(Calendar.DAY_OF_MONTH, 26);
        c29.add(Calendar.DAY_OF_MONTH, -1);
        String tv29 = sdf7.format(c29.getTime());
        t29.setText(tv29);

        Calendar c30 = Calendar.getInstance();
        c30.set(Calendar.DAY_OF_MONTH, 27);
        c30.add(Calendar.DAY_OF_MONTH, -1);
        String tv30 = sdf7.format(c30.getTime());
        t30.setText(tv30);

        Calendar c31 = Calendar.getInstance();
        c31.set(Calendar.DAY_OF_MONTH, 28);
        c31.add(Calendar.DAY_OF_MONTH, -1);
        String tv31 = sdf7.format(c31.getTime());
        t31.setText(tv31);

        Calendar c32 = Calendar.getInstance();
        c32.set(Calendar.DAY_OF_MONTH, 29);
        c32.add(Calendar.DAY_OF_MONTH, -1);
        String tv32 = sdf7.format(c32.getTime());
        t32.setText(tv32);

        Calendar c33 = Calendar.getInstance();
        c33.set(Calendar.DAY_OF_MONTH, 30);
        c33.add(Calendar.DAY_OF_MONTH, -1);
        String tv33 = sdf7.format(c33.getTime());
        t33.setText(tv33);

        Calendar c34 = Calendar.getInstance();
        c34.set(Calendar.DAY_OF_MONTH, 31);
        c34.add(Calendar.DAY_OF_MONTH, -1);
        String tv34 = sdf7.format(c34.getTime());
        t34.setText(tv34);

        Calendar c35 = Calendar.getInstance();
        c35.set(Calendar.DAY_OF_MONTH, 32);
        c35.add(Calendar.DAY_OF_MONTH, -1);
        String tv35 = sdf7.format(c35.getTime());
        t35.setText(tv35);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rb_btn_day:
                Intent d = new Intent(this, CheckinActivity.class);
                startActivity(d);
                break;
            case R.id.rb_btn_week:
                Intent w = new Intent(this, Checkin2Activity.class);
                startActivity(w);
                break;
            case R.id.rb_btn_month:
                break;
            case R.id.rb_btn_back:
                Intent b = new Intent(this, IntroActivity.class);
                startActivity(b);
                break;
            case R.id.iv_checkin_filter:
                Intent p1 = new Intent(this, CheckinFilter.class);
                startActivity(p1);
                break;
            case R.id.iv_Back:
                cal.add(Calendar.MONTH, -1);
                SimpleDateFormat sdf1 = new SimpleDateFormat("MMMM", Locale.getDefault());
                SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy", Locale.getDefault());
                String strLastmonth = sdf1.format(cal.getTime());
                String strLastyear = sdf3.format(cal.getTime());
                tvMonth.setText(strLastmonth);
                tvYear.setText(strLastyear);
                lastMonth();
                break;
            case R.id.iv_Next:
                cal.add(Calendar.MONTH, 1);
                SimpleDateFormat sdf2 = new SimpleDateFormat("MMMM", Locale.getDefault());
                SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy", Locale.getDefault());
                String strNextmonth = sdf2.format(cal.getTime());
                String strNextyear = sdf4.format(cal.getTime());
                tvMonth.setText(strNextmonth);
                tvYear.setText(strNextyear);
                nextMonth();
                break;
            case R.id.img_show:
                if (v.getId() == show.getId()) {
                    if (user.getVisibility() == View.VISIBLE) {
                        user.setVisibility(View.GONE);
                        show.startAnimation(RotateDown());

                    } else if (user.getVisibility() == View.GONE) {
                        user.setVisibility(View.VISIBLE);
                        show.startAnimation(RotateUp());
                    }
                }
                break;
        }
    }

    private Animation RotateUp() {
        Animation animation = new RotateAnimation(0f, 180f, RotateAnimation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setAnimationListener(mAnimationListener);
        animation.setFillAfter(true);
        animation.setDuration(250);
        return animation;
    }

    private Animation RotateDown() {
        Animation animation = new RotateAnimation(180f, 0f, RotateAnimation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setAnimationListener(mAnimationListener);
        animation.setFillAfter(true);
        animation.setDuration(250);
        return animation;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void getAllCheckinSuccess() {

    }

}
