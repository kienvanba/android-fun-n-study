package com.horical.gito.interactor.api.response.companySetting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.RoleProject;

import java.util.List;

/**
 * Created by thanhle on 3/23/17.
 */

public class GetAllProjectRoleResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<RoleProject> roleProjects;
}
