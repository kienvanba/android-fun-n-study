package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.injection.moduel;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.tag.presenter.TagPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */
@Module
public class TagModule {
    @PerFragment
    @Provides
    TagPresenter provideCodeTagPresenter(){
        return new TagPresenter();
    }
}
