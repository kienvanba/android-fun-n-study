package com.horical.gito.mvp.salary.list.fragments.subEmail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.salary.dto.emailForm.EmailFormDto;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListEmailFormAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<EmailFormDto> items;
    private EmailFormAdapterListener callback;

    public ListEmailFormAdapter(Context context, List<EmailFormDto> items, EmailFormAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_email_form, parent, false);
        return new EmailFormHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EmailFormDto emailFormDto = items.get(position);
        EmailFormHolder emailFormHolder = (EmailFormHolder) holder;
        emailFormHolder.tvName.setText(emailFormDto.getTitle());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class EmailFormHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_email_form_name)
        TextView tvName;

        @Bind(R.id.imv_email_form_delete)
        ImageView imvDelete;

        View view;

        public EmailFormHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onSelected(getAdapterPosition());
                }
            });

            imvDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onRemove(getAdapterPosition());
                }
            });
        }
    }

    public interface EmailFormAdapterListener {
        void onSelected(int position);

        void onRemove(int position);
    }
}