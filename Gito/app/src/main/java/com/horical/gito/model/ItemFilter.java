package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/9/17
 */

public class ItemFilter implements Serializable{

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("selected")
    @Expose
    private boolean selected;

    public ItemFilter(String name, boolean selected) {
        this.name = name;
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
