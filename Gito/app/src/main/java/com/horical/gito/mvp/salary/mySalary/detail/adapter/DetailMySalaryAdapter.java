package com.horical.gito.mvp.salary.mySalary.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.salary.dto.staff.ListStaffDto;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailMySalaryAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<ListStaffDto> items;
    private DetailStaffAdapterListener callback;

    public DetailMySalaryAdapter(Context context, List<ListStaffDto> items, DetailStaffAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_detail_metric_step_param, parent, false);
        return new DetailStaffHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListStaffDto listStaffDto = items.get(position);
        DetailStaffHolder detailStaffHolder = (DetailStaffHolder) holder;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class DetailStaffHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_metric_name)
        TextView tvName;

        @Bind(R.id.imv_metric_delete)
        ImageView imvDelete;

        public DetailStaffHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface DetailStaffAdapterListener {
        void onSelected(int position);
    }
}