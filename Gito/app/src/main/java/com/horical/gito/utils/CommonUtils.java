package com.horical.gito.utils;

import android.text.TextUtils;
import android.widget.Spinner;

import com.horical.gito.AppConstants;
import com.horical.gito.BuildConfig;
import com.horical.gito.model.TokenFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.floor;
import static java.lang.Math.pow;

public class CommonUtils {

    public static String[] splitName(String name) {
        if (name == null) return new String[]{"", ""};
        String[] part = name.split(" ");
        String firstName = "";
        if (part.length > 0) {
            firstName = part[0];
        }
        String lastName = "";
        if (part.length > 1) {
            lastName = part[part.length - 1];
        }
        return new String[]{firstName, lastName};
    }

    public static String getURL(TokenFile file) {
        if (file == null || file.getTokenFile() == null || file.getTokenFile().isEmpty()) return "";

        String key[] = {"edocze", "edocne", "gitoserver"};
        String token1 = file.getTokenFile().substring(0, 10);
        String token2 = file.getTokenFile().substring(10, file.getTokenFile().length());

        return BuildConfig.FILE_URL
                + "/fileitem/"
                + file.getId() + "/"
                + token1 + key[0] + "/"
                + file.getCompanyId() + "/"
                + token2 + key[1] + "/"
                + key[2];
    }

    public static String amountFormatEnGB(double amount, String currency) {
        DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance();
        formatter.applyPattern("#,###,##0.00");
        if (TextUtils.isEmpty(currency)) {
            currency = "usd";
        }
        return formatter.format(roundAmount(amount, 2)) + " " + currency.toUpperCase();
    }

    public static String amountCurrency(int amount, String currency) {
        if (TextUtils.isEmpty(currency)) {
            currency = "usd";
        }
        return amount + " " + currency.toUpperCase();
    }

    public static double roundAmount(double amount, int precision) {
        double power = pow(10, precision);
        double poweredAmount = amount * power;
        double poweredPrimary = floor(poweredAmount);
        double poweredRemain = poweredAmount - poweredPrimary;
        if (poweredRemain > 0.5) {
            poweredPrimary += 1;
        }
        return poweredPrimary / power;
    }

    public static Object deepCopy(Object o) {
        try {
            //Serialization of object
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(o);

            //De-serialization of object
            ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
            ObjectInputStream in = new ObjectInputStream(bis);

            return in.readObject();
        } catch (Exception ex) {

        }
        return null;
    }

    public static void hideSpinnerDropDown(Spinner spinner) {
        try {
            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            method.setAccessible(true);
            method.invoke(spinner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        if (TextUtils.isEmpty(email)) {
            return true;
        }
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isPassword(String pass) {
        boolean isValid = false;
        if (TextUtils.isEmpty(pass)) {
            return true;
        }
        String expression = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*()?&])[A-Za-z\\d$@$!%*?&()]{8,}";

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pass);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static int parseStringToInt(String source) {
        int result;
        if (TextUtils.isEmpty(source)) {
            result = 0;
        } else {
            try {
                result = Integer.parseInt(source);
            } catch (NumberFormatException e) {
                result = 0;
            }
        }
        return result;
    }
}
