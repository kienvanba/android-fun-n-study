package com.horical.gito.mvp.companySetting.detail.newProjectRole.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.dto.ContentNewCompanyRole;

import java.util.HashMap;
import java.util.List;

/**
 * Created by thanhle on 3/20/17.
 */

public class NewProjectRoleAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private List<String> mListDataHeader;
    private HashMap<String, List<ContentNewCompanyRole>> mListDataChild;

    public NewProjectRoleAdapter(Context context, List<String> listDataHeader, HashMap<String, List<ContentNewCompanyRole>> listChildData) {
        this.mContext = context;
        this.mListDataHeader = listDataHeader;
        this.mListDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final ContentNewCompanyRole content = (ContentNewCompanyRole) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_content_new_company_role, null);
        }
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_content_title);
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.img_icon);
        CheckBox chkCheck = (CheckBox) convertView.findViewById(R.id.chk_check_item);

        tvTitle.setText(content.getTitle());
        imgIcon.setImageResource(content.getIdDrawalble());
        chkCheck.setChecked(content.isSelection());

        chkCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content.setSelection(!content.isSelection());
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.mListDataChild.get(this.mListDataHeader.get(groupPosition)) != null ? this.mListDataChild.get(this.mListDataHeader.get(groupPosition))
                .size() : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mListDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.mListDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String header = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_header_new_company_role, null);
        }

        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
        ImageView imgDropDown = (ImageView) convertView.findViewById(R.id.img_drop_down);

        tvTitle.setText(header);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}