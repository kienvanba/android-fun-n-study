package com.horical.gito.mvp.report.newReport.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.report.newReport.presenter.NewReportPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class NewReportModule {
    @PerActivity
    @Provides
    NewReportPresenter provideReportPresenter(){
        return new NewReportPresenter();
    }
}
