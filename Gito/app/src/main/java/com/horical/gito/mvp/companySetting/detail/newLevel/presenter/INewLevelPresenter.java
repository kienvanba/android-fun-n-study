package com.horical.gito.mvp.companySetting.detail.newLevel.presenter;

import com.horical.gito.model.Level;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface INewLevelPresenter {
    void updateLevel(Level level);

    void createLevel(Level level);
}
