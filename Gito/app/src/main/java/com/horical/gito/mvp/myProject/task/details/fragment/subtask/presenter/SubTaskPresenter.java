package com.horical.gito.mvp.myProject.task.details.fragment.subtask.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IView;
import com.horical.gito.mvp.myProject.task.details.fragment.subtask.view.ISubTaskView;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class SubTaskPresenter extends BasePresenter implements ISubTaskPresenter {

    private static final String TAG = makeLogTag(SubTaskPresenter.class);

    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public ISubTaskView getView() {
        return ((ISubTaskView) getIView());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

}
