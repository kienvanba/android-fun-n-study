package com.horical.gito.mvp.survey.statistical.view;

import com.horical.gito.base.IView;

/**
 * Created by nhattruong251295 on 4/7/2017.
 */

public interface IStatisticalView extends IView{
    void showLoading();

    void hideLoading();
}
