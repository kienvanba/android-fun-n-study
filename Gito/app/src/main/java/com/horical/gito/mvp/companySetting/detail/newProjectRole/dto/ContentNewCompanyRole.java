package com.horical.gito.mvp.companySetting.detail.newProjectRole.dto;

/**
 * Created by thanhle on 3/20/17.
 */

public class ContentNewCompanyRole {
    private int idDrawalble;
    private String title;
    private boolean isSelection;

    public ContentNewCompanyRole(int idDrawalble, String title, boolean isSelection) {
        this.idDrawalble = idDrawalble;
        this.title = title;
        this.isSelection = isSelection;
    }

    public int getIdDrawalble() {
        return idDrawalble;
    }

    public void setIdDrawalble(int idDrawalble) {
        this.idDrawalble = idDrawalble;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelection() {
        return isSelection;
    }

    public void setSelection(boolean selection) {
        isSelection = selection;
    }
}
