package com.horical.gito.mvp.roomBooking.detail.view;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.roomBooking.detail.adapter.RoomDetailAdapter;
import com.horical.gito.mvp.roomBooking.detail.adapter.RoomDetailViewPagerAdapter;
import com.horical.gito.mvp.roomBooking.detail.injection.component.DaggerRoomDetailComponent;
import com.horical.gito.mvp.roomBooking.detail.injection.component.RoomDetailComponent;
import com.horical.gito.mvp.roomBooking.detail.injection.module.RoomDetailModule;
import com.horical.gito.mvp.roomBooking.detail.presenter.RoomDetailPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lemon on 3/15/2017.
 */

public class RoomDetailActivity extends BaseActivity implements IRoomDetailView {
    public static final String ARG_ROOMBOOKING = "RoomBooking";

    @Bind(R.id.view_top_bar_room_detail)
    CustomViewTopBar topBar;

    @Bind(R.id.room_detail_list_item)
    RecyclerView mRvListItem;

    @Bind(R.id.room_detail__viewPager)
    ViewPager mViewPager;

    private RoomDetailAdapter mAdapterRoomDetail;
    private RoomDetailViewPagerAdapter mAdapterViewPager;


    @Inject
    RoomDetailPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_detail);
        ButterKnife.bind(this);

        RoomDetailComponent component = DaggerRoomDetailComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .roomDetailModule(new RoomDetailModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle("Room 1");
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);

        mAdapterRoomDetail = new RoomDetailAdapter(RoomDetailActivity.this, mPresenter.getListData());
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvListItem.setLayoutManager(llm);
        mRvListItem.setHasFixedSize(false);
        mRvListItem.setAdapter(mAdapterRoomDetail);

        mAdapterViewPager = new RoomDetailViewPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapterViewPager);
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

            }
        });

        mAdapterRoomDetail.setOnItemClickListener(new RoomDetailAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                mAdapterRoomDetail.setSelectedIndex(position);
                mAdapterRoomDetail.notifyDataSetChanged();
                mViewPager.setCurrentItem(position);
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mAdapterRoomDetail.setSelectedIndex(position);
                mAdapterRoomDetail.notifyDataSetChanged();
                mRvListItem.smoothScrollToPosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

}
