package com.horical.gito.mvp.salary.detail.metricGroup.presenter;

import com.horical.gito.model.SalaryMetricGroup;


public interface IDetailMetricGroupPresenter {
    SalaryMetricGroup getMetricGroup();

    void requestGetMetricGroup(String metricGroupId);

    void requestDeleteSalaryMetric(String metricId);

}
