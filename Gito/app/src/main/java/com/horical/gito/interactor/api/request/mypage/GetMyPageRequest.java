package com.horical.gito.interactor.api.request.mypage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class GetMyPageRequest {

    @SerializedName("startDate")
    @Expose
    public Date startDate;

    @SerializedName("endDate")
    @Expose
    public Date endDate;

    public GetMyPageRequest(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
