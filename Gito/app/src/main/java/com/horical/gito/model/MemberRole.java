package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nxon on 3/17/17
 */

public class MemberRole implements Serializable {

    @SerializedName("add")
    @Expose
    private Boolean add;

    @SerializedName("del")
    @Expose
    private Boolean del;

    @SerializedName("viewUser")
    @Expose
    private Boolean viewUser;

    @SerializedName("modify")
    @Expose
    private Boolean modify;

    @SerializedName("view")
    @Expose
    private Boolean view;

    public Boolean isViewUser() {
        return viewUser;
    }

    public void setViewUser(Boolean viewUser) {
        this.viewUser = viewUser;
    }

    public Boolean isAdd() {
        return add;
    }

    public void setAdd(Boolean add) {
        this.add = add;
    }

    public Boolean isDel() {
        return del;
    }

    public void setDel(Boolean del) {
        this.del = del;
    }

    public Boolean isModify() {
        return modify;
    }

    public void setModify(Boolean modify) {
        this.modify = modify;
    }

    public Boolean isView() {
        return view;
    }

    public void setView(Boolean view) {
        this.view = view;
    }
}
