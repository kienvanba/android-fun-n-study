package com.horical.gito.mvp.todolist.todo.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.todolist.todo.list.presenter.TodoListPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Luong on 14-Mar-17.
 */
@Module
public class TodoListModule  {
    @Provides
    @PerActivity
    TodoListPresenter provideTodoListPresenter(){
        return new TodoListPresenter();
    }
}
