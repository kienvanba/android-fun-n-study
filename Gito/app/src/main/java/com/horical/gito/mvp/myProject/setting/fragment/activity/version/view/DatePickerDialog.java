package com.horical.gito.mvp.myProject.setting.fragment.activity.version.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DatePickerDialog extends Dialog{
    @Bind(R.id.date_picker_start)
    public DatePicker dpStart;
    @Bind(R.id.date_picker_end)
    public DatePicker dpEnd;
    @Bind(R.id.btn_save)
    Button btnSave;

    private OnSaveClickListener mListener;

    public DatePickerDialog(@NonNull Context context) {
        super(context,android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar_MinWidth);
        setContentView(R.layout.layout_date_picker);

        initData();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick();
            }
        });
    }

    private void initData(){
        ButterKnife.bind(this);
    }

    public void setSaveClickListener(OnSaveClickListener listener){
        this.mListener = listener;
    }

    public interface OnSaveClickListener{
        void onClick();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ButterKnife.unbind(this);
    }
}
