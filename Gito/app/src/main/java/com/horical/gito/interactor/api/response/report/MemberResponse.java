package com.horical.gito.interactor.api.response.report;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.ResultsReport;

import java.util.ArrayList;
import java.util.List;

public class MemberResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    private List<ResultsReport> results;

    public List<ResultsReport> getResults() {
        if (results == null) {
            results = new ArrayList<>();
        }
        return results;
    }

    public void setResults(List<ResultsReport> results) {
        this.results = results;
    }
}
