package com.horical.gito.mvp.companySetting.list.fragment.projectrole.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllProjectRoleResponse;
import com.horical.gito.model.RoleProject;
import com.horical.gito.mvp.companySetting.list.fragment.projectrole.view.IProjectRoleView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class ProjectRolePresenter extends BasePresenter implements IProjectRolePresenter {

    private List<RoleProject> listProjectRole;

    public void attachView(IProjectRoleView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IProjectRoleView getView() {
        return (IProjectRoleView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        listProjectRole = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<RoleProject> getListProjectRole() {
        return listProjectRole;
    }

    public void setListProjectRole(List<RoleProject> listProjectRole) {
        this.listProjectRole = listProjectRole;
    }

    @Override
    public void getAllProjectRole() {
        getApiManager().getAllProjectRole(new ApiCallback<GetAllProjectRoleResponse>() {
            @Override
            public void success(GetAllProjectRoleResponse res) {
                if (listProjectRole != null) {
                    listProjectRole.clear();
                } else {
                    listProjectRole = new ArrayList<>();
                }
                listProjectRole.addAll(res.roleProjects);
                getView().getAllProjectRoleSuccess();
            }

            @Override
            public void failure(RestError error) {
                if (listProjectRole != null) {
                    listProjectRole.clear();
                } else {
                    listProjectRole = new ArrayList<>();
                }
                getView().getAllProjectRoleFailure(error.message);
            }
        });
    }

    @Override
    public void deleteProjectRole(String idProjectRole, final int position) {
        getApiManager().deleteProjectRole(idProjectRole, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteProjectRoleSuccess(position);
            }

            @Override
            public void failure(RestError error) {
                getView().deleteProjectRoleFailure(error.message);
            }
        });
    }
}

