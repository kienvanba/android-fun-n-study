package com.horical.gito.mvp.report.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.dialog.dialogInputEdit.InputEditDialog;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.popup.dateSelected.DateDto;
import com.horical.gito.mvp.popup.dateSelected.SelectDatePopup;
import com.horical.gito.mvp.report.adapter.ReportByContentAdapter;
import com.horical.gito.mvp.report.adapter.ReportByDateAdapter;
import com.horical.gito.mvp.report.adapter.ReportMemberAdapter;
import com.horical.gito.mvp.report.injection.component.DaggerReportComponent;
import com.horical.gito.mvp.report.injection.component.ReportComponent;
import com.horical.gito.mvp.report.injection.module.ReportModule;
import com.horical.gito.mvp.report.newReport.view.NewReportActivity;
import com.horical.gito.mvp.report.presenter.ReportPresenter;
import com.horical.gito.utils.AnimationUtils;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.FROM_MENU;
import static com.horical.gito.AppConstants.KEY_FROM;

public class ReportActivity extends DrawerActivity implements View.OnClickListener, IReportView {

    public static final int STATUS_NEW = 0;
    public static final int STATUS_APPROVED = 1;
    public static final int STATUS_REJECTED = 2;
    public static final int STATUS_SUBMITTED = 3;
    public final int KEY_SEND_NEW_REPORT = 33;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.rcv_report_member)
    RecyclerView mLvMember;

    @Bind(R.id.rcv_list_detail_by_date)
    RecyclerView mLvDetailDate;

    @Bind(R.id.rcv_list_detail_by_content)
    RecyclerView mLvDetailContent;

    @Bind(R.id.fr_hide_show_member)
    FrameLayout frHideShowMember;

    @Bind(R.id.imv_hide_show_member)
    ImageView imvHideShowMember;

    @Bind(R.id.tv_by_date)
    TextView tvByDate;

    @Bind(R.id.tv_by_content)
    TextView tvByContent;

    @Bind(R.id.ln_choose_date)
    LinearLayout lnChoose;

    @Bind(R.id.tv_date)
    TextView tvDate;

    private ReportMemberAdapter adapterMember;
    private ReportByDateAdapter adapterByDate;
    private ReportByContentAdapter adapterByContent;

    private InputEditDialog inputDialog;
    private Intent newReport;
    private int initialMemberHeight;

    @Inject
    ReportPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_report;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_REPORT;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        ReportComponent component = DaggerReportComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .reportModule(new ReportModule())
                .build();

        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(getString(R.string.report));
        int from = getIntent().getIntExtra(KEY_FROM, FROM_MENU);
        mPresenter.setFrom(from);
        if (from == FROM_MENU) {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        } else {
            topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
            lockDrawer();
        }
        topBar.setImageViewRight(
                CustomViewTopBar.DRAWABLE_USER_REPORT,
                CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT
        );

        adapterMember = new ReportMemberAdapter(this, mPresenter.getListMember());
        mLvMember.setHasFixedSize(true);
        mLvMember.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mLvMember.setAdapter(adapterMember);

        tvDate.setText(mPresenter.getDate().getTypeDate());

        //list detail arr
        adapterByDate = new ReportByDateAdapter(this, mPresenter.getListByDate());
        mLvDetailDate.setHasFixedSize(true);
        mLvDetailDate.setLayoutManager(new LinearLayoutManager(this));
        mLvDetailDate.setAdapter(adapterByDate);

        adapterByContent = new ReportByContentAdapter(this, mPresenter.getListByContent());
        mLvDetailContent.setHasFixedSize(true);
        mLvDetailContent.setLayoutManager(new StickyHeaderLayoutManager());
        mLvDetailContent.setAdapter(adapterByContent);

        mPresenter.getDataMember();
    }

    protected void initListener() {
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                if (mPresenter.getFrom() == FROM_MENU) openDrawer();
                else onBackPressed();
            }

            @Override
            public void onRightButtonOneClicked() {
                if (newReport == null) {
                    newReport = new Intent(ReportActivity.this, NewReportActivity.class);
                    startActivityForResult(newReport, KEY_SEND_NEW_REPORT);
                }
            }

            @Override
            public void onRightButtonTwoClicked() {
                mPresenter.getMyReport();
            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });

        adapterMember.setOnItemClickListener(new ReportMemberAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                mPresenter.setOnChooseMember(position);
            }
        });

        adapterByDate.setOnItemClickListener(new ReportByDateAdapter.OnClickItemListener() {
            @Override
            public void onEditDoing(final int position) {
                if (mPresenter.isCheckPermission()) {
                    showErrorDialog(getString(R.string.inform_not_permission));
                    return;
                }
                if (inputDialog == null || !inputDialog.isShowing()) {
                    inputDialog = new InputEditDialog(ReportActivity.this, mPresenter.getListByDate().get(position).getDoing(), new InputEditDialog.OnDoneListener() {
                        @Override
                        public void onDone(String text) {
                            mPresenter.setDataDoingForByDate(position, text);
                            inputDialog.dismiss();

                        }
                    });
                    inputDialog.show();

                }
            }

            @Override
            public void onEditProblem(final int position) {
                if (mPresenter.isCheckPermission()) {
                    showErrorDialog(getString(R.string.inform_not_permission));
                    return;
                }
                if (inputDialog == null || !inputDialog.isShowing()) {
                    inputDialog = new InputEditDialog(ReportActivity.this, mPresenter.getListByDate().get(position).getProblem(), new InputEditDialog.OnDoneListener() {
                        @Override
                        public void onDone(String text) {
                            mPresenter.setDataProblemForByDate(position, text);
                            inputDialog.dismiss();
                        }
                    });
                    inputDialog.show();
                }
            }

            @Override
            public void onEditFinish(final int position) {
                if (mPresenter.isCheckPermission()) {
                    showErrorDialog(getString(R.string.inform_not_permission));
                    return;
                }

                if (inputDialog == null || !inputDialog.isShowing()) {
                    inputDialog = new InputEditDialog(ReportActivity.this, mPresenter.getListByDate().get(position).getFinished(), new InputEditDialog.OnDoneListener() {
                        @Override
                        public void onDone(String text) {
                            mPresenter.setDataFinishForByDate(position, text);
                            inputDialog.dismiss();
                        }
                    });
                    inputDialog.show();
                }
            }

            @Override
            public void onButtonReject(final int position) {
                if (mPresenter.isCheckPermission()) {
                    showErrorDialog(getString(R.string.inform_not_permission));
                    return;
                }

                if (inputDialog == null || !inputDialog.isShowing()) {
                    inputDialog = new InputEditDialog(ReportActivity.this, mPresenter.getListByDate().get(position).getRejectMsg(), new InputEditDialog.OnDoneListener() {
                        @Override
                        public void onDone(String text) {
                            mPresenter.setDataRejectForByDate(position, text);
                            inputDialog.dismiss();
                        }
                    });
                    inputDialog.show();
                }
            }

            @Override
            public void onButtonApprove(int position) {
                if (mPresenter.isCheckPermission()) {
                    showErrorDialog(getString(R.string.inform_not_permission));
                    return;
                }

                mPresenter.setStatusApproveReportByDate(position);
            }

            @Override
            public void onButtonSubmit(int position) {
                if (mPresenter.isCheckPermission()) {
                    showErrorDialog(getString(R.string.inform_not_permission));
                    return;
                }

                mPresenter.setStatusSubmittedReportByDate(position);
            }
        });

        frHideShowMember.setOnClickListener(this);
        tvByDate.setOnClickListener(this);
        tvByContent.setOnClickListener(this);
        lnChoose.setOnClickListener(this);
        tvByDate.performClick();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        newReport = null;
        if (requestCode == KEY_SEND_NEW_REPORT) {
            if (resultCode == RESULT_OK) {
                mPresenter.getMyReport();
            }
        }
    }

    private SelectDatePopup selectedDate;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fr_hide_show_member:
                if (mLvMember.getVisibility() == View.GONE) {
                    AnimationUtils.expand(mLvMember, initialMemberHeight);
                    imvHideShowMember.setRotation(180);
                } else {
                    initialMemberHeight = mLvMember.getMeasuredHeight();
                    AnimationUtils.collapse(mLvMember, initialMemberHeight);
                    imvHideShowMember.setRotation(0);
                }
                break;

            case R.id.tv_by_date:
                tvByDate.setSelected(true);
                tvByContent.setSelected(false);
                mLvDetailDate.setVisibility(View.VISIBLE);
                mLvDetailContent.setVisibility(View.GONE);

                break;

            case R.id.tv_by_content:
                tvByDate.setSelected(false);
                tvByContent.setSelected(true);
                mLvDetailDate.setVisibility(View.GONE);
                mLvDetailContent.setVisibility(View.VISIBLE);

                break;

            case R.id.ln_choose_date:
                if (selectedDate == null || !selectedDate.isShowing()) {
                    selectedDate = new SelectDatePopup(this, mPresenter.getDate(), new SelectDatePopup.OnItemSelectedListener() {
                        @Override
                        public void onClickItemDialog(DateDto dateRange) {
                            mPresenter.setDate(dateRange);
                            tvDate.setText(dateRange.getTypeDate());
                            mPresenter.getDataMember();
                        }
                    });
                    selectedDate.showDateDefault(
                            lnChoose,
                            90,
                            SelectDatePopup.GRAVITY_END,
                            6);
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void showError(String error) {
        dismissDialog();
        showErrorDialog(error);
    }

    @Override
    public void onNotifyAdapterReport() {
        adapterMember.notifyDataSetChanged();
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void dismissLoading() {
        dismissDialog();
    }

    @Override
    public void onNotifyAdapterByDate() {
        adapterByDate.notifyDataSetChanged();
    }

    @Override
    public void onNotifyAdapterByContent() {
        adapterByContent.notifyAllSectionsDataSetChanged();
    }
}

