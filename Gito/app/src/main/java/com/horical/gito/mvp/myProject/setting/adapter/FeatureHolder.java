package com.horical.gito.mvp.myProject.setting.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FeatureHolder extends RecyclerView.ViewHolder{
    @Bind(R.id.img_icon)
    ImageView mImIcon;
    @Bind(R.id.edit_name)
    TextView mTvName;
    @Bind(R.id.checkbox)
    CheckBox mCbCheck;

    public FeatureHolder(View itemView){
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
