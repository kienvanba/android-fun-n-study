package com.horical.gito.mvp.recruitment.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.recruitment.adapter.RecruitmentAdapter;
import com.horical.gito.mvp.recruitment.injection.component.DaggerRecruitmentComponent;
import com.horical.gito.mvp.recruitment.injection.component.RecruitmentComponent;
import com.horical.gito.mvp.recruitment.injection.module.RecruitmentModule;
import com.horical.gito.mvp.recruitment.presenter.RecruitmentPresenter;
import com.horical.gito.mvp.recruitment.recruitDetail.view.RecruitmentDetailActivity;
import com.horical.gito.widget.dialog.DialogPositiveNegative;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.FROM_MENU;
import static com.horical.gito.AppConstants.KEY_FROM;

public class RecruitmentActivity extends DrawerActivity implements IRecruitmentView, View.OnClickListener {

    public static final String KEY_ID_RECRUIT = "KEY_ID_RECRUIT";

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.rcv_recruit)
    RecyclerView mLv;

    @Bind(R.id.refresh)
    SwipeRefreshLayout refreshLayout;

    private RecruitmentAdapter adapter;
    private Intent recruitDetail;
    @Inject
    RecruitmentPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_recruitment;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_RECRUITMENT;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RecruitmentComponent component = DaggerRecruitmentComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .recruitmentModule(new RecruitmentModule())
                .build();

        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    int from;

    protected void initData() {
        topBar.setTextTitle(getString(R.string.recruitment));
        topBar.setImageViewLeft(FROM_MENU);
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_LAMP, CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);

        from = getIntent().getIntExtra(KEY_FROM, FROM_MENU);
        if (from != FROM_MENU) {
            topBar.setImageViewLeft(R.drawable.ic_back_white);
            lockDrawer();
        }

        adapter = new RecruitmentAdapter(this, mPresenter.getListRecruit());
        mLv.setHasFixedSize(true);
        mLv.setLayoutManager(new LinearLayoutManager(this));
        mLv.setAdapter(adapter);

        mPresenter.getListRecruitmentFromServer();

    }

    protected void initListener() {
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {

            @Override
            public void onLeftClicked() {
                if (from == FROM_MENU) openDrawer();
                else onBackPressed();
            }

            @Override
            public void onRightButtonOneClicked() {
                if (recruitDetail == null) {
                    recruitDetail = new Intent(RecruitmentActivity.this, RecruitmentDetailActivity.class);
                    startActivityForResult(recruitDetail, 33);
                }
            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getListRecruitmentFromServer();

            }
        });

        adapter.setOnItemClickListener(new RecruitmentAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (recruitDetail == null) {
                    recruitDetail = new Intent(RecruitmentActivity.this, RecruitmentDetailActivity.class);
                    recruitDetail.putExtra(KEY_ID_RECRUIT, mPresenter.getListRecruit().get(position));
                    startActivityForResult(recruitDetail, 33);
                }
            }

            @Override
            public void onDelete(final int position) {
                showConfirmDialog(getString(R.string.app_name), getString(R.string.do_you_delete, mPresenter.getListRecruit().get(position).getTitle()), new DialogPositiveNegative.IPositiveNegativeDialogListener() {
                    @Override
                    public void onIPositiveNegativeDialogAnswerPositive(DialogPositiveNegative dialog) {
                        mPresenter.deleteRecruitment(position);
                    }

                    @Override
                    public void onIPositiveNegativeDialogAnswerNegative(DialogPositiveNegative dialog) {
                        dialog.dismiss();
                    }
                });
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        recruitDetail = null;
        mPresenter.getListRecruitmentFromServer();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void getListRecruitmentSuccess() {
        adapter.notifyDataSetChanged();
        dismissDialog();
        refreshLayout.setRefreshing(false);

    }

    @Override
    public void getDataError(String error) {
        dismissDialog();
        showErrorDialog(error);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void dismissLoading() {
        dismissDialog();
    }
}