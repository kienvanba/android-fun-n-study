package com.horical.gito.mvp.caseStudy.detail.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.CaseStudy;
import com.horical.gito.model.TokenFile;
import com.horical.gito.mvp.caseStudy.detail.adapter.AttachFileAdapter;
import com.horical.gito.mvp.caseStudy.detail.injection.component.CaseStudyDetailComponent;
import com.horical.gito.mvp.caseStudy.detail.injection.component.DaggerCaseStudyDetailComponent;
import com.horical.gito.mvp.caseStudy.detail.injection.module.CaseStudyDetailModule;
import com.horical.gito.mvp.caseStudy.detail.presenter.CaseStudyDetailPresenter;
import com.horical.gito.mvp.dialog.dialogCaseStudy.DialogCaseStudy;
import com.horical.gito.mvp.dialog.dialogInputText.InputTextDialog;
import com.horical.gito.utils.FileUtils;
import com.horical.gito.utils.NetworkUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;

public class CaseStudyDetailActivity extends BaseActivity implements ICaseStudyDetailView, View.OnClickListener {
    public static final String ARG_CASESTUDY = "CaseStudy";
    private static final int REQUEST_READ_LIBRARY = 4;
    private static final int REQUEST_PERMISSION_READ_LIBRARY = 1;
    public static final int MODE_DETAIL = 1;
    public static final int MODE_ADD = 2;
    public static final int MODE_EDIT = 3;
    public int currentMode = MODE_DETAIL;

    public static final int NEW = 0;
    public static final int APPROVED = 1;
    public static final int REJECTED = 2;
    public static final int SUBMITED = 3;
    public static final int PUBLISH = 4;


    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.edt_title)
    EditText edtTitle;

    @Bind(R.id.edt_content)
    EditText edtContent;

    @Bind(R.id.ll_attach_file)
    LinearLayout llAttachFile;

    @Bind(R.id.rcv_attach_file)
    RecyclerView rcvAttachFile;

    @Bind(R.id.img_add_attach_file)
    ImageView imgAddAttachFile;

    @Bind(R.id.ll_reasons_for_rejected)
    LinearLayout llReasonsForRejected;

    @Bind(R.id.tv_reasons_for_rejected)
    TextView tvReasonsForRejected;

    @Bind(R.id.ll_bottom)
    LinearLayout llBottom;

    @Bind(R.id.tv_approved)
    TextView tvApproved;

    @Bind(R.id.tv_reject)
    TextView tvReject;

    @Bind(R.id.ll_submit)
    LinearLayout llSubmit;

    @Bind(R.id.tv_submit)
    TextView tvSubmit;

    private File mUploadFile;
    private CaseStudy caseStudy;
    private boolean isEdit = false;
    private AttachFileAdapter adapterAttachFile;
    private List<TokenFile> listTokenFiles = new ArrayList<>();
    private List<String> listNameFile = new ArrayList<>();

    @Inject
    CaseStudyDetailPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case_study_detail);
        ButterKnife.bind(this);
        CaseStudyDetailComponent component = DaggerCaseStudyDetailComponent.builder()
                .caseStudyDetailModule(new CaseStudyDetailModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        caseStudy = (CaseStudy) getIntent().getSerializableExtra(ARG_CASESTUDY);

        if (caseStudy != null) {
            currentMode = MODE_DETAIL;
        } else {
            currentMode = MODE_ADD;
        }

        initData();
        updateViewMode();
        initListener();
    }

    protected void initData() {
        adapterAttachFile = new AttachFileAdapter(this, listTokenFiles, listNameFile);
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvAttachFile.setLayoutManager(llm);
        rcvAttachFile.setAdapter(adapterAttachFile);

        if (caseStudy != null) {
            edtTitle.setText(caseStudy.getTitle());
            edtContent.setText(caseStudy.getDesc());
        } else {
            caseStudy = new CaseStudy();
            edtTitle.setText("");
            edtContent.setText("");

            llBottom.setVisibility(View.GONE);
            llSubmit.setVisibility(View.VISIBLE);
        }
    }

    private void updateViewMode() {
        switch (currentMode) {
            case MODE_DETAIL:
                topBar.setTextTitle(getString(R.string.edit_case_study));
                topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);

                edtTitle.setEnabled(false);
                edtContent.setEnabled(false);

                llReasonsForRejected.setVisibility(View.GONE);
                llBottom.setVisibility(View.GONE);
                llSubmit.setVisibility(View.GONE);
                llAttachFile.setVisibility(View.GONE);
                if (caseStudy.getStatus() == NEW) {
                    topBar.setImageViewRight(R.drawable.ic_edit_white);
                    llSubmit.setVisibility(View.VISIBLE);
                    llAttachFile.setVisibility(View.VISIBLE);
                    rcvAttachFile.setVisibility(View.VISIBLE);
                } else if (caseStudy.getStatus() == SUBMITED) {
                    tvApproved.setSelected(true);
                    llBottom.setVisibility(View.VISIBLE);
                } else if (caseStudy.getStatus() == REJECTED) {
                    llReasonsForRejected.setVisibility(View.VISIBLE);
                    tvReasonsForRejected.setText(caseStudy.getRejectMsg());
                }
                tvSubmit.setVisibility(View.VISIBLE);
                tvSubmit.setSelected(true);
                break;
            case MODE_ADD:
                topBar.setTextTitle(getString(R.string.new_case_study));
                topBar.setTextViewLeft(getString(R.string.cancel));
                topBar.setTextViewRight(getString(R.string.save));

                llAttachFile.setVisibility(View.GONE);
                rcvAttachFile.setVisibility(View.GONE);
                llReasonsForRejected.setVisibility(View.GONE);
                tvSubmit.setVisibility(View.GONE);
                break;
            case MODE_EDIT:
                topBar.setTextTitle(getString(R.string.edit_case_study));
                topBar.setTextViewLeft(getString(R.string.cancel));
                topBar.setTextViewRight(getString(R.string.save));
                break;
        }
    }

    protected void initListener() {
        tvReject.setOnClickListener(this);
        tvApproved.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        imgAddAttachFile.setOnClickListener(this);

        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                if (isEdit) {
                    setResult(RESULT_OK);
                }
                onBackPressed();
            }

            @Override
            public void onRightButtonOneClicked() {
                edtTitle.setEnabled(true);
                edtContent.setEnabled(true);

                currentMode = MODE_EDIT;
                updateViewMode();
            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });

        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                if (currentMode == MODE_EDIT) {
                    getDataCaseStudy();
                } else {
                    onBackPressed();
                }
            }

            @Override
            public void onRightClicked() {
                getDataCaseStudy();
            }
        });

        adapterAttachFile.setOnItemClickListener(new AttachFileAdapter.OnItemClickListener() {
            @Override
            public void onDelete(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CaseStudyDetailActivity.this);
                builder.setMessage(R.string.are_you_sure_to_delete_this_case_study);
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                showDialog("Loading...");
                                mPresenter.deleteAttachFile(caseStudy.get_id(), adapterAttachFile.getListTokenFile().get(position).getId());
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    public void getDataCaseStudy() {
        if (edtTitle.getText().toString().equalsIgnoreCase("") || edtContent.getText().toString().equalsIgnoreCase("")) {
            showErrorDialog("Casestudy validation failed");
            return;
        }
        if (currentMode == MODE_ADD) {
            caseStudy.setTitle(edtTitle.getText().toString());
            caseStudy.setDesc(edtContent.getText().toString());
            caseStudy.setStatus(NEW);

            showLoading();
            mPresenter.createCaseStudy(caseStudy);
        } else if (currentMode == MODE_EDIT) {
            caseStudy.setTitle(edtTitle.getText().toString());
            caseStudy.setDesc(edtContent.getText().toString());
            showLoading();
            mPresenter.updateCaseStudy(caseStudy);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_approved:
                showLoading();
                mPresenter.updateStatusCaseStudy(caseStudy.get_id(), APPROVED, null);
                break;
            case R.id.tv_reject:
                InputTextDialog dialogReject = new InputTextDialog(this, 0, new InputTextDialog.OnClickListener() {
                    @Override
                    public void onSave(String text) {
                        showLoading();
                        mPresenter.updateStatusCaseStudy(caseStudy.get_id(), REJECTED, text);
                    }
                });
                dialogReject.show();
                break;
            case R.id.tv_submit:
                caseStudy.setStatus(SUBMITED);
                showLoading();
                mPresenter.updateCaseStudy(caseStudy);
                break;
            case R.id.img_add_attach_file:
                final DialogCaseStudy dialogCaseStudy = new DialogCaseStudy(this);
                dialogCaseStudy.setOnClickItemListener(new DialogCaseStudy.OnClickItemListener() {
                    @Override
                    public void gallery() {
                        handleOpenLibrary();
                        dialogCaseStudy.dismiss();
                    }

                    @Override
                    public void uploadFile() {

                    }
                });
                dialogCaseStudy.show();
                break;
        }
    }


    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void createCaseStudySuccess(CaseStudy c) {
        hideLoading();
        showToast("Success");
        isEdit = true;
        caseStudy.set_id(c.get_id());
        caseStudy.setStatus(NEW);
        currentMode = MODE_DETAIL;
        updateViewMode();
    }

    @Override
    public void createCaseStudyFailure(String error) {
        hideLoading();
        showToast("Failure");
    }

    @Override
    public void updateCaseStudySuccess(int status) {
        hideLoading();
        showToast("Success");
        if (status == SUBMITED) {
            setResult(RESULT_OK);
            finish();
        } else {
            currentMode = MODE_DETAIL;
            updateViewMode();
        }
    }

    @Override
    public void updateCaseStudyFailure(String error) {
        hideLoading();
        showToast("Failure");
    }

    @Override
    public void updateStatusCaseStudySuccess(CaseStudy c) {
        hideLoading();
        showToast("Success");
        isEdit = true;
        caseStudy = c;
        currentMode = MODE_DETAIL;
        updateViewMode();
    }

    @Override
    public void updateStatusCaseStudyFailure(String error) {
        hideLoading();
        showToast("Failure");
    }

    @Override
    public void uploadFileSuccess(TokenFile tokenFile) {
        hideLoading();
        showToast("Success");
        listTokenFiles.add(tokenFile);
        InputTextDialog inputTextDialog = new InputTextDialog(this, 0, new InputTextDialog.OnClickListener() {
            @Override
            public void onSave(String text) {
                listNameFile.add(text);
            }
        });
        inputTextDialog.show();
        adapterAttachFile.notifyDataSetChanged();

        showLoading();
        mPresenter.addAttachFile(caseStudy.get_id(), tokenFile.getId());
    }

    @Override
    public void uploadFileFailure(String error) {
        hideLoading();
        showToast("Failure");
    }

    @Override
    public void updateAttachFileSuccess(CaseStudy caseStudy) {
        hideLoading();
        showToast("Success");
    }

    @Override
    public void updateAttachFileFailure(String error) {
        hideLoading();
        showToast("Failure");
    }

    @Override
    public void deleteAttachFileSuccess() {
        hideLoading();
        showToast("Success");
    }

    @Override
    public void deleteAttachFileFailure(String error) {
        hideLoading();
        showToast("Failure");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    private void handleOpenLibrary() {
        String s[] = {android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(s)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, REQUEST_READ_LIBRARY);
        } else {
            ActivityCompat.requestPermissions(this, s, REQUEST_PERMISSION_READ_LIBRARY);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_READ_LIBRARY) {
            handleOpenLibrary();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_READ_LIBRARY) {
                System.out.println("File from library " + data.getData());
                if (!NetworkUtils.isConnected(this)) {
                    showNoNetworkErrorDialog();
                } else {
                    mUploadFile = FileUtils.convertUriToFile(this, data.getData());
                    mPresenter.uploadFile(
                            MediaType.parse(getContentResolver().getType(data.getData())),
                            mUploadFile);
                }
            }
        }
    }
}
