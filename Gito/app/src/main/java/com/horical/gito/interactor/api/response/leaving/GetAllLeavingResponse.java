package com.horical.gito.interactor.api.response.leaving;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Leaving;
import com.horical.gito.model.LeavingApprover;

import java.util.List;

/**
 * Created by Dragonoid on 12/30/2016
 */

public class GetAllLeavingResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Leaving> leavings;
}
