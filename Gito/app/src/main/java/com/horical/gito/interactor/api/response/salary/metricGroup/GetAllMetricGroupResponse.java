package com.horical.gito.interactor.api.response.salary.metricGroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.SalaryMetricGroup;

import java.util.ArrayList;
import java.util.List;

public class GetAllMetricGroupResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    private List<SalaryMetricGroup> metricGroups;

    public List<SalaryMetricGroup> getMetricGroups() {
        if (metricGroups == null) {
            metricGroups = new ArrayList<>();
        }
        return metricGroups;
    }

    public void setMetricGroups(List<SalaryMetricGroup> metricGroups) {
        this.metricGroups = metricGroups;
    }
}
