package com.horical.gito.mvp.myProject.setting.fragment.activity.member.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RoleProjectHolder extends RecyclerView.ViewHolder{
    @Bind(R.id.cb_role)
    CheckBox cbRole;
    @Bind(R.id.tv_role_name)
    GitOTextView tvName;
    public RoleProjectHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
