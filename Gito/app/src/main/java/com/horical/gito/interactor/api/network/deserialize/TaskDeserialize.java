package com.horical.gito.interactor.api.network.deserialize;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.horical.gito.model.Task;
import com.horical.gito.utils.GsonUtils;

import java.lang.reflect.Type;

public class TaskDeserialize implements JsonDeserializer<Task> {

    @Override
    public Task deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        Task task;
        try {
            task = GsonUtils.createGson(Task.class).fromJson(json, Task.class);
        } catch (Exception ex) {
            task = new Task();
            task.set_id(json.getAsString());
        }
        return task;
    }
}
