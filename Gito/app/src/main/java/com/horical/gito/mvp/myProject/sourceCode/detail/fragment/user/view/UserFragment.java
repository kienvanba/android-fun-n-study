package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.BaseSourceCodeFragment;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.adapter.UserAdapter;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.injection.component.DaggerUserComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.injection.component.UserComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.injection.module.UserModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.presenter.UserPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UserFragment extends BaseSourceCodeFragment implements IUserView, View.OnClickListener {

    @Bind(R.id.code_fragment_user)
    RecyclerView rcvUser;

    private UserAdapter adapterUser;

    @Inject
    UserPresenter mPresenter;

    public static UserFragment newInstance() {
        Bundle args = new Bundle();
        UserFragment fragment = new UserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_code_user;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        UserComponent component = DaggerUserComponent.builder()
                .userModule(new UserModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }


    @Override
    protected void initData() {
//        adapterUser = new UserGroupAdapter(getContext(), mPresenter.getListUser());
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
//        rcvUser.setLayoutManager(layoutManager);
//        rcvUser.setHasFixedSize(false);
//        rcvUser.setAdapter(adapterUser);
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View view) {

    }
}
