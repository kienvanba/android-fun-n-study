package com.horical.gito.mvp.login.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.mvp.intro.view.IntroActivity;
import com.horical.gito.mvp.login.injection.component.DaggerLoginComponent;
import com.horical.gito.mvp.login.injection.component.LoginComponent;
import com.horical.gito.mvp.login.injection.module.LoginModule;
import com.horical.gito.mvp.login.presenter.LoginPresenter;
import com.horical.gito.mvp.myPage.view.MyPageActivity;
import com.horical.gito.mvp.signUp.view.SignUpActivity;
import com.horical.gito.utils.DeviceUtils;
import com.horical.gito.widget.button.GitOButton;
import com.horical.gito.widget.edittext.GitOEditText;
import com.horical.gito.widget.textview.GitOTextView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class LoginActivity extends BaseActivity implements ILoginView, View.OnClickListener {

    private static final String TAG = makeLogTag(LoginActivity.class);

    @Bind(R.id.login_btn_back)
    ImageView mBtnBack;

    @Bind(R.id.login_btn_login)
    GitOButton mBtnLogin;

    @Bind(R.id.login_edt_email)
    GitOEditText mEdtEmail;

    @Bind(R.id.login_edt_password)
    GitOEditText mEdtPassword;

    @Bind(R.id.login_tv_sign_up_here)
    GitOTextView mTvSignUpHere;

    @Inject
    LoginPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        Window window = getWindow();
        window.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.transparent));
        }

        LoginComponent component = DaggerLoginComponent.builder()
                .loginModule(new LoginModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        // hard code to test
        mBtnLogin.setBlueStyle();

        Spannable spannable = new SpannableString(getString(R.string.sign_up_here));
        spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#009DF7")),
                27,
                getString(R.string.sign_up_here).length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTvSignUpHere.setText(spannable);

        mEdtEmail.setText("ngoc@gmail.com");
        mEdtPassword.setText("Aa123456?1");
    }

    protected void initListener() {
        mBtnLogin.setOnClickListener(this);
        mBtnBack.setOnClickListener(this);
        mTvSignUpHere.setOnClickListener(this);

        mEdtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE ||
                        (event != null && event.getAction() == KeyEvent.ACTION_DOWN &&
                                event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    mPresenter.login(
                            mEdtEmail.getText().toString(),
                            mEdtPassword.getText().toString(),
                            DeviceUtils.getDeviceName(),
                            DeviceUtils.getDeviceName(),
                            DeviceUtils.getOsName(),
                            DeviceUtils.getOsVersion(),
                            DeviceUtils.getDeviceUuid(LoginActivity.this)
                    );
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mBtnBack.getId()) {
            Intent intent = new Intent(LoginActivity.this, IntroActivity.class);
            startActivity(intent);
            finish();
        } else if (v.getId() == mBtnLogin.getId()) {
            mPresenter.login(
                    mEdtEmail.getText().toString(),
                    mEdtPassword.getText().toString(),
                    DeviceUtils.getDeviceName(),
                    DeviceUtils.getDeviceName(),
                    DeviceUtils.getOsName(),
                    DeviceUtils.getOsVersion(),
                    DeviceUtils.getDeviceUuid(this)
            );
        } else if (v.getId() == mTvSignUpHere.getId()) {
            Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void loginError(RestError error) {
        showRestErrorDialog(error);
    }

    @Override
    public void loginSuccess() {
        Intent intent = new Intent(LoginActivity.this, MyPageActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void errorEmailInvalid() {
        showErrorDialog(getString(R.string.email_invalid));
    }

    @Override
    public void errorEmptyInput() {
        showErrorDialog(getString(R.string.empty_input));
    }
}
