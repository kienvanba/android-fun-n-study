package com.horical.gito.mvp.survey.list.view;

import com.horical.gito.base.IView;
import com.horical.gito.model.Survey;

import java.util.List;

/**
 * Created by thanhle on 11/9/16.
 */

public interface ISurveyView extends IView {

    void showLoading();

    void hideLoading();

    void getAllSurveySuccess();

    void getAllSurveyFailure(String error);
}