package com.horical.gito.mvp.report.selectUserGroup.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.report.GroupResponse;
import com.horical.gito.interactor.api.response.user.GetAllUserResponse;
import com.horical.gito.model.Group;
import com.horical.gito.model.User;
import com.horical.gito.mvp.report.selectUserGroup.view.IUserGroupView;

import java.util.ArrayList;
import java.util.List;

public class UserGroupPresenter extends BasePresenter implements IUserGroupPresenter {

    private List<Object> mList = new ArrayList<>();
    private List<Object> mListSearch = new ArrayList<>();

    private IUserGroupView getView() {
        return (IUserGroupView) getIView();
    }

    public List<Object> getListSearch() {
        return mListSearch;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public void setCheckAll(boolean check) {

        for (int i = 0; i < mListSearch.size(); i++) {
            if (mListSearch.get(i) instanceof User) {
                ((User) mListSearch.get(i)).setSelected(check);
            } else if (mListSearch.get(i) instanceof Group) {
                ((Group) mListSearch.get(i)).setSelected(check);
            }
        }
    }

    public boolean isCheckedAll() {

        for (int i = 0; i < mListSearch.size(); i++) {
            if (mListSearch.get(i) instanceof User) {
                if (!((User) mListSearch.get(i)).isSelected()) {
                    return false;
                }
            } else if (mListSearch.get(i) instanceof Group) {
                if (!((Group) mListSearch.get(i)).isSelected()) {
                    return false;
                }
            }
        }
        return true;
    }

    public List<Object> getListSave() {

        List<Object> list = new ArrayList<>();
        for (int i = 0; i < mListSearch.size(); i++) {
            if (mListSearch.get(i) instanceof User) {
                if (((User) mListSearch.get(i)).isSelected()) {
                    list.add(mListSearch.get(i));
                }
            } else if (mListSearch.get(i) instanceof Group) {
                if (((Group) mListSearch.get(i)).isSelected()) {
                    list.add(mListSearch.get(i));
                }
            }
        }
        return list;
    }

    @Override
    public void getListReportToUser() {

        getView().showLoading();

        getApiManager().getAllUserReport(new ApiCallback<GetAllUserResponse>() {
            @Override
            public void success(GetAllUserResponse res) {
                for (int i = 0; i < res.getUsers().size(); i++) {
                    if (res.getUsers().get(i).get_id().equalsIgnoreCase(getPreferManager().getUser().get_id())) {
                        res.getUsers().remove(i);
                        break;
                    }
                }

                mList.clear();
                mList.addAll(res.getUsers());
                getView().onSuccessList();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                getView().onFailList(error.message);
                getView().hideLoading();
            }
        });
    }

    @Override
    public void getListInformToUser() {
        getView().showLoading();
        getApiManager().getAllUser(new ApiCallback<GetAllUserResponse>() {
            @Override
            public void success(GetAllUserResponse res) {
                for (int i = 0; i < res.getUsers().size(); i++) {
                    if (res.getUsers().get(i).get_id().equalsIgnoreCase(getPreferManager().getUser().get_id())) {
                        res.getUsers().remove(i);
                        break;
                    }
                }
                mList.clear();
                mList.addAll(res.getUsers());
                getView().onSuccessList();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                getView().onFailList(error.message);
                getView().hideLoading();
            }
        });
    }

    @Override
    public void getListGroup() {
        getView().showLoading();
        String id = "";
        if (GitOStorage.getInstance().getProject() != null) {
            id = GitOStorage.getInstance().getProject().getId();
        } else {
            getView().onFailList("You need pick a Project");
            getView().hideLoading();
            return;
        }
        getApiManager().getAllGroupReport(id, new ApiCallback<GroupResponse>() {
            @Override
            public void success(GroupResponse res) {
                mList.clear();
                mList.addAll(res.getList());
                getView().onSuccessList();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                getView().onFailList(error.message);
                getView().hideLoading();
            }
        });
    }

    @Override
    public List<Object> getListSearchUser(String keyword) {
        mListSearch.clear();
        if (keyword != null && keyword.trim().length() > 0) {

            for (int i = 0; i < mList.size(); i++) {
                if (mList.get(i) instanceof User) {
                    if (((User) mList.get(i)).getDisplayName() != null
                            && ((User) mList.get(i)).getDisplayName().toLowerCase().contains(keyword.toLowerCase())) {
                        mListSearch.add(mList.get(i));
                    }
                } else if (mList.get(i) instanceof Group) {
                    if (((Group) mList.get(i)).getGroupName() != null
                            && ((Group) mList.get(i)).getGroupName().toLowerCase().contains(keyword.toLowerCase())) {
                        mListSearch.add(mList.get(i));
                    }
                }
            }


        } else {
            mListSearch.addAll(mList);
        }
        return mListSearch;
    }

    public int sizeSelected() {
        int data = 0;
        for (int i = 0; i < mListSearch.size(); i++) {
            if (mListSearch.get(i) instanceof User) {
                if (((User) mListSearch.get(i)).isSelected()) {
                    data = data + 1;
                }
            } else if (mListSearch.get(i) instanceof Group) {
                if (((Group) mListSearch.get(i)).isSelected()) {
                    data = data + 1;
                }
            }
        }
        return data;
    }

}
