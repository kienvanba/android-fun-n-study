package com.horical.gito.mvp.myProject.task.details.fragment.subtask.injection.component;


import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.subtask.injection.module.SubTaskModule;
import com.horical.gito.mvp.myProject.task.details.fragment.subtask.view.SubTaskFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = SubTaskModule.class)
public interface SubTaskComponent {
    void inject(SubTaskFragment fragment);
}
