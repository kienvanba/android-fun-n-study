package com.horical.gito.mvp.myProject.setting.fragment.recycler.presenter;

import com.horical.gito.R;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.component.CreateComponentRequest;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllProjectRoleResponse;
import com.horical.gito.interactor.api.response.component.CreateComponentResponse;
import com.horical.gito.interactor.api.response.component.GetAllComponentResponse;
import com.horical.gito.interactor.api.response.project.GetMemberAndRoleResponse;
import com.horical.gito.interactor.api.response.version.GetAllVersionResponse;
import com.horical.gito.interactor.api.response.project.GetProjectResponse;
import com.horical.gito.model.Component;
import com.horical.gito.model.MemberAndRole;
import com.horical.gito.model.Project;
import com.horical.gito.model.RoleProject;
import com.horical.gito.model.Version;
import com.horical.gito.mvp.myProject.setting.adapter.ComponentItemBody;
import com.horical.gito.mvp.myProject.setting.adapter.FeatureItemBody;
import com.horical.gito.mvp.myProject.setting.adapter.MemberItemBody;
import com.horical.gito.mvp.myProject.setting.adapter.VersionItemBody;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.view.IRecyclerFragment;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.LOGE;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class RecyclerPresenter extends BasePresenter implements IRecyclerPresenter {
    private static final String TAG = makeLogTag(RecyclerPresenter.class);

    private final String projectId = GitOStorage.getInstance().getProject().getId();

    private List<IRecyclerItem> mItems;
    private List<RoleProject> mRoles;

    public IRecyclerFragment getView() {
        return (IRecyclerFragment) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mItems = new ArrayList<>();
        mRoles = new ArrayList<>();

        getEventManager().register(this);
    }

    @Override
    public List<IRecyclerItem> getRecyclerItems() {
        return mItems;
    }
    public List<RoleProject> getRoles(){
        return mRoles;
    }

    private void setFeatureItem(Project mProject) {
        if (mProject.getFeature().hfSummary)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_summary,
                    "Summary"));
        if (mProject.getFeature().hfTask)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_task,
                    "Task"));
        if (mProject.getFeature().hfGantt)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_gantt,
                    "Gantt"));
        if (mProject.getFeature().hfCalendar)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_calendar,
                    "Calendar"));
        if (mProject.getFeature().hfDocument)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_document,
                    "Document"));
        if (mProject.getFeature().hfCode)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_source_code,
                    "Source Code"));
        if (mProject.getFeature().hfTestCase)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_test_case,
                    "Test Case"));
        if (mProject.getFeature().hfNote)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_note,
                    "Note & Todo"));
        if (mProject.getFeature().hfMeeting)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_meeting_room,
                    "Meeting"));
        if (mProject.getFeature().hfReport)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_report,
                    "Report"));
        if (mProject.getFeature().hfChangeLog) mItems.add(new FeatureItemBody(
                R.drawable.ic_change_log,
                "Change Log"));
        if (mProject.getFeature().hfChat)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_chat,
                    "Chat"));
        if (mProject.getFeature().hfEstate)
            mItems.add(new FeatureItemBody(
                    R.drawable.ic_estate,
                    "Estate"));
    }

    public void getAllFeatures() {
        if (!isViewAttached()) return;
        getView().showLoading();
        mItems.clear();

        getApiManager().getProject(projectId, new ApiCallback<GetProjectResponse>() {
            @Override
            public void success(GetProjectResponse res) {
                if (res.getProjectResult().getProject() != null) {
                    Project project = res.getProjectResult().getProject();
                    setFeatureItem(project);
                }
                getView().getAllRecyclerSuccess();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    public void getAllVersions() {
        if (!isViewAttached()) return;
        getView().showLoading();

        mItems.clear();
        getApiManager().getAllVersion(projectId, new ApiCallback<GetAllVersionResponse>() {
            @Override
            public void success(GetAllVersionResponse res) {
                if (!res.versions.isEmpty()) {
                    for (Version version : res.versions) {
                        VersionItemBody body = new VersionItemBody(version);
                        mItems.add(body);
                    }
                }
                getView().getAllRecyclerSuccess();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    public void deleteVersion(final int position) {
        if (!isViewAttached()) return;
        getView().showLoading();

        String versionId = ((VersionItemBody) mItems.get(position)).version.get_id();

        getApiManager().deleteVersion(projectId, versionId, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                if (res.success) getView().deleteItemSuccess(position);
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    public void getMemberAndRole() {
        if (!isViewAttached()) return;
        getView().showLoading();

        mItems.clear();
        getApiManager().getMemberAndRole(projectId, new ApiCallback<GetMemberAndRoleResponse>() {
            @Override
            public void success(GetMemberAndRoleResponse res) {
                if (!res.memberAndRoles.isEmpty()) {
                    for (MemberAndRole mar : res.memberAndRoles) {
                        mItems.add(new MemberItemBody(mar));
                    }
                }
                getView().getAllRecyclerSuccess();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    public void getAllProjectRole(){
        if(!isViewAttached()) return;
        getView().showLoading();

        mRoles.clear();
        getApiManager().getAllProjectRole(new ApiCallback<GetAllProjectRoleResponse>() {
            @Override
            public void success(GetAllProjectRoleResponse res) {
                if(res.success && res.roleProjects!=null){
                    mRoles.addAll(res.roleProjects);
                }
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    public void getAllComponent() {
        if (!isViewAttached()) return;
        getView().showLoading();

        mItems.clear();
        getApiManager().getAllComponent(projectId, new ApiCallback<GetAllComponentResponse>() {
            @Override
            public void success(GetAllComponentResponse res) {
                if (!res.components.isEmpty()) {
                    for (Component component : res.components) {
                        ComponentItemBody body = new ComponentItemBody(component);
                        mItems.add(body);
                    }
                }
                getView().getAllRecyclerSuccess();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    public void deleteComponent(final int position) {
        if (!isViewAttached()) return;
        getView().showLoading();

        String componentId = ((ComponentItemBody) mItems.get(position)).component._id;

        getApiManager().deleteComponent(projectId, componentId, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                if (res.success) getView().deleteItemSuccess(position);
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    public void createComponent(final CreateComponentRequest request) {
        if (!isViewAttached()) return;
        getView().showLoading();

        getApiManager().createComponent(projectId, request, new ApiCallback<CreateComponentResponse>() {
            @Override
            public void success(CreateComponentResponse res) {
                if (res.success && res.component != null) {
                    mItems.add(new ComponentItemBody(res.component));
                    getView().getAllRecyclerSuccess();
                    getView().hideLoading();
                } else {
                    getView().hideLoading();
                    getView().showToast(res.message);
                }
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
