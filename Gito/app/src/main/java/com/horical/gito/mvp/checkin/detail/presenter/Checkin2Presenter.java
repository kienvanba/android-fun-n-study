package com.horical.gito.mvp.checkin.detail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.checkin.GetAllCheckinResponse;
import com.horical.gito.model.CheckIn;
import com.horical.gito.mvp.checkin.detail.adapter.DayofWeek;
import com.horical.gito.mvp.checkin.detail.view.ICheckin2View;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Penguin on 23-Nov-16.
 */

public class Checkin2Presenter extends BasePresenter implements ICheckin2Presenter {
    private List<IRecyclerItem> mCheckin;

    private static final String TAG = makeLogTag(Checkin2Presenter.class);

    public ICheckin2View getView() {
        return (ICheckin2View) getIView();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mCheckin = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<IRecyclerItem> getCheckin() {
        return mCheckin;
    }

    public void setCheckin(List<IRecyclerItem> checkin) {
        this.mCheckin = checkin;
    }

    public void getAllCheckin() {
        getView().showLoading();
        getApiManager().getAllCheckin(new ApiCallback<GetAllCheckinResponse>() {
            @Override
            public void success(GetAllCheckinResponse res) {
                if (!isViewAttached()) return;
                getView().hideLoading();
                if (res == null) return;
                mCheckin.clear();
                if (!res.checkIn.isEmpty()) {
                    for (CheckIn checkIn : res.checkIn) {
                        DayofWeek dow = new DayofWeek(checkIn);
                        mCheckin.add(dow);
                    }
                }

                getView().getAllCheckinSuccess();
            }

            @Override
            public void failure(RestError error) {
                if (!isViewAttached()) return;
                getView().hideLoading();
                getView().getAllCheckinFailure(error);
            }
        });
    }

    @Override
    public CheckIn getCheckin(int position) {
        DayofWeek dow = (DayofWeek) mCheckin.get(position);
        return dow.getCheckin();
    }

}


