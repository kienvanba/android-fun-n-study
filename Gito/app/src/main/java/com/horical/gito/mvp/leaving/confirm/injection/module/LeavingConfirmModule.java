package com.horical.gito.mvp.leaving.confirm.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.leaving.confirm.presenter.LeavingConfirmPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Dragonoid on 1/17/2017.
 */
@Module
public class LeavingConfirmModule {
    @PerActivity
    @Provides
    LeavingConfirmPresenter provideLeavingConfirmPresenter(){
        return new LeavingConfirmPresenter();
    }
}
