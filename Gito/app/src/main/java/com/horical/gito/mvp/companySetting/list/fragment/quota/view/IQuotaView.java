package com.horical.gito.mvp.companySetting.list.fragment.quota.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IQuotaView extends IView {
    void showLoading();

    void hideLoading();

    void getInfoQuotaSuccess();

    void getInfoQuotaFailure(String err);

    void getSummaryQuotaSuccess();

    void getSummaryQuotaFailure(String err);
}
