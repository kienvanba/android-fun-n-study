package com.horical.gito.mvp.roomBooking.detail.fragment.week.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.utils.ConvertUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Lemon on 3/31/2017.
 */

public class DetailWeekAdapter extends BaseRecyclerAdapter {

    List<IRecyclerItem> weekDetailDto;

    public DetailWeekAdapter(List<IRecyclerItem> items, Context context) {
        super(items, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(mContext).inflate(R.layout.item_roombooking_week_detail, parent, false);
        return new WeekDetailHolder(item);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        WeekDetailItem detailItem = (WeekDetailItem) mItems.get(holder.getAdapterPosition());
        WeekDetailHolder detailHolder = (WeekDetailHolder) holder;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ConvertUtils.convertDpToInt(mContext, 20), ViewGroup.LayoutParams.MATCH_PARENT);
        detailHolder.mWhiteViewWeekDetail.setLayoutParams(params);

        detailHolder.mViewWeekDetail.getLayoutParams().width = detailItem.lengh;

//        weekDetailDto = new ArrayList<>();
//        for (int i = 0; i < 7; i++) {
//            WeekDetailDto detailDto = new WeekDetailDto();
//            detailDto.weekDetailDto = "button " + i;
//            WeekDetailItem item = new WeekDetailItem(detailDto);
//            item.lengh = ConvertUtils.convertDpToInt(mContext, 7 ) * 30;
//            weekDetailDto.add(item);
//        }
    }

    public class WeekDetailHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.view_week_white)
        View mWhiteViewWeekDetail;

        @Bind(R.id.view_week_detail)
        View mViewWeekDetail;

        public WeekDetailHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


    }
}