package com.horical.gito.mvp.companySetting.detail.newMember.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.RoleCompany;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by kut3b on 11/04/2017.
 */

public class MemberCompanyRoleAdapter extends RecyclerView.Adapter<MemberCompanyRoleAdapter.CompanyRoleHolder> {
    private Context mContext;
    private List<RoleCompany> mList;

    public MemberCompanyRoleAdapter(Context context, List<RoleCompany> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public CompanyRoleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_member, parent, false);
        return new CompanyRoleHolder(view);
    }

    @Override
    public void onBindViewHolder(final CompanyRoleHolder holder, final int position) {
        final RoleCompany roleCompany = mList.get(position);
        holder.tvName.setText(roleCompany.getRoleName());
        holder.chkCheck.setChecked(roleCompany.isSelected());

        holder.chkCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                roleCompany.setSelected(!roleCompany.isSelected());
                notifyItemChanged(position);
                mOnItemClickListener.onItemClickSelected(roleCompany.getId());
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                roleCompany.setSelected(!roleCompany.isSelected());
                notifyItemChanged(position);
                mOnItemClickListener.onItemClickSelected(roleCompany.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class CompanyRoleHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.chk_check)
        CheckBox chkCheck;

        @Bind(R.id.tv_name)
        TextView tvName;

        View view;

        public CompanyRoleHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(String id);
    }
}