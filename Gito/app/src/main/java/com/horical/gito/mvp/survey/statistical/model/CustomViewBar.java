package com.horical.gito.mvp.survey.statistical.model;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by nhattruong251295 on 4/8/2017.
 */

public class CustomViewBar extends View {

    final int MIN_WIDTH = 100;

    final int MIN_HEIGHT = 50;

    int DEFAULT_COLOR = Color.RED;

    int color;

    final int STROKE_WIDTH = 2;

    public CustomViewBar(Context context) {
        super(context);
        init();
    }

    public CustomViewBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomViewBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){
        setMinimumWidth(MIN_WIDTH);
        setMinimumHeight(MIN_HEIGHT);
        color = DEFAULT_COLOR;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(getSuggestedMinimumWidth(), getSuggestedMinimumHeight());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setStrokeWidth(STROKE_WIDTH);
        canvas.drawRect(0, 0, getWidth(), getHeight(), paint);
    }

    public void setColor(int color){
        this.color = color;
    }
}
