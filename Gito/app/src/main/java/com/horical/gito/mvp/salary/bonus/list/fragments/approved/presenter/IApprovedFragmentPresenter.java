package com.horical.gito.mvp.salary.bonus.list.fragments.approved.presenter;

import com.horical.gito.model.InputAddon;

import java.util.List;

public interface IApprovedFragmentPresenter {
    List<InputAddon> getListInputAddon();

    void requestGetApprovedInputAddon();
}
