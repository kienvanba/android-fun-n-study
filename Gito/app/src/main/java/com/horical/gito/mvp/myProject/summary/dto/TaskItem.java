package com.horical.gito.mvp.myProject.summary.dto;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.Issue;
import com.horical.gito.mvp.myProject.summary.adapter.SummaryAdapter;

/**
 * Created by nhonvt.dk on 3/21/17
 */

public class TaskItem implements IRecyclerItem {

    private Issue issue;

    public TaskItem(Issue issue) {
        this.issue = issue;
    }

    public Issue getIssue() {
        return issue;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    @Override
    public int getItemViewType() {
        return SummaryAdapter.SUMMARY_TASK_TYPE;
    }
}
