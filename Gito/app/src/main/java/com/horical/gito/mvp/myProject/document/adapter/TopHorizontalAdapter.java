package com.horical.gito.mvp.myProject.document.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.AppConstants;
import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.mvp.myProject.setting.adapter.TabHolder;
import com.horical.gito.mvp.myProject.setting.adapter.TabItemBody;

import java.util.List;

public class TopHorizontalAdapter extends BaseRecyclerAdapter {
    private int currentPosition=0;
    private int currentViewWidth=0;

    public TopHorizontalAdapter(Context context, List<IRecyclerItem> items) {
        super(items, context);
    }

    public void setCurrentPosition(int position){
        this.currentPosition = position;
    }
    public int getCurrentViewWidth(){
        return currentViewWidth;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case AppConstants.PATH_ITEM:
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_path,parent,false);
                return new PathHolder(view);
            case AppConstants.TAB_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_tab,parent,false);
                return new TabHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder,position);
        switch (getItemViewType(position)){
            case AppConstants.PATH_ITEM:
                PathItemBody mPathBody = (PathItemBody) mItems.get(position);
                PathHolder mPathHolder = (PathHolder) holder;
                mPathHolder.mImSlash.setColorFilter(mContext.getResources().getColor(R.color.black));
                mPathHolder.mTvPath.setText(mPathBody.path.length()>=25?mPathBody.path.substring(0,22)+"...":mPathBody.path);
                if(mPathBody.path.equals("Home")){
                    mPathHolder.mImSlash.setVisibility(View.GONE);
                }else{
                    mPathHolder.mImSlash.setVisibility(View.VISIBLE);
                }
                if(position == currentPosition){
                    mPathHolder.mTvPath.setTextColor(mContext.getResources().getColor(R.color.blue));
                }else{
                    mPathHolder.mTvPath.setTextColor(mContext.getResources().getColor(R.color.black));
                }
                break;
            case AppConstants.TAB_ITEM:
                TabItemBody mTabBody = (TabItemBody) mItems.get(position);
                TabHolder mTabHolder = (TabHolder) holder;
                mTabHolder.mTvTabName.setText(mTabBody.getTabName());
                if(position == currentPosition){
                    mTabHolder.mTabContainer.setBackground(mContext.getResources().getDrawable(R.drawable.bg_tab_selected));
                    mTabHolder.mTvTabName.setTextColor(mContext.getResources().getColor(R.color.white));
                    currentViewWidth = holder.itemView.getWidth();
                }else{
                    mTabHolder.mTabContainer.setBackground(mContext.getResources().getDrawable(R.color.transparent));
                    mTabHolder.mTvTabName.setTextColor(mContext.getResources().getColor(R.color.blue));
                }
                break;
        }
    }
}
