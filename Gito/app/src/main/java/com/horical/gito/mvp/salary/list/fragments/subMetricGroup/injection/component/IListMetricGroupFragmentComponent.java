package com.horical.gito.mvp.salary.list.fragments.subMetricGroup.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.list.fragments.subMetricGroup.injection.module.ListMetricGroupFragmentModule;
import com.horical.gito.mvp.salary.list.fragments.subMetricGroup.view.ListMetricGroupFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ListMetricGroupFragmentModule.class)
public interface IListMetricGroupFragmentComponent {
    void inject(ListMetricGroupFragment activity);
}
