package com.horical.gito.mvp.myProject.setting.fragment.activity.member.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ProgressBar;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserHolder extends RecyclerView.ViewHolder{
    @Bind(R.id.cb_user)
    CheckBox cbUser;
    @Bind(R.id.task_imv_avatar)
    CircleImageView imgAvatar;
    @Bind(R.id.task_pgr_avt_loading)
    ProgressBar prgAvatar;
    @Bind(R.id.task_tv_sort_name)
    GitOTextView tvNonAvatar;
    @Bind(R.id.task_tv_name)
    GitOTextView tvName;
    @Bind(R.id.task_tv_role)
    GitOTextView tvLevel;
    public UserHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
