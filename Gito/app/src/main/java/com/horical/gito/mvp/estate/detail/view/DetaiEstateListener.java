package com.horical.gito.mvp.estate.detail.view;

import com.horical.gito.base.IView;
import com.horical.gito.model.estate.Estate;

import java.util.Date;

/**
 * Created by hoangsang on 11/8/16.
 */

public interface DetaiEstateListener extends IView {

    void enableCurrentView(int editTextId, int imageViewId);
    void disableCurrentView(int editTextId, int imageViewId);

    interface DialogPickerCallBack{
        void success(Date date);
        void failure();
    }
    void showDialogPicker(Date currentDate, DialogPickerCallBack callBack);

    Estate getEstate();
}
