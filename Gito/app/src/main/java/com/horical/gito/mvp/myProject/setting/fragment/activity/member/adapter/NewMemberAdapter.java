package com.horical.gito.mvp.myProject.setting.fragment.activity.member.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.AppConstants;
import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.utils.CommonUtils;

import java.util.List;

public class NewMemberAdapter extends BaseRecyclerAdapter{
    private int currentRolePosition=0;
    public NewMemberAdapter(List<IRecyclerItem> items, Context context) {
        super(items, context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case AppConstants.USER_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_user, parent, false);
                return new UserHolder(view);
            case AppConstants.ROLE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_pick_role, parent, false);
                return new RoleProjectHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        switch (mItems.get(position).getItemViewType()){
            case AppConstants.USER_ITEM:
                UserItemBody userBody = (UserItemBody) mItems.get(position);
                UserHolder userHolder = (UserHolder) holder;

                userHolder.cbUser.setChecked(userBody.isChecked());
                if(userBody.getUser().getAvatar()==null){
                    userHolder.tvNonAvatar.setVisibility(View.VISIBLE);
                    userHolder.tvNonAvatar.setText(userBody.getUser().getDisplayName().substring(0,1).toUpperCase());
                    userHolder.prgAvatar.setVisibility(View.GONE);
                }else{
                    userHolder.tvNonAvatar.setVisibility(View.GONE);
                    userHolder.prgAvatar.setVisibility(View.VISIBLE);
                    String url = CommonUtils.getURL(userBody.getUser().getAvatar());
                    ImageLoader.load(mContext, url, userHolder.imgAvatar, userHolder.prgAvatar);
                }
                userHolder.tvName.setText(userBody.getUser().getDisplayName());
                userHolder.tvLevel.setText(userBody.getUser().getLevel().getName());
                break;
            case AppConstants.ROLE_ITEM:
                RoleProjectItemBody roleBody = (RoleProjectItemBody) mItems.get(position);
                RoleProjectHolder roleHolder = (RoleProjectHolder) holder;

                if(position==currentRolePosition){
                    roleHolder.cbRole.setChecked(true);
                }else{
                    roleHolder.cbRole.setChecked(false);
                }
                roleHolder.tvName.setText(roleBody.getRoleProject().getRoleName());
                break;
        }
    }

    public void setCurrentRolePosition(int  position){
        this.currentRolePosition = position;
    }
    public int getCurrentRolePosition(){
        return currentRolePosition;
    }
}
