package com.horical.gito.mvp.myProject.summary.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.summary.injection.module.SummaryModule;
import com.horical.gito.mvp.myProject.summary.view.SummaryActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = SummaryModule.class)
public interface SummaryComponent {
    void inject(SummaryActivity activity);
}
