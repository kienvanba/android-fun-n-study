package com.horical.gito.mvp.estate.detail.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.horical.gito.mvp.estate.detail.view.fragment.DetailEstateFragmentOne;
import com.horical.gito.mvp.estate.detail.view.fragment.DetailEstateFragmentTwo;

/**
 * Created by hoangsang on 11/9/16.
 */

public class FragmentPaggerAdapter extends FragmentPagerAdapter {

    public FragmentPaggerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return DetailEstateFragmentOne.newInstance();
            case 1:
                return DetailEstateFragmentTwo.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
