package com.horical.gito.mvp.myProject.document.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.Document;

public class DocumentItemBody implements IRecyclerItem {
    public Document document;

    public DocumentItemBody(Document document) {
        this.document  = document;
    }

    @Override
    public int getItemViewType() {
        return AppConstants.DOCUMENT_ITEM;
    }
}
