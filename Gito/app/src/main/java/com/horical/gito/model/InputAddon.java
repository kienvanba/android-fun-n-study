package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InputAddon implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("applyFrom")
    @Expose
    private Date applyFrom;

    @SerializedName("applyTo")
    @Expose
    private Date applyTo;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("acronym")
    @Expose
    private String acronym;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("creatorId")
    @Expose
    private User creatorId;

    @SerializedName("total")
    @Expose
    private int total;

    @SerializedName("status")
    @Expose
    private int status;

    //    @SerializedName("listUser")
    @Expose(serialize = false, deserialize = false)
    private List<InputAddonUser> listUser;

    @SerializedName("createdDate")
    @Expose
    private Date createdDate;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Date getApplyFrom() {
        return applyFrom;
    }

    public void setApplyFrom(Date applyFrom) {
        this.applyFrom = applyFrom;
    }

    public Date getApplyTo() {
        return applyTo;
    }

    public void setApplyTo(Date applyTo) {
        this.applyTo = applyTo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public User getCreatorId() {
        if (creatorId == null) {
            creatorId = new User();
        }
        return creatorId;
    }

    public void setCreatorId(User creatorId) {
        this.creatorId = creatorId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<InputAddonUser> getListUser() {
        if (listUser == null) {
            listUser = new ArrayList<>();
        }
        return listUser;
    }

    public void setListUser(List<InputAddonUser> listUser) {
        this.listUser.clear();
        this.listUser.addAll(listUser);
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}