package com.horical.gito.mvp.user.detail.presenter;

import com.horical.gito.model.User;

/**
 * Created by thanhle on 4/7/17.
 */

public interface IUserDetailPresenter {
    void updateUser(User user, int isAdmin, int isLock, int type, String displayName, String idRoleCompany, String idPosition, String email,
                    String userId, String phone, String idDepartment, String address);

    void deleteMember(String idMember);

    void changePasswordByAdmin(String id, String newPassword);

    void changePasswordByThisUser(String currentPassword, String newPassword, String confirmPassword);
}
