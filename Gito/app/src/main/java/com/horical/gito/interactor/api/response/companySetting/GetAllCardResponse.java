package com.horical.gito.interactor.api.response.companySetting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Card;

import java.util.List;

/**
 * Created by kut3b on 12/04/2017.
 */

public class GetAllCardResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Card> cards;
}
