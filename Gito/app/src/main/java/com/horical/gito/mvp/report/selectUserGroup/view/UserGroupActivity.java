package com.horical.gito.mvp.report.selectUserGroup.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.ObjectSerialized;
import com.horical.gito.mvp.report.selectUserGroup.adapter.UserGroupAdapter;
import com.horical.gito.mvp.report.selectUserGroup.injection.component.DaggerUserGroupComponent;
import com.horical.gito.mvp.report.selectUserGroup.injection.component.UserGroupComponent;
import com.horical.gito.mvp.report.selectUserGroup.injection.module.UserGroupModule;
import com.horical.gito.mvp.report.selectUserGroup.presenter.UserGroupPresenter;
import com.horical.gito.utils.LogUtils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UserGroupActivity extends BaseActivity implements IUserGroupView, View.OnClickListener {

    public static final String TAG = LogUtils.makeLogTag(UserGroupActivity.class);

    public final static String KEY_FROM = "KEY_FROM";

    public final static int KEY_FROM_REPORT_TO_USER = 0;

    public final static int KEY_FROM_INFORM_TO_USER = 1;

    public final static int KEY_FROM_INFORM_TO_GROUP = 2;

    public final static String KEY_SAVE_DATA = "KEY_SAVE_DATA";

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.edt_search_user)
    EditText edtSearch;

    @Bind(R.id.imv_clear_search)
    ImageView imvClearSearch;

    @Bind(R.id.ll_check_all)
    LinearLayout llCheckAll;

    @Bind(R.id.chk_item_user)
    CheckBox chkCheckAll;

    @Bind(R.id.refresh_user)
    SwipeRefreshLayout refreshUser;

    @Bind(R.id.rcv_user)
    RecyclerView rcvUser;

    private UserGroupAdapter adapter;

    private int keyFrom;
    private boolean isCheckAll;


    @Inject
    UserGroupPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);

        UserGroupComponent component = DaggerUserGroupComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .userGroupModule(new UserGroupModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        keyFrom = getIntent().getIntExtra(KEY_FROM, 0);

        initData();
        initListener();
    }

    protected void initData() {
        adapter = new UserGroupAdapter(this, mPresenter.getListSearch());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcvUser.setLayoutManager(layoutManager);
        rcvUser.setHasFixedSize(false);
        rcvUser.setAdapter(adapter);

        switch (keyFrom) {
            case KEY_FROM_REPORT_TO_USER:
                topBar.setTextAll(getString(R.string.cancel), getString(R.string.title_user, mPresenter.sizeSelected()), getString(R.string.save));
                mPresenter.getListReportToUser();
                break;
            case KEY_FROM_INFORM_TO_USER:
                topBar.setTextAll(getString(R.string.cancel), getString(R.string.title_user, mPresenter.sizeSelected()), getString(R.string.save));
                mPresenter.getListInformToUser();
                break;

            case KEY_FROM_INFORM_TO_GROUP:
                topBar.setTextAll(getString(R.string.cancel), getString(R.string.add), getString(R.string.save));
                mPresenter.getListGroup();
                break;
        }
    }

    protected void initListener() {
        llCheckAll.setOnClickListener(this);
        chkCheckAll.setOnClickListener(this);
        imvClearSearch.setOnClickListener(this);

        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

                if (!mPresenter.getListSave().isEmpty()) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(KEY_SAVE_DATA, new ObjectSerialized(mPresenter.getListSave()));
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    showErrorDialog(getString(R.string.error_user_picker));
                }
            }
        });

        refreshUser.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                switch (keyFrom) {
                    case KEY_FROM_REPORT_TO_USER:
                        mPresenter.getListReportToUser();
                        break;
                    case KEY_FROM_INFORM_TO_USER:
                        mPresenter.getListInformToUser();
                        break;

                    case KEY_FROM_INFORM_TO_GROUP:
                        mPresenter.getListGroup();

                        break;
                }
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onSearch();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) imvClearSearch.setVisibility(View.VISIBLE);
                else imvClearSearch.setVisibility(View.GONE);
            }
        });

        adapter.setOnItemClickListener(new UserGroupAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                isCheckAll = mPresenter.isCheckedAll();
                chkCheckAll.setChecked(mPresenter.isCheckedAll());

                updateTitle();
            }
        });
    }

    private void onSearch() {
        mPresenter.getListSearchUser(edtSearch.getText().toString());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_check_all:
            case R.id.chk_item_user:
                isCheckAll = !isCheckAll;
                chkCheckAll.setChecked(isCheckAll);

                mPresenter.setCheckAll(isCheckAll);
                adapter.notifyDataSetChanged();

                updateTitle();
                break;

            case R.id.imv_clear_search:
                edtSearch.setText("");
                break;
        }
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void onSuccessList() {
        refreshUser.setRefreshing(false);
        onSearch();
        updateTitle();
    }

    @Override
    public void onFailList(String error) {
        showErrorDialog(error);
        refreshUser.setRefreshing(false);
        updateTitle();
    }

    private void updateTitle() {
        switch (keyFrom) {
            case KEY_FROM_REPORT_TO_USER:
            case KEY_FROM_INFORM_TO_USER:
                topBar.setTextAll(getString(R.string.cancel), getString(R.string.title_user, mPresenter.sizeSelected()), getString(R.string.save));
        }
    }
}

