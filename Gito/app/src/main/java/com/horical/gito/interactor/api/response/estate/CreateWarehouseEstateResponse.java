package com.horical.gito.interactor.api.response.estate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.estate.WarehouseEstate;

/**
 * Created by hoangsang on 4/13/17.
 */

public class CreateWarehouseEstateResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    public WarehouseEstate warehouseEstate;
}
