package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Metric implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("acronym")
    @Expose
    private String acronym;

    @SerializedName("inputSource")
    @Expose
    private int inputSource;

    @SerializedName("value")
    @Expose
    private int value;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("total")
    @Expose
    private double total;

    @SerializedName("stepParam")
    @Expose
    private List<StepParam> stepParam;

    @SerializedName("isTotal")
    @Expose
    private boolean isTotal;

    @SerializedName("inputType")
    @Expose
    private int inputType;

    @SerializedName("calculateType")
    @Expose
    private int calculateType;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public int getInputSource() {
        return inputSource;
    }

    public void setInputSource(int inputSource) {
        this.inputSource = inputSource;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<StepParam> getStepParam() {
        if (stepParam == null) {
            stepParam = new ArrayList<>();
        }
        return stepParam;
    }

    public void setStepParam(List<StepParam> stepParam) {
        this.stepParam = stepParam;
    }

    public boolean isTotal() {
        return isTotal;
    }

    public void setTotal(boolean total) {
        isTotal = total;
    }

    public int getInputType() {
        return inputType;
    }

    public void setInputType(int inputType) {
        this.inputType = inputType;
    }

    public int getCalculateType() {
        return calculateType;
    }

    public void setCalculateType(int calculateType) {
        this.calculateType = calculateType;
    }
}