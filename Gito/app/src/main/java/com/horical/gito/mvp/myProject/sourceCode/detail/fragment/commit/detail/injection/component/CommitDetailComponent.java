package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.injection.module.CommitDetailModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.commit.detail.view.CommitDetailActivity;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class , modules = CommitDetailModule.class)
public interface CommitDetailComponent {
    void inject(CommitDetailActivity activity);
}
