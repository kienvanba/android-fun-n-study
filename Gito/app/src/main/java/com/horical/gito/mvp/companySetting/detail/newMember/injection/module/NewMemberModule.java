package com.horical.gito.mvp.companySetting.detail.newMember.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.detail.newMember.presenter.NewMemberPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class NewMemberModule {
    @PerActivity
    @Provides
    NewMemberPresenter provideNewMemberPresenter(){
        return new NewMemberPresenter();
    }
}
