package com.horical.gito.mvp.salary.bonus.list.fragments.mine.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.InputAddon;
import com.horical.gito.utils.DateUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BonusMineAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<InputAddon> items;
    private MineAdapterListener callback;

    public BonusMineAdapter(Context context, List<InputAddon> items, MineAdapterListener callback) {
        this.context = context;
        this.items = items;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list_bonus, parent, false);
        return new InputAddonHolder(v);
    }

    private void updateStatus(InputAddonHolder inputAddonHolder, int status) {
        switch (status) {
            //Rejected
            case 0:
                inputAddonHolder.imvDelete.setVisibility(View.VISIBLE);
                inputAddonHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.red));
                inputAddonHolder.tvStatus.setText(context.getString(R.string.rejected));
                break;
            //New
            case 1:
                inputAddonHolder.imvDelete.setVisibility(View.VISIBLE);
                inputAddonHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.orange));
                inputAddonHolder.tvStatus.setText(context.getString(R.string.new_salary));
                break;
            //Approved
            case 2:
                inputAddonHolder.imvDelete.setVisibility(View.GONE);
                inputAddonHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.green_color));
                inputAddonHolder.tvStatus.setText(context.getString(R.string.approved));
                break;
            //Submitted
            case 3:
                inputAddonHolder.imvDelete.setVisibility(View.GONE);
                inputAddonHolder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.green_doing));
                inputAddonHolder.tvStatus.setText(context.getString(R.string.submitted));
                break;

            default:
                break;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        InputAddon inputAddon = items.get(position);
        InputAddonHolder inputAddonHolder = (InputAddonHolder) holder;
        inputAddonHolder.tvTitle.setText(inputAddon.getName());
        updateStatus(inputAddonHolder, inputAddon.getStatus());
        inputAddonHolder.tvUser.setText(String.valueOf(inputAddon.getCreatorId().getGitId()));
        inputAddonHolder.tvAcronym.setText(inputAddon.getAcronym());
        inputAddonHolder.tvDateFrom.setText(DateUtils.formatDate(inputAddon.getApplyFrom()));
        inputAddonHolder.tvDateTo.setText(DateUtils.formatDate(inputAddon.getApplyTo()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class InputAddonHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @Bind(R.id.tv_tile)
        TextView tvTitle;

        @Bind(R.id.tv_status)
        TextView tvStatus;

        @Bind(R.id.imv_delete)
        ImageView imvDelete;

        @Bind(R.id.tv_user)
        TextView tvUser;

        @Bind(R.id.tv_acronym)
        TextView tvAcronym;

        @Bind(R.id.tv_date_from)
        TextView tvDateFrom;

        @Bind(R.id.tv_date_to)
        TextView tvDateTo;

        View view;

        InputAddonHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imvDelete.setOnClickListener(this);
            view = itemView;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onSelected(getAdapterPosition());
                }
            });
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == imvDelete.getId()) {
                callback.onDelete(getAdapterPosition());
            }
        }
    }

    public interface MineAdapterListener {
        void onSelected(int position);

        void onDelete(int position);
    }
}
