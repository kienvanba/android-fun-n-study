package com.horical.gito.mvp.dialog.dialogChangePassWord;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by kut3b on 13/04/2017.
 */

public class DialogChangePassword extends Dialog implements View.OnClickListener {
    @Bind(R.id.edt_current_password)
    EditText edtCurrentPassword;

    @Bind(R.id.edt_new_password)
    EditText edtNewPassword;

    @Bind(R.id.edt_confirm_password)
    EditText edtConfirmPassword;

    @Bind(R.id.tv_cancel)
    TextView tvCancel;

    @Bind(R.id.tv_done)
    TextView tvDone;

    public DialogChangePassword(@NonNull Context context) {
        super(context, R.style.FullscreenDialog);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_change_pass);
        if (getWindow() != null) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        ButterKnife.bind(this);

        initData();
        initListener();
    }

    private void initData() {

    }

    private void initListener() {
        tvCancel.setOnClickListener(this);
        tvDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_done:
                mListener.onClickDone(edtCurrentPassword.getText().toString(), edtNewPassword.getText().toString(),
                        edtConfirmPassword.getText().toString());
                dismiss();
                break;
        }
    }

    private OnClickItemListener mListener;

    public void setOnClickItemListener(OnClickItemListener callBack) {
        this.mListener = callBack;
    }

    public interface OnClickItemListener {
        void onClickDone(String currentPassword, String newPassword, String confirmPassword);
    }
}
