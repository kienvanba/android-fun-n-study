package com.horical.gito.mvp.salary.detail.salary.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.detail.salary.presenter.DetailSalaryPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailSalaryModule {
    @PerActivity
    @Provides
    DetailSalaryPresenter provideReportPresenter(){
        return new DetailSalaryPresenter();
    }
}
