package com.horical.gito.mvp.user.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.user.detail.injection.module.UserDetailModule;
import com.horical.gito.mvp.user.detail.view.UserDetailActivity;

import dagger.Component;

/**
 * Created by thanhle on 4/7/17.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = UserDetailModule.class)
public interface UserDetailComponent {
    void inject(UserDetailActivity activity);
}
