package com.horical.gito.mvp.myPage.adapter;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.Task;

public class TaskItem implements IRecyclerItem {

    private Task task;

    public TaskItem(Task task) {
        this.task = task;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    @Override
    public int getItemViewType() {
        return 0;
    }
}
