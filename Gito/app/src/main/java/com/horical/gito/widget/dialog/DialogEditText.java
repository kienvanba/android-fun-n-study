package com.horical.gito.widget.dialog;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class DialogEditText extends GitODialog implements View.OnClickListener{
    @Bind(R.id.save_btn)
    public Button btnSave;
    @Bind(R.id.text_edit)
    public EditText editText;
    public DialogEditText(Context context){
        super(context,android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar_MinWidth);
        setContentView(R.layout.layout_dialog_edit_text);
        ButterKnife.bind(this);
        btnSave.setOnClickListener(this);
    }
    public DialogEditText(Context context, int themeResId) {
        super(context, themeResId);
        setContentView(R.layout.layout_dialog_edit_text);
        ButterKnife.bind(this);
        btnSave.setOnClickListener(this);
    }
    public void setHint(String hint){
        editText.setHint(hint);
    }
    public abstract void setSaveButtonClickListener();
    @Override
    public void onClick(View v) {
        setSaveButtonClickListener();
    }
}
