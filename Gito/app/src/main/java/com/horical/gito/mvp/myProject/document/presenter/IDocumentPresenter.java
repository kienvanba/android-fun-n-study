package com.horical.gito.mvp.myProject.document.presenter;

import com.horical.gito.base.IRecyclerItem;

import java.util.List;

public interface IDocumentPresenter  {
    List<IRecyclerItem> getAllItems();
    void getAllDocuments(String linkFile);
}
