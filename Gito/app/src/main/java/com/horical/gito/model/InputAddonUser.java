package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InputAddonUser implements Serializable {
    @SerializedName("userId")
    @Expose
    private User userId;

    @SerializedName("amount")
    @Expose
    private int amount;

    public InputAddonUser(User userId, int amount) {
        this.userId = userId;
        this.amount = amount;
    }

    public User getUserId() {
        if (userId == null) {
            userId = new User();
        }
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
