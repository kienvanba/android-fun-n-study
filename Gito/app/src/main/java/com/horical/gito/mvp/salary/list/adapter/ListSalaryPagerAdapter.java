package com.horical.gito.mvp.salary.list.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.horical.gito.mvp.salary.list.fragments.BaseListSalaryFragment;

import java.util.List;

public class ListSalaryPagerAdapter extends FragmentPagerAdapter {

    private List<BaseListSalaryFragment> mSalaryFragments;

    public ListSalaryPagerAdapter(FragmentManager fm, List<BaseListSalaryFragment> salaryFragments) {
        super(fm);
        this.mSalaryFragments = salaryFragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mSalaryFragments.get(position);
    }

    @Override
    public int getCount() {
        return mSalaryFragments.size();
    }
}
