package com.horical.gito.interactor.api.network.deserialize;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.horical.gito.model.Level;
import com.horical.gito.utils.GsonUtils;

import java.lang.reflect.Type;

public class LevelDeserialize implements JsonDeserializer<Level> {

    @Override
    public Level deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Level level;
        try {
            level = GsonUtils.createGson(Level.class).fromJson(json, Level.class);
        } catch (Exception e) {
            level = new Level();
            level.setId(json.getAsString());
        }
        return level;
    }
}
