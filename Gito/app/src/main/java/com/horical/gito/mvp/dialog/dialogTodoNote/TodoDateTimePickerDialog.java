package com.horical.gito.mvp.dialog.dialogTodoNote;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Created by Luong on 05-Apr-17.
 */

public class TodoDateTimePickerDialog extends Dialog
        implements DatePicker.OnDateChangedListener, TimePicker.OnTimeChangedListener, View.OnClickListener {

    @Bind(R.id.dp_todo_start_day)
    DatePicker dpStartDate;
    @Bind(R.id.tp_todo_start_time)
    TimePicker tpStartTime;
    @Bind(R.id.tv_cancel)
    TextView Cancel;
    @Bind(R.id.tv_done)
    TextView Done;


    DateTimeDialogCallback mCallback;

    Context mContext;

    public TodoDateTimePickerDialog(final Context context, DateTimeDialogCallback callback) {
        super(context);
        mContext = context;
        mCallback = callback;
        this.setContentView(R.layout.fragment_todo_date_time_picker);
        ButterKnife.bind(this);
    }

    public Date getFrom() {
        Date DateTime = new Date(
                dpStartDate.getYear(), dpStartDate.getMonth(), dpStartDate.getDayOfMonth(),
                tpStartTime.getCurrentHour(), tpStartTime.getCurrentMinute(), 0);
        return DateTime;
    }

    public void setFrom(Date DateTime) {
        dpStartDate.updateDate(DateTime.getYear(), DateTime.getMonth(), DateTime.getDay());
        tpStartTime.setCurrentHour(DateTime.getHours());
        tpStartTime.setCurrentMinute(DateTime.getMinutes());
    }

    private boolean isSmallerDate(Date var1, Date var2) {
        if (!(var1.getYear() == var2.getYear())) {
            return var1.getYear() < var2.getYear();
        }
        if (!(var1.getMonth() == var2.getMonth())) {
            return var1.getMonth() < var2.getMonth();
        }
        if (!(var1.getDay() == var2.getDay())) {
            return var1.getDay() < var2.getDay();
        }
        return false;
    }

    private boolean isSmallerTime(Date var1, Date var2) {
        if (!(var1.getHours() == var2.getHours())) return var1.getHours() < var2.getHours();
        if (var1.getMinutes() == var2.getMinutes()) return var1.getMinutes() < var2.getMinutes();
        else return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);
        tpStartTime.setOnTimeChangedListener(this);
        Calendar c = Calendar.getInstance();
        dpStartDate.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), this);
        Done.setOnClickListener(this);
        Cancel.setOnClickListener(this);
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//        if (view.getId() == mDateFrom.getId()) {
//            if (isSmallerDate(getTo(), getFrom())) setTo(getFrom());
//            return;
//        }
//        if (view.getId() == mDateTo.getId()) {
//            if (isSmallerDate(getTo(), getFrom())) setFrom(getTo());
//            return;
//        }
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
//        if (!isSmallerDate(getFrom(), getTo()) && !isSmallerDate(getTo(), getFrom())) {
//            if (view.getId() == mTimeFrom.getId()) {
//                if (isSmallerTime(getTo(), getFrom())) setTo(getFrom());
//                return;
//            }
//            if (view.getId() == mTimeTo.getId()) {
//                if (isSmallerTime(getTo(), getFrom())) setFrom(getTo());
//                return;
//            }
//        }
    }

    @Override
    public void onClick(View v) {
//        if (Done.getId() == v.getId()) {
//            mCallback.onDoneClick(getFrom(), getTo());
//            cancel();
//        }
//        if (Cancel.getId() == v.getId()) {
//            mCallback.onCancelClick();
//            cancel();
//        }
    }

    public interface DateTimeDialogCallback {
        void onDoneClick(Date DateTimeFrom, Date DateTimeTo);

        void onCancelClick();
    }
}
