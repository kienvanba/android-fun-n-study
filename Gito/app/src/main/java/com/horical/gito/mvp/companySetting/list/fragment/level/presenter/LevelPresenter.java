package com.horical.gito.mvp.companySetting.list.fragment.level.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllLevelResponse;
import com.horical.gito.model.Level;
import com.horical.gito.mvp.companySetting.list.fragment.level.view.ILevelView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class LevelPresenter extends BasePresenter implements ILevelPresenter {
    private List<Level> listLevel;

    public void attachView(ILevelView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public ILevelView getView() {
        return (ILevelView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        listLevel = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Level> getListLevel() {
        return listLevel;
    }

    public void setListLevel(List<Level> listLevel) {
        this.listLevel = listLevel;
    }

    @Override
    public void getAllLevel() {
        getApiManager().getAllLevel(new ApiCallback<GetAllLevelResponse>() {
            @Override
            public void success(GetAllLevelResponse res) {
                if (listLevel != null) {
                    listLevel.clear();
                } else {
                    listLevel = new ArrayList<>();
                }
                listLevel.addAll(res.levels);
                if(GitOStorage.getInstance().getListLevel() != null) {
                    GitOStorage.getInstance().getListLevel().clear();
                }
                GitOStorage.getInstance().setListLevel(res.levels);
                getView().getAllLevelSuccess();
            }

            @Override
            public void failure(RestError error) {
                if (listLevel != null) {
                    listLevel.clear();
                } else {
                    listLevel = new ArrayList<>();
                }
                getView().getAllLevelFailure(error.message);
            }
        });
    }

    @Override
    public void deleteLevel(String idLevel, final int position) {
        getApiManager().deleteLevel(idLevel, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteLevelSuccess(position);
            }

            @Override
            public void failure(RestError error) {
                getView().deleteLevelFailure(error.message);
            }
        });
    }
}

