package com.horical.gito.mvp.salary.detail.metricGroup.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.detail.metricGroup.injection.module.DetailMetricGroupModule;
import com.horical.gito.mvp.salary.detail.metricGroup.view.DetailMetricGroupActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = DetailMetricGroupModule.class)
public interface IDetailMetricGroupComponent {
    void inject(DetailMetricGroupActivity activity);
}
