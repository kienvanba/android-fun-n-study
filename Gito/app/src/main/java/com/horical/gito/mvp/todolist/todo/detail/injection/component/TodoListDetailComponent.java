package com.horical.gito.mvp.todolist.todo.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.todolist.todo.detail.injection.module.TodoListDetailModule;
import com.horical.gito.mvp.todolist.todo.detail.view.TodoListDetailActivity;

import dagger.Component;

/**
 * Created by Luong on 16-Mar-17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = TodoListDetailModule.class)
public interface TodoListDetailComponent {
    void inject(TodoListDetailActivity activity);
}