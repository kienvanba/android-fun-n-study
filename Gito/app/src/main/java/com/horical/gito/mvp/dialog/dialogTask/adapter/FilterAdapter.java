package com.horical.gito.mvp.dialog.dialogTask.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.ItemFilter;

/**
 * Created by thanhle on 3/9/17.
 */

public class FilterAdapter extends ArrayAdapter<ItemFilter> {

    private Context context;
    private int resource;

    public FilterAdapter(Context context, int resource) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_filter_task, parent, false);

        TextView tvItem = (TextView) v.findViewById(R.id.tv_item_task);
        CheckBox chkCheck = (CheckBox) v.findViewById(R.id.chk_check_item_filter);

        ItemFilter itemFilter = this.getItem(position);
        tvItem.setText(itemFilter.getName());
        chkCheck.setChecked(itemFilter.isSelected());
        return v;
    }
}