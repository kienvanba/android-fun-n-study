package com.horical.gito.mvp.checkin.list.view.adapter;

import com.horical.gito.base.IRecyclerItem;

/**
 * Created by Penguin on 20-Dec-16.
 */

public class UserProfile implements IRecyclerItem {
    public int Avatar;
    public String UserName;
    public String Major;
    public int Sun,Mon,Tue,Wed,Thu,Fri,Sat;

    public UserProfile(int avatar, String username, String major, int sun, int mon, int tue, int wed, int thu, int fri, int sat)
    {
        this.Avatar = avatar;
        this.UserName = username;
        this.Major = major;
        this.Sun = sun;
        this.Mon = mon;
        this.Tue = tue;
        this.Wed = wed;
        this.Thu = thu;
        this.Fri = fri;
        this.Sat = sat;
    }
    @Override
    public int getItemViewType() {
        return 0;
    }
}

