package com.horical.gito.mvp.salary.dto.staff;

import java.io.Serializable;

public class ListStaffDto implements Serializable{
    private String name;
    private String major;
    private String type;
    private double salary;
    private double performance;
    private double flexible;

    public ListStaffDto() {
    }

    public ListStaffDto(String name, String major, String type, double salary, double performance, double flexible) {
        this.name = name;
        this.major = major;
        this.type = type;
        this.salary = salary;
        this.performance = performance;
        this.flexible = flexible;
    }

    //get set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getPerformance() {
        return performance;
    }

    public void setPerformance(double performance) {
        this.performance = performance;
    }

    public double getFlexible() {
        return flexible;
    }

    public void setFlexible(double flexible) {
        this.flexible = flexible;
    }
}
