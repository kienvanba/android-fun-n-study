package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Leaving implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("creatorId")
    @Expose
    private CreatorId creatorId;
    @SerializedName("companyId")

    @Expose
    private String companyId;
    @SerializedName("fromDate")
    @Expose
    private Date fromDate;
    @SerializedName("toDate")
    @Expose
    private Date toDate;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("reason")
    @Expose
    private String reason;

//    @SerializedName("approverListId")
//    private List<String> approverListId;
    @SerializedName("amountDate")
    @Expose
    private int amountDate;
    @SerializedName("createdDate")
    @Expose
    private Date createdDate;

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public CreatorId getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(CreatorId creatorId) {
        this.creatorId = creatorId;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getAmountDate() {
        return amountDate;
    }

    public void setAmountDate(int amountDate) {
        this.amountDate = amountDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
