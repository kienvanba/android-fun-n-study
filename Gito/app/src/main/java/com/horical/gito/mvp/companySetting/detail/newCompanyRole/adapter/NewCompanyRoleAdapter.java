package com.horical.gito.mvp.companySetting.detail.newCompanyRole.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.mvp.companySetting.detail.newCompanyRole.dto.ContentNewCompanyRole;

import java.util.HashMap;
import java.util.List;

/**
 * Created by thanhle on 3/20/17.
 */

public class NewCompanyRoleAdapter extends BaseExpandableListAdapter{

    private Context context;
    private List<String> listDataHeader;
    private HashMap<String, List<ContentNewCompanyRole>> listDataChild;

    public NewCompanyRoleAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<ContentNewCompanyRole>> listChildData) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final ContentNewCompanyRole content = (ContentNewCompanyRole) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_content_new_company_role, null);
        }

        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_content_title);
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.img_icon);
        final CheckBox chkCheck = (CheckBox) convertView.findViewById(R.id.chk_check_item);

        tvTitle.setText(content.getTitle());
        if (content.getTitle().equalsIgnoreCase("Add")
                || content.getTitle().equalsIgnoreCase("Add member")
                || content.getTitle().equalsIgnoreCase("Create company room")
                || content.getTitle().equalsIgnoreCase("Add Estate item")
                || content.getTitle().equalsIgnoreCase("Add free room")) {
            imgIcon.setImageResource(R.drawable.ic_add_blue);
        } else if (content.getTitle().equalsIgnoreCase("Delete")
                || content.getTitle().equalsIgnoreCase("Delete Estate item")) {
            imgIcon.setImageResource(R.drawable.ic_delete);
        } else if (content.getTitle().equalsIgnoreCase("Modify")
                || content.getTitle().equalsIgnoreCase("Modify Estate item")) {
            imgIcon.setImageResource(R.drawable.ic_edit);
        } else if (content.getTitle().equalsIgnoreCase("Lock")) {
            imgIcon.setImageResource(R.drawable.ic_lock);
        } else if (content.getTitle().equalsIgnoreCase("Approve")) {
            imgIcon.setImageResource(R.drawable.ic_approve);
        } else if (content.getTitle().equalsIgnoreCase("Leave room")) {
            imgIcon.setImageResource(R.drawable.ic_leave_room);
        } else if (content.getTitle().equalsIgnoreCase("Eject member")) {
            imgIcon.setImageResource(R.drawable.ic_eject);
        } else if (content.getTitle().equalsIgnoreCase("Interview")) {
            imgIcon.setImageResource(R.drawable.ic_interview);
        } else if (content.getTitle().equalsIgnoreCase("View") || content.getTitle().equalsIgnoreCase("View User")) {
            imgIcon.setImageResource(R.drawable.ic_view);
        }

        chkCheck.setChecked(content.getSelection());

        chkCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content.setSelection(!content.getSelection());
                mOnItemClickListener.onItemContentSelected();
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition)) != null ? this.listDataChild.get(this.listDataHeader.get(groupPosition))
                .size() : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String header = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_header_new_company_role, null);
        }
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
        final ImageView imgDropDown = (ImageView) convertView.findViewById(R.id.img_drop_down);

        tvTitle.setText(header);
        imgDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemHeaderSelected(imgDropDown);
            }
        });

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemHeaderSelected(ImageView img);

        void onItemContentSelected();
    }
}