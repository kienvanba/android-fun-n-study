package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.injection.module.BrowseModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.view.BrowseFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class , modules = BrowseModule.class)
public interface BrowseComponent {
    void inject(BrowseFragment fragment);
}
