package com.horical.gito.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import com.horical.gito.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.horical.gito.utils.LogUtils.LOGI;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class FileUtils {

    private static final String TAG = makeLogTag(FileUtils.class);

    public static final String JPEG_FILE_PREFIX = "IMG_";
    public static final String JPEG_FILE_SUFFIX = ".jpg";

    public static void startActionGetImage(Activity activity, int requestCode, String chooserTitle) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        activity.startActivityForResult(Intent.createChooser(intent, chooserTitle), requestCode);
    }

    public static File startActionImageCapture(Activity activity, int requestCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = null;
        try {
            file = createTempFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    activity.getResources().getString(R.string.app_name), JPEG_FILE_PREFIX, JPEG_FILE_SUFFIX);
            if (file == null) {
                LOGI(TAG, "file is null");
                return null;
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            activity.startActivityForResult(takePictureIntent, requestCode);
        } catch (IOException e) {
            e.printStackTrace();
            if (file != null) {
                file.delete();
            }
            file = null;
        }
        return file;
    }

    public static File createImageFile(Activity activity) {
        try {
            return createTempFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    activity.getResources().getString(R.string.app_name), JPEG_FILE_PREFIX, JPEG_FILE_SUFFIX);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static File createTempFile(File rootDir, String dirName, String prefix, String suffix) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String fileName = prefix + timeStamp + "_";
        File storageDir = getStorageDir(rootDir, dirName);
        if (storageDir == null) {
            LOGI(TAG, "storageDir is null");
            return null;
        }
        return File.createTempFile(fileName, suffix, storageDir);
    }

    public static File getStorageDir(File dir, String name) {
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = new File(dir, name);
            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    LOGI(TAG, "Failed to create directory");
                    return null;
                }
            }
        } else {
            LOGI(TAG, "External storage is not mounted READ/WRITE.");
        }
        return storageDir;
    }

    public static void saveBitmapToFile(Bitmap image, File file) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            LOGI(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            LOGI(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    public static Bitmap getResizedBitmap(Bitmap image, int newHeight, int newWidth) {
        int width = image.getWidth();
        int height = image.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(image, 0, 0, width, height, matrix, false);
    }

    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static File convertUriToFile(Context context, Uri uri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                && isMediaDocument(uri)) {
            final String docId = DocumentsContract.getDocumentId(uri);
            String[] slip = docId.split(":");
            final String type = slip[0];

            Uri contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

            final String selection = "_id=?";
            final String[] selectionArgs = new String[]{slip[1]};

            String filePath = getDataColumn(context, contentUri, selection, selectionArgs);

            if (filePath != null)
                return new File(filePath);
            else
                return null;

        } else {
            try {
                Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
                assert cursor != null;
                int index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                File file = new File(cursor.getString(index));
                cursor.close();
                return file;

            } catch (Exception e) {
                return new File(uri.getPath());
            }
        }
    }

    private static String getDataColumn(Context context, Uri uri,
                                        String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
}
