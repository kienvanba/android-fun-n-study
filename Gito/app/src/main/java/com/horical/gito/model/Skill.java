package com.horical.gito.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Skill implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    @SerializedName("skillId")
    @Expose
    private String skillId;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("skilldetails")
    @Expose
    private List<SkillDetail> skillDetails;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getSkillId() {
        return skillId;
    }

    public void setSkillId(String skillId) {
        this.skillId = skillId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SkillDetail> getSkillDetails() {
        return skillDetails;
    }

    public void setSkillDetails(List<SkillDetail> skillDetails) {
        this.skillDetails = skillDetails;
    }
}
