package com.horical.gito.mvp.checkin.list.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Penguin on 16-Dec-16.
 */

public class CheckinFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyRecyclerHeadersAdapter {

    private Context Icontext;
    private List<ListUser> mList;
    private OnItemClickListener mOnItemClickListener;

    public CheckinFilterAdapter(Context context, List<ListUser> list) {
        Icontext = context;
        mList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_checkin_filter_user, parent, false);
        viewHolder = new CheckinViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ListUser listUser = mList.get(position);
        CheckinViewHolder viewHolder = (CheckinViewHolder) holder;
        viewHolder.mIvAvatar.setImageResource(listUser.Avatar);
        viewHolder.mTvName.setText(listUser.UserName);
        viewHolder.mTvMajor.setText(listUser.Major);
        viewHolder.mIvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mList.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public long getHeaderId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class CheckinViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.iv_filter_avatar)
        ImageView mIvAvatar;

        @Bind(R.id.tv_filter_name)
        TextView mTvName;

        @Bind(R.id.tv_filter_major)
        TextView mTvMajor;

        @Bind(R.id.iv_filter_delete)
        ImageView mIvDelete;

        public CheckinViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Calendar cal = Calendar.getInstance();
        }

    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);
    }
}