package com.horical.gito.mvp.salary.detail.metricGroup.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.mvp.salary.detail.metric.view.DetailMetricActivity;
import com.horical.gito.mvp.salary.detail.metricGroup.adapter.DetailMetricGroupAdapter;
import com.horical.gito.mvp.salary.detail.metricGroup.injection.component.DaggerIDetailMetricGroupComponent;
import com.horical.gito.mvp.salary.detail.metricGroup.injection.component.IDetailMetricGroupComponent;
import com.horical.gito.mvp.salary.detail.metricGroup.injection.module.DetailMetricGroupModule;
import com.horical.gito.mvp.salary.detail.metricGroup.presenter.DetailMetricGroupPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailMetricGroupActivity extends BaseActivity implements View.OnClickListener,
        IDetailMetricGroupView {

    @Bind(R.id.imv_edit_title)
    ImageView imvEditTitle;

    @Bind(R.id.edt_title)
    EditText edtTitle;

    @Bind(R.id.imv_add_metric)
    ImageView imvAddMetric;

    @Bind(R.id.rcv_metric)
    RecyclerView rcvMetric;

    @Bind(R.id.tv_no_data)
    TextView tvNoData;

    @Inject
    DetailMetricGroupPresenter mPresenter;

    private DetailMetricGroupAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_metric_group);
        ButterKnife.bind(this);
        IDetailMetricGroupComponent component = DaggerIDetailMetricGroupComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .detailMetricGroupModule(new DetailMetricGroupModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        String metricGroupId = getIntent().getStringExtra(AppConstants.METRIC_GROUP_ID_KEY);
        if (!TextUtils.isEmpty(metricGroupId)) {
            mPresenter.requestGetMetricGroup(metricGroupId);
        }

        initData();
        initListener();
    }

    protected void initData() {
        mAdapter = new DetailMetricGroupAdapter(this, mPresenter.getMetricGroup().getMetrics(), new DetailMetricGroupAdapter.MetricAdapterListener() {
            @Override
            public void onSelected(int position) {
                Intent detailMetricIntent = new Intent(DetailMetricGroupActivity.this, DetailMetricActivity.class);
                detailMetricIntent.putExtra(AppConstants.METRIC_ID_KEY, mPresenter.getMetricGroup().getMetrics().get(position).get_id());
                startActivity(detailMetricIntent);
            }

            @Override
            public void onRemove(int position) {
                showLoading();
                mPresenter.requestDeleteSalaryMetric(mPresenter.getMetricGroup().getMetrics().get(position).get_id());
            }
        });
        rcvMetric.setAdapter(mAdapter);
        rcvMetric.setLayoutManager(new LinearLayoutManager(this));
        tvNoData.setText(getString(R.string.no_data, getString(R.string.metric)));
        checkEmptyMetricGroup();
    }

    protected void initListener() {
        imvEditTitle.setOnClickListener(this);
        imvAddMetric.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_edit_title:

                break;

            case R.id.imv_add_metric:

                break;

            default:
                break;

        }
    }

    @Override
    public void requestGetAllMetricSuccess() {
        hideLoading();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void requestGetAllMetricError(RestError error) {
        hideLoading();
        showErrorDialog(error.message);
    }

    @Override
    public void requestDeleteMetricSuccess() {
        hideLoading();
        checkEmptyMetricGroup();
        mAdapter.setDataChanged(mPresenter.getMetricGroup().getMetrics());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void requestDeleteMetricError(RestError error) {
        hideLoading();
        showErrorDialog(error.message);
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    private void checkEmptyMetricGroup() {
        if (mPresenter.getMetricGroup().getMetrics().size() > 0) {
            tvNoData.setVisibility(View.GONE);
            rcvMetric.setVisibility(View.VISIBLE);
        } else {
            tvNoData.setVisibility(View.VISIBLE);
            rcvMetric.setVisibility(View.GONE);
        }
    }
}