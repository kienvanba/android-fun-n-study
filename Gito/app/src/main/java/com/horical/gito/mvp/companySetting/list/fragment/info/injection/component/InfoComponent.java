package com.horical.gito.mvp.companySetting.list.fragment.info.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.fragment.info.injection.module.InfoModule;
import com.horical.gito.mvp.companySetting.list.fragment.info.view.InfoFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = InfoModule.class)
public interface InfoComponent {
    void inject(InfoFragment fragment);
}
