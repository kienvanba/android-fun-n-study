package com.horical.gito.mvp.myProject.setting.fragment.group.presenter;


import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.group.GetAllGroupResponse;
import com.horical.gito.model.Group;
import com.horical.gito.mvp.myProject.setting.adapter.GroupMemItemBody;
import com.horical.gito.mvp.myProject.setting.fragment.group.view.IGroupFragment;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.LOGE;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class GroupPresenter extends BasePresenter implements IGroupPresenter {
    private static final String TAG = makeLogTag(GroupPresenter.class);

    private List<GroupMemItemBody> mGroups;

    public IGroupFragment getView(){
        return (IGroupFragment) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mGroups = new ArrayList<>();

        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        getEventManager().unRegister(this);
    }

    @Override
    public List<GroupMemItemBody> getGroups() {
        return mGroups;
    }

    @Override
    public void getAllGroup() {
        if (!isViewAttached()) return;
        getView().showLoading();

        String projectId = GitOStorage.getInstance().getProject().getId();

        mGroups.clear();
        getApiManager().getAllGroup(projectId, new ApiCallback<GetAllGroupResponse>() {
            @Override
            public void success(GetAllGroupResponse res) {
                if(!res.groups.isEmpty()){
                    for(Group group: res.groups){
                        GroupMemItemBody body = new GroupMemItemBody(group);
                        mGroups.add(body);
                    }
                }

                getView().getAllGroupSuccess();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }
}
