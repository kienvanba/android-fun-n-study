package com.horical.gito.mvp.companySetting.detail.newMember.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.model.Level;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by kut3b on 11/04/2017.
 */

public class MemberLevelAdapter extends RecyclerView.Adapter<MemberLevelAdapter.LevelHolder> {
    private Context mContext;
    private List<Level> mList;

    public MemberLevelAdapter(Context context, List<Level> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public LevelHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_member, parent, false);
        return new LevelHolder(view);
    }

    @Override
    public void onBindViewHolder(final LevelHolder holder, final int position) {
        final Level level = mList.get(position);

        holder.chkCheck.setChecked(level.isSelected());
        holder.tvName.setText(level.getName());
        holder.chkCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                level.setSelected(!level.isSelected());
                notifyItemChanged(position);
                mOnItemClickListener.onItemClickSelected(level.getId());
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                level.setSelected(!level.isSelected());
                notifyItemChanged(position);
                mOnItemClickListener.onItemClickSelected(level.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class LevelHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.chk_check)
        CheckBox chkCheck;

        @Bind(R.id.tv_name)
        TextView tvName;

        View view;

        public LevelHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(String id);
    }
}