package com.horical.gito.mvp.salary.bonus.list.fragments.approved.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.mvp.salary.bonus.detail.view.DetailBonusActivity;
import com.horical.gito.mvp.salary.bonus.list.fragments.BaseListBonusFragment;
import com.horical.gito.mvp.salary.bonus.list.fragments.approved.adapter.BonusApprovedAdapter;
import com.horical.gito.mvp.salary.bonus.list.fragments.approved.injection.component.DaggerIListApprovedFragmentComponent;
import com.horical.gito.mvp.salary.bonus.list.fragments.approved.injection.component.IListApprovedFragmentComponent;
import com.horical.gito.mvp.salary.bonus.list.fragments.approved.injection.module.ListApprovedFragmentModule;
import com.horical.gito.mvp.salary.bonus.list.fragments.approved.presenter.ListApprovedFragmentPresenter;

import javax.inject.Inject;

import butterknife.Bind;

public class ListApprovedFragment extends BaseListBonusFragment implements View.OnClickListener,
        IListApprovedFragmentView {

    @Bind(R.id.rcv_bonus)
    RecyclerView rcvBonus;

    @Inject
    ListApprovedFragmentPresenter mPresenter;

    private BonusApprovedAdapter mAdapter;

    public static ListApprovedFragment newInstance() {
        Bundle args = new Bundle();
        ListApprovedFragment fragment = new ListApprovedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list_bonus;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IListApprovedFragmentComponent component = DaggerIListApprovedFragmentComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .listApprovedFragmentModule(new ListApprovedFragmentModule())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        showLoading();
        mPresenter.requestGetApprovedInputAddon();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        mAdapter = new BonusApprovedAdapter(getActivity(), mPresenter.getListInputAddon(), new BonusApprovedAdapter.ApprovedAdapterListener() {
            @Override
            public void onSelected(int position) {
                Intent detailBonusIntent = new Intent(getActivity(), DetailBonusActivity.class);
                detailBonusIntent.putExtra(AppConstants.BONUS_ID_KEY, mPresenter.getListInputAddon().get(position).get_id());
                detailBonusIntent.putExtra(AppConstants.BONUS_MODE_KEY, getString(R.string.bonus_detail));
                startActivity(detailBonusIntent);
            }

            @Override
            public void onDelete(int position) {
                Toast.makeText(getActivity(), "delete at position:" + position, Toast.LENGTH_SHORT).show();
            }
        });
        rcvBonus.setAdapter(mAdapter);
        rcvBonus.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void requestGetApprovedInputAddonSuccess() {
        hideLoading();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void requestGetApprovedInputAddonError(RestError error) {
        hideLoading();
        showErrorDialog(error.message, null);
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }
}

