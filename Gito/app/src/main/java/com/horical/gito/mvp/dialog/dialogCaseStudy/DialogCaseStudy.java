package com.horical.gito.mvp.dialog.dialogCaseStudy;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DialogCaseStudy extends Dialog implements View.OnClickListener {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.ll_gallery)
    LinearLayout llGallery;

    @Bind(R.id.ll_uploadfile)
    LinearLayout llUploadFile;

    public DialogCaseStudy(@NonNull Context context) {
        super(context, R.style.FullscreenDialog);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_case_study);
        if (getWindow() != null) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        ButterKnife.bind(this);

        initData();
        initListener();
    }

    private void initData() {
        topBar.setTextTitle("Add attach file");
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
    }

    private void initListener() {
        llGallery.setOnClickListener(this);
        llUploadFile.setOnClickListener(this);
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                dismiss();
            }

            @Override
            public void onRightClicked() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_gallery:
                mListener.gallery();
                break;
            case R.id.ll_uploadfile:
                mListener.uploadFile();
                break;
        }
    }

    private OnClickItemListener mListener;

    public void setOnClickItemListener(OnClickItemListener callBack) {
        this.mListener = callBack;
    }

    public interface OnClickItemListener {

        void gallery();

        void uploadFile();
    }
}
