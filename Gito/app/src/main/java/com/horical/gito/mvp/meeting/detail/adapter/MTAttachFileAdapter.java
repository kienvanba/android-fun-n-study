package com.horical.gito.mvp.meeting.detail.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.horical.gito.R;
import com.horical.gito.model.TokenFile;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MTAttachFileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<TokenFile> attachFiles;
    private OnItemClickListener callback;

    public MTAttachFileAdapter(Context context, List<TokenFile> attachFiles, OnItemClickListener callback) {
        this.context = context;
        this.attachFiles = attachFiles;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_meeting_file_attatch, parent, false);
        return new AttachFileViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TokenFile attachFile = attachFiles.get(position);
        AttachFileViewHolder viewHolder = (AttachFileViewHolder) holder;
        viewHolder.mTvAttachFile.setText(attachFile.getName());


    }

    @Override
    public int getItemCount() {
        return attachFiles != null ? attachFiles.size() : 0;
    }

    public class AttachFileViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view;

        @Bind(R.id.tv_add_meeting_detail_file_attatch)
        GitOTextView mTvAttachFile;

        @Bind(R.id.ic_delete_file_attatch)
        ImageView mBtnDeleteAttachFile;

        public AttachFileViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
            view.setOnClickListener(this);
            mBtnDeleteAttachFile.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == view.getId()) {
                callback.onMTAttachFileClick(getAdapterPosition());
            } else if (v.getId() == mBtnDeleteAttachFile.getId()) {
                callback.onMTAttachFileDelete(getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onMTAttachFileDelete(int position);

        void onMTAttachFileClick(int position);
    }

}
