package com.horical.gito.interactor.api.response.report;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Reports;

public class UpdateReportResponse extends BaseResponse{
    @SerializedName("results")
    @Expose
    private Reports reports;

    public Reports getReports() {
        return reports;
    }

    public void setReports(Reports reports) {
        this.reports = reports;
    }
}
