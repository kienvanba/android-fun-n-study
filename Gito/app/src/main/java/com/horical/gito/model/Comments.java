package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by thanhle on 3/9/17
 */

public class Comments implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("creatorId")
    @Expose
    private CommentCreatorId creatorId;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("modifiedDate")
    @Expose
    private String modifiedDate;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public CommentCreatorId getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(CommentCreatorId creatorId) {
        this.creatorId = creatorId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
