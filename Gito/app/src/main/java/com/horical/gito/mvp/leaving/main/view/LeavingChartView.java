package com.horical.gito.mvp.leaving.main.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.horical.gito.R;
import com.horical.gito.widget.chartview.line.LineChartView;

import static java.lang.Thread.sleep;

/**
 * Created by Dragonoid on 1/13/2017.
 */

public class LeavingChartView extends LineChartView {
    protected int mColorForeground;

    public LeavingChartView(Context context) {
        super(context);
    }

    public LeavingChartView(Context context, AttributeSet attr) {
        super(context, attr);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attr,
                R.styleable.LeavingChartView, 0, 0);

        mColorForeground = getResources().getColor(R.color.app_color);
        try {
            mColorForeground = typedArray.getColor(R.styleable.LeavingChartView_colorForegroundPercent, 0);
        } finally {
            typedArray.recycle();
        }
    }

    @Override
    protected void onDrawBackground(Canvas canvas) {
        //do nothing
    }

    @Override
    public void setPercent(float mPercent) {
        super.setPercent(mPercent);
    }

    public float getPercent() {
        return mPercent;
    }

    @Override
    protected void onDrawForegroundPercent(final Canvas canvas, float widthParent) {
        mPaint.setColor(mColorForeground);
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.FILL);

        canvas.drawRect(
                0,
                mHeightBarTop,
                mPercent * getWidth() / 100,
                mHeightBarBot,
                mPaint
        );
    }
}
