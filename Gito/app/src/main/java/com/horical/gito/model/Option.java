package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nhattruong251295 on 4/11/2017.
 */

public class Option implements Serializable {
    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("id")
    @Expose
    private String id;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
