package com.horical.gito.mvp.companySetting.list.fragment.department.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllDepartmentResponse;
import com.horical.gito.model.Department;
import com.horical.gito.mvp.companySetting.list.fragment.department.view.IDepartmentView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class DepartmentPresenter extends BasePresenter implements IDepartmentPresenter {

    private List<Department> listDepartment;

    public void attachView(IDepartmentView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IDepartmentView getView() {
        return (IDepartmentView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        listDepartment = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Department> getListDepartment() {
        return listDepartment;
    }

    public void setListDepartment(List<Department> listDepartment) {
        this.listDepartment = listDepartment;
    }

    @Override
    public void getAllDepartment() {
        getApiManager().getAllDepartment(new ApiCallback<GetAllDepartmentResponse>() {
            @Override
            public void success(GetAllDepartmentResponse res) {
                if (listDepartment != null) {
                    listDepartment.clear();
                } else {
                    listDepartment = new ArrayList<>();
                }
                listDepartment.addAll(res.departments);
                if(GitOStorage.getInstance().getListDepartment() != null) {
                    GitOStorage.getInstance().getListDepartment().clear();
                }
                GitOStorage.getInstance().setListDepartment(res.departments);
                getView().getAllDepartmentSuccess();
            }

            @Override
            public void failure(RestError error) {
                if (listDepartment != null) {
                    listDepartment.clear();
                } else {
                    listDepartment = new ArrayList<>();
                }
                getView().getAllDepartmentFailure(error.message);
            }
        });
    }

    @Override
    public void deleteDepartment(String idDepartment, final int position) {
        getApiManager().deleteDepartment(idDepartment, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteDepartmentSuccess(position);
            }

            @Override
            public void failure(RestError error) {
                getView().deleteDepartmentFailure(error.message);
            }
        });
    }
}

