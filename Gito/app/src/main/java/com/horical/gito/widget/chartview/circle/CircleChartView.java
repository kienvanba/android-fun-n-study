package com.horical.gito.widget.chartview.circle;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;

import com.horical.gito.R;
import com.horical.gito.widget.chartview.ChartView;
import com.horical.gito.widget.chartview.dto.CircleChartIssueItem;

import java.util.ArrayList;
import java.util.List;

public class CircleChartView extends ChartView {

    public static final int START_ANGLE_POINT = 180;
    public static String SEMI_CIRCLE = "semi_circle";
    public static String CIRCLE = "circle";

    /**
     * Issues
     */
    protected int mIssues = 0;

    /**
     *
     */
    protected RectF mOval;

    /**
     * Angle of circle pie chart to draw
     */
    protected float mAngleDraw;

    /**
     * Type Draw
     */
    protected String mCircleType;

    /**
     * Percent to draw
     */
    protected float mPercentDraw;

    protected List<CircleChartIssueItem> mCircleChartHolders;

    /**
     * @param context
     */
    public CircleChartView(Context context) {
        super(context);
    }

    /**
     * @param context
     * @param attrs
     */
    public CircleChartView(Context context, AttributeSet attrs) {
        super(context, attrs);

        /** Get the attributes specified in attrs.xml using the name we included */
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.LineChartView, 0, 0);
        try {
            /** Get the text and colors specified using the names in attrs.xml */
            mCircleType = typedArray.getString(R.styleable.CircleChartView_circleType);
            mIssues = typedArray.getInteger(R.styleable.CircleChartView_issuse, 0);
        } finally {
            typedArray.recycle();
        }

        mAngleDraw = 0;
        mPercentDraw = 0;
        mCircleChartHolders = new ArrayList<>();
        mOval = new RectF();

        initData();
    }

    protected void initData() {
        CircleChartIssueItem issueItem = new CircleChartIssueItem(
                getResources().getColor(R.color.red),
                100);
        mCircleChartHolders.add(issueItem);

        issueItem = new CircleChartIssueItem(
                getResources().getColor(R.color.grey),
                0);
        mCircleChartHolders.add(issueItem);

        issueItem = new CircleChartIssueItem(
                getResources().getColor(R.color.app_color),
                0);
        mCircleChartHolders.add(issueItem);
    }

    public void setData(int remain, int needToTest, int resolved) {
        mCircleChartHolders.get(0).setPercent(remain);
        mCircleChartHolders.get(1).setPercent(needToTest);
        mCircleChartHolders.get(2).setPercent(resolved);
    }

    public float getPercentDraw() {
        return mPercentDraw;
    }

    public void setPercentDraw(float mPercentDraw) {
        this.mPercentDraw = mPercentDraw;
    }

    public float getAngleDraw() {
        return mAngleDraw;
    }

    public void setAngleDraw(float mAngleDraw) {
        this.mAngleDraw = mAngleDraw;
    }

    public void setIssues(int mIssues) {
        this.mIssues = mIssues;
    }

    /**
     * Begin Drawn
     *
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float radius = getHeight();

        /** Draw semi-circle foreground with percent **/
        onDrawForegroundPercent(canvas);

        /** Draw semi-circle container percent **/
        onDrawContainerPercent(radius, canvas);

        /** Draw Issue text **/
        onDrawIssueText(canvas);

        /** Line bottom **/
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(8);
        mPaint.setColor(getResources().getColor(R.color.app_color_opacity));

        canvas.drawLine(0,
                getHeight(),
                getWidth(),
                getHeight(),
                mPaint);
    }

    protected void onDrawContainerPercent(float radius, Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.white_opacity));
        mPaint.setStyle(Paint.Style.FILL);

        canvas.drawCircle(
                getWidth() / 2.0f,
                getHeight(),
                radius / 3 + convertDp(15),
                mPaint);

        mPaint.setColor(getResources().getColor(R.color.white));
        canvas.drawCircle(getWidth() / 2.0f,
                getHeight(),
                radius / 3,
                mPaint);
    }

    protected void onDrawIssueText(Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.black));
        mPaint.setStrokeWidth(4);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTextSize(convertDp(14));
        canvas.drawText(String.valueOf(mIssues),
                getWidth() / 2.0f,
                getHeight() - convertDp(7),
                mPaint);
    }

    protected void onDrawForegroundPercent(Canvas canvas) {
        for (CircleChartIssueItem issueItem : mCircleChartHolders) {
            mPaint.setColor(issueItem.getForegroundColor());
            mPaint.setStyle(Paint.Style.STROKE);
            float stroke = getWidth() / 2;
            mPaint.setStrokeWidth(stroke);
            float margin = convertDp(8);

            // smooth
            mPaint.setAntiAlias(true);

            mOval = new RectF(stroke / 2 + margin,
                    stroke / 2 + margin,
                    getWidth() - stroke / 2 - margin,
                    getHeight() * 2 - stroke / 2 - margin);
            canvas.drawArc(mOval, START_ANGLE_POINT, mAngleDraw * issueItem.getPercent() / 100, false, mPaint);
        }
    }
}