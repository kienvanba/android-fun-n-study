package com.horical.gito.mvp.myProject.task.details.fragment.overview.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IView;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.view.IOverView;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class OverViewPresenter extends BasePresenter implements IOverViewPresenter {

    private static final String TAG = makeLogTag(OverViewPresenter.class);

    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IOverView getView() {
        return ((IOverView) getIView());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

}
