package com.horical.gito.mvp.myProject.task.creating.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by nhattruong251295 on 3/28/2017.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserHolder>{

    private Context mContext;
    private List<String> mList;
    private OnItemClickListener mOnItemClickListener;

    public UserAdapter(Context context, List<String> list) {
        mContext = context;
        mList = list;
    }

    public List<String> getList() {
        return mList;
    }

    public void setList(List<String> mList) {
        this.mList = mList;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_user_new_survey, parent, false);
        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, final int position) {
        holder.tvName.setText(mList.get(position));
      /*  holder.tvJob.setText(mList.get(position).getLevel().getName());
        holder.tvShortName.setText(mList.get(position).getDiaplayName().substring(0, 1));*/
        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mList.remove(position);
                notifyDataSetChanged();
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* mList.get(position).setSelected(!mList.get(position).isSelected());
                notifyItemChanged(position);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class UserHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_shortname_user)
        TextView tvShortName;
        @Bind(R.id.img_avatar_user)
        CircleImageView imvAvatar;
        @Bind(R.id.tv_name_user)
        TextView tvName;
        @Bind(R.id.tv_job_user)
        TextView tvJob;
        @Bind(R.id.ll_delete_user)
        LinearLayout llDelete;
        View view;

        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}
