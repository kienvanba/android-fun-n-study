package com.horical.gito.mvp.myProject.task.creating.view;

import com.horical.gito.base.IView;

public interface ITaskCreateNewView extends IView {
    void showLoading();

    void hideLoading();

    void createTaskSuccess();

    void createTaskFailure(String error);
}
