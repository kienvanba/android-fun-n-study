package com.horical.gito.mvp.meeting.detail.selectRoom.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.room.GetAllRoomResponse;
import com.horical.gito.model.Room;
import com.horical.gito.mvp.meeting.detail.selectRoom.view.IRoomView;

import java.util.ArrayList;
import java.util.List;


public class RoomPresenter extends BasePresenter implements IRoomPresenter {

    @Override
    public void attachView(IView view) {
        super.attachView(view);
    }

    private List<Room> mList = new ArrayList<>();
    private List<Room> searchRoom = new ArrayList<>();

    private IRoomView getView() {
        return (IRoomView) getIView();
    }

    public List<Room> getSearchRoom() {
        return searchRoom;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void detachView() {
        super.detachView();
        getEventManager().unRegister(this);
    }

    public void setListRoom(List<Room> list) {
        mList.clear();
        searchRoom.addAll(list);
        mList.addAll(list);
    }

    public void setCheckAll(boolean check) {
        for (Room item : searchRoom) {
            item.setSelected(check);
        }
    }

    public List<Room> getListRoom() {
        return mList;
    }

    @Override
    public void getAllRoomBooking() {
        getApiManager().getAllRoom(new ApiCallback<GetAllRoomResponse>() {
            @Override
            public void success(GetAllRoomResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.room);
                getView().getAllRoomBookingSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getAllRoomBookingFailure(error.message);
            }
        });
    }

    @Override
    public List<Room> getListSearchRoom(String keyword) {
        searchRoom.clear();
        if (keyword != null && keyword.trim().length() > 0) {
            for (Room myRoom : mList) {
                if (myRoom.getName() != null
                        && myRoom.getName().toLowerCase().contains(keyword.toLowerCase())) {
                    searchRoom.add(myRoom);
                }
            }
        } else {
            searchRoom.addAll(mList);
        }
        return searchRoom;
    }
}
