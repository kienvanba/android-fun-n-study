package com.horical.gito.interactor.api.response.group;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Group;

import java.util.List;

public class GetAllGroupResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Group> groups;
}
