package com.horical.gito.mvp.roomBooking.list.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.meeting.list.view.MeetingActivity;
import com.horical.gito.mvp.roomBooking.detail.view.RoomDetailActivity;
import com.horical.gito.mvp.roomBooking.list.adapter.RoomBookingAdapter;
import com.horical.gito.mvp.roomBooking.list.injection.component.DaggerRoomBookingComponent;
import com.horical.gito.mvp.roomBooking.list.injection.component.RoomBookingComponent;
import com.horical.gito.mvp.roomBooking.list.injection.module.RoomBookingModule;
import com.horical.gito.mvp.roomBooking.list.presenter.RoomBookingPresenter;
import com.horical.gito.mvp.roomBooking.newRoom.view.NewRoomActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RoomBookingActivity extends DrawerActivity implements IRoomBookingView, View.OnClickListener {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.rb_rv_list_room)
    RecyclerView mRvListRoom;

    @Bind(R.id.rbt_btn_meeting)
    RadioButton mBtnMeeting;

    @Bind(R.id.room_booking_refresh_layout)
    SwipeRefreshLayout mRefresh;

    @Bind(R.id.rbt_btn_roombooking)
    RadioButton mBtnRoomBooking;

    RoomBookingAdapter mRoomBookingAdapter;

    @Inject
    RoomBookingPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_room_booking;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_MEETING;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RoomBookingComponent component = DaggerRoomBookingComponent.builder()
                .roomBookingModule(new RoomBookingModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(getString(R.string.room_booking));
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);

        mRoomBookingAdapter = new RoomBookingAdapter(this, mPresenter.getListRoomBooking());
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRvListRoom.setLayoutManager(llm);
        mRvListRoom.setAdapter(mRoomBookingAdapter);

        showLoading();
        mPresenter.getAllRoomBooking();

        mRoomBookingAdapter.setOnItemClickListener(new RoomBookingAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        });

        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                mPresenter.getAllRoomBooking();
            }
        });

    }

    protected void initListener() {
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                openDrawer();
            }

            @Override
            public void onRightButtonOneClicked() {
                Intent intent = new Intent(RoomBookingActivity.this, NewRoomActivity.class);
                startActivity(intent);
            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });

        mRoomBookingAdapter.setOnItemClickListener(new RoomBookingAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                Intent intent = new Intent(RoomBookingActivity.this, RoomDetailActivity.class);
                startActivity(intent);
            }
        });

        mBtnMeeting.setOnClickListener(this);
        mBtnRoomBooking.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rbt_btn_meeting: {
                mBtnMeeting.setSelected(true);
                mBtnRoomBooking.setSelected(false);
                Intent intent = new Intent(RoomBookingActivity.this, MeetingActivity.class);
                startActivity(intent);
                break;
            }

        }
    }

    @Override
    public void showLoading() {
        showDialog(R.string.loading);
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllRoomBookingSuccess() {
        dismissDialog();
        mRefresh.setRefreshing(false);
        mRoomBookingAdapter.notifyDataSetChanged();
        showToast(getString(R.string.success));
    }

    @Override
    public void getAllRoomBookingFailure(String err) {
        dismissDialog();
        mRefresh.setRefreshing(false);
        showToast(getString(R.string.fail));

    }
}
