package com.horical.gito.mvp.roomBooking.newRoom.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.roomBooking.newRoom.injection.module.NewRoomModule;
import com.horical.gito.mvp.roomBooking.newRoom.view.NewRoomActivity;

import dagger.Component;

/**
 * Created by Lemon on 3/27/2017.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = NewRoomModule.class)
public interface NewRoomComponent {
    void inject(NewRoomActivity activity);
}
