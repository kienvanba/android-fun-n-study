package com.horical.gito.mvp.myProject.setting.fragment.activity.member.presenter;

import android.util.Log;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.companySetting.GetAllProjectRoleResponse;
import com.horical.gito.interactor.api.response.project.GetMemberAndRoleResponse;
import com.horical.gito.interactor.api.response.user.GetAllUserResponse;
import com.horical.gito.model.MemberAndRole;
import com.horical.gito.model.RoleProject;
import com.horical.gito.model.User;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.adapter.RoleProjectItemBody;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.adapter.UserItemBody;
import com.horical.gito.mvp.myProject.setting.fragment.activity.member.view.INewMemberView;

import java.util.ArrayList;
import java.util.List;

import static com.horical.gito.utils.LogUtils.LOGE;

public class NewMemberPresenter extends BasePresenter implements INewMemberPresenter {
    private static final String TAG = NewMemberPresenter.class.getSimpleName();


    private final String projectId = GitOStorage.getInstance().getProject().getId();

    private List<IRecyclerItem> users, roles;
    private List<MemberAndRole> memberAndRoles;

    private INewMemberView getView(){
        return (INewMemberView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        users = new ArrayList<>();
        roles = new ArrayList<>();
        memberAndRoles = new ArrayList<>();

        getEventManager().register(this);
    }

    public List<IRecyclerItem> getUsers(){
        return users;
    }
    public List<IRecyclerItem> getRoles(){
        return roles;
    }

    public void getAllUsers(){
        if(!isViewAttached())return;

        users.clear();
        getView().showLoading();

        getMemberAndRole();

        getApiManager().getAllUser(new ApiCallback<GetAllUserResponse>() {
            @Override
            public void success(GetAllUserResponse res) {
                if(!res.getUsers().isEmpty()){
                    for(User user : res.getUsers()){
                        for(MemberAndRole mar: memberAndRoles){
                            if(!user.get_id().equals(mar.getUserId().get_id())){
                                users.add(new UserItemBody(user));
                            }
                        }
                    }
                }
                getView().getAllUserSuccess();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                Log.e(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    public void getAllRoles(){
        if(!isViewAttached())return;

        roles.clear();
        getView().showLoading();
        getApiManager().getAllProjectRole(new ApiCallback<GetAllProjectRoleResponse>() {
            @Override
            public void success(GetAllProjectRoleResponse res) {
                if(res.roleProjects!=null){
                    for(RoleProject roleProject:res.roleProjects){
                        roles.add(new RoleProjectItemBody(roleProject));
                    }
                }
                getView().getAllRoleSuccess();
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                Log.e(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    private void getMemberAndRole(){
        if (!isViewAttached()) return;
        getView().showLoading();

        memberAndRoles.clear();
        getApiManager().getMemberAndRole(projectId, new ApiCallback<GetMemberAndRoleResponse>() {
            @Override
            public void success(GetMemberAndRoleResponse res) {
                if (!res.memberAndRoles.isEmpty() && res.memberAndRoles!=null) {
                    memberAndRoles.addAll(res.memberAndRoles);
                }
                getView().hideLoading();
            }

            @Override
            public void failure(RestError error) {
                LOGE(TAG, error.message);
                getView().hideLoading();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}
