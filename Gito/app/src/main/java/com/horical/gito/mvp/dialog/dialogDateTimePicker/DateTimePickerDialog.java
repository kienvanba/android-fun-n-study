package com.horical.gito.mvp.dialog.dialogDateTimePicker;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.horical.gito.R;
import com.horical.gito.utils.HDate;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Created by Dragonoid on 12/9/2016.
 */
public class DateTimePickerDialog extends Dialog implements DatePicker.OnDateChangedListener, TimePicker.OnTimeChangedListener, View.OnClickListener {
    @Bind(R.id.leaving_dp_from)
    DatePicker mDateFrom;

    @Bind(R.id.leaving_tp_from)
    TimePicker mTimeFrom;

    @Bind(R.id.leaving_dp_to)
    DatePicker mDateTo;

    @Bind(R.id.leaving_tp_to)
    TimePicker mTimeTo;

    @Bind(R.id.tv_cancel)
    TextView tvCancel;

    @Bind(R.id.tv_done)
    TextView tvDone;

    private OnClickDoneListener mCallback;

    public DateTimePickerDialog(final Context context, OnClickDoneListener callback) {
        super(context);
        mCallback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.dialog_datetime_picker);
        ButterKnife.bind(this);
        getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);

        initData();
        initListener();
    }

    private void initListener() {
        mTimeFrom.setOnTimeChangedListener(this);
        mTimeTo.setOnTimeChangedListener(this);
        tvCancel.setOnClickListener(this);
        tvDone.setOnClickListener(this);
    }

    private void initData() {
        Calendar c = Calendar.getInstance();
        mDateFrom.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), this);
        mDateTo.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), this);
    }

    public HDate getFrom() {
        HDate DateTime = new HDate(
                mDateFrom.getYear(), mDateFrom.getMonth(), mDateFrom.getDayOfMonth(),
                mTimeFrom.getCurrentHour(), mTimeFrom.getCurrentMinute(), 0);
        return DateTime;
    }

    public void setFrom(HDate DateTime) {
        mDateFrom.updateDate(DateTime.year, DateTime.month, DateTime.day);
        mTimeFrom.setCurrentHour(DateTime.hour);
        mTimeFrom.setCurrentMinute(DateTime.minute);
    }

    public HDate getTo() {
        HDate DateTime = new HDate(
                mDateTo.getYear(), mDateTo.getMonth(), mDateTo.getDayOfMonth(),
                mTimeTo.getCurrentHour(), mTimeTo.getCurrentMinute(), 0);
        return DateTime;
    }

    public void setTo(HDate DateTime) {
        mDateTo.updateDate(DateTime.year, DateTime.month, DateTime.day);
        mTimeTo.setCurrentHour(DateTime.hour);
        mTimeTo.setCurrentMinute(DateTime.minute);
    }

    private boolean isSmallerDate(HDate var1, HDate var2) {
        if (!var1.year.equals(var2.year)) {
            return var1.year < var2.year;
        }
        if (!var1.month.equals(var2.month)) {
            return var1.month < var2.month;
        }
        if (!var1.day.equals(var2.day)) {
            return var1.day < var2.day;
        }
        return false;
    }

    private boolean isSmallerTime(HDate var1, HDate var2) {
        if (!var1.hour.equals(var2.hour)) return var1.hour < var2.hour;
        if (var1.minute.equals(var2.minute)) return var1.minute < var2.minute;
        else return false;
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (view.getId() == mDateFrom.getId()) {
            if (isSmallerDate(getTo(), getFrom())) setTo(getFrom());
            return;
        }
        if (view.getId() == mDateTo.getId()) {
            if (isSmallerDate(getTo(), getFrom())) setFrom(getTo());
            return;
        }
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        if (!isSmallerDate(getFrom(), getTo()) && !isSmallerDate(getTo(), getFrom())) {
            if (view.getId() == mTimeFrom.getId()) {
                if (isSmallerTime(getTo(), getFrom())) setTo(getFrom());
                return;
            }
            if (view.getId() == mTimeTo.getId()) {
                if (isSmallerTime(getTo(), getFrom())) setFrom(getTo());
                return;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_done:
                mCallback.onDoneClick(getFrom(), getTo());
                dismiss();
                break;
        }
    }

    public interface OnClickDoneListener {
        void onDoneClick(HDate DateTimeFrom, HDate DateTimeTo);
    }
}
