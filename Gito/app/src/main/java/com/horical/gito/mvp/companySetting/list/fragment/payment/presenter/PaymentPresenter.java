package com.horical.gito.mvp.companySetting.list.fragment.payment.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.companySetting.GetAllCardResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllHistoryResponse;
import com.horical.gito.model.Card;
import com.horical.gito.model.History;
import com.horical.gito.mvp.companySetting.list.fragment.payment.view.IPaymentView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class PaymentPresenter extends BasePresenter implements IPaymentPresenter {
    private List<History> mListHistory;
    private List<Card> mListCard;

    public void attachView(IPaymentView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IPaymentView getView() {
        return (IPaymentView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mListHistory = new ArrayList<>();
        mListCard = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<History> getListHistory() {
        return mListHistory;
    }

    public void setListHistory(List<History> mListHistory) {
        this.mListHistory = mListHistory;
    }

    public List<Card> getListCard() {
        return mListCard;
    }

    public void setListCard(List<Card> mListCard) {
        this.mListCard = mListCard;
    }

    @Override
    public void getAllHistory() {
        getApiManager().getAllHistory(new ApiCallback<GetAllHistoryResponse>() {
            @Override
            public void success(GetAllHistoryResponse res) {
                if(mListHistory != null){
                    mListHistory.clear();
                } else {
                    mListHistory = new ArrayList<History>();
                }
                mListHistory.addAll(res.histories);
                getAllCard();
            }

            @Override
            public void failure(RestError error) {
                getView().getAllHistoryFailure(error.message);
            }
        });
    }

    @Override
    public void getAllCard() {
        getApiManager().getAllCard(new ApiCallback<GetAllCardResponse>() {
            @Override
            public void success(GetAllCardResponse res) {
                if(mListCard != null){
                    mListCard.clear();
                } else {
                    mListCard = new ArrayList<Card>();
                }
                mListCard.addAll(res.cards);
                getView().getAllCardSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getAllCardFailure(error.message);
            }
        });
    }
}

