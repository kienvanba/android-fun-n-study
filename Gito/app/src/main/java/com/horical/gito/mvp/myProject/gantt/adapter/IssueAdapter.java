package com.horical.gito.mvp.myProject.gantt.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class IssueAdapter extends BaseRecyclerAdapter {
    private LayoutInflater inflater;

    public IssueAdapter(List<IRecyclerItem> items, Context context) {
        super(items, context);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = inflater.inflate(R.layout.item_gantt_issue, parent, false);
        return new IssueHolder(item);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        IssueItem item = (IssueItem) mItems.get(holder.getAdapterPosition());
        IssueHolder issueHolder = (IssueHolder) holder;
        issueHolder.tvIssue.setText(item.issue.name);

    }

    class IssueHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_issue)
        GitOTextView tvIssue;

        public IssueHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
