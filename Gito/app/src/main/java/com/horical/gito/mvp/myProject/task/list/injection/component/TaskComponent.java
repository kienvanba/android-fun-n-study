package com.horical.gito.mvp.myProject.task.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.task.list.injection.module.TaskModule;
import com.horical.gito.mvp.myProject.task.list.view.TaskActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = TaskModule.class)
public interface TaskComponent {
    void inject(TaskActivity activity);
}
