package com.horical.gito.interactor.api.request.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kut3b on 11/04/2017.
 */

public class CreateMemberRequest {
    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("password")
    @Expose
    public String password;

    @SerializedName("roleCompany")
    @Expose
    public String roleCompany;

    @SerializedName("displayName")
    @Expose
    public String displayName;

    @SerializedName("level")
    @Expose
    public String level;

    @SerializedName("department")
    @Expose
    public String department;
}
