package com.horical.gito.mvp.roomBooking.detail.fragment.day.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.roomBooking.detail.fragment.day.adapter.DayItem;
import com.horical.gito.mvp.roomBooking.detail.fragment.day.view.IDayView;

import java.util.ArrayList;
import java.util.List;

public class DayPresenter extends BasePresenter implements IDayPresenter {
    private List<DayItem> mListDayItem;

    public void attachView(IDayView view) {
        super.attachView(view);
    }

    public IDayView getView() {
        return (IDayView) getIView();
    }

    public List<DayItem> getListDay(){
        return mListDayItem;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mListDayItem = new ArrayList<>();
        getEventManager().register(this);
        initData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public void initData(){
        for (int i = 0; i < 9; i++){
            mListDayItem.add(new DayItem());
        }
    }
}
