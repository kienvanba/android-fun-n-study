package com.horical.gito.mvp.myProject.task.creating.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.task.creating.injection.module.TaskCreateNewModule;
import com.horical.gito.mvp.myProject.task.creating.view.TaskCreateNewActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = TaskCreateNewModule.class)
public interface TaskCreateNewComponent {
    void inject(TaskCreateNewActivity activity);
}
