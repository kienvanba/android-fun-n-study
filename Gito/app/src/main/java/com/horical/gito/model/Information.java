package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by thanhle on 3/20/17.
 */

public class Information implements Serializable {

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("timeZone")
    @Expose
    private int timeZone;

    @SerializedName("gitId")
    @Expose
    private int gitId;

    @SerializedName("avatar")
    @Expose
    private TokenFile avatar;

    @SerializedName("quotaEstate")
    @Expose
    private int quotaEstate;

    @SerializedName("yearlyHoliday")
    @Expose
    private int yearlyHoliday;

    @SerializedName("monthlyHoliday")
    @Expose
    private int monthlyHoliday;

    @SerializedName("workingDay")
    @Expose
    private int workingDay;

    @SerializedName("startOfWeekDay")
    @Expose
    private int startOfWeekDay;

    @SerializedName("workingDayWeek")
    @Expose
    private WorkingDayWeek workingDayWeek;

    @SerializedName("eveningEndTime")
    @Expose
    private Date eveningEndTime;

    @SerializedName("eveningStartTime")
    @Expose
    private Date eveningStartTime;

    @SerializedName("afternoonEndTime")
    @Expose
    private Date afternoonEndTime;

    @SerializedName("afternoonStartTime")
    @Expose
    private Date afternoonStartTime;

    @SerializedName("morningEndTime")
    @Expose
    private Date morningEndTime;

    @SerializedName("morningStartTime")
    @Expose
    private Date morningStartTime;

    @SerializedName("language")
    @Expose
    private String language;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("fax")
    @Expose
    private String fax;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("domain")
    @Expose
    private String domain;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("companyName")
    @Expose
    private String companyName;

    @SerializedName("companyId")
    @Expose
    private String companyId;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(int timeZone) {
        this.timeZone = timeZone;
    }

    public int getGitId() {
        return gitId;
    }

    public void setGitId(int gitId) {
        this.gitId = gitId;
    }

    public int getQuotaEstate() {
        return quotaEstate;
    }

    public void setQuotaEstate(int quotaEstate) {
        this.quotaEstate = quotaEstate;
    }

    public int getYearlyHoliday() {
        return yearlyHoliday;
    }

    public void setYearlyHoliday(int yearlyHoliday) {
        this.yearlyHoliday = yearlyHoliday;
    }

    public int getMonthlyHoliday() {
        return monthlyHoliday;
    }

    public void setMonthlyHoliday(int monthlyHoliday) {
        this.monthlyHoliday = monthlyHoliday;
    }

    public int getWorkingDay() {
        return workingDay;
    }

    public void setWorkingDay(int workingDay) {
        this.workingDay = workingDay;
    }

    public int getStartOfWeekDay() {
        return startOfWeekDay;
    }

    public void setStartOfWeekDay(int startOfWeekDay) {
        this.startOfWeekDay = startOfWeekDay;
    }

    public WorkingDayWeek getWorkingDayWeek() {
        return workingDayWeek;
    }

    public void setWorkingDayWeek(WorkingDayWeek workingDayWeek) {
        this.workingDayWeek = workingDayWeek;
    }

    public TokenFile getAvatar() {
        return avatar;
    }

    public void setAvatar(TokenFile avatar) {
        this.avatar = avatar;
    }

    public Date getEveningEndTime() {
        return eveningEndTime;
    }

    public void setEveningEndTime(Date eveningEndTime) {
        this.eveningEndTime = eveningEndTime;
    }

    public Date getEveningStartTime() {
        return eveningStartTime;
    }

    public void setEveningStartTime(Date eveningStartTime) {
        this.eveningStartTime = eveningStartTime;
    }

    public Date getAfternoonEndTime() {
        return afternoonEndTime;
    }

    public void setAfternoonEndTime(Date afternoonEndTime) {
        this.afternoonEndTime = afternoonEndTime;
    }

    public Date getAfternoonStartTime() {
        return afternoonStartTime;
    }

    public void setAfternoonStartTime(Date afternoonStartTime) {
        this.afternoonStartTime = afternoonStartTime;
    }

    public Date getMorningEndTime() {
        return morningEndTime;
    }

    public void setMorningEndTime(Date morningEndTime) {
        this.morningEndTime = morningEndTime;
    }

    public Date getMorningStartTime() {
        return morningStartTime;
    }

    public void setMorningStartTime(Date morningStartTime) {
        this.morningStartTime = morningStartTime;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
