package com.horical.gito.mvp.leaving.main.presenter;

import android.content.res.Resources;
import android.util.Log;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.base.BasePresenter;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.leaving.GetAllLeavingResponse;
import com.horical.gito.interactor.api.response.leaving.GetLeavingByIdResponse;
import com.horical.gito.model.Leaving;
import com.horical.gito.model.User;
import com.horical.gito.mvp.leaving.main.adapter.LeavingMonthDetailItem;
import com.horical.gito.mvp.leaving.main.adapter.LeavingMonthInfoItem;
import com.horical.gito.mvp.leaving.main.adapter.LeavingPersonItem;
import com.horical.gito.mvp.leaving.main.view.ILeavingView;
import com.horical.gito.utils.HDate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dragonoid on 12/7/2016.
 */

public class LeavingPresenter extends BasePresenter implements ILeavingPresenter {
    private List<LeavingMonthInfoItem> mLeavingMonthList = new ArrayList<>();
    private List<User> mLeavingPersonList = new ArrayList<>();
    private List<Leaving> mLeavingGetAll = new ArrayList<>();
    private List<Leaving> mLeavingById = new ArrayList<>();
    private List<String> mLeavingFileList = new ArrayList<>();
    Resources mResources;
    private int from;

    public ILeavingView getView() {
        return (ILeavingView) super.getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        initData();
        getData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().register(this);
    }

    public List<LeavingMonthInfoItem> getMonthList() {
        return mLeavingMonthList;
    }

    public List<User> getPersonList() {
        return mLeavingPersonList;
    }

    public List<String> getFileList() {
        return mLeavingFileList;
    }

    private void initData() {
        for (int i = 1; i <= 12; i++) {
            HDate month = new HDate(0, i, 0);
            mLeavingMonthList.add(new LeavingMonthInfoItem(month, 0, new ArrayList<IRecyclerItem>()));
        }
    }

    private void getData() {
        //getAllLeaving();
        filterLeavingById(MainApplication.getAppComponent().getPreferManager().getUser().get_id());
    }

    public void generateTestData() {
//        List<IRecyclerItem> mChild1 = new ArrayList<>();
//        mChild1.add(new LeavingMonthDetailItem("Approved", "Reason 1", new HDate(4, 2, 6)));
//        mChild1.add(new LeavingMonthDetailItem("Not Approved", "", new HDate(5, 2, 6)));
//        mChild1.add(new LeavingMonthDetailItem("Approved", "Reason 3", new HDate(6, 2, 6)));
//
//        mLeavingMonthList.add(new LeavingMonthInfoItem(new HDate(1, 2, 1), 10, mChild1));
//
//        List<IRecyclerItem> mChild2 = new ArrayList<>();
//        mLeavingMonthList.add(new LeavingMonthInfoItem(new HDate(1, 3, 1), 26, mChild2));
//
//        List<IRecyclerItem> mChild3 = new ArrayList<>();
//        mChild3.add(new LeavingMonthDetailItem("Approved", "", new HDate(3, 4, 6)));
//        mChild1.add(new LeavingMonthDetailItem("Approved", "", new HDate(5, 4, 6)));
//        mLeavingMonthList.add(new LeavingMonthInfoItem(new HDate(1, 4, 1), 25, mChild3));
        //------------------------------------------------------------------------------------
//        User test = new User();
//        test.setTestData("Name 1","position 1");
//        mLeavingPersonList.add(test);
        //-------------------------------------------------------------------------------------
//        mLeavingFileList.add("File1.rar");
//        mLeavingFileList.add("File2.rar");
//        mLeavingFileList.add("File3.rar");
    }

    public void setResources(Resources resource) {
        mResources = resource;
    }

    //    public List filterLeavingById(String index){
//        List<Leaving> result = new ArrayList();
//        for (Leaving item: mLeavingGetAll ) {
//            if(item.getCreatorId().id.equals(index)){
//                HDate date = new HDate();
//                date.setFromString(item.getCreatedDate());
//                LeavingMonthInfoItem month = mLeavingMonthList.get(date.month);
//                String status;
//                switch (item.getStatus()){
//                    case 2: status="Rejected"; break;
//                    case 1: status="Approved"; break;
//                    default:status="Pending"; break;
//                }
//                month.getList().add(new LeavingMonthDetailItem(status,item.getReason(),date));
//            }
//        }
//        return result;
//    }
    public List filterLeavingById(String userId) {
        List<Leaving> result = new ArrayList();
        getLeavingById(userId);
        for (Leaving item : mLeavingById) {
            HDate date = new HDate();
            date.setFromString(item.getCreatedDate().toString());
            LeavingMonthInfoItem month = mLeavingMonthList.get(date.month);
            String status;
            switch (item.getStatus()) {
                case 2:
                    status = "Rejected";
                    break;
                case 1:
                    status = "Approved";
                    break;
                case 0:
                    status = "Submitted";
                    break;
                default:
                    status = "Pending";
                    break;
            }
            month.getList().add(new LeavingMonthDetailItem(status, item.getReason(), date));
        }
        return result;
    }

    public void getAllLeaving() {
        String token = AppConstants.BEGIN_TOKEN
                + getPreferManager().getToken().substring(8, getPreferManager().getToken().length() - 8)
                + AppConstants.END_TOKEN;
        Log.d("TAG", token);
        getApiManager().getAllLeaving(new ApiCallback<GetAllLeavingResponse>() {
            @Override
            public void success(GetAllLeavingResponse res) {
                if (mLeavingGetAll != null) {
                    mLeavingGetAll.clear();
                } else {
                    mLeavingGetAll = new ArrayList<>();
                }
                mLeavingGetAll.addAll(res.leavings);
                getView().getAllLeavingSuccess();
            }

            @Override
            public void failure(RestError error) {
                if (mLeavingGetAll != null) {
                    mLeavingGetAll.clear();
                } else {
                    mLeavingGetAll = new ArrayList<>();
                }
                getView().getAllLeavingFailure(error.message);
            }
        });
    }

    public void getLeavingById(String userId) {
        getApiManager().getLeavingById(userId, new ApiCallback<GetLeavingByIdResponse>() {
            @Override
            public void success(GetLeavingByIdResponse res) {
                if (mLeavingById != null) {
                    mLeavingById.clear();
                } else {
                    mLeavingById = new ArrayList<>();
                }
                mLeavingById.addAll(res.leavingsById);
                getView().getLeavingByIdSuccess();
            }

            @Override
            public void failure(RestError error) {
                if (mLeavingById != null) {
                    mLeavingById.clear();
                } else {
                    mLeavingById = new ArrayList<>();
                }
                getView().getLeavingByIdFailure(error.message);
            }
        });
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }
}
