package com.horical.gito.interactor.api.response.estate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.estate.Estate;

import java.util.List;

/**
 * Created by hoangsang on 3/14/17
 */

public class GetAllEstatesResponse extends BaseResponse {

    @SerializedName("results")
    @Expose
    public List<Estate> estateList;
}
