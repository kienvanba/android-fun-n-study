package com.horical.gito.mvp.salary.dto;

public class SalaryHeaderDto {
    private String name;

    public SalaryHeaderDto() {
    }

    public SalaryHeaderDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
