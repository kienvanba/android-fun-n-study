package com.horical.gito.mvp.salary.list.fragments.subEmail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.salary.dto.emailForm.EmailFormDto;
import com.horical.gito.mvp.salary.list.fragments.subEmail.view.IListEmailFormFragmentView;

import java.util.ArrayList;
import java.util.List;

public class ListEmailFormFragmentPresenter extends BasePresenter implements IEmailFormFragmentPresenter {

    public void attachView(IListEmailFormFragmentView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public IListEmailFormFragmentView getView() {
        return (IListEmailFormFragmentView) getIView();
    }

    private List<EmailFormDto> mEmailForms;

    private void setDummyData() {
        getEmailForms().add(new EmailFormDto("Form a"));
        getEmailForms().add(new EmailFormDto("Form b"));
        getEmailForms().add(new EmailFormDto("Form c"));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        setDummyData();
    }

    @Override
    public List<EmailFormDto> getEmailForms() {
        if (mEmailForms == null) {
            mEmailForms = new ArrayList<>();
        }
        return mEmailForms;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

}
