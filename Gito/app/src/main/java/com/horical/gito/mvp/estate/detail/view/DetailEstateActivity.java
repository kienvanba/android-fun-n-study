package com.horical.gito.mvp.estate.detail.view;

import android.app.DatePickerDialog;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.estate.Estate;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.estate.detail.adapter.FragmentPaggerAdapter;
import com.horical.gito.mvp.estate.detail.injection.component.DaggerDetailEstateComponent;
import com.horical.gito.mvp.estate.detail.injection.component.DetailEstateComponent;
import com.horical.gito.mvp.estate.detail.injection.module.DetailEstateModule;
import com.horical.gito.mvp.estate.detail.presenter.DetailEstatePresenter;
import com.horical.gito.mvp.estate.list.view.IEstateView;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.Bind;

public class DetailEstateActivity extends DrawerActivity implements DetaiEstateListener {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.fragment_pagger)
    ViewPager fragmentPagger;

    @Bind(R.id.ln_indicators)
    LinearLayout lnIndicators;

    private Estate mEstate;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_detail_estate;
    }

    @Override
    protected int getNavId() {
        return 0;
    }

    @Inject
    DetailEstatePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DetailEstateComponent component = DaggerDetailEstateComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .detailEstateModule(new DetailEstateModule())
                .build();
        component.inject(this);

        initData();
        initListener();
    }

    protected void initData() {

        if (getIntent().hasExtra(IEstateView.KEY_ESTATE)) {
            mEstate = (Estate) getIntent().getSerializableExtra(IEstateView.KEY_ESTATE);
            Log.d("ESTATE : ", new Gson().toJson(mEstate));
        }

        fragmentPagger.setAdapter(new FragmentPaggerAdapter(getSupportFragmentManager()));
        initIndicators(fragmentPagger.getAdapter().getCount());
    }

    private void initIndicators(int count) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 0, 0);
        for (int i = 0; i < count; i++) {
            View indicator = new ImageView(getApplicationContext());
            indicator.setLayoutParams(layoutParams);

            if (i == 0)
                indicator.setBackground(getResources().getDrawable(R.drawable.ic_circle_selected));
            else
                indicator.setBackground(getResources().getDrawable(R.drawable.ic_circle_normal));

            lnIndicators.addView(indicator);
        }
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                finish();
            }

            @Override
            public void onRightClicked() {

            }
        });

        fragmentPagger.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateIndicators(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void updateIndicators(int position) {
        lnIndicators.removeAllViews();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 0, 0);

        for (int i = 0; i < fragmentPagger.getAdapter().getCount(); i++) {
            View indicator = new ImageView(getApplicationContext());
            indicator.setLayoutParams(layoutParams);

            if (i == position)
                indicator.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_circle_selected));
            else
                indicator.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_circle_normal));

            lnIndicators.addView(indicator);
        }
    }

    @Override
    public void enableCurrentView(int editTextId, int imageViewId) {
        View editView = this.findViewById(editTextId);
        View imageView = this.findViewById(imageViewId);

        if (editView != null)
            editView.setEnabled(true);
        if (imageView != null)
            imageView.setVisibility(View.GONE);
    }

    @Override
    public void disableCurrentView(int editTextId, int imageViewId) {
        View editView = this.findViewById(editTextId);
        View imageView = this.findViewById(imageViewId);

        if (editView != null && !(editView instanceof TextView)) {
            editView.setEnabled(false);
        }

        if (imageView != null) {
            imageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showDialogPicker(Date currentDate, final DialogPickerCallBack callBack) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        final int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        int mMonth = calendar.get(Calendar.MONTH);
        int mYear = calendar.get(Calendar.YEAR);

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                callBack.success(calendar.getTime());
            }
        };

        DatePickerDialog datePickerDialog = new DatePickerDialog(DetailEstateActivity.this, listener, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    @Override
    public Estate getEstate() {
        return mEstate;
    }
}
