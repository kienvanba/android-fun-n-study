package com.horical.gito.mvp.salary.bonus.list.fragments.mine.presenter;

import com.horical.gito.model.InputAddon;

import java.util.List;

public interface IMineFragmentPresenter {
    List<InputAddon> getListInputAddon();

    void requestGetMineInputAddon();
}
