package com.horical.gito.interactor.api.response.room;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Room;

/**
 * Created by Lemon on 4/4/2017.
 */

public class GetCreateRoomResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    public Room room;
}
