package com.horical.gito.mvp.myProject.document.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.document.injection.module.DocumentModule;
import com.horical.gito.mvp.myProject.document.view.DocumentActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = DocumentModule.class)
public interface DocumentComponent {
    void inject(DocumentActivity activity);
}
