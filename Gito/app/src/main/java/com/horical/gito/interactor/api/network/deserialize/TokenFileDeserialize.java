package com.horical.gito.interactor.api.network.deserialize;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.horical.gito.model.TokenFile;
import com.horical.gito.utils.GsonUtils;

import java.lang.reflect.Type;

/**
 * Created by thanhle on 3/30/17.
 */

public class TokenFileDeserialize implements JsonDeserializer<TokenFile> {

    @Override
    public TokenFile deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        TokenFile tokenFile;
        try {
            tokenFile = GsonUtils.createGson(TokenFile.class).fromJson(json, TokenFile.class);
        } catch (Exception ex) {
            tokenFile = new TokenFile();
            tokenFile.setId(json.getAsString());
        }
        return tokenFile;
    }
}
