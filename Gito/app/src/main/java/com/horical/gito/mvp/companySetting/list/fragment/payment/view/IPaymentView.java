package com.horical.gito.mvp.companySetting.list.fragment.payment.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IPaymentView extends IView {
    void showLoading();

    void hideLoading();

    void getAllHistorySuccess();

    void getAllHistoryFailure(String err);

    void getAllCardSuccess();

    void getAllCardFailure(String err);
}
