package com.horical.gito.mvp.accounting.list.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.model.Accounting;

/**
 * Created by Luong on 06-Mar-17.
 */

public class AccountingDetailItem implements IRecyclerItem {
    public Accounting mAccounting;

    public AccountingDetailItem (Accounting accounting)
    {
        this.mAccounting = accounting;
    }

    public Accounting getAccounting()
    {
        return mAccounting;
    }
    public void setAccounting(Accounting accounting)
    {
        mAccounting = accounting;
    }
    @Override
    public int getItemViewType() {
        return AppConstants.ACCOUNTING_ITEM;
    }
}
