package com.horical.gito.mvp.leaving.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.base.BaseRecyclerAdapter;
import com.horical.gito.base.IRecyclerItem;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 12/6/2016.
 */

public class LeavingMonthDetailAdapter extends BaseRecyclerAdapter{
    public LeavingMonthDetailAdapter (List<IRecyclerItem> items,Context context) {
        super(items,context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leaving_month_detail,parent,false);
        return new LeavingMonthDetailHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        LeavingMonthDetailHolder detailHolder = (LeavingMonthDetailHolder) holder;
        LeavingMonthDetailItem detailItem = (LeavingMonthDetailItem) mItems.get(position);
        detailHolder.mDate.setText(detailItem.getStringDateLeaving());
        if(detailItem.mReason!="") detailHolder.mReason.setText(detailItem.mReason);
        detailHolder.mStatus.setText(detailItem.mStatus);
        if(detailItem.mStatus == "Rejected")
            detailHolder.mStatus.setTextColor(detailHolder.mStatus.getResources().getColor(R.color.red));
        else if(detailItem.mStatus == "Approved")
            detailHolder.mStatus.setTextColor(detailHolder.mStatus.getResources().getColor(R.color.green));

    }
    public class LeavingMonthDetailHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.leaving_tv_month_detail_reason)
        TextView mReason;
        @Bind(R.id.leaving_tv_month_detail_day)
        TextView mDate;
        @Bind(R.id.leaving_tv_month_detail_status)
        TextView mStatus;
        public LeavingMonthDetailHolder (View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }
}
