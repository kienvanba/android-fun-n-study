package com.horical.gito.mvp.allProject.detail.view;

import com.horical.gito.base.IView;
import com.horical.gito.interactor.api.network.RestError;

/**
 * Created by thanhle on 2/28/17.
 */

public interface IProjectDetailView extends IView {

    void createSuccess();

    void createFailure(String error);

    void showLoading();

    void hideLoading();

    void uploadError(String error);

    void uploadSuccess(String url);
}
