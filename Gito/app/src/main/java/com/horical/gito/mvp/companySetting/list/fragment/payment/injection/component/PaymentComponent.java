package com.horical.gito.mvp.companySetting.list.fragment.payment.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.fragment.payment.injection.module.PaymentModule;
import com.horical.gito.mvp.companySetting.list.fragment.payment.view.PaymentFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = PaymentModule.class)
public interface PaymentComponent {
    void inject(PaymentFragment fragment);
}
