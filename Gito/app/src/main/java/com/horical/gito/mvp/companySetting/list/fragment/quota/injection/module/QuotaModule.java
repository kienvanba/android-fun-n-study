package com.horical.gito.mvp.companySetting.list.fragment.quota.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.list.fragment.quota.presenter.QuotaPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class QuotaModule {
    @PerFragment
    @Provides
    QuotaPresenter provideQuotaPresenter(){
        return new QuotaPresenter();
    }
}
