package com.horical.gito.widget.chartview.line;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;

import com.horical.gito.R;
import com.horical.gito.utils.HDate;
import com.horical.gito.widget.chartview.ChartView;

public class TimeLineView extends ChartView {
    /**
     * Date - Start Project
     */
    private String mDateStart = "mm/dd/yyyy";
    private HDate dateStart;

    /**
     * Date - End Project (Due)
     */
    private String mDateRelease = "mm/dd/yyyy";
    private HDate dateRelease;


    private int mBackgroundColor;
    private int mForegroundColor;
    private float startX = 12,radius = 12;
    private float y = convertDp(20);
    private float endX = getWidth()-100;
    private float currentX = startX;

    public TimeLineView(Context context) {
        super(context);
    }

    public TimeLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.TimeLineView,0,0);
        try{
            mBackgroundColor = typedArray.getColor(R.styleable.TimeLineView_colorBackground,0);
            mForegroundColor = typedArray.getColor(R.styleable.TimeLineView_colorForeground,0);
        }finally {
            typedArray.recycle();
        }
    }

    public void setDateStart(HDate date){
        this.mDateStart = date.shortDisplay();
        this.dateStart = date;
    }
    public void setDateRelease(HDate date){
        this.mDateRelease = date.shortDisplay();
        this.dateRelease = date;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        /** draw background **/
        onDrawBackground(canvas);

        /** draw Arrow **/
        onDrawArrow(canvas);

        /** draw current date point **/
        onDrawCurrentPoint(canvas);

        /** draw string data **/
        onDrawData(canvas);
    }

    protected void onDrawBackground(Canvas canvas) {
        mPaint.setColor(mBackgroundColor);
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.FILL);

        canvas.drawRect(
                0,
                convertDp(15),
                getWidth(),
                convertDp(45),
                mPaint);
    }

    protected void onDrawArrow(Canvas canvas){
        if(dateRelease!=null && dateStart!=null) {
            long allDate = dateRelease.numberDayFrom(dateStart);
            if (allDate <= 0) {
                endX = startX;
            }else{
                endX = getWidth()-100;
            }
        }

        /** Draw line **/
        mPaint.setColor(mForegroundColor);
        mPaint.setStrokeWidth(4);
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawLine(
                0,
                y,
                getWidth(),
                y,
                mPaint
        );
        mPaint.setStrokeWidth(6);
        canvas.drawLine(
                startX,
                y-1,
                endX,
                y-1,
                mPaint
        );
        canvas.drawLine(
                startX,
                y+1,
                endX,
                y+1,
                mPaint
        );

        /** Arrow head **/
        mPaint.setStrokeWidth(4);
        canvas.drawLine(
                getWidth()-25,
                y-10,
                getWidth(),
                y,
                mPaint
        );
        canvas.drawLine(
                getWidth()-25,
                y+10,
                getWidth(),
                y,
                mPaint
        );

        /** Start Point **/
        mPaint.setStrokeWidth(2);
        mPaint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(
                startX,
                y,
                radius,
                mPaint
        );
        mPaint.setColor(getResources().getColor(R.color.white));
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(
                startX,
                y,
                radius-1,
                mPaint
        );

        /** End point **/
        canvas.drawCircle(
                endX,
                y,
                radius-1,
                mPaint
        );
        mPaint.setColor(mForegroundColor);
        mPaint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(
                endX,
                y,
                radius,
                mPaint
        );
    }

    protected void onDrawCurrentPoint(Canvas canvas){
        if(dateRelease!=null && dateStart!=null) {
            long allDate = dateRelease.numberDayFrom(dateStart);
            long currentDate = new HDate().numberDayFrom(dateStart);
            if (allDate <= 0 ) {
                currentX = startX;
            }else{
                float percent = ((float)currentDate / (float)allDate);
                currentX = (endX - startX) * percent + startX;
            }
        }

        mPaint.setStrokeWidth(2);
        mPaint.setColor(getResources().getColor(R.color.red));
        canvas.drawLine(
                currentX,
                y+25,
                currentX,
                y-25,
                mPaint);
        mPaint.setColor(mForegroundColor);
        mPaint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(
                currentX,
                y,
                radius,
                mPaint
        );
        mPaint.setColor(getResources().getColor(R.color.white));
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(
                currentX,
                y,
                radius-1,
                mPaint
        );
    }

    protected void onDrawData(Canvas canvas){
        mPaint.setTextSize(convertDp(14));
        mPaint.setColor(getResources().getColor(R.color.black_opacity));
        mPaint.setTextAlign(Paint.Align.LEFT);

        // draw date start
        canvas.drawText(
                mDateStart,
                0,
                convertDp(45),
                mPaint);

        // draw date end
        mPaint.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText(
                mDateRelease,
                getWidth(),
                convertDp(45),
                mPaint);
    }

}
