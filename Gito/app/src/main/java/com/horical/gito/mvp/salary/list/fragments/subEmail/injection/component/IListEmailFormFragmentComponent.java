package com.horical.gito.mvp.salary.list.fragments.subEmail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.list.fragments.subEmail.injection.module.ListEmailFormFragmentModule;
import com.horical.gito.mvp.salary.list.fragments.subEmail.view.ListEmailFormFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ListEmailFormFragmentModule.class)
public interface IListEmailFormFragmentComponent {
    void inject(ListEmailFormFragment activity);
}
