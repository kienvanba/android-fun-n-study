package com.horical.gito.mvp.myProject.setting.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<String>{
    private Context mContext;
    private List<String> mItems;
    private int currentItem;
    public SpinnerAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        mContext = context;
        mItems = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_spinner, parent, false);
        TextView textView = (TextView) view.findViewById(R.id.tv);
        textView.setText(mItems.get(position));
        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_spinner, parent, false);
        TextView textView = (TextView) view.findViewById(R.id.tv);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.container);
        textView.setText(mItems.get(position));
        if(position == currentItem){
            textView.setTextColor(mContext.getResources().getColor(R.color.blue));
            linearLayout.setBackground(mContext.getResources().getDrawable(R.drawable.bg_spinner_item));
        }else{
            textView.setTextColor(mContext.getResources().getColor(R.color.black));
            linearLayout.setBackgroundResource(R.color.white);
        }
        return view;
    }
    public void setCurrentItem(int position){
        this.currentItem = position;
    }
}
