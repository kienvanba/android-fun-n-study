package com.horical.gito.mvp.companySetting.list.fragment.department.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.companySetting.detail.newDepartment.view.NewDepartmentActivity;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.department.adapter.DepartmentAdapter;
import com.horical.gito.mvp.companySetting.list.fragment.department.injection.component.DaggerDepartmentComponent;
import com.horical.gito.mvp.companySetting.list.fragment.department.injection.component.DepartmentComponent;
import com.horical.gito.mvp.companySetting.list.fragment.department.injection.module.DepartmentModule;
import com.horical.gito.mvp.companySetting.list.fragment.department.presenter.DepartmentPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class DepartmentFragment extends BaseCompanyFragment implements IDepartmentView {
    public static final String TAG = makeLogTag(DepartmentFragment.class);
    public final static int REQUEST_CODE_EDIT_DEPARTMENT = 5;
    @Bind(R.id.refresh)
    SwipeRefreshLayout refresh;

    @Bind(R.id.rcv_department)
    RecyclerView rcvDepartment;

    private DepartmentAdapter adapterDepartment;

    @Inject
    DepartmentPresenter mPresenter;

    public static DepartmentFragment newInstance() {
        Bundle args = new Bundle();
        DepartmentFragment fragment = new DepartmentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_company_setting_department;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        DepartmentComponent component = DaggerDepartmentComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .departmentModule(new DepartmentModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        adapterDepartment = new DepartmentAdapter(getContext(), mPresenter.getListDepartment());
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvDepartment.setLayoutManager(llm);
        rcvDepartment.setAdapter(adapterDepartment);

        mPresenter.getAllDepartment();
    }

    @Override
    protected void initListener() {
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getAllDepartment();
            }
        });
        adapterDepartment.setOnItemClickListener(new DepartmentAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(int position) {
                Intent intent = new Intent(getContext(), NewDepartmentActivity.class);
                intent.putExtra(NewDepartmentActivity.NEW_DEPARTMENT, mPresenter.getListDepartment().get(position));
                getActivity().startActivityForResult(intent, REQUEST_CODE_EDIT_DEPARTMENT);
            }

            @Override
            public void onDelete(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.are_you_sure_for_delete_this, "department"));
                builder.setCancelable(true);
                builder.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mPresenter.deleteDepartment(mPresenter.getListDepartment().get(position).get_id(), position);
                            }
                        });

                builder.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public void onUpdateData() {
        mPresenter.getAllDepartment();
    }

    @Override
    public void getAllDepartmentSuccess() {
        refresh.setRefreshing(false);
        adapterDepartment.notifyDataSetChanged();
    }

    @Override
    public void getAllDepartmentFailure(String err) {
        refresh.setRefreshing(false);
        adapterDepartment.notifyDataSetChanged();
    }

    @Override
    public void deleteDepartmentSuccess(int position) {
        mPresenter.getListDepartment().remove(position);
        Toast.makeText(getContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
        adapterDepartment.notifyDataSetChanged();
    }

    @Override
    public void deleteDepartmentFailure(String err) {
        adapterDepartment.notifyDataSetChanged();
        Toast.makeText(getContext(), "Failure", Toast.LENGTH_SHORT).show();
    }
}
