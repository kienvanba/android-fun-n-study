package com.horical.gito.mvp.logTime.filter.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.horical.gito.R;
import com.horical.gito.mvp.popup.dateSelected.DateDto;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LogTimeFilterAdapter extends RecyclerView.Adapter<LogTimeFilterAdapter.LogTimeFilterViewHolder> {

    private Context mContext;
    private List<DateDto> mList;

    public interface OnItemClickListener {
        void onItemClicked(int pos);
    }

    private OnItemClickListener itemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.itemClickListener = listener;
    }


    public LogTimeFilterAdapter(Context context, List<DateDto> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public LogTimeFilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_log_time_filter, parent, false);
        return new LogTimeFilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LogTimeFilterViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        final DateDto dateDto = mList.get(position);
        holder.cbSelect.setChecked(dateDto.isSelected());
        holder.tvTitleTime.setText(dateDto.getTypeDate());
        String day = mList.get(position).showDateStartEnd();
        holder.tvTime.setText(
                day.isEmpty() ? mContext.getString(R.string.custom_day_pick) : day);

        final View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (position == mList.size() - 1) {
                    itemClickListener.onItemClicked(position);
                }

                boolean isChecked = dateDto.isSelected();
                if (!isChecked) {
                    resetSelected(position);
                    notifyDataSetChanged();
                }
            }
        };

        holder.view.setOnClickListener(listener);
        holder.cbSelect.setOnClickListener(listener);
        holder.cbSelect.setOnCheckedChangeListener(null);

    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class LogTimeFilterViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.log_time_cb_check_item_filter)
        CheckBox cbSelect;

        @Bind(R.id.log_time_tv_title_time)
        GitOTextView tvTitleTime;

        @Bind(R.id.log_time_tv_time)
        GitOTextView tvTime;

        View view;

        public LogTimeFilterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    private void resetSelected(int position) {
        for (int i = 0; i < mList.size(); i++) {
            if (i == position) mList.get(i).setSelected(true);
            else mList.get(i).setSelected(false);
        }
    }

    public DateDto getCurrentDateFilter() {
        for (DateDto dateDto : mList) {
            if (dateDto.isSelected()) return dateDto;
        }

        return null;
    }
}
