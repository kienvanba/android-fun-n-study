package com.horical.gito.mvp.myProject.task.details.fragment.overview.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.task.details.fragment.overview.presenter.OverViewPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class OverViewModule {
    @PerFragment
    @Provides
    OverViewPresenter providerOverViewPresenter() {
        return new OverViewPresenter();
    }
}
