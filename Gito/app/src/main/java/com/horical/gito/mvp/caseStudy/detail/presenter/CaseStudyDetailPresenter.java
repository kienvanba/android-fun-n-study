package com.horical.gito.mvp.caseStudy.detail.presenter;

import android.util.Log;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.caseStudy.CreateCaseStudyRequest;
import com.horical.gito.interactor.api.request.caseStudy.UpdateAttachFileRequest;
import com.horical.gito.interactor.api.request.caseStudy.UpdateCaseStudyRequest;
import com.horical.gito.interactor.api.request.caseStudy.UpdateStatusCaseStudyRequest;
import com.horical.gito.interactor.api.request.file.UploadFileRequest;
import com.horical.gito.interactor.api.request.file.UploadFileResponse;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetCreateCaseStudyResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetUpdateAttachFileResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetUpdateCaseStudyResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetUpdateStatusCaseStudyResponse;
import com.horical.gito.model.CaseStudy;
import com.horical.gito.mvp.caseStudy.detail.view.ICaseStudyDetailView;
import com.horical.gito.utils.CommonUtils;

import java.io.File;
import java.util.List;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;


public class CaseStudyDetailPresenter extends BasePresenter implements ICaseStudyDetailPresenter {

    public void attachView(ICaseStudyDetailView view) {
        super.attachView(view);
    }

    public void detachView() {
        super.detachView();
    }

    public ICaseStudyDetailView getView() {
        return (ICaseStudyDetailView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }


    @Override
    public void createCaseStudy(CaseStudy caseStudy) {
        CreateCaseStudyRequest caseStudyRequest = new CreateCaseStudyRequest();
        caseStudyRequest.title = caseStudy.getTitle();
        caseStudyRequest.desc = caseStudy.getDesc();
        caseStudyRequest.status = 0;

        getApiManager().createCaseStudy(caseStudyRequest, new ApiCallback<GetCreateCaseStudyResponse>() {
            @Override
            public void success(GetCreateCaseStudyResponse res) {
                Log.d("TAG", res.caseStudy.get_id());
                getView().createCaseStudySuccess(res.caseStudy);
            }

            @Override
            public void failure(RestError error) {
                getView().createCaseStudyFailure(error.message);
            }
        });
    }

    @Override
    public void updateCaseStudy(final CaseStudy caseStudy) {
        UpdateCaseStudyRequest caseStudyRequest = new UpdateCaseStudyRequest();
        caseStudyRequest.title = caseStudy.getTitle();
        caseStudyRequest.desc = caseStudy.getDesc();
        caseStudyRequest.status = caseStudy.getStatus();

        getApiManager().updateContentCaseStudy(caseStudy.get_id(), caseStudyRequest, new ApiCallback<GetUpdateCaseStudyResponse>() {
            @Override
            public void success(GetUpdateCaseStudyResponse res) {
                getView().updateCaseStudySuccess(caseStudy.getStatus());
            }

            @Override
            public void failure(RestError error) {
                getView().updateCaseStudyFailure(error.message);
            }
        });
    }

    @Override
    public void updateStatusCaseStudy(String id, int status, String rejectMsg) {
        UpdateStatusCaseStudyRequest caseStudyRequest;
        if (status == 1) {
            caseStudyRequest = new UpdateStatusCaseStudyRequest();
            caseStudyRequest.status = status;
        } else {
            caseStudyRequest = new UpdateStatusCaseStudyRequest();
            caseStudyRequest.status = status;
            caseStudyRequest.rejectMsg = rejectMsg;
        }
        getApiManager().updateStatusCaseStudy(id, caseStudyRequest, new ApiCallback<GetUpdateStatusCaseStudyResponse>() {
            @Override
            public void success(GetUpdateStatusCaseStudyResponse res) {
                getView().updateStatusCaseStudySuccess(res.caseStudy);
            }

            @Override
            public void failure(RestError error) {
                getView().updateStatusCaseStudyFailure(error.message);
            }
        });
    }

    @Override
    public void uploadFile(MediaType type, File file) {
        UploadFileRequest request = new UploadFileRequest(type, file);
        RequestBody status = RequestBody.create(okhttp3.MultipartBody.FORM, "0");
        getApiManager().uploadFile(request.file, status, new ApiCallback<UploadFileResponse>() {
            @Override
            public void success(UploadFileResponse res) {
                getView().uploadFileSuccess(res.tokenFile);
            }

            @Override
            public void failure(RestError error) {
                getView().uploadFileFailure(error.message);
            }
        });
    }

    @Override
    public void addAttachFile(String idCaseStudy, String idAttachFile) {
        UpdateAttachFileRequest attachFileRequest = new UpdateAttachFileRequest();
        attachFileRequest.attachFiles.add(idAttachFile);
        getApiManager().updateAttachFile(idCaseStudy, attachFileRequest, new ApiCallback<GetUpdateAttachFileResponse>() {
            @Override
            public void success(GetUpdateAttachFileResponse res) {
                getView().updateAttachFileSuccess(res.caseStudy);
            }

            @Override
            public void failure(RestError error) {
                getView().updateAttachFileFailure(error.message);
            }
        });
    }

    @Override
    public void deleteAttachFile(String idCaseStudy, String idAttachFile) {
        UpdateAttachFileRequest attachFileRequest = new UpdateAttachFileRequest();
        attachFileRequest.attachFiles.add(idAttachFile);
        getApiManager().deleteAttachFile(idCaseStudy, attachFileRequest, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteAttachFileSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().deleteAttachFileFailure(error.message);
            }
        });
    }
}
