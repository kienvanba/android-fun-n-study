package com.horical.gito.mvp.salary.mySalary.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.mySalary.list.presenter.ListMySalaryPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListMySalaryModule {
    @PerActivity
    @Provides
    ListMySalaryPresenter provideReportPresenter(){
        return new ListMySalaryPresenter();
    }
}
