package com.horical.gito.mvp.companySetting.list.fragment.payment.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.mvp.companySetting.list.fragment.BaseCompanyFragment;
import com.horical.gito.mvp.companySetting.list.fragment.payment.adapter.CardAdapter;
import com.horical.gito.mvp.companySetting.list.fragment.payment.adapter.HistoryAdapter;
import com.horical.gito.mvp.companySetting.list.fragment.payment.injection.component.DaggerPaymentComponent;
import com.horical.gito.mvp.companySetting.list.fragment.payment.injection.component.PaymentComponent;
import com.horical.gito.mvp.companySetting.list.fragment.payment.injection.module.PaymentModule;
import com.horical.gito.mvp.companySetting.list.fragment.payment.presenter.PaymentPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

/**
 * Created by Tin on 23-Nov-16.
 */

public class PaymentFragment extends BaseCompanyFragment implements IPaymentView, View.OnClickListener {
    public static final String TAG = makeLogTag(PaymentFragment.class);
    @Bind(R.id.chk_decrease)
    CheckBox chkDecrease;

    @Bind(R.id.rcv_history)
    RecyclerView rcvHistory;

    @Bind(R.id.ll_add_card)
    LinearLayout llAddCard;

    @Bind(R.id.rcv_card)
    RecyclerView rcvCard;

    private CardAdapter adapterCard;
    private HistoryAdapter adapterHistory;

    @Inject
    PaymentPresenter mPresenter;


    public static PaymentFragment newInstance() {
        Bundle args = new Bundle();
        PaymentFragment fragment = new PaymentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_company_setting_payment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        PaymentComponent component = DaggerPaymentComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .paymentModule(new PaymentModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {
        adapterHistory = new HistoryAdapter(getContext(), mPresenter.getListHistory());
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvHistory.setLayoutManager(llm);
        rcvHistory.setAdapter(adapterHistory);

        adapterCard = new CardAdapter(getContext(), mPresenter.getListCard());
        LinearLayoutManager llmCard = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llmCard.setOrientation(LinearLayoutManager.VERTICAL);
        rcvCard.setLayoutManager(llmCard);
        rcvCard.setAdapter(adapterCard);

        showLoading();
        mPresenter.getAllHistory();
    }

    @Override
    protected void initListener() {
        adapterCard.setOnItemClickListener(new CardAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(int position) {

            }

            @Override
            public void onClickEdit(int position) {

            }
        });

        adapterHistory.setOnItemClickListener(new HistoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClickSelected(int position) {

            }
        });
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllHistorySuccess() {
    }

    @Override
    public void getAllHistoryFailure(String err) {
        Toast.makeText(getContext(), "Failure", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getAllCardSuccess() {
        hideLoading();
        Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
        adapterHistory.notifyDataSetChanged();
        adapterCard.notifyDataSetChanged();
    }

    @Override
    public void getAllCardFailure(String err) {
        hideLoading();
        Toast.makeText(getContext(), "Failure", Toast.LENGTH_SHORT).show();
    }
}
