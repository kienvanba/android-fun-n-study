package com.horical.gito.interactor.api.response.companySetting;

import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.Feature;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by thanhle on 3/21/17.
 */

public class GetFeatureResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    public Feature feature;
}