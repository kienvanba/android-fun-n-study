package com.horical.gito.mvp.leaving.confirm.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.mvp.leaving.confirm.adapter.LeavingConfirmAdapter;
import com.horical.gito.mvp.leaving.confirm.injection.component.DaggerLeavingConfirmComponent;
import com.horical.gito.mvp.leaving.confirm.injection.component.LeavingConfirmComponent;
import com.horical.gito.mvp.leaving.confirm.injection.module.LeavingConfirmModule;
import com.horical.gito.mvp.leaving.confirm.presenter.LeavingConfirmPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LeavingConfirmActivity extends BaseActivity implements View.OnClickListener, ILeavingConfirmView {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.leaving_lv_confirm)
    RecyclerView mList;

    @Inject
    LeavingConfirmPresenter mPresenter;

    LeavingConfirmAdapter mAdapterConfirm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaving_confirm);
        ButterKnife.bind(this);

        LeavingConfirmComponent component = DaggerLeavingConfirmComponent.builder()
                .leavingConfirmModule(new LeavingConfirmModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();
        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle("Confirm New Leaving");
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);

        mPresenter.setResource(this.getResources());
        mAdapterConfirm = new LeavingConfirmAdapter(mPresenter.getList(), this, new LeavingConfirmAdapter.ApproveRejectFromChild() {
            @Override
            public void isEmptyChildList(int position) {
                mPresenter.getList().remove(position);
                mAdapterConfirm.notifyDataSetChanged();
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mList.setAdapter(mAdapterConfirm);
        mList.setLayoutManager(layoutManager);
        mList.setHasFixedSize(true);
        mAdapterConfirm.notifyDataSetChanged();
    }

    protected void initListener() {
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }
}
