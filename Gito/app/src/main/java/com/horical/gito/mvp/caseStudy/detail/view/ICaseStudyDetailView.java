package com.horical.gito.mvp.caseStudy.detail.view;

import com.horical.gito.base.IView;
import com.horical.gito.model.CaseStudy;
import com.horical.gito.model.TokenFile;

public interface ICaseStudyDetailView extends IView {

    void showLoading();

    void hideLoading();

    void createCaseStudySuccess(CaseStudy caseStudy);

    void createCaseStudyFailure(String error);

    void updateCaseStudySuccess(int status);

    void updateCaseStudyFailure(String error);

    void updateStatusCaseStudySuccess(CaseStudy caseStudy);

    void updateStatusCaseStudyFailure(String error);

    void uploadFileSuccess(TokenFile tokenFile);

    void uploadFileFailure(String error);

    void updateAttachFileSuccess(CaseStudy caseStudy);

    void updateAttachFileFailure(String error);

    void deleteAttachFileSuccess();

    void deleteAttachFileFailure(String error);
}
