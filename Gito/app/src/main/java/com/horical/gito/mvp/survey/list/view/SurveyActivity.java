package com.horical.gito.mvp.survey.list.view;

import android.content.Intent;
import android.os.Bundle;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.Survey;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.survey.list.adapter.SurveyAdapter;
import com.horical.gito.mvp.survey.list.injection.component.DaggerSurveyComponent;
import com.horical.gito.mvp.survey.list.injection.component.SurveyComponent;
import com.horical.gito.mvp.survey.list.injection.module.SurveyModule;
import com.horical.gito.mvp.survey.list.presenter.SurveyPresenter;
import com.horical.gito.mvp.survey.newsurvey.view.NewSurveyActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.SURVEY;
import static com.horical.gito.utils.LogUtils.makeLogTag;

public class SurveyActivity extends DrawerActivity implements ISurveyView, View.OnClickListener {
    private static final String TAG = makeLogTag(SurveyActivity.class);

    public static final String KEY_FROM = "KEY_FROM";
    public static final int KEY_APPROVED = 0;
    public static final int KEY_PENDING = 1;
    public static final int KEY_MINE = 2;
    public static final int KEY_ADD = 3;

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.tv_approved_survey)
    TextView tvAppreoved;

    @Bind(R.id.tv_pending_survey)
    TextView tvPending;

    @Bind(R.id.tv_mine_survey)
    TextView tvMine;

    @Bind(R.id.refresh_survey)
    SwipeRefreshLayout mRefresh;

    @Bind(R.id.rcv_survey)
    RecyclerView mRcvSurvey;

    private SurveyAdapter adapterSurvey;

    private int currentTab = 0;

    @Inject
    SurveyPresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_survey;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_SURVEY;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        SurveyComponent component = DaggerSurveyComponent.builder()
                .surveyModule(new SurveyModule())
                .applicationComponent(MainApplication.getAppComponent())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    protected void initData() {
        topBar.setTextTitle(getString(R.string.survey));
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);
        topBar.setImageViewRight(CustomViewTopBar.DRAWABLE_ADD_TRANSPARENT);

        tvAppreoved.setSelected(true);

        mPresenter.getAllSurveyApproved();
        adapterSurvey = new SurveyAdapter(this, mPresenter.getListSurvey());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRcvSurvey.setLayoutManager(layoutManager);
        mRcvSurvey.setHasFixedSize(false);
        mRcvSurvey.setAdapter(adapterSurvey);

        showLoading();
    }

    protected void initListener() {
        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                openDrawer();
            }

            @Override
            public void onRightButtonOneClicked() {
                Intent intent = new Intent(SurveyActivity.this, NewSurveyActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(KEY_FROM,KEY_ADD);
                intent.putExtras(bundle);
                startActivity(intent);

              /*  Intent intent = new Intent(SurveyActivity.this,ResultSurveyActivity.class);
                startActivity(intent);*/

                /*Intent intent = new Intent(SurveyActivity.this, StatisticalActivity.class);
                startActivity(intent);*/
            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });
        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                if(currentTab == KEY_APPROVED)
                    mPresenter.getAllSurveyApproved();
                if(currentTab == KEY_PENDING)
                    mPresenter.getAllSurveyPending();
                if (currentTab == KEY_MINE)
                    mPresenter.getAllSurvey();
            }
        });
        adapterSurvey.setOnItemClickListener(new SurveyAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                Intent intent = new Intent(SurveyActivity.this, NewSurveyActivity.class);
                Bundle bundle = new Bundle();
                Survey survey = mPresenter.getListSurvey().get(position);
                bundle.putSerializable(SURVEY,survey);
                bundle.putInt(KEY_FROM,currentTab);
                intent.putExtras(bundle);
                startActivity(intent);

               /* Intent intent = new Intent(SurveyActivity.this,ResultSurveyActivity.class);
                startActivity(intent);*/
            }
        });

        tvAppreoved.setOnClickListener(this);
        tvPending.setOnClickListener(this);
        tvMine.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_approved_survey:
                tvAppreoved.setSelected(true);
                tvPending.setSelected(false);
                tvMine.setSelected(false);

                mPresenter.getAllSurveyApproved();
                adapterSurvey.setListSurvey(mPresenter.getListSurvey());
                adapterSurvey.notifyDataSetChanged();
                currentTab = KEY_APPROVED;
                break;
            case R.id.tv_pending_survey:
                tvAppreoved.setSelected(false);
                tvPending.setSelected(true);
                tvMine.setSelected(false);

                mPresenter.getAllSurveyPending();
                adapterSurvey.setListSurvey(mPresenter.getListSurvey());
                adapterSurvey.notifyDataSetChanged();
                currentTab = KEY_PENDING;
                break;
            case R.id.tv_mine_survey:
                tvAppreoved.setSelected(false);
                tvPending.setSelected(false);
                tvMine.setSelected(true);

                mPresenter.getAllSurvey();
                adapterSurvey.setListSurvey(mPresenter.getListSurvey());
                adapterSurvey.notifyDataSetChanged();
                currentTab = KEY_MINE;
                break;
        }
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllSurveySuccess() {
        mRefresh.setRefreshing(false);
        hideLoading();
        showToast("Success");
        adapterSurvey.setListSurvey(mPresenter.getListSurvey());
        adapterSurvey.notifyDataSetChanged();
    }

    @Override
    public void getAllSurveyFailure(String error) {
        mRefresh.setRefreshing(false);
        hideLoading();
        showToast("Failure");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.bind(this);
    }
}
