package com.horical.gito.mvp.roomBooking.detail.fragment.month.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.roomBooking.detail.fragment.month.injection.module.MonthModule;
import com.horical.gito.mvp.roomBooking.detail.fragment.month.view.MonthFragment;

import dagger.Component;

/**
 * Created by QuangHai on 08/11/2016.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = MonthModule.class)
public interface MonthComponent {
    void inject(MonthFragment fragment);
}
