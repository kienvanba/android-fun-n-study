package com.horical.gito.caches.storage;

import com.horical.gito.model.Department;
import com.horical.gito.model.Information;
import com.horical.gito.model.Level;
import com.horical.gito.model.Project;
import com.horical.gito.model.RoleCompany;

import java.util.List;

public class GitOStorage implements IGitOStorage {

    public synchronized static final GitOStorage getInstance() {
        return GitOStorage.INSTANCE;
    }

    private static final GitOStorage INSTANCE = new GitOStorage();

    public Project project;

    public Information information;

    public List<Level> listLevel;

    public List<Department> listDepartment;

    public List<RoleCompany> listRoleCompany;

    public List<RoleCompany> getListRoleCompany() {
        return listRoleCompany;
    }

    public void setListRoleCompany(List<RoleCompany> listRoleCompany) {
        this.listRoleCompany = listRoleCompany;
    }

    public List<Level> getListLevel() {
        return listLevel;
    }

    public void setListLevel(List<Level> listLevel) {
        this.listLevel = listLevel;
    }

    public List<Department> getListDepartment() {
        return listDepartment;
    }

    public void setListDepartment(List<Department> listDepartment) {
        this.listDepartment = listDepartment;
    }

    public Project getProject() {
        return project;
    }

    public Information getInformation() {
        return information;
    }

    public void setInformation(Information information) {
        this.information = information;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public boolean isProjectDetails() {
        return project != null;
    }
}
