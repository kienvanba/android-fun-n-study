package com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.companySetting.list.fragment.workingAndHoliday.presenter.WorkingAndHolidayPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tin on 23-Nov-16.
 */


@Module
public class WorkingAndHolidayModule {
    @PerFragment
    @Provides
    WorkingAndHolidayPresenter provideWorkingAndHolidayPresenter(){
        return new WorkingAndHolidayPresenter();
    }
}
