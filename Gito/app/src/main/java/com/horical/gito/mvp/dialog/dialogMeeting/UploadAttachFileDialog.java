package com.horical.gito.mvp.dialog.dialogMeeting;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;

import butterknife.Bind;
import butterknife.ButterKnife;


public class UploadAttachFileDialog extends Dialog implements View.OnClickListener {

    @Bind(R.id.top_bar_add_attach_file)
    CustomViewTopBar topBar;

    @Bind(R.id.rl_gallery)
    RelativeLayout rlGallery;

    @Bind(R.id.rl_upload_file)
    RelativeLayout rlUploadFile;

    private OnDialogEditFileClickListener mListener;

    public void setListener(OnDialogEditFileClickListener mListener) {
        this.mListener = mListener;
    }

    public UploadAttachFileDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fragment_meeting_detail_add_attach_file);
        init();

    }

    private void init() {
        ButterKnife.bind(this);

        if (this.getWindow() != null) {
            this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            this.getWindow().setBackgroundDrawableResource(android.R.color.white);
        }

        topBar.setTextTitle(String.valueOf(R.string.add_attach_file));
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_BACK);
        topBar.setOnLeftRightClickListener(new CustomViewTopBar.OnLeftRightClickListener() {
            @Override
            public void onLeftClicked() {
                onBackPressed();
            }

            @Override
            public void onRightClicked() {

            }
        });

        rlGallery.setOnClickListener(this);
        rlUploadFile.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_gallery:
                mListener.albumGallery();
                break;

            case R.id.rl_upload_file:
                mListener.documentGallery();
                break;
        }
    }

    public interface OnDialogEditFileClickListener {

        void albumGallery();

        void documentGallery();
    }
}
