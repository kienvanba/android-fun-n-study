package com.horical.gito.mvp.companySetting.list.fragment.members.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.user.UpdateMemberRequest;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllDepartmentResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllLevelResponse;
import com.horical.gito.interactor.api.response.user.GetAllUserResponse;
import com.horical.gito.model.RoleCompany;
import com.horical.gito.model.User;
import com.horical.gito.mvp.companySetting.list.fragment.members.view.IMembersView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Tin on 23-Nov-16.
 */

public class MembersPresenter extends BasePresenter implements IMembersPresenter {
    private List<User> mListMembers = new ArrayList<>();
    private HashSet<RoleCompany> listRoleCompany = new HashSet<>();
    private List<RoleCompany> mListRoleCompany;

    public void attachView(IMembersView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public IMembersView getView() {
        return (IMembersView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
        getAllLevel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<User> getListMembers() {
        return mListMembers;
    }

    public void setListMembers(List<User> mListMembers) {
        this.mListMembers = mListMembers;
    }

    @Override
    public void getAllMember() {
        getApiManager().getAllUser(new ApiCallback<GetAllUserResponse>() {
            @Override
            public void success(GetAllUserResponse res) {
                if (mListMembers != null) {
                    mListMembers.clear();
                } else {
                    mListMembers = new ArrayList<User>();
                }
                mListMembers.addAll(res.getUsers());
                if(GitOStorage.getInstance().getListRoleCompany() != null) {
                    GitOStorage.getInstance().getListRoleCompany().clear();
                }

                for(User user : res.getUsers()){
                    listRoleCompany.add(user.getRoleCompany());
                }
                mListRoleCompany = new ArrayList<>(listRoleCompany);

                GitOStorage.getInstance().setListRoleCompany(mListRoleCompany);
                getView().getAllMemberSuccess();
            }

            @Override
            public void failure(RestError error) {
                if (mListMembers != null) {
                    mListMembers.clear();
                } else {
                    mListMembers = new ArrayList<>();
                }
                getView().getAllMemberFailure(error.message);
            }
        });
    }

    @Override
    public void deleteMember(String idMember, final int position) {
        getApiManager().deleteMember(idMember, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().deleteMemberSuccess(position);
            }

            @Override
            public void failure(RestError error) {
                getView().deleteMemberFailure(error.message);
            }
        });
    }

    @Override
    public void updateMember(User user, String idLevel, String idDepartment) {
        UpdateMemberRequest memberRequest = new UpdateMemberRequest();
        memberRequest.address = user.getAddress();
        if(idLevel.equalsIgnoreCase("")){
            memberRequest.level = user.getLevel().getId();
            memberRequest.department = idDepartment;
        } else {
            memberRequest.level = idLevel;
            memberRequest.department = user.getDepartment().get_id();
        }
        memberRequest.displayName = user.getDisplayName();
        memberRequest.email = user.getEmail();
        memberRequest.isAdmin = user.isAdmin();
        memberRequest.phone = user.getPhone();
        memberRequest.roleCompany = user.getRoleCompany().getRoleCompanyId();
        memberRequest.status = user.getStatus();
        memberRequest.userId = user.getUserId();
        getApiManager().updateMember(user.get_id(), memberRequest, new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                getView().updateMemberSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().updateMemberFailure(error.message);
            }
        });
    }

    @Override
    public void getAllLevel() {
        getApiManager().getAllLevel(new ApiCallback<GetAllLevelResponse>() {
            @Override
            public void success(GetAllLevelResponse res) {
                if(GitOStorage.getInstance().getListLevel() != null){
                    GitOStorage.getInstance().getListLevel().clear();
                }
                GitOStorage.getInstance().setListLevel(res.levels);
                getAllDepartment();
            }

            @Override
            public void failure(RestError error) {

            }
        });
    }

    @Override
    public void getAllDepartment() {
        getApiManager().getAllDepartment(new ApiCallback<GetAllDepartmentResponse>() {
            @Override
            public void success(GetAllDepartmentResponse res) {
                if(GitOStorage.getInstance().getListDepartment() != null) {
                    GitOStorage.getInstance().getListDepartment().clear();
                }
                GitOStorage.getInstance().setListDepartment(res.departments);
            }

            @Override
            public void failure(RestError error) {
            }
        });
    }
}

