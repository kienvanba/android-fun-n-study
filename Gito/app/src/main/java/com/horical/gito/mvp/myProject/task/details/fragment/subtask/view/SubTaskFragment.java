package com.horical.gito.mvp.myProject.task.details.fragment.subtask.view;

import android.os.Bundle;
import android.view.View;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseFragment;
import com.horical.gito.mvp.myProject.task.details.fragment.subtask.injection.component.DaggerSubTaskComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.subtask.injection.component.SubTaskComponent;
import com.horical.gito.mvp.myProject.task.details.fragment.subtask.injection.module.SubTaskModule;
import com.horical.gito.mvp.myProject.task.details.fragment.subtask.presenter.SubTaskPresenter;

import javax.inject.Inject;

import butterknife.ButterKnife;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class SubTaskFragment extends BaseFragment implements ISubTaskView, View.OnClickListener{

    public static final String TAG = makeLogTag(SubTaskFragment.class);

    @Inject
    SubTaskPresenter mPresenter;

    public static SubTaskFragment newInstance() {

        Bundle args = new Bundle();

        SubTaskFragment fragment = new SubTaskFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_task_details_subtask;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        SubTaskComponent component = DaggerSubTaskComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .subTaskModule(new SubTaskModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onClick(View v) {

    }
}
