package com.horical.gito.caches.images;

import android.graphics.Bitmap;

public interface ImageLoaderCallback {

    void onCompleted(Bitmap bm);

    void onFailed(Exception e);
}