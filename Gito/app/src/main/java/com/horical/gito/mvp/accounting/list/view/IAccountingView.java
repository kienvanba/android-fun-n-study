package com.horical.gito.mvp.accounting.list.view;

import com.horical.gito.base.IView;

public interface IAccountingView extends IView {
    void showLoading();

    void hideLoading();

    void getAllAccountingSuccess();

    void getAllAccountingFailure(String error);

    void deleteAccountingSuccess(int position);

    void deleteAccountingFailure();
}
