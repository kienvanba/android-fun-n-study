package com.horical.gito.interactor.api.network;

import com.horical.gito.interactor.api.request.TodoNote.Note.NoteRequest;
import com.horical.gito.interactor.api.request.TodoNote.Note.UpdateAttachFileNoteRequest;
import com.horical.gito.interactor.api.request.TodoNote.Todo.TodoRequest;
import com.horical.gito.interactor.api.request.caseStudy.CreateCaseStudyRequest;
import com.horical.gito.interactor.api.request.caseStudy.GetAllCaseStudyRequest;
import com.horical.gito.interactor.api.request.caseStudy.UpdateAttachFileRequest;
import com.horical.gito.interactor.api.request.caseStudy.UpdateCaseStudyRequest;
import com.horical.gito.interactor.api.request.caseStudy.UpdateStatusCaseStudyRequest;
import com.horical.gito.interactor.api.request.changePassword.ChangePassWordRequest;
import com.horical.gito.interactor.api.request.companySetting.CreateDepartmentRequest;
import com.horical.gito.interactor.api.request.companySetting.CreateLevelRequest;
import com.horical.gito.interactor.api.request.component.CreateComponentRequest;
import com.horical.gito.interactor.api.request.leaving.CreateLeavingRequest;
import com.horical.gito.interactor.api.request.meeting.MeetingRequest;
import com.horical.gito.interactor.api.request.mypage.GetMyPageRequest;
import com.horical.gito.interactor.api.request.project.CreateProjectRequest;
import com.horical.gito.interactor.api.request.project.GetLogTimeRequest;
import com.horical.gito.interactor.api.request.project.GetMembersHaveLogTimeRequest;
import com.horical.gito.interactor.api.request.project.UpdateLogTimeRequest;
import com.horical.gito.interactor.api.request.project.UpdateProjectRequest;
import com.horical.gito.interactor.api.request.report.MemberRequest;
import com.horical.gito.interactor.api.request.report.NewReportRequest;
import com.horical.gito.interactor.api.request.report.RejectApprovedRequest;
import com.horical.gito.interactor.api.request.room.RoomRequest;
import com.horical.gito.interactor.api.request.salary.inputAddon.CreateInputAddonRequest;
import com.horical.gito.interactor.api.request.salary.inputAddon.FilterInputAddonRequest;
import com.horical.gito.interactor.api.request.salary.stepParam.AddStepParamRequest;
import com.horical.gito.interactor.api.request.salary.stepParam.DeleteStepParamRequest;
import com.horical.gito.interactor.api.request.user.CreateMemberRequest;
import com.horical.gito.interactor.api.request.user.ForgotPasswordRequest;
import com.horical.gito.interactor.api.request.user.LoginRequest;
import com.horical.gito.interactor.api.request.user.RegisterRequest;
import com.horical.gito.interactor.api.request.user.UpdateMemberRequest;
import com.horical.gito.interactor.api.request.version.Request;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.accounting.GetAllAccountingResponse;
import com.horical.gito.interactor.api.response.accounting.GetCreateAccountingResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetAllCaseStudyResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetCreateCaseStudyResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetUpdateAttachFileResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetUpdateCaseStudyResponse;
import com.horical.gito.interactor.api.response.caseStudy.GetUpdateStatusCaseStudyResponse;
import com.horical.gito.interactor.api.response.checkin.GetAllCheckinResponse;
import com.horical.gito.interactor.api.response.checkin.GetCheckinResponse;
import com.horical.gito.interactor.api.response.companySetting.CreateDepartmentResponse;
import com.horical.gito.interactor.api.response.companySetting.CreateLevelResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllCardResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllCompanyRoleResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllDepartmentResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllHistoryResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllHolidayResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllLevelResponse;
import com.horical.gito.interactor.api.response.companySetting.GetAllProjectRoleResponse;
import com.horical.gito.interactor.api.response.companySetting.GetCompanyInfoResponse;
import com.horical.gito.interactor.api.response.companySetting.GetDevicesResponse;
import com.horical.gito.interactor.api.response.companySetting.GetFeatureResponse;
import com.horical.gito.interactor.api.response.companySetting.GetInfoQuotaResponse;
import com.horical.gito.interactor.api.response.companySetting.GetSummaryQuotaResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateDepartmentResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateFeatureResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateInfoResponse;
import com.horical.gito.interactor.api.response.companySetting.GetUpdateLevelResponse;
import com.horical.gito.interactor.api.response.component.CreateComponentResponse;
import com.horical.gito.interactor.api.response.component.GetAllComponentResponse;
import com.horical.gito.interactor.api.response.document.GetAllDocumentResponse;
import com.horical.gito.interactor.api.response.estate.CreateEstateResponse;
import com.horical.gito.interactor.api.response.estate.CreateWarehouseEstateResponse;
import com.horical.gito.interactor.api.response.estate.GetAllEstatesResponse;
import com.horical.gito.interactor.api.response.estate.GetAllWarehourseEstateResponse;
import com.horical.gito.interactor.api.response.group.GetAllGroupResponse;
import com.horical.gito.interactor.api.response.leaving.CreateLeavingResponse;
import com.horical.gito.interactor.api.response.leaving.GetAllLeavingResponse;
import com.horical.gito.interactor.api.response.leaving.GetLeavingByIdResponse;
import com.horical.gito.interactor.api.response.meeting.GetAllMeetingResponse;
import com.horical.gito.interactor.api.response.meeting.GetCreateMeetingResponse;
import com.horical.gito.interactor.api.response.meeting.GetUpdateMeetingResponse;
import com.horical.gito.interactor.api.response.mypage.GetMyPageResponse;
import com.horical.gito.interactor.api.response.note.GetAllNoteResponse;
import com.horical.gito.interactor.api.response.note.GetCreateNoteResponse;
import com.horical.gito.interactor.api.response.note.GetUpdateNoteResponse;
import com.horical.gito.interactor.api.response.project.CreateProjectResponse;
import com.horical.gito.interactor.api.response.project.GetAllProjectResponse;
import com.horical.gito.interactor.api.response.project.GetLogTimeResponse;
import com.horical.gito.interactor.api.response.project.GetMemberAndRoleResponse;
import com.horical.gito.interactor.api.response.project.GetMemberResponse;
import com.horical.gito.interactor.api.response.project.GetMembersHaveLogTimeResponse;
import com.horical.gito.interactor.api.response.project.GetProjectResponse;
import com.horical.gito.interactor.api.response.project.UpdateLogTimeResponse;
import com.horical.gito.interactor.api.response.project.UpdateProjectResponse;
import com.horical.gito.interactor.api.response.recruitment.RecruitmentResponse;
import com.horical.gito.interactor.api.response.report.AllReportResponse;
import com.horical.gito.interactor.api.response.report.GroupResponse;
import com.horical.gito.interactor.api.response.report.MemberResponse;
import com.horical.gito.interactor.api.response.room.GetAllRoomResponse;
import com.horical.gito.interactor.api.response.room.GetCreateRoomResponse;
import com.horical.gito.interactor.api.response.roomBooking.GetAllRoomBookingResponse;
import com.horical.gito.interactor.api.response.roomBooking.GetRoomBookingResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetAllInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetCreateInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetDeleteInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetDetailInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetFilterInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.bonus.GetUpdateInputAddonResponse;
import com.horical.gito.interactor.api.response.salary.metric.GetAllMetricResponse;
import com.horical.gito.interactor.api.response.salary.metric.GetCreateMetricResponse;
import com.horical.gito.interactor.api.response.salary.metric.GetDeleteMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metric.GetDetailMetricResponse;
import com.horical.gito.interactor.api.response.salary.metric.GetUpdateMetricResponse;
import com.horical.gito.interactor.api.response.salary.metric.param.GetAddParamResponse;
import com.horical.gito.interactor.api.response.salary.metric.param.GetDeleteParamResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetAddMetricMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetAllMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetCreateMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetDeleteMetricMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetDetailMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.metricGroup.GetUpdateMetricGroupResponse;
import com.horical.gito.interactor.api.response.salary.staff.GetAllSalaryStaffResponse;
import com.horical.gito.interactor.api.response.salary.staff.GetCreateSalaryStaffResponse;
import com.horical.gito.interactor.api.response.salary.staff.GetDeleteSalaryStaffResponse;
import com.horical.gito.interactor.api.response.salary.staff.GetDetailSalaryStaffResponse;
import com.horical.gito.interactor.api.response.salary.staff.GetUpdateSalaryStaffResponse;
import com.horical.gito.interactor.api.response.sourceCode.GetAllCodeResponse;
import com.horical.gito.interactor.api.response.sourceCode.GetCreateCodeResponse;
import com.horical.gito.interactor.api.response.summary.GetSummaryResponse;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyApprovedResponse;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyItemResponse;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyPendingResponse;
import com.horical.gito.interactor.api.response.survey.GetAllSurveyResponse;
import com.horical.gito.interactor.api.response.survey.GetSubmiterResponse;
import com.horical.gito.interactor.api.response.task.GetAllTaskResponse;
import com.horical.gito.interactor.api.response.task.GetCreateTaskResponse;
import com.horical.gito.interactor.api.response.task.GetDeleteTaskResponse;
import com.horical.gito.interactor.api.response.todolist.GetAllTodoListResponse;
import com.horical.gito.interactor.api.response.todolist.GetCreateTodoListResponse;
import com.horical.gito.interactor.api.response.todolist.GetUpdateTodoListResponse;
import com.horical.gito.interactor.api.response.user.ForgotPasswordResponse;
import com.horical.gito.interactor.api.response.user.GetAllUserResponse;
import com.horical.gito.interactor.api.response.user.GetCreateMemberResponse;
import com.horical.gito.interactor.api.response.user.LoginResponse;
import com.horical.gito.interactor.api.response.user.RegisterResponse;
import com.horical.gito.interactor.api.response.version.GetAllVersionResponse;
import com.horical.gito.interactor.api.response.version.GetVersionResponse;
import com.horical.gito.model.Accounting;
import com.horical.gito.model.Department;
import com.horical.gito.model.Feature;
import com.horical.gito.model.Information;
import com.horical.gito.model.InputAddon;
import com.horical.gito.model.Level;
import com.horical.gito.model.Metric;
import com.horical.gito.model.Reports;
import com.horical.gito.model.Staff;
import com.horical.gito.model.Task;
import com.horical.gito.model.estate.Estate;
import com.horical.gito.model.estate.WarehouseEstate;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiServices {

    @POST("user/register")
    Call<RegisterResponse> register(
            @Header("media-type") String mediaType,
            @Body RegisterRequest registerRequest
    );

    @POST("user/login")
    Call<LoginResponse> login(
            @Header("media-type") String mediaType,
            @Body LoginRequest loginRequest
    );

    @POST("user/logout")
    Call<BaseResponse> logout(
            @Header("token") String token
    );

    @POST("mail/send")
    Call<ForgotPasswordResponse> forgotPassword(
            @Body ForgotPasswordRequest forgetPasswordRequest
    );

    @POST("mypage")
    Call<GetMyPageResponse> getMyPage(
            @Header("token") String token,
            @Body GetMyPageRequest request
    );

    @GET("project")
    Call<GetAllProjectResponse> getAllProject(
            @Header("token") String token
    );

    @POST("project")
    Call<CreateProjectResponse> createProject(
            @Header("token") String token,
            @Body CreateProjectRequest projectRequest
    );

    @GET("project/{projectId}")
    Call<GetProjectResponse> getProject(
            @Header("token") String token,
            @Path("projectId") String projectId
    );

    @GET("project/{projectId}/userrole")
    Call<GetMemberAndRoleResponse> getMemberAndRole(
            @Header("token") String token,
            @Path("projectId") String projectId
    );

    @GET("project/{projectId}/userrole")
    Call<GetMemberResponse> getMembers(
            @Header("token") String token,
            @Path("projectId") String projectId
    );

    @POST("project/{projectId}/logtime/filteruser")
    Call<GetMembersHaveLogTimeResponse> getLogTimes(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Body GetMembersHaveLogTimeRequest logTimesRequest
    );

    @POST("project/{projectId}/logtime/filter")
    Call<GetLogTimeResponse> getLogTime(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Body GetLogTimeRequest logTimeRequest
    );

    @PUT("project/{projectId}/logtime/{logTimeId}/approved")
    Call<UpdateLogTimeResponse> updateLogTime(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Path("logTimeId") String logTimeId,
            @Body UpdateLogTimeRequest logTimeRequest
    );

    @PUT("project/{projectId}")
    Call<UpdateProjectResponse> updateProject(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Body UpdateProjectRequest project
    );

    @POST("project/{projectId}/summary")
    Call<GetSummaryResponse> getSummary(
            @Header("token") String token,
            @Path("projectId") String projectId
    );

    @GET("project/{projectId}/version")
    Call<GetAllVersionResponse> getAllVersion(
            @Header("token") String token,
            @Path("projectId") String projectId
    );

    @DELETE("project/{projectId}/version/{versionId}")
    Call<BaseResponse> deleteVersion(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Path("versionId") String versionId
    );

    @GET("/project/{projectId}/version/{versionId}")
    Call<GetVersionResponse> getVersion(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Path("versionId") String versionId
    );

    @POST("/project/{projectId}/version")
    Call<BaseResponse> createVersion(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Body Request request
    );

    @PUT("/project/{projectId}/version/{versionId}")
    Call<GetVersionResponse> updateVersion(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Path("versionId") String versionId,
            @Body Request request
    );

    @GET("project/{projectId}/component")
    Call<GetAllComponentResponse> getAllComponent(
            @Header("token") String token,
            @Path("projectId") String projectId);

    @DELETE("project/{projectId}/component/{componentId}")
    Call<BaseResponse> deleteComponent(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Path("componentId") String componentId
    );

    @POST("project/{projectId}/component")
    Call<CreateComponentResponse> createComponent(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Body CreateComponentRequest createComponentRequest
    );

    @GET("project/{projectId}/group")
    Call<GetAllGroupResponse> getAllGroup(
            @Header("token") String token,
            @Path("projectId") String projectId
    );

    @GET("project/{projectId}/task")
    Call<GetAllTaskResponse> getAllTask(
            @Header("token") String token,
            @Path("projectId") String projectId
    );

    @POST("project/{projectId}/task")
    Call<GetCreateTaskResponse> createTask(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Body Task task
    );

    @DELETE("project/{projectId}/task/{taskID}")
    Call<GetDeleteTaskResponse> deleteTask(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Path("taskID") String taskId
    );

    @GET("project/{projectId}/document")
    Call<GetAllDocumentResponse> getAllDocument(
            @Header("token") String token,
            @Path("projectId") String projectId
    );


    @GET("meeting")
    Call<GetAllMeetingResponse> getAllMeeting(
            @Header("token") String token
    );

    @POST("meeting")
    Call<GetCreateMeetingResponse> createMeeting(
            @Header("token") String token,
            @Body MeetingRequest meeting
    );

    @DELETE("meeting/{meetingId}")
    Call<BaseResponse> deleteMeeting(
            @Header("token") String token,
            @Path("meetingId") String componentId
    );

    @PUT("meeting/{meetingId}")
    Call<GetUpdateMeetingResponse> updateContentMeeting(
            @Header("token") String token,
            @Path("meetingId") String meetingId,
            @Body MeetingRequest request
    );

    @GET("roombooking")
    Call<GetAllRoomBookingResponse> getAllRoomBooking(
            @Header("token") String token
    );

    @GET("roombooking/{roomBookingId}")
    Call<GetRoomBookingResponse> getRoomBooking(
            @Header("token") String token,
            @Path("RoomBookingId") String roomBookingId
    );

    @GET("room")
    Call<GetAllRoomResponse> getAllRoom(
            @Header("token") String token
    );

    @POST("room")
    Call<GetCreateRoomResponse> createRoom(
            @Header("token") String token,
            @Body RoomRequest room
    );

    @GET("checkIn")
    Call<GetAllCheckinResponse> getAllCheckin(
            @Header("token") String token
    );

    @GET("checkIn/{checkinId}")
    Call<GetCheckinResponse> getCheckin(
            @Header("token") String token,
            @Path("checkinId") String checkinId
    );

    @GET("project/{projectId}/code/{perPage}/{page}")
    Call<GetAllCodeResponse> getAllCode(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Path("perPage") int perPage,
            @Path("page") int page
    );

    @POST("project/{projectId}/code")
    Call<GetCreateCodeResponse> createCode(
            @Header("token") String token,
            @Path("projectId") String projectId,
            @Body String name
    );


    @GET("todolist")
    Call<GetAllTodoListResponse> getAllTodoList(
            @Header("token") String token
    );

    @DELETE("todolist/{todolistId}")
    Call<BaseResponse> deleteTodoList(
            @Header("token") String token,
            @Path("todolistId") String todolistId
    );

    @DELETE("todolist/{todolistId}/attachfile")
    Call<BaseResponse> deleteFileTodo(
            @Header("token") String token,
            @Path("todolistId") String componentId
    );

    @POST("todolist")
    Call<GetCreateTodoListResponse> createTodoList(
            @Header("token") String token,
            @Body TodoRequest todoList
    );

    @PUT("todolist/{todolistId}")
    Call<GetUpdateTodoListResponse> updateContentTodo(
            @Header("token") String token,
            @Path("todolistId") String todolistId,
            @Body TodoRequest request
    );

    @GET("note")
    Call<GetAllNoteResponse> getAllNote(
            @Header("token") String token
    );

    @DELETE("note/{noteId}")
    Call<BaseResponse> deleteNote(
            @Header("token") String token,
            @Path("noteId") String noteId
    );

    @DELETE("note/{noteId}/attachfile")
    Call<BaseResponse> deleteFileNote(
            @Header("token") String token,
            @Path("noteId") String noteId,
            @Body UpdateAttachFileNoteRequest attachFileRequest
    );

    @POST("note")
    Call<GetCreateNoteResponse> createNote(
            @Header("token") String token,
            @Body NoteRequest note
    );

    @PUT("note/{noteId}")
    Call<GetUpdateNoteResponse> updateContentNote(
            @Header("token") String token,
            @Path("noteId") String noteId,
            @Body NoteRequest request
    );

    @GET("accounting")
    Call<GetAllAccountingResponse> getAllAccounting(
            @Header("token") String token
    );

    @DELETE("accounting/{accountingId}")
    Call<BaseResponse> deleteAccounting(
            @Header("token") String token,
            @Path("accountingId") String accountingId
    );

    @POST("accounting")
    Call<GetCreateAccountingResponse> createAccounting(
            @Header("token") String token,
            @Body Accounting accounting
    );

    @POST("filter/casestudy")
    Call<GetAllCaseStudyResponse> getAllCaseStudy(
            @Header("token") String token,
            @Body GetAllCaseStudyRequest caseStudyRequest
    );

    @POST("casestudy")
    Call<GetCreateCaseStudyResponse> createCaseStudy(
            @Header("token") String token,
            @Body CreateCaseStudyRequest caseStudyRequest
    );

    @DELETE("casestudy/{caseStudyId}")
    Call<BaseResponse> deleteCaseStudy(
            @Header("token") String token,
            @Path("caseStudyId") String id
    );

    @PUT("casestudy/{caseStudyId}")
    Call<GetUpdateCaseStudyResponse> updateCaseStudy(
            @Header("token") String token,
            @Path("caseStudyId") String componentId,
            @Body UpdateCaseStudyRequest caseStudyRequest
    );

    @PUT("casestudy/{caseStudyId}/approved")
    Call<GetUpdateStatusCaseStudyResponse> updateStatusCaseStudy(
            @Header("token") String token,
            @Path("caseStudyId") String id,
            @Body UpdateStatusCaseStudyRequest caseStudyRequest
    );

    @POST("casestudy/{caseStudyId}/attachfiles")
    Call<GetUpdateAttachFileResponse> updateAttachFile(
            @Header("token") String token,
            @Path("caseStudyId") String id,
            @Body UpdateAttachFileRequest attachFileRequest
    );

    @DELETE("casestudy/{caseStudyId}/attachfiles")
    Call<BaseResponse> deleteAttachFile(
            @Header("token") String token,
            @Path("caseStudyId") String id,
            @Body UpdateAttachFileRequest attachFileRequest
    );


    @GET("company")
    Call<GetCompanyInfoResponse> getCompanyInfo(
            @Header("token") String token
    );

    @PUT("company")
    Call<GetUpdateInfoResponse> updateInfo(
            @Header("token") String token,
            @Body Information information
    );


    @GET("company/quota")
    Call<GetInfoQuotaResponse> getInfoQuota(
            @Header("token") String token
    );

    @GET("summary/quota")
    Call<GetSummaryQuotaResponse> getSummaryQuota(
            @Header("token") String token
    );

    @GET("company/feature")
    Call<GetFeatureResponse> getFeature(
            @Header("token") String token
    );

    @PUT("company/feature")
    Call<GetUpdateFeatureResponse> updateFeature(
            @Header("token") String token,
            @Body Feature feature
    );

    @GET("device")
    Call<GetDevicesResponse> getAllDevice(
            @Header("token") String token
    );

    @DELETE("device/{idDevice}")
    Call<BaseResponse> deleteDevice(
            @Header("token") String token,
            @Path("idDevice") String idDevice
    );

    @GET("level")
    Call<GetAllLevelResponse> getAllLevel(
            @Header("token") String token
    );

    @DELETE("level/{idLevel}")
    Call<BaseResponse> deleteLevel(
            @Header("token") String token,
            @Path("idLevel") String idLevel
    );

    @PUT("level/{idLevel}")
    Call<GetUpdateLevelResponse> updateLevel(
            @Header("token") String token,
            @Path("idLevel") String idLevel,
            @Body Level level
    );

    @POST("level")
    Call<CreateLevelResponse> createLevel(
            @Header("token") String token,
            @Body CreateLevelRequest createLevelRequest
    );

    @DELETE("user/delete/{idUser}")
    Call<BaseResponse> deleteMember(
            @Header("token") String token,
            @Path("idUser") String idUser
    );

    @PUT("user/update/{idUser}")
    Call<BaseResponse> updateMember(
            @Header("token") String token,
            @Path("idUser") String idUser,
            @Body UpdateMemberRequest memberRequest
    );

    @PUT("user/changepass")
    Call<BaseResponse> changePasswordByUser(
            @Header("token") String token,
            @Body ChangePassWordRequest changePassWordRequest
    );

    @PUT("user/update/{idUser}")
    Call<BaseResponse> changePasswordByAdmin(
            @Header("token") String token,
            @Path("idUser") String idUser,
            @Body ChangePassWordRequest changePassWordRequest
    );

    @GET("department")
    Call<GetAllDepartmentResponse> getAllDepartment(
            @Header("token") String token
    );

    @DELETE("department/{idDepartment}")
    Call<BaseResponse> deleteDepartment(
            @Header("token") String token,
            @Path("idDepartment") String idDepartment
    );

    @PUT("department/{idDepartment}")
    Call<GetUpdateDepartmentResponse> updateDepartment(
            @Header("token") String token,
            @Path("idDepartment") String idDepartment,
            @Body Department department
    );

    @POST("department")
    Call<CreateDepartmentResponse> createDepartment(
            @Header("token") String token,
            @Body CreateDepartmentRequest createDepartmentRequest
    );

    @POST("user/adduser")
    Call<GetCreateMemberResponse> createMember(
            @Header("token") String token,
            @Body CreateMemberRequest createMemberRequest
    );


    @GET("rolecompany")
    Call<GetAllCompanyRoleResponse> getAllCompanyRole(
            @Header("token") String token
    );

    @DELETE("rolecompany/{idCompanyRole}")
    Call<BaseResponse> deleteCompanyRole(
            @Header("token") String token,
            @Path("idCompanyRole") String idCompanyRole
    );

    @GET("role")
    Call<GetAllProjectRoleResponse> getAllProjectRole(
            @Header("token") String token
    );

    @DELETE("role/{idProjectRole}")
    Call<BaseResponse> deleteProjectRole(
            @Header("token") String token,
            @Path("idProjectRole") String idProjectRole
    );

    @GET("holiday")
    Call<GetAllHolidayResponse> getAllHoliday(
            @Header("token") String token
    );

    @GET("leavingregister")
    Call<GetAllLeavingResponse> getAllLeaving(
            @Header("token") String token
    );

    @GET("leavingregister/{userId}")
    Call<GetLeavingByIdResponse> getLeavingById(
            @Header("token") String token,
            @Path("userId") String userId
    );

    @POST("leavingregister")
    Call<CreateLeavingResponse> createLeavingRegister(
            @Header("token") String token,
            @Body CreateLeavingRequest request
    );

    @GET("charge")
    Call<GetAllHistoryResponse> getAllHistory(
            @Header("token") String token
    );

    @GET("card")
    Call<GetAllCardResponse> getAllCard(
            @Header("token") String token
    );

    // Survey
    @GET("survey/getallsurveyofme")
    Call<GetAllSurveyResponse> getAllSurvey(
            @Header("token") String token
    );

    @GET("survey/getallsurveyapproved")
    Call<GetAllSurveyApprovedResponse> getAllSurveyApproved(
            @Header("token") String token
    );

    @GET("survey/getallsurveypending")
    Call<GetAllSurveyPendingResponse> getAllSurveyPending(
            @Header("token") String token
    );

    @GET("survey/getsubmiter")
    Call<GetSubmiterResponse> getSubmiter(
            @Header("token") String token
    );

    @GET("/survey/{surveyId}/getallsurveyitem")
    Call<GetAllSurveyItemResponse> getAllSurveyItem(
            @Header("token") String token,
            @Path("surveyId") String surveyId
    );

    @GET("user")
    Call<GetAllUserResponse> getAllUser(
            @Header("token") String token
    );

    @GET("estatewarehouse")
    Call<GetAllWarehourseEstateResponse> getWarehourseEstate(
            @Header("token") String token
    );

    @POST("estatewarehouse")
    Call<CreateWarehouseEstateResponse> createWarehouseEstate(
            @Header("token") String token,
            @Body WarehouseEstate warehouseEstate
    );

    @DELETE("estatewarehouse/{warehouseId}")
    Call<BaseResponse> deleteWarehouseEstate(
            @Header("token") String token,
            @Path("warehouseId") String warehouseId
    );

    @GET("estatewarehouse/estate/{warehouseId}")
    Call<GetAllEstatesResponse> getAllEstate(
            @Header("token") String token,
            @Path("warehouseId") String warehouseId
    );

    @POST("estatewarehouse/estate/{warehouseId}")
    Call<CreateEstateResponse> createEstateItem(
            @Header("token") String token,
            @Path("warehouseId") String warehouseId,
            @Body Estate estate
    );

    @DELETE("estatewarehouse/estate/{warehouseId}/{estateItemId}")
    Call<BaseResponse> deleteEstateItem(
            @Header("token") String token,
            @Path("warehouseId") String warehouseId,
            @Path("estateItemId") String estateItemId
    );

    //get report since id
    @POST("report/reportfilter")
    Call<MemberResponse> getMemberReport(
            @Header("token") String token,
            @Body MemberRequest createDepartmentRequest
    );

    //get all report
    @GET("report")
    Call<AllReportResponse> getAllReport(
            @Header("token") String token
    );

    //update report
    @PUT("report/{idReport}")
    Call<BaseResponse> updateReport(
            @Header("token") String token,
            @Path("idReport") String id,
            @Body Reports project
    );

    @PUT("report/{idReport}/approved")
    Call<BaseResponse> updateReportSubmitAndReject(
            @Header("token") String token,
            @Path("idReport") String id,
            @Body RejectApprovedRequest project
    );

    //create report
    @POST("report")
    Call<BaseResponse> createReport(
            @Header("token") String token,
            @Body NewReportRequest createReportRequest
    );

    @GET("leavingregister/approver/user")
    Call<GetAllUserResponse> getUserReport(
            @Header("token") String token
    );

    @GET("project/{id}/group/")
    Call<GroupResponse> getGroupReport(
            @Header("token") String token,
            @Path("id") String id
    );

    /* Salary Staff */
    //get all salary staff
    @GET("salarystaff/")
    Call<GetAllSalaryStaffResponse> getAllSalaryStaff(
            @Header("token") String token
    );

    //get salary staff by Id
    @GET("salarystaff/{salarystaffId}")
    Call<GetDetailSalaryStaffResponse> getSalaryStaffById(
            @Header("token") String token,
            @Path("salarystaffId") String salarystaffId
    );

    //create salary staff
    @POST("salarystaff")
    Call<GetCreateSalaryStaffResponse> createSalaryStaff(
            @Header("token") String token,
            @Body Staff staff
    );

    //update salary staff
    @PUT("salarystaff/{salarystaffId}")
    Call<GetUpdateSalaryStaffResponse> updateSalaryStaff(
            @Header("token") String token,
            @Path("salarystaffId") String salarystaffId,
            @Body Staff staff
    );

    //delete salary staff
    @DELETE("salarystaff/{salarystaffId}")
    Call<GetDeleteSalaryStaffResponse> deleteSalaryStaff(
            @Header("token") String token,
            @Path("salarystaffId") String salarystaffId
    );

    /* Salary Metric Group */
    //get all metric group
    @GET("metricgroup/")
    Call<GetAllMetricGroupResponse> getAllMetricGroup(
            @Header("token") String token
    );

    //get metric group by Id
    @GET("metricgroup/{metricGroupId}")
    Call<GetDetailMetricGroupResponse> getMetricGroupById(
            @Header("token") String token,
            @Path("metricGroupId") String metricGroupId
    );

    //create metric group
    @POST("metricgroup")
    Call<GetCreateMetricGroupResponse> createMetricGroup(
            @Header("token") String token,
            @Body Metric metric
    );

    //update metric group
    @PUT("metricgroup/{metricGroupId}")
    Call<GetUpdateMetricGroupResponse> updateMetricGroup(
            @Header("token") String token,
            @Path("metricGroupId") String metricGroupId,
            @Body Metric metric
    );

    //delete metric group
    @DELETE("metricgroup/{metricGroupId}/")
    Call<GetDeleteMetricGroupResponse> deleteMetricGroup(
            @Header("token") String token,
            @Path("metricGroupId") String metricGroupId
    );

    //add metric to metric group
    @POST("metricgroup/{metricGroupId}/metric")
    Call<GetAddMetricMetricGroupResponse> addMetricToMetricGroup(
            @Header("token") String token,
            @Path("metricGroupId") String metricGroupId,
            @Body Metric metric
    );

    //delete metric from metric group
    @DELETE("metricgroup/{metricGroupId}/metric")
    Call<GetDeleteMetricMetricGroupResponse> deleteMetricFromMetricGroup(
            @Header("token") String token,
            @Path("metricGroupId") String metricGroupId,
            @Body String metricId
    );

    /* Salary Metric */
    //get all salary metric
    @GET("salarymetric/")
    Call<GetAllMetricResponse> getAllMetric(
            @Header("token") String token
    );

    //get salary metric by Id
    @GET("salarymetric/{salarymetricId}")
    Call<GetDetailMetricResponse> getMetricById(
            @Header("token") String token,
            @Path("salarymetricId") String salarymetricId
    );

    //create salary metric
    @POST("salarymetric")
    Call<GetCreateMetricResponse> createMetric(
            @Header("token") String token,
            @Body Metric metric
    );

    //update salary metric
    @PUT("salarymetric/{salarymetricId}")
    Call<GetUpdateMetricResponse> updateMetric(
            @Header("token") String token,
            @Path("salarymetricId") String salarymetricId,
            @Body Metric metric
    );

    //delete salary metric
    @DELETE("salarymetric/{salarymetricId}/")
    Call<GetDeleteMetricGroupResponse> deleteMetric(
            @Header("token") String token,
            @Path("salarymetricId") String salarymetricId
    );

    /* Salary Param */
    //add param
    @POST("salarymetric/salarymetricId/param")
    Call<GetAddParamResponse> addMetricParam(
            @Header("token") String token,
            @Body AddStepParamRequest addStepParamRequest
    );

    //delete param
    @DELETE("salarymetric/{salarymetricId}/param")
    Call<GetDeleteParamResponse> deleteMetricParam(
            @Header("token") String token,
            @Path("salarymetricId") String salarymetricId,
            @Body DeleteStepParamRequest deleteStepParamRequest
    );

    /* Salary Input Addon */
    //get all input addon
    @GET("salaryinputaddon")
    Call<GetAllInputAddonResponse> getAllInputAddon(
            @Header("token") String token
    );

    //get input addon by Id
    @GET("salaryinputaddon/{inputaddonId}")
    Call<GetDetailInputAddonResponse> getInputAddonById(
            @Header("token") String token,
            @Path("inputaddonId") String inputaddonId
    );

    //filter input addon
    @POST("filter/bonus")
    Call<GetFilterInputAddonResponse> filterInputAddon(
            @Header("token") String token,
            @Body FilterInputAddonRequest filterInputAddonRequest
    );

    //create input addon
    @POST("salaryinputaddon")
    Call<GetCreateInputAddonResponse> createInputAddon(
            @Header("token") String token,
            @Body CreateInputAddonRequest createInputAddonRequest
    );

    //update input addon
    @PUT("salaryinputaddon/{inputaddonId}")
    Call<GetUpdateInputAddonResponse> updateInputAddon(
            @Header("token") String token,
            @Path("inputaddonId") String inputaddonId,
            @Body InputAddon addon
    );

    //delete salary input addon
    @DELETE("salaryinputaddon/{inputaddonId}")
    Call<GetDeleteInputAddonResponse> deleteInputAddon(
            @Header("token") String token,
            @Path("inputaddonId ") String inputaddonId
    );

    //recruitment
    @GET("recruitment")
    Call<RecruitmentResponse> getListRecruitment(
            @Header("token") String token
    );

    @DELETE("recruitment/{id}")
    Call<BaseResponse> deleteRecruitment(
            @Header("token") String token,
            @Path("id") String id
    );
}