package com.horical.gito.mvp.meeting.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.meeting.list.injection.module.MeetingModule;
import com.horical.gito.mvp.meeting.list.view.MeetingActivity;

import dagger.Component;

/**
 * Created by Lemon on 11/21/2016.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = MeetingModule.class)
public interface MeetingComponent {
    void inject(MeetingActivity meetingActivity);
}
