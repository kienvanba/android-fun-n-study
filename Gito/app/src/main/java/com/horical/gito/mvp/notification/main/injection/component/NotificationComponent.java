package com.horical.gito.mvp.notification.main.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.notification.main.injection.module.NotificationModule;
import com.horical.gito.mvp.notification.main.view.NotificationActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = NotificationModule.class)
public interface NotificationComponent {
    void inject(NotificationActivity activity);
}
