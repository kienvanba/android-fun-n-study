package com.horical.gito.mvp.myProject.task.details.fragment.logtime.injection.module;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.mvp.myProject.task.details.fragment.logtime.presenter.LogTimePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class LogTimeModule {
    @PerFragment
    @Provides
    LogTimePresenter providerLogTimePresenter() {
        return new LogTimePresenter();
    }
}
