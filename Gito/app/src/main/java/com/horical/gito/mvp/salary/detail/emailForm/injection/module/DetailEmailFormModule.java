package com.horical.gito.mvp.salary.detail.emailForm.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.detail.emailForm.presenter.DetailEmailFormPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailEmailFormModule {
    @PerActivity
    @Provides
    DetailEmailFormPresenter provideReportPresenter(){
        return new DetailEmailFormPresenter();
    }
}
