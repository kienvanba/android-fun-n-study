package com.horical.gito.mvp.myPage.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.horical.gito.AppConstants;
import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.customView.CustomViewTopBar;
import com.horical.gito.model.MyPage;
import com.horical.gito.mvp.calendar.view.CalendarActivity;
import com.horical.gito.mvp.caseStudy.list.view.CaseStudyActivity;
import com.horical.gito.mvp.checkin.list.view.dto.CheckinActivity;
import com.horical.gito.mvp.drawer.DrawerActivity;
import com.horical.gito.mvp.leaving.main.view.LeavingActivity;
import com.horical.gito.mvp.meeting.list.view.MeetingActivity;
import com.horical.gito.mvp.myPage.injection.component.DaggerMyPageComponent;
import com.horical.gito.mvp.myPage.injection.component.MyPageComponent;
import com.horical.gito.mvp.myPage.injection.module.MyPageModule;
import com.horical.gito.mvp.myPage.presenter.MyPagePresenter;
import com.horical.gito.mvp.notification.main.view.NotificationActivity;
import com.horical.gito.mvp.popup.dateSelected.DateDto;
import com.horical.gito.mvp.popup.dateSelected.SelectDatePopup;
import com.horical.gito.mvp.recruitment.view.RecruitmentActivity;
import com.horical.gito.mvp.report.view.ReportActivity;
import com.horical.gito.mvp.todolist.todo.list.view.TodoListActivity;
import com.horical.gito.utils.DateUtils;
import com.horical.gito.widget.textview.GitOTextView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.horical.gito.AppConstants.FROM_MY_PAGE;
import static com.horical.gito.AppConstants.KEY_FROM;

public class MyPageActivity extends DrawerActivity implements IMyPageView, View.OnClickListener {

    @Bind(R.id.view_bar_top)
    CustomViewTopBar topBar;

    @Bind(R.id.date_filter)
    TextView tvDateFilter;

    // My Task
    @Bind(R.id.my_page_ln_task)
    LinearLayout lnTask;

    @Bind(R.id.my_page_tv_task_count)
    GitOTextView tvTaskCount;

    @Bind(R.id.my_page_imv_task_next)
    ImageView imvTaskNext;

    @Bind(R.id.my_page_lv_tasks)
    ListView rcvTasks;

    // My Version
    @Bind(R.id.my_page_ln_version)
    LinearLayout lnVersion;

    @Bind(R.id.my_page_tv_version_count)
    GitOTextView tvVersionCount;

    @Bind(R.id.my_page_imv_versions_next)
    ImageView imvVersionNext;

    @Bind(R.id.my_page_lv_versions)
    ListView lvVersions;

    // My Notify
    @Bind(R.id.my_page_ln_notify)
    LinearLayout lnNotify;

    @Bind(R.id.my_page_tv_notify_count)
    GitOTextView tvNotifyCount;

    // My Meeting
    @Bind(R.id.my_page_ln_meeting)
    LinearLayout lnMeeting;

    @Bind(R.id.my_page_tv_meeting_count)
    GitOTextView tvMeetingCount;

    // My Recruitment
    @Bind(R.id.my_page_ln_recruitment)
    LinearLayout lnRecruitment;

    @Bind(R.id.my_page_tv_recruitment_count)
    GitOTextView tvRecruitmentCount;

    // My Report
    @Bind(R.id.my_page_ln_report)
    LinearLayout lnReport;

    @Bind(R.id.my_page_tv_report_count)
    GitOTextView tvReportCount;

    // Birthday Member
    @Bind(R.id.my_page_ln_birthday)
    LinearLayout lnBirthday;

    @Bind(R.id.my_page_tv_birthday_count)
    GitOTextView tvBirthdayCount;

    // To do List
    @Bind(R.id.my_page_ln_to_do_list)
    LinearLayout lnToDoList;

    @Bind(R.id.my_page_tv_to_do_list_count)
    GitOTextView tvToDoListCount;

    // Need Approve Leaving
    @Bind(R.id.my_page_ln_leaving)
    LinearLayout lnLeaving;

    @Bind(R.id.my_page_tv_leaving_count)
    GitOTextView tvLeavingCount;

    // Need Approve Checking
    @Bind(R.id.my_page_ln_checking)
    LinearLayout lnChecking;

    @Bind(R.id.my_page_tv_checking_count)
    GitOTextView tvCheckingCount;

    // Log Time Need Approve
    @Bind(R.id.my_page_ln_log_time)
    LinearLayout lnLogTime;

    @Bind(R.id.my_page_imv_log_time_count)
    GitOTextView tvLogTimeCount;

    @Bind(R.id.my_page_imv_log_time_next)
    ImageView imvLogTimeNext;

    @Bind(R.id.my_page_lv_log_times)
    ListView lvLogTimes;

    // CaseStudy Need to be Approve
    @Bind(R.id.my_page_ln_case_study)
    LinearLayout lnCaseStudy;

    @Bind(R.id.my_page_tv_case_study_count)
    GitOTextView tvCaseStudyCount;

    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout refreshLayout;

    @Inject
    MyPagePresenter mPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_page;
    }

    @Override
    protected int getNavId() {
        return AppConstants.NAV_DRAWER_ID_MY_PAGE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        MyPageComponent component = DaggerMyPageComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .myPageModule(new MyPageModule())
                .build();

        component.inject(this);
        mPresenter.attachView(this);
        mPresenter.onCreate();

        initData();
        initListener();
    }


    protected void initData() {
        topBar.setTextTitle(getString(R.string.my_page));
        topBar.setImageViewLeft(CustomViewTopBar.LEFT_MENU);

        if (mPresenter.getCurrentDateFilter() == null) {
            mPresenter.setCurrentDateFilter(new DateDto(
                    getString(R.string.today),
                    DateUtils.getTodayStart(),
                    DateUtils.getTodayEnd()));
        }
        tvDateFilter.setText(mPresenter.getCurrentDateFilter().getTypeDate());
        mPresenter.getData();
    }

    protected void initListener() {
        tvDateFilter.setOnClickListener(this);
        lnTask.setOnClickListener(this);
        lnVersion.setOnClickListener(this);
        lnNotify.setOnClickListener(this);
        lnMeeting.setOnClickListener(this);
        lnRecruitment.setOnClickListener(this);
        lnReport.setOnClickListener(this);
        lnBirthday.setOnClickListener(this);
        lnToDoList.setOnClickListener(this);
        lnLeaving.setOnClickListener(this);
        lnChecking.setOnClickListener(this);
        lnLogTime.setOnClickListener(this);
        lnCaseStudy.setOnClickListener(this);

        topBar.setOnClickListener(new CustomViewTopBar.OnItemClickListener() {
            @Override
            public void onLeftClicked() {
                openDrawer();
            }

            @Override
            public void onRightButtonOneClicked() {

            }

            @Override
            public void onRightButtonTwoClicked() {

            }

            @Override
            public void onRightButtonThreeClicked() {

            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getData();
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == tvDateFilter.getId()) {
            SelectDatePopup selectDateRangePopup = new SelectDatePopup(
                    this,
                    mPresenter.getCurrentDateFilter(),
                    new SelectDatePopup.OnItemSelectedListener() {
                        @Override
                        public void onClickItemDialog(DateDto dateRange) {
                            mPresenter.setCurrentDateFilter(dateRange);
                            tvDateFilter.setText(dateRange.getTypeDate());
                            mPresenter.getData();
                        }
                    });
            selectDateRangePopup.showDateDefault(
                    tvDateFilter,
                    80,
                    SelectDatePopup.GRAVITY_END,
                    0);
        } else if (id == lnTask.getId()) {
        } else if (id == lnVersion.getId()) {
        } else if (id == lnNotify.getId()) {
            Intent intent = new Intent(this, NotificationActivity.class);
            intent.putExtra(KEY_FROM, FROM_MY_PAGE);
            startActivity(intent);
        } else if (id == lnMeeting.getId()) {
            Intent intent = new Intent(this, MeetingActivity.class);
            intent.putExtra(KEY_FROM, FROM_MY_PAGE);
            startActivity(intent);
        } else if (id == lnRecruitment.getId()) {
            Intent intent = new Intent(this, RecruitmentActivity.class);
            intent.putExtra(KEY_FROM, FROM_MY_PAGE);
            startActivity(intent);
        } else if (id == lnReport.getId()) {
            Intent intent = new Intent(this, ReportActivity.class);
            intent.putExtra(KEY_FROM, FROM_MY_PAGE);
            startActivity(intent);
        } else if (id == lnBirthday.getId()) {
            Intent intent = new Intent(this, CalendarActivity.class);
            intent.putExtra(KEY_FROM, FROM_MY_PAGE);
            startActivity(intent);
        } else if (id == lnToDoList.getId()) {
            Intent intent = new Intent(this, TodoListActivity.class);
            intent.putExtra(KEY_FROM, FROM_MY_PAGE);
            startActivity(intent);
        } else if (id == lnLeaving.getId()) {
            Intent intent = new Intent(this, LeavingActivity.class);
            intent.putExtra(KEY_FROM, FROM_MY_PAGE);
            startActivity(intent);
        } else if (id == lnChecking.getId()) {
            Intent intent = new Intent(this, CheckinActivity.class);
            intent.putExtra(KEY_FROM, FROM_MY_PAGE);
            startActivity(intent);
        } else if (id == lnLogTime.getId()) {

        } else if (id == lnCaseStudy.getId()) {
            Intent intent = new Intent(this, CaseStudyActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        mPresenter.detachView();
        mPresenter.onDestroy();
    }

    @Override
    public void showLoading() {
        showDialog(getString(R.string.loading));
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getMyPageFailure(String message) {
        showErrorDialog(message);
        if (refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void getMyPageSuccess(MyPage myPage) {
        String count = myPage.getTasks().size() +
                " " +
                getString(R.string.task);
        tvTaskCount.setText(count);

        count = myPage.getVersions().size() +
                " " +
                getString(R.string.item);
        tvVersionCount.setText(count);

        count = myPage.getAnnouncement() +
                " " +
                getString(R.string.item);
        tvNotifyCount.setText(count);

        count = myPage.getMeeting() +
                " " +
                getString(R.string.meeting).toLowerCase();
        tvMeetingCount.setText(count);

        count = myPage.getRecruitment() +
                " " +
                getString(R.string.candidates);
        tvRecruitmentCount.setText(count);

        count = myPage.getReport() +
                " " +
                getString(R.string.report).toLowerCase();
        tvReportCount.setText(count);

        count = myPage.getReport() +
                " " +
                getString(R.string.member).toLowerCase();
        tvBirthdayCount.setText(count);

        count = myPage.getToDoList() +
                " " +
                getString(R.string.list);
        tvToDoListCount.setText(count);

        count = myPage.getLeavingRegister() +
                " " +
                getString(R.string.staff).toLowerCase();
        tvLeavingCount.setText(count);

        count = myPage.getCheckIn() + "" +
                " " +
                getString(R.string.user).toLowerCase();
        tvCheckingCount.setText(count);

        count = myPage.getLogTimes().size() +
                " " +
                getString(R.string.item);
        tvLogTimeCount.setText(count);

        count = myPage.getCaseStudy() +
                " " +
                getString(R.string.item);
        tvCaseStudyCount.setText(count);

        if (refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
    }
}
