package com.horical.gito.mvp.accounting.detail.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.horical.gito.MainApplication;
import com.horical.gito.R;
import com.horical.gito.base.BaseActivity;
import com.horical.gito.model.Accounting;
import com.horical.gito.mvp.accounting.detail.adapter.AccountingDetailAdapter;
import com.horical.gito.mvp.accounting.detail.injection.component.AccountingDetailComponent;
import com.horical.gito.mvp.accounting.detail.injection.component.DaggerAccountingDetailComponent;
import com.horical.gito.mvp.accounting.detail.injection.module.AccountingDetailModule;
import com.horical.gito.mvp.accounting.detail.presenter.AccountingDetailPresenter;
import com.horical.gito.mvp.accounting.list.view.AccountingActivity;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AccountingDetailActivity extends BaseActivity implements IAccountingDetailView {

    public static final String ARG_ACCOUNTING = "Accounting";

    @Bind(R.id.imv_accounting_detail_back)
    ImageView ivAccountingBack;

    @Bind(R.id.rcv_accounting_detail)
    RecyclerView rcvAccountingDetail;

    @Bind(R.id.accountingdetail_refresh_layout)
    SwipeRefreshLayout mRefreshDetail;

    private Accounting accounting;

    AccountingDetailAdapter mAccountingDetailAdapter;

    @Inject
    AccountingDetailPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounting_detail);
        ButterKnife.bind(this);

        AccountingDetailComponent component = DaggerAccountingDetailComponent.builder()
                .applicationComponent(MainApplication.getAppComponent())
                .accountingDetailModule(new AccountingDetailModule())
                .build();
        component.inject(this);

        mPresenter.attachView(this);
        mPresenter.onCreate();

        accounting = (Accounting) getIntent().getSerializableExtra(ARG_ACCOUNTING);

        initData();
        initListener();
    }

    protected void initData() {

        mAccountingDetailAdapter = new AccountingDetailAdapter(this, mPresenter.getListAccountings());
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcvAccountingDetail.setLayoutManager(llm);
        rcvAccountingDetail.setAdapter(mAccountingDetailAdapter);

        showLoading();
        mPresenter.getAllAccountingDetail();
    }


    protected void initListener() {
        mRefreshDetail.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                mPresenter.getAllAccountingDetail();
            }
        });

        ivAccountingBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(AccountingDetailActivity.this, AccountingActivity.class);
                startActivity(back);
            }
        });
    }

    @Override
    public void showLoading() {
        showDialog("Loading");
    }

    @Override
    public void hideLoading() {
        dismissDialog();
    }

    @Override
    public void getAllAccountingSuccess() {
        dismissDialog();
        mRefreshDetail.setRefreshing(false);
        mAccountingDetailAdapter.notifyDataSetChanged();
        showToast("Success");
    }

    @Override
    public void getAllAccountingFailure(String error) {
        dismissDialog();
        mRefreshDetail.setRefreshing(false);
        showToast("Failure");
    }

    @Override
    public void deleteAccountingSuccess(int position) {

    }

    @Override
    public void deleteAccountingFailure() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        mPresenter.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            showLoading();
            mPresenter.getAllAccountingDetail();
        }
    }
}
