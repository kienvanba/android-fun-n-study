package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Feature implements Serializable {

    @SerializedName("hfWareHouse")
    @Expose
    public boolean hfWareHouse;

    @SerializedName("hfSetting")
    @Expose
    public boolean hfSetting;

    @SerializedName("hfRecruit")
    @Expose
    public boolean hfRecruit;

    @SerializedName("hfBookingRoom")
    @Expose
    public boolean hfBookingRoom;

    @SerializedName("hfLeavingRegister")
    @Expose
    public boolean hfLeavingRegister;

    @SerializedName("hfCheckIn")
    @Expose
    public boolean hfCheckIn;

    @SerializedName("hfSalary")
    @Expose
    public boolean hfSalary;

    @SerializedName("hfTestCase")
    @Expose
    public boolean hfTestCase;

    @SerializedName("hfUpload")
    @Expose
    public boolean hfUpload;

    @SerializedName("hfCaseStudy")
    @Expose
    public boolean hfCaseStudy;

    @SerializedName("hfEmail")
    @Expose
    public boolean hfEmail;

    @SerializedName("hfChat")
    @Expose
    public boolean hfChat;

    @SerializedName("hfToDoList")
    @Expose
    public boolean hfToDoList;

    @SerializedName("hfNote")
    @Expose
    public boolean hfNote;

    @SerializedName("hfMeeting")
    @Expose
    public boolean hfMeeting;

    @SerializedName("hfGroup")
    @Expose
    public boolean hfGroup;

    @SerializedName("hfChangeLog")
    @Expose
    public boolean hfChangeLog;

    @SerializedName("hfAssess")
    @Expose
    public boolean hfAssess;

    @SerializedName("hfRoomBooking")
    @Expose
    public boolean hfRoomBooking;

    @SerializedName("hfAssessment")
    @Expose
    public boolean hfAssessment;

    @SerializedName("hfEstate")
    @Expose
    public boolean hfEstate;

    @SerializedName("hfAccounting")
    @Expose
    public boolean hfAccounting;

    @SerializedName("hfCheck")
    @Expose
    public boolean hfCheck;

    @SerializedName("hfNotification")
    @Expose
    public boolean hfNotification;

    @SerializedName("hfReport")
    @Expose
    public boolean hfReport;

    @SerializedName("hfLogTime")
    @Expose
    public boolean hfLogTime;

    @SerializedName("hfCode")
    @Expose
    public boolean hfCode;

    @SerializedName("hfDocument")
    @Expose
    public boolean hfDocument;

    @SerializedName("hfCalendar")
    @Expose
    public boolean hfCalendar;

    @SerializedName("hfGantt")
    @Expose
    public boolean hfGantt;

    @SerializedName("hfTask")
    @Expose
    public boolean hfTask;

    @SerializedName("hfMypage")
    @Expose
    public boolean hfMyPage;

    @SerializedName("hfSummary")
    @Expose
    public boolean hfSummary;

    @SerializedName("hfTodo")
    @Expose
    public boolean hfTodo;

    @SerializedName("hfRecruitment")
    @Expose
    public boolean hfRecruitment;

    @SerializedName("hfAnnouncement")
    @Expose
    public boolean hfAnnouncement;

    @SerializedName("hfSurvey")
    @Expose
    public boolean hfSurvey;
}
