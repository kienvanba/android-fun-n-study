package com.horical.gito.mvp.leaving.newLeaving.approverAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dragonoid on 12/20/2016.
 */

public class LeavingSelectedApproverHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.leaving_tv_selected_approver_delete)
    ImageView mDelete;
    @Bind(R.id.leaving_tv_selected_approver_name)
    TextView mSelectedName;
    @Bind(R.id.leaving_tv_selected_approver_position)
    TextView mPosition;
//    @Bind(R.id.leaving_im_selected_approver_avatar)
//    ImageView mAvatar;


    public LeavingSelectedApproverHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);

    }
}
