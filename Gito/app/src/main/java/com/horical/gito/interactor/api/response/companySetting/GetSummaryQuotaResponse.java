package com.horical.gito.interactor.api.response.companySetting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.QuotaSummary;

/**
 * Created by thanhle on 3/30/17.
 */

public class GetSummaryQuotaResponse extends BaseResponse {
    @SerializedName("results")
    @Expose
    public QuotaSummary quotaSummary;
}