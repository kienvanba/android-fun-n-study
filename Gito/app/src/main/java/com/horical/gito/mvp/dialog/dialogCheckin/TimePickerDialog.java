package com.horical.gito.mvp.dialog.dialogCheckin;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.TimePicker;

import com.horical.gito.R;
import com.horical.gito.interactor.prefer.PreferManager;

import javax.inject.Inject;

import butterknife.ButterKnife;


/**
 * Created by Luong on 04-Mar-17.
 */

public class TimePickerDialog extends Dialog implements View.OnClickListener {

    TimePicker TimeIn;
    TimePicker TimeOut;

    TextView Cancel;
    TextView Done;
//    TextView tvTimeIn;
//    TextView tvTimeOut;

    @Inject
    PreferManager mPreferManager;

    private Context mContext;


    public TimePickerDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getWindow() != null) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.item_checkin_timepicker);
        ButterKnife.bind(this);
        InitListener();
    }

    private void InitListener() {
        TimeIn = (TimePicker) findViewById(R.id.TimeIn);
        TimeIn.setIs24HourView(false);
        TimeOut = (TimePicker) findViewById(R.id.Timeout);
        TimeOut.setIs24HourView(false);
        Cancel = (TextView) findViewById(R.id.tv_cancel);
        Done = (TextView) findViewById(R.id.tv_done);
//        tvTimeIn = (TextView) findViewById(R.id.tv_time_in);
//        tvTimeOut = (TextView) findViewById(R.id.tv_time_out);
        Cancel.setOnClickListener(this);
        Done.setOnClickListener(this);
        InitData();
    }

    private void InitData() {
        TimeIn.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                updateDisplay1(hourOfDay, minute);
            }
        });
        TimeOut.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                updateDisplay2(hourOfDay, minute);
            }
        });
    }

    private void updateDisplay1(int hourOfDay, int minute) {

//        tvTimeIn.setText(new StringBuilder().append(pad(hourOfDay)).append(":").append(pad(minute)));
    }

    private void updateDisplay2(int hourOfDay, int minute) {
//        tvTimeOut.setText(new StringBuilder().append(pad(hourOfDay)).append(":").append(pad(minute)));
    }

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_done:
                InitData();
                dismiss();
                break;
            case R.id.tv_cancel:
                cancel();
                break;
        }
    }

}