package com.horical.gito.mvp.caseStudy.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.horical.gito.R;
import com.horical.gito.model.CaseStudy;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CaseStudyAdapter extends RecyclerView.Adapter<CaseStudyAdapter.CaseStudyHolder> {
    public final static int NEW = 0;
    public final static int APPROVED = 1;
    public final static int REJECTED = 2;
    public final static int SUBMITED = 3;
    public static final int PUBLISH = 4;

    private List<CaseStudy> mList;
    private Context mContext;

    private OnItemClickListener mOnItemClickListener;

    public CaseStudyAdapter(Context mContext, List<CaseStudy> mList) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public CaseStudyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_case_study, parent, false);
        return new CaseStudyHolder(view);
    }

    @Override
    public void onBindViewHolder(CaseStudyHolder holder, final int position) {
        CaseStudy caseStudy = mList.get(position);
        holder.mTvTitle.setText(caseStudy.getTitle());
        if (caseStudy.getStatus() == NEW) {
            holder.imgApproved.setVisibility(View.GONE);
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("New");
            holder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.orange_new));
        } else if (caseStudy.getStatus() == APPROVED) {
            holder.imgApproved.setVisibility(View.VISIBLE);
            holder.tvStatus.setVisibility(View.GONE);
        } else if (caseStudy.getStatus() == REJECTED) {
            holder.imgApproved.setVisibility(View.GONE);
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Rejected");
            holder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.red));
        } else {
            holder.imgApproved.setVisibility(View.GONE);
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Submited");
            holder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.green_doing));
        }
        if(caseStudy.getStatus() == SUBMITED){
            holder.llDelete.setVisibility(View.GONE);
        } else {
            holder.llDelete.setVisibility(View.VISIBLE);
        }
        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onDelete(position);
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickListener(position);
            }
        });
    }

    public List<CaseStudy> getListCaseStudy() {
        return mList;
    }

    public void setListCaseStudy(List<CaseStudy> mList) {
        this.mList = mList;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class CaseStudyHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ll_delete)
        LinearLayout llDelete;

        @Bind(R.id.tv_title)
        GitOTextView mTvTitle;

        @Bind(R.id.tv_status)
        GitOTextView tvStatus;

        @Bind(R.id.img_approved)
        ImageView imgApproved;

        View view;

        public CaseStudyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);

        void onDelete(int position);
    }
}
