package com.horical.gito.mvp.myProject.task.creating.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.task.GetCreateTaskResponse;
import com.horical.gito.model.Task;
import com.horical.gito.mvp.myProject.task.creating.view.ITaskCreateNewView;

import static com.horical.gito.utils.LogUtils.makeLogTag;

public class TaskCreateNewPresenter extends BasePresenter implements ITaskCreateNewPresenter{

    private static final String TAG = makeLogTag(TaskCreateNewPresenter.class);

    public ITaskCreateNewView getView() {
        return (ITaskCreateNewView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    @Override
    public void createTask(Task task) {
        String projectId = GitOStorage.getInstance().getProject().getId();

        getApiManager().createTask(projectId, task, new ApiCallback<GetCreateTaskResponse>() {
            @Override
            public void success(GetCreateTaskResponse res) {
                getView().createTaskSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().createTaskFailure(error.message);
            }
        });

    }
}
