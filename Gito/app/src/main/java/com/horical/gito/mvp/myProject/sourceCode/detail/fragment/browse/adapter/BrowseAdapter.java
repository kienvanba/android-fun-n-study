package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.browse.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horical.gito.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Tin on 28-Nov-16.
 */

public class BrowseAdapter extends RecyclerView.Adapter<BrowseAdapter.BrowseHolder> {
    private Context mContext;
    private List<String> mList;
    private OnItemClickListener mOnItemClickListener;

    public BrowseAdapter(Context context, List<String> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public BrowseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_code_browse, parent, false);
        return new BrowseHolder(view);
    }

    @Override
    public void onBindViewHolder(BrowseHolder holder, final int position) {
        String data = mList.get(position);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClickSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class BrowseHolder extends RecyclerView.ViewHolder {

        View view;

        public BrowseHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickSelected(int position);
    }
}
