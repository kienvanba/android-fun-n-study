package com.horical.gito.mvp.myProject.testCase.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.myProject.testCase.presenter.TestCasePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TestCaseModule {
    @PerActivity
    @Provides
    TestCasePresenter provideTestCasePresenter(){
        return new TestCasePresenter();
    }
}
