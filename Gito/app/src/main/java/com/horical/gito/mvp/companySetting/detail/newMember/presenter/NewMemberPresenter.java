package com.horical.gito.mvp.companySetting.detail.newMember.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.request.user.CreateMemberRequest;
import com.horical.gito.interactor.api.response.user.GetCreateMemberResponse;
import com.horical.gito.model.Member;
import com.horical.gito.mvp.companySetting.detail.newMember.view.INewMemberView;

/**
 * Created by Tin on 23-Nov-16.
 */

public class NewMemberPresenter extends BasePresenter implements INewMemberPresenter {
    private String idCompanyRole = "";
    private String idLevel = "";
    private String idDepartment = "";
    public void attachView(INewMemberView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public INewMemberView getView() {
        return (INewMemberView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public String getIdCompanyRole() {
        return idCompanyRole;
    }

    public void setIdCompanyRole(String idCompanyRole) {
        this.idCompanyRole = idCompanyRole;
    }

    public String getIdLevel() {
        return idLevel;
    }

    public void setIdLevel(String idLevel) {
        this.idLevel = idLevel;
    }

    public String getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(String idDepartment) {
        this.idDepartment = idDepartment;
    }

    @Override
    public void createMember(String name, String pass, String email) {
        CreateMemberRequest memberRequest = new CreateMemberRequest();
        memberRequest.department = getIdDepartment();
        memberRequest.displayName = name;
        memberRequest.email = email;
        memberRequest.password = pass;
        memberRequest.roleCompany = getIdCompanyRole();
        memberRequest.level = getIdLevel();
        getApiManager().createMember(memberRequest, new ApiCallback<GetCreateMemberResponse>() {
            @Override
            public void success(GetCreateMemberResponse res) {
                getView().createMemberSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().createMemberFailure(error.message);
            }
        });
    }
}

