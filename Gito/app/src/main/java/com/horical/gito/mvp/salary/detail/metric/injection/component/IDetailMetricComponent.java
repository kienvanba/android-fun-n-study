package com.horical.gito.mvp.salary.detail.metric.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.detail.metric.injection.module.DetailMetricModule;
import com.horical.gito.mvp.salary.detail.metric.view.DetailMetricActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = DetailMetricModule.class)
public interface IDetailMetricComponent {
    void inject(DetailMetricActivity activity);
}
