package com.horical.gito.mvp.allProject.detail.presenter;

import java.io.File;

import okhttp3.MediaType;

public interface IProjectDetailPresenter {

    void createProject(String name, String projectID, String desc);

    void uploadFile(MediaType type, File file);
}
