package com.horical.gito.mvp.salary.list.fragments.subSalary.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.salary.list.fragments.subSalary.presenter.ListSalaryFragmentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListSalaryFragmentModule {
    @PerActivity
    @Provides
    ListSalaryFragmentPresenter provideSalaryPresenter(){
        return new ListSalaryFragmentPresenter();
    }
}
