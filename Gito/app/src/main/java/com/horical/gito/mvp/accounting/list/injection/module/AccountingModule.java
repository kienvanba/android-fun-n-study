package com.horical.gito.mvp.accounting.list.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.accounting.list.presenter.AccountingPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class AccountingModule {
    @Provides
    @PerActivity
    AccountingPresenter provideAccountingPresenter(){
        return new AccountingPresenter();
    }
}
