package com.horical.gito.mvp.salary.dto.metric;

import java.io.Serializable;

public class SummaryDto implements Serializable{
    private String id;
    private String title;
    private String acronym;
    private double total;

    public SummaryDto(String title, String acronym, double total) {
        this.title = title;
        this.acronym = acronym;
        this.total = total;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
