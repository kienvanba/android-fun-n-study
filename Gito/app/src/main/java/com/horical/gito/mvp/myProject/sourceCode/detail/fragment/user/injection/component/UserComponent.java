package com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.injection.module.UserModule;
import com.horical.gito.mvp.myProject.sourceCode.detail.fragment.user.view.UserFragment;

import dagger.Component;

/**
 * Created by Tin on 21-Dec-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class , modules = UserModule.class)
public interface UserComponent {
    void inject(UserFragment fragment);
}
