package com.horical.gito.interactor.api.network.deserialize;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.horical.gito.model.User;
import com.horical.gito.utils.GsonUtils;

import java.lang.reflect.Type;

public class UserDeserialize implements JsonDeserializer<User> {

    @Override
    public User deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        User user;
        try {
            user = GsonUtils.createGson(User.class).fromJson(json, User.class);
        } catch (Exception e) {
            user = new User();
            user.set_id(json.getAsString());
        }
        return user;
    }
}
