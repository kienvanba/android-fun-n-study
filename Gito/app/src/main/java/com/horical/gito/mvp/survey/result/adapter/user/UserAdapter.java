package com.horical.gito.mvp.survey.result.adapter.user;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horical.gito.R;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nhattruong251295 on 4/3/2017.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserHolder>{
    private Context mContext;
    private List<String> mList;
    private OnItemClickListener mOnItemClickListener;

    public UserAdapter(Context context, List<String> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_user_result_survey, parent, false);
        return new UserHolder(view);
    }

    public List<String> getmList() {
        return mList;
    }

    public void setmList(List<String> mList) {
        this.mList = mList;
    }

    @Override
    public void onBindViewHolder(final UserHolder holder, final int position) {
        holder.tvName.setText(mList.get(position));
        holder.tvJob.setText("Coder");
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class UserHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.img_avatar_user)
        CircleImageView imgAvatar;

        @Bind(R.id.tv_shortname_user)
        TextView tvShortName;

        @Bind(R.id.tv_name_user)
        GitOTextView tvName;

        @Bind(R.id.tv_job_user)
        GitOTextView tvJob;

        View view;

        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }
}
