package com.horical.gito.mvp.intro.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.intro.presenter.IntroPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class IntroModule {
    @Provides
    @PerActivity
    IntroPresenter provideIntroPresenter() {
        return new IntroPresenter();
    }
}
