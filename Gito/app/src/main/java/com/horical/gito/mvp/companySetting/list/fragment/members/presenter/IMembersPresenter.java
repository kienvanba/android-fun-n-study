package com.horical.gito.mvp.companySetting.list.fragment.members.presenter;

import com.horical.gito.model.User;

/**
 * Created by Tin on 23-Nov-16.
 */

public interface IMembersPresenter {
    void getAllMember();

    void deleteMember(String idMember, int position);

    void updateMember(User user, String idLevel, String idDepartment);

    void getAllLevel();

    void getAllDepartment();
}
