package com.horical.gito.mvp.myProject.setting.adapter;

import com.horical.gito.AppConstants;
import com.horical.gito.base.IRecyclerItem;

public class FeatureItemBody implements IRecyclerItem {
    public String featureName;
    public int icon;
    public FeatureItemBody(int icon, String featureName){
        this.icon = icon;
        this.featureName = featureName;
    }
    @Override
    public int getItemViewType() {
        return AppConstants.FEATURE_ITEM;
    }
}
