package com.horical.gito.mvp.myProject.gantt.adapter;

import com.horical.gito.base.IRecyclerItem;
import com.horical.gito.mvp.myProject.gantt.dto.GanttDetailDto;

/**
 * Created by hoangsang on 3/6/17.
 */

public class GanttDetailItem implements IRecyclerItem {

    public int lengh;

    public GanttDetailDto detailItem;

    public GanttDetailItem() {
    }

    public GanttDetailItem(GanttDetailDto detailItem) {
        this.detailItem = detailItem;
    }

    @Override
    public int getItemViewType() {
        return 0;
    }
}
