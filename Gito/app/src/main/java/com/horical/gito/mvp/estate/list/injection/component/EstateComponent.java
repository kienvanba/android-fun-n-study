package com.horical.gito.mvp.estate.list.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.estate.list.injection.module.EstateModule;
import com.horical.gito.mvp.estate.list.view.EstateActivity;

import dagger.Component;

/**
 * Created by hoangsang on 11/8/16.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = EstateModule.class)
public interface EstateComponent {

    void inject(EstateActivity activity);
}
