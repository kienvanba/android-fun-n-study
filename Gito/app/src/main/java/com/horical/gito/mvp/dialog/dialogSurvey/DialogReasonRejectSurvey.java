package com.horical.gito.mvp.dialog.dialogSurvey;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import com.horical.gito.R;

/**
 * Created by nhattruong251295 on 3/27/2017.
 */

public class DialogReasonRejectSurvey extends Dialog {


    public DialogReasonRejectSurvey(Context context, String note, OnClickListener callBack) {
        super(context, R.style.DialogDefault);
        //this.mCallBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_reject_survey);
    }
}
