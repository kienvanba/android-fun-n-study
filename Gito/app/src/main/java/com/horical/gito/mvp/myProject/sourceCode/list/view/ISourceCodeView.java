package com.horical.gito.mvp.myProject.sourceCode.list.view;

import com.horical.gito.base.IView;

/**
 * Created by Tin on 24-Nov-16.
 */

public interface ISourceCodeView extends IView {
    void showLoading();

    void hideLoading();

    void getAllCodeSuccess();

    void getAllCodeFailure(String err);

    void createCodeSuccess();

    void createCodeFailure(String err);
}
