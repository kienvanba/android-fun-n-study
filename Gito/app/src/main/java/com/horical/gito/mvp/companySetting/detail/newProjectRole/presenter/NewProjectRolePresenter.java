package com.horical.gito.mvp.companySetting.detail.newProjectRole.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.companySetting.detail.newProjectRole.view.INewProjectRoleView;

/**
 * Created by Tin on 23-Nov-16.
 */

public class NewProjectRolePresenter extends BasePresenter implements INewProjectRolePresenter {

    public void attachView(INewProjectRoleView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public INewProjectRoleView getView() {
        return (INewProjectRoleView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }
}

