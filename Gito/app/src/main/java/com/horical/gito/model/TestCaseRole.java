package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by nxon on 3/17/17
 */

public class TestCaseRole implements Serializable {

    @SerializedName("changeStatus")
    @Expose
    private boolean changeStatus;

    @SerializedName("del")
    @Expose
    private boolean del;

    @SerializedName("modify")
    @Expose
    private boolean modify;

    @SerializedName("view")
    @Expose
    private boolean view;

    @SerializedName("add")
    @Expose
    private boolean add;

    public boolean isChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(boolean changeStatus) {
        this.changeStatus = changeStatus;
    }

    public boolean isDel() {
        return del;
    }

    public void setDel(boolean del) {
        this.del = del;
    }

    public boolean isModify() {
        return modify;
    }

    public void setModify(boolean modify) {
        this.modify = modify;
    }

    public boolean isView() {
        return view;
    }

    public void setView(boolean view) {
        this.view = view;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }
}
