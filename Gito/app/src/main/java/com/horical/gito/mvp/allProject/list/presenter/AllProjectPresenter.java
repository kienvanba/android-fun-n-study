package com.horical.gito.mvp.allProject.list.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.project.GetAllProjectResponse;
import com.horical.gito.interactor.api.response.project.GetProjectResponse;
import com.horical.gito.model.Project;
import com.horical.gito.mvp.allProject.list.view.IAllProjectView;

import java.util.ArrayList;
import java.util.List;

public class AllProjectPresenter extends BasePresenter implements IAllProjectPresenter {

    private List<Project> mList;

    public IAllProjectView getView() {
        return (IAllProjectView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }

    public List<Project> getListProject() {
        return mList;
    }

    @Override
    public void getAllProject() {

        getView().showLoading();
        getApiManager().getAllProject(new ApiCallback<GetAllProjectResponse>() {
            @Override
            public void success(GetAllProjectResponse res) {
                if (mList != null) {
                    mList.clear();
                } else {
                    mList = new ArrayList<>();
                }
                mList.addAll(res.projects);
                getView().hideLoading();
                getView().getAllProjectSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().getAllProjectFailure(error.message);
            }
        });
    }

    @Override
    public void getProjectDetails(String id) {

        getView().showLoading();
        getApiManager().getProject(id, new ApiCallback<GetProjectResponse>() {
            @Override
            public void success(GetProjectResponse res) {
                getView().hideLoading();
                GitOStorage.getInstance().setProject(res.getProjectResult().getProject());
                getView().getProjectDetailsSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().hideLoading();
                getView().getProjectDetailsFailure(error.message);
            }
        });
    }
}

