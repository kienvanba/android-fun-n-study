package com.horical.gito.mvp.salary.mySalary.detail.injection.component;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.salary.mySalary.detail.injection.module.DetailMySalaryModule;
import com.horical.gito.mvp.salary.mySalary.detail.view.DetailMySalaryActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = DetailMySalaryModule.class)
public interface IDetailMySalaryComponent {
    void inject(DetailMySalaryActivity activity);
}
