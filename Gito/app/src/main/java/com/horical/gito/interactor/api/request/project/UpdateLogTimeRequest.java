package com.horical.gito.interactor.api.request.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateLogTimeRequest {

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("rejectMsg")
    @Expose
    public String rejectMsg;

    public UpdateLogTimeRequest(int status, String rejectMsg) {
        this.status = status;
        this.rejectMsg = rejectMsg;
    }
}
