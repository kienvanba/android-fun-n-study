package com.horical.gito.mvp.companySetting.list.fragment.devices.injection.component;

import com.horical.gito.injection.PerFragment;
import com.horical.gito.injection.component.ApplicationComponent;
import com.horical.gito.mvp.companySetting.list.fragment.devices.injection.module.DevicesModule;
import com.horical.gito.mvp.companySetting.list.fragment.devices.view.DevicesFragment;

import dagger.Component;

/**
 * Created by Tin on 23-Nov-16.
 */
@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = DevicesModule.class)
public interface DevicesComponent {
    void inject(DevicesFragment fragment);
}
