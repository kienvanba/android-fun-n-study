package com.horical.gito.mvp.myProject.setting.fragment.recycler.view.member;

import android.content.Intent;

import com.horical.gito.mvp.myProject.setting.fragment.activity.member.view.NewMemberActivity;
import com.horical.gito.mvp.myProject.setting.fragment.recycler.view.BaseRecyclerFragment;

public class MemberFragment extends BaseRecyclerFragment {
    @Override
    protected void requestData() {
        mPresenter.getMemberAndRole();
    }

    @Override
    protected void setOnItemClickListener(int position) {

    }

    @Override
    protected void setOnDeleteItemClickListener(int position) {

    }

    @Override
    public void setOnAddItemClickListener() {
        Intent intent = new Intent(getActivity(), NewMemberActivity.class);
        startActivity(intent);
    }
}
