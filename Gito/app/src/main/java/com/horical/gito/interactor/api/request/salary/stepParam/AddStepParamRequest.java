package com.horical.gito.interactor.api.request.salary.stepParam;

import com.horical.gito.model.StepParam;

import java.util.List;

public class AddStepParamRequest {
    private List<StepParam> stepParams;

    public List<StepParam> getStepParams() {
        return stepParams;
    }

    public void setStepParams(List<StepParam> stepParams) {
        this.stepParams = stepParams;
    }
}
