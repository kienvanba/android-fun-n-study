package com.horical.gito.mvp.estate.detail.injection.module;

import com.horical.gito.injection.PerActivity;
import com.horical.gito.mvp.estate.detail.presenter.DetailEstatePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hoangsang on 11/8/16.
 */
@Module
public class DetailEstateModule {
    @PerActivity
    @Provides
    DetailEstatePresenter provideDetailEstatePresenter(){
        return new DetailEstatePresenter();
    }
}
