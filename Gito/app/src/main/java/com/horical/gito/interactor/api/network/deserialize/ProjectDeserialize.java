package com.horical.gito.interactor.api.network.deserialize;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.horical.gito.model.Project;
import com.horical.gito.utils.GsonUtils;

import java.lang.reflect.Type;

public class ProjectDeserialize implements JsonDeserializer<Project> {

    @Override
    public Project deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Project project;

        try {
            project = GsonUtils.createGson(Project.class).fromJson(json, Project.class);
        } catch (Exception e) {
            project = new Project();
            project.setId(json.getAsString());
        }
        return project;
    }
}
