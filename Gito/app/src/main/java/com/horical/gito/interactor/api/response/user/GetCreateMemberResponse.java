package com.horical.gito.interactor.api.response.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.model.User;

public class GetCreateMemberResponse extends BaseResponse{
    @SerializedName("results")
    @Expose
    public User user;
}
