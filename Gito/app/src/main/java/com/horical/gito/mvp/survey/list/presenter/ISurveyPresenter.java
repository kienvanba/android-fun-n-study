package com.horical.gito.mvp.survey.list.presenter;

/**
 * Created by thanhle on 11/9/16.
 */
public interface ISurveyPresenter {
   void getAllSurvey();

   void getAllSurveyApproved();

   void getAllSurveyPending();
}