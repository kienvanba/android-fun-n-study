package com.horical.gito.mvp.recruitment.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.interactor.api.network.ApiCallback;
import com.horical.gito.interactor.api.network.RestError;
import com.horical.gito.interactor.api.response.BaseResponse;
import com.horical.gito.interactor.api.response.recruitment.RecruitmentResponse;
import com.horical.gito.model.Recruitment;
import com.horical.gito.mvp.recruitment.view.IRecruitmentView;

import java.util.ArrayList;
import java.util.List;

public class RecruitmentPresenter extends BasePresenter implements IRecruitmentPresenter {


    public void attachView(IRecruitmentView view) {
        super.attachView(view);
    }

    public IRecruitmentView getView() {
        return (IRecruitmentView) getIView();
    }


    private List<Recruitment> list = new ArrayList<>();

    public List<Recruitment> getListRecruit() {
        return list;
    }

    @Override
    public void getListRecruitmentFromServer() {
        getView().showLoading();
        getApiManager().getListRecruitment(new ApiCallback<RecruitmentResponse>() {
            @Override
            public void success(RecruitmentResponse res) {
                list.clear();
                list.addAll(res.getData());
                getView().getListRecruitmentSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getDataError(error.message);

            }
        });
    }

    @Override
    public void deleteRecruitment(final int position) {

        getView().showLoading();
        getApiManager().deleteRecruitment(list.get(position).get_id(), new ApiCallback<BaseResponse>() {
            @Override
            public void success(BaseResponse res) {
                list.remove(position);
                getView().getListRecruitmentSuccess();
            }

            @Override
            public void failure(RestError error) {
                getView().getDataError(error.message);

            }
        });

    }

    @Override
    public void onCreate() {
        super.onCreate();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }


}
