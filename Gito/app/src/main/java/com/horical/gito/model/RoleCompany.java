package com.horical.gito.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RoleCompany implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("companyId")
    @Expose
    private Company companyId;

    @SerializedName("roleCompanyId")
    @Expose
    private String roleCompanyId;

    @SerializedName("notification")
    @Expose
    private NotificationRole notificationRole;

    @SerializedName("overtime")
    @Expose
    private OverTimeRole overTimeRole;

    @SerializedName("calendar")
    @Expose
    private CalendarRole calendarRole;

    @SerializedName("chat")
    @Expose
    private ChatRole chatRole;

    @SerializedName("project")
    @Expose
    private ProjectRole project;

    @SerializedName("salary")
    @Expose
    private SalaryRole salaryRole;

    @SerializedName("bonus")
    @Expose
    private BonusRole bonus;

    @SerializedName("accounting")
    @Expose
    private AccountingRole accountingRole;

    @SerializedName("survey")
    @Expose
    private SurveyRole surveyRole;

    @SerializedName("leaving")
    @Expose
    private LeavingRole leavingRole;

    @SerializedName("checkin")
    @Expose
    private CheckinRole checkinRole;

    @SerializedName("announcement")
    @Expose
    private AnnouncementRole announcementRole;

    @SerializedName("recruitment")
    @Expose
    private RecruitmentRole recruitmentRole;

    @SerializedName("caseStudy")
    @Expose
    private CaseStudyRole caseStudyRole;

    @SerializedName("estate")
    @Expose
    private EstateRole estateRole;

    @SerializedName("prepayment")
    @Expose
    private PrepaymentRole prepaymentRole;

    @SerializedName("report")
    @Expose
    private ReportRole reportRole;

    @SerializedName("meeting")
    @Expose
    private MeetingRole meetingRole;

    @SerializedName("room")
    @Expose
    private RoomRole roomRole;

    @SerializedName("department")
    @Expose
    private DepartmentRole departmentRole;

    @SerializedName("level")
    @Expose
    private LevelRole levelRole;

    @SerializedName("device")
    @Expose
    private DeviceRole deviceRole;

    @SerializedName("projectRole")
    @Expose
    private ProjectRole projectRole;

    @SerializedName("companyRole")
    @Expose
    private CompanyRole companyRole;

    @SerializedName("payment")
    @Expose
    private PaymentRole paymentRole;

    @SerializedName("quota")
    @Expose
    private QuotaRole quotaRole;

    @SerializedName("working")
    @Expose
    private WorkingRole workingRole;

    @SerializedName("member")
    @Expose
    private MemberRole memberRole;

    @SerializedName("feature")
    @Expose
    private FeatureRole featureRole;

    @SerializedName("info")
    @Expose
    private InfoRole infoRole;

    @SerializedName("createDate")
    @Expose
    private String createDate;

    @SerializedName("roleName")
    @Expose
    private String roleName;

    @SerializedName("wasUsed")
    @Expose
    private boolean wasUsed;

    private boolean isSelected;

    public RoleCompany() {
    }

    public RoleCompany(String roleName) {
        this.roleName = roleName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Company getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Company companyId) {
        this.companyId = companyId;
    }

    public String getRoleCompanyId() {
        return roleCompanyId;
    }

    public void setRoleCompanyId(String roleCompanyId) {
        this.roleCompanyId = roleCompanyId;
    }

    public NotificationRole getNotificationRole() {
        return notificationRole;
    }

    public void setNotificationRole(NotificationRole notificationRole) {
        this.notificationRole = notificationRole;
    }

    public OverTimeRole getOverTimeRole() {
        return overTimeRole;
    }

    public void setOverTimeRole(OverTimeRole overTimeRole) {
        this.overTimeRole = overTimeRole;
    }

    public CalendarRole getCalendarRole() {
        return calendarRole;
    }

    public void setCalendarRole(CalendarRole calendarRole) {
        this.calendarRole = calendarRole;
    }

    public ChatRole getChatRole() {
        return chatRole;
    }

    public void setChatRole(ChatRole chatRole) {
        this.chatRole = chatRole;
    }

    public ProjectRole getProject() {
        return project;
    }

    public void setProject(ProjectRole project) {
        this.project = project;
    }

    public SalaryRole getSalaryRole() {
        return salaryRole;
    }

    public void setSalaryRole(SalaryRole salaryRole) {
        this.salaryRole = salaryRole;
    }

    public AccountingRole getAccountingRole() {
        return accountingRole;
    }

    public void setAccountingRole(AccountingRole accountingRole) {
        this.accountingRole = accountingRole;
    }

    public SurveyRole getSurveyRole() {
        return surveyRole;
    }

    public void setSurveyRole(SurveyRole surveyRole) {
        this.surveyRole = surveyRole;
    }

    public LeavingRole getLeavingRole() {
        return leavingRole;
    }

    public void setLeavingRole(LeavingRole leavingRole) {
        this.leavingRole = leavingRole;
    }

    public CheckinRole getCheckinRole() {
        return checkinRole;
    }

    public void setCheckinRole(CheckinRole checkinRole) {
        this.checkinRole = checkinRole;
    }

    public AnnouncementRole getAnnouncementRole() {
        return announcementRole;
    }

    public void setAnnouncementRole(AnnouncementRole announcementRole) {
        this.announcementRole = announcementRole;
    }

    public RecruitmentRole getRecruitmentRole() {
        return recruitmentRole;
    }

    public void setRecruitmentRole(RecruitmentRole recruitmentRole) {
        this.recruitmentRole = recruitmentRole;
    }

    public CaseStudyRole getCaseStudyRole() {
        return caseStudyRole;
    }

    public void setCaseStudyRole(CaseStudyRole caseStudyRole) {
        this.caseStudyRole = caseStudyRole;
    }

    public EstateRole getEstateRole() {
        return estateRole;
    }

    public void setEstateRole(EstateRole estateRole) {
        this.estateRole = estateRole;
    }

    public PrepaymentRole getPrepaymentRole() {
        return prepaymentRole;
    }

    public void setPrepaymentRole(PrepaymentRole prepaymentRole) {
        this.prepaymentRole = prepaymentRole;
    }

    public ReportRole getReportRole() {
        return reportRole;
    }

    public void setReportRole(ReportRole reportRole) {
        this.reportRole = reportRole;
    }

    public MeetingRole getMeetingRole() {
        return meetingRole;
    }

    public void setMeetingRole(MeetingRole meetingRole) {
        this.meetingRole = meetingRole;
    }

    public RoomRole getRoomRole() {
        return roomRole;
    }

    public void setRoomRole(RoomRole roomRole) {
        this.roomRole = roomRole;
    }

    public DepartmentRole getDepartmentRole() {
        return departmentRole;
    }

    public void setDepartmentRole(DepartmentRole departmentRole) {
        this.departmentRole = departmentRole;
    }

    public LevelRole getLevelRole() {
        return levelRole;
    }

    public void setLevelRole(LevelRole levelRole) {
        this.levelRole = levelRole;
    }

    public DeviceRole getDeviceRole() {
        return deviceRole;
    }

    public void setDeviceRole(DeviceRole deviceRole) {
        this.deviceRole = deviceRole;
    }

    public ProjectRole getProjectRole() {
        return projectRole;
    }

    public void setProjectRole(ProjectRole projectRole) {
        this.projectRole = projectRole;
    }

    public CompanyRole getCompanyRole() {
        return companyRole;
    }

    public void setCompanyRole(CompanyRole companyRole) {
        this.companyRole = companyRole;
    }

    public PaymentRole getPaymentRole() {
        return paymentRole;
    }

    public void setPaymentRole(PaymentRole paymentRole) {
        this.paymentRole = paymentRole;
    }

    public QuotaRole getQuotaRole() {
        return quotaRole;
    }

    public void setQuotaRole(QuotaRole quotaRole) {
        this.quotaRole = quotaRole;
    }

    public WorkingRole getWorkingRole() {
        return workingRole;
    }

    public void setWorkingRole(WorkingRole workingRole) {
        this.workingRole = workingRole;
    }

    public MemberRole getMemberRole() {
        return memberRole;
    }

    public void setMemberRole(MemberRole memberRole) {
        this.memberRole = memberRole;
    }

    public FeatureRole getFeatureRole() {
        return featureRole;
    }

    public void setFeatureRole(FeatureRole featureRole) {
        this.featureRole = featureRole;
    }

    public InfoRole getInfoRole() {
        return infoRole;
    }

    public void setInfoRole(InfoRole infoRole) {
        this.infoRole = infoRole;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getRoleName() {
        if (roleName == null || roleName.isEmpty()) {
            return "";
        }
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public boolean isWasUsed() {
        return wasUsed;
    }

    public void setWasUsed(boolean wasUsed) {
        this.wasUsed = wasUsed;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof RoleCompany) {
            RoleCompany roleCompany = (RoleCompany) obj;
            result = this.roleName.equals(roleCompany.roleName);
        }
        return result;
    }

    private int getIntFromString(String data) {
        int count = 0;
        for (int i = 0; i < data.length(); i++) {
            count += Character.getNumericValue(data.charAt(i));
        }
        return count;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + getIntFromString(this.roleName);
        return result;
    }
}
