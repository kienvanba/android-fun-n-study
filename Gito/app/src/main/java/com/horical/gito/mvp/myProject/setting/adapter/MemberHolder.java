package com.horical.gito.mvp.myProject.setting.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.horical.gito.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MemberHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.img_avatar)
    public CircleImageView mImAvatar;
    @Bind(R.id.progress_avatar)
    public ProgressBar progressAvatar;
    @Bind(R.id.tv_avatar)
    public TextView mTvNonAvatar;
    @Bind(R.id.edit_name)
    public TextView mTvName;
    @Bind(R.id.btn_delete)
    public ImageButton mBtnDelete;
    @Bind(R.id.spin_role)
    public Spinner mSpRole;

    public MemberHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
