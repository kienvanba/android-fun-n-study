package com.horical.gito.widget.chartview.dto;

/**
 * Created by nxon on 3/17/17
 */

public class CircleChartIssueItem {

    private int mForegroundColor;

    private int mPercent;

    public CircleChartIssueItem() {
    }

    public CircleChartIssueItem(int mForegroundColor, int percent) {
        this.mForegroundColor = mForegroundColor;
        this.mPercent = percent;
    }

    public int getForegroundColor() {
        return mForegroundColor;
    }

    public void setForegroundColor(int mForegroundColor) {
        this.mForegroundColor = mForegroundColor;
    }

    public int getPercent() {
        return mPercent;
    }

    public void setPercent(int percent) {
        this.mPercent = percent;
    }
}
