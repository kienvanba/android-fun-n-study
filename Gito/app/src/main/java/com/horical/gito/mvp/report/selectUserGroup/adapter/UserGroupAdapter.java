package com.horical.gito.mvp.report.selectUserGroup.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;

import com.horical.gito.R;
import com.horical.gito.caches.images.ImageLoader;
import com.horical.gito.caches.storage.GitOStorage;
import com.horical.gito.model.Group;
import com.horical.gito.model.User;
import com.horical.gito.utils.CommonUtils;
import com.horical.gito.widget.textview.GitOTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserGroupAdapter extends RecyclerView.Adapter<UserGroupAdapter.UserHolder> {

    private Context mContext;
    private List<Object> mList;

    private OnItemClickListener listener;

    public UserGroupAdapter(Context context, List<Object> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_user_group, parent, false);
        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(final UserHolder holder, final int position) {
        if (mList.get(position) instanceof User) {
            final User user = (User) mList.get(position);
            holder.tvName.setText(user.getDisplayName());
            holder.tvDepartment.setText(user.getLevel().getName());
            holder.cbUser.setChecked(user.isSelected());

            if (user.getAvatar() != null && !TextUtils.isEmpty(user.getAvatar().getTokenFile())) {
                String url = CommonUtils.getURL(user.getAvatar());
                ImageLoader.load(mContext, url, holder.imvAvatar, holder.pgrLoading);
            } else {
                holder.pgrLoading.setVisibility(View.GONE);
                holder.tvNamePhoto.setText(user.getDisplayPhotoName());
                holder.tvNamePhoto.setVisibility(View.VISIBLE);
            }

            // Set role
            if (user.isAdmin()) {
                holder.tvDepartment.setText(mContext.getString(R.string.admin));
            } else {
                holder.tvDepartment.setText(user.getRoleCompany().getRoleName());
            }

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    user.setSelected(!user.isSelected());
                    holder.cbUser.setChecked(user.isSelected());
                    listener.onItemClickListener(position);
                }
            });
        } else if (mList.get(position) instanceof Group) {
            final Group group = (Group) mList.get(position);
            holder.cbUser.setChecked(group.isSelected());
            holder.tvName.setText(group.getGroupName());

            holder.pgrLoading.setVisibility(View.GONE);
            holder.tvNamePhoto.setText(group.getDisplayPhotoName());
            holder.tvNamePhoto.setVisibility(View.VISIBLE);

            if (GitOStorage.getInstance().getProject() == null) {
                holder.tvDepartment.setText("");
            } else {
                holder.tvDepartment.setText(GitOStorage.getInstance().getProject().getName());
            }

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    group.setSelected(!group.isSelected());
                    holder.cbUser.setChecked(group.isSelected());
                    listener.onItemClickListener(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }


    public class UserHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.task_imv_avatar)
        CircleImageView imvAvatar;

        @Bind(R.id.task_tv_name)
        GitOTextView tvName;

        @Bind(R.id.tv_department)
        GitOTextView tvDepartment;

        @Bind(R.id.task_pgr_avt_loading)
        ProgressBar pgrLoading;

        @Bind(R.id.tv_name_photo)
        GitOTextView tvNamePhoto;

        @Bind(R.id.cb_user)
        CheckBox cbUser;

        View view;

        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }

}
