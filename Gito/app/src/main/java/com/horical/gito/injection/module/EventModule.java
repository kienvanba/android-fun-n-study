package com.horical.gito.injection.module;

import com.horical.gito.interactor.event.EventManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class EventModule {

    @Provides
    @Singleton
    EventManager provideEventManager() {
        return new EventManager();
    }

}
