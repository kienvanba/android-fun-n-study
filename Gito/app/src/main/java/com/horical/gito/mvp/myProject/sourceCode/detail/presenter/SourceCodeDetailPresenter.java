package com.horical.gito.mvp.myProject.sourceCode.detail.presenter;

import com.horical.gito.base.BasePresenter;
import com.horical.gito.mvp.myProject.sourceCode.detail.view.ISourceCodeDetailView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tin on 24-Nov-16.
 */

public class SourceCodeDetailPresenter extends BasePresenter implements ISourceCodeDetailPresenter {
    private List<String> mList;

    public void attachView(ISourceCodeDetailView view) {
        super.attachView(view);
    }

    public ISourceCodeDetailView getView() {
        return (ISourceCodeDetailView) getIView();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mList = new ArrayList<>();
        getEventManager().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getEventManager().unRegister(this);
    }


}

