package com.horical.gito.widget.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.horical.gito.R;
import com.horical.gito.widget.button.GitOButton;
import com.horical.gito.widget.edittext.GitOEditText;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by hoangsang on 3/8/17.
 */

public class EditTwoOptionsDialog extends Dialog {

    @Bind(R.id.edt)
    GitOEditText mEditText;
    @Bind(R.id.btn_done)
    GitOButton mBtnDone;
    @Bind(R.id.btn_cancel)
    GitOButton mBtnCancel;

    private String hintText;

    public EditTwoOptionsDialog(@NonNull Context context, String hintText) {
        super(context);
        this.hintText = hintText;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_dialog_edit_two_options);
        if (getWindow() != null){
            getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        }

        this.setCanceledOnTouchOutside(false);
        ButterKnife.bind(this);

        initView();
        initListener();
    }

    private void initListener() {
        mBtnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = mEditText.getText().toString().trim();
                if ( ! text.isEmpty())
                    mListener.eventDone(mEditText.getText().toString().trim());

                dismiss();
            }
        });

        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.eventCancel();
                dismiss();
            }
        });

    }

    private void initView() {
        mEditText.setHint(hintText);
    }

    private OnClick mListener;

    public void setListener(OnClick mListener) {
        this.mListener = mListener;
    }

    public interface OnClick {
        void eventDone(String text);

        void eventCancel();
    }
}
