//
// Created by KIEN on 9/19/2017.
//

#include <string.h>
#include <jni.h>

JNIEXPORT jstring JNICALL Java_com_example_kien_funnstudy_ndk_NDKActivity_getJniHelloString(JNIEnv* env, jobject obj){
    return (*env)->NewStringUTF(env, "Hello Kien dep trai from JNI!");
}