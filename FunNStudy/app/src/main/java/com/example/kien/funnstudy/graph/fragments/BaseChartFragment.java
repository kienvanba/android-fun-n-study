package com.example.kien.funnstudy.graph.fragments;


import android.view.View;

import com.example.kien.funnstudy.base.BaseFragment;
import com.example.kien.funnstudy.graph.MultiChartActivity;
import com.example.kien.funnstudy.graph.Singleton;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by kienvanba on 9/25/17.
 */

public abstract class BaseChartFragment extends BaseFragment {
    protected static final int DEFAULT_BAR_SIZE= 160;
    protected static final int LOADED_BAR_NUMBER = 300;
    protected int barNumber;

    protected BarChart mChart;
    protected ArrayList<BarEntry> mEntries;
    protected BarDataSet mDataSet;
    protected ArrayList<IBarDataSet> mListDataSet;
    protected BarData mData;

    @Override
    protected void initOnlyCreateOnceData() {
        super.initOnlyCreateOnceData();
        mEntries = new ArrayList<>();
        mDataSet = new BarDataSet(mEntries, initLabel());
        mListDataSet = new ArrayList<>();
        mListDataSet.add(mDataSet);
        mData = new BarData(mListDataSet);
    }

    @Override
    protected void initOnCreatingViewData() {
        super.initOnCreatingViewData();
        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);

        mChart.getDescription().setEnabled(false);

        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);

        YAxis leftAxis = mChart.getAxisLeft();
        YAxis rightAxis = mChart.getAxisRight();
        XAxis xAxis = mChart.getXAxis();

        leftAxis.setGranularity(200f);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        rightAxis.setEnabled(false);

        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setValueFormatter(initAxisFormatter());
        xAxis.setGranularity(1f);

        mChart.setScaleEnabled(false);

        mChart.animateY(1000);
    }

    @Override
    protected void initCreatedViewData(View v) {
        super.initCreatedViewData(v);

        mChart.setData(mData);
        mChart.invalidate();
    }

    @Override
    protected void initListener() {
        super.initListener();
        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Singleton.getInstance().setCurrentEntry((BarEntry)e);
                mChart.centerViewToAnimated(
                        e.getX(),
                        e.getY(),
                        mChart.getAxisRight().getAxisDependency(),
                        500
                );
                ((MultiChartActivity)getActivity()).setTvData(new DateTime().minusDays(LOADED_BAR_NUMBER-1-((int)e.getX())));
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    protected abstract IAxisValueFormatter initAxisFormatter();
    protected abstract String initLabel();

    public void setSelectedValue(int offsetChanged){
        float x = Singleton.getInstance().getCurrentEntry()==null
                ?LOADED_BAR_NUMBER-1
                :Singleton.getInstance().getCurrentEntry().getX();
        float y = Singleton.getInstance().getCurrentEntry()==null
                ?0
                :Singleton.getInstance().getCurrentEntry().getY();
        x+=offsetChanged;
        mChart.centerViewToAnimated(x, y, mChart.getAxisRight().getAxisDependency(), 500);
        Highlight h = new Highlight(x, y, 0);
        mChart.highlightValue(h);
        Singleton.getInstance().setCurrentEntry(new BarEntry(x, y));
    }
}
