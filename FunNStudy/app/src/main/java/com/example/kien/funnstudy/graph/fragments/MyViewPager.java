package com.example.kien.funnstudy.graph.fragments;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;

import java.lang.reflect.Field;

/**
 * Created by kienvanba on 9/26/17.
 */

public class MyViewPager extends ViewPager {

    private boolean isSlide;

    public MyViewPager(Context context) {
        super(context);
        setScroller();
    }

    public MyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        setScroller();
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return isSlide;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return isSlide;
    }

    public void setSlide(boolean slide){
        this.isSlide = slide;
    }

    private void setScroller(){
        try{
            Class<?> viewPager = ViewPager.class;
            Field scroller = viewPager.getDeclaredField("mScroller");
            scroller.setAccessible(true);
            scroller.set(this, new MyScroller(getContext()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private class MyScroller extends Scroller{

        private MyScroller(Context context) {
            super(context, new DecelerateInterpolator());
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            super.startScroll(startX, startY, dx, dy, 350);
        }
    }
}
