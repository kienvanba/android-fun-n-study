package com.example.kien.funnstudy.graph;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.kien.funnstudy.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Random;

public class Graph extends AppCompatActivity {
    private static final String TAG = "GRAPH ACTIVITY";
    private static final int DEFAULT_BAR_SIZE= 160;
    private BarChart barChart;
    private Button button;

    private int barNumber;

    private ArrayList<BarEntry> entries;
    private BarData data;
    private BarDataSet dataSet;
    private ArrayList<IBarDataSet> dataSets;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        barChart = (BarChart) findViewById(R.id.bar_chart);
        button = (Button) findViewById(R.id.btn);
//        if(savedInstanceState == null) {
            initData();
            initListener();
//        }
    }

    private void initData(){
        setChartAxis();
        barChart.getDescription().setEnabled(false);
        setChartData();
    }

    private void initListener(){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                entries.add(new BarEntry(1, 777));
//                barNumber++;
                data.addEntry(new BarEntry(data.getEntryCount(), 777), 0);
                data.notifyDataChanged();
                dataSet.notifyDataSetChanged();
                barChart.invalidate();
            }
        });


        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Singleton.getInstance().setCurrentEntry((BarEntry)e);
                barChart.centerViewToAnimated(
                        e.getX(),
                        e.getY(),
                        barChart.getAxisRight().getAxisDependency(),
                        500
                );
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        setData();
    }

    private void setChartAxis(){
        IAxisValueFormatter custom = new MyValueFormatter(barNumber);
        //get axis
        YAxis leftAxis = barChart.getAxisLeft();
        YAxis rightAxis = barChart.getAxisRight();
        XAxis xAxis = barChart.getXAxis();
        //set left axis
        leftAxis.setGranularity(200f);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        //set right axis
        rightAxis.setEnabled(false);
        //set x axis
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(custom);
//        xAxis.setLabelCount(barNumber);
//        xAxis.setGranularityEnabled(true);
        xAxis.setDrawGridLines(false);
    }

    private void setChartData(){
        entries = new ArrayList<>();
        dataSet = new BarDataSet(entries, "Data set");
        dataSet.setHighLightAlpha(99);
        dataSets = new ArrayList<>();
        dataSets.add(dataSet);
        data = new BarData(dataSets);
//        barChart.setData(data);

        barChart.setScaleEnabled(false);
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);

        for(int i=0; i<3000; i++){
            float val = (float)(new Random().nextInt(1000));
            BarEntry entry = new BarEntry(i, val);
//            entries.add(entry);
            data.addEntry(entry, 0);
        }

        data.notifyDataChanged();

        barChart.notifyDataSetChanged();

        barChart.animateY(1000);
//        barChart.invalidate();

        showToast("set chart data");
    }

    private void setData(){
        showToast(String.valueOf(entries.size()));
        barChart.setData(data);
        barNumber = barChart.getWidth()/DEFAULT_BAR_SIZE;

//        entries.notifyAll();
//        dataSet.notifyDataSetChanged();

        barChart.setVisibleXRangeMaximum(barNumber);
//        barChart.centerViewTo(
//                barChart.getXChartMax(),
//                barChart.getYChartMax(),
//                barChart.getAxisRight().getAxisDependency()
//        );
        barChart.centerViewTo(
                Singleton.getInstance().getCurrentEntry()==null?barChart.getXChartMax():Singleton.getInstance().getCurrentEntry().getX(),
                Singleton.getInstance().getCurrentEntry()==null?barChart.getYChartMax():Singleton.getInstance().getCurrentEntry().getY(),
                barChart.getAxisRight().getAxisDependency()
        );
        Highlight h = new Highlight(
                Singleton.getInstance().getCurrentEntry()==null?barChart.getXChartMax():Singleton.getInstance().getCurrentEntry().getX(),
                Singleton.getInstance().getCurrentEntry()==null?barChart.getYChartMax():Singleton.getInstance().getCurrentEntry().getY(),
                0
        );
        barChart.highlightValue(h);
        barChart.getXAxis().setGranularity(1f);
    }

    private void showToast(String mess){
        Toast.makeText(this, mess, Toast.LENGTH_SHORT).show();
        Log.e(TAG, mess);
    }

    private int dpToPx(float dp){
        return (int)dp*(this.getResources().getDisplayMetrics().densityDpi/ DisplayMetrics.DENSITY_DEFAULT);
    }

    private Point getScreenWidth(){
        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);
        return point;
    }

    private class MyValueFormatter implements IAxisValueFormatter{
        private int barNumber;

        MyValueFormatter(int barNumber) {
            this.barNumber = barNumber;
        }


        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            if(axis instanceof XAxis){
//                return String.valueOf(new DateTime().minusDays(barNumber-((int)value)-1).getDayOfMonth());
                return String.valueOf((int)value);
            }else{
                return null;
            }
        }


    }
}
