package com.example.kien.funnstudy.graph;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;

/**
 * Created by kienvanba on 9/26/17.
 */

public class ChartData {
    private ArrayList<Entry> entries;
    private Entry selectedEntry;

    public ChartData(ArrayList<Entry> entries, Entry selectedEntry) {
        this.entries = entries;
        this.selectedEntry = selectedEntry;
    }
}
