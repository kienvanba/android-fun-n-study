package com.example.kien.funnstudy.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kien.funnstudy.R;

import java.util.ArrayList;

/**
 * Created by KIEN on 9/19/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyHolder> {
    private Context context;
    private ArrayList<String> items;
    private OnItemClickListener listener;

    public RecyclerAdapter(Context context, ArrayList<String> list){
        this.context = context;
        this.items = list;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_lesson_item, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, final int position) {
        holder.textView.setText(items.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener{
        void onClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    class MyHolder extends RecyclerView.ViewHolder{
        TextView textView;
        private MyHolder(View itemView){
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tv);
        }
    }
}
