package com.example.kien.funnstudy.ndk;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.example.kien.funnstudy.R;

/**
 * Created by KIEN on 9/18/2017.
 */

public class NDKActivity extends AppCompatActivity {
    static {
        System.loadLibrary("hello-jni");
    }


    private TextView jniTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ndk);

        jniTextView = (TextView) findViewById(R.id.jni_tv);
        jniTextView.setText(getJniHelloString());
    }

    public native String getJniHelloString();
}
