package com.example.kien.funnstudy.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.kien.funnstudy.graph.Graph;
import com.example.kien.funnstudy.graph.MultiChartActivity;
import com.example.kien.funnstudy.ndk.NDKActivity;
import com.example.kien.funnstudy.R;
import com.example.kien.funnstudy.selector.SelectorActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private TextView title;
    private RecyclerView rcvLesson;

    private ArrayList<String> lessons;
    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        title = (TextView) findViewById(R.id.title);
        rcvLesson = (RecyclerView) findViewById(R.id.rcv_lesson);

        initData();
        initListener();
    }

    private void initData(){
        title.setText("All Lessons Learn");
        lessons = new ArrayList<>();
        lessons.add("NDK hello world from JNI");
        lessons.add("Button and item selector");
        lessons.add("Graph with MPAndroid Chart");
        lessons.add("Multi chart with fragment");

        //--
        adapter = new RecyclerAdapter(this, lessons);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcvLesson.setLayoutManager(layoutManager);
        rcvLesson.setAdapter(adapter);
    }

    private void initListener(){
        adapter.setOnItemClickListener(new RecyclerAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent;
                switch (position){
                    case 0:
                        intent = new Intent(MainActivity.this, NDKActivity.class);
                        break;
                    case 1:
                        intent = new Intent(MainActivity.this, SelectorActivity.class);
                        break;
                    case 2:
                        intent = new Intent(MainActivity.this, Graph.class);
                        break;
                    case 3:
                        intent = new Intent(MainActivity.this, MultiChartActivity.class);
                        break;
                    default:
                        intent = new Intent(MainActivity.this, MainActivity.class);
                }
                startActivity(intent);
            }
        });
    }
}
