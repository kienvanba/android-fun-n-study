package com.example.kien.funnstudy.graph;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.example.kien.funnstudy.graph.fragments.BaseChartFragment;
import com.example.kien.funnstudy.graph.fragments.FragmentAHI;
import com.example.kien.funnstudy.graph.fragments.FragmentMask;
import com.example.kien.funnstudy.graph.fragments.FragmentUsage;

/**
 * Created by kienvanba on 9/26/17.
 */

public class ChartPagerAdapter extends FragmentPagerAdapter {
    private Fragment fragment;
    private BaseChartFragment currentFragment;

    public ChartPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                fragment = new FragmentAHI();
                break;
            case 1:
                fragment = new FragmentUsage();
                break;
            case 2:
                fragment = new FragmentMask();
                break;
        }
        return fragment;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        currentFragment = (BaseChartFragment)object;
    }

    @Override
    public int getCount() {
        return 3;
    }

    public BaseChartFragment getCurrentFragment(){
        return currentFragment;
    }
}
