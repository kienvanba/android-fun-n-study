package com.example.kien.funnstudy.graph;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Created by KIEN on 9/20/2017.
 */

public abstract class ChartView extends View{
    protected Paint mPaint;

    public ChartView(Context context) {
        super(context);
    }

    public ChartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setAntiAlias(true);
    }

    protected float pxToDp(float px){
        return (px/((float)getContext().getResources().getDisplayMetrics().densityDpi/ DisplayMetrics.DENSITY_DEFAULT));
    }

    protected float dpToPx(float dp){
        return (dp*((float)getContext().getResources().getDisplayMetrics().densityDpi/ DisplayMetrics.DENSITY_DEFAULT));
    }
}
