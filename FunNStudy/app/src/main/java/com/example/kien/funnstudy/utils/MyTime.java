package com.example.kien.funnstudy.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by kienvanba on 9/26/17.
 */

public class MyTime {
    private static final int dateMillisecond = 86400000;
    private static final int localeMillisecond = TimeZone.getDefault().getRawOffset();

    public static long getOnlyDate(DateTime date){
        long mil = date.getMillis();
        return mil-mil%dateMillisecond-localeMillisecond;
    }

    public static String getDateString(DateTime date){
        return dateFormat("dd/MM/yyyy").print(date);
    }

    public static String getTimeString(DateTime date){
        return dateFormat("HH:mm:ss").print(date);
    }

    private static DateTimeFormatter dateFormat(String reg){
        return DateTimeFormat.forPattern(reg);
    }

    public static String getWeekDay(DateTime time){
        return dateFormat("EEEE").withLocale(Locale.getDefault()).print(time);
    }
}
