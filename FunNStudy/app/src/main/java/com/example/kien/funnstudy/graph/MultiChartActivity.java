package com.example.kien.funnstudy.graph;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kien.funnstudy.R;
import com.example.kien.funnstudy.graph.fragments.BaseChartFragment;
import com.example.kien.funnstudy.graph.fragments.FragmentAHI;
import com.example.kien.funnstudy.graph.fragments.MyViewPager;
import com.example.kien.funnstudy.utils.MyTime;

import org.joda.time.DateTime;

/**
 * Created by kienvanba on 9/26/17.
 */

public class MultiChartActivity extends AppCompatActivity {
    private TextView tvDay, tvDate;
    private ImageButton btnNext, btnPrev;
    private RadioGroup groupPager;
    private MyViewPager chartPager;
    private ChartPagerAdapter adapter;

    private DateTime currentDate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_chart);
        tvDate = (TextView) findViewById(R.id.txt_date);
        tvDay = (TextView) findViewById(R.id.txt_day);
        btnNext = (ImageButton) findViewById(R.id.btn_next);
        btnPrev = (ImageButton) findViewById(R.id.btn_prev);
        /*test */
        currentDate = new DateTime();
        setTvData(currentDate);

        chartPager = (MyViewPager) findViewById(R.id.chart_pager);
        groupPager = (RadioGroup) findViewById(R.id.pager_group);
        adapter = new ChartPagerAdapter(getSupportFragmentManager());
        chartPager.setAdapter(adapter);
        chartPager.setSlide(false);
        groupPager.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.ahi:
                        chartPager.setCurrentItem(0);
                        break;
                    case R.id.usage:
                        chartPager.setCurrentItem(1);
                        break;
                    case R.id.mask:
                        chartPager.setCurrentItem(2);
                        break;
                    default:
                        chartPager.setCurrentItem(0);
                        break;
                }
            }
        });

        setButtonListener();
    }

    private void setButtonListener() {
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate = currentDate.plusDays(1);
                setTvData(currentDate);
                adapter.getCurrentFragment().setSelectedValue(1);
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate = currentDate.minusDays(1);
                setTvData(currentDate);
                adapter.getCurrentFragment().setSelectedValue(-1);
            }
        });
    }

    public void setTvData(DateTime time) {
        tvDay.setText(MyTime.getWeekDay(time));
        tvDate.setText(MyTime.getDateString(time));

        if(currentDate!=time)currentDate = time;
        btnNext.setEnabled(MyTime.getOnlyDate(time) != MyTime.getOnlyDate(new DateTime()));
    }

    private String getDay(int dayOfWeek) {
        switch (dayOfWeek) {
            case 0:
                return "Sunday";
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
        }
        return "N/A";
    }

    private void showToast(String mess){
        Toast.makeText(this, mess, Toast.LENGTH_SHORT).show();
    }
}
