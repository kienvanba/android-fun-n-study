package com.example.kien.funnstudy.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.kien.funnstudy.graph.fragments.BaseChartFragment;

/**
 * Created by kienvanba on 9/25/17.
 */

public abstract class BaseFragment extends Fragment {
    protected static final String TAG = BaseChartFragment.class.getSimpleName();
    protected Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initOnlyCreateOnceData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(initLayout(), container, false);
        getControl(view);
        initOnCreatingViewData();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initCreatedViewData(view);
        initListener();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * Fragment is activated
     */

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     *  if get called back, onCreateView() will be call
     */

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    //fragment is destroyed

    protected abstract int initLayout();
    protected abstract void getControl(View v);
    protected void initOnlyCreateOnceData(){}
    protected void initOnCreatingViewData(){}
    protected void initCreatedViewData(View v){}
    protected void initListener(){}
    protected void showToast(String toastString){
        Toast.makeText(getContext(), toastString, Toast.LENGTH_SHORT).show();
    }
    protected void makeLog(String logString){
        Log.e(TAG, logString);
    }
    protected void makeLog(String TAG, String logString){
        Log.e(TAG, logString);
    }
}