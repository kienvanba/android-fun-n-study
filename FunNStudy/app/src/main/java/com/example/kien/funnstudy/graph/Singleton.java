package com.example.kien.funnstudy.graph;

import com.github.mikephil.charting.data.BarEntry;

/**
 * Created by kienvanba on 9/25/17.
 */

public class Singleton {
    private static Singleton instance = new Singleton();
    private BarEntry currentEntry;
    private Singleton(){}
    public static Singleton getInstance(){
        return instance;
    }
    public void setCurrentEntry(BarEntry entry){
        this.currentEntry = entry;
    }
    public BarEntry getCurrentEntry(){
        return currentEntry;
    }
}
