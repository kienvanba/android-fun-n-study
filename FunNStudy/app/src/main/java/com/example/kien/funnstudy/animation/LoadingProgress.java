package com.example.kien.funnstudy.animation;

import android.content.Context;
import android.view.animation.Animation;
import android.widget.ProgressBar;

/**
 * Created by KIEN on 9/19/2017.
 */

public class LoadingProgress extends ProgressBar {
    public LoadingProgress(Context context) {
        super(context);
    }

    @Override
    public void startAnimation(Animation animation) {
        super.startAnimation(animation);
    }
}
