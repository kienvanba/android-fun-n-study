package com.example.kien.funnstudy.graph.fragments;

import android.view.View;

import com.example.kien.funnstudy.R;
import com.example.kien.funnstudy.graph.MultiChartActivity;
import com.example.kien.funnstudy.graph.Singleton;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;

import org.joda.time.DateTime;

import java.util.Random;

/**
 * Created by kienvanba on 9/26/17.
 */

public class FragmentMask extends BaseChartFragment {
    private static final String TAG = FragmentMask.class.getSimpleName();

    @Override
    protected int initLayout() {
        return R.layout.layout_fragment_mask;
    }

    @Override
    protected void getControl(View v) {
        mChart = (BarChart) v.findViewById(R.id.chart);
    }

    @Override
    protected IAxisValueFormatter initAxisFormatter() {
        return new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return String.valueOf((int)value);
            }
        };
    }

    @Override
    protected String initLabel() {
        return TAG;
    }

    @Override
    protected void initOnlyCreateOnceData() {
        super.initOnlyCreateOnceData();
        for(int i=0; i<LOADED_BAR_NUMBER; i++){
            float val = (float)(new Random().nextInt(1000));
            BarEntry entry = new BarEntry(i, val);
            mData.addEntry(entry, 0);
        }
        mData.notifyDataChanged();
    }

    @Override
    protected void initCreatedViewData(View v) {
        super.initCreatedViewData(v);
        barNumber = v.getWidth()/DEFAULT_BAR_SIZE;
        v.post(new Runnable() {
            @Override
            public void run() {
                barNumber = mChart.getWidth()/DEFAULT_BAR_SIZE;
                mChart.setVisibleXRangeMaximum(barNumber);
                float x = Singleton.getInstance().getCurrentEntry()==null
                        ?mChart.getXChartMax()
                        :Singleton.getInstance().getCurrentEntry().getX();
                float y = Singleton.getInstance().getCurrentEntry()==null
                        ?mChart.getYChartMax()
                        :Singleton.getInstance().getCurrentEntry().getY();
                mChart.centerViewTo(
                        x,
                        y,
                        mChart.getAxisRight().getAxisDependency()
                );
                Highlight h = new Highlight(
                        x,
                        y,
                        0
                );
                mChart.highlightValue(h);
                ((MultiChartActivity)getActivity()).setTvData(new DateTime().minusDays(LOADED_BAR_NUMBER-1-((int)x)));
            }
        });
    }

    @Override
    protected void initListener() {
        super.initListener();
    }
}
