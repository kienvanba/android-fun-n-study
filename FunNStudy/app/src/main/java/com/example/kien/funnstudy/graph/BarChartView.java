package com.example.kien.funnstudy.graph;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.WindowManager;

import com.example.kien.funnstudy.R;

/**
 * Created by KIEN on 9/20/2017.
 */

public class BarChartView extends ChartView {
    private static final int DEFAULT_MAX_VALUE = 50;
    private static final int DEFAULT_MIN_VALUE = 0;
    private static final int DEFAULT_BAR_NUMBER = 3;
    private static final int DEFAULT_JUMP_VALUE = 10;

    private int mMaxValue;
    private int mMinValue;
    private int mJumpValue;

    private float startx, starty, stopx, stopy;

    public BarChartView(Context context) {
        super(context);
    }

    public BarChartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public int getmMaxValue() {
        return mMaxValue;
    }

    public void setmMaxValue(int maxValue) {
        this.mMaxValue = maxValue;
    }

    public int getmMinValue() {
        return mMinValue;
    }

    public void setmMinValue(int minValue) {
        this.mMinValue = minValue;
    }

    public int getmJumpValue() {
        return mJumpValue;
    }

    public void setmJumpValue(int jumpValue) {
        this.mJumpValue = jumpValue;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawBackground(canvas);
    }

    private void checkValues(){
        if(mMaxValue-mMinValue==0){
            mMaxValue = DEFAULT_MAX_VALUE;
            mMinValue = DEFAULT_MIN_VALUE;
            mJumpValue = DEFAULT_JUMP_VALUE;
        }
        if(mJumpValue==0)mJumpValue = DEFAULT_JUMP_VALUE;
    }

    protected void drawBackground(Canvas canvas){
        checkValues();
        mPaint.setColor(getContext().getResources().getColor(R.color.white));
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawRect(
                0,
                0,
                dpToPx(getWidth()),
                dpToPx(getHeight()),
                mPaint
        );
        mPaint.setStrokeWidth(5);
        mPaint.setColor(getContext().getResources().getColor(R.color.black));
//        canvas.drawLine(
//                dpToPx(0),
//                dpToPx(0),
//                dpToPx(0),
//                (dpToPx(getHeight()-dpToPx(5))),
//                mPaint
//        );
        canvas.drawLine(
                dpToPx(5),
                (dpToPx(getHeight()-dpToPx(5))),
                dpToPx(getWidth())-dpToPx(5),
                (dpToPx(getHeight()-dpToPx(5))),
                mPaint
        );
        canvas.drawCircle(
                dpToPx(0),
                dpToPx(0),
                dpToPx(10),
                mPaint
        );
        canvas.drawCircle(
                dpToPx(0),
                dpToPx(getHeight()),
                dpToPx(10),
                mPaint
        );
        canvas.drawCircle(
                dpToPx(getWidth()),
                dpToPx(0),
                dpToPx(10),
                mPaint
        );
        canvas.drawCircle(
                dpToPx(getWidth()),
                dpToPx(getHeight()),
                dpToPx(10),
                mPaint
        );
    }
}
